﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Adviser.Client.DataModel;
using Adviser.Client.Core.Presentation;

namespace Advisor.Modules.Interpretation
{
    public class ChakraViewModel : ViewModelBase
    {
        public ChakraViewModel()
        {
        }

        public ChakraViewModel(String name, double delta, string description)
        {
            this.Name = name;
            this.Delta = delta;
            this.Description = description;
        }

        public string Name
        {
            get;
            set;
        }

        public double Delta
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }
    }
}
