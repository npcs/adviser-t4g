﻿using System;
using Microsoft.Practices.Composite.Events;
using Advisor.Common.DataModel;

namespace Advisor.Modules.Catalog.Views
{
    public interface IPatientListPresenter
    {
        event EventHandler<DataEventArgs<Patient>> PatientSelected;
        IPatientListView View { get; }
        void SetSelectedPatient(Patient patient);
        
    }
}
