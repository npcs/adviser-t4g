﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Composite.Events;
using System.Data.Objects;
using System.Data.Objects.DataClasses;

namespace Advisor.Modules.Catalog.Views
{
    using System.Collections.ObjectModel;
    using Advisor.Common.DataModel;

    public interface IPatientListView
    {
        ObjectResult<Patient> Model { get; set; } //DTO
        event EventHandler<DataEventArgs<Patient>> PatientSelected;
    }
}
