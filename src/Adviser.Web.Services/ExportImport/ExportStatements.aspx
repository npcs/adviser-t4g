﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportStatements.aspx.cs" Inherits="Adviser.Web.Services.ExportImport.ExportStatements" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ListBox ID="YearListBox" runat="server"></asp:ListBox>
        <asp:ListBox ID="MonthListBox" runat="server" Rows="12">
            <asp:ListItem>01 - January</asp:ListItem>
            <asp:ListItem>02 - February</asp:ListItem>
            <asp:ListItem>03 - March </asp:ListItem>
            <asp:ListItem>04 - April</asp:ListItem>
            <asp:ListItem>05 - May</asp:ListItem>
            <asp:ListItem>06 - June</asp:ListItem>
            <asp:ListItem>07 - July</asp:ListItem>
            <asp:ListItem>08 - August</asp:ListItem>
            <asp:ListItem>09 - September</asp:ListItem>
            <asp:ListItem>10 - October</asp:ListItem>
            <asp:ListItem>11 - November</asp:ListItem>
            <asp:ListItem>12 - December</asp:ListItem>
        </asp:ListBox>
        <asp:Button ID="GetCustomersButton" runat="server" Text="Get Practitioners" 
            onclick="GetButton_Click" />
        <asp:Button ID="GetTransactionsButton" runat="server" 
            Text="Get Transactions" onclick="GetButton_Click" />
    </div>
    </form>
</body>
</html>
