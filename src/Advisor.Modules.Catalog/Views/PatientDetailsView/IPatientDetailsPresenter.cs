﻿using System;
using Microsoft.Practices.Composite.Events;
using Advisor.Common.DataModel;

namespace Advisor.Modules.Catalog.Views
{
    public interface IPatientDetailsPresenter
    {
        event EventHandler<DataEventArgs<Patient>> BackClicked;
        IPatientDetailsView View { get; set; }
        void SetSelectedPatient(Patient patient);
    }
}
