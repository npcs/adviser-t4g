﻿using System;
using Microsoft.Practices.Composite.Wpf.Events;
using Adviser.Client.DataModel;

namespace Adviser.Client.Core.Events
{
    public class PractitionerChangedEvent : CompositeWpfEvent<Practitioner>
    {
    }
}
