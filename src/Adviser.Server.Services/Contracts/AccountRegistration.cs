﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Adviser.Server.Services.Contracts
{
    [DataContract]
    public class AccountRegistration
    {
        [DataMember]
        public string AccountId;

        [DataMember]
        public bool IsSuccess;

        [DataMember]
        public string MessageText;
    }
}
