﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Advisor.Common.Presentation
{
    public interface IPrintableModel
    {
        UserControl PrintableView { get; }
        int OrderKey { get; }
        string Name { get; }
        bool SelectedToPrint { get; set; }
    }
}
