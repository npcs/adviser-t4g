﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;

using Advisor.Core;
using Advisor.Common.DataModel;

namespace Advisor.Presentation.Models
{
    public class BodyAssessmentViewModel : ViewModelBase
    {
        private ObservableCollection<ZoneAssessmentModel> zoneViewModels;
        private string assessment;
        private string recommendations;

        public BodyAssessmentViewModel(BodyAssessment model)
        {
            zoneViewModels = new ObservableCollection<ZoneAssessmentModel>();
            foreach(ZoneAssessment zone in model.Zones)
            {
                ZoneAssessmentModel zoneViewModel = new ZoneAssessmentModel(zone);
                zoneViewModels.Add(zoneViewModel);
            }
        }

        public string Assessment
        {
            get { return assessment; }
            set 
            { 
                assessment = value;
                base.RaisePropertyChangedEvent("Assessment");
            }
        }

        public string Recommendations
        {
            get { return recommendations; }
            set 
            { 
                recommendations = value;
                base.RaisePropertyChangedEvent("Recommendations");
            }
        }

        public ObservableCollection<ZoneAssessmentModel> Zones
        {
            get { return zoneViewModels; }
        }
    }
}
