﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Client.DataModel;
    using Microsoft.Practices.Composite.Events;

    public interface IRequestServerResutsView
    {
        event EventHandler ResutsRequested;
    }
}
