﻿using System;
using Adviser.Common.Domain;

namespace Adviser.Client.Core.DeviceControl
{
    public interface IDeviceManager
    {
        event MeasurementCompletedEventHandler MeasurementCompleted;
        event MeasurementProgressEventHandler MeasurementProgress;
        MeasurementSettings Settings { get; set; }

        void ReInitialize();
        void Initialize(DeviceSettings deviceSettings, MeasurementSettings measurementSettings);
        void Release();
        void StartMeasurement();
        void StopMeasurement();
        
        Guid GetDeviceId();
        byte[] GetDeviceVersion();
        double GetBusVoltage();
        bool IsConnected { get; }
        bool IsSet { get; }
        bool IsStopped { get; }
    }
}
