﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Adviser.Modules.PatientDatabase
{
    using Microsoft.Practices.Composite.Events;
    using Adviser.Client.DataModel;

    /// <summary>
    /// Interaction logic for VisitListView.xaml
    /// </summary>
    public partial class VisitListView : UserControl, IVisitListView
    {
        public VisitListView()
        {
            InitializeComponent();
        }

        public event EventHandler<DataEventArgs<Visit>> VisitSelected = delegate { };

        public IVisitListViewModel Model
        {
            get
            {
                return this.DataContext as IVisitListViewModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public void SetSelectedVisit(Visit visit)
        {
            VisitList.SelectedItem = visit;
        }

        private void VisitList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Visit visit = e.AddedItems[0] as Visit;
                if (visit != null)
                {
                    VisitSelected(this, new DataEventArgs<Visit>(visit));
                }
            }
        }

        #region Sorting
        GridViewColumnHeader _lastHeaderClicked = null;

        ListSortDirection _lastDirection = System.ComponentModel.ListSortDirection.Ascending;

        void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;

            if (headerClicked != null && headerClicked.Column != null)
            {
                if (headerClicked != _lastHeaderClicked)
                {
                    direction = ListSortDirection.Ascending;
                }
                else
                {
                    if (_lastDirection == ListSortDirection.Ascending)
                    {
                        direction = ListSortDirection.Descending;
                    }
                    else
                    {
                        direction = ListSortDirection.Ascending;
                    }
                }
                string header = headerClicked.Column.Header as string;
                Sort(header, direction);
                _lastHeaderClicked = headerClicked;
                _lastDirection = direction;
            }
        }

        private void Sort(string sortBy, ListSortDirection direction)
        {
            ICollectionView dataView =
       CollectionViewSource.GetDefaultView(VisitList.ItemsSource);
            //System.Windows.Data.Binding.



            dataView.SortDescriptions.Clear();
            SortDescription sd = new SortDescription(sortBy, direction);
            dataView.SortDescriptions.Add(sd);
            dataView.Refresh();

        }

        #endregion

        #region Delete Confirmation

        private void btnDeleteVisit_Click(object sender, RoutedEventArgs e)
        {
            this.DeleteConfirmationPopup.IsOpen = true;
            this.DeleteConfirmationPopup.StaysOpen = false;
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            this.DeleteConfirmationPopup.IsOpen = false;
        }
        #endregion
    }
}
