﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Server.Services
{
    public class AdviserSecurityException : ApplicationException
    {
        public AdviserSecurityException(string message) : base(message) { }
    }
}
