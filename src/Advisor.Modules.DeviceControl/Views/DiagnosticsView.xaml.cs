﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Advisor.Modules.DeviceControl.Views
{
    /// <summary>
    /// Interaction logic for DiagnosticsView.xaml
    /// </summary>
    public partial class DiagnosticsView : UserControl, IDiagnosticsView
    {
        public DiagnosticsView()
        {
            InitializeComponent();
        }

        #region IDiagnosticsView Members

        public void SetLeftPanel(object view)
        {
            LeftPanel.Content = view;
        }

        public void SetRightPanel(object view)
        {
            RightPanel.Content = view;
        }

        #endregion
    }
}
