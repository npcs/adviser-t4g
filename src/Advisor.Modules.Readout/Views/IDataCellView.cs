﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Composite.Events;

using Adviser.Common.Domain;
using Advisor.Modules.Readout.PresentationModels;

namespace Advisor.Modules.Readout.Views
{
    public interface IDataCellView
    {
        IDataCellViewModel Model { get; set; }
        event EventHandler<DataEventArgs<IDataCellViewModel>> CellSelected;
    }
}
