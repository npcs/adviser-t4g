﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using Advisor.Common.Configuration;
using Advisor.Common.DataModel;

namespace Advisor.Common.Services
{
    /// <summary>
    /// Provides access to the configuration settings.
    /// </summary>
    public class ConfigurationDataService : IConfigurationDataService
    {
        #region IConfigurationDataService Members

        /// <summary>
        /// Returns settings in the generic property bag (name/value pairs dictionary)
        /// </summary>
        /// <param name="settingsName"></param>
        /// <returns></returns>
        public XmlDictionary<string, string> LoadXmlSettings(string settingsFileName)
        {
//#if DEBUG
            //settingsFileName = settingsFileName;
//#endif
            XmlDictionary<string, string> xml = null;
            XmlSerializer ser = new XmlSerializer(typeof(XmlDictionary<string, string>));
            XmlTextReader reader = new XmlTextReader(settingsFileName);
            try
            {
                xml = (XmlDictionary<string, string>)ser.Deserialize(reader);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Exception while reading settings file " + settingsFileName, ex);
            }
            finally
            {
                reader.Close();
            }

            return xml;
        }

        /// <summary>
        /// Saves settings to the specified location serialized as Xml property bag.
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="settingsFileName"></param>
        public void SaveXmlSettings(XmlDictionary<string, string> xml, string settingsFileName)
        {
//#if DEBUG
//            settingsFileName = @"..\..\" + settingsFileName;
//#endif
            XmlSerializer ser = new XmlSerializer(typeof(XmlDictionary<string, string>));
            XmlTextWriter writer = new XmlTextWriter(settingsFileName, Encoding.UTF8);
            try
            {
                ser.Serialize(writer, xml);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Exception while saving settings file to " + settingsFileName, ex);
            }
            finally
            {
                writer.Close();
            }
        }

        /// <summary>
        /// Returns measurement settings.
        /// </summary>
        /// <returns></returns>
        public MeasurementSettings GetMeasurementSettings()
        {

            XmlDictionary<string, string> xml = LoadXmlSettings("MeasurementSettings.xml");
            if (xml == null)
                throw new ApplicationException("Exception while reading MeasurementSettings");

            MeasurementSettings settings = new MeasurementSettings();
            settings.DeviceSamplingRate = Convert.ToInt32(xml["DeviceSamplingRate"]);
            settings.IsFrontPositive = Convert.ToBoolean(xml["IsFrontPositive"]);
            settings.PairMeasurementDuration = Convert.ToInt32(xml["PairMeasurementDuration"]);
            settings.FrontLeads = Convert.ToInt32(xml["FrontLeads"]);
            settings.BackLeads = Convert.ToInt32(xml["BackLeads"]);
            settings.ShuntResistor = Convert.ToDouble(xml["ShuntResistor"]);
            settings.Voltage = Convert.ToDouble(xml["Voltage"]);
            settings.Calibration = Convert.ToBoolean(xml["Calibration"]);
            settings.ElectrodesConnectionMap = new List<PairConnection>(settings.FrontLeads * settings.BackLeads);

            //MeasurementSettings settings = new MeasurementSettings();
            //settings.DeviceSamplingRate = 5;
            //settings.IsFrontPositive = true;
            //settings.PairMeasurementDuration = 100;
            //settings.FrontLeads = 7;
            //settings.BackLeads = 7;
            //settings.ShuntResistor = 100;
            //settings.Voltage = 3.3;
            //settings.Calibration = false;
            //settings.ElectrodesConnectionMap = new List<PairConnection>(49);

            for (byte x = 0; x < settings.FrontLeads; ++x)
            {
                for (byte y = 0; y < settings.BackLeads; ++y)
                {
                    PairConnection pair = new PairConnection();
                    pair.Front = x;
                    pair.Back = y;
                    settings.ElectrodesConnectionMap.Add(pair);
                }
            }

            return settings;
        }

        /// <summary>
        /// Returns device settings.
        /// </summary>
        /// <returns></returns>
        public DeviceSettings GetDeviceSettings()
        {
            DeviceSettings deviceSettings = new DeviceSettings();

            XmlDictionary<string, string> xml = LoadXmlSettings("DeviceSettings.xml");
            deviceSettings.ADCAddress = Convert.ToByte(xml["ADCAddress"], 16);
            deviceSettings.ADCBusVoltageRegisterAddress = Convert.ToByte(xml["ADCBusVoltageRegisterAddress"], 16);
            deviceSettings.ADCConfigRegisterAddress = Convert.ToByte(xml["ADCConfigRegisterAddress"], 16);
            deviceSettings.ADCSettings = Convert.ToUInt16(xml["ADCSettings"], 16);
            deviceSettings.ADCShuntVoltageRegisterAddress = Convert.ToByte(xml["ADCShuntVoltageRegisterAddress"], 16);
            deviceSettings.CalibrationLeadA = byte.Parse(xml["CalibrationLeadA"]);
            deviceSettings.CalibrationLeadB = byte.Parse(xml["CalibrationLeadB"]);
            deviceSettings.ElectrodeConnectorDeviceAddress = Convert.ToByte(xml["ElectrodeConnectorDeviceAddress"], 16);
            deviceSettings.GuidAddress = int.Parse(xml["GuidAddress"]);
            deviceSettings.IsHighSpeedBus = bool.Parse(xml["IsHighSpeedBus"]);
            deviceSettings.MeasurementScaleFactor = Convert.ToDouble(xml["MeasurementScaleFactor"]);
            deviceSettings.EmulateDevice = Convert.ToBoolean(xml["EmulateDevice"]);
            return deviceSettings;
        }

        #endregion
    }
}
