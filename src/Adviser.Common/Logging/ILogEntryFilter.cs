﻿using System;
namespace Adviser.Common.Logging
{
    public interface ILogEntryFilter
    {
        LogSeverity SeverityThreshold { get; set; }
    }
}
