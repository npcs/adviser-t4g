﻿using System;
using System.Linq;
using System.ComponentModel;

using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Unity;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Services;
using Adviser.DataSync.Core;

namespace Adviser.Modules.DataSync
{
	public class PractitionerSyncHandler : IPractitionerSyncHandler
    {
        private readonly IEventAggregator _eventAggregator;
        private IUnityContainer _container;
        private SyncDataService _syncService;
        private IDBService _dbService;
        private BackgroundWorker _worker;


        public PractitionerSyncHandler(IEventAggregator eventAggregator, IUnityContainer container, IDBService dbService)
        {
            _eventAggregator = eventAggregator;
            _container = container;
            _dbService = dbService;
            _syncService = new SyncDataService();

            _worker = new BackgroundWorker()
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            _worker.DoWork += worker_DoWork;
            _worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            _worker.ProgressChanged += worker_ProgressChanged;

            _eventAggregator.GetEvent<PractitionerSyncEvent>().Subscribe(OnPractitionerSync, true);
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DataSyncResult r = (DataSyncResult)e.Result;
            var evt = new PractitionerSyncCompletedEventArgs()
            {
                Message = r.Message
            };
            if(r.IsSuccess)
            {
                var p = _dbService.GetPractitionerByID((Guid)r.Data);
                if (p.Status != AccountStatus.Confirmed)
                {
                    p.Status = AccountStatus.Confirmed;
                    _dbService.DataContext.SaveChanges();
                }
				evt.Practitioner = p;
            }
            _eventAggregator.GetEvent<PractitionerSyncCompletedEvent>().Publish(evt);
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var practitioners = _syncService.FindPractitioner((string)e.Argument);

            if (practitioners.Count == 1)
            {
                e.Result = _syncService.SyncData(practitioners.First(), _worker, e);
            }
            else if (practitioners.Count > 1)
            {
                e.Result = new DataSyncResult { IsSuccess = false, Message = "More than one partitioner with the same email" };
            }
            else
            {
                e.Result = new DataSyncResult { IsSuccess = false, Message = "Practitioner not found." };
            }
        }

        //private void worker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    e.Result = _syncService.ImportPractitioner((string)e.Argument, _worker, e);

        //}

        public void OnPractitionerSync(PractitionerSyncEventArgs args)
        {
            _worker.RunWorkerAsync(args.Email);
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
    }
}
