﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Diagnostics;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.UnityExtensions;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;

using Adviser.Client.Core.Presentation;
using Adviser.Client.Core.Services;
using Adviser.Common.Logging;

namespace AdvisorClient
{
    public class Bootstrapper : UnityBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            IShellView view = null;

            try
            {
                ShellPresenter presenter = Container.Resolve<ShellPresenter>();
                view = presenter.View;
                view.ShowView();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("AdvisorClient", ex.ToString(), EventLogEntryType.Error);
            }

            return view as DependencyObject;
        }

        protected override IModuleEnumerator GetModuleEnumerator()
        {
            ConfigurationModuleEnumerator enumerator = null;
            try
            {
                enumerator = new ConfigurationModuleEnumerator();
            }
            catch (Exception ex)
            {
                string message = "Error occured while loading Adviser application. Please contact support for assistance.";
                MessageBox.Show(message, "Application Error", MessageBoxButton.OK);
                
                ILogger logService = Container.Resolve<ILogger>();
                logService.LogError(ex, message);
            }

            return enumerator;

            //return new StaticModuleEnumerator()
            //    .AddModule(typeof(MainToolbarModule))
            //    .AddModule(typeof(CatalogModule))
            //    .AddModule(typeof(DeviceControlModule))
            //    .AddModule(typeof(InterpretationModule));
            ////.AddModule(typeof(ReadoutModule)); 
        }

        protected override void ConfigureContainer()
        {     
            Container.RegisterType<IShellView, Shell>();
            Container.RegisterType<IChildWindow, ChildWindow>();
            Container.RegisterType<ILogger, LogService<AdviserLogEntry>>(new Microsoft.Practices.Unity.ContainerControlledLifetimeManager());

            base.ConfigureContainer();
        }
    }
}
