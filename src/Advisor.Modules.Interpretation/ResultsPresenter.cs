﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Events;

using Adviser.Client.Core.Presentation;
using Adviser.Client.Core.Events;

namespace Advisor.Modules.Interpretation
{
    using Advisor.Modules.Interpretation.Views;

    public class ResultsPresenter : IResultsPresenter
    {
        IUnityContainer _container;
        AppointmentSession _session;

        public ResultsPresenter(IUnityContainer container, IEventAggregator eventAggregator, IResultsView view, AppointmentSession session)//IHumanBodyPresenter bodyPresenter, IResulsDetailPresenter resultsDetail
        {
            _container = container;
            _session = session;
            View = view;

            eventAggregator.GetEvent<NavigationEvent>().Subscribe(OnNavigationEvent, true);
        }

        public IResultsView View { get; set; }

        private void OnNavigationEvent(NavigationEventArgs e)
        {
            if (e.EventName == NavigationEventNames.Results)
            {
                if (_session.HasResults)
                {
                    IHumanBodyPresenter bodyPresenter = _container.Resolve<IHumanBodyPresenter>();
                    IResulsDetailPresenter resultsDetail = _container.Resolve<IResulsDetailPresenter>();

                    View.SetLeftView(bodyPresenter.View);
                    View.SetRightView(resultsDetail.View);
                }
                else
                {
                    //IRequestServerResultsPresenter requestPresenter = _container.Resolve<IRequestServerResultsPresenter>();
                    ////requestPresenter.ResultsReceived += new EventHandler(requestPresenter_ResultsReceived);
                    //requestPresenter.Show();
                }
            }
            else if(e.EventName == NavigationEventNames.GetServerResults)
            {
                IRequestServerResultsPresenter requestPresenter = _container.Resolve<IRequestServerResultsPresenter>();
                requestPresenter.Show();
            }
            else{
            }
        }

        void requestPresenter_ResultsReceived(object sender, EventArgs e)
        {
            IHumanBodyPresenter bodyPresenter = _container.Resolve<IHumanBodyPresenter>();
            IResulsDetailPresenter resultsDetail = _container.Resolve<IResulsDetailPresenter>();

            View.SetLeftView(bodyPresenter.View);
            View.SetRightView(resultsDetail.View);
        }
    }
}
