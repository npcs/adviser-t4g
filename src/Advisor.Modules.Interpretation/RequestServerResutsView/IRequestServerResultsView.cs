﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Modules.Interpretation
{
    using Adviser.Client.DataModel;
    using Microsoft.Practices.Composite.Events;

    public interface IRequestServerResultsView
    {
        event EventHandler ResutsRequested;
        event EventHandler CancelClick;
        string UserMessage { get; set; }
        bool SubmitEnabled { get; set; }
        bool CancelEnabled { get; set; }
    }
}
