﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

using Adviser.Client.Core.Services;
using Adviser.Client.Core.Interfaces;
using Adviser.Common.Domain;
using Adviser.Client.Core.Presentation;

namespace Adviser.Modules.DataSync
{
    public class DataSyncModule : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;
        private IPractitionerSyncHandler _syncService;

        public DataSyncModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            _container.RegisterType<IDBService, DBService>(new Microsoft.Practices.Unity.ContainerControlledLifetimeManager());
            _container.RegisterType<IPractitionerSyncHandler, PractitionerSyncHandler>();

            _syncService = _container.Resolve<IPractitionerSyncHandler>();
        }
    }
}
