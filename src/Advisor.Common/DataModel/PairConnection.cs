﻿using System;

namespace Advisor.Common.DataModel
{
    [Serializable()]
    public struct PairConnection
    {
        public byte Front;
        public byte Back;
    }
}
