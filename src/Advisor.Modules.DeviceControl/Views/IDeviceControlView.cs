﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Adviser.Common.Domain;

namespace Advisor.Modules.DeviceControl.Views
{
    public interface IDeviceControlView
    {
        IDeviceControlPresentationModel Model { set; }
        event EventHandler ViewUnloaded;
    }
}
