﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Client.DataModel
{
    public partial class Patient
    {
        public static string[] LifestyleEnumValues
            = new string[] { PatientLifestyle.Active.ToString(), PatientLifestyle.Moderate.ToString(), PatientLifestyle.Sedentary.ToString() };

        public string[] LifestyleValues
        {
            get
            {
                return Patient.LifestyleEnumValues;
            }
        }
    }
}
