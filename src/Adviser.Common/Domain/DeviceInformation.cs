﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Common.Domain
{
    public class DeviceInformation
    {
        public Guid ID
        {
            get;
            set;
        }

        public byte[] Version
        {
            get;
            set;
        }

        public bool IsConnected
        {
            get;
            set;
        }

        public bool IsSet
        {
            get;
            set;
        }

        public double Voltage
        {
            get;
            set;
        }
    }
}
