﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Data.Objects.DataClasses;
using System.IO;
using System.Runtime.Remoting;
using System.Runtime.Serialization.Formatters.Binary;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Wpf.Commands;

using Adviser.Common.Domain;
using Adviser.Client.Core.Presentation;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Services;
using Advisor.Modules.Interpretation.Views;
using Adviser.Common.Logging;

namespace Advisor.Modules.Interpretation
{
    public interface IResultsDetailViewModel
    {
        event EventHandler<MeasurementEventArgs> MeasurementDataExtracted;
        ObservableCollection<RecommendedMedicineViewModel> RecommendedMedicine { get; }
        ObservableCollection<Prescription> Prescriptions { get; }
        ObservableCollection<ChakraViewModel> Chakras { get; }
        MandalaViewModel Mandala { get; set; }
        DelegateCommand<object> AddPrescriptionCommand { get; }
        DelegateCommand<object> DeletePrescriptionCommand { get; }
        DelegateCommand<object> UpdatePrescriptionCommand { get; }
        DelegateCommand<object> MoveToPrescriptionCommand { get; }
        DelegateCommand<object> SaveChangesCommand { get; }
        DelegateCommand<object> ShowNotesHistoryCommand { get; }
        DelegateCommand<object> StartMandalaCommand { get; }

        string NewPrescriptionName { get; set; }
        string NewPrescriptionDescription { get; set; }
        string Notes { get; set; }
        ObservableCollection<NotesHistoryModel> NotesHistory { get; }
    }

    /// <summary>
    /// Presentation model for Adviser results view.
    /// </summary>
    public class ResultsDetailViewModel : ViewModelBase, IResultsDetailViewModel
    {
        public event EventHandler<MeasurementEventArgs> MeasurementDataExtracted;

        private IDBService _dbService;
        private AppointmentSession _session;

        private ObservableCollection<RecommendedMedicineViewModel> _recommendedMedicine;
        private ObservableCollection<Prescription> _prescriptions;
        private string _notes;
        private ObservableCollection<NotesHistoryModel> _notesHistory;
        private ObservableCollection<ChakraViewModel> _chakras;
        private ObservableCollection<Condition> _allConditions;
        private IUnityContainer _container;
        private ILogger _logService;

        public DelegateCommand<object> AddPrescriptionCommand { get; private set; }
        public DelegateCommand<object> DeletePrescriptionCommand { get; private set; }
        public DelegateCommand<object> UpdatePrescriptionCommand { get; private set; }
        public DelegateCommand<object> MoveToPrescriptionCommand { get; private set; }
        public DelegateCommand<object> ShowNotesHistoryCommand { get; private set; }
        public DelegateCommand<object> StartMandalaCommand { get; private set; }
        public DelegateCommand<object> SaveChangesCommand { get; private set; }

        public ResultsDetailViewModel(IDBService service, IEventAggregator eventAggregator, IUnityContainer container, ILogger logService)
        {
            _dbService = service;
            _session = container.Resolve<AppointmentSession>();
            _container = container;
            _logService = logService;

            AllConditions = new ObservableCollection<Condition>(_session.AllConditions);
            NotesHistory = new ObservableCollection<NotesHistoryModel>();

            AddPrescriptionCommand = new DelegateCommand<object>(AddPrescription);
            DeletePrescriptionCommand = new DelegateCommand<object>(DeletePrescription);
            MoveToPrescriptionCommand = new DelegateCommand<object>(MoveToPrescription);
            UpdatePrescriptionCommand = new DelegateCommand<object>(UpdatePrescription);
            ShowNotesHistoryCommand = new DelegateCommand<object>(ShowNotesHistory);
            StartMandalaCommand = new DelegateCommand<object>(StartMandala);
            SaveChangesCommand = new DelegateCommand<object>(SaveChanges);

            Aura = new AuraViewModel(_dbService);
        }

        private void AddPrescription(object p)
        {
            Prescription prescription = new Prescription();

            prescription.ID = Guid.NewGuid();
            prescription.MedicineName = NewPrescriptionName;
            prescription.Directions = NewPrescriptionDescription;

            _session.Visit.Prescriptions.Add(prescription);
            this.Prescriptions.Add(prescription);
            _dbService.SaveChanges();
        }

        private void DeletePrescription(object p)
        {
            Prescription prescription = (Prescription)p;
            this.Prescriptions.Remove(prescription);
            _dbService.DeleteObject(prescription);
        }

        private void UpdatePrescription(object p)
        {
            SaveChanges(p);
        }

        private void ShowNotesHistory(object p)
        {
            NotesHistory.Clear();
            string HighlightColor = "#00000000";
            foreach (Visit v in _session.Patient.Visits.OrderBy(b=>b.Date))
            {
                if (v.Notes != null && v.Notes != String.Empty)
                {
                    if (v == _session.Visit)
                    {
                        HighlightColor = "#769271";
                    }
                    else
                    {
                        HighlightColor = "#00000000";
                    }
                    NotesHistory.Add(new NotesHistoryModel(v.Date.ToLocalTime(), v.Notes, HighlightColor));
                }
            }
        }

        private void SaveChanges(object p)
        {           
            _dbService.SaveChanges();
        }

        private void StartMandala(object p)
        {
            IMandalaTherapyPresenter mandalaPresenter = _container.Resolve<IMandalaTherapyPresenter>();
            mandalaPresenter.Show(Aura.Mandala);
        }

        private void MoveToPrescription(object p)
        {
            try
            {
                if (p == null)
                    return;
                RecommendedMedicineViewModel medicine = (RecommendedMedicineViewModel)p;
                Prescription prescription = new Prescription();

                prescription.ID = Guid.NewGuid();
                prescription.MedicineID = medicine.ID;
                prescription.MedicineName = medicine.Name;
                prescription.Directions = medicine.Direction;
                _session.Visit.Prescriptions.Add(prescription);
                this.Prescriptions.Add(prescription);
                _dbService.SaveChanges();
            }
            catch (Exception ex)
            {
                string errMsg = String.Format("Error occured while adding prescription for visit {0}", _session.Visit.ID);
                System.Windows.MessageBox.Show(errMsg, "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                _logService.LogError(ex, errMsg);
            }
        }

        internal void SetData(Visit visit)
        {
            try
            {
                if (visit == null)
                    return;

                if (visit.ExamResult == null || visit.ExamResult.Length == 0)
                    return;

                //Notes = visit.Notes;

                AdviserResults results = null;
                byte[] rawData = visit.ExamResult;
                if (rawData != null)
                {
                    BinaryFormatter fmt = new BinaryFormatter();
                    using (MemoryStream stream = new MemoryStream(rawData))
                    {
                        stream.Seek(0, SeekOrigin.Begin);
                        results = (AdviserResults)fmt.Deserialize(stream);
                    }
                }

                NewPrescriptionName = "";
                NewPrescriptionDescription = "";

                //- fill list of recommended medicine
                ObservableCollection<RecommendedMedicineViewModel> recMed = new ObservableCollection<RecommendedMedicineViewModel>();
                if (results != null && results.MedicineRecommendation != null)
                {
                    foreach (MedicineRelevance item in results.MedicineRecommendation)
                    {
                        //TODO: replace this call with lookup in session dictionary for better performance
                        Medicine medicine = _dbService.DataContext.MedicineSet.FirstOrDefault(m => m.ID == item.MedicineID);//_dbService.GetMedicineByID(item.MedicineID);
                        RecommendedMedicineViewModel medVM = new RecommendedMedicineViewModel(medicine);
                        medVM.Relevance = item.Relevance;
                        recMed.Add(medVM);
                    }
                    RecommendedMedicine = recMed;

                }

                //- load existing prescriptions
                if (!visit.Prescriptions.IsLoaded)
                    visit.Prescriptions.Load();
                Prescriptions = new ObservableCollection<Prescription>(visit.Prescriptions);

                Aura.SetData(results, _session.Patient.Sex);
            }
            catch (Exception ex)
            {
                string errMsg = String.Format("Error occured while reading interpretation data for visit {0}", _session.Visit.ID);
                System.Windows.MessageBox.Show(errMsg, "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                _logService.LogError(ex, errMsg);   
            }
        }

        #region IResultsDetailViewModel Members

        private string _NewPrescriptionName;
        public string NewPrescriptionName
        {
            get
            {
                return _NewPrescriptionName;
            }
            set
            {
                _NewPrescriptionName = value;
                base.RaisePropertyChangedEvent("NewPrescriptionName");
            }
        }

        private string _NewPrescriptionDescription;
        public string NewPrescriptionDescription
        {
            get
            {
                return _NewPrescriptionDescription;
            }
            set
            {
                _NewPrescriptionDescription = value;
                base.RaisePropertyChangedEvent("NewPrescriptionDescription");
            }
        }

        public ObservableCollection<Condition> AllConditions
        {
            get { return _allConditions; }
            set
            {
                _allConditions = value;
                RaisePropertyChangedEvent("AllConditions");
            }
        }

        public AuraViewModel Aura
        {
            get;
            set;
        }

        /// <summary>
        /// Read only collection of recommended medicine per Adviser testing.
        /// </summary>
        public ObservableCollection<RecommendedMedicineViewModel> RecommendedMedicine
        {
            get { return _recommendedMedicine; }
            private set
            {
                _recommendedMedicine = value;
                base.RaisePropertyChangedEvent("RecommendedMedicine");
            }
        }

        public ObservableCollection<Prescription> Prescriptions
        {
            get { return _prescriptions; }
            private set
            {
                _prescriptions = value;
                base.RaisePropertyChangedEvent("Prescriptions");
            }
        }

        public ObservableCollection<ChakraViewModel> Chakras
        {
            get { return _chakras; }
            private set
            {
                _chakras = value;
                base.RaisePropertyChangedEvent("Chakras");
            }
        }

        public ObservableCollection<NotesHistoryModel> NotesHistory
        {
            get
            {
                return _notesHistory;
            }
            private set
            {
                _notesHistory = value;
                base.RaisePropertyChangedEvent("NotesHistory");
            }
        }

        public string Notes
        {
            get { return _session.Visit != null ? _session.Visit.Notes : String.Empty; }
            set
            {
                if (_session.Visit != null && _session.Visit.Notes != value)
                {
                    _session.Visit.Notes = value;
                    base.RaisePropertyChangedEvent("Notes");
                }
            }
        }

        private MandalaViewModel _mandala;
        public MandalaViewModel Mandala
        {
            get { return _mandala; }
            set
            {
                _mandala = value;
            }

        }

        #endregion
    }
}
