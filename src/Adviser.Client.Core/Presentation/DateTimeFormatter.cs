﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace Adviser.Client.Core.Presentation
{
    [ValueConversion(typeof(DateTime), typeof(String))]
    public class DateTimeFormatter : IValueConverter
    {
        public DateTimeFormatter() { }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string result = String.Empty;
            if (value != null)
            {
                string formatString = (string)parameter;
                result = ((DateTime)value).ToLocalTime().ToString(formatString);
            }

            return result;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("Method is not implemented.");
        }
    }
}
