﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Common.Domain
{
    public interface IValueTransform
    {
        double Transform(double source);
        void SetParameters(object[] args);
    }
}
