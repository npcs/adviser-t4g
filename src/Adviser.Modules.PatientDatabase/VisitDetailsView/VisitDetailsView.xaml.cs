﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Composite.Events;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Client.DataModel;

    /// <summary>
    /// Interaction logic for VisitDetailsView.xaml
    /// </summary>
    public partial class VisitDetailsView : UserControl, IVisitDetailsView
    {
        public VisitDetailsView()
        {
            InitializeComponent();
        }

        public IVisitDetailsViewModel Model
        {
            get
            {
                return this.DataContext as IVisitDetailsViewModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        private void txtNotes_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Model.ShowNotesHistoryCommand.Execute(null);
            ShowNotesPopup(sender);
        }

        private void ShowNotesPopup(object sender)
        {
            VisitNotesHistoryPopup.PlacementTarget = (UIElement)sender;
            this.VisitNotesHistoryPopup.IsOpen = true;
            VisitNotesHistoryPopup.StaysOpen = true;
        }

        private void HideNotesPopup()
        {
            VisitNotesHistoryPopup.IsOpen = false;
        }

        private void ShowNotesHistory_Click(object sender, RoutedEventArgs e)
        {
            ShowNotesPopup(sender);
        }

        private void btnHideVisitNotesHistoryButton_Click(object sender, RoutedEventArgs e)
        {
            HideNotesPopup();
        }
        
        /// <summary>
        /// Update database when anything is changed on the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void UpdateDatabase(object sender, RoutedEventArgs e)
        //{
        //    Model.UpdateVisitCommand.Execute(null);
        //}
    }
}
