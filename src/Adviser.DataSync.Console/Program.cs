﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Adviser.DataSync.Core;

namespace Adviser.DataSync.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SyncDataService service = new SyncDataService();

                service.SyncData();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Console.ReadLine();
        }
    }
}
