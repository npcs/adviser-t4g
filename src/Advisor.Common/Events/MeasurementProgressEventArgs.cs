﻿using System;
using System.Collections.Generic;
using Advisor.Common.DataModel;

namespace Advisor.Common.Events
{
    [Serializable()]
    public class MeasurementProgressEventArgs : EventArgs
    {
        public int FrontElectrode { get; set; }
        public int BackElectrode { get; set; }
        public decimal TotalProgress { get; set; }
        public decimal PairProgress { get; set; }
        public int SampleIndex { get; set; }
        public PairMeasurement Measurement { get; set; }
    }
}
