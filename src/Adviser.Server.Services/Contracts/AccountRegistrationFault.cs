﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace Adviser.Server.Services.Contracts
{
    [DataContract]
    public class AccountRegistrationFault
    {
        public string Message;
    }
}
