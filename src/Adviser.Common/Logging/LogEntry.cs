﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Common.Logging
{
    /// <summary>
    /// Logging entry logged by a Logger.
    /// </summary>
    [Serializable]
    public class LogEntry : ILogEntry
    {
        /// <summary>
        /// Create a new instance of LogEntry.
        /// </summary>
        protected internal LogEntry()
            : base()
        {
        }
        /// <summary>
        /// Create a new instance of LogEntry.
        /// </summary>
        /// <param name="severity">The severity of the LogEntry.</param>
        /// <param name="application">A String representing the application.</param>
        /// <param name="category">A category to classify the LogEntry.</param>
        /// <param name="aMessage">The primary text of the LogEntry.</param>
        protected internal LogEntry(LogSeverity severity, string application, string category, string logItemType, string message)
            : this(severity, application, category, logItemType, message, null, DateTime.Now.ToUniversalTime())
        {
        }

        protected internal LogEntry(LogSeverity severity, String application, string category, string logItemType, String message, Object details)
            : this(severity, application, category, logItemType, message, details, DateTime.Now.ToUniversalTime())
        {
        }

        /// <summary>
        /// Create a new instance of LogEntry.
        /// </summary>
        /// <param name="severity">The severity of the LogEntry.</param>
        /// <param name="application">A String representing the application.</param>
        /// <param name="category">A category to classify the LogEntry.</param>
        /// <param name="aMessage">The primary text of the LogEntry.</param>
        /// <param name="time">The timestamp when the LogEntry was created.</param>
        protected internal LogEntry(LogSeverity severity, string application, string category, string logItemType, string message, Object details, DateTime time)
            : this()
        {
            Severity = severity;
            Application = application;
            Category = category;
            LogItemType = logItemType;
            ErrorMessage = message;
            Details = details;
            Time = time;
        }

        /// <summary>
        /// Gets and sets the application.
        /// </summary>
        public String Application
        {
            get;
            set;
        }
        /// <summary>
        /// Gets and sets the category.
        /// </summary>
        public string Category
        {
            get;
            set;
        }
        /// <summary>
        /// Gets and sets the timestamp.
        /// </summary>
        public DateTime Time
        {
            get;
            set;
        }
        /// <summary>
        /// Gets and sets the type.
        /// </summary>
        public String LogItemType
        {
            get;
            set;
        }

        /// <summary>
        /// Error message.
        /// </summary>
        public String ErrorMessage
        {
            get;
            set;
        }

        /// <summary>
        /// Gets and sets details.
        /// </summary>
        public Object Details
        {
            get;
            set;
        }

        /// <summary>
        /// Gets and sets the severity.
        /// </summary>
        public LogSeverity Severity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a friendly String that represents the severity.
        /// </summary>
        public String SeverityString
        {
            get { return Severity.ToString("G"); }
        }

        /// <summary>
        /// Represent the LogEntry as a String.
        /// </summary>
        /// <returns>The String representation of the LogEntry.</returns>
        public override String ToString()
        {
            return "" + Application
                + ", " + SeverityString
                + ", " + Category
                + ", " + LogItemType
                + ", " + Time
                + "\n" + ErrorMessage
                + "\n" + Details;
        }
    }
}
