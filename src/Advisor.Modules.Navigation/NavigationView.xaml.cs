﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Advisor.Modules.Navigation
{
    /// <summary>
    /// Interaction logic for NavigationView.xaml
    /// </summary>
    public partial class NavigationView : UserControl, INavigationView
    {
        public NavigationView()
        {
            InitializeComponent(); 
        }

        public INavigator Model
        {
            get { return DataContext as INavigator; }
            set 
            { 
                DataContext = value;
                CreateCommandBindings();
            }
        }

        private void NavigationContol_Click(object sender, RoutedEventArgs e)
        {
            Model.OnNavigationCommand(sender, e);
        }

        protected void CreateCommandBindings()
        {
            KeyBinding keyBinding = new KeyBinding(Model.KeyboardCommand, Key.P, ModifierKeys.Control);
            keyBinding.CommandParameter = Key.P;
            InputBindings.Add(keyBinding);
        }
    }
}
