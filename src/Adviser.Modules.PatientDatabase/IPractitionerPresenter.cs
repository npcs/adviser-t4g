﻿using System;
namespace Adviser.Modules.PatientDatabase
{
    public interface IPractitionerPresenter
    {
        IPractitionerDetailsView DetailsView { get; set; }
        IPractitionerLoginView LoginView { get; set; }
        IPractitionerRetrievePasswordView RetrievePasswordView { get; set; }
        void ShowUpdateAccount(bool isUpdateMode);        
        void Start();
    }
}
