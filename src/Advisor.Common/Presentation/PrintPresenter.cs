﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Advisor.Common.Presentation
{
    /// <summary>
    /// Base class for the presenter that supports rendering 
    /// print friendly view. 
    /// </summary>
    public abstract class PrintPresenter : ViewModelBase, IPrintableModel
    {
        private bool _selectedToPrint;

        public string Name
        {
            get;
            protected set;
        }

        public int OrderKey
        {
            get;
            protected set;
        }

        public bool SelectedToPrint
        {
            get
            {
                return _selectedToPrint;
            }
            set
            {
                _selectedToPrint = value;
                RaisePropertyChangedEvent("SelectedToPrint");
            }
        }

        public UserControl PrintableView
        {
            get;
            protected set;
        }
    }
}
