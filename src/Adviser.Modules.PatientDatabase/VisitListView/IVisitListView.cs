﻿using System;
using System.Collections.Generic;

namespace Adviser.Modules.PatientDatabase
{
    using Microsoft.Practices.Composite.Events;
    using Adviser.Client.DataModel;

    public interface IVisitListView
    {
        IVisitListViewModel Model { get; set; }
        event EventHandler<DataEventArgs<Visit>> VisitSelected;
        void SetSelectedVisit(Visit visit);
    }
}
