﻿using System;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using Advisor.Common.DataModel;

namespace Advisor.Common.Services
{
    public interface IDBService
    {
        void AddPatient(Practitioner CurrentPractitioner);
        Patient CreateNewPatient(Practitioner CurrentPractitioner);
        Visit CreateNewVisit(Patient CurrentPatient);
        IQueryable<Organ> GetAllOrgans();
        IQueryable<Condition> GetAllConditions();
        IQueryable<Medicine> GetAllMedicine();
        IQueryable<Organ> GetOrgansBySex(bool isMale);
        IQueryable<OrganZoneDependency> GetOrganZoneDependencies();
        IQueryable<Symptom> GetAllSymptoms();
        Medicine GetMedicineByID(int id);
        void DeleteObject(object EntityObject);
        object GetObjectByKey(System.Data.EntityKey key);
        Condition GetCondition(int organId, int examValue);
        void AddMedicineChronicOrgan(MedicineChronicOrgan entry);
        void AddMedicineOrganCondition(MedicineOrganConditionXRef entry);
        //EntityCollection<Condition> RetriveConditionEntityCollection(Organ CurrentOrgan);
        EntityCollection<OrganExamination> RetriveOrganExaminationEntityCollection(Visit CurrentVisit);
        EntityCollection<Patient> RetrivePatientEntityCollection(Practitioner CurrentPractitioner);
        Practitioner RetrivePractitioner(Guid PractitionerID);
        EntityCollection<Prescription> RetrivePrescriptionEntityCollection(Visit CurrentVisit);
        EntityCollection<Symptom> RetriveSymptomEntityCollection(Visit CurrentVisit);
        EntityCollection<Visit> RetriveVisitEntityCollection(Patient CurrentPatient);
        void SaveChanges();
    }
}
