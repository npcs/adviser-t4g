﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Adviser.Common.ServiceContract
{
    /// <summary>
    /// This is interface for account services.
    /// </summary>
    [ServiceContract]
    public interface IAccountService
    {
        [OperationContract]
        AccountServiceResponse CreateAccount(Practitioner practitioner);

        [OperationContract]
        AccountServiceResponse AddDeviceToAccount(Practitioner practitioner, string deviceId);

    }

    /// <summary>
    /// TODO: This is a stub contract. Replace with real one.
    /// </summary>
    [DataContract]
    public class Practitioner
    {
        string login = "";
        string password = "";

        [DataMember]
        public string Login
        {
            get { return login; }
            set { login = value; }
        }

        [DataMember]
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
    }

    [DataContract]
    public class AccountServiceResponse
    {
    }
}
