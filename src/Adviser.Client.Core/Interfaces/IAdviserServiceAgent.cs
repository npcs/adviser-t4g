﻿using System;
using System.Collections.Generic;
using System.Linq;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;

namespace Adviser.Client.Core.Interfaces
{
    public interface IAdviserServiceAgent
    {
        bool UpdatePractitioner(Practitioner practitioner, out string message);
        bool RegisterPractitioner(Practitioner practitioner, out string message);
        bool RetrievePassword(string email, out string message);
        ResultsReceivedEventArgs GetResults(Visit visit);
        bool ReportError(Adviser.Common.Logging.AdviserLogEntry logEntry);
        bool UpdatePatients(Practitioner practitioner, Patient[] patients);
        bool UpdateVisits(Practitioner practitioner, Visit[] visits);
        bool UpdatePrescriptions(Practitioner practitioner, Prescription[] prescriptions);
    }
}
