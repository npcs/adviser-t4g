﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Modules.Navigation
{
    /// <summary>
    /// Lookup strings for Navigation controls (must match id in the NavigationMap.xml)
    /// </summary>
    public static class NavigationControls
    {
        public static string Patients = "btnPatients";
        public static string Visits = "btnVisits";
        public static string Measurement = "btnDiagnostics";
        public static string Results = "btnResults";
        public static string Print = "btnPrint";
    }
}
