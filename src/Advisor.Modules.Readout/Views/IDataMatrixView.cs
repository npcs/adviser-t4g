﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Wpf.Commands;

using Adviser.Common.Domain;

using Advisor.Modules.Readout.PresentationModels;

namespace Advisor.Modules.Readout.Views
{
    public interface IDataMatrixView
    {
        event EventHandler<DataEventArgs<IDataCellViewModel>> CellSelected;
        IDataMatrixViewModel Model { get; set; }
    }
}
