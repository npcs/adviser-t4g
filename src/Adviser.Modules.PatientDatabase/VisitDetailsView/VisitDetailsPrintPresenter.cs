﻿using System;
using System.Collections.Generic;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Wpf;

using Adviser.Client.DataModel;
using Adviser.Client.Core.Presentation;

namespace Adviser.Modules.PatientDatabase
{
    /// <summary>
    /// Implements PrintPresenter for Visit details section.
    /// </summary>
    public class VisitDetailsPrintPresenter : PrintPresenter
    {
        public VisitDetailsPrintPresenter(VisitDetailsPrintView printableView, IVisitDetailsPrintViewModel model)
        {
            Name = "Visit Information";
            OrderKey = 20;
            SelectedToPrint = true;
            printableView.DataContext = model;
            PrintableView = printableView;
        }
    }
}
