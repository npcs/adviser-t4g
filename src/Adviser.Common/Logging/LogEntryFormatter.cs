﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Common.Logging
{
    public abstract class LogEntryFormatter : ILogEntryFormatter
    {
        /// <summary>
        /// A format String for DateTime.
        /// </summary>
        private String _timeFormat = "";
        /// <summary>
        /// Gets and sets the format String.
        /// </summary>
        public String TimeFormat
        {
            get { return _timeFormat; }
            set { _timeFormat = value; }
        }

        /// <summary>
        /// String format of the LogEntry.
        /// </summary>
        /// <param name="logEntry">The LogEntry to format.</param>
        /// <returns>A nicely formatted String.</returns>
        protected internal abstract string AsString(ILogEntry logEntry);


        /// <summary>
        /// String format of the DateTime of the LogEntry.
        /// </summary>
        /// <param name="logEntry">The LogEntry whose timestamp needs formatting.</param>
        /// <returns>A nicely formatted String.</returns>
        protected string DateString(ILogEntry logEntry)
        {
            return logEntry.Time.ToString(TimeFormat);
        }
        /// <summary>
        /// LogEntryFormatter constructor.
        /// </summary>
        protected LogEntryFormatter()
            : base()
        {
        }
    }
}
