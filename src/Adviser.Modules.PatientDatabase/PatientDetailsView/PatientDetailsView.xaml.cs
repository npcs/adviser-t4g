﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Windows.Controls;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Client.DataModel;

    /// <summary>
    /// Interaction logic for PatientDetailsView.xaml
    /// </summary>
    public partial class PatientDetailsView : UserControl, IPatientDetailsView
    {
        public PatientDetailsView()
        {
            InitializeComponent();
        }

        public IPatientsPresentationModel Model
        {
            get
            {
                return this.DataContext as IPatientsPresentationModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            Model.SaveDataCommand.Execute(sender);
        }
    }
}
