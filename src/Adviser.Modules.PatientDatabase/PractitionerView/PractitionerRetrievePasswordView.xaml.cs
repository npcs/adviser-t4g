﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Adviser.Modules.PatientDatabase
{
    /// <summary>
    /// Interaction logic for PractitionerRetrievePasswordView.xaml
    /// </summary>
    public partial class PractitionerRetrievePasswordView : UserControl, IPractitionerRetrievePasswordView
    {
        public PractitionerRetrievePasswordView()
        {
            InitializeComponent();
        }

        #region IPractitionerRetrievePasswordView Members

        public void ShowMessage(string message)
        {
            MessageTextBlock.Text = message;
            MessagePopup.IsOpen = true;
            MessagePopup.StaysOpen = true;
        }

        public IPractitionerModel Model
        {
            get
            {
                return this.DataContext as IPractitionerModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public event EventHandler RetrievePasswordClicked;
        public event EventHandler CancelClicked;

        #endregion

        private void RetrievePasswordButton_Click(object sender, RoutedEventArgs e)
        {
            BindingExpression binding = EmailTextBox.GetBindingExpression(TextBox.TextProperty);
            binding.UpdateSource();

            if (!Validation.GetHasError(EmailTextBox))
            {
                RetrievePasswordClicked(this, null);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            CancelClicked(this, null);
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            this.MessagePopup.IsOpen = false;
            if (Model.IsStatusOk)
            {
                CancelClicked(this, null);
            }
        }
    }
}
