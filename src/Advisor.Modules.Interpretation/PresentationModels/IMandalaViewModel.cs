﻿using System;
namespace Advisor.Modules.Interpretation
{
    public interface IMandalaViewModel
    {
        string Name { get; set; }
        string ImageSource { get; set; }
        string MandalaDescription { get; set; }
        string MelodySource { get; set; }
        int TimeLength { get; set; }

        string Recommendations { get; set; }
        string ExpectedResults { get; set; }
        string Influence { get; set; }
    }
}
