﻿using Adviser.Client.Core.DeviceControl;
using Adviser.Common.Domain;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Advisor.Core.Tests
{
    
    
    /// <summary>
    ///This is a test class for DataMatrixTest and is intended
    ///to contain all DataMatrixTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DataMatrixTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        private DataMatrix<PairMeasurement> GetMatrix(int rows, int columns)
        {
            PairMeasurement defaultVal = new PairMeasurement();
            defaultVal.Add(0.0F);
            DataMatrix<PairMeasurement> matrix = new DataMatrix<PairMeasurement>(rows, columns, defaultVal);
            for (int y = 0; y < rows; ++y)
            {
                for (int x = 0; x < columns; ++x)
                {
                    matrix.AddAt(x, y, defaultVal);
                }
            }

            return matrix;
        }


        [TestMethod()]
        public void AddAtTest()
        {
            int rows = 3;
            int columns = 3;
            PairMeasurement defaultVal = new PairMeasurement();
            defaultVal.Add(1.0F);
            DataMatrix<PairMeasurement> target = new DataMatrix<PairMeasurement>(rows, columns, defaultVal);
            int x = 2;
            int y = 0;
            PairMeasurement addedValue = new PairMeasurement();
            addedValue.Add(100.0F);
            target.AddAt(x, y, addedValue);
            Assert.AreEqual(addedValue.Maximum, target[x, y].Maximum);
        }


        [TestMethod()]
        public void CalculateStatisticsTest()
        {
            int rows = 3;
            int columns = 3;
            DataMatrix<PairMeasurement> matrix = GetMatrix(rows, columns);
            Assert.AreEqual(0.0F, matrix.Min);
            Assert.AreEqual(0.0F, matrix.Max);
            Assert.AreEqual(0.0F, matrix.Mean);
            matrix.CalculateStatistics();
            int idx = 0;
            for (int y = 0; y < rows; ++y)
            {
                for (int x = 0; x < columns; ++x)
                {
                    PairMeasurement d = new PairMeasurement();
                    d.Add(idx++);
                    matrix.AddAt(x, y, d);
                }
            }
            matrix.CalculateStatistics();
            Assert.AreEqual(0.0F, matrix.Min);
            Assert.AreEqual(8.0F, matrix.Max);
            //Assert.AreEqual(0.0F, matrix.Mean);

        }

        /// <summary>
        ///A test for ToString
        ///</summary>
        //public void ToStringTestHelper<T>()
        //{
        //    int rows = 3;
        //    int columns = 3;
        //    string defaultValue = " ";
        //    DataMatrix<string> target = new DataMatrix<string>(rows, columns, defaultValue);
        //    target.AddAt(0, 0, "00");
        //    target.AddAt(1, 0, "10");
        //    target.AddAt(2, 0, "20");
        //    target.AddAt(0, 1, "01");
        //    target.AddAt(1, 1, "11");
        //    target.AddAt(2, 1, "21");
        //    target.AddAt(0, 2, "02");
        //    target.AddAt(1, 2, "12");
        //    target.AddAt(2, 2, "22");
        //    string output = target.ToString();
        //    Assert.IsTrue(output.StartsWith("00"));
        //    Trace.Write(output);
        //}

        [TestMethod()]
        public void ToStringTest()
        {
           // ToStringTestHelper<GenericParameterHelper>();
        }
    }
}
