﻿using System;
namespace Adviser.Modules.PatientDatabase
{
    public interface IVisitDetailsPrintViewModel
    {
        System.Collections.ObjectModel.ObservableCollection<string> OrganExaminations { get; }
        System.Collections.ObjectModel.ObservableCollection<string> Symptoms { get; }
        string Notes { get; }
    }
}
