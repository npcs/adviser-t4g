﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Events;
using Advisor.Modules.Catalog.Services;
using Advisor.Common.DataModel;

namespace Advisor.Modules.Catalog.Views
{
    public class PatientListPresenter : IPatientListPresenter
    {
        public event EventHandler<DataEventArgs<Patient>> PatientSelected = delegate { };

        public PatientListPresenter(IPatientListView view, IPatientService service)
        {
            View = view;
            View.PatientSelected += delegate(object sender, DataEventArgs<Patient> e)
            {
                    PatientSelected(sender, e);
            };

            //View.Model = service.RetrievePatients();
        }

        public IPatientListView View { get; private set; }

        public void SetSelectedPatient(Patient patient)
        {
            
        }

    }
}
