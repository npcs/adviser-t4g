﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.ServiceModel;

using Adviser.Server.Services;

namespace Adviser.Services.Host
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private ServiceHost _host;

        public Window1()
        {
            InitializeComponent();

            this.Closing += new System.ComponentModel.CancelEventHandler(Window1_Closing);

            //Uri tcpAdrs = new Uri("net.tcp://" + txtIP.Text.ToString() + ":" + txtPort.Text.ToString() + "/AdviserHost/");
            //Uri httpAdrs = new Uri("http://" + txtIP.Text.ToString() + ":" + (int.Parse(txtPort.Text.ToString()) + 1).ToString() + "/AdviserHost/");
            //Uri[] baseAdresses = { tcpAdrs, httpAdrs };
            //_host = new ServiceHost(typeof(Adviser.Server.Services.AdviserService), baseAdresses);

        }

        void Window1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Stop();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            _host = new ServiceHost(typeof(AdviserService));
            try
            {
                _host.Open();
                this.btnStart.IsEnabled = false;
                this.btnStop.IsEnabled = true;
                //string ports = "Using ports: ";
                //foreach(var uri in _host.BaseAddresses)
                //{
                //    ports += uri.Port + ";";
                //}
                //ports = ports.Remove(ports.Length - 1, 1);
                //this.txtIP.Text = ports;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());

                this.btnStart.IsEnabled = true;
                this.btnStop.IsEnabled = false;
            }
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            Stop();
            this.btnStart.IsEnabled = true;
            this.btnStop.IsEnabled = false;
        }

        private void Stop()
        {
            if (_host.State == CommunicationState.Opened)
            {
                _host.Close();
            }
        }
    }
}
