﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Microsoft.Practices.Composite.Wpf;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Wpf.Commands;

using Advisor.Modules.Interpretation.Views;
using Adviser.Client.Core.Presentation;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;

namespace Advisor.Modules.Interpretation
{
    public interface IHumanBodyViewModel
    {
        bool IsMale { set; get; }
        ObservableCollection<OrganViewModel> OrganViewList { get; }
        ObservableCollection<ConditionSummaryViewModel> ConditionSummary { get; }
        IResultsDetailViewModel ResultsDetail { get; }
    }
}
