﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Advisor.Core;
using Advisor.Common.DataModel;
using Advisor.Presentation.Models;

namespace Advisor.Presentation.Controls
{
    /// <summary>
    /// Interaction logic for DataMatrixView.xaml
    /// </summary>
    public partial class DataMatrixView : UserControl
    {
        private ObservableCollection<DataCellViewModel> _dataCells;

        public DataMatrixView()
        {
            InitializeComponent();
        }

        public void UpdateData(DataMatrix<PairMeasurement> matrix)
        {
            //- build grid based on matrix dimensions
            if (gridMatrix.ColumnDefinitions.Count == 0)
            {
                for (int x = 0; x <= matrix.ColumnCount; ++x)
                {
                    ColumnDefinition column = new ColumnDefinition();
                    gridMatrix.ColumnDefinitions.Add(column);
                    if (x != 0)
                    {
                        Label label = new Label();
                        label.Content = x.ToString();
                        label.HorizontalAlignment = HorizontalAlignment.Center;
                        label.VerticalAlignment = VerticalAlignment.Center;
                        Grid.SetColumn(label, x);
                        Grid.SetRow(label, 0);
                        gridMatrix.Children.Add(label);
                    }
                }
            }
            if (gridMatrix.RowDefinitions.Count == 0)
            {
                for (int y = 0; y <= matrix.RowCount; ++y)
                {
                    RowDefinition row = new RowDefinition();
                    gridMatrix.RowDefinitions.Add(row);
                    if (y != 0)
                    {
                        Label label = new Label();
                        label.Content = y.ToString();
                        label.HorizontalAlignment = HorizontalAlignment.Center;
                        label.VerticalAlignment = VerticalAlignment.Center;
                        Grid.SetColumn(label, 0);
                        Grid.SetRow(label, y);
                        gridMatrix.Children.Add(label);
                    }
                }
            }

            float sum = 0;
            int count = 0;
            for (int x = 0; x < matrix.ColumnCount; ++x)
            {
                for (int y = 0; y < matrix.RowCount; ++y)
                {
                    PairMeasurement pair = matrix[x, y];
                    string cellName = "cell_" + x.ToString() + y.ToString();
                    DataCellView cellView = gridMatrix.FindName(cellName) as DataCellView;
                    if (cellView == null)
                    {
                        cellView = new DataCellView();
                        cellView.Name = cellName;
                        Grid.SetColumn(cellView, x + 1);
                        Grid.SetRow(cellView, y + 1);
                        gridMatrix.Children.Add(cellView);
                    }

                    if (pair != null)
                    {
                        int val = (int)(pair.Maximum * 10000);
                        cellView.Value = val.ToString();
                        sum += val;
                        ++count;
                    }

                    //DataCellViewModel cellViewModel = _dataCells.F
                }
            }

            lblAverage.Content = "Mean: " + (sum / count).ToString();
        }

        //public int CalculateAverage(DataMatrix<PairMeasurement> matrix)
        //{
        //    int average = 0;
        //    foreach (List<PairMeasurement> column in matrix.Columns)
        //    {
        //        average += (int)column.Average(new Func<T, int>(AverageTransform));
        //    }

        //    average = average / data.Count;

        //    return average;
        //}
    }
}
