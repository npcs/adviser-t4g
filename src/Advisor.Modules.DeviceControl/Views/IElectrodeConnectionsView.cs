﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Modules.DeviceControl.Views
{
    public interface IElectrodeConnectionsView
    {
        IElectrodeConnectionsViewModel Model {get; set;}
    }
}
