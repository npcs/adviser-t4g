﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Objects;

using Adviser.Server.DataModel;
using EntityModel = Adviser.Server.DataModel;
using DataContracts = Adviser.Server.Services.Contracts;

namespace Adviser.Server.Services
{
    /// <summary>
    /// Maps entity objects to/from data contract objects.
    /// </summary>
    public class DataContractMapper
    {
        private EntityModel::AdviserDBEntities _dbContext;

        public DataContractMapper()
        {
            _dbContext = new EntityModel::AdviserDBEntities();
        }

        public DataContractMapper(EntityModel::AdviserDBEntities dbContext)
        {
            _dbContext = dbContext;
        }

        public EntityModel::Practitioner MapToEntity(DataContracts::Practitioner inPractitioner)
        {
            EntityModel::Practitioner outPractitioner = new EntityModel::Practitioner();
            outPractitioner.ID = inPractitioner.ID;
            outPractitioner.BusinessName = inPractitioner.BusinessName;
            outPractitioner.City = inPractitioner.City;
            outPractitioner.Country = inPractitioner.Country;
            outPractitioner.Email = inPractitioner.Email;
            outPractitioner.Fax = inPractitioner.Fax;
            outPractitioner.FirstName = inPractitioner.FirstName;
            outPractitioner.LastName = inPractitioner.LastName;
            outPractitioner.LastUpdDate = inPractitioner.LastUpdDate;
            outPractitioner.MidName = inPractitioner.MidName;
            outPractitioner.Occupation = inPractitioner.Occupation;
            outPractitioner.Password = inPractitioner.Password;
            outPractitioner.PostalCode = inPractitioner.PostalCode;
            outPractitioner.PrimaryPhone = inPractitioner.PrimaryPhone;
            outPractitioner.SecondaryPhone = inPractitioner.SecondaryPhone;
            outPractitioner.StateProvince = inPractitioner.StateProvince;
            outPractitioner.StreetAddress = inPractitioner.StreetAddress;
            outPractitioner.TypeOfPractice = inPractitioner.TypeOfPractice;

            return outPractitioner;
        }

        public DataContracts::Patient MapToDataContract(EntityModel::Patient inPatient)
        {
            DataContracts::Patient outPatient = new DataContracts::Patient();

            outPatient.City = inPatient.City;
            outPatient.Country = inPatient.Country;
            outPatient.Deleted = inPatient.Deleted;
            outPatient.DOB = inPatient.DOB;
            outPatient.Email = inPatient.Email;
            outPatient.FirstName = inPatient.FirstName;
            outPatient.Height = inPatient.Height;
            outPatient.ID = inPatient.ID;
            outPatient.LastName = inPatient.LastName;
            outPatient.LastUpdDate = inPatient.LastUpdDate;
            outPatient.Lifestyle = inPatient.Lifestyle;
            outPatient.MajorComplain = inPatient.MajorComplain;
            outPatient.MajorIllnessId = inPatient.MajorIllnessId;
            outPatient.MidName = inPatient.MidName;
            outPatient.Occupation = inPatient.Occupation;
            outPatient.PostalCode = inPatient.PostalCode;
            outPatient.PrimaryPhone = inPatient.PrimaryPhone;
            outPatient.SecondaryPhone = inPatient.SecondaryPhone;
            outPatient.Sex = inPatient.Sex;
            outPatient.StreetAddress = inPatient.StreetAddress;
            outPatient.Weight = inPatient.Weight;

            outPatient.Visits = new DataContracts::Visit[inPatient.Visits.Count];
            int idx = 0;
            foreach (EntityModel::Visit inVisit in inPatient.Visits)
            {
                DataContracts::Visit outVisit = MapToDataContract(inVisit);
                outPatient.Visits[idx] = outVisit;
                ++idx;
            }

            return outPatient;
        }

        public DataContracts::Visit MapToDataContract(EntityModel::Visit inVisit)
        {
            DataContracts::Visit outVisit = new DataContracts::Visit();

            outVisit.ID = inVisit.ID;
            outVisit.MeasurementData = null;//inVisit.MeasurementData;
            outVisit.ExamResult = inVisit.ExamResult;
            outVisit.LastUpdDate = inVisit.LastUpdDate;
            outVisit.Notes = inVisit.Notes;
            outVisit.Date = inVisit.Date;

            int idx = 0;
            outVisit.OrganExaminations = new DataContracts::OrganExamination[inVisit.OrganExaminations.Count];
            foreach (EntityModel::OrganExamination inOrganExam in inVisit.OrganExaminations)
            {
                DataContracts::OrganExamination outOrganExam = MapToDataContract(inOrganExam);
                outVisit.OrganExaminations[idx] = outOrganExam;
                ++idx;
            }

            idx = 0;
            outVisit.Symptoms = new DataContracts::Symptom[inVisit.Symptoms.Count];
            foreach (EntityModel::Symptom inSymptom in inVisit.Symptoms)
            {
                DataContracts::Symptom outSymptom = MapToDataContract(inSymptom);
                outVisit.Symptoms[idx] = outSymptom;
                ++idx;
            }

            idx = 0;
            outVisit.Prescriptions = new DataContracts::Prescription[inVisit.Prescriptions.Count];
            foreach (EntityModel::Prescription inPrescription in inVisit.Prescriptions)
            {
                DataContracts::Prescription outPrescription = MapToDataContract(inPrescription);
                outVisit.Prescriptions[idx] = outPrescription;
                ++idx;
            }

            return outVisit;
        }

        public DataContracts::Practitioner MapToDataContract(EntityModel::Practitioner inPractitioner)
        {
            DataContracts::Practitioner outPractitioner = new DataContracts::Practitioner()
            {
                BusinessName = inPractitioner.BusinessName,
                City = inPractitioner.City,
                Country = inPractitioner.Country,
                Email = inPractitioner.Email,
                Fax = inPractitioner.Fax,
                FirstName = inPractitioner.FirstName,
                ID = inPractitioner.ID,
                LastName = inPractitioner.LastName,
                LastUpdDate = inPractitioner.LastUpdDate,
                MidName = inPractitioner.MidName,
                Occupation = inPractitioner.Occupation,
                Password = inPractitioner.Password,
                PostalCode = inPractitioner.PostalCode,
                PrimaryPhone = inPractitioner.PrimaryPhone,
                SecondaryPhone = inPractitioner.SecondaryPhone,
                StateProvince = inPractitioner.StateProvince,
                StreetAddress = inPractitioner.StreetAddress,
                TypeOfPractice = inPractitioner.TypeOfPractice
            };

            return outPractitioner;
        }

        public DataContracts::Prescription MapToDataContract(EntityModel::Prescription inPrescription)
        {
            DataContracts::Prescription outPrescription = new DataContracts::Prescription();
            outPrescription.Directions = inPrescription.Directions;
            outPrescription.ID = inPrescription.ID;
            outPrescription.MedicineID = inPrescription.MedicineID;
            outPrescription.MedicineName = inPrescription.MedicineName;

            return outPrescription;
        }

        public DataContracts::OrganExamination MapToDataContract(EntityModel::OrganExamination organExamIn)
        {
            DataContracts::OrganExamination organExamOut = new DataContracts::OrganExamination();
            organExamOut.ChronicDisease = organExamIn.ChronicDisease;
            organExamOut.ID = organExamIn.ID;
            organExamOut.Implant = organExamIn.Implant;
            organExamOut.Removed = organExamIn.Removed;
            organExamOut.Surgery = organExamIn.Surgery;
            organExamOut.Value = organExamIn.Value;
            organExamOut.OrganID = organExamIn.Organ.ID;

            return organExamOut;
        }

        public DataContracts::Symptom MapToDataContract(EntityModel::Symptom symptomIn)
        {
            DataContracts::Symptom symptomOut = new DataContracts::Symptom();
            symptomOut.ID = symptomIn.ID;
            symptomOut.Name = symptomIn.Name;
            symptomOut.OrderKey = symptomIn.OrderKey;
            symptomOut.Description = symptomIn.Description;

            return symptomOut;
        }

        public EntityModel::Patient MapToEntity(DataContracts::Patient inPatient)
        {
            EntityModel::Patient outPatient = new EntityModel::Patient();

            outPatient.City = inPatient.City;
            outPatient.Country = inPatient.Country;
            outPatient.Deleted = inPatient.Deleted;
            outPatient.DOB = inPatient.DOB;
            outPatient.Email = inPatient.Email;
            outPatient.FirstName = inPatient.FirstName;
            outPatient.Height = inPatient.Height;
            outPatient.ID = inPatient.ID;
            outPatient.LastName = inPatient.LastName;
            outPatient.LastUpdDate = inPatient.LastUpdDate;
            outPatient.Lifestyle = inPatient.Lifestyle;
            outPatient.MajorComplain = inPatient.MajorComplain;
            outPatient.MajorIllnessId = inPatient.MajorIllnessId;
            outPatient.MidName = inPatient.MidName;
            outPatient.Occupation = inPatient.Occupation;
            outPatient.PostalCode = inPatient.PostalCode;
            outPatient.PrimaryPhone = inPatient.PrimaryPhone;
            outPatient.SecondaryPhone = inPatient.SecondaryPhone;
            outPatient.Sex = inPatient.Sex;
            outPatient.StreetAddress = inPatient.StreetAddress;
            outPatient.StateProvince = inPatient.StateProvince;
            outPatient.Weight = inPatient.Weight;
            outPatient.Deleted = inPatient.Deleted;

            foreach (DataContracts::Visit inVisit in inPatient.Visits)
            {
                EntityModel::Visit outVisit = MapToEntity(inVisit);
                outPatient.Visits.Add(outVisit);
            }

            return outPatient;
        }

        public EntityModel::Visit MapToEntity(DataContracts::Visit inVisit)
        {
            EntityModel::Visit outVisit = EntityModel::Visit.CreateVisit(inVisit.ID, inVisit.Date, inVisit.LastUpdDate, false);

            outVisit.MeasurementData = inVisit.MeasurementData;
            outVisit.ExamResult = inVisit.ExamResult;
            outVisit.Notes = inVisit.Notes;
            outVisit.Deleted = inVisit.Deleted;

            foreach (DataContracts::OrganExamination inOrganExam in inVisit.OrganExaminations)
            {
                EntityModel::OrganExamination outOrganExam = MapToEntity(inOrganExam);
                outVisit.OrganExaminations.Add(outOrganExam);
            }

            foreach (DataContracts::Symptom inSymptom in inVisit.Symptoms)
            {
                EntityModel::Symptom outSymptom = MapToEntity(inSymptom);
                outVisit.Symptoms.Add(outSymptom);
            }

            foreach (DataContracts::Prescription inPrescription in inVisit.Prescriptions)
            {
                EntityModel::Prescription outPrescription = MapToEntity(inPrescription);
                outVisit.Prescriptions.Add(outPrescription);
            }

            return outVisit;
        }

        public EntityModel::Prescription MapToEntity(DataContracts::Prescription inPrescription)
        {
            EntityModel::Prescription outPrescription = new EntityModel::Prescription();
            outPrescription.Directions = inPrescription.Directions;
            outPrescription.ID = inPrescription.ID;
            outPrescription.MedicineID = inPrescription.MedicineID;
            outPrescription.MedicineName = inPrescription.MedicineName;

            return outPrescription;
        }

        public EntityModel::OrganExamination MapToEntity(DataContracts::OrganExamination organExamIn)
        {
            EntityModel::OrganExamination organExamOut =
                EntityModel::OrganExamination.CreateOrganExamination(organExamIn.ID,
                organExamIn.Value,
                organExamIn.Surgery,
                organExamIn.ChronicDisease,
                organExamIn.Removed,
                organExamIn.Implant);

            //organExamOut.ChronicDisease = organExamIn.ChronicDisease;
            //organExamOut.ID = organExamIn.ID;
            //organExamOut.Implant = organExamIn.Implant;
            //organExamOut.Removed = organExamIn.Removed;
            //organExamOut.Surgery = organExamIn.Surgery;
            //organExamOut.Value = organExamIn.Value;

            organExamOut.Organ = _dbContext.LoadByKey<Organ>("ID", organExamIn.OrganID);

            return organExamOut;
        }

        public EntityModel::Symptom MapToEntity(DataContracts::Symptom symptomIn)
        {
            EntityModel::Symptom symptomOut = _dbContext.LoadByKey<EntityModel::Symptom>("ID", symptomIn.ID);//new EntityModel::Symptom();

            symptomOut.Name = symptomIn.Name;
            symptomOut.OrderKey = symptomIn.OrderKey;
            symptomOut.Description = symptomIn.Description;

            return symptomOut;
        }


        public EntityModel::Patient FlatCopyToEntity(DataContracts::Patient inPatient, EntityModel::Patient outPatient)
        {
            //outPatient.ID = inPatient.ID;
            outPatient.City = inPatient.City;
            outPatient.Country = inPatient.Country;
            outPatient.Deleted = inPatient.Deleted;
            outPatient.DOB = inPatient.DOB;
            outPatient.Email = inPatient.Email;
            outPatient.FirstName = inPatient.FirstName;
            outPatient.Height = inPatient.Height;
            outPatient.LastName = inPatient.LastName;
            outPatient.LastUpdDate = inPatient.LastUpdDate;
            outPatient.Lifestyle = inPatient.Lifestyle;
            outPatient.MajorComplain = inPatient.MajorComplain;
            outPatient.MajorIllnessId = inPatient.MajorIllnessId;
            outPatient.MidName = inPatient.MidName;
            outPatient.Occupation = inPatient.Occupation;
            outPatient.PostalCode = inPatient.PostalCode;
            outPatient.PrimaryPhone = inPatient.PrimaryPhone;
            outPatient.SecondaryPhone = inPatient.SecondaryPhone;
            outPatient.Sex = inPatient.Sex;
            outPatient.StateProvince = inPatient.StateProvince;
            outPatient.StreetAddress = inPatient.StreetAddress;
            outPatient.Weight = inPatient.Weight;
            outPatient.Deleted = inPatient.Deleted;

            return outPatient;
        }

        public EntityModel::Visit FlatCopyToEntity(DataContracts::Visit inVisit, EntityModel::Visit outVisit)
        {
            //outVisit.ID = inVisit.ID;
            outVisit.MeasurementData = inVisit.MeasurementData;
            outVisit.ExamResult = inVisit.ExamResult;
            outVisit.LastUpdDate = inVisit.LastUpdDate;
            outVisit.Notes = inVisit.Notes;
            outVisit.Date = inVisit.Date;
            outVisit.Deleted = inVisit.Deleted;

            return outVisit;
        }

        public EntityModel::Prescription FlatCopyToEntity(DataContracts::Prescription inPrescription, EntityModel::Prescription outPrescription)
        {
            outPrescription.Directions = inPrescription.Directions;
            outPrescription.MedicineID = inPrescription.MedicineID;
            outPrescription.MedicineName = inPrescription.MedicineName;

            return outPrescription;
        }

        public DataContracts::Patient FlatCopyToDataContract(EntityModel::Patient inPatient, DataContracts::Patient outPatient)
        {
            outPatient.City = inPatient.City;
            outPatient.Country = inPatient.Country;
            outPatient.Deleted = inPatient.Deleted;
            outPatient.DOB = inPatient.DOB;
            outPatient.Email = inPatient.Email;
            outPatient.FirstName = inPatient.FirstName;
            outPatient.Height = inPatient.Height;
            outPatient.ID = inPatient.ID;
            outPatient.LastName = inPatient.LastName;
            outPatient.LastUpdDate = inPatient.LastUpdDate;
            outPatient.Lifestyle = inPatient.Lifestyle;
            outPatient.MajorComplain = inPatient.MajorComplain;
            outPatient.MajorIllnessId = inPatient.MajorIllnessId;
            outPatient.MidName = inPatient.MidName;
            outPatient.Occupation = inPatient.Occupation;
            outPatient.PostalCode = inPatient.PostalCode;
            outPatient.PrimaryPhone = inPatient.PrimaryPhone;
            outPatient.SecondaryPhone = inPatient.SecondaryPhone;
            outPatient.Sex = inPatient.Sex;
            outPatient.StateProvince = inPatient.StateProvince;
            outPatient.StreetAddress = inPatient.StreetAddress;
            outPatient.Weight = inPatient.Weight;

            return outPatient;
        }

        public EntityModel::Practitioner FlatCopyToEntity(DataContracts::Practitioner inPractitioner, EntityModel::Practitioner outPractitioner)
        {
            //outPractitioner.ID = inPractitioner.ID;
            outPractitioner.BusinessName = inPractitioner.BusinessName;
            outPractitioner.City = inPractitioner.City;
            outPractitioner.StateProvince = inPractitioner.StateProvince;
            outPractitioner.Country = inPractitioner.Country;
            outPractitioner.Email = inPractitioner.Email;
            outPractitioner.Fax = inPractitioner.Fax;
            outPractitioner.FirstName = inPractitioner.FirstName;
            outPractitioner.LastName = inPractitioner.LastName;
            outPractitioner.LastUpdDate = DateTime.Now.ToUniversalTime();
            outPractitioner.MidName = inPractitioner.MidName;
            outPractitioner.Occupation = inPractitioner.Occupation;
            outPractitioner.Password = inPractitioner.Password;
            outPractitioner.PostalCode = inPractitioner.PostalCode;
            outPractitioner.PrimaryPhone = inPractitioner.PrimaryPhone;
            outPractitioner.SecondaryPhone = inPractitioner.SecondaryPhone;
            outPractitioner.StreetAddress = inPractitioner.StreetAddress;
            outPractitioner.TypeOfPractice = inPractitioner.TypeOfPractice;
            outPractitioner.LastUpdDate = inPractitioner.LastUpdDate;
            //outPractitioner.Status = (AccountStatus)inPractitioner.StatusID;

            return outPractitioner;
        }
    }
}
