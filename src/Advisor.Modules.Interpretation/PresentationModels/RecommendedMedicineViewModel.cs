﻿using System;
using System.Collections.Generic;
using System.Linq;

using Adviser.Client.DataModel;

namespace Advisor.Modules.Interpretation
{
    /// <summary>
    /// Presentation model for Adviser recommended medicine item.
    /// </summary>
    public class RecommendedMedicineViewModel
    {
        public RecommendedMedicineViewModel(Medicine medicine)
        {
            ID = medicine.ID;
            Name = medicine.Name;
            Direction = medicine.Direction;
            Description = medicine.Description;
        }

        public int ID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public int Relevance
        {
            get;
            set;
        }

        public string Direction
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }
    }
}
