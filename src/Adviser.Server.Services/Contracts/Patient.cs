﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Adviser.Server.Services.Contracts
{
    [DataContract]
    public class Patient
    {
        [DataMember]
        public Guid ID;

        [DataMember]
        public DateTime LastUpdDate;

        [DataMember]
        public string FirstName;

        [DataMember]
        public string LastName;

        [DataMember]
        public string MidName;

        [DataMember]
        public string StreetAddress;

        [DataMember]
        public string City;

        [DataMember]
        public string StateProvince;

        [DataMember]
        public string PostalCode;

        [DataMember]
        public string Country;

        [DataMember]
        public string Occupation;

        [DataMember]
        public string PrimaryPhone;

        [DataMember]
        public string SecondaryPhone;

        [DataMember]
        public string Email;

        [DataMember]
        public DateTime? DOB;
        
        [DataMember]
        public string Sex;

        [DataMember]
        public int? Height;

        [DataMember]
        public int? Weight;

        [DataMember]
        public string Lifestyle;

        [DataMember]
        public string MajorComplain;

        [DataMember]
        public int? MajorIllnessId;

        [DataMember]
        public bool Deleted;

        [DataMember]
        public Visit[] Visits;

    }
}
