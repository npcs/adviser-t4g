﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Common.Logging
{
    /// <summary>
    /// Concrete subclasses of this abstract class are used by loggers
    /// to "filter" each item and decide whether or not to actually log it.
    /// </summary>
    /// <remarks>
    /// Subclasses should override the canPass(LogEntry) method.
    /// </remarks>
    public abstract class LogEntryFilter : ILogEntryFilter
    {
        /// <summary>
        /// The lowest severity of a LogEntry that will pass this filter.
        /// Debug is the default.
        /// </summary>
        private LogSeverity _severityThreshold = LogSeverity.Debug;
        /// <summary>
        /// Gets and sets the severity threshold.
        /// </summary>
        public LogSeverity SeverityThreshold
        {
            get { return _severityThreshold; }
            set { _severityThreshold = value; }
        }

        /// <summary>
        /// Concrete subclasses override this method and determine if logEntry
        /// should pass through the filter or not.
        /// </summary>
        /// <param name="logEntry">The LogEntry being tested.</param>
        /// <returns>true if logEntry passes the filter, false otherwise.</returns>
        protected abstract bool CanPass(ILogEntry logEntry);

        /// <summary>
        /// Test to determine if logEntry should be logged.
        /// </summary>
        /// <param name="logEntry">The LogEntry being tested.</param>
        /// <returns>true if logEntry should be logged, false otherwise.</returns>
        protected internal bool ShouldLog(ILogEntry logEntry)
        {
            return (logEntry.Severity >= SeverityThreshold) && CanPass(logEntry);
        }
        /// <summary>
        /// LogEntryFilter constructor.
        /// </summary>
        protected LogEntryFilter()
            : base()
        {
        }
    }
}
