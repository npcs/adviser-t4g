﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Adviser.Client.DataModel;
using Adviser.Client.Core.Presentation;

namespace Advisor.Modules.Interpretation
{
    public class MandalaViewModel : ViewModelBase, IMandalaViewModel
    {
        public MandalaViewModel()
        {

        }

        public MandalaViewModel(string name, string imageSource, int timeLength, string mandalaDescription, string melodySource, 
            string recommendations, string expectedResults, string influence)
        {
            Name = name;
            ImageSource = imageSource;
            MandalaDescription = mandalaDescription;
            MelodySource = melodySource;

            TimeLength = timeLength;
            
            Recommendations = recommendations;
            ExpectedResults = expectedResults;
            Influence = influence;
        }

        public string Name { get; set; }
        public string ImageSource { get; set; }
        public string MandalaDescription { get; set; }
        public string MelodySource { get; set; }
        
        public int TimeLength { get; set; } //in minutes

        public string Recommendations { get; set; }
        public string ExpectedResults { get; set; }
        public string Influence { get; set; }
    }
}
