﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;

using System.Diagnostics;

namespace AdvisorClient
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Shell : Window, IShellView
    {
        public IInputElement DummyField
        {
            get { return this.DummyButton; }
        }

        public event EventHandler ClosingApplication = delegate { };
        public event EventHandler ShellViewLoaded = delegate { };
        public event EventHandler USBChanged = delegate { };
        public event EventHandler MyAccountClicked = delegate { };
        public event EventHandler HelpClicked = delegate { };        

        public Shell()
        {
            InitializeComponent();
        }

        public void CloseApplication()
        {
            this.Close();
        }

        public void ShowView()
        {
            this.Show();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ClosingApplication(this, null);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ShellViewLoaded(this, null);
        }

        #region USB detection code

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            WindowInteropHelper MainFormWinInteropHelper = new WindowInteropHelper(this);
            HwndSource.FromHwnd(MainFormWinInteropHelper.Handle).AddHook(HwndHandler);
        }

        public IntPtr HwndHandler(IntPtr hwnd, int msg, IntPtr wParam, IntPtr IParam, ref bool handled)
        {
            ProcessWinMessage(msg, wParam, IParam);
            handled = false;
            return IntPtr.Zero;
        }

        public void ProcessWinMessage(int msg, IntPtr wParam, IntPtr IParam)
        {
            int WM_DEVICECHANGE = 0x219;
            if (msg == WM_DEVICECHANGE)
            {
                USBChanged(this, null);
            }
        }

        #endregion

        private void MyAccountButton_Click(object sender, RoutedEventArgs e)
        {
            MyAccountClicked(this, null);
        }

        private void HelpButton_Click(object sender, RoutedEventArgs e)
        {
            HelpClicked(this, null);
        }



        //public ItemsControl Content
        //{
        //    get { return this.ContentRegion; }
        //    set { this.ContentRegion.Items.Add(value); }
        //}
    }
}
