﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Events;
using Advisor.Common.DataModel;
using Advisor.Modules.Catalog;
using Advisor.Modules.Catalog.Views;
using Advisor.Modules.Catalog.PresentationModels;

namespace Advisor.Modules.Catalog.Views
{
    public class PatientDetailsPresenter : IPatientDetailsPresenter
    {
        public event EventHandler<DataEventArgs<Patient>> BackClicked = delegate { };

        public PatientDetailsPresenter(PatientDetailsView view)
        {
            View = view;
            View.BackClicked += delegate(object sender, DataEventArgs<Patient> e)
            {
                BackClicked(sender, e);
            };
        }

        #region IPatientDetailsPresenter Members

        public IPatientDetailsView View
        {
            get;
            set;
        }

        public void SetSelectedPatient(Patient patient)
        {
            PatientDetailsPresentationModel model = new PatientDetailsPresentationModel(patient);
            View.Model = model;
        }

        

        #endregion
    }
}
