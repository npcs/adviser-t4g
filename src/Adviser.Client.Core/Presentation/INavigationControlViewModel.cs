﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Client.Core.Presentation
{
    public interface INavigationControlViewModel
    {
        string Label { get; set; }
        string ActionName { get; set; }
        bool IsSelected { get; set; }
        bool IsEnabled { get; set; }
    }
}
