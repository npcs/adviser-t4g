﻿using System;
namespace Adviser.Common.Logging
{
    public interface ILogEntry
    {
        string Application { get; set; }
        string Category { get; set; }
        string LogItemType { get; set; }
        string ErrorMessage { get; set; }
        object Details { get; set; }
        LogSeverity Severity { get; set; }
        string SeverityString { get; }
        DateTime Time { get; set; }
        string ToString();
    }
}
