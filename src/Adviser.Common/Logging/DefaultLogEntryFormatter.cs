﻿using System;

namespace Adviser.Common.Logging
{
    /// <summary>
    /// Default log entry formatter.
    /// </summary>
    public class DefaultLogEntryFormatter : LogEntryFormatter
    {
        protected internal override String AsString(ILogEntry logEntry)
        {
            String appString = "";
            if (logEntry.Application != null)
                appString = "[" + logEntry.Application + "] -- ";
            if (logEntry.Category != null)
                try
                {
                    appString = appString + "{" + logEntry.Category + "} --";
                }
                catch
                {
                }
            return appString + "<" + logEntry.SeverityString + "> -- "
                + DateString(logEntry) + "\n" + logEntry.LogItemType;
        }

        public DefaultLogEntryFormatter()
            : base()
        {
        }
    }
}
