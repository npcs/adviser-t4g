﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Events;
using Advisor.Modules.Catalog.Controllers;
using Advisor.Common.DataModel;

namespace Advisor.Modules.Catalog.Views
{

    public class CatalogPresenter: ICatalogPresenter
    {
        private IUnityContainer container;
        private IPatientListPresenter patientListPresenter;
        private ICatalogController catalogController;
        private Dictionary<string, object> _viewsCache;

        public CatalogPresenter(
            ICatalogView catalogView,
            IUnityContainer container
            //IPatientListPresenter patientListPresenter,
            //ICatalogController catalogController
            )
        {
            this.View = catalogView;
            this.container = container;
            //this.catalogController = catalogController;
            _viewsCache = new Dictionary<string, object>();

            //this.patientListPresenter = patientListPresenter;
            //this.patientListPresenter.PatientSelected += new EventHandler<DataEventArgs<Patient>>(this.OnPatientSelected);
            //View.SetDefaultView(patientListPresenter.View);

            //SetPatientListView(null);
            SetPatientView();
        }

        public ICatalogView View { get; set; }

        private void OnPatientSelected(object sender, DataEventArgs<Patient> e)
        {
            //catalogController.OnPatientSelected(e.Value);
            SetPatientListView(e.Value);
        }

        public void SetPatientView()
        {

        }

        public void SetPatientListView(Patient patient)
        {
            if (this.View.CurrentContent is PatientListView)
                return;

            IPatientListPresenter patientListPresenter = null;
            if (_viewsCache.ContainsKey("PatientList"))
            {
                patientListPresenter = (IPatientListPresenter)_viewsCache["PatientList"];
            }
            else
            {
                patientListPresenter = container.Resolve<IPatientListPresenter>();
                patientListPresenter.PatientSelected += new EventHandler<DataEventArgs<Patient>>(patientListPresenter_PatientSelected);
                _viewsCache.Add("PatientList", patientListPresenter);
            }
            if (patient != null)
                patientListPresenter.SetSelectedPatient(patient);

            View.CurrentContent = patientListPresenter.View;
        }

        void patientListPresenter_PatientSelected(object sender, DataEventArgs<Patient> e)
        {
            SetPatientDetailsView(e.Value);
        }

        public void SetPatientDetailsView(Patient patient)
        {
            if (this.View.CurrentContent is PatientDetailsView)
                return;

            IPatientDetailsPresenter patientDetailsPresenter = null;
            if (_viewsCache.ContainsKey("PatientDetails"))
            {
                patientDetailsPresenter = (IPatientDetailsPresenter)_viewsCache["PatientDetails"];
            }
            else
            {
                patientDetailsPresenter = container.Resolve<IPatientDetailsPresenter>();
                patientDetailsPresenter.BackClicked += new EventHandler<DataEventArgs<Patient>>(patientDetailsPresenter_BackClicked);
                _viewsCache.Add("PatientDetails", patientDetailsPresenter);
            }

            View.CurrentContent = patientDetailsPresenter.View;

        }

        void patientDetailsPresenter_BackClicked(object sender, DataEventArgs<Patient> e)
        {
            SetPatientListView(e.Value);
        }

    }
}
