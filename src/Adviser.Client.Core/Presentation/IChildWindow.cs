﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Adviser.Client.Core.Presentation
{
    public interface IChildWindow
    {
        string WindowTitle { set; }
        SizeToContent ChildWindowSizeToContent { set; }
        bool CanCloseOnEsc { set; }
        bool IsFullScreen { get; set; }
        void ShowAsDialog();
        void SetContent(object content);
        void CloseWindow();
        WindowStyle ChildWindowStyle { set; }
        double ChildWindowWidth { set; }
        double ChildWindowHeight { set; }

    }
}
