﻿using System;

namespace Adviser.Common.Domain
{
    [Serializable()]
    public struct PairConnection
    {
        public byte Front;
        public byte Back;
    }
}
