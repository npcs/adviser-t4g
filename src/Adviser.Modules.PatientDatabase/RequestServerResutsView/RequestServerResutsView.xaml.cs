﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Composite.Events;
using Adviser.Client.DataModel;

namespace Adviser.Modules.PatientDatabase
{
    /// <summary>
    /// Interaction logic for RequestServerResutsView.xaml
    /// </summary>
    public partial class RequestServerResutsView : UserControl, IRequestServerResutsView
    {
        public RequestServerResutsView()
        {
            InitializeComponent();
        }

        #region IRequestServerResutsView Members

        public event EventHandler ResutsRequested;

        #endregion

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnRequestServerResuts_Click(object sender, RoutedEventArgs e)
        {
            if (ResutsRequested != null)
            {
                ResutsRequested(this, null);
            }

        }
    }
}
