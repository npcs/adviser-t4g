﻿using System;
using System.Collections.Generic;
using System.Windows.Media;

using Adviser.Client.Core.Presentation;
using Adviser.Client.DataModel;

namespace Advisor.Modules.Interpretation
{
    /// <summary>
    /// Presentation model for the summary item on the condition summary pie chart.
    /// </summary>
    public class ConditionSummaryViewModel : ViewModelBase
    {
        private Condition _condition;
        private int _count = 0;
        private Brush _conditionBrush;
        private int _rate;

        public ConditionSummaryViewModel(Condition condition) 
        {
            _condition = condition;
        }

        public Condition Condition
        {
            get { return _condition; }
            set
            {
                _condition = value;
                RaisePropertyChangedEvent("Condition");
                RaisePropertyChangedEvent("Range");
            }
        }

        public int? Range
        {
            get
            {
                return (_condition.MaxRange - _condition.MinRange)*10;
            }
        }

        public int Count
        {
            get { return _count; }
            set
            {
                _count = value;
              RaisePropertyChangedEvent("Count");
            }
        }

        public int Rate
        {
            get { return _rate; }
            set
            {
                _rate = value;
                RaisePropertyChangedEvent("Rate");
            }
        }

        public Brush ConditionBrush
        {
            get { return _conditionBrush; }
            set
            {
                _conditionBrush = value;
                RaisePropertyChangedEvent("ConditionBrush");
            }
        }
    }
}
