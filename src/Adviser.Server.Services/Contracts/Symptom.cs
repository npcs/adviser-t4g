﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Adviser.Server.Services.Contracts
{
    [DataContract]
    public class Symptom
    {
        [DataMember]
        public int ID;

        [DataMember]
        public string Name;

        [DataMember]
        public string Description;

        [DataMember]
        public int OrderKey;
    }
}
