﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

using Advisor.Common;
using Adviser.Common.Domain;
using Adviser.Client.Core.Presentation;
using Advisor.Modules.Readout.PresentationModels;
using Advisor.Modules.Readout.Views;

namespace Advisor.Modules.Readout
{
    public class ReadoutModule : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public ReadoutModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            RegisterViewsAndServices();

            //IRegion region = _regionManager.Regions[RegionNames.BottomRegion];
            //IReadoutView view = _container.Resolve<IReadoutView>();
            //IDataMatrixViewModel matrixViewModel = _container.Resolve<IDataMatrixViewModel>();
            //view.ShowMatrix(matrixViewModel.View);
            //region.Add(view);

            IRegion region = _regionManager.Regions[RegionNames.ContentRegion];
            IReadoutPresenter readoutPresenter = _container.Resolve<IReadoutPresenter>();
            region.Add(readoutPresenter.View, "IReadoutView");
            region.Deactivate(readoutPresenter.View);
        }

        protected void RegisterViewsAndServices()
        {
            _container.RegisterType<IReadoutView, ReadoutView>();
            _container.RegisterType<IReadoutPresenter, ReadoutPresenter>();
            _container.RegisterType<IDataCellView, DataCellView>();
            _container.RegisterType<IDataCellViewModel, DataCellViewModel>();
            _container.RegisterType<IDataMatrixView, DataMatrixView>();
            _container.RegisterType<IDataMatrixViewModel, DataMatrixViewModel>();
            _container.RegisterType<IPairDetailsView, PairDetailsView>();
            //_container.RegisterType<IValueTransform, LinearMapValueTransform>();
        }

    }
}
