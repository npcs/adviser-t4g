﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Adviser.Common.Domain;
using Adviser.Server.Services.Contracts;

namespace Adviser.Server.Services
{
    /// <summary>
    /// This is interface for main Adviser client services.
    /// Exposed to the desktop clients.
    /// </summary>
    [ServiceContract]
    public interface IAdviserService
    {
        [OperationContract]
        [FaultContract(typeof(AdviserServiceFault))]
        AccountRegistration RegisterAccount(Practitioner practitioner, Guid deviceId);

        [OperationContract]
        [FaultContract(typeof(AdviserServiceFault))]
        AccountRegistration UpdateAccount(Practitioner practitioner);

        [OperationContract]
        [FaultContract(typeof(AdviserServiceFault))]
        Practitioner GetPractitioner(string email, string password, Guid deviceId);

        [OperationContract]
        [FaultContract(typeof(AdviserServiceFault))]
        RetrievePasswordResult RetrievePassword(Guid accountId, string email);

        /// <summary>
        /// Returns patient visit(s) results processed by server.
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="deviceId"></param>
        /// <param name="patient"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AdviserServiceFault))]
        Patient GetExamResults(Guid practitionerId, Guid deviceId, Patient patient);

        [OperationContract]
        SyncDataItem[] GetSyncData(Guid practitionerId);

        /// <summary>
        /// Returns patient data with all visits.
        /// </summary>
        /// <param name="practitionerId"></param>
        /// <param name="patientId"></param>
        /// <returns>Patient object including related visits.</returns>
        [OperationContract]
        Patient GetPatientWithAllVisits(Guid practitionerId, Guid patientId);

        /// <summary>
        /// Returns Patient objects with requested visits.
        /// </summary>
        /// <param name="practitionerId">Practitioner ID</param>
        /// <param name="patientId">ID of patient to return.</param>
        /// <param name="visitIds">IDs of visits to return.</param>
        /// <returns>Patient object and requested visits.</returns>
        [OperationContract]
        Patient GetPatient(Guid practitionerId, Guid patientId, Guid[] visitIds);

        [OperationContract]
        Visit[] GetVisits(Guid practitionerId, Guid patientId, Guid[] visitIds);

        [OperationContract]
        void CreatePatient(Guid practitionerId, Patient patient);

        [OperationContract]
        void CreateVisits(Guid practitionerId, Guid patientId, Visit[] visits);

        [OperationContract]
        bool UpdatePatients(Guid practitionerId, Patient[] patients);

        [OperationContract]
        bool UpdateVisits(Guid practitionerId, Visit[] visits);

        [OperationContract]
        bool UpdatePrescriptions(Guid practitionerId, Prescription[] prescriptions);

        [OperationContract]
        void DeleteVisits(Guid practitionerId, Guid[] visits);

        [OperationContract]
        bool ReportError(AdviserClientLogEntry error);

        //[OperationContract]
        //AccountServiceResponse AddDeviceToAccount(Practitioner practitioner, string deviceId);
    }
}
