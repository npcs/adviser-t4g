﻿using System;
using System.Collections.Generic;
using Adviser.Common.Domain;

namespace Advisor.Modules.DeviceControl.Services
{
    public interface IDeviceControlService
    {
        void ReInitialize();
        void InitializeDevice();
        void ReleaseDevice();
        DeviceInformation GetDeviceInfo();
        void StartMeasurement();
        void StopMeasurement();

        //event EventHandler<MeasurementEventArgs> MeasurementProgress;
        //event EventHandler<MeasurementEventArgs> MeasurementCompleted;
    }
}
