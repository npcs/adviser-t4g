﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Modules.Navigation
{
    public interface INavigationView
    {
        INavigator Model { get; set; }
    }
}
