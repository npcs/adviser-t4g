﻿using System;
using Microsoft.Practices.Composite.Events;
using Advisor.Modules.Catalog.PresentationModels;
using Advisor.Common.DataModel;


namespace Advisor.Modules.Catalog.Views
{
    public interface IPatientDetailsView
    {
        event EventHandler<DataEventArgs<Patient>> BackClicked;
        PatientDetailsPresentationModel Model { get; set; }
    }
}
