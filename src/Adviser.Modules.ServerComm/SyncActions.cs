﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Modules.ServerComm
{
    /// <summary>
    /// List of possible synchronization actions applied to single entity instance.
    /// </summary>
    public enum SyncActions
    {
        SendCreate = 1,
        SendUpdate = 2,
        SendDelete = 3,
        ReceiveCreate = 4,
        ReceiveUpdate = 5,
        ReceiveDelete = 6
    }
}
