﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Adviser.Server.Services.Contracts
{
    [DataContract]
    public class Practitioner
    {
        [DataMember]
        public Guid ID;

        [DataMember]
        public string FirstName;

        [DataMember]
        public string LastName;

        [DataMember]
        public string MidName;

        [DataMember]
        public string BusinessName;
        
        [DataMember]
        public string TypeOfPractice;

        [DataMember]
        public string StreetAddress;

        [DataMember]
        public string City;

        [DataMember]
        public string StateProvince;

        [DataMember]
        public string PostalCode;

        [DataMember]
        public string Country;
        
        [DataMember]
        public string Occupation;

        [DataMember]
        public string PrimaryPhone;

        [DataMember]
        public string SecondaryPhone;

        [DataMember]
        public string Fax;

        [DataMember]
        public string Email;

        [DataMember]
        public string Password;

        [DataMember]
        public Int16 StatusID;

        [DataMember]
        public DateTime LastUpdDate;

    }
}
