﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Advisor.Common.DataModel;
using System.Data.Objects.DataClasses;
using Microsoft.Practices.Composite.Events;

namespace Advisor.Modules.Catalog.Views
{
    public interface IPatientView
    {
        event EventHandler<DataEventArgs<Patient>> PatientSelected;
        event EventHandler<DataEventArgs<Patient>> PatientDeleted;
        event EventHandler PatientUpdated;
        event EventHandler PatientAdded;
        EntityCollection<Patient> PatientList { get; set; }
        Patient SelectedPatient { get; set; }

        
        event EventHandler AnamnezAdded;
        event EventHandler<DataEventArgs<Anamnesis>> AnamnesisDeleted;
        EntityCollection<Anamnesis> AnamnesisList { get; set; }
        Anamnesis SelectedAnamnesis { get; set; }
        
        event EventHandler VisitAdded;
        event EventHandler<DataEventArgs<Visit>> VisitDeleted;
        event EventHandler<DataEventArgs<Visit>> VisitSelected;
        EntityCollection<Visit> VisitList { get; set; }
        Visit SelectedVisit { get; set; }
    }
}
