﻿using System;
using Microsoft.Practices.Composite.Wpf.Events;

namespace Advisor.Common.Events
{
    public class NavigationEvent : CompositeWpfEvent<NavigationEventArgs>
    {
    }
}
