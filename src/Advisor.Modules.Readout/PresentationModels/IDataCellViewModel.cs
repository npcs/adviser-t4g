﻿using System;
using System.Collections.Generic;
using Advisor.Modules.Readout.Views;
using Adviser.Common.Domain;

namespace Advisor.Modules.Readout.PresentationModels
{
    public interface IDataCellViewModel
    {
        double Value { get; }
        PairDataSampleCollection Series { get; }
        int Column { get; }
        int Row { get; }
        IDataCellView View { get; }
        void SetData(PairMeasurement data);

    }
}
