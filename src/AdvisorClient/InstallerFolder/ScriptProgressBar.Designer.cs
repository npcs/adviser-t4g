﻿namespace AdvisorClient
{
    partial class ScriptProgressBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.totalProgressBar = new System.Windows.Forms.ProgressBar();
            this.SubscriptProgressBar = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.MessageLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // totalProgressBar
            // 
            this.totalProgressBar.Location = new System.Drawing.Point(112, 55);
            this.totalProgressBar.Name = "totalProgressBar";
            this.totalProgressBar.Size = new System.Drawing.Size(464, 19);
            this.totalProgressBar.TabIndex = 0;
            // 
            // SubscriptProgressBar
            // 
            this.SubscriptProgressBar.Location = new System.Drawing.Point(112, 91);
            this.SubscriptProgressBar.Name = "SubscriptProgressBar";
            this.SubscriptProgressBar.Size = new System.Drawing.Size(464, 19);
            this.SubscriptProgressBar.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Total Progress: ";
            // 
            // MessageLabel
            // 
            this.MessageLabel.AutoSize = true;
            this.MessageLabel.Location = new System.Drawing.Point(12, 94);
            this.MessageLabel.Name = "MessageLabel";
            this.MessageLabel.Size = new System.Drawing.Size(51, 13);
            this.MessageLabel.TabIndex = 3;
            this.MessageLabel.Text = "Subscript";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(169, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(263, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "Updating database. Please, wait...";
            // 
            // ScriptProgressBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 139);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.MessageLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SubscriptProgressBar);
            this.Controls.Add(this.totalProgressBar);
            this.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ScriptProgressBar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ScriptProgressBar";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ProgressBar totalProgressBar;
        public System.Windows.Forms.ProgressBar SubscriptProgressBar;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label MessageLabel;
        private System.Windows.Forms.Label label2;

    }
}