﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Advisor.Common.DataModel;
using System.Data.Objects.DataClasses;
using Microsoft.Practices.Composite.Events;

namespace Advisor.Modules.Catalog.Views
{
    /// <summary>
    /// Interaction logic for PatientView.xaml
    /// </summary>
    public partial class PatientView : UserControl, IPatientView
    {
        #region Events

        public event EventHandler<DataEventArgs<Patient>> PatientSelected = delegate { };
        public event EventHandler<DataEventArgs<Patient>> PatientDeleted = delegate { };
        public event EventHandler PatientUpdated = delegate { };
        public event EventHandler PatientAdded = delegate { };

        public event EventHandler AnamnezAdded = delegate { };
        public event EventHandler<DataEventArgs<Anamnesis>> AnamnesisDeleted = delegate { };

        public event EventHandler VisitAdded = delegate { };
        public event EventHandler<DataEventArgs<Visit>> VisitDeleted = delegate { };
        public event EventHandler<DataEventArgs<Visit>> VisitSelected = delegate { };


        #endregion

        #region Private

        private void ControlStateSet(Control control)
        {
            if (control.DataContext == null)
            {
                control.IsEnabled = false;
            }
            else
            {
                control.IsEnabled = true;
            }
        }

        private void Control_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ControlStateSet((Control)sender);
        }

        private void Control_Loaded(object sender, RoutedEventArgs e)
        {
            ControlStateSet((Control)sender);
        }

        #endregion

        #region Constaructor

        public PatientView()
        {
            InitializeComponent();
        }

        #endregion

        #region IPatientView implementation

        public EntityCollection<Patient> PatientList
        {
            get { return this.PatientListListView.DataContext as EntityCollection<Patient>; }
            set { this.PatientListListView.DataContext = value; }
        }

        public Patient SelectedPatient
        {
            get { return this.PatientInformationPanel.DataContext as Patient; }
            set { this.PatientInformationPanel.DataContext = value; }
        }


        public EntityCollection<Anamnesis> AnamnesisList
        {
            get { return this.AnamnezListView.DataContext as EntityCollection<Anamnesis>; }
            set { this.AnamnezListView.DataContext = value; }
        }

        public Anamnesis SelectedAnamnesis
        {
            get { return this.AnamnezDetailPanel.DataContext as Anamnesis; }
            set { this.AnamnezDetailPanel.DataContext = value; }
        }

        public Visit SelectedVisit
        {
            get { return this.VisitDetailPanel.DataContext as Visit; }
            set { this.VisitDetailPanel.DataContext = value; }
        }

        public EntityCollection<Visit> VisitList
        {
            get { return this.VisitListView.DataContext as EntityCollection<Visit>; }
            set { this.VisitListView.DataContext = value; }
        }

        #endregion

        #region View event handlers

        #region Patients

        private void PatientList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Patient selected = e.AddedItems[0] as Patient;
                if (selected != null)
                {
                    SelectedPatient = selected;
                    PatientSelected(this, new DataEventArgs<Patient>(selected));
                }
            }
            else
            {
                if (PatientListListView.Items.Count == 0)
                {
                    this.SelectedPatient = null;
                    //PatientSelected(this, new DataEventArgs<Patient>(null));
                }
            }
        }

        private void AddParientButton_Click(object sender, RoutedEventArgs e)
        {
            PatientAdded(this, EventArgs.Empty);
        }

        private void DeletePatient_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedPatient != null)
            {
                PatientDeleted(this, new DataEventArgs<Patient>(SelectedPatient));
            }
        }

        private void UpdatePatientInformationButton_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedPatient != null)
            {
                PatientUpdated(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Anamnez

        private void AnamnezListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Anamnesis selected = e.AddedItems[0] as Anamnesis;
                if (selected != null)
                {
                    this.SelectedAnamnesis = selected;
                }
            }
            else
            {
                if (AnamnezListView.Items.Count == 0)
                {
                    this.SelectedAnamnesis = null;
                }
            }
        }

        private void AddAnamnezButton_Click(object sender, RoutedEventArgs e)
        {
            AnamnezAdded(this, EventArgs.Empty);

        }

        private void UpdateAnamnezButton_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedAnamnesis != null)
            {
                PatientUpdated(this, EventArgs.Empty);
            }
        }

        private void DeleteAnamnezButton_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedAnamnesis != null)
            {
                AnamnesisDeleted(this, new DataEventArgs<Anamnesis>(SelectedAnamnesis));
            }
        }

        #endregion

        #region Visits

        private void AddVisitButton_Click(object sender, RoutedEventArgs e)
        {
            VisitAdded(this, EventArgs.Empty);
        }

        private void DeleteVisitButton_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedVisit != null)
            {
                VisitDeleted(this, new DataEventArgs<Visit>(SelectedVisit));
            }
        }

        private void VisitListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Visit selected = e.AddedItems[0] as Visit;
                if (selected != null)
                {
                    this.SelectedVisit = selected;
                    VisitSelected(sender, new DataEventArgs<Visit>(selected));
                }
            }
            else
            {
                if (VisitListView.Items.Count == 0)
                {
                    this.SelectedVisit = null;
                    VisitSelected(sender, new DataEventArgs<Visit>(null));
                }
            }
        }

        private void UpdateVisitButton_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedVisit != null)
            {
                PatientUpdated(this, EventArgs.Empty);
            }
        }

        #endregion

  
        #endregion


    }
}
