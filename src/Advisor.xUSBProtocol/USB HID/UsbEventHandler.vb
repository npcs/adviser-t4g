Public Class UsbEventHandler
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
        Me.Text = "EventHandler"
    End Sub

#End Region

    Private mUsbHid As UsbHid

    Public Sub New(ByVal usbHid As UsbHid)

        'Store reference
        mUsbHid = usbHid

    End Sub

    Protected Overrides Sub WndProc(ByRef m As Message)

        'Purpose    : Overrides WndProc to enable checking for and handling
        '           : WM_DEVICECHANGE(messages)

        'Accepts    : m - a Windows Message                   

        Try
            'The OnDeviceChange routine processes WM_DEVICECHANGE messages.
            If m.Msg = WM_DEVICECHANGE Then
                mUsbHid.OnDeviceChange(m)
            End If

            'Let the base form process the message.
            MyBase.WndProc(m)

        Catch ex As Exception
            Call mUsbHid.HandleException("WndProc", ex)
        End Try

    End Sub

End Class
