﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Client.DataModel;
    using Adviser.Client.Core.Presentation;
    using Adviser.Client.Core.Events;
    using Adviser.Client.Core.Services;

    /// <summary>
    /// Presentation model for OrganExamination.
    /// </summary>
    public class OrganExaminationViewModel : ViewModelBase
    {
        private Organ _organ;
        private OrganExamination _examination;
        private bool _wasAdded;
        private object _synclock = new object();
        private Visit _visit;
        private bool _surgery;
        private bool _chronicDisease;
        private bool _removed;
        private bool _implant;

        public OrganExaminationViewModel(Organ organ, Visit visit)
        {
            _organ = organ;
            _wasAdded = false;
            _visit = visit;
            //_visit.OrganExaminations.Single(examination => examination.Organ.Name == _organ.Name);
        }

        #region Public properties

        public string OrganName
        {
            get { return _organ.Name; }
        }

        public bool Surgery
        {
            get
            {
                return _surgery;
            }
            set 
            {
                _surgery = value;
                RaisePropertyChangedEvent("Surgery");
            }
        }

        public bool ChronicDisease
        {
            get
            {
                return _chronicDisease;
            }
            set
            {
                _chronicDisease = value;
                RaisePropertyChangedEvent("ChronicDisease");
            }
        }

        public bool Removed
        {
            get
            {
                return _removed;
            }
            set
            {
                _removed = value;
                RaisePropertyChangedEvent("Removed");
            }
        }

        public bool Implant
        {
            get
            {
                return _implant;
            }
            set
            {
                _implant = value;
                RaisePropertyChangedEvent("Implant");
            }
        }

        #endregion Public properties

        #region Public methods

        /// <summary>
        /// Assigns underlying OrganExamination entity.
        /// </summary>
        /// <param name="organExamination"></param>
        public void SetDataModel(OrganExamination organExamination)
        {
            if (_examination != null)
                throw new InvalidOperationException("Can not set OrganExamination entity when one was already set. Entity being added " 
                    + organExamination.Organ.Name + ". Entity exists " + _examination.Organ.Name);

            _examination = organExamination;
            UpdateViewModel();
        }

        /// <summary>
        /// Updates underlying data model (OrganExamination entity)
        /// </summary>
        public void UpdateDataModel()
        {
            if (IsAnySelected)
            {
                EnsureExaminationCreated();
                _examination.Surgery = Surgery;
                _examination.ChronicDisease = ChronicDisease;
                _examination.Removed = Removed;
                _examination.Implant = Implant;
            }
            else
            {
                if (_examination != null)
                {
                    _visit.OrganExaminations.Remove(_examination);
                    _examination.VisitReference = null;
                    _examination = null;
                }
            }
        }

        /// <summary>
        /// Updates dependency properies of this instance
        /// from the underlying data model.
        /// </summary>
        public void UpdateViewModel()
        {
            Surgery = _examination.Surgery;
            ChronicDisease = _examination.ChronicDisease;
            Removed = _examination.Removed;
            Implant = _examination.Implant;
        }

        #endregion Public methods

        /// <summary>
        /// Creates underlying OrganExamination entity
        /// if one doesn't exist.
        /// </summary>
        private void EnsureExaminationCreated()
        {
            if (_examination == null)
            {
                lock (_synclock)
                {
                    if (_examination == null)
                    {
                        _examination = OrganExamination.CreateOrganExamination(Guid.NewGuid(), -1, false, false, false, false);
                        _examination.Organ = _organ;
                        _visit.OrganExaminations.Add(_examination);
                    }
                }
            }
        }

        /// <summary>
        /// Synchronized state of the view and model when
        /// property bound to it is changed.
        /// </summary>
        /// <param name="propertyName">Name of the property that was changed by the view.</param>
        private void OnDependencyPropertyChange(string propertyName)
        {
            if (!_wasAdded)
            {//- examination was not added to collection
                if (IsAnySelected)
                {
                    _visit.OrganExaminations.Add(_examination);
                    _wasAdded = true;
                }
            }
            else
            {//- examination was added to collection
                if (!IsAnySelected)
                {
                    _visit.OrganExaminations.Remove(_examination);
                    _wasAdded = false;
                }
            }
            base.RaisePropertyChangedEvent(propertyName);
        }

        /// <summary>
        /// Indicates whether organ examination must be added to the current visit.
        /// </summary>
        private bool IsAnySelected
        {
            get
            {
                return Surgery || ChronicDisease || Removed || Implant;
            }
        }
    }
}
