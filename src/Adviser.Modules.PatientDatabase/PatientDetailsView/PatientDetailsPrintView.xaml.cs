﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Client.DataModel;
    /// <summary>
    /// Interaction logic for PatientDetailsPrintView.xaml
    /// </summary>
    public partial class PatientDetailsPrintView : UserControl, IPatientDetailsView
    {
        public PatientDetailsPrintView()
        {
            InitializeComponent();
        }

        public IPatientsPresentationModel Model
        {
            get
            {
                return this.DataContext as IPatientsPresentationModel;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
