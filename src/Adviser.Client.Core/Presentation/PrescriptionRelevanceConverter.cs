﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace Adviser.Client.Core.Presentation
{
    [ValueConversion(typeof(int), typeof(String))]
    public class PrescriptionRelevanceConverter : IValueConverter
    {
        public PrescriptionRelevanceConverter() { }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string result = " * ";
            if (value != null)
            {
                int intValue = (int)value;
                if (intValue > 0) intValue--;
                int startCount = intValue / 10;

                for (int i = 0; i < startCount; i++)
                {
                    result = String.Concat(result, "* ");
                }
//#if DEBUG
//                result += ((int)value).ToString();
//#endif
            }

            return result;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("Method is not implemented.");
        }
    }
}
