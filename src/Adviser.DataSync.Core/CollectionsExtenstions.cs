﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adviser.DataSync.Core
{
    public static class CollectionsExtenstions
    {
        public static string ToSqlInFilter(this List<Guid> IDs)
        {
            var filter = "(";
            foreach (var id in IDs)
                filter += "'" + id.ToString("D") + "',";
            filter += filter.TrimEnd(',') + ")";

            return filter;
        }
    }
}
