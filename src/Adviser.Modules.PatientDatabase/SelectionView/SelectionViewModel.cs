﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;

using Adviser.Client.DataModel;
using Adviser.Client.Core.Presentation;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Services;

namespace Adviser.Modules.PatientDatabase
{
    public class SelectionViewModel : ViewModelBase
    {
        private string _patientName;
        private string _visitDate;
        private string _nameFormatter = ">>  {0} {1}  ";
        private string _dateFormatter = ">>  {0} ";
        private AppointmentSession _session;

        public SelectionViewModel(EventAggregator eventAggregator, AppointmentSession session)
        {
            _session = session;
            eventAggregator.GetEvent<PatientSelectedEvent>().Subscribe(OnPatientSelected);
            eventAggregator.GetEvent<VisitSelectedEvent>().Subscribe(OnVisitSelected);
        }

        public string PatientName
        {
            get { return _patientName; }
            private set
            {
                _patientName = value;
                RaisePropertyChangedEvent("PatientName");
            }
        }

        public string VisitDate
        {
            get { return _visitDate; }
            set
            {
                _visitDate = value;
                RaisePropertyChangedEvent("VisitDate");
            }
        }

        private void OnPatientSelected(Patient patient)
        {
            //- filter pseudo-selections (ListView sometimes fires Selection_Changed event on exit)
            if (_session.Patient.ID == patient.ID)
                return;

            _session.Patient = patient;
            _session.Visit = null;
            VisitDate = String.Empty;
            PatientName = String.Format(_nameFormatter, patient.FirstName, patient.LastName);
        }

        private void OnVisitSelected(Visit visit)
        {
            //- filter pseudo-selections (ListView sometimes fires Selection_Changed event on exit)
            if (_session.Visit.ID == visit.ID)
                return;
            _session.Visit = visit;
            VisitDate = String.Format(_dateFormatter, visit.Date.ToString("f"));
        }
    }
}
