﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Text;

namespace AdvisorClient
{
    public interface IShellView
    {
        void CloseApplication();
        void ShowView();
        IInputElement DummyField { get; }

        event EventHandler ClosingApplication;
        event EventHandler ShellViewLoaded;
        event EventHandler MyAccountClicked;
        event EventHandler HelpClicked;

        event EventHandler USBChanged;
        
    }
}
