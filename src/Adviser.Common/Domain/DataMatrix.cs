﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Adviser.Common.Domain
{
    /// <summary>
    /// Data structure to store measurement results.
    /// Supports binary and XML serialization.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class DataMatrix<T> : IXmlSerializable
        where T : IDataSeries
    {
        private List<List<T>> data;
        private double _min;
        private double _max;
        private double _sum;
        private double _mean;
        private double _diagMean;
        private List<double> _frontalMeans;

        public DataMatrix(int rows, int columns, T defaultValue)
        {
            data = new List<List<T>>(columns);
            //- initialize one column
            List<T> column = new List<T>(rows);
            for (int i = 0; i < rows; ++i)
            {
                column.Add(defaultValue);
            }

            //- clone column
            for (int i = 0; i < columns; ++i)
            {
                data.Add(new List<T>(column.ToArray()));
            }

            _frontalMeans = new List<double>(rows);
        }

        public void AddAt(int x, int y, T value)
        {
            data[x][y] = value;
        }

        public T this[int x, int y]
        {
            get
            {
                return data[x][y];
            }
        }

        public int RowCount
        {
            get { return data[0].Count; }
        }

        public int ColumnCount
        {
            get { return data.Count; }
        }

        public List<List<T>> Columns
        {
            get
            {
                return data;
            }
        }

        /// <summary>
        /// Calculates Min, Max, and Mean values for matrix.
        /// </summary>
        public void CalculateStatistics()
        {
            //TODO: implement generic functor and IComparable interface for the T
            // to be able compare on any cell aggregate (i.e. Min, Max, Mean)
            _min = this[0, 0].Maximum;
            _max = this[0, 0].Maximum;
            double diagSum = 0;

            for (int x = 0; x < data.Count; ++x)
            {
                _frontalMeans.Add(0);

                for (int y = 0; y < data[x].Count; ++y)
                {
                    T cell = data[x][y];

                    if (cell == null)
                        continue;
                    if (cell.Maximum > _max)
                        _max = cell.Maximum;
                    if (cell.Maximum < _min)
                        _min = cell.Maximum;

                    _sum += cell.Maximum;

                    if (x == y)
                        diagSum += cell.Maximum;
                    _frontalMeans[x] += cell.Maximum;
                }
                _frontalMeans[x] /= RowCount;
            }

            //foreach (List<T> column in data)
            //{
            //    foreach (T cell in column)
            //    {
            //        if (cell == null)
            //            continue;
            //        if (cell.Maximum > _max)
            //            _max = cell.Maximum;
            //        if (cell.Minimum < _min)
            //            _min = cell.Minimum;

            //        _sum += cell.Maximum;
            //    }
            //}

            _mean = _sum / (ColumnCount * RowCount);
            _diagMean = diagSum / data.Count;
        }

        public double Min
        {
            get { return _min; }
        }

        public double Max
        {
            get { return _max; }
        }

        public double Sum
        {
            get { return _sum; }
        }

        public double Mean
        {
            get { return _mean; }
        }

        public double DiagonalMean
        {
            get { return _diagMean; }
        }

        public List<double> FrontalMeans
        {
            get { return _frontalMeans; }
        }

        /// <summary>
        /// Clears all data items of the matrix.
        /// </summary>
        public void Clear()
        {
            foreach (List<T> column in data)
            {
                foreach (T cell in column)
                {
                    if (cell != null)
                        cell.Clear();
                }
            }
            _max = 0;
            _min = 0;
            _mean = 0;
            _diagMean = 0;
            _sum = 0;
            _frontalMeans.Clear();
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            int rows = data[0].Count;
            int columns = data.Count;
            for (int y = 0; y < rows; ++y)
            {
                for (int x = 0; x < columns; ++x)
                {
                    result.Append(Convert.ToString(this[x, y]) + "\t");
                }
                result.Append(Environment.NewLine);
            }
            result.Append("Diagonal mean = " + DiagonalMean);

            return result.ToString();
        }


        //TODO: implement IXmlSerializable
        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
