﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Adviser.Modules.PatientDatabase
{
    /// <summary>
    /// Interaction logic for PractitionerDetailsView.xaml
    /// </summary>
    public partial class PractitionerDetailsView : UserControl, IPractitionerDetailsView
    {
        public PractitionerDetailsView()
        {
            InitializeComponent();
        }

        private void btnUpdatePatient_Click(object sender, RoutedEventArgs e)
        {
            UpdateClicked(this, null);
        }

        #region IPractitionerDetailsView Members

        public void ShowMessage(string message)
        {
            btnOk.Visibility = Visibility.Visible;
            MessageTextBlock.Text = message;
            MessagePopup.StaysOpen = true;
            MessagePopup.IsOpen = true;
        }

        public void ShowMessage(string message, bool isOKButtonVisible)
        {
            btnOk.Visibility = isOKButtonVisible ? Visibility.Visible : Visibility.Hidden;
            MessageTextBlock.Text = message;
            MessagePopup.StaysOpen = true;
            MessagePopup.IsOpen = true;           
        }

        public IPractitionerModel Model
        {
            get
            {
                return this.DataContext as IPractitionerModel;
            }
            set
            {
                this.DataContext = value;
            }
        }
        private bool _IsUpdateMode = true;
        public bool IsUpdateMode
        {
            get
            {
                return _IsUpdateMode;
            }
            set
            {
                _IsUpdateMode = value;
                if (_IsUpdateMode)
                {
                    CreateAccountMessage.Visibility = Visibility.Hidden;
                    this.UpdateButton.Content = "Update";
                }
                else
                {
                    CreateAccountMessage.Visibility = Visibility.Visible;
                    this.UpdateButton.Content = "Create";
                }
            }
        }

        public event EventHandler UpdateClicked;
        public event EventHandler CreateClicked;
        public event EventHandler CancelClicked;
        public event EventHandler OkClicked;

        #endregion

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            CancelClicked(this, null);
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            if (!FormHasErrors())
            {
                if (_IsUpdateMode)
                {
                    UpdateClicked(this, null);
                }
                else
                {
                    CreateClicked(this, null);
                }
            }
        }

        #region Validation

        private bool FormHasErrors()
        {
            StringBuilder sb = new StringBuilder();
            GetErrors(sb, PractitionerInformation);
            sb.ToString();
            return sb.ToString() != "";
        }

        private void GetErrors(StringBuilder sb, object obj)
        {
            TextBox element = obj as TextBox;
            if (element != null)
            {
                BindingExpression binding = element.GetBindingExpression(TextBox.TextProperty);
                // Update the linked source (the TextBlock).
                binding.UpdateSource();

                if (Validation.GetHasError(element))
                {
                    sb.Append(element.Text + " has errors:\r\n");
                    foreach (ValidationError error in Validation.GetErrors(element))
                    {
                        sb.Append("  " + error.ErrorContent.ToString());
                        sb.Append("\r\n");
                    }
                }
            }

            DependencyObject depObj = obj as DependencyObject;
            if (depObj != null)
            {
                foreach (object child in LogicalTreeHelper.GetChildren(depObj))
                {
                    GetErrors(sb, child);

                }
            }
        }

        #endregion

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            MessageTextBlock.Text = "";
            this.MessagePopup.IsOpen = false;
            if(OkClicked != null)
                OkClicked(this, null);
        }

    }
}
