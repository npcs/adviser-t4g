﻿using System;
namespace Adviser.Common.Logging
{
    public interface ILogEntryFormatter
    {
        string TimeFormat { get; set; }
    }
}
