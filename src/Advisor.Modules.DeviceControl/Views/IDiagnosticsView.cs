﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Modules.DeviceControl.Views
{
    public interface IDiagnosticsView
    {
        void SetLeftPanel(object view);
        void SetRightPanel(object view);
    }
}
