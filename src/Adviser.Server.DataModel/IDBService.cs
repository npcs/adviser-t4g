﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;

namespace Adviser.Server.DataModel
{
    public interface IDBService
    {
        void AddPatient(Practitioner CurrentPractitioner);
        Patient CreateNewPatient(Practitioner CurrentPractitioner);
        Visit CreateNewVisit(Patient CurrentPatient);
        IQueryable<Organ> GetAllOrgans();
        IQueryable<Condition> GetAllConditions();
        IQueryable<Medicine> GetAllMedicine();
        IQueryable<Organ> GetOrgansBySex(bool isMale);
        IQueryable<OrganZoneDependency> GetOrganZoneDependencies();
        IQueryable<Symptom> GetAllSymptoms();
        Medicine GetMedicineByID(int id);
        Device GetDeviceByID(Guid id);
        MedicineChronicOrgan GetMedicineIdByChronicOrgan(int organId);
        List<Medicine> GetMedicineIdByOrganCondition(int organId, int value);
        Condition GetConditionByValue(int examValue);
        void DeletePatientById(Guid id);
        void SaveVisit(Guid patientId, Visit visit);
        Condition GetCondition(int organId, int examValue);
        void AddMedicineChronicOrgan(MedicineChronicOrgan entry);
        void AddMedicineOrganCondition(MedicineOrganConditionXRef entry);
        
        EntityCollection<OrganExamination> RetriveOrganExaminationEntityCollection(Visit CurrentVisit);
        EntityCollection<Patient> RetrivePatientEntityCollection(Practitioner CurrentPractitioner);
        Practitioner GetPractitionerById(Guid PractitionerID);
        EntityCollection<Prescription> RetrivePrescriptionEntityCollection(Visit CurrentVisit);
        EntityCollection<Symptom> RetriveSymptomEntityCollection(Visit CurrentVisit);
        EntityCollection<Visit> RetriveVisitEntityCollection(Patient CurrentPatient);

        void SaveChanges();
        void DeleteObject(object EntityObject);
        object GetObjectByKey(System.Data.EntityKey key);
        void DeleteObjectByKey<T>(Guid id);
    }
}
