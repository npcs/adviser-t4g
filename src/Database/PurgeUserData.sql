-- client
delete from organexamination
delete from prescriptions
delete from symptomVisitXRef
delete from visits
delete from patients

delete from changes
delete from errors
delete from SyncItems

delete from practitioners

-- server
delete from organexamination
delete from prescriptions
delete from symptomVisitXRef
delete from visits
delete from patients
delete from usagedata
delete from servererrors
delete from clienterrors
delete from practitioners

-- delete from practitioners where id <> '00000000-0000-0000-0000-000000000000'
-- update devices set status = 'Available'