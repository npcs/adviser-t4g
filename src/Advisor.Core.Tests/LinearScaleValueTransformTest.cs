﻿using Adviser.Client.Core.DeviceControl;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Adviser.Common.Domain;

namespace Advisor.Core.Tests
{
    
    
    /// <summary>
    ///This is a test class for LinearScaleValueTransformTest and is intended
    ///to contain all LinearScaleValueTransformTest Unit Tests
    ///</summary>
    [TestClass()]
    public class LinearScaleValueTransformTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for LinearScaleValueTransform Constructor
        ///</summary>
        [TestMethod()]
        public void LinearScaleValueTransformConstructorTest()
        {
            double min = 100d; // TODO: Initialize to an appropriate value
            double max = 500d; // TODO: Initialize to an appropriate value
            double lowerBound = 0d; // TODO: Initialize to an appropriate value
            double upperBound = 100d; // TODO: Initialize to an appropriate value
            LinearMapValueTransform target = new LinearMapValueTransform(min, max, lowerBound, upperBound);
            Assert.AreEqual(lowerBound, target.Transform(min));
            Assert.AreEqual(upperBound, target.Transform(max));
            Assert.AreEqual(50D, target.Transform(300F));
        }
    }
}
