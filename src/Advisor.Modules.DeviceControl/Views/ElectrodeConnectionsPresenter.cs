﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Events;


namespace Advisor.Modules.DeviceControl.Views
{
    public interface IElectrodeConnectionsPresenter
    {
        IElectrodeConnectionsView View { get; set; }
    }

    public class ElectrodeConnectionsPresenter : IElectrodeConnectionsPresenter
    {
        public ElectrodeConnectionsPresenter(IElectrodeConnectionsView view, IElectrodeConnectionsViewModel model)
        {
            this.View = view;
            this.View.Model = model;
        }

        #region IElectrodeConnectionsPresenter Members

        public IElectrodeConnectionsView View
        {
            get;
            set;
        }

        #endregion
    }
}
