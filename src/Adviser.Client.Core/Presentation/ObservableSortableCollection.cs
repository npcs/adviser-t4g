﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Adviser.Client.Core.Presentation
{
    /// <summary>
    /// Observabel collection that can be sorted.
    /// </summary>
    /// <remarks>
    /// Usage: myCollection.Sort(item => item.Value);
    /// </remarks>
    /// <typeparam name="T">Collection items type.</typeparam>
    public class ObservableSortableCollection<T> : ObservableCollection<T>
    {
        /// <summary>
        /// Sort by given key.
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="keySelector"></param>
        public void Sort<TKey>(Func<T, TKey> keySelector)
        {
            SortItems(Items.OrderBy(keySelector));
        }

        /// <summary>
        /// Sort by given selector and custom comparer.
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="keySelector"></param>
        /// <param name="comparer"></param>
        public void Sort<TKey>(Func<T, TKey> keySelector, IComparer<TKey> comparer)
        {
            SortItems(Items.OrderBy(keySelector, comparer));
        }

        private void SortItems(IEnumerable<T> sortedItems)
        {
            var sortedItemsList = sortedItems.ToList();
            foreach (var item in sortedItemsList)
            {
                Move(IndexOf(item), sortedItemsList.IndexOf(item));
            }
        }
    }
}
