﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace Advisor.Common.Presentation
{
    [ValueConversion(typeof(DateTime), typeof(String))]
    public class DateTimeFormatter : IValueConverter
    {
        public DateTimeFormatter() { }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string result = String.Empty;
            if (value != null)
            {
                string formatString = (string)parameter;
                result = ((DateTime)value).ToString(formatString);
            }

            return result;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("Method is not implemented.");
        }
    }
}
