﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Adviser.Server.Services.Contracts
{
    [DataContract]
    public class OrganExamination
    {
        [DataMember]
        public Guid ID;

        [DataMember]
        public int Value;

        [DataMember]
        public bool Surgery;

        [DataMember]
        public bool ChronicDisease;

        [DataMember]
        public bool Removed;

        [DataMember]
        public bool Implant;

        [DataMember]
        public int OrganID;
    }
}
