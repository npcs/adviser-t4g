﻿using Adviser.DeviceTester;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Adviser.HardwareTests.Tests
{


    /// <summary>
    ///This is a test class for TestFormTest and is intended
    ///to contain all TestFormTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TestFormTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AreEqual
        ///</summary>
        [TestMethod()]
        public void AreEqualTest()
        {
            TestForm target = new TestForm();
            byte[] bytes1 = new byte[16];
            byte[] bytes2 = new byte[16];
            bool expected = true;
            bool actual;
            actual = target.AreEqual(bytes1, bytes2);
            Assert.AreEqual(expected, actual);

            //------------------------------ 

            bytes1 = new byte[16];
            bytes2 = new byte[10];
            expected = false;
            actual = target.AreEqual(bytes1, bytes2);
            Assert.AreEqual(expected, actual);

            //------------------------------

            bytes1 = new byte[16];
            bytes2 = new byte[16];
            for (int i = 0; i < bytes1.Length; i++)
            {
                bytes1[i] = (byte)i;
            }
            for (int i = 0; i < bytes2.Length; i++)
            {
                bytes2[i] = (byte)i;
            }
            expected = true;
            actual = target.AreEqual(bytes1, bytes2);
            Assert.AreEqual(expected, actual);

            //------------------------------

            bytes1 = new byte[16];
            bytes2 = new byte[16];
            for (int i = 0; i < bytes1.Length; i++)
            {
                bytes1[i] = (byte)i;
            }
            for (int i = 0; i < bytes2.Length; i++)
            {
                bytes2[i] = (byte)i;
            }
            bytes2[15] = 0;
            expected = false;
            actual = target.AreEqual(bytes1, bytes2);
            Assert.AreEqual(expected, actual);

            //------------------------------

            bytes1 = new byte[16];
            bytes2 = new byte[16];
            for (int i = 0; i < bytes1.Length; i++)
            {
                bytes1[i] = (byte)
                    (i + 1);
            }
            for (int i = 0; i < bytes2.Length; i++)
            {
                bytes2[i] = (byte)i;
            }
            expected = false;
            actual = target.AreEqual(bytes1, bytes2);
            Assert.AreEqual(expected, actual);

        }
    }
}
