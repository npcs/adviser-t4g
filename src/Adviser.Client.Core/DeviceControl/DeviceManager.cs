﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Messaging;
using System.Diagnostics;

using Adviser.Common.Domain;
using Adviser.Client.Core.Events;

namespace Adviser.Client.Core.DeviceControl
{
    public delegate void MeasurementCompletedEventHandler(object source, MeasurementEventArgs e);
    public delegate void MeasurementProgressEventHandler(object source, MeasurementProgressEventArgs e);
    public delegate DataMatrix<PairMeasurement> DoMeasurementDelegate();

    public class DeviceManager : IDeviceManager
    {
        private IDeviceDriver _device;
        private MeasurementSettings _measurementSettings;
        private DeviceSettings _deviceSettings;

        private bool _isStopped;

        public DeviceManager()
        {
            _isStopped = true;
        }

        #region events

        /// <summary>
        /// Raised when measurement session is completed.
        /// </summary>
        public event MeasurementCompletedEventHandler MeasurementCompleted;

        /// <summary>
        /// Raised every time measurement session status is updated.
        /// </summary>
        public event MeasurementProgressEventHandler MeasurementProgress;

        #endregion events

        #region properties

        public IDeviceDriver Device
        {
            get { return _device; }
            private set { _device = value; }
        }

        public MeasurementSettings Settings
        {
            get { return _measurementSettings; }
            set { _measurementSettings = value; }
        }

        public bool IsStopped
        {
            get { return _isStopped; }
        }

        public bool IsConnected
        {
            get { return _device.IsConnected; }

        }

        public bool IsSet
        {
            get;
            private set;
        }

        #endregion properites

        public void Initialize(DeviceSettings deviceSettings, MeasurementSettings measurementSettings)
        {
            _deviceSettings = deviceSettings;
            _measurementSettings = measurementSettings;

            if (_device == null)
            {
                if (_deviceSettings.EmulateDevice)
                    _device = new DeviceEmulator();
                else
                    _device = new DeviceDriver();
            }

            //IsConnected = _device.IsConnected;
            if (!IsConnected)
                return;
            IsSet = _device.SetupDevice(_deviceSettings);
        }

        public void Release()
        {
            //if (_device != null)
            //{//- release and clean up
            //    _device.Release();
            //    _device = null;
            //}
        }

        /// <summary>
        /// Main method that starts measurement session.
        /// </summary>
        public void StartMeasurement()
        {
            if (_isStopped)
            {
                _isStopped = false;

                _device.SetupDevice(_deviceSettings);
                _device.SetPolarity(_measurementSettings.IsFrontPositive);
                _measurementSettings.Voltage = GetBusVoltage();

                //- start measurement on delegate thread
                DoMeasurementDelegate target = new DoMeasurementDelegate(DoMeasurement);
                AsyncCallback callback = new AsyncCallback(MeasurementEndCallback);
                MeasurementContext context = new MeasurementContext();
                context.DeviceSettings = _deviceSettings;
                context.MeasurementSettings = _measurementSettings;
                target.BeginInvoke(callback, context);
            }
            //TODO: create alternate branch
        }

        public void StopMeasurement()
        {
            if (_isStopped)
                return;

            _isStopped = true;
        }

        /// <summary>
        /// Returns voltage on measurement bus
        /// </summary>
        /// <returns></returns>
        public double GetBusVoltage()
        {
            ushort s = _device.ReadBusVoltage();//TODO: find out how to convert received value to Volts
            double volts = s / 2000.0;
            return volts;
        }

        /// <summary>
        /// Returns returns device identificator.
        /// </summary>
        /// <returns></returns>
        public Guid GetDeviceId()
        {
            Guid id = Guid.NewGuid();
            bool hasId = _device.GetDeviceGUID(ref id);
            return id;
        }

        /// <summary>
        /// Returns mainboard version.
        /// </summary>
        /// <returns></returns>
        public byte[] GetDeviceVersion()
        {
            byte[] version = new byte[16];
            string errorMessage = "";
            bool hasVersion = _device.GetMainBoardVersion(out version, ref errorMessage);
            return version;
        }

        /// <summary>
        /// Method that is called upon DoMeasurement asynchronous invocation.
        /// </summary>
        /// <param name="ar"></param>
        private void MeasurementEndCallback(IAsyncResult ar)
        {
            try
            {
                DoMeasurementDelegate targetDelegate = (DoMeasurementDelegate)((AsyncResult)ar).AsyncDelegate;
                DataMatrix<PairMeasurement> matrix = targetDelegate.EndInvoke(ar);
                MeasurementContext context = (MeasurementContext)ar.AsyncState;
                _isStopped = true;
                if (MeasurementCompleted != null && matrix != null)
                {
                    MeasurementEventArgs e = new MeasurementEventArgs();
                    e.Status = MeasurementStatus.Completed;
                    e.MeasurementData = matrix;
                    e.Context = context;

                    MeasurementCompleted(this, e);
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Exception in DeveiceManger.MeasurementEndCallback():\n" + ex.ToString());
                Trace.WriteLine("Measurement Context was:\n" + ex.ToString());

                if (MeasurementCompleted != null)
                {
                    MeasurementEventArgs e = new MeasurementEventArgs();
                    e.Status = MeasurementStatus.Failed;
                    e.MeasurementData = null;
                    e.Context = null;

                    MeasurementCompleted(this, e);
                }
            }
            finally
            {
            }
        }

        /// <summary>
        /// Conducts measurement using Device driver according to the mesurement
        /// settings.
        /// </summary>
        private DataMatrix<PairMeasurement> DoMeasurement()
        {
            int readPeriod = _measurementSettings.DeviceSamplingRate;
            int pairProgress = 0;

            int totalTime = _measurementSettings.PairMeasurementDuration * _measurementSettings.DeviceSamplingRate * _measurementSettings.ElectrodesConnectionMap.Count;
            int totalRemaining = totalTime;
            int totalElapsed = 0;
            int totalProgress = 0;
            ushort readout = 0;
            //TODO: Implement this using timer vs. Thread.Sleep() (considering, ???)
            //Timer timer = new Timer(OnTimerExpired);

            DataMatrix<PairMeasurement> measurementMatrix = new DataMatrix<PairMeasurement>(_measurementSettings.FrontLeads, _measurementSettings.BackLeads, null);
            foreach (PairConnection pair in _measurementSettings.ElectrodesConnectionMap)
            {

                //If the real device is used, then make mesurements on callibration resistor
                if (_measurementSettings.Calibration)
                {
                    _device.ConnectElectrodes(_deviceSettings.CalibrationLeadA, _deviceSettings.CalibrationLeadB);
                }
                else
                {
                    _device.ConnectElectrodes(pair.Front, pair.Back);
                }

                Thread.Sleep(_measurementSettings.CommutationDelay);

                PairMeasurement measurement = new PairMeasurement();
                measurement.SampleRate = (ushort)_measurementSettings.DeviceSamplingRate;
                for (int samples = 0; samples < _measurementSettings.PairMeasurementDuration;)
                {       
                    //- read next sample
                    readout = _device.ReadData();
                    //measurement.Add(CalculateConductivity(readout));
                    measurement.Add(readout);

                    //- calculate progress
                    //samples += _measurementSettings.DeviceSamplingRate;
                    pairProgress = 100 * (++samples / _measurementSettings.PairMeasurementDuration);
                    totalElapsed += _measurementSettings.DeviceSamplingRate;
                    totalProgress = 100 * totalElapsed / totalTime;

                    Thread.Sleep(_measurementSettings.DeviceSamplingRate);
                }

                FireMeasurementProgress(pair, pairProgress, totalProgress, measurement);

                //if (!_deviceSettings.EmulateDevice)
                {
                    ////-detect short and fire event
                    //if (measurement.Mean < _deviceSettings.MeasurementMeanNoConnectionValue) // 111
                    //{
                    //    FireMeasurementError(String.Concat("Front electrode ", (pair.Front + 1).ToString(), " and back electrode ", (pair.Back + 1).ToString(), " may not be properly connected.\n Please check for reliable contact and start measurement again.\n", "[code: ", measurement.Mean.ToString(), "]"));
                    //    return null;
                    //}

                    //-no connection detection and fire event
                    if (measurement.Mean > _deviceSettings.MeasurementMeanShortValue) // 4000
                    {
                        FireMeasurementError(String.Concat("Front electrode ", (pair.Front + 1).ToString(), " and back electrode ", (pair.Back + 1).ToString(), " may not be properly connected.\n Please check for reliable contact and start measurement again.\n", "[code: ", measurement.Mean.ToString(), "]"));
                        return null;
                    }
                }

                measurementMatrix.AddAt(pair.Front, pair.Back, measurement);

                if (_isStopped)
                    break;
            }

            measurementMatrix.CalculateStatistics();

            Debug.WriteLine("DataMatrix completed:\n" + measurementMatrix.ToString());

            return measurementMatrix;
        }

        /// <summary>
        /// Raises MeasurementProgress event.
        /// </summary>
        /// <param name="pair"></param>
        /// <param name="pairProgress"></param>
        /// <param name="totalProgress"></param>
        private void FireMeasurementProgress(PairConnection pair, int pairProgress, int totalProgress, PairMeasurement measurement)
        {
            Debug.WriteLine("Conductivity between Front " + pair.Front + " and Back " + pair.Back + ":" + measurement.Maximum + ". Progress " + totalProgress + "%");

            if (MeasurementProgress != null)
            {
                MeasurementProgressEventArgs e = new MeasurementProgressEventArgs();
                e.FrontElectrode = pair.Front;
                e.BackElectrode = pair.Back;
                e.PairProgress = pairProgress;
                e.TotalProgress = totalProgress;
                e.Measurement = measurement;

                MeasurementProgress(this, e);
            }
        }

        private void FireMeasurementError(string error)
        {
            if (MeasurementCompleted != null)
            {
                MeasurementEventArgs e = new MeasurementEventArgs();
                e.Status = MeasurementStatus.Failed;                
                e.Message = error;
                MeasurementCompleted(this, e);
            }
        }
        /// <summary>
        /// Calculates real conductivity value (1/R) of measured object:
        /// Conductivity = Vmeasured / (Rshunt * Vinput)
        ///   where 
        ///   Vmeasured - measured voltage on Rshunt
        ///   Icurrent = Vmeasured / Rshunt - current in circuit
        ///   
        /// </summary>
        /// <param name="rawV"></param>
        /// <returns></returns>
        private double CalculateConductivity(ushort rawV)
        {
            double conductivity;
            double Vmeasured = rawV * _deviceSettings.MeasurementScaleFactor;

            conductivity = Vmeasured / (_measurementSettings.ShuntResistor * _measurementSettings.Voltage);
            return conductivity;
        }

        public void OnTimerExpired(object state)
        {
            //TODO: implement this instead of Thread.Sleep() ???
        }

        #region IDeviceManager Members


        public void ReInitialize()
        {
            if (_device != null)
            {
                _device.ReInitialize();

            }
        }

        #endregion
    }

}
