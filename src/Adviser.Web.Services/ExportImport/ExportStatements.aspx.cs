﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.IO;

using System.Text.RegularExpressions;

namespace Adviser.Web.Services.ExportImport
{
    public enum ExportType
    {
        CSV, Excel, Word
    }

    public sealed class ExportHelper
    {
        public static void ExportToCsv(string gridViewText, string contentType, HttpResponse response)
        {
            const string m_Delimiter_Column = ",";
            string m_Delimiter_Row = Environment.NewLine;

            response.ContentType = contentType;

            Regex m_RegEx = new Regex(@"(>\s+<)", RegexOptions.IgnoreCase);
            gridViewText = m_RegEx.Replace(gridViewText, "><");

            gridViewText = gridViewText.Replace(m_Delimiter_Row, String.Empty);
            gridViewText = gridViewText.Replace("</td></tr>", m_Delimiter_Row);
            gridViewText = gridViewText.Replace("<tr><td>", String.Empty);
            gridViewText = gridViewText.Replace(m_Delimiter_Column, "\\" + m_Delimiter_Column);
            gridViewText = gridViewText.Replace("</td><td>", m_Delimiter_Column);

            m_RegEx = new Regex(@"<[^>]*>", RegexOptions.IgnoreCase);
            gridViewText = m_RegEx.Replace(gridViewText, String.Empty);

            gridViewText = HttpUtility.HtmlDecode(gridViewText);
            response.Write(gridViewText);
            response.End();
        }

        public static void ExportToExcelWord(string gridViewText, string contentType, HttpResponse response)
        {
            response.ContentType = contentType;
            response.Write(gridViewText);
            response.End();
        }
    }

    public partial class ExportStatements : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                YearListBox.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString()));
                YearListBox.Items.Add(new ListItem(DateTime.Now.Year.ToString()));
                YearListBox.SelectedIndex = 1;

                MonthListBox.SelectedIndex = DateTime.Now.Month - 1;
            }


        }

        public override void VerifyRenderingInServerForm(Control control) { }

        protected void GetButton_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string spName;
            string fileName;
            if (btn.ID == "GetCustomersButton")
            {
                spName = "ExportCustomers";
                fileName = "ImportCustomers";
            }
            else
            {
                spName = "ExportInvoices";
                fileName = "ImportInvoices";
            }

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.csv", fileName));
            Response.ContentEncoding = Encoding.UTF8;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            int year = int.Parse(YearListBox.SelectedValue);
            int month = MonthListBox.SelectedIndex + 1;
            int daysInSelectedMonth = DateTime.DaysInMonth(year, month);

            string sConnStr = "{SERVER_DB}";
            SqlConnection connection = new SqlConnection(sConnStr);
            SqlCommand command = new SqlCommand(spName, connection);

            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = 999999;

            command.Parameters.Add(new SqlParameter("@BeginDate", SqlDbType.DateTime));
            command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));

            //TODO: make sure dates are inclusive
            command.Parameters["@BeginDate"].Value = new DateTime(year, month, 1);
            command.Parameters["@EndDate"].Value = (new DateTime(year, month, daysInSelectedMonth)).AddDays(1);

            try
            {
                connection.Open();
                SqlDataReader dr = command.ExecuteReader();
                StringBuilder sb = new StringBuilder();
                //Add Header          
                for (int count = 0; count < dr.FieldCount; count++)
                {
                    if (dr.GetName(count) != null)
                        sb.Append(dr.GetName(count));
                    if (count < dr.FieldCount - 1)
                    {
                        sb.Append(",");
                    }
                }
                Response.Write(sb.ToString() + "\n");
                Response.Flush();
                //Append Data
                while (dr.Read())
                {
                    sb = new StringBuilder();

                    for (int col = 0; col < dr.FieldCount - 1; col++)
                    {
                        if (!dr.IsDBNull(col))
                            sb.Append(dr.GetValue(col).ToString().Replace(",", " "));
                        sb.Append(",");
                    }
                    if (!dr.IsDBNull(dr.FieldCount - 1))
                        sb.Append(dr.GetValue(dr.FieldCount - 1).ToString().Replace(",", " "));
                    Response.Write(sb.ToString() + "\n");
                    Response.Flush();
                }
                dr.Dispose();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                connection.Close();
            }

            Response.End();

        }
    }
}
