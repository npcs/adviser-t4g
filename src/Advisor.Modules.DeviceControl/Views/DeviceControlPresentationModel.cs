﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Events;

using Adviser.Common.Domain;
using Adviser.Client.Core.Services;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Presentation;
using Advisor.Modules.DeviceControl.Services;


namespace Advisor.Modules.DeviceControl.Views
{
    public enum DeviceState
    {
        NotReady,
        Ready,
        Working,
        LowVoltage
    }

    public class DeviceControlPresentationModel : ViewModelBase, IDeviceControlPresentationModel
    {
        private IDeviceControlView _deviceControlView;
        private IDeviceControlService _deviceService;
        private DeviceInformation _deviceInfo;
        private DeviceState _deviceState;
        private AppointmentSession _session;

        private decimal _totalProgress;
        private decimal _pairProgress;
        private string _deviceStatus;
        private string _batteryMessage;
        private string _userMessage;
        private string _errorMessage;

        public DeviceControlPresentationModel(
            IDeviceControlView view,
            IEventAggregator eventAggregator,
            IDeviceControlService deviceService,
            AppointmentSession session)
        {
            _deviceControlView = view;
            View.Model = this;
            _session = session;

            _deviceService = deviceService;
            _deviceState = DeviceState.NotReady;

            StartCommand = new DelegateCommand<string>(OnStart, CanStart);
            StopCommand = new DelegateCommand<string>(OnStop, CanStop);
            ConnectToTheDeviceCommand = new DelegateCommand<string>(OnConnectToTheDevice, CanConnectToTheDevice);

            eventAggregator.GetEvent<MeasurementProgressEvent>().Subscribe(MeasurementProgress, ThreadOption.UIThread);
            eventAggregator.GetEvent<MeasurementCompletedEvent>().Subscribe(MeasurementCompleted, ThreadOption.UIThread);
            eventAggregator.GetEvent<MeasurementResetEvent>().Subscribe(MeasurementReset, ThreadOption.UIThread);
            eventAggregator.GetEvent<NavigationEvent>().Subscribe(OnNavigationEvent);
            eventAggregator.GetEvent<USBChangedEvent>().Subscribe(OnUSBChangedEvent);

            view.ViewUnloaded += new EventHandler(OnViewUnload);

//#if DEBUG
            deviceService.InitializeDevice();
            session.Device = deviceService.GetDeviceInfo();
//#endif
        }

        #region IDeviceControlPresentationModel Members

        public IDeviceControlView View
        {
            get { return _deviceControlView; }
        }

        public DelegateCommand<string> ConnectToTheDeviceCommand
        {
            get;
            private set;
        }

        public DelegateCommand<string> StartCommand
        {
            get;
            private set;
        }

        public DelegateCommand<string> StopCommand
        {
            get;
            private set;
        }

        public decimal TotalProgress
        {
            get { return _totalProgress; }
            private set
            {
                _totalProgress = value;
                base.RaisePropertyChangedEvent("TotalProgress");
            }
        }

        public decimal PairProgress
        {
            get { return _pairProgress; }
            private set
            {
                _pairProgress = value;
                base.RaisePropertyChangedEvent("PairProgress");
            }
        }

        public string DeviceStatusMessage
        {
            get { return _deviceStatus; }
            set
            {
                _deviceStatus = value;
                RaisePropertyChangedEvent("DeviceStatusMessage");
            }
        }

        public string BatteryMessage
        {
            get { return _batteryMessage; }
            set
            {
                _batteryMessage = value;
                RaisePropertyChangedEvent("BatteryMessage");
            }
        }

        public string UserMessage
        {
            get { return _userMessage; }
            set
            {
                _userMessage = value;
                RaisePropertyChangedEvent("UserMessage");
            }
        }

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                _errorMessage = value;
                RaisePropertyChangedEvent("ErrorMessage");
            }
        }

        public bool IsSoundOn
        {
            get;
            set;
        }

        #endregion

        private void OnStart(string parameter)
        {
            _deviceService.StartMeasurement();
            _deviceState = DeviceState.Working;
        }

        private void OnConnectToTheDevice(string parameter)
        {
            OnUSBChangedEvent(null);
        }

        private bool CanConnectToTheDevice(string parameter)
        {
            if (_deviceState == DeviceState.LowVoltage)
                return true;
            else
                return false;
        }


        private bool CanStart(string parameter)
        {
            if (_deviceState == DeviceState.Ready)
                return true;
            else
                return false;
        }

        private void OnStop(string parameter)
        {
            _deviceService.StopMeasurement();
            _deviceState = DeviceState.Ready;
        }

        private bool CanStop(string parameter)
        {
            if (_deviceState == DeviceState.Working)
                return true;
            else
                return false;
        }

        private void OnNavigationEvent(NavigationEventArgs e)
        {
            if (e.EventName == NavigationEventNames.Measurement)
            {
                InitializeDevice();
                TotalProgress = 0;
            }
        }

        private void OnUSBChangedEvent(object e)
        {
            _deviceService.InitializeDevice();
            _deviceService.ReInitialize();
            InitializeDevice();
        }

        private void OnViewUnload(object source, EventArgs e)
        {
            _deviceService.ReleaseDevice();
        }

        /// <summary>
        /// Initializes device and populates presentation model with device status information.
        /// </summary>
        private void InitializeDevice()
        {
            _deviceService.InitializeDevice();
            _deviceInfo = _deviceService.GetDeviceInfo();
            _session.Device = _deviceInfo;

            if (_deviceInfo.IsConnected)
            {
                DeviceStatusMessage = "Connected";
                if (_deviceInfo.Voltage >= 0.5)//TODO: move this to the settings
                {
                    BatteryMessage = "Normal (" + _deviceInfo.Voltage + "V)";
                    UserMessage = "Please connect electrodes to the indicated locations on the body and click START to begin diagnostics.";
                    _deviceState = DeviceState.Ready;
                    StartCommand.RaiseCanExecuteChanged();
                    ConnectToTheDeviceCommand.RaiseCanExecuteChanged();
                }
                else
                {
                    _deviceState = DeviceState.LowVoltage;

                    StartCommand.RaiseCanExecuteChanged();
                    ConnectToTheDeviceCommand.RaiseCanExecuteChanged();

                    BatteryMessage = "Low (" + _deviceInfo.Voltage + "V)";
                    UserMessage = "The battery voltage is low.\n" +
                        "Please make sure that ON/OFF switch is on (the indicator light is on).\n" +
                        "Please switch the device on and click 'Connect to the Device'.\n" +
                        "If switch is ON and you still receive this message, please replace batteries from the back of Adviser device.\n";
                }
            }
            else
            {
                _deviceState = DeviceState.NotReady;
                StartCommand.RaiseCanExecuteChanged();
                ConnectToTheDeviceCommand.RaiseCanExecuteChanged();
                BatteryMessage = "Not available";
                DeviceStatusMessage = "Not connected";
                UserMessage = "Please check USB connection between Adviser device and computer. \n" +
                    "Make sure USB light indicator is illuminated when you connect cable.\n";
            }
        }

        private void MeasurementProgress(MeasurementProgressEventArgs e)
        {
            TotalProgress = e.TotalProgress;
            PairProgress = e.PairProgress;

            UserMessage = "Measurement is in progress. Please wait until the operation completed.\n" +
                "Progress so far: " + TotalProgress + "%";

            if(e.Measurement != null && IsSoundOn)
                System.Console.Beep((int)e.Measurement.Maximum, 100);
        }

        private void MeasurementCompleted(MeasurementEventArgs e)
        {
            System.Media.SystemSounds.Beep.Play();
            if (e.Status == MeasurementStatus.Completed)
            {
                TotalProgress = 100;
                PairProgress = 100;

                _deviceState = DeviceState.NotReady;
                UserMessage = "Measurement completed.\n" +
                    "Please turn the switch OFF and disconnect wires from the patient.";
                StartCommand.RaiseCanExecuteChanged();
                StopCommand.RaiseCanExecuteChanged();
            }

            if (e.Status == MeasurementStatus.Failed)
            {
                //TotalProgress = 0;
                //PairProgress = 0;

                _deviceState = DeviceState.Ready;
                UserMessage = "Measurement failed.\n" + e.Message;
                StartCommand.RaiseCanExecuteChanged();
                StopCommand.RaiseCanExecuteChanged();
            }

        }

        private void MeasurementReset(bool e)
        {
            TotalProgress = 0;
            PairProgress = 0;

            _deviceState = DeviceState.Ready;
            UserMessage = "Measurement was cancelled.\n" +
                "If you are done please turn the switch OFF and disconnect wires from the patient.\n" +
                "Otherwise, check connections and start measurement again.";
            StartCommand.RaiseCanExecuteChanged();
            StopCommand.RaiseCanExecuteChanged();
        }
    }
}
