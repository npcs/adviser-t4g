﻿using System;
using System.Collections.Generic;

namespace Adviser.Client.DataModel
{
    public partial class OrganExamination
    {
        public bool IsEditable
        {
            get;
            set;
        }
    }
}
