﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Modules.PatientDatabase
{
    public interface ISelectionView
    {
        object Model { get; set; }
    }
}
