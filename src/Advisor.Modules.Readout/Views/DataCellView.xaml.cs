﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Composite.Events;
using Advisor.Modules.Readout.PresentationModels;

namespace Advisor.Modules.Readout.Views
{
    /// <summary>
    /// Interaction logic for DataCellView.xaml
    /// </summary>
    public partial class DataCellView : UserControl, IDataCellView
    {
        public event EventHandler<DataEventArgs<IDataCellViewModel>> CellSelected = delegate { };

        public DataCellView()
        {
            InitializeComponent();
        }

        public IDataCellViewModel Model
        {
            get
            {
                return this.DataContext as IDataCellViewModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            CellSelected(sender, new DataEventArgs<IDataCellViewModel>(Model));
        }

    }
}
