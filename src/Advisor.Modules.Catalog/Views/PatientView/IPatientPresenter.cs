﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Modules.Catalog.Views
{
    public interface IPatientPresenter
    {
        IPatientView View { get; }

    }
}
