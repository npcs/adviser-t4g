﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Events;

using Adviser.Common;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Presentation;

namespace Advisor.Modules.Navigation
{
    public class PrintNavigationState : NavigationState
    {
        private AppointmentSession _session;
        private IUnityContainer _container;

        public PrintNavigationState(IUnityContainer container, AppointmentSession session, IEventAggregator eventAggregator)
        {
            _container = container;
            _session = session;
        }

        protected override void SetControls()
        {
            ControlMap[NavigationControls.Print].IsSelected = true;
        }
    }
}
