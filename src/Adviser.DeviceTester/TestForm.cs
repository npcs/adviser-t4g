﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Advisor.xUSBProtocol;
using System.Data;
using System.Data.SqlClient;

namespace Adviser.DeviceTester
{
    public partial class TestForm : Form
    {
        private bool _isUSBEventTimerActivated = false;
        private const int GuidAddress = 8160;
        //private string connectionString = "Data Source=(local);Initial Catalog=AdviserServerDB;Integrated Security=true";
        private string connectionString = @"Data Source=.\SQLEXPRESS;Database=AdviserDB;Integrated Security=True;MultipleActiveResultSets=True";

        private xUSBProtocol connector = new Advisor.xUSBProtocol.xUSBProtocol();

        public TestForm()
        {
            InitializeComponent();
        }



        private byte[] GetVersion()
        {
            byte[] versionArray = new byte[16];
            for (int i = 0; i <= 15; i++)
            {
                versionArray[i] = 0;
            }
            versionArray[0] = 1;
            return versionArray;
        }

        /// <summary>
        /// Check if guid exists in the database;
        /// </summary>
        /// <param name="guid">guid to check</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>Null if there is an error; True if guid exists in the database; False if guid does not exists in the database.</returns>
        public bool? IsGuidExitsInDB(Guid guid, ref string errorMessage)
        {
            bool? returnValue = null;

            string queryString =
                "SELECT ID from Devices "
                    + "WHERE ID = @inputID;";

            Guid paramValue = guid;


            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@inputID", paramValue);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    returnValue = reader.HasRows;
                    reader.Close();
                }
                catch (Exception ex)
                {
                    returnValue = null;
                    errorMessage += "Database error: " + ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }
            return returnValue;
        }

        public bool? WriteGuidToDB(Guid guid, ref string errorMessage)
        {
            bool? returnValue = null;

            string queryString =
                "insert into Devices (ID,Version,ManufDate,Status) VALUES (@ID,@Version,@ManufDate,@Status)";

            Guid paramID = guid;
            string paramVersion = "0.0.0";
            byte[] version = GetVersion();
            paramVersion = String.Format("{0}.{1}.{2}", version[0], version[1], version[2]);
            DateTime paramManufDate = DateTime.Now.ToUniversalTime();
            string paramStatus = "Available";

            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ID", paramID);
                command.Parameters.AddWithValue("@Version", paramVersion);
                command.Parameters.AddWithValue("@ManufDate", paramManufDate);
                command.Parameters.AddWithValue("@Status", paramStatus);

                try
                {
                    connection.Open();
                    int rowsAffected = command.ExecuteNonQuery();

                    if (rowsAffected == 1)
                    {
                        returnValue = true;
                    }
                    else
                    {
                        errorMessage += "SQL Insert affected rows: " + rowsAffected.ToString() + " SQL: " + command.ToString();
                        returnValue = false;
                    }
                }
                catch (Exception ex)
                {
                    returnValue = null;
                    errorMessage += "Database error: " + ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Reads guid from the board.
        /// </summary>
        /// <param name="boardGuid">guiud on the board</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>True if read operation is successfull</returns>
        private bool ReadGuidFromEEprom(out Guid boardGuid, ref string errorMessage)
        {
            bool returnValue = false;

            byte[] data = new byte[16];

            boardGuid = Guid.Empty;
            if (this.connector.readEepromPacket(GuidAddress, 16, ref data) == xUSBProtocol.Status.success)
            {
                boardGuid = new Guid(data);
                returnValue = true;
            }
            else
            {
                returnValue = false;
                errorMessage += "Error: Cannot read guid from EE Prom. ";
            }
            return returnValue;
        }

        private bool ReadVersionFromEEprom(out byte[] boardVersion, ref string errorMessage)
        {
            bool returnValue = false;

            boardVersion = new byte[16];

            if (this.connector.readEepromPacket(GuidAddress + 16, 16, ref boardVersion) == xUSBProtocol.Status.success)
            {
                returnValue = true;
            }
            else
            {
                returnValue = false;
                errorMessage += "Error: Cannot read version from EE Prom. ";
            }
            return returnValue;
        }

        private bool WriteAndValidateVersionToEEProm(byte[] version, ref string errorMessage)
        {
            bool returnValue = false;

            byte[] boardVersion = new byte[16];

            if (this.connector.writeEepromPacket(GuidAddress + 16, 16, version) == xUSBProtocol.Status.success)
            {
                if (ReadVersionFromEEprom(out boardVersion, ref errorMessage))
                {
                    if (AreEqual(boardVersion, version))
                    {
                        returnValue = true;
                    }
                    else
                    {
                        errorMessage += "Version did not pass validation. Requsted version is not euqal to written one.";
                    }
                }
            }
            else
            {
                errorMessage += "Cannot write version to the board.";
            }
            return returnValue;
        }

        /// <summary>
        /// Write guid to the board and  validate it
        /// </summary>
        /// <param name="guid">guid to write</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>true if write and validate operations are successful; false otherwise</returns>
        private bool WriteAndValidateGuidToEEProm(Guid guid, ref string errorMessage)
        {
            bool returnValue = false;
            byte[] data = new byte[16];
            byte[] GUIDByte = null;
            GUIDByte = guid.ToByteArray();

            for (byte i = 0; i <= 15; i++)
            {
                data[i] = GUIDByte[i];
            }

            Guid guidFromBoard;
            if (this.connector.writeEepromPacket(GuidAddress, 16, data) == xUSBProtocol.Status.success)
            {
                if (ReadGuidFromEEprom(out guidFromBoard, ref errorMessage))
                {
                    if (guidFromBoard.Equals(guid))
                    {
                        returnValue = true;
                    }
                    else
                    {
                        errorMessage += "Guid did not pass validation. Requsted Guid is not euqal to written one.";
                    }
                }
            }
            else
            {
                errorMessage += "Cannot write Guid to the board.";
            }

            return returnValue;
        }

        private string ConvertToString(byte[] bytes)
        {
            string returnValue = "";
            for (int i = 0; i < bytes.Length; i++)
            {
                returnValue += Environment.NewLine + "[" + i.ToString() + "] " + bytes[i].ToString();
            }
            return returnValue;
        }

        public bool AreEqual(byte[] bytes1, byte[] bytes2)
        {
            bool returnValue = true;
            if (bytes1.Length == bytes2.Length)
            {
                for (int i = 0; i < bytes1.Length; i++)
                {
                    if (bytes1[i] != bytes2[i])
                    {
                        returnValue = false;
                        break;
                    }
                }
            }
            else
            {
                returnValue = false;
            }
            return returnValue;
        }




        void OnDeviceChange()
        {
            //Purpose : Called when a WM_DEVICECHANGE message has arrived, 
            // : indicating that a device has been attached or removed. 

            //Accepts : m - a message with information about the device 

            //just see if it's our device 
            //find out which image is selected 
            this.connector.ReInitialize();
            GetFirmwareVersion();
        }

        private void GetFirmwareVersion()
        {
            {
                string myVersion = "";
                xUSBProtocol.Status status = default(xUSBProtocol.Status);
                status = this.connector.version(ref myVersion);
                if (status == xUSBProtocol.Status.success)
                {

                    HisrotyTextBlock.Text += Environment.NewLine + "Firmware version: " + myVersion;

                }
                else
                {

                    HisrotyTextBlock.Text += Environment.NewLine + "Firmware version call failed ";

                }
            }

        }

        protected override void WndProc(ref System.Windows.Forms.Message m)
        {

            //Purpose : Overrides WndProc to enable checking for and handling 
            // : WM_DEVICECHANGE(messages) 

            //Accepts : m - a Windows Message 
            int WM_DEVICECHANGE = 0x219;

            try
            {
                //The OnDeviceChange routine processes WM_DEVICECHANGE messages. 
                if (m.Msg == WM_DEVICECHANGE)
                {
                    //Activate the timer 
                    if (!this._isUSBEventTimerActivated)
                    {
                        this._isUSBEventTimerActivated = true;
                        this.USBEventTimer.Interval = 1500;
                        this.USBEventTimer.Start();
                    }
                }

                //Let the base form process the message. 

                base.WndProc(ref m);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Call HandleException(Me.Name, ex) 
        }

        private void WriteGuidButton_Click(object sender, EventArgs e)
        {

            Guid newGuid = Guid.NewGuid();
            Guid boardGuid;
            string errorMessage = "";
            string Message = Environment.NewLine + "Time: " + DateTime.Now.ToLongTimeString() + " ";

            //-read guid from the board
            if (!ReadGuidFromEEprom(out boardGuid, ref errorMessage))
            {
                Message += errorMessage;
            }
            else
            {
                Message += "Board old guid = " + boardGuid.ToString() + ".";

                bool? isGuidExitsInDB = IsGuidExitsInDB(boardGuid, ref errorMessage);

                if (isGuidExitsInDB == true)
                {
                    Message += "Warning! Board has a Guid that exists in the Database " + boardGuid.ToString() + " ! new guid is not saved to the board.";
                }
                if (isGuidExitsInDB == null)
                {
                    Message += "isGuidExitsInDB error: " + errorMessage;
                }

                if (isGuidExitsInDB == false)
                {
                    //-guid does not exists in the database. write new guit to the board
                    if (WriteAndValidateGuidToEEProm(newGuid, ref errorMessage))
                    {
                        Message += " New Guid on the board = " + newGuid.ToString();
                        bool? isGuidWrittenToDB = WriteGuidToDB(newGuid, ref errorMessage);
                        if (isGuidWrittenToDB == true)
                        {
                            Message += " New guid ( " + newGuid.ToString() + " ) is written to the DB. ";
                        }
                        else
                        {
                            Message += errorMessage;
                        }
                    }
                    else
                    {
                        Message += errorMessage;
                    }
                }
            }
            HisrotyTextBlock.Text += Message;

            WriteVersionButton_Click(null, null);
        }

        private void ReadAndCheckButton_Click(object sender, EventArgs e)
        {
            string Message = Environment.NewLine + "Time: " + DateTime.Now.ToLongTimeString() + ". ";
            string errorMessage = "";
            Guid boardGuid;
            if (ReadGuidFromEEprom(out boardGuid, ref errorMessage))
            {
                Message += "Guid on the board is: " + boardGuid.ToString();
                bool? isGuidExitsInDB = IsGuidExitsInDB(boardGuid, ref errorMessage);
                if (isGuidExitsInDB == true)
                {
                    Message += " Guid exists in the database.";
                }
                if (isGuidExitsInDB == false)
                {
                    Message += " Guid DOES NOT exist in the database.";
                }
                if (isGuidExitsInDB == null)
                {
                    Message += errorMessage;
                }
            }
            else
            {
                Message += errorMessage;
            }

            HisrotyTextBlock.Text += Message;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {

            HisrotyTextBlock.Text = "";
        }

        private void WriteVersionButton_Click(object sender, EventArgs e)
        {
            string Message = Environment.NewLine + "Time: " + DateTime.Now.ToLongTimeString() + " ";
            string errorMessage = "";
            byte[] newVersion = GetVersion();
            if (WriteAndValidateVersionToEEProm(newVersion, ref errorMessage))
            {
                Message += "Version is successfully written and validated. Version: " + ConvertToString(newVersion);
            }
            else
            {
                Message += errorMessage;
            }
            HisrotyTextBlock.Text += Message;
        }

        private void ReadButton_Click(object sender, EventArgs e)
        {
            string Message = Environment.NewLine + "Time: " + DateTime.Now.ToLongTimeString() + " ";
            string errorMessage = "";
            Guid boardGuid;
            if (ReadGuidFromEEprom(out boardGuid, ref errorMessage))
            {
                Message += "Guid on the board is: " + boardGuid.ToString();
            }
            else
            {
                Message += errorMessage;
            }

            HisrotyTextBlock.Text += Message;
        }

        private void ReadVersionButton_Click(object sender, EventArgs e)
        {
            string Message = Environment.NewLine + "Time: " + DateTime.Now.ToLongTimeString() + " ";
            string errorMessage = "";
            byte[] boardVersion;
            if (ReadVersionFromEEprom(out boardVersion, ref errorMessage))
            {
                Message += "Version on the board is: " + ConvertToString(boardVersion);
            }
            else
            {
                Message += errorMessage;
            }
            HisrotyTextBlock.Text += Message;
        }

        private void USBEventTimer_Tick(object sender, EventArgs e)
        {
            _isUSBEventTimerActivated = false;

            USBEventTimer.Stop();

            OnDeviceChange();
        }

        private void GetFirmwareVersionButton_Click(object sender, EventArgs e)
        {
            GetFirmwareVersion();
        }

        private void TestForm_Load(object sender, EventArgs e)
        {

            GetFirmwareVersion();
        }

        private void btnDeleteGuid_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("This will delete GUID from device and it will be lost.\n " 
                + " Make sure to delete guid from the database.\n" 
            + "Are you sure you want to continue?", "Warning", MessageBoxButtons.YesNo);
            string errorMessage = "";
            string message = "";
            if(result == DialogResult.Yes)
            {
                WriteGuidButton_Click(null, null);
                WriteVersionButton_Click(null, null);
            }

        }

    }
}
