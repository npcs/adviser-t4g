﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Adviser.Modules.PatientDatabase
{
    /// <summary>
    /// Interaction logic for PatientDatabaseView.xaml
    /// </summary>
    public partial class PatientsView : UserControl, IPatientsView
    {
        public PatientsView()
        {
            InitializeComponent();
        }

        public void SetLeftView(object view)
        {
            this.LeftPanel.Content = view;
        }

        public void SetRightView(object view)
        {
            this.RightPanel.Content = view;
        }

    }
}
