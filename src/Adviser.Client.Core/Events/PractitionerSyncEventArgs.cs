﻿using System;

namespace Adviser.Client.Core.Events
{
    [Serializable]
    public class PractitionerSyncEventArgs : EventArgs
    {
        public string Email { get; set; }
    }
}
