﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Adviser.Modules.PatientDatabase
{
    [ValueConversion(typeof(Boolean), typeof(Boolean))]
    public class BooleanNegationConverter : IValueConverter
    {
        private static readonly BooleanNegationConverter _instance = new BooleanNegationConverter();

        static BooleanNegationConverter() { }

        public static BooleanNegationConverter Instance
        {
            get { return _instance; }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !System.Convert.ToBoolean(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !System.Convert.ToBoolean(value);
        }

    }
}
