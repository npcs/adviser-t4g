﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Wpf;
using Microsoft.Practices.Composite.Events;

using Adviser.Common.Domain;
using Adviser.Client.Core.Services;
using Adviser.Client.DataModel;

namespace Adviser.Client.Core.Presentation
{
    /// <summary>
    /// Holds data shared between modules during Adviser desktop client session.
    /// Must be initialized and used as singleton.
    /// </summary>
    public class AppointmentSession : ViewModelBase
    {
        private Dictionary<string, object> _dictionary = new Dictionary<string, object>();
        private IDBService _dbService;
        private Practitioner _practitioner;
        private Patient _patient;
        private Visit _visit;

        public AppointmentSession(
            IDBService service,
            IEventAggregator eventAggregator,
            IUnityContainer container)
        {
            _dbService = service;

            //Practitioner = _dbService.RetrivePractitioner(new System.Guid(ConfigurationManager.AppSettings["defaultPractitionerId"]));
            AllOrgans = _dbService.GetAllOrgans().ToList();
            AllSymptoms = _dbService.GetAllSymptoms().ToList();
            AllConditions = _dbService.GetAllConditions().OrderByDescending(c => c.OrderKey).ToList();

            FrontElectrodes = _dbService.GetElectrodes(true).ToList();
            BackElectrodes = _dbService.GetElectrodes(false).ToList();

        }

        /// <summary>
        /// Last connected device information.
        /// </summary>
        public DeviceInformation Device
        {
            get;
            set;
        }

        /// <summary>
        /// Current logged in practitioner.
        /// </summary>
        public Practitioner Practitioner
        {
            get
            {
                return _practitioner;
            }
            set
            {
                _practitioner = value;
                RaisePropertyChangedEvent("Practitioner");
            }
        }

        /// <summary>
        /// Current selected patient.
        /// </summary>
        public Patient Patient
        {
            get
            {
                return _patient;
            }
            set
            {
                if (value != _patient)
                    Visit = null;

                _patient = value;
                RaisePropertyChangedEvent("Patient");
            }
        }


        /// <summary>
        /// Current selected visit.
        /// </summary>
        public Visit Visit
        {
            get
            {
                return _visit;
            }
            set
            {
                _visit = value;
                RaisePropertyChangedEvent("Visit");
            }
        }

        public bool HasDiagnostics
        {
            get
            {
                if (Visit != null && Visit.MeasurementData != null)
                    return true;
                else
                    return false;
            }
        }

        public bool HasResults
        {
            get
            {
                if (HasDiagnostics)
                {
                    if (Visit.ExamResult != null)
                        return true;
                }

                return false;
            }
        }

        /// <summary>
        /// All organs.
        /// </summary>
        public List<Organ> AllOrgans
        {
            get;
            set;
        }


        /// <summary>
        /// Front Electrodes
        /// </summary>
        public List<Electrode> FrontElectrodes
        {
            get;
            set;
        } 
        
        /// <summary>
        /// Back Electrodes
        /// </summary>
        public List<Electrode> BackElectrodes
        {
            get;
            set;
        }


        /// <summary>
        /// Exposes all symptoms
        /// </summary>
        public List<Symptom> AllSymptoms
        {
            get;
            set;
        }

        /// <summary>
        /// Exposes all conditions.
        /// </summary>
        public List<Condition> AllConditions
        {
            get;
            set;
        }

        /// <summary>
        /// Dictionary of shared objects.
        /// </summary>
        public IDictionary<string, object> Objects
        {
            get
            {
                return _dictionary;
            }
        }
    }
}
