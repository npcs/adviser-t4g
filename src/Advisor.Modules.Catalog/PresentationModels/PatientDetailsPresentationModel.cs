﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Advisor.Modules.Catalog.PresentationModels;
using Advisor.Common.DataModel;
using Advisor.Common.Presentation;

namespace Advisor.Modules.Catalog.PresentationModels
{
    public class PatientDetailsPresentationModel : ViewModelBase
    {
        private Patient _patient;

        public PatientDetailsPresentationModel(Patient patient)
        {
            _patient = patient;
        }
    }
}
