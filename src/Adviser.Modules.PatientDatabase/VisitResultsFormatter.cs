﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace Adviser.Modules.PatientDatabase
{
    [ValueConversion(typeof(Byte[]), typeof(String))]
    public class VisitResultsFormatter : IValueConverter
    {
        public VisitResultsFormatter() { }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string result = String.Empty;
            string fieldName = (string)parameter;
            if (fieldName == "MeasurementData")
            {
                if (value == null)
                {
                    result = "Not done";
                }
                else
                {
                    result = "Done";

                    //Byte[] data = (Byte[])value;
                    //if(data.Length > 0)
                    //    result = "Done";
                    //else
                    //    result = "Not done";
                }
            }
            else
            {
                if (value == null)
                {
                    result = "Not ready";
                }
                else
                {
                    Byte[] data = (Byte[])value;
                    if (data.Length > 0)
                        result = "Ready";
                    else
                        result = "Not ready";
                }
            }

            return result;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("Method is not implemented.");
        }
    }
}
