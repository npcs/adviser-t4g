﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Common.Presentation
{
    public class RegionNames
    {
        public const string ToolbarRegion = "ToolbarRegion";
        public const string NavigationRegion = "NavigationRegion";
        public const string ContentRegion = "ContentRegion";
        public const string StatusBarRegion = "StatusBarRegion";
        public const string PrintRegion = "PrintRegion";

        //- old regions, will be deprecated
        public const string MainToolbarRegion = "MainToolbarRegion";
        public const string MainRegion = "MainRegion";
        public const string UpperRightRegion = "UpperRightRegion";
        public const string LowerLeftRegion = "LowerLeftRegion";
        public const string LowerRightRegion = "LowerRightRegion";
        public const string DetailsRegion = "DetailsRegion";
    }
}
