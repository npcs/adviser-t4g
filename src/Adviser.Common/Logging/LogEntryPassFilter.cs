﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Common.Logging
{
    public class LogEntryPassFilter : LogEntryFilter
    {
        /// <summary>
        /// Create a new instance of LogEntryPassFilter.
        /// </summary>
        public LogEntryPassFilter()
            : base()
        {
        }
        /// <summary>
        /// Determine if logEntry should pass through the filter.
        /// </summary>
        /// <param name="logEntry">The LogEntry being tested.</param>
        /// <returns>Always return true.</returns>
        protected override bool CanPass(ILogEntry logEntry)
        {
            return true;
        }
    }
}
