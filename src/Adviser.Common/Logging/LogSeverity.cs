﻿using System;

namespace Adviser.Common.Logging
{
    public enum LogSeverity
    {
        Debug = 1,
        Info = 2,
        Status = 3,
        Warning = 4,
        Error = 5
    }
}
