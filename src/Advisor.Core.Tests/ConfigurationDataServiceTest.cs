﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Adviser.Client.Core.Services;
using Adviser.Common.Domain;
using Adviser.Common.Collections;

namespace Advisor.Core.Tests
{
    /// <summary>
    /// Summary description for ConfigurationDataService
    /// </summary>
    [TestClass]
    public class ConfigurationDataServiceTest
    {
        public ConfigurationDataServiceTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestSaveLoadXmlSettings()
        {
            string fileName = @"C:\temp\MeasurementSettings.xml";
            XmlDictionary<string, string> xmlBefore = new XmlDictionary<string, string>();
            MeasurementSettings settings = new MeasurementSettings();
            settings.DeviceSamplingRate = 5;
            settings.IsFrontPositive = true;
            settings.PairMeasurementDuration = 100;
            settings.FrontLeads = 7;
            settings.BackLeads = 7;
            settings.ShuntResistor = 100;
            settings.Voltage = 3.3;
            settings.Calibration = false;
            settings.ElectrodesConnectionMap = new List<PairConnection>(49);

            for (byte x = 0; x < 7; ++x)
            {
                for (byte y = 0; y < 7; ++y)
                {
                    PairConnection pair = new PairConnection();
                    pair.Front = x;
                    pair.Back = y;
                    settings.ElectrodesConnectionMap.Add(pair);
                }
            }

            xmlBefore.Add("DeviceSamplingRate", settings.DeviceSamplingRate.ToString());
            xmlBefore.Add("IsFrontPositive", settings.IsFrontPositive.ToString());
            xmlBefore.Add("PairMeasurementDuration", settings.PairMeasurementDuration.ToString());
            xmlBefore.Add("FrontLeads", settings.FrontLeads.ToString());
            xmlBefore.Add("BackLeads", settings.BackLeads.ToString());
            xmlBefore.Add("ShuntResistor", settings.ShuntResistor.ToString());
            xmlBefore.Add("Voltage", settings.Voltage.ToString());
            xmlBefore.Add("Calibration", settings.Calibration.ToString());

            ConfigurationDataService service = new ConfigurationDataService();
            service.SaveXmlSettings(xmlBefore, fileName);

            XmlDictionary<string, string> xmlAfter = service.LoadXmlSettings(fileName);

            foreach (string key in xmlBefore.Keys)
            {
                Assert.IsTrue(xmlAfter.ContainsKey(key));
                Assert.AreEqual(xmlAfter[key], xmlBefore[key]);
            }
        }
    }
}
