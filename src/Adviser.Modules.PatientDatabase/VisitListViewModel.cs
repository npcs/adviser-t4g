﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Configuration;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Wpf;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Wpf.Commands;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Client.DataModel;
    using Adviser.Client.Core.Presentation;
    using Adviser.Client.Core.Events;
    using Adviser.Client.Core.Services;
    using Adviser.Common.Logging;

    /// <summary>
    /// Interface for the Visits view presentation model.
    /// </summary>
    public interface IVisitListViewModel
    {
        ObservableCollection<Visit> Visits { get; }
        void OnVisitSelected(object sender, DataEventArgs<Visit> e);
        
        DelegateCommand<object> AddVisitCommand { get; }
        DelegateCommand<object> DeleteVisitCommand { get; }
    }

    /// <summary>
    /// Presentation model for the Visits master/detail view (VisitListView + VisitDetailsView).
    /// </summary>
    public class VisitListViewModel : ViewModelBase, IVisitListViewModel
    {
        private IDBService _dbService;
        private IEventAggregator _eventAggregator;
        private ILogger _logService;
        private ObservableCollection<Visit> _visits;
        private Visit _selectedVisit;
        private AppointmentSession _session;

        public VisitListViewModel(
            IDBService service,
            IEventAggregator eventAggregator,
            IUnityContainer container,
            ILogger logService,
            VisitDetailsPrintView printableView)
        {
            _dbService = service;
            _eventAggregator = eventAggregator;
            _logService = logService;
            _session = container.Resolve<AppointmentSession>();

            _eventAggregator.GetEvent<PatientSelectedEvent>().Subscribe(OnPatientSelected);

            AddVisitCommand = new DelegateCommand<object>(AddVisit);
            DeleteVisitCommand = new DelegateCommand<object>(DeleteVisit);
        }

        public IVisitListView MasterView { get; set; }
        public IVisitDetailsView DetailsView { get; set; }

        public DelegateCommand<object> AddVisitCommand { get; private set; }
        public DelegateCommand<object> DeleteVisitCommand { get; private set; }

        /// <summary>
        /// Exposes collection of visits.
        /// </summary>
        public ObservableCollection<Visit> Visits
        {
            get { return _visits; }
            private set
            {
                _visits = value;
                RaisePropertyChangedEvent("Visits");
            }
        }

        public Visit SelectedVisit
        {
            get
            {
                return _selectedVisit;
            }
            set
            {
                _selectedVisit = value;
                RaisePropertyChangedEvent("SelectedVisit");
            }
        }

        public void OnVisitSelected(object sender, DataEventArgs<Visit> e)
        {
            //if (SelectedVisit == e.Value)
            //    return;

            _session.Visit = e.Value;
            //SelectedVisit = e.Value;

            _eventAggregator.GetEvent<VisitSelectedEvent>().Publish(e.Value);
        }

        private void OnPatientSelected(Patient patient)
        {
            //if (patient.Visits.Count == 0)
            //    return;

            if (!patient.Visits.IsLoaded)
                patient.Visits.Load();

            Visits = new ObservableCollection<Visit>(patient.Visits);
        }

        private void AddVisit(object parameters)
        {
            try
            {
                Visit visit = _dbService.CreateNewVisit(_session.Patient);
                Visits.Add(visit);
                SelectedVisit = visit;
            }
            catch (Exception ex)
            {
                string errMsg = String.Format("Error occured while adding visit.");
                System.Windows.MessageBox.Show(errMsg, "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                _logService.LogError(ex, errMsg);
            }
        }

        private void DeleteVisit(object parameters)
        {
            try
            {
                if (SelectedVisit != null)
                {
                    SelectedVisit.Prescriptions.Load();
                    SelectedVisit.OrganExaminations.Load();
                    SelectedVisit.Symptoms.Load();

                    _dbService.DeleteObject(SelectedVisit);
                    _visits.Remove(SelectedVisit);
                }

                if (_visits.Count != 0)
                {
                    SelectedVisit = _visits.FirstOrDefault();
                }
                else
                {
                    SelectedVisit = null;
                    _session.Visit = null;
                    _eventAggregator.GetEvent<VisitSelectedEvent>().Publish(null);
                }
            }
            catch (Exception ex)
            {
                string errMsg = String.Format("Error occured while deleting visit {0}", SelectedVisit.ID.ToString());
                System.Windows.MessageBox.Show(errMsg, "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                _logService.LogError(ex, errMsg);
            }
        }
    }
}
