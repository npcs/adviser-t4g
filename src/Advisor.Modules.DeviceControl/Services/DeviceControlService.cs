﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Events;

using Adviser.Common.Domain;
using Adviser.Common.Collections;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Services;
using Adviser.Client.Core.DeviceControl;

namespace Advisor.Modules.DeviceControl.Services
{
    /// <summary>
    /// Provides service facade for controlling measurement device.
    /// </summary>
    public class DeviceControlService : IDeviceControlService
    {
        IEventAggregator _eventAggregator;
        IDeviceManager _deviceManager;
        private IConfigurationDataService _configService;

        public DeviceControlService(
            IEventAggregator eventAggregator, 
            IDeviceManager deviceManager,
            IConfigurationDataService configService)
        {
            _eventAggregator = eventAggregator;
            _deviceManager = deviceManager;
            _configService = configService;

            _deviceManager.MeasurementProgress += new MeasurementProgressEventHandler(OnMeasurementProgress);
            _deviceManager.MeasurementCompleted += new MeasurementCompletedEventHandler(OnMeasurementCompleted);
        }

        //public event EventHandler<Advisor.Common.DataModel.MeasurementEventArgs> MeasurementProgress;
        //public event EventHandler<Advisor.Common.DataModel.MeasurementEventArgs> MeasurementCompleted;

        public void StartMeasurement()
        {
            _deviceManager.StartMeasurement();
        }

        public void StopMeasurement()
        {
            _deviceManager.StopMeasurement();
        }

        /// <summary>
        /// Initializes device with the current settings.
        /// </summary>
        public void InitializeDevice()
        {
            DeviceSettings deviceSettings = _configService.GetDeviceSettings();
            MeasurementSettings measurementSettings = _configService.GetMeasurementSettings();

            if (_deviceManager.IsStopped)
                _deviceManager.Initialize(deviceSettings, measurementSettings);
        }

        public DeviceInformation GetDeviceInfo()
        {
            DeviceInformation info = new DeviceInformation();
            info.IsConnected = _deviceManager.IsConnected;
            if (info.IsConnected)
            {
                info.IsSet = _deviceManager.IsSet;
                info.ID = _deviceManager.GetDeviceId();
                info.Version = _deviceManager.GetDeviceVersion();
                info.Voltage = _deviceManager.GetBusVoltage();
            }

            return info;
        }

        public void ReleaseDevice()
        {
            _deviceManager.Release();
        }

        private void OnMeasurementProgress(object sender, MeasurementProgressEventArgs e)
        {
            _eventAggregator.GetEvent<MeasurementProgressEvent>().Publish(e);
        }

        private void OnMeasurementCompleted(object sender, MeasurementEventArgs e)
        {
            _eventAggregator.GetEvent<MeasurementCompletedEvent>().Publish(e);
        }

        public void ReInitialize()
        {
            _deviceManager.ReInitialize();
        }

    }
}
