﻿using System;
using Adviser.Client.DataModel;

namespace Adviser.Client.Core.Events
{
    [Serializable]
    public class PractitionerSyncCompletedEventArgs : EventArgs
    {
        public Practitioner Practitioner { get; set; }
        public string Message { get; set; }
    }
}
