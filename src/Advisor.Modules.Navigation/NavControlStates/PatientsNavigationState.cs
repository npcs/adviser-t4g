﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Events;

using Adviser.Common;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Presentation;

namespace Advisor.Modules.Navigation
{
    public class PatientsNavigationState : NavigationState
    {
        private IEventAggregator _eventAggregator;
        private AppointmentSession _session;

        public PatientsNavigationState(IEventAggregator eventAggregator, AppointmentSession session)
        {
            _eventAggregator = eventAggregator;
            _session = session;
            _eventAggregator.GetEvent<PatientSelectedEvent>().Subscribe(OnPatientSelectedEvent);
            _eventAggregator.GetEvent<NavigationRequestEvent>().Subscribe(OnNavigationRequestEvent, 
                ThreadOption.PublisherThread, 
                false, 
                e => e.EventName == NavigationEventNames.Visits);
        }

        protected override void SetControls()
        {
            ControlMap[NavigationControls.Patients].IsSelected = true;
            if (_session.Patient != null)
                ControlMap[NavigationControls.Visits].IsEnabled = true;
        }

        private void OnPatientSelectedEvent(Patient p)
        {
            SetControls();
        }

        private void OnNavigationRequestEvent(NavigationEventArgs e)
        {
            TransitionState(e.EventName);
        }

    }
}
