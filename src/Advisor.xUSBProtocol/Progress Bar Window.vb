Public Class Progress_Bar_Window
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "


    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents progressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents progressBarLabel As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.progressBar = New System.Windows.Forms.ProgressBar
        Me.progressBarLabel = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'progressBar
        '
        Me.progressBar.Location = New System.Drawing.Point(16, 32)
        Me.progressBar.Name = "progressBar"
        Me.progressBar.Size = New System.Drawing.Size(296, 16)
        Me.progressBar.Step = 1
        Me.progressBar.TabIndex = 0
        '
        'progressBarLabel
        '
        Me.progressBarLabel.Location = New System.Drawing.Point(16, 10)
        Me.progressBarLabel.Name = "progressBarLabel"
        Me.progressBarLabel.Size = New System.Drawing.Size(296, 16)
        Me.progressBarLabel.TabIndex = 1
        Me.progressBarLabel.Text = "Label1"
        '
        'Progress_Bar_Window
        '
        Me.ClientSize = New System.Drawing.Size(320, 54)
        Me.ControlBox = False
        Me.Controls.Add(Me.progressBarLabel)
        Me.Controls.Add(Me.progressBar)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "Progress_Bar_Window"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Updating USB Firmware ..."
        Me.ResumeLayout(False)

    End Sub

#End Region


    Public Sub New(ByVal windowText As String, ByVal maximum As Integer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        progressBarLabel.Text = windowText
        progressBar.Minimum = 1
        progressBar.Maximum = maximum
        progressBar.Value = 1

    End Sub


    Public Sub incrementValue()
        progressBar.PerformStep()
        Me.Refresh()
    End Sub

    Public Sub setText(ByVal str As String)
        progressBar.Text = str
        Me.Refresh()
    End Sub
End Class
