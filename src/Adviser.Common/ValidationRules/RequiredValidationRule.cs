﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Globalization;

namespace Adviser.Common.ValidationRules
{
    public class RequiredValidationRule : ValidationRule
    {


        public override ValidationResult Validate(object value,
          CultureInfo cultureInfo)
        {
            if (value == null || ((String)value).Trim().Equals(String.Empty))
                return new ValidationResult(false, "The field is mandatory.");
            else
                return ValidationResult.ValidResult;

        }

    }
}
