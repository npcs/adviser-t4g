﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Configuration;
using System.IO;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;

namespace Adviser.Modules.ServerComm
{
    using Adviser.Client.DataModel;
    using DataContracts = Adviser.Server.Services.Contracts;
    using EntityModel = Adviser.Client.DataModel;
    using Adviser.Client.Core.Services;
    using Adviser.Client.Core.Events;
    using Adviser.Common.Utils;
    using Adviser.Client.Core.Interfaces;
    using Adviser.Client.Core.Presentation;
    using Adviser.Common.Logging;
    using System.Net;

    /// <summary>
    /// This class handles all communications with server.
    /// </summary>
    public class AdviserServiceAgent : IAdviserServiceAgent
    {
        private IEventAggregator _eventAggregator;
        private string _msgConnectDevice = "";
        private IDBService _dbService;
        private IConfigurationDataService _configService;
        private ILogger _logService;
        private AppointmentSession _session;

        public AdviserServiceAgent(
            AppointmentSession session, 
            IEventAggregator eventAggregator, 
            IDBService service, 
            IConfigurationDataService configService,
            ILogger logService) 
        {
            _session = session;
            _logService = logService;
            _eventAggregator = eventAggregator;
            _dbService = service;
            _configService = configService;
            _msgConnectDevice = "You must connect Adviser device to the computer (you don't need to turn on the power).\n" +
                    "Please connect Adviser to the USB port of the computer and try again.";

#if DEBUG
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
#endif

            //throw new ApplicationException("Test exception!");
        }

        private bool IsDeviceConnected()
        {
            if (_session.Device == null
//#if !DEBUG //- allow default device for testing
// || _session.Device.ID == Guid.Empty
//#endif
                )
                return false;
            else
                return true;
        }

        private AdviserServiceClient ConfigureClient()
        {
            string endpointConfig = _configService.GetAppSetting("serviceEndpoint");
            AdviserServiceClient client = new AdviserServiceClient(endpointConfig);
            client.ChannelFactory.Credentials.UserName.UserName = _session.Device.ID.ToString();
            //client.ChannelFactory.Credentials.UserName.Password = _session.Practitioner.Password;
            return client;
        }

        /// <summary>
        /// Sends measurement data and requests results from the server.
        /// </summary>
        /// <param name="visit"></param>
        /// <returns></returns>
        public ResultsReceivedEventArgs GetResults(Visit visit)
        {
            ResultsReceivedEventArgs responseEventArgs = new ResultsReceivedEventArgs();
            responseEventArgs.IsSuccess = false;

            if (!IsDeviceConnected())
            {
                responseEventArgs.Message = _msgConnectDevice;
                return responseEventArgs;
            }

            try
            {
                DataContractMapper mapper = new DataContractMapper();
                DataContracts::Patient outPatient = new DataContracts::Patient();
                outPatient = mapper.FlatCopyToDataContract(visit.Patient, outPatient);
                DataContracts::Visit outVisit = mapper.MapToDataContract(visit);
                outVisit.Deleted = false;
                outPatient.Visits = new DataContracts::Visit[] { outVisit };

                #region utility code to save some datacontracts
                //DataContractSerializer dataContractSerializer = new DataContractSerializer(typeof(DataContracts::Visit));
                //System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(@"c:\temp\Visit.xml", System.Text.Encoding.UTF8);
                //dataContractSerializer.WriteObject(writer, outVisit);
                //writer.Flush();
                //writer.Close();

                //BinaryFormatter fmt = new BinaryFormatter();
                //MemoryStream stream = new MemoryStream();
                //fmt.Serialize(stream, outVisit.OrganExaminations);
                //byte[] binData = stream.ToArray();
                //FileStream file = new FileStream(@"c:\temp\OrganExaminations.bin", FileMode.Create);
                //file.Write(binData, 0, binData.Length);
                //file.Close();
                #endregion

                //- send data to the server and get back results
                DataContracts::Patient patientFromServer = null;
                DataContracts::Visit visitFromServer = null;
                
                using (AdviserServiceClient client = ConfigureClient())
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12;
                    patientFromServer = client.GetExamResults(visit.Patient.Practitioners.ID, _session.Device.ID, outPatient);
                    client.Close();
                }

                if (patientFromServer != null)
                {//- map back from data contract to entity
                    visitFromServer = patientFromServer.Visits[0];
                    visit.ExamResult = visitFromServer.ExamResult;
#if !DIMA_BUILD
                    visit.MeasurementData = new byte[0];
#endif
                    visit.Notes = visitFromServer.Notes;
                    
                    foreach (DataContracts::OrganExamination serverOE in visitFromServer.OrganExaminations)
                    {
                        OrganExamination oe = visit.OrganExaminations.FirstOrDefault(e => e.ID == serverOE.ID);
                        oe.Value = serverOE.Value;
                    }

                    foreach (DataContracts::Prescription inPrescription in visitFromServer.Prescriptions)
                    {
                        Prescription localPresc = visit.Prescriptions.FirstOrDefault(p => p.ID == inPrescription.ID);
                        if (localPresc == null)
                            visit.Prescriptions.Add(mapper.MapToEntity(inPrescription));
                        else
                            localPresc = mapper.FlatCopyToEntity(inPrescription, localPresc);
                    }
                    responseEventArgs.IsSuccess = true;
                    responseEventArgs.VisitData = visit;
                }
            }
            catch (FaultException<DataContracts::AdviserServiceFault> fault)
            {
                System.Diagnostics.Trace.WriteLine(fault.ToString());

                responseEventArgs.IsSuccess = false;
                responseEventArgs.Message = fault.Detail.Message;

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.ToString());
                responseEventArgs.IsSuccess = false;
                responseEventArgs.Message = ex.Message;
            }

            return responseEventArgs;
        }

        /// <summary>
        /// Requests registration in the server database.
        /// </summary>
        /// <param name="practitioner"></param>
        /// <param name="message"></param>
        /// <returns>True if success</returns>
        public bool RegisterPractitioner(Practitioner practitioner, out string message)
        {
            bool isSuccess = false;

            if (!IsDeviceConnected())
            {
                message = _msgConnectDevice;
                return false;
            }

            message = "Registration error. Cannot connect to the server. Make sure the computer is connected to the internet.";
            try
            {
                DataContractMapper mapper = new DataContractMapper();
                DataContracts::Practitioner outPractitioner = new DataContracts::Practitioner();
                outPractitioner = mapper.MapToDataContract(practitioner);

                //- sent data to the server and get back results
                DataContracts::AccountRegistration accountRegistration = null;
                using (AdviserServiceClient client = ConfigureClient())
                {
                    accountRegistration = client.RegisterAccount(outPractitioner, _session.Device.ID);
                    client.Close();
                }

                if (accountRegistration != null)
                {
                    isSuccess = accountRegistration.IsSuccess;
                    message = accountRegistration.MessageText;
                }
            }
            catch (FaultException<DataContracts::AdviserServiceFault> fault)
            {
                System.Diagnostics.Debug.WriteLine(fault.ToString());

                isSuccess = false;
                message = fault.Detail.Message;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());

                isSuccess = false;
                message = ex.Message;
            }

            return isSuccess;
        }

        /// <summary>
        /// Updates practitioner information.
        /// </summary>
        /// <param name="practitioner"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool UpdatePractitioner(Practitioner practitioner, out string message)
        {
            bool isSuccess = false;
            message = String.Empty;

            if (!IsDeviceConnected())
            {
                message = _msgConnectDevice;
                return false;
            }
            AdviserServiceClient client = null;
            try
            {
                DataContractMapper mapper = new DataContractMapper();
                DataContracts::Practitioner outPractitioner = new DataContracts::Practitioner();
                outPractitioner = mapper.MapToDataContract(practitioner);

                //- sent data to the server and get back results
                DataContracts::AccountRegistration response = null;

                //string endpointConfig = _configService.GetAppSetting("serviceEndpoint");
                client = ConfigureClient();
                response = client.UpdateAccount(outPractitioner);

                //using (AdviserServiceClient client = new AdviserServiceClient(endpointConfig))
                //{
                //    response = client.UpdateAccount(outPractitioner);
                //    client.Close();
                //}

                if (response != null)
                {
                    isSuccess = response.IsSuccess;
                    message = response.MessageText;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
            finally
            {
                if(client != null && client.State == CommunicationState.Opened)
                    client.Close();
            }

            return isSuccess;
        }

        public bool RetrievePassword(string email, out string message)
        {
            bool isSuccess = false;

            if (!IsDeviceConnected())
            {
                message = _msgConnectDevice;
                return false;
            }

            try
            {
                DataContracts::RetrievePasswordResult response = null;
                Guid id = Guid.Empty;
                using (AdviserDBEntities dbContext = new AdviserDBEntities())
                {
                    Practitioner practitioner = dbContext.PractitionerSet.FirstOrDefault(p => p.Email == email);
                    if (practitioner != null)
                    {
                        id = practitioner.ID;
                    }
                    else
                    {
                        message = "Your account is not found. Please contact support for assistance.";
                        return false;
                    }
                }

                string endpointConfig = _configService.GetAppSetting("serviceEndpoint");
                using (AdviserServiceClient client = new AdviserServiceClient(endpointConfig))
                {
                    response = client.RetrievePassword(id, email);
                    client.Close();
                }

                isSuccess = response.IsSuccess;
                message = response.Message;
            }
            catch (FaultException<DataContracts::AdviserServiceFault> fault)
            {
                System.Diagnostics.Debug.WriteLine(fault.ToString());

                isSuccess = false;
                message = fault.Detail.Message;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());

                isSuccess = false;
                message = "Error occured during communication with server. Try again later or contact system administrator.";
            }

            return isSuccess;
        }

        /// <summary>
        /// Sends report to the server.
        /// </summary>
        /// <param name="logEntry"></param>
        /// <returns></returns>
        public bool ReportError(AdviserLogEntry logEntry)
        {
            bool result = false;
            DataContractMapper mapper = new DataContractMapper();
            DataContracts::AdviserClientLogEntry outEntry = mapper.MapToDataContract(logEntry);

            try
            {
                string endpointConfig = _configService.GetAppSetting("serviceEndpoint");
                using (AdviserServiceClient client = new AdviserServiceClient(endpointConfig))
                {
                    result = client.ReportError(outEntry);
                    client.Close();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                //_logService["EventLogger"].Log(LogSeverity.Error, ex);
            }

            return result;
        }

        public bool UpdatePatients(Practitioner practitioner, Patient[] patients)
        {
            bool result = false;
            DataContractMapper mapper = new DataContractMapper();
            try
            {
                using (AdviserServiceClient client = ConfigureClient())
                {
                    List<DataContracts::Patient> outPatients = new List<Adviser.Server.Services.Contracts.Patient>();
                    foreach (Patient patient in patients)
                    {
                        outPatients.Add(mapper.MapToDataContract(patient));
                    }
                    result = client.UpdatePatients(practitioner.ID, outPatients.ToArray());
                    client.Close();
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        public bool UpdateVisits(Practitioner practitioner, Visit[] visits)
        {
            bool result = false;
            DataContractMapper mapper = new DataContractMapper();
            try
            {
                using (AdviserServiceClient client = ConfigureClient())
                {
                    List<DataContracts::Visit> outVisits = new List<DataContracts::Visit>();
                    foreach (Visit visit in visits)
                    {
                        outVisits.Add(mapper.MapToDataContract(visit));
                    }
                    //DataContracts::Patient outPatient = mapper.MapToDataContract(patient);
                    result = client.UpdateVisits(practitioner.ID, outVisits.ToArray());
                    client.Close();
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        public bool UpdatePrescriptions(Practitioner practitioner, Prescription[] prescriptions)
        {
            bool result = false;
            DataContractMapper mapper = new DataContractMapper();
            try
            {
                using (AdviserServiceClient client = ConfigureClient())
                {
                    List<DataContracts::Prescription> outRXs = new List<DataContracts::Prescription>();
                    foreach (Prescription p in prescriptions)
                    {
                        outRXs.Add(mapper.MapToDataContract(p));
                    }

                    result = client.UpdatePrescriptions(practitioner.ID, outRXs.ToArray());
                    client.Close();
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
    }
}
