﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Windows.Controls.Primitives;

using Adviser.Common.Logging;
using Adviser.Client.Core.Services;
using Adviser.Modules.PatientDatabase;
using Adviser.Client.Core.Events;

using System.Windows.Threading;
using System.Diagnostics;

namespace AdvisorClient
{
    public class ShellPresenter
    {
        public DelegateCommand<object> MyAccountCommand { get; private set; }

        private DispatcherTimer _timer;

        private IEventAggregator _eventAggregator;
        private IUnityContainer _container;

        public ShellPresenter(
            IShellView view,
            IUnityContainer container,
            IRegionManager regionManager,
            IEventAggregator eventAggregator)
        {
            View = view;
            this._eventAggregator = eventAggregator;
            _container = container;
            View.ClosingApplication += new EventHandler(View_ClosingApplication);
            View.ShellViewLoaded += new EventHandler(View_ShellViewLoaded);
            View.USBChanged += new EventHandler(View_USBChanged);
            View.MyAccountClicked +=new EventHandler(View_MyAccountClicked);
            View.HelpClicked += new EventHandler(View_HelpClicked);
            _timer = new DispatcherTimer();
            _timer.Tick += new EventHandler(OnTimerCallback);
            eventAggregator.GetEvent<CloseApplicationEvent>().Subscribe(CloseApplication);
        }

        void View_HelpClicked(object sender, EventArgs e)
        {
            Process help = new Process();
            help.StartInfo.FileName = "AdviserHelp.chm";
            help.Start();

        }

        void View_USBChanged(object sender, EventArgs e)
        {
            if (!_timer.IsEnabled)
            {
                _timer.Interval = new TimeSpan(0, 0, 2);
               
                _timer.IsEnabled = true;
                _timer.Start();
            }
        }

        private void OnTimerCallback(object source, EventArgs e)
        {
            //_timer.Tick -= new EventHandler(OnTimerCallback);
            _timer.IsEnabled = false;
            _timer.Stop();
            _eventAggregator.GetEvent<USBChangedEvent>().Publish(null);            
        }

        private void CloseApplication(object p)
        {
            _timer.Stop();
            _timer.IsEnabled = false;
            View.CloseApplication();
        }

        void View_ShellViewLoaded(object sender, EventArgs e)
        {
            try
            {
                IPractitionerPresenter practitionerPresenter = _container.Resolve<IPractitionerPresenter>();
                practitionerPresenter.Start();
            }
            catch (Exception ex)
            {
                string message = "Error occured while loading application. Please contact support for assistance if error persists.";
                MessageBox.Show(message, "Application Error", MessageBoxButton.OK);
                ILogger logService = _container.Resolve<ILogger>();
                logService.LogError(ex, message);
            }
            //View.ShowView();
        }

        void View_MyAccountClicked(object sender, EventArgs e)
        {
            IPractitionerPresenter practitionerPresenter = _container.Resolve<IPractitionerPresenter>();
            practitionerPresenter.ShowUpdateAccount(true);
        }

        private void View_ClosingApplication(object sender, EventArgs e)
        {
            try
            {
                // The following code will trigger the LostFocus-Event on any type of control and make it update itself
                IInputElement x = System.Windows.Input.Keyboard.FocusedElement;
                if (x != null)
                {
                    View.DummyField.Focus();
                    x.Focus();
                }
                IDBService _service = _container.Resolve<IDBService>();
                _service.SaveChanges();
            }
            catch (Exception ex)
            {
                string message = "Error occured while exiting application. Please contact support for assistance if error persists.";
                MessageBox.Show(message, "Application Error", MessageBoxButton.OK);
                ILogger logService = _container.Resolve<ILogger>();
                logService.LogError(ex, message);
            }
        }

        public IShellView View { get; set; }
    }
}
