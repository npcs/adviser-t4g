﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.Security.Principal;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Data;
using System.Data.Common;
using System.Data.Objects;
using System.Linq;

using Adviser.Server.DataModel;
using Adviser.Server.Core.Services;
using Adviser.Common.Logging;
using Adviser.Server.Core.Logging;

namespace Adviser.Server.Services
{
    /// <summary>
    /// Validates security token against Adviser database.
    /// </summary>
    public class AdviserUserNamePasswordValidator : UserNamePasswordValidator
    {
        private const string _exceptionMessage = "Authentication failed.";

        /// <summary>
        /// Currently it validates only deviceId as userName.
        /// It is expected to be registered in the database and be in proper status.
        /// </summary>
        /// <param name="userName">UserName token.</param>
        /// <param name="password"></param>
        public override void Validate(string userName, string password)
        {
            string errorMsg = "";
            if (null == userName || userName == String.Empty || userName.Length > 36)
            {
                //TODO: Log it as Security category entry
                errorMsg = "***SecurityTokenException -- DeviceId format is not valid: " + userName;
                LogProvider.Log(LogProvider.CreateLogEntry(null, null, null, null, LogSeverity.Error, LogCategory.Security.ToString(), errorMsg, _exceptionMessage));
                throw new SecurityTokenException(_exceptionMessage);
            }

            Guid deviceId = Guid.Empty;
            try
            {
                deviceId = new Guid(userName);
            }
            catch
            {
                errorMsg = "***SecurityTokenException -- DeviceId is not guid: " + userName;
                LogProvider.Log(LogProvider.CreateLogEntry(null, null, null, null, LogSeverity.Error, LogCategory.Security.ToString(), errorMsg, _exceptionMessage));
                throw new SecurityTokenException(_exceptionMessage);
            }

            using (AdviserDBEntities dbContext = new AdviserDBEntities())
            {
                List<Device> devices = dbContext.GetDeviceByID(deviceId).ToList();
                if (devices.Count == 0)
                {
                    errorMsg = "***SecurityTokenException -- Device does not exist: " + userName;
                    LogProvider.Log(LogProvider.CreateLogEntry(null, null, null, null, LogSeverity.Error, LogCategory.Security.ToString(), errorMsg, _exceptionMessage));
                    throw new SecurityTokenException(_exceptionMessage);
                }

                if (devices[0].Status == DeviceStatus.NotAvailable.ToString())
                {
                    errorMsg = "***SecurityTokenException -- Device is not available: " + userName;
                    LogProvider.Log(LogProvider.CreateLogEntry(null, null, null, null, LogSeverity.Error, LogCategory.Security.ToString(), errorMsg, _exceptionMessage));
                    throw new SecurityTokenException(_exceptionMessage);
                }
            }
        }

        //public override void Validate(string userName, string password)
        //{
        //    if (null == userName || null == password)
        //        throw new ArgumentNullException();

        //    //- validate password to prevent DoS and SQL injection
        //    if (userName.Length > 50)
        //        throw new SecurityTokenException("Invalid username");

        //    using (AdviserDBEntities dbContext = new AdviserDBEntities())
        //    {
        //        DbCommand cmd = dbContext.CreateCommand("GetPassword", CommandType.StoredProcedure, null);
        //        DbParameter param = cmd.CreateParameter();
        //        param.ParameterName = "@Email";
        //        param.Value = userName;
        //        cmd.Parameters.Add(param);
        //        cmd.Connection.Open();
        //        object result = cmd.ExecuteScalar();
        //        if (result == null)
        //            throw new SecurityTokenException("Invalid user");

        //        string serverPwd = (string)result;

        //        if (serverPwd != password)
        //            throw new SecurityTokenException("Invalid password");

        //        cmd.Dispose();
        //    }
        //}
    }
}
