﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Adviser.Common.Logging;

namespace Adviser.Server.Core.Logging
{
    public static class LogProvider
    {
        private static object _lockSync = new object();
        private static string _appName = "AdviserServer";
        private static ILogger _logger;

        static LogProvider()
        {
            lock (_lockSync)
            {
                CompositeLogger<AdviserLogEntry> compLogger = new CompositeLogger<AdviserLogEntry>();
                compLogger.Application = _appName;
                ServerDbLogger<AdviserLogEntry> dbLogger = new ServerDbLogger<AdviserLogEntry>();
                dbLogger.Application = _appName;
                compLogger.AddLogger("ServerDbLogger", dbLogger);
                TraceLogger<AdviserLogEntry> traceLogger = new TraceLogger<AdviserLogEntry>();
                traceLogger.Application = _appName;

                compLogger.AddLogger("TraceLogger", traceLogger);
                _logger = compLogger;
            }
        }

        public static ILogger Logger
        {
            get { return _logger; }
        }

        public static void Log(AdviserLogEntry entry)
        {
            _logger.Log(entry);
        }

        public static AdviserLogEntry CreateLogEntry(Guid? deviceId,
            Guid? practitionerId, 
            Guid? patientId, 
            Guid? visitId, 
            LogSeverity severity, 
            string category, 
            Object logItem, 
            string errorMessage)
        {
            AdviserLogEntry entry = new AdviserLogEntry()
            {
                Application = _appName,
                Category = category,
                Details = logItem.ToString(),
                DeviceId = deviceId,
                ErrorMessage = errorMessage,
                LogItemType = logItem.GetType().Name,
                PractitionerId = practitionerId,
                Severity = severity,
                Time = DateTime.Now,
                VisitId = visitId
            };

            return entry;
        }
    }
}
