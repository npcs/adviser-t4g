﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.Objects.DataClasses;

namespace Adviser.Server.Core.Domain
{
    using Adviser.Server.DataModel;
    using Adviser.Server.Core.Services;
    using Adviser.Common.Domain;

    /// <summary>
    /// Processes measurement data and generates calculated results to the client.
    /// </summary>
    public class AdviserLab
    {
        private Dictionary<int, List<string>> _organZones;
        private IDBService _dbService;

        public AdviserLab(IDBService service)
        {
            _dbService = service;
            List<OrganZoneDependency> organZoneList = _dbService.GetOrganZoneDependencies().ToList();
            _organZones = new Dictionary<int, List<string>>(organZoneList.Count);
            for (int i = 0; i < organZoneList.Count; ++i)
            {
                List<string> values = new List<string>(7);
                values.Add(organZoneList[i].Zone1);
                values.Add(organZoneList[i].Zone2);
                values.Add(organZoneList[i].Zone3);
                values.Add(organZoneList[i].Zone4);
                values.Add(organZoneList[i].Zone5);
                values.Add(organZoneList[i].Zone6);
                values.Add(organZoneList[i].Zone7);
                _organZones.Add(organZoneList[i].OrganID, values);
            }
        }

        /// <summary>
        /// Processes visit measurement data and sets result value for each OrganExamination.
        /// </summary>
        /// <param name="visit"></param>
        /// <returns>Visit with calculated organ examination results</returns>
        public Visit ProcessData(Visit visit)
        {
            AdviserResults results = new AdviserResults();
            EvaluateOrgans(visit, results);
            ComputeMedicineRecommendations(visit, results);

            BinaryFormatter fmt = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            fmt.Serialize(stream, results);
            byte[] binData = stream.ToArray();
            visit.ExamResult = binData;

            return visit;
        }

        /// <summary>
        /// Calculates organs conditions based on measurement data from the given visit.
        /// </summary>
        /// <param name="visit">Visit that contatins measurement data.</param>
        internal void EvaluateOrgans(Visit visit, AdviserResults results)
        {
            AdviserMeasurement measurementData = null;
            byte[] rawData = visit.MeasurementData;
            if (rawData != null)
            {
                BinaryFormatter fmt = new BinaryFormatter();
                using (MemoryStream stream = new MemoryStream(rawData))
                {
                    stream.Seek(0, SeekOrigin.Begin);
                    measurementData = (AdviserMeasurement)fmt.Deserialize(stream);
                }
            }

            //- calculate percentage ratios to the main average (which is DiagMean)
            DataMatrix<PairMeasurement> matrix = measurementData.Data;
            List<int> frontalRatios = new List<int>();
            foreach (double frontalVal in matrix.FrontalMeans)
            {
                double ratio = frontalVal / matrix.DiagonalMean;
                int percentage = Convert.ToInt32(ratio * 100);
                frontalRatios.Add(percentage);
            }

            List<int> diagRatios = new List<int>();
            for (int i = 0; i < matrix.RowCount; ++i)
            {
                double ratio = matrix[i, i].Maximum / matrix.DiagonalMean;
                int percentage = Convert.ToInt32(ratio * 100);
                diagRatios.Add(percentage);
            }

            //- calculate exam result values
            foreach (OrganExamination exam in visit.OrganExaminations)
            {
                int organId = exam.Organ.ID;
                if (!_organZones.ContainsKey(organId))
                    continue;
                List<string> organDependency = _organZones[organId];
                int plusSum = 0;
                int plusCount = 0;
                int mSum = 0;
                int mCount = 0;
                IEnumerable<string> pluses = organDependency.Where(n => n == "+");
                for (int i = 0; i < organDependency.Count; ++i)
                {
                    if (organDependency[i] == "+")
                    {//- for '+' take frontals
                        plusSum += frontalRatios[i];
                        ++plusCount;
                    }
                    else if (organDependency[i] == "M")
                    {//- for 'M' take diagonals 
                        mSum += diagRatios[i];
                        ++mCount;
                    }
                    else
                    {
                    }
                }

                double plusAvg = 0;
                double mAvg = 0;
                double organValue = 0;
                if (plusCount > 0)
                {
                    plusAvg = plusSum / plusCount;
                    organValue = plusAvg;
                }
                if (mCount > 0)
                {
                    mAvg = mSum / mCount;
                    organValue = (plusAvg + mAvg) / 2;
                }
                exam.Value = Convert.ToInt32(Math.Round(organValue, 0));
            }

            //- diagonal ratios correspond to chakras conditions (we count chakras in reverse order from top to bottom: [0] is 7th,.., [6] is 1st chakra)
            results.ChakrasCondition = diagRatios;
        }

        /// <summary>
        /// Selects prescriptions and ranks them based on visit data.
        /// </summary>
        /// <param name="visit">Visit that contains measurement data.</param>
        internal void ComputeMedicineRecommendations(Visit visit, AdviserResults results)
        {
            Dictionary<int, MedicineRelevance> prescriptionsByMed = new Dictionary<int, MedicineRelevance>();
            Condition normCondition = _dbService.GetConditionByValue(100);
            int qualifierCount = 0;

            //- rank medicine by symptoms
            foreach (Symptom symptom in visit.Symptoms)
            {
                ++qualifierCount;

                symptom.Medicine.Load();
                EntityCollection<Medicine> meds = symptom.Medicine;
                foreach (Medicine med in meds)
                {
                    MedicineRelevance relev = null;
                    if (prescriptionsByMed.ContainsKey(med.ID))
                    {
                        relev = prescriptionsByMed[med.ID];
                        relev.Relevance += 1;
                    }
                    else
                    {
                        relev = new MedicineRelevance();
                        //relev.MedicineReference = med;
                        relev.MedicineID = med.ID;
                        relev.Relevance = 1;
                        prescriptionsByMed.Add(med.ID, relev);
                    }
                }
            }

            //- rank medicine by organ examination results
            foreach (OrganExamination exam in visit.OrganExaminations)
            {
                MedicineRelevance relev = null;

                //- get by organ chronic condition
                if (exam.ChronicDisease)
                {
                    ++qualifierCount;
                    MedicineChronicOrgan medByChronic = _dbService.GetMedicineIdByChronicOrgan(exam.Organ.ID);
                    if (prescriptionsByMed.ContainsKey(medByChronic.MedicineID))
                    {
                        relev = prescriptionsByMed[medByChronic.MedicineID];
                        relev.Relevance += 1;
                    }
                    else
                    {
                        relev = new MedicineRelevance();
                        relev.MedicineID = medByChronic.MedicineID;
                        relev.Relevance = 1;
                        prescriptionsByMed.Add(medByChronic.MedicineID, relev);
                    }
                }

                //- get by condition value
                if (exam.Value > normCondition.MaxRange || exam.Value < normCondition.MinRange)
                {
                    ++qualifierCount;

                    List<Medicine> medicineList = _dbService.GetMedicineIdByOrganCondition(exam.Organ.ID, exam.Value);

                    foreach (Medicine med in medicineList)
                    {
                        if (prescriptionsByMed.ContainsKey(med.ID))
                        {
                            relev = prescriptionsByMed[med.ID];
                            relev.Relevance += 1;
                        }
                        else
                        {
                            relev = new MedicineRelevance();
                            relev.MedicineID = med.ID;
                            relev.Relevance = 1;
                            prescriptionsByMed.Add(med.ID, relev);
                        }
                    }

                    //if (!exam.Organ.MedicineOrganConditionXRef.IsLoaded)
                    //    exam.Organ.MedicineOrganConditionXRef.Load();

                    //EntityCollection<MedicineOrganConditionXRef> refs = exam.Organ.MedicineOrganConditionXRef;
                    
                    //foreach (MedicineOrganConditionXRef reference in refs)
                    //{
                    //    if (exam.Value > reference.Conditions.MinRange && exam.Value <= reference.Conditions.MaxRange)
                    //    {
                    //        //MedicineRelevance relev = null; //prescriptions.Single(p => p.Medicine.ID == reference.MedicineID);
                    //        if (prescriptionsByMed.ContainsKey(reference.MedicineID))
                    //        {
                    //            relev = prescriptionsByMed[reference.MedicineID];
                    //            relev.Relevance += 1;
                    //        }
                    //        else
                    //        {
                    //            relev = new MedicineRelevance();
                    //            relev.MedicineID = reference.MedicineID;
                    //            relev.Relevance = 1;
                    //            prescriptionsByMed.Add(reference.MedicineID, relev);
                    //        }
                    //    }
                    //}
                }
            }

            if (qualifierCount == 0)
                return;

            //- add top relevant prescriptions to the recommended list for this visit
            List<MedicineRelevance> sortedPrescriptions = prescriptionsByMed.Values.ToList();
            sortedPrescriptions.Sort(
                delegate(MedicineRelevance p1, MedicineRelevance p2)
                {
                    return p2.Relevance.CompareTo(p1.Relevance); //- flip them to get descending sort order
                });

            //double max = sortedPrescriptions[0].Relevance;
            int relevanceThreshold = 5; //- include only those medicine that is relevant more than relevanceThreshold
            int countThreshold = 1000; //- limit number of recommended
            List<MedicineRelevance> recommendedList = new List<MedicineRelevance>();

            //- calculate relevance in percentage
            double max = (double)qualifierCount;
            foreach (MedicineRelevance medRelev in sortedPrescriptions)
            {
                double ratio = ((double)medRelev.Relevance) / max;
                medRelev.Relevance = (ushort)(ratio * 100);

                if (medRelev.Relevance > relevanceThreshold)
                {
                    recommendedList.Add(medRelev);
                }
                if (--countThreshold == 0)
                    break;
            }
            results.MedicineRecommendation = recommendedList;
        }

    }
}
