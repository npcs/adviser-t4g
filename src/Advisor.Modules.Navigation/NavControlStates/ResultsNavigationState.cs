﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Events;

using Adviser.Common;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Presentation;

namespace Advisor.Modules.Navigation
{
    public class ResultsNavigationState : NavigationState
    {
        private AppointmentSession _session;
        private IUnityContainer _container;

        public ResultsNavigationState(IUnityContainer container, AppointmentSession session, IEventAggregator eventAggregator)
        {
            _container = container;
            _session = session;
        }

        protected override void SetControls()
        {
            ControlMap[NavigationControls.Results].IsSelected = true;

            ControlMap[NavigationControls.Patients].IsEnabled = true;
            ControlMap[NavigationControls.Visits].IsEnabled = true;
            ControlMap[NavigationControls.Measurement].IsEnabled = false;
            if (_session.HasResults)
            {
                ControlMap[NavigationControls.Results].IsEnabled = false;
                ControlMap[NavigationControls.Print].IsEnabled = true;
            }
        }

    }
}
