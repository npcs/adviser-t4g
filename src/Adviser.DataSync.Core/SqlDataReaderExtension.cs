﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.SqlClient;

namespace Adviser.DataSync.Core
{
    public static class SqlDataReaderExtension
    {
        public static T GetValue<T>(this SqlDataReader sqlDataReader, string columnName)
        {
            var value = sqlDataReader[columnName];

            if (value != DBNull.Value)
            {
                return (T)value;
            }

            return default(T);
        }
    }
}
