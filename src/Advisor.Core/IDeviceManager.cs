﻿using System;
using Advisor.Common.DataModel;

namespace Advisor.Core
{
    public interface IDeviceManager
    {
        event MeasurementCompletedEventHandler MeasurementCompleted;
        event MeasurementProgressEventHandler MeasurementProgress;
        MeasurementSettings Settings { get; set; }

        void Initialize(DeviceSettings deviceSettings, MeasurementSettings measurementSettings);
        void Release();
        void StartMeasurement();
        void StopMeasurement();
        
        Guid GetDeviceId();
        double GetBusVoltage();
        bool IsConnected { get; }
        bool IsSet { get; }
        bool IsStopped { get; }
    }
}
