﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;

using Advisor.Core;
using Advisor.Common.DataModel;

namespace Advisor.Presentation.Models
{
    /// <summary>
    /// Implements presentation model for data matrix.
    /// </summary>
    public class DataMatrixViewModel : ViewModelBase
    {
        private DataMatrix<PairMeasurement> _matrix;
        private ObservableCollection<DataCellViewModel> _dataCells;
        private double _mean;

        public DataMatrixViewModel(DataMatrix<PairMeasurement> matrix)
        {
            _matrix = matrix;
            _dataCells = new ObservableCollection<DataCellViewModel>();
            UpdateData(matrix);

        }

        public void UpdateData(DataMatrix<PairMeasurement> matrix)
        {
            _matrix = matrix;

            double sum = 0;
            for (int x = 0; x < _matrix.ColumnCount; ++x)
            {
                for (int y = 0; y < _matrix.RowCount; ++y)
                {
                    _dataCells.Add(new DataCellViewModel(x, y, _matrix[x, y], SourceValueType.Maximum, new ScaleValueTransform(10000)));
                    sum += matrix[x, y].Mean;
                }
            }

            Mean = sum / (_matrix.ColumnCount * _matrix.RowCount);
        }

        public ObservableCollection<DataCellViewModel> Cells
        {
            get
            {           
                return _dataCells;
            }
        }

        public double Mean
        {
            get
            {
                return _mean;
            }
            private set
            {
                _mean = value;
                base.RaisePropertyChangedEvent("Mean");
            }
        }
    }
}
