﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Common.DataModel
{
    public interface IValueTransform
    {
        double Transform(double source);
        void SetParameters(object[] args);
    }
}
