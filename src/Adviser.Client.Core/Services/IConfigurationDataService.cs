﻿using System;
using System.Collections.Generic;

using Adviser.Common.Domain;
using Adviser.Common.Collections;
using Adviser.Client.DataModel;

namespace Adviser.Client.Core.Services
{
    public interface IConfigurationDataService
    {
        XmlDictionary<string, string> LoadXmlSettings(string settingsFileName);
        void SaveXmlSettings(XmlDictionary<string, string> xml, string settingsFileName);
        MeasurementSettings GetMeasurementSettings();
        DeviceSettings GetDeviceSettings();
        string GetAppSetting(string key);
    }
}
