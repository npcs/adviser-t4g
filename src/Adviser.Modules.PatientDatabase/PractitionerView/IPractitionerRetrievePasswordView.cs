﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Modules.PatientDatabase
{
    public interface IPractitionerRetrievePasswordView
    {
        IPractitionerModel Model { get; set; }
        void ShowMessage(string message);
        event EventHandler RetrievePasswordClicked;
        event EventHandler CancelClicked;
    }
}
