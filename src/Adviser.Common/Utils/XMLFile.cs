﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Adviser.Common.Utils
{

    /// <summary>
    ///  Generic class to save/load IXmlSerializable .
    /// </summary>
    /// <typeparam name="TClass"></typeparam>
    public class XMLFile<TClass> where TClass : IXmlSerializable
    {
        public void Save(TClass classInstance, string fileName)
        {
            XmlSerializer ser = new XmlSerializer(typeof(TClass));
            XmlTextWriter writer = new XmlTextWriter(fileName, Encoding.Default);
            try
            {
                ser.Serialize(writer, classInstance);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Exception while saving xml file to " + fileName, ex);
            }
            finally
            {
                writer.Close();
            }
        }

        public TClass Load(string fileName)
        {
            TClass className = default(TClass);
            XmlSerializer ser = new XmlSerializer(typeof(TClass));

            XmlTextReader reader = new XmlTextReader(fileName);
            try
            {
                className = (TClass)ser.Deserialize(reader);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Exception while reading xml file " + fileName, ex);
            }
            finally
            {
                reader.Close();
            }
            return className;
        }

    }
}
