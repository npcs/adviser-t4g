﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;
using Advisor.Modules.Catalog.Services;
using Advisor.Common.DataModel;
using Advisor.Common.Events;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Events;
using Advisor.Modules.Catalog.Controllers;

namespace Advisor.Modules.Catalog.Views
{
    public class PatientPresenter : IPatientPresenter
    {
        private IUnityContainer _Container;
        private IPatientService _Service;
        private Practitioner _SelectedPractitioner;
        private Patient _SelectedPatient;
        private IEventAggregator _eventAggregator;

        public PatientPresenter(
          IPatientView view,
          IUnityContainer container,
          IEventAggregator eventAggregator,
          IPatientService service
            )
        {
            this.View = view;
            this._Container = container;
            this._Service = service;
            this._eventAggregator = eventAggregator;

            _SelectedPractitioner = _Service.RetrivePractitioner();
            View.PatientList = _Service.RetrivePatientList(_SelectedPractitioner);

            View.PatientSelected += new EventHandler<DataEventArgs<Patient>>(OnPatientSelected);
            View.PatientDeleted += new EventHandler<DataEventArgs<Patient>>(OnPatientDeleted);
            View.PatientUpdated += new EventHandler(OnPatientUpdated);
            View.PatientAdded += new EventHandler(OnPatientAdded);
            
            View.AnamnezAdded += new EventHandler(OnAnamnezAdded);
            View.AnamnesisDeleted += new EventHandler<DataEventArgs<Anamnesis>>(OnAnamnesisDeleted);
            
            View.VisitAdded += new EventHandler(OnVisitAdded);
            View.VisitDeleted += new EventHandler<DataEventArgs<Visit>>(OnVisitDeleted);
            View.VisitSelected += new EventHandler<DataEventArgs<Visit>>(OnVisitSelected);

            _eventAggregator.GetEvent<MeasurementCompletedEvent>().Subscribe(OnMeasurementCompleted, ThreadOption.UIThread);

        }

        private void OnPatientSelected(Object sender, DataEventArgs<Patient> e)
        {
            _SelectedPatient = e.Value;
            View.SelectedPatient = _SelectedPatient;
            View.AnamnesisList = _Service.RetriveAnamnesisList(_SelectedPatient);
            View.VisitList = _Service.RetriveVisitList(_SelectedPatient);
        }

        private void OnVisitDeleted(Object sender, DataEventArgs<Visit> e)
        {
            _Service.DeleteObject(e.Value);
        }

        private void OnAnamnesisDeleted(Object sender, DataEventArgs<Anamnesis> e)
        {
            _Service.DeleteObject(e.Value);
        }

        private void OnPatientDeleted(Object sender, DataEventArgs<Patient> e)
        {
            _Service.DeleteObject(e.Value);
        }

        private void OnPatientUpdated(Object sender, EventArgs e)
        {
            _Service.UpdateDatabase();
        }

        private void OnPatientAdded(Object sender, EventArgs e)
        {
            _Service.AddPatient(_SelectedPractitioner);
        }

        private void OnAnamnezAdded(Object sender, EventArgs e)
        {
            _Service.AddAnamnez(_SelectedPatient);
        }

        private void OnVisitAdded(Object sender, EventArgs e)
        {
            _Service.AddVisit(_SelectedPatient);

        }

        private void OnVisitSelected(Object sender, DataEventArgs<Visit> e)
        {
            MeasurementEventArgs eventArgs = null;
            if (e.Value != null)
            {
                byte[] rawData = e.Value.MeasurementData;
                if (rawData != null)
                {
                    BinaryFormatter fmt = new BinaryFormatter();
                    using (MemoryStream stream = new MemoryStream(rawData))
                    {
                        stream.Seek(0, SeekOrigin.Begin);
                        eventArgs = (MeasurementEventArgs)fmt.Deserialize(stream);

                    }
                }
            }
            _eventAggregator.GetEvent<MeasurementLoadedEvent>().Publish(eventArgs);
        }

        private void OnMeasurementCompleted(MeasurementEventArgs e)
        {
            if (View.SelectedVisit == null)
                return;
            Visit visit = View.SelectedVisit;
            BinaryFormatter fmt = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            fmt.Serialize(stream, e);
            byte[] binData = stream.ToArray();
            visit.MeasurementData = binData;
            _Service.UpdateDatabase();
  
        }


        public event EventHandler<DataEventArgs<Patient>> PatientSelected = delegate { };
        public event EventHandler<DataEventArgs<Visit>> VisitSelected = delegate { };

        public IPatientView View { get; set; }

    }
}
