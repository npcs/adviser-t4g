﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

using Advisor.Common;
using Advisor.Common.Presentation;
using Advisor.Modules.Readout.PresentationModels;
using Advisor.Modules.Readout.Views;
using AdvisorClient.Views;
using AdvisorClient.Modules;

namespace AdvisorClient.Modules
{
    public class MainToolbarModule : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public MainToolbarModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            RegisterViewsAndServices();

            //IRegion region = _regionManager.Regions[RegionNames.ToolbarRegion];
            //MainToolbar toolBar = new MainToolbar();
            //region.Add(toolBar);
        }

        protected void RegisterViewsAndServices()
        {
            _container.RegisterType<IMainToolbarView, MainToolbar>();
        }
    }
}
