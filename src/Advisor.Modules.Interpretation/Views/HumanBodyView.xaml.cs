﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Advisor.Modules.Interpretation.PresentationModels;
using _3DTools;
using System.Globalization;

namespace Advisor.Modules.Interpretation.Views
{
    /// <summary>
    /// Interaction logic for HumanBody.xaml
    /// </summary>
    public partial class HumanBodyView : UserControl, IHumanBodyView
    {

        public HumanBodyView()
        {
            InitializeComponent();
        }

        public IHumanBodyViewModel Model
        {
            get
            {
                return this.DataContext as IHumanBodyViewModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        /// <summary>
        /// Controller action, called on Loaded event.
        /// Renders organs according to the current state of HumanBodyViewModel.
        /// </summary>
        public void RenderOrgans()
        {
            this.OrganGroup.Children.Clear();

            if (Model.OrganViewList != null && Model.OrganViewList.Count != 0)
            {   
                foreach (OrganViewModel organViewModel  in Model.OrganViewList)
                {

                    if (!organViewModel.IsVisible)
                        continue;

                    //- pick mesh geometry from the XAML resource
                    MeshGeometry3D organMesh = (MeshGeometry3D)ZAM3DViewport3D.Resources[organViewModel.Name.Replace(" ", "") + "Geometry"];

                    MaterialGroup materialGroup = new MaterialGroup();                    

                    if (organViewModel.Surgery || organViewModel.ChronicDisease)
                    {  
                        DiffuseMaterial _diffuseMaterial = new DiffuseMaterial(new SolidColorBrush(Color.FromArgb(50,0,0,0)));
                        
                        EmissiveMaterial   _emissiveMaterial = new EmissiveMaterial(organViewModel.OrganBrush);
                        materialGroup.Children.Add(_diffuseMaterial);
                        materialGroup.Children.Add(_emissiveMaterial);
                    }
                    else
                    {
                        DiffuseMaterial _diffuseMaterial= new DiffuseMaterial(organViewModel.OrganBrush);
                        SpecularMaterial _specularMaterial = (SpecularMaterial)ZAM3DViewport3D.Resources["OrganSpecularMaterial"]; 
                        materialGroup.Children.Add(_diffuseMaterial);
                        materialGroup.Children.Add(_specularMaterial);
                    }

                  

                    //- initialize organ 3D view
                    GeometryModel3D organ3D = new GeometryModel3D(organMesh, materialGroup);

                    this.OrganGroup.Children.Add(organ3D);
                }
            }

            this.SkinGroup.Children.Clear();

            //- pick skin mesh geometry from the XAML resource
            MeshGeometry3D skinMesh = (MeshGeometry3D)ZAM3DViewport3D.Resources[(Model.IsMale?"Male":"Female") + "SkinGeometry"];
            this.SkinGroup.Children.Add(new GeometryModel3D(skinMesh, (MaterialGroup)ZAM3DViewport3D.Resources["SkinMaterial"]));
        }



        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            RenderOrgans();
        }

        private void OrganCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            //CheckBox c = (CheckBox)sender;
            //bool? isChecked = c.IsChecked;

            //if (Model.OrganViewCollection.ContainsKey(c.Content.ToString()))
            //{
            //    Model.OrganViewCollection[c.Content.ToString()].IsVisible = (bool)c.IsChecked;
            //}
            RenderOrgans();
        }

        private void HumanModelGroupBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            thisTrackballDecorator.ResetToOriginal();
        }



    }
}
