﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Server.Core.Logging
{
    public enum LogCategory
    {
        General,
        Services,
        Security,
        Data
    }
}
