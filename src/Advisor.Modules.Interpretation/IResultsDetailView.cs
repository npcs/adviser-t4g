﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Advisor.Modules.Interpretation.PresentationModels;

namespace Advisor.Modules.Interpretation
{
    public interface IResultsDetailView
    {
        IHumanBodyViewModel Model { get; set; }
    }
}
