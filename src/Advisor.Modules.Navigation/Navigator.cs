﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using System.Reflection;
using System.IO;

using System.Configuration;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;

using Adviser.Client.Core.Events;
using Adviser.Client.Core.Presentation;
using Adviser.Client.DataModel;
using Adviser.Common.Logging;
using Adviser.Client.Core.Services;

namespace Advisor.Modules.Navigation
{
    public class Navigator : ViewModelBase, INavigator
    {
        private IUnityContainer _container;
        private IEventAggregator _eventAggregator;
        private Dictionary<string, NavigationState> _states;
        private NavigationState _currentState;
        private INavigationView _view;
        private IRegionManager _regionManager;
        private AppointmentSession _session;

        public DelegateCommand<Key> KeyboardCommand { get; private set; }

        public Navigator(
            IUnityContainer container,
            IEventAggregator eventAggregator,
            IRegionManager regionManager,
            INavigationView view,
            AppointmentSession session)
        {
            _container = container;
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _session = session;

            //TODO: remove this in Release!!!
            KeyboardCommand = new DelegateCommand<Key>(OnKeyboardCommand);

            _view = view;
            _view.Model = this;
            _states = new Dictionary<string, NavigationState>();

            LoadNavigation(ConfigurationManager.AppSettings["navigationMap"]);

            _eventAggregator.GetEvent<NavigationRequestEvent>().Subscribe(OnNavigationRequestEvent);

            //_eventAggregator.GetEvent<NavigationEvent>().Subscribe(OnNavigationEvent);
            //_eventAggregator.GetEvent<PatientSelectedEvent>().Subscribe(OnPatientSelectedEvent);
            //_eventAggregator.GetEvent<VisitSelectedEvent>().Subscribe(OnVisitSelectedEvent);
            //_eventAggregator.GetEvent<ResultsReceivedEvent>().Subscribe(OnResultsReceivedEvent); 
        }

        private void OnNavigationRequestEvent(NavigationEventArgs e)
        {
            Navigate(e.EventName);
        }

        /// <summary>
        /// Loads navigation configuration from given location.
        /// </summary>
        /// <param name="configPath">Path for the XML configuration file.</param>
        public void LoadNavigation(string configPath)
        {
            XmlDocument config = new XmlDocument();
            //config.Load(configPath);
            _states.Clear();

            string resourceName = string.Format("{0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.Namespace, "NavigationMap.xml");

            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
            if (stream == null)
            {
                throw new ApplicationException("EmbeddedResourceCache.GetXmlDocResource():  resourceName " + resourceName + " stream is null");
            }
            else
            {
                config = new XmlDocument();
                config.Load(stream);
            }

            foreach (XmlNode stateConfig in config.DocumentElement.SelectNodes("state"))
            {
                NavigationState state = null;
                string stateName = stateConfig.Attributes["name"].Value;
                if (stateConfig.Attributes["typeName"] != null)
                {
                    string typeName = stateConfig.Attributes["typeName"].Value;
                    state = _container.Resolve<NavigationState>(typeName);
                    state.Name = stateName;
                }
                else
                {
                    state = new NavigationState();
                    state.Name = stateName;
                }

                foreach (XmlNode mapItem in stateConfig.SelectNodes("sub/event"))
                {
                    state.AddSubscription(mapItem.Attributes["name"].Value, mapItem.Attributes["to"].Value);
                }

                foreach (XmlNode controlItem in stateConfig.SelectNodes("controls/control"))
                {
                    NavigationControlViewModel control = new NavigationControlViewModel(_eventAggregator,
                        controlItem.Attributes["id"].Value,
                        controlItem.Attributes["label"].Value,
                        Convert.ToBoolean(controlItem.Attributes["isEnabled"].Value),
                        controlItem.Attributes["event"].Value);

                    if (controlItem.Attributes["isSelected"] != null)
                        control.IsSelected = true;

                    state.AddControl(control);
                }

                foreach (XmlNode contentItem in stateConfig.SelectNodes("content/view"))
                {
                    state.AddContentMapping(contentItem.Attributes["id"].Value, contentItem.Attributes["target"].Value);
                }

                state.RegionManager = _regionManager;
                state.Context = this;

                _states.Add(state.Name, state);
                if (stateConfig.Attributes["initial"] != null)
                    Navigate(state);
            }
        }

        public NavigationState CurrentState
        {
            get { return _currentState; }
            set
            {
                _currentState = value;
                base.RaisePropertyChangedEvent("CurrentState");
            }
        }

        /// <summary>
        /// Navigates to the new state and activates corresponding views.
        /// </summary>
        /// <param name="newState">State to navigate to.</param>
        public void Navigate(string stateName)
        {
            string oldStateName = CurrentState.Name;
            NavigationState newState = FindState(stateName);
            Navigate(newState);
        }

        /// <summary>
        /// Navigates to the new state and activates corresponding views.
        /// </summary>
        /// <param name="newState">State to navigate to.</param>
        public void Navigate(NavigationState newState)
        {
            try
            {
                string oldStateName = CurrentState != null ? CurrentState.Name : String.Empty;
                newState.Initialize(oldStateName);
                CurrentState = newState;

                NavigationEventArgs args = new NavigationEventArgs();
                args.EventName = newState.Name;
                _eventAggregator.GetEvent<NavigationEvent>().Publish(args);
            }
            catch (Exception ex)
            {
                string message = "Exception while navigating to " + newState.Name;
                ILogger logService = _container.Resolve<ILogger>();
                logService.LogError(ex, message);
            }
        }

        /// <summary>
        /// Return navigation state by name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public NavigationState FindState(string name)
        {
            return _states[name];
        }

        public INavigationView View
        {
            get { return _view; }
        }

        /// <summary>
        /// Navigation event handler.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public void OnNavigationCommand(object source, RoutedEventArgs e)
        {
            string actionName = ((System.Windows.Controls.Button)source).Tag.ToString();
            NavigationEventArgs args = new NavigationEventArgs();
            args.EventName = actionName;
            _eventAggregator.GetEvent<NavigationEvent>().Publish(args);
        }

        //private void UpdateNavControlsState()
        //{
        //    //CurrentState.SetControlsState();
        //}

        //private void OnPatientSelectedEvent(Patient p)
        //{
        //    UpdateNavControlsState();
        //}

        //private void OnVisitSelectedEvent(Visit v)
        //{
        //    UpdateNavControlsState();
        //}

        ///// <summary>
        ///// Refreshes navigation buttons state on results received event
        ///// </summary>
        ///// <param name="v"></param>
        //private void OnResultsReceivedEvent(ResultsReceivedEventArgs e)
        //{
        //    if (_session.Visit != null)
        //    {
        //        if(e.IsSuccess && e.VisitData.ID == _session.Visit.ID)
        //            UpdateNavControlsState();
        //    }
        //}

        /// <summary>
        /// Loads data readout screen.
        /// </summary>
        /// <remarks>Must be disabled for common release.</remarks>
        /// <param name="p"></param>
        private void OnKeyboardCommand(Key p)
        {
            if (p == Key.P)
            {
                NavigationEventArgs args = new NavigationEventArgs();
                args.EventName = "ReadoutKeyGesture";
                //_eventAggregator.GetEvent<NavigationEvent>().Publish(args);
                Navigate("Readout");
            }

            ////string fn = _session.Patient.LastName + " " + _session.Patient.FirstName + " " + _session.Visit.Date.ToString("YYYY-mm-dd");
            //Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            //dlg.FileName = "Document"; //- Default file name
            //dlg.DefaultExt = ".csv"; //- Default file extension
            ////dlg.Filter = "Text documents (.txt)|*.txt"; //- Filter files by extension

            ////- Show save file dialog box
            //Nullable<bool> result = dlg.ShowDialog();

            ////- Process save file dialog box results
            //if (result == true)
            //{
            //    // Save document
            //    string filename = dlg.FileName;
            //    //SaveToFile(filename, visit);
            //}
        }
    }
}
