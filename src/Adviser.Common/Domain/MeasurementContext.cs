﻿using System;
using System.Collections.Generic;


namespace Adviser.Common.Domain
{
    [Serializable()]
    public class MeasurementContext
    {
        public DeviceSettings DeviceSettings { get; set; }
        public MeasurementSettings MeasurementSettings { get; set; }
    }
}
