﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Configuration;
using System.ComponentModel;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Wpf;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Wpf.Commands;
using System.Collections.ObjectModel;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Client.DataModel;
    using Adviser.Client.Core.Presentation;
    using Adviser.Client.Core.Events;
    using Adviser.Client.Core.Services;
    using Adviser.Common.Logging;
    
    public interface IPatientsPresentationModel
    {
        IPatientListView ListView { get; set; }
        IPatientDetailsView DetailsView { get; set; }
        ObservableCollection<Patient> Patients { get; }
        Patient SelectedPatient { get; }
        System.Windows.Visibility DetailsVisibility { get; }
        DelegateCommand<object> AddPatientCommand { get; }
        DelegateCommand<object> DeletePatientCommand { get; }
        DelegateCommand<object> SaveDataCommand { get; }
    }

    /// <summary>
    /// Presentation model for the PatientsView (PatientListView + PatientDetailsView).
    /// </summary>
    public class PatientsPresentationModel : ViewModelBase, IPatientsPresentationModel
    {
        private IDBService _dbService;
        private IEventAggregator _eventAggregator;
        private AppointmentSession _session;
        private ILogger _logService;

        private ObservableCollection<Patient> _patients;
        private Patient _selectedPatient;
        private System.Windows.Visibility _detailsVisibility;

        public PatientsPresentationModel(
            IPatientListView patientListView, 
            IPatientDetailsView patientDetailsView, 
            PatientDetailsPrintView printableView,
            IDBService service, 
            IEventAggregator eventAggregator,
            IUnityContainer container,
            ILogger logService)
        {
            ListView = patientListView;
            DetailsView = patientDetailsView;
            
            _dbService = service;
            _eventAggregator = eventAggregator;
            _session = container.Resolve<AppointmentSession>();
            _logService = logService;
            _eventAggregator.GetEvent<PractitionerChangedEvent>().Subscribe(PractitionerChanged);

            AddPatientCommand = new DelegateCommand<object>(AddPatient);
            DeletePatientCommand = new DelegateCommand<object>(DeletePatient);
            SaveDataCommand = new DelegateCommand<object>(OnSaveDataCommand);
            ListView.PatientSelected += new EventHandler<DataEventArgs<Patient>>(ListView_PatientSelected);
            ListView.PatientDoubleClicked += new EventHandler(ListView_PatientDoubleClicked);

            ListView.Model = this;// Patients;
            DetailsView.Model = this;// SelectedPatient;

            if (_session.Patient != null)
                SelectedPatient = _session.Patient;
        }

        public IPatientListView ListView { get; set; }
        public IPatientDetailsView DetailsView { get; set; }

        public DelegateCommand<object> AddPatientCommand { get; private set; }
        public DelegateCommand<object> DeletePatientCommand { get; private set; }
        public DelegateCommand<object> SaveDataCommand { get; private set; }

        /// <summary>
        /// Exposes bindable collection of patient objects.
        /// </summary>
        public ObservableCollection<Patient> Patients
        {
            get { return _patients; }
            private set
            {
                _patients = value;
                RaisePropertyChangedEvent("Patients");
            }
        }

        /// <summary>
        /// Exposes selected patient.
        /// </summary>
        public Patient SelectedPatient
        {
            get { return _selectedPatient; }
            set
            {
                _selectedPatient = value;
                base.RaisePropertyChangedEvent("SelectedPatient");

                DetailsVisibility = _selectedPatient != null ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;
            }
        }

        public System.Windows.Visibility DetailsVisibility
        {
            get { return _detailsVisibility; }
            private set
            {
                _detailsVisibility = value;
                base.RaisePropertyChangedEvent("DetailsVisibility");
            }
        }

        private void PractitionerChanged(Practitioner practitioner)
        {
            //_session.Practitioner = practitioner;
            if (practitioner == null)
                return;

            if (!practitioner.Patients.IsLoaded)
                practitioner.Patients.Load();
            Patients = new ObservableCollection<Patient>(practitioner.Patients);
            SelectedPatient = _session.Patient;
        }

        private void OnPractitionerSyncCompleted(PractitionerSyncCompletedEventArgs e)
        {
            PractitionerChanged(e.Practitioner);
        }

        private void ListView_PatientDoubleClicked(object sender, EventArgs e)
        {
            NavigationEventArgs args = new NavigationEventArgs();
            args.EventName = NavigationEventNames.Visits;
            _eventAggregator.GetEvent<NavigationRequestEvent>().Publish(args);
        }

        private void ListView_PatientSelected(object sender, DataEventArgs<Patient> e)
        {
            if (e.Value == SelectedPatient)
                return;

            _session.Patient = e.Value;
            SelectedPatient = e.Value;
            _eventAggregator.GetEvent<PatientSelectedEvent>().Publish(e.Value);
        }

        private void AddPatient(object p)
        {
            try
            {
                Patient patient = _dbService.CreateNewPatient(_session.Practitioner);
                Patients.Add(patient);
                ListView.SetSelectedPatient(patient);
                RaisePropertyChangedEvent("Patients");
            }
            catch (Exception ex)
            {
                string errMsg = String.Format("Error occured while adding patient.");
                System.Windows.MessageBox.Show(errMsg, "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                _logService.LogError(ex, errMsg);    
            }
        }

        private void DeletePatient(object parameters)
        {
            try
            {
                if (SelectedPatient != null)
                {
                    SelectedPatient.Visits.Load();
                    foreach (Visit v in SelectedPatient.Visits)
                    {
                        v.Prescriptions.Load();
                        v.OrganExaminations.Load();
                        v.Symptoms.Load();
                    }
                    
                    _dbService.DeleteObject(SelectedPatient);
                    Patients.Remove(SelectedPatient);
                    //_dbService.SaveChanges();
                }
                _session.Patient = null;
                SelectedPatient = null;
            }
            catch (Exception ex)
            {
                string errMsg = String.Format("Error occured while deleting patient {0} {1}", _session.Patient.FirstName, _session.Patient.LastName);
                System.Windows.MessageBox.Show(errMsg, "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                _logService.LogError(ex, errMsg);
            }
        }

        private void OnSaveDataCommand(object parameters)
        {
            try
            {
                _dbService.SaveChanges();
            }
            catch (Exception ex)
            {
                string errMsg = String.Format("Error occured while updating patient {0} {1}", _session.Patient.FirstName, _session.Patient.LastName);
                System.Windows.MessageBox.Show(errMsg, "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                _logService.LogError(ex, errMsg);
            }
        }
    }
}
