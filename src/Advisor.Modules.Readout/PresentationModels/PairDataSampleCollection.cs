﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Adviser.Common.Domain;

namespace Advisor.Modules.Readout.PresentationModels
{
    /// <summary>
    /// Represents single measurement sample in time.
    /// </summary>
    public class PairDataSampleItem
    {
        public int Time
        {
            get;
            set;
        }

        public double Value
        {
            get;
            set;
        }
    }

    public class PairDataSampleCollection : ObservableCollection<PairDataSampleItem>
    {
        public PairDataSampleCollection() { }

        public PairDataSampleCollection(PairMeasurement measurement, int sampleRate, IValueTransform transform)
        {
            int currentTime = 0;
            //IValueTransform transform = new LinearMapValueTransform(measurement.Minimum, measurement.Maximum, 0.0, 100.0);
            PairDataSampleItem sample = new PairDataSampleItem();
            sample.Time = currentTime;
            sample.Value = 0;
            base.Add(sample);//- adding first item for [0,0] sample presentation
            currentTime += sampleRate;
            for (int i = 0; i < measurement.Samples.Count; ++i)
            {
                sample = new PairDataSampleItem();
                sample.Time = currentTime;
                sample.Value = transform.Transform(measurement.Samples[i]);
                base.Add(sample);
                currentTime += sampleRate;
            }
        }
    }
}
