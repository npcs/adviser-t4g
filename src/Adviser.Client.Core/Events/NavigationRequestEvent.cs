﻿using System;
using Microsoft.Practices.Composite.Wpf.Events;
namespace Adviser.Client.Core.Events
{
    public class NavigationRequestEvent : CompositeWpfEvent<NavigationEventArgs>
    {
    }
}
