﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using Advisor.Core;
using Advisor.Common.DataModel;
using Advisor.Common.Events;
using Advisor.Common.Services;
using Advisor.Presentation.Models;
using Advisor.Presentation.Controls;

namespace Advisor.Presentation
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DeviceManager _manager;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void tabControl1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            progressBarTotal.Value = 0;
            btnStart.IsEnabled = false;

            //TODO: Temp fix - create default Device Settings class
            DeviceSettings deviceSettings = new DeviceSettings();

            //Check if real device is connected. If not - use DeviceEmulator
            IDeviceDriver device = new DeviceDriver();
            device.SetupDevice(deviceSettings);
            if (!device.IsConnected)
            {
                device = new DeviceEmulator();
            }

            //MeasurementContext context = new MeasurementContext();
            //context.DeviceSamplingRate = 100;
            //context.IsFrontPositive = true;
            //context.PairMeasurementDuration = 50;
            //context.ElectrodesConnectionMap = new System.Collections.Generic.List<PairConnection>(49);
            ////TODO: Temp fix - created default DeviceSettings class. 
            //context.DeviceSettings = deviceSettings;
            //for (byte x = 0; x < 7; ++x)
            //{
            //    for (byte y = 0; y < 7; ++y)
            //    {
            //        PairConnection pair = new PairConnection();
            //        pair.Front = x;
            //        pair.Back = y;
            //        context.ElectrodesConnectionMap.Add(pair);
            //    }
            //}

            _manager = new DeviceManager();
            _manager.MeasurementProgress += new MeasurementProgressEventHandler(manager_MeasurementProgress);
            _manager.MeasurementCompleted += new MeasurementCompletedEventHandler(manager_MeasurementCompleted);

            btnStop.IsEnabled = true;

            _manager.StartMeasurement();
        }

        void manager_MeasurementCompleted(object source, MeasurementEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal,
                new Action(
                    delegate()
                    {
                        progressBarTotal.Value = 100;
                        labelProgress.Content = "100% completed";
                        listBoxFront.SelectedIndex = -1;
                        listBoxBack.SelectedIndex = -1;
                        btnStart.IsEnabled = true;
                        btnStop.IsEnabled = false;
                        btnProcess.IsEnabled = true;
                        RenderDataMatrix(e.MeasurementData);
                    })
                   );

            //groupMeasurement.UpdateLayout();
        }

        void manager_MeasurementProgress(object source, MeasurementProgressEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal,
                new Action(
                    delegate()
                    {
                        listBoxFront.SelectedIndex = e.FrontElectrode;
                        listBoxBack.SelectedIndex = e.BackElectrode;
                        labelProgress.Content = e.TotalProgress.ToString() + "% completed";
                        progressBarTotal.Value = (double)e.TotalProgress;
                    }
                    ));

            //groupMeasurement.UpdateLayout();
        }

        private void RenderDataMatrix(DataMatrix<PairMeasurement> matrix)
        {

            matrixView.UpdateData(matrix);

            //DataMatrixViewModel matrixVM = new DataMatrixViewModel(matrix);
            //this.DataContext = matrixVM;
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            _manager.StopMeasurement();
            btnStop.IsEnabled = false;
            btnStart.IsEnabled = true;
            btnProcess.IsEnabled = true;
            groupMeasurement.UpdateLayout();
        }

        private void btnProcess_Click(object sender, RoutedEventArgs e)
        {

            BodyAssessment assessment = new BodyAssessment();
            assessment.Assessment = "General health assessment";
            assessment.Recommendations = "General recommendations";

            ZoneAssessment zone = new ZoneAssessment();
            zone.Name = "First";
            zone.Description = "This zone is responsible for head.";
            zone.RiskLevel = HealthRiskLevel.Low;
            zone.Risks = "Slight risks: a)";
            zone.Recommendations = "Pay attention to these organs";
            assessment.Zones.Add(zone);

            zone = new ZoneAssessment();
            zone.Name = "Second";
            zone.Description = "This zone is responsible for ear, nose, throad.";
            zone.RiskLevel = HealthRiskLevel.Normal;
            zone.Risks = "No risks";
            zone.Recommendations = "";
            assessment.Zones.Add(zone);

            zone = new ZoneAssessment();
            zone.Name = "Third";
            zone.Description = "This zone is responsible for lungs, heart.";
            zone.RiskLevel = HealthRiskLevel.VeryHigh;
            zone.Risks = "High risks:" + Environment.NewLine + "  a) Heart" + Environment.NewLine + "  b) Lungs";
            zone.Recommendations = "Do something about your health! Quit smoking!";
            assessment.Zones.Add(zone);

            zone = new ZoneAssessment();
            zone.Name = "Fourth";
            zone.Description = "This zone is responsible for stomach.";
            zone.RiskLevel = HealthRiskLevel.Normal;
            zone.Recommendations = "";
            assessment.Zones.Add(zone);

            zone = new ZoneAssessment();
            zone.Name = "Fifth";
            zone.Description = "This zone is responsible for liver, kidney.";
            zone.RiskLevel = HealthRiskLevel.Medium;
            zone.Recommendations = "Recommendations are here.";
            assessment.Zones.Add(zone);

            zone = new ZoneAssessment();
            zone.Name = "Sixth";
            zone.Description = "This zone is responsible for bowels.";
            zone.RiskLevel = HealthRiskLevel.Normal;
            zone.Recommendations = "";
            assessment.Zones.Add(zone);

            zone = new ZoneAssessment();
            zone.Name = "Seventh";
            zone.Description = "This zone is responsible for reproductive system.";
            zone.RiskLevel = HealthRiskLevel.High;
            zone.Recommendations = "Recommendations are here. Take following medications:";
            assessment.Zones.Add(zone);

            BodyAssessmentViewModel viewModel = new BodyAssessmentViewModel(assessment);

            this.DataContext = viewModel;
            BodyAssessmentView ctrl = new BodyAssessmentView();

            Grid mainGrid = (Grid)this.FindName("gridMain");
            mainGrid.Children.Add(ctrl);
        }
    }
}
