﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Common.DataModel
{
    [Serializable()]
    public class ZoneAssessment
    {
        private string name;
        private string description;
        private HealthRiskLevel riskLevel;
        private string risks;
        private string recommendations;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public HealthRiskLevel RiskLevel
        {
            get { return riskLevel; }
            set { riskLevel = value; }
        }

        public string Risks
        {
            get { return risks; }
            set { risks = value; }
        }

        public string Recommendations
        {
            get { return recommendations; }
            set { recommendations = value; }
        }

    }
}
