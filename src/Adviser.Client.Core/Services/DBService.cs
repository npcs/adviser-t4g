﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows.Media;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using Adviser.Client.Core.Services;
using Adviser.Client.DataModel;
using Adviser.Common.Logging;
using System.Diagnostics;

namespace Adviser.Client.Core.Services
{
    public class DBService : IDBService
    {
        private AdviserDBEntities _dataContext;
        private object _syncLock = new object();

        public DBService()
        {
            _dataContext = new AdviserDBEntities();

            //_dataContext.ObjectStateManager.ObjectStateManagerChanged += new System.ComponentModel.CollectionChangeEventHandler(ObjectStateManager_ObjectStateManagerChanged);
        }

        void ObjectStateManager_ObjectStateManagerChanged(object sender, System.ComponentModel.CollectionChangeEventArgs e)
        {
            Debug.WriteLine(e.Action.ToString() + " : " + e.Element.ToString());
        }

        public AdviserDBEntities DataContext
        {
            get { return _dataContext; }
        }

        public void CreateNewPractitioner(ref Practitioner newPractitioner)
        {
            _dataContext.AddToPractitionerSet(newPractitioner);
            //_dataContext.SaveChanges();
            SaveChanges_Synchronized();
        }

        public void AddPatient(Practitioner CurrentPractitioner)
        {
            Patient newPatient = new Patient();
            newPatient.ID = Guid.NewGuid();
            newPatient.FirstName = "FirstName";
            newPatient.LastName = "LastName";
            newPatient.LastUpdDate = DateTime.Now.ToUniversalTime();

            CurrentPractitioner.Patients.Add(newPatient);
            //_dataContext.SaveChanges();
            SaveChanges_Synchronized();
        }

        public Patient CreateNewPatient(Practitioner CurrentPractitioner)
        {
            Patient newPatient = Patient.CreatePatient(
                Guid.NewGuid(),
                DateTime.Now.ToUniversalTime(),
                "FirstName",
                "LastName",
                "M",
                false);

            CurrentPractitioner.Patients.Add(newPatient);
            _dataContext.AddToPatientSet(newPatient);
            //_dataContext.SaveChanges();
            SaveChanges_Synchronized();

            return newPatient;
        }

        /// <summary>
        /// Creates new Visit in the database for given patient.
        /// </summary>
        /// <param name="CurrentPatient"></param>
        /// <returns></returns>
        public Visit CreateNewVisit(Patient currentPatient)
        {
            //ObjectResult<Visit> result = _dataContext.CreateNewVisit(currentPatient.ID);
            //Visit visit = result.FirstOrDefault();

            Visit visit = Visit.CreateVisit(Guid.NewGuid(), DateTime.Now.ToUniversalTime(), DateTime.Now.ToUniversalTime());
            //visit.Date = DateTime.Now.ToUniversalTime();

            //currentPatient.Visits.Add(visit);
            //CreateOrganExaminationEntityCollection(visit);

            currentPatient.Visits.Add(visit);
            _dataContext.AddToVisitSet(visit);
            SaveChanges_Synchronized();

            return visit;
        }

        public EntityCollection<Visit> LoadVisitEntityCollection(Patient CurrentPatient)
        {
            if (!CurrentPatient.Visits.IsLoaded)
            {
                CurrentPatient.Visits.Load();
            }
            return CurrentPatient.Visits;
        }

        //public EntityCollection<Property> LoadPropertiesCollection()
        //{
        //    EntityCollection<Property> properties = new EntityCollection<Property>();

        //    IQueryable<Property> query =
        //                      from property in _dataContext.PropertySet
        //                      select property;

        //    foreach (Property p in query)
        //    {
        //        properties.Add(p);
        //    }

        //    return properties;
        //}

        /// <summary>
        /// Creates initial collection of OrganExamination entities.
        /// </summary>
        /// <returns></returns>
        public void CreateOrganExaminationEntityCollection(Visit visit)
        {
            //EntityCollection<OrganExamination> organExams = new EntityCollection<OrganExamination>();
            IQueryable<Organ> organs = GetOrgansBySex(visit.Patient.Sex == "M" ? true : false);

            foreach (Organ organ in organs)
            {
                OrganExamination exm = OrganExamination.CreateOrganExamination(Guid.NewGuid(), -1, false, false, false, false);
                exm.Organ = organ;
                visit.OrganExaminations.Add(exm);
            }
        }

        public EntityCollection<Symptom> CreateSymptomExaminationEntityCollection()
        {
            EntityCollection<Symptom> symptomExams = new EntityCollection<Symptom>();
            IQueryable<Symptom> symptoms = GetAllSymptoms();
            foreach (Symptom symptom in symptoms)
            {
                symptomExams.Add(Symptom.CreateSymptom(symptom.ID, symptom.Name, symptom.OrderKey));
            }

            return symptomExams;
        }

        public Condition GetConditionByValue(int examValue)
        {
            Condition condition = null;
            ObjectResult<Condition> result = _dataContext.GetConditionByValue(examValue);
            List<Condition> list = result.ToList();
            if (list.Count != 0)
                condition = list[0];

            return condition;
        }

        public EntityCollection<OrganExamination> LoadOrganExaminationEntityCollection(Visit CurrentVisit)
        {
            if (!CurrentVisit.OrganExaminations.IsLoaded)
            {
                CurrentVisit.OrganExaminations.Load();
            }
            return CurrentVisit.OrganExaminations;
        }

        public object GetObjectByKey(System.Data.EntityKey key)
        {
            return _dataContext.GetObjectByKey(key);
        }

        public EntityCollection<Symptom> LoadSymptomEntityCollection(Visit CurrentVisit)
        {
            if (!CurrentVisit.Symptoms.IsLoaded)
            {
                CurrentVisit.Symptoms.Load();
            }
            return CurrentVisit.Symptoms;
        }

        public EntityCollection<Prescription> LoadPrescriptionEntityCollection(Visit CurrentVisit)
        {
            if (!CurrentVisit.Prescriptions.IsLoaded)
            {
                CurrentVisit.Prescriptions.Load();
            }
            return CurrentVisit.Prescriptions;
        }

        /// <summary>
        /// Returns all available Organ entities from the database.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Organ> GetAllOrgans()
        {
            IQueryable<Organ> query =
                              from organ in _dataContext.OrganSet
                              orderby organ.OrderKey
                              select organ;
            return query;
        }

        /// <summary>
        /// Returns all available Organ entities from the database.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Organ> GetOrgansBySex(bool isMale)
        {
            int OrganGender = isMale ? 1 : 2;
            IQueryable<Organ> query =
                              from organ in _dataContext.OrganSet
                              where organ.Gender == 0 || organ.Gender == OrganGender
                              orderby organ.OrderKey
                              select organ;
            return query;
        }

        /// <summary>
        /// Returns collection of all available symptoms.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Symptom> GetAllSymptoms()
        {
            IQueryable<Symptom> query =
                              from symptom in _dataContext.SymptomSet
                              orderby symptom.Name
                              select symptom;
            return query;
        }

        /// <summary>
        /// Returns all conditions from the Conditions table.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Condition> GetAllConditions()
        {
            IQueryable<Condition> query =
                from condition in _dataContext.ConditionSet
                orderby condition.OrderKey
                select condition;
            return query;
        }

        /// <summary>
        /// Returns all medicine from the Medicine table.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Medicine> GetAllMedicine()
        {
            IQueryable<Medicine> query =
                from medicine in _dataContext.MedicineSet
                select medicine;

            return query;
        }

        public IQueryable<Chakra> GetAllChakras()
        {
            IQueryable<Chakra> query =
                              from chakra in _dataContext.ChakraSet
                              orderby chakra.ID descending
                              select chakra;
            return query;
        }

        public IQueryable<Electrode> GetElectrodes(bool isFront)
        {
            IQueryable<Electrode> query =
                              from Electrode in _dataContext.ElectrodeSet
                              orderby Electrode.Number
                              where Electrode.IsFront == isFront
                              select Electrode;
            return query;
        }

        public Medicine GetMedicineByID(int id)
        {
            return _dataContext.MedicineSet.First(m => m.ID == id);
        }

        public Practitioner GetPractitionerByEmail(string email, string password)
        {
            var practitioners =
               from practitioner in _dataContext.PractitionerSet
               where practitioner.Email == email &&
               practitioner.Password == password
               select practitioner;

            if (practitioners.Count<Practitioner>() > 0)
                return practitioners.First<Practitioner>();
            else
                return null;
        }

        public Practitioner GetPractitionerByEmailOnly(string email)
        {
            var practitioners =
               from practitioner in _dataContext.PractitionerSet
               where practitioner.Email == email
               select practitioner;

            return practitioners.FirstOrDefault();
        }

        public Practitioner GetPractitionerByID(Guid id)
        {
            var practitioners =
               from practitioner in _dataContext.PractitionerSet
               where practitioner.ID == id
               select practitioner;
            return practitioners.FirstOrDefault();

            //return _dataContext.PractitionerSet.FirstOrDefault(x => x.ID == id);
        }


        public bool IsPractitionerEmailUnique(string email)
        {
            int practitionerCount =
                          (from practitioner in _dataContext.PractitionerSet
                          where practitioner.Email == email
                          select practitioner).Count();

            if (practitionerCount > 0)
                return false;
            else
                return true;
        }

        public bool DoesAnyPractitionerExist()
        {
            int practitionerCount = (from practitioner in _dataContext.PractitionerSet
                                select practitioner).Count();

            if (practitionerCount > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Deletes entity object from the database.
        /// </summary>
        /// <param name="EntityObject"></param>
        public void DeleteObject(Object EntityObject)
        {
            if (EntityObject != null)
            {
                _dataContext.DeleteObject(EntityObject);
                SaveChanges();
            }
        }

        /// <summary>
        /// Calls SaveChanges() on underlying context with serialized access.
        /// </summary>
        private void SaveChanges_Synchronized()
        {
            _dataContext.SaveChanges();
        }

        /// <summary>
        /// Saves changes initiated by client. Logs changes in the Change table.
        /// </summary>
        public void SaveChanges()
        {
            //- process added and modified entries
            IEnumerable<ObjectStateEntry> addModifyEntries = _dataContext.ObjectStateManager.GetObjectStateEntries(EntityState.Modified | EntityState.Added);
            IEnumerable<ObjectStateEntry> deletedEntries = _dataContext.ObjectStateManager.GetObjectStateEntries(EntityState.Deleted);
            //Dictionary<Guid, ObjectStateEntry> trackAddModifyEntries = new Dictionary<Guid, ObjectStateEntry>();
            foreach (ObjectStateEntry entry in addModifyEntries)
            {
                if (!entry.IsRelationship && entry.EntitySet.Name != "ChangeSet")
                {
#if DEBUG
                    string id = "";
                    if (entry.EntityKey.EntityKeyValues != null)
                        id = entry.EntityKey.EntityKeyValues.ToArray()[0].Value.ToString();
                    Debug.WriteLine(entry.EntityKey.EntitySetName + " -- " + entry.State + " -- " + id);
#endif

                    TrackAddedModifiedChange(entry);
                }
            }

            //- process deleted entries

            if (deletedEntries.Count() > 0)
            {
                Dictionary<Guid, Guid> cascadedVisits = new Dictionary<Guid, Guid>();
                Dictionary<Guid, Guid> cascadedPrescriptions = new Dictionary<Guid, Guid>();
                Dictionary<Guid, ObjectStateEntry> trackDeleteEntries = new Dictionary<Guid, ObjectStateEntry>();
                foreach (ObjectStateEntry entry in deletedEntries)
                {
                    if (!entry.IsRelationship && entry.EntitySet.Name != "ChangeSet")
                    {
#if DEBUG
                        string id = "";
                        if (entry.EntityKey.EntityKeyValues != null)
                            id = entry.EntityKey.EntityKeyValues.ToArray()[0].Value.ToString();
                        Debug.WriteLine(entry.EntityKey.EntitySetName + " -- " + entry.State + " -- " + id);
#endif

                        //- cascade delete changes
                        if (entry.EntitySet.Name == "PatientSet")
                        {
                            Guid patientId = (Guid)entry.EntityKey.EntityKeyValues.ToArray()[0].Value;
                            trackDeleteEntries.Add(patientId, entry);

                            Patient patient = _dataContext.PatientSet.FirstOrDefault(p => p.ID == patientId);
                            IEnumerable<Visit> visits = _dataContext.VisitSet.Where(v => v.Patient.ID == patientId);
                            foreach (Visit v in visits)
                                cascadedVisits.Add(v.ID, patientId);
                        }
                        else if (entry.EntitySet.Name == "VisitSet")
                        {
                            Guid visitId = (Guid)entry.EntityKey.EntityKeyValues.ToArray()[0].Value;
                            Visit visit = _dataContext.LoadByKey<Visit>("ID", visitId);

                            IEnumerable<Prescription> prescriptions = _dataContext.PrescriptionSet.Where(p => p.Visits.ID == visitId);
                            foreach (Prescription presc in prescriptions)
                                cascadedPrescriptions.Add(presc.ID, visitId);

                            if (!cascadedVisits.ContainsKey(visitId))
                                trackDeleteEntries.Add(visitId, entry);
                        }
                        else if (entry.EntitySet.Name == "PrescriptionSet")
                        {
                            Guid prescId = (Guid)entry.EntityKey.EntityKeyValues.ToArray()[0].Value;
                            if (!cascadedPrescriptions.ContainsKey(prescId))
                                trackDeleteEntries.Add(prescId, entry);
                        }
                        else { }
                    }
                }

                foreach (Guid id in trackDeleteEntries.Keys)
                {
                    ObjectStateEntry entry = trackDeleteEntries[id];
                    //- remove cascaded entities from tracking plan
                    if (!cascadedVisits.ContainsKey(id) && !cascadedPrescriptions.ContainsKey(id))
                    {
                        TrackDeleteChange(entry);
                    }

                    //- delete corresponding change log entries if present
                    Change change = _dataContext.ChangeSet.FirstOrDefault(c => c.ID == id);
                    if (change != null)
                        _dataContext.DeleteObject(change);
                }
            }

            SaveChanges_Synchronized();
        }

        /// <summary>
        /// Tracks change in Added and Modified state.
        /// </summary>
        /// <param name="stateEntry">State entry corresponding to change.</param>
        private void TrackAddedModifiedChange(ObjectStateEntry stateEntry)
        {
            if (stateEntry.EntitySet.Name == "PractitionerSet")
            {
                Guid id = (Guid)stateEntry.EntityKey.EntityKeyValues.ToArray()[0].Value;
                Practitioner target = (Practitioner)stateEntry.Entity;
                target.LastUpdDate = DateTime.Now.ToUniversalTime();
            }
            else if (stateEntry.EntitySet.Name == "PatientSet")
            {
                Guid id = (Guid)stateEntry.EntityKey.EntityKeyValues.ToArray()[0].Value;
                Patient target = (Patient)stateEntry.Entity;
                target.LastUpdDate = DateTime.Now.ToUniversalTime();
                LogAddModifyChange(id, stateEntry.EntitySet.Name, stateEntry.State);
            }
            else if (stateEntry.EntitySet.Name == "VisitSet")
            {//- filter out non-synch visits (those that don't have exam results need not to be tracked because they will be synched on GetExamResults())
                if (((Visit)stateEntry.Entity).ExamResult != null)
                {
                    Guid id = (Guid)stateEntry.EntityKey.EntityKeyValues.ToArray()[0].Value;
                    Visit target = (Visit)stateEntry.Entity;
                    target.LastUpdDate = DateTime.Now.ToUniversalTime();
                    LogAddModifyChange(id, stateEntry.EntitySet.Name, stateEntry.State);
                }
            }
            else if (stateEntry.EntitySet.Name == "PrescriptionSet")
            {//- upgrade prescription change to a visit change

                Prescription presc = (Prescription)_dataContext.GetObjectByKey(stateEntry.EntityKey);
                Guid visitId = presc.Visits.ID;
                Visit target = presc.Visits;
                target.LastUpdDate = DateTime.Now.ToUniversalTime();
                LogAddModifyChange(visitId, "VisitSet", stateEntry.State);
            }
            else
            {
            }
        }

        public void TrackDeleteChange(ObjectStateEntry stateEntry)
        {
            if (stateEntry.EntitySet.Name == "PractitionerSet")
            {
                Guid id = (Guid)stateEntry.EntityKey.EntityKeyValues.ToArray()[0].Value;
                LogDeleteChange(id, stateEntry.EntitySet.Name, stateEntry.State);
            }
            else if (stateEntry.EntitySet.Name == "PatientSet")
            {
                Guid id = (Guid)stateEntry.EntityKey.EntityKeyValues.ToArray()[0].Value;
                LogDeleteChange(id, stateEntry.EntitySet.Name, stateEntry.State);
            }
            else if (stateEntry.EntitySet.Name == "VisitSet")
            {
                if (((Visit)stateEntry.Entity).ExamResult != null)
                {
                    Guid id = (Guid)stateEntry.EntityKey.EntityKeyValues.ToArray()[0].Value;
                    LogDeleteChange(id, stateEntry.EntitySet.Name, stateEntry.State);
                }
            }
            else if (stateEntry.EntitySet.Name == "PrescriptionSet")
            {//- log prescription deletion as a parent Visit change (VisitSet=Modified)
                //Prescription presc = (Prescription)_dataContext.GetObjectByKey(stateEntry.EntityKey);

                Guid id = (Guid)stateEntry.EntityKey.EntityKeyValues.ToArray()[0].Value;
                //TODO: get parent visit here to update it's date
                //LogDeleteChange(id, "VisitSet", EntityState.Modified);
                LogDeleteChange(id, stateEntry.EntitySet.Name, stateEntry.State);
            }
            else
            {
            }
        }

        private void LogAddModifyChange(Guid id, string entityName, EntityState state)
        {
            //Change change = _dataContext.LoadByKey<Change>("ID", id);
            if (!CheckObjectStateManager<Change>("ID", id))
            {
                //TODO: we need to check the Change table for the existing record. 

                int changeCount =
                              (from change in _dataContext.ChangeSet
                              where change.ID == id
                              select change).Count();
                if (changeCount == 0)
                {
                    Change change = new Change()
                    {
                        ID = id,
                        EntitySet = entityName,
                        State = state.ToString(),
                        Timestamp = DateTime.Now.ToUniversalTime()
                    };
                    _dataContext.SaveChanges();
                    _dataContext.AddToChangeSet(change);
                }

            }
        }

        private void LogDeleteChange(Guid id, string entityName, EntityState state)
        {
            Change change = _dataContext.LoadByKey<Change>("ID", id);

            if (change == null)
            {
                change = new Change()
                {
                    ID = id,
                    EntitySet = entityName,
                    State = state.ToString(),
                    Timestamp = DateTime.Now.ToUniversalTime()
                };
                _dataContext.AddToChangeSet(change);
            }
            else
            {
                change.State = EntityState.Deleted.ToString();
            }
        }

        private bool CheckObjectStateManager<T>(string propertyName, object keyValue)
        {
            IEnumerable<KeyValuePair<string, object>> entityKeyValues =
               new KeyValuePair<string, object>[] { new KeyValuePair<string, object>(propertyName, keyValue) };
            string entitySetName = _dataContext.GetType().Name + "." + typeof(T).Name + "Set";
            EntityKey key = new EntityKey(entitySetName, entityKeyValues);
            ObjectStateEntry entry = null;

            return _dataContext.ObjectStateManager.TryGetObjectStateEntry(key, out entry);
        }

        /// <summary>
        /// Saves changes initiated by server. Removes appropriate entries in the Changes log table.
        /// </summary>
        public void SaveServerChanges()
        {
            IEnumerable<ObjectStateEntry> stateEntries = _dataContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified | EntityState.Deleted);
            foreach (ObjectStateEntry entry in stateEntries)
            {
                if (!entry.IsRelationship && entry.EntitySet.Name != "ChangeSet")
                {
#if DEBUG
                    string id = "";
                    if (entry.EntityKey.EntityKeyValues != null)
                        id = entry.EntityKey.EntityKeyValues.ToArray()[0].Value.ToString();
                    Debug.WriteLine("SaveServerChanges: " + entry.EntityKey.EntitySetName + " -- " + entry.State + " -- " + id);
#endif
                }
            }

            SaveChanges_Synchronized();
        }
    }
}
