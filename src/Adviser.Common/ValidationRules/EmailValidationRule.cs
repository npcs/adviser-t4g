﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Adviser.Common.ValidationRules
{
    public class EmailValidationRule : ValidationRule
    {
        public bool IsEmptyAllowed { get; set; }

        private bool IsMatch(string emailAddress)
        {
            string patternStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
                  + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                  + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                  + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                  + @"[a-zA-Z]{2,}))$";
            Regex reStrict = new Regex(patternStrict);
            return reStrict.IsMatch(emailAddress);
        }

        public override ValidationResult Validate(object value,
          CultureInfo cultureInfo)
        {
            if (!this.IsEmptyAllowed && (value == null || ((String)value).Trim().Equals(String.Empty)))
                return new ValidationResult(false, "E-mail is not valid.");

            if (IsMatch((string)value))
                return ValidationResult.ValidResult;
            else
                return new ValidationResult(false, "E-mail is not valid..");

        }
    }
}
