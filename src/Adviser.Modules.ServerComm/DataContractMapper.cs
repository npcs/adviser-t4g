﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

using Adviser.Client.DataModel;
using DataContracts = Adviser.Server.Services.Contracts;
using EntityModel = Adviser.Client.DataModel;

namespace Adviser.Modules.ServerComm
{
    /// <summary>
    /// Maps entities from/to DataContracts types.
    /// </summary>
    public class DataContractMapper
    {
        private EntityModel::AdviserDBEntities _dbContext;

        public DataContractMapper()
        {
            _dbContext = new EntityModel::AdviserDBEntities();
        }

        public DataContractMapper(EntityModel::AdviserDBEntities dbContext)
        {
            _dbContext = dbContext;
        }

        public DataContracts::Patient MapToDataContract(EntityModel::Patient inPatient)
        {
            DataContracts::Patient outPatient = new DataContracts::Patient();

            outPatient.City = inPatient.City;
            outPatient.Country = inPatient.Country;
            outPatient.Deleted = inPatient.Deleted;
            outPatient.DOB = inPatient.DOB;
            outPatient.Email = inPatient.Email;
            outPatient.FirstName = inPatient.FirstName;
            outPatient.Height = inPatient.Height;
            outPatient.ID = inPatient.ID;
            outPatient.LastName = inPatient.LastName;
            outPatient.LastUpdDate = inPatient.LastUpdDate;
            outPatient.Lifestyle = inPatient.Lifestyle;
            outPatient.MajorComplain = inPatient.MajorComplain;
            outPatient.MajorIllnessId = inPatient.MajorIllnessId;
            outPatient.MidName = inPatient.MidName;
            outPatient.Occupation = inPatient.Occupation;
            outPatient.PostalCode = inPatient.PostalCode;
            outPatient.PrimaryPhone = inPatient.PrimaryPhone;
            outPatient.SecondaryPhone = inPatient.SecondaryPhone;
            outPatient.Sex = inPatient.Sex;
            outPatient.StateProvince = inPatient.StateProvince;
            outPatient.StreetAddress = inPatient.StreetAddress;
            outPatient.Weight = inPatient.Weight;

            outPatient.Visits = new DataContracts::Visit[inPatient.Visits.Count];
            int idx = 0;
            foreach (EntityModel::Visit inVisit in inPatient.Visits)
            {
                DataContracts::Visit outVisit = MapToDataContract(inVisit);
                outPatient.Visits[idx] = outVisit;
                ++idx;
            }

            return outPatient;
        }

        /// <summary>
        /// Maps Visit entity to DataContracts Visit.
        /// </summary>
        /// <param name="inVisit"></param>
        /// <returns></returns>
        public DataContracts::Visit MapToDataContract(EntityModel::Visit inVisit)
        {
            DataContracts::Visit outVisit = new DataContracts::Visit();

            outVisit.ID = inVisit.ID;
            outVisit.MeasurementData = inVisit.MeasurementData;
            outVisit.ExamResult = inVisit.ExamResult;
            outVisit.LastUpdDate = inVisit.LastUpdDate;
            outVisit.Notes = inVisit.Notes;
            outVisit.Date = inVisit.Date;
            outVisit.Deleted = Convert.ToBoolean(inVisit.Deleted);

            int idx = 0;
            outVisit.OrganExaminations = new DataContracts::OrganExamination[inVisit.OrganExaminations.Count];
            foreach (EntityModel::OrganExamination inOrganExam in inVisit.OrganExaminations)
            {
                DataContracts::OrganExamination outOrganExam = MapToDataContract(inOrganExam);
                outVisit.OrganExaminations[idx] = outOrganExam;
                ++idx;
            }

            idx = 0;
            outVisit.Symptoms = new DataContracts::Symptom[inVisit.Symptoms.Count];
            foreach (EntityModel::Symptom inSymptom in inVisit.Symptoms)
            {
                DataContracts::Symptom outSymptom = MapToDataContract(inSymptom);
                outVisit.Symptoms[idx] = outSymptom;
                ++idx;
            }

            idx = 0;
            outVisit.Prescriptions = new DataContracts::Prescription[inVisit.Prescriptions.Count];
            foreach (EntityModel::Prescription inPrescription in inVisit.Prescriptions)
            {
                DataContracts::Prescription outPrescription = MapToDataContract(inPrescription);
                outVisit.Prescriptions[idx] = outPrescription;
                ++idx;
            }

            return outVisit;
        }

        public DataContracts::Prescription MapToDataContract(EntityModel::Prescription inPrescription)
        {
            DataContracts::Prescription outPrescription = new DataContracts::Prescription();
            outPrescription.Directions = inPrescription.Directions;
            outPrescription.ID = inPrescription.ID;
            outPrescription.MedicineID = inPrescription.MedicineID;
            outPrescription.MedicineName = inPrescription.MedicineName;
            outPrescription.Deleted = inPrescription.Deleted;

            return outPrescription;
        }

        public DataContracts::OrganExamination MapToDataContract(EntityModel::OrganExamination inOrganExam)
        {
            DataContracts::OrganExamination outOrganExam = new DataContracts::OrganExamination();
            outOrganExam.ID = inOrganExam.ID;
            outOrganExam.Implant = inOrganExam.Implant;          
            outOrganExam.Removed = inOrganExam.Removed;
            outOrganExam.Surgery = inOrganExam.Surgery;
            inOrganExam.OrganReference.Load();
            outOrganExam.OrganID = inOrganExam.Organ.ID;

            return outOrganExam;
        }

        public DataContracts::Symptom MapToDataContract(EntityModel::Symptom inSymptom)
        {
            DataContracts::Symptom outSymptom = new DataContracts::Symptom();
            outSymptom.Description = inSymptom.Description;
            outSymptom.ID = inSymptom.ID;
            outSymptom.Name = inSymptom.Name;
            outSymptom.OrderKey = inSymptom.OrderKey;

            return outSymptom;
        }

        public DataContracts::Practitioner MapToDataContract(EntityModel::Practitioner inPractitioner)
        {
            DataContracts::Practitioner outPractitioner = new DataContracts::Practitioner();

            outPractitioner.City = inPractitioner.City;
            outPractitioner.Country = inPractitioner.Country;
            outPractitioner.BusinessName = inPractitioner.BusinessName;
            outPractitioner.Fax = inPractitioner.Fax;
            outPractitioner.Email = inPractitioner.Email;
            outPractitioner.FirstName = inPractitioner.FirstName;
            outPractitioner.Occupation = inPractitioner.Occupation;
            outPractitioner.ID = inPractitioner.ID;
            outPractitioner.LastName = inPractitioner.LastName;
            outPractitioner.LastUpdDate = inPractitioner.LastUpdDate;
            outPractitioner.Password = inPractitioner.Password;
            outPractitioner.StatusID = (short)inPractitioner.Status;
            outPractitioner.TypeOfPractice = inPractitioner.TypeOfPractice;
            outPractitioner.MidName = inPractitioner.MidName;
            outPractitioner.Occupation = inPractitioner.Occupation;
            outPractitioner.PostalCode = inPractitioner.PostalCode;
            outPractitioner.PrimaryPhone = inPractitioner.PrimaryPhone;
            outPractitioner.SecondaryPhone = inPractitioner.SecondaryPhone;
            outPractitioner.StreetAddress = inPractitioner.StreetAddress;
            outPractitioner.StateProvince = inPractitioner.StateProvince;

            return outPractitioner;
        }

        public DataContracts::Patient FlatCopyToDataContract(EntityModel::Patient inPatient, DataContracts::Patient outPatient)
        {
            outPatient.City = inPatient.City;
            outPatient.Country = inPatient.Country;
            outPatient.Deleted = inPatient.Deleted;
            outPatient.DOB = inPatient.DOB;
            outPatient.Email = inPatient.Email;
            outPatient.FirstName = inPatient.FirstName;
            outPatient.Height = inPatient.Height;
            outPatient.ID = inPatient.ID;
            outPatient.LastName = inPatient.LastName;
            outPatient.LastUpdDate = inPatient.LastUpdDate;
            outPatient.Lifestyle = inPatient.Lifestyle;
            outPatient.MajorComplain = inPatient.MajorComplain;
            outPatient.MajorIllnessId = inPatient.MajorIllnessId;
            outPatient.MidName = inPatient.MidName;
            outPatient.Occupation = inPatient.Occupation;
            outPatient.PostalCode = inPatient.PostalCode;
            outPatient.PrimaryPhone = inPatient.PrimaryPhone;
            outPatient.SecondaryPhone = inPatient.SecondaryPhone;
            outPatient.Sex = inPatient.Sex;
            outPatient.StateProvince = inPatient.StateProvince;
            outPatient.StreetAddress = inPatient.StreetAddress;
            outPatient.Weight = inPatient.Weight;

            return outPatient;
        }

        public DataContracts::AdviserClientLogEntry MapToDataContract(Adviser.Common.Logging.AdviserLogEntry inLogEntry)
        {
            DataContracts::AdviserClientLogEntry outEntry = new Adviser.Server.Services.Contracts.AdviserClientLogEntry();
            outEntry.Application = inLogEntry.Application;
            outEntry.Category = inLogEntry.Category;
            outEntry.DeviceId = inLogEntry.DeviceId;
            outEntry.ErrorMessage = inLogEntry.ErrorMessage;
            outEntry.Details = inLogEntry.Details;
            outEntry.LogItemType = inLogEntry.LogItemType;
            outEntry.PractitionerId = inLogEntry.PractitionerId;
            outEntry.Severity = Convert.ToByte(inLogEntry.Severity);
            outEntry.Time = inLogEntry.Time;
            outEntry.VisistId = inLogEntry.VisitId;

            return outEntry;
        }

        public EntityModel::Visit MapToEntity(DataContracts::Visit inVisit)
        {
            EntityModel::Visit outVisit = EntityModel::Visit.CreateVisit(inVisit.ID, inVisit.Date, inVisit.LastUpdDate);

            outVisit.MeasurementData = inVisit.MeasurementData;
            outVisit.ExamResult = inVisit.ExamResult;
            outVisit.Notes = inVisit.Notes;
            outVisit.Deleted = inVisit.Deleted;

            foreach (DataContracts::OrganExamination inOrganExam in inVisit.OrganExaminations)
            {
                EntityModel::OrganExamination outOrganExam = MapToEntity(inOrganExam);
                outVisit.OrganExaminations.Add(outOrganExam);
            }

            foreach (DataContracts::Symptom inSymptom in inVisit.Symptoms)
            {
                EntityModel::Symptom outSymptom = MapToEntity(inSymptom);
                outVisit.Symptoms.Add(outSymptom);
            }

            foreach (DataContracts::Prescription inPrescription in inVisit.Prescriptions)
            {
                EntityModel::Prescription outPrescription = MapToEntity(inPrescription);
                outVisit.Prescriptions.Add(outPrescription);
            }

            return outVisit;
        }

        public EntityModel::Prescription MapToEntity(DataContracts::Prescription inPrescription)
        {
            EntityModel::Prescription outPrescription = new EntityModel::Prescription();
            outPrescription.Directions = inPrescription.Directions;
            outPrescription.ID = inPrescription.ID;
            outPrescription.MedicineID = inPrescription.MedicineID;
            outPrescription.MedicineName = inPrescription.MedicineName;

            return outPrescription;
        }

        public EntityModel::OrganExamination MapToEntity(DataContracts::OrganExamination organExamIn)
        {
            EntityModel::OrganExamination organExamOut =
                EntityModel::OrganExamination.CreateOrganExamination(organExamIn.ID,
                organExamIn.Value,
                organExamIn.Surgery,
                organExamIn.ChronicDisease,
                organExamIn.Removed,
                organExamIn.Implant);

            //organExamOut.ChronicDisease = organExamIn.ChronicDisease;
            //organExamOut.ID = organExamIn.ID;
            //organExamOut.Implant = organExamIn.Implant;
            //organExamOut.Removed = organExamIn.Removed;
            //organExamOut.Surgery = organExamIn.Surgery;
            //organExamOut.Value = organExamIn.Value;

            organExamOut.Organ = _dbContext.LoadByKey<Organ>("ID", organExamIn.OrganID);

            return organExamOut;
        }

        public EntityModel::Symptom MapToEntity(DataContracts::Symptom symptomIn)
        {
            EntityModel::Symptom symptomOut = _dbContext.LoadByKey<EntityModel::Symptom>("ID", symptomIn.ID);//new EntityModel::Symptom();

            symptomOut.Name = symptomIn.Name;
            symptomOut.OrderKey = symptomIn.OrderKey;
            symptomOut.Description = symptomIn.Description;

            return symptomOut;
        }

        public EntityModel::Visit FlatCopyToEntity(DataContracts::Visit inVisit, EntityModel::Visit outVisit)
        {
            //outVisit.ID = inVisit.ID;
            outVisit.MeasurementData = inVisit.MeasurementData;
            outVisit.ExamResult = inVisit.ExamResult;
            outVisit.LastUpdDate = inVisit.LastUpdDate;
            outVisit.Notes = inVisit.Notes;
            outVisit.Date = inVisit.Date;

            return outVisit;
        }

        public EntityModel::Prescription FlatCopyToEntity(DataContracts::Prescription inPrescription, EntityModel::Prescription outPrescription)
        {
            outPrescription.Directions = inPrescription.Directions;
            outPrescription.MedicineID = inPrescription.MedicineID;
            outPrescription.MedicineName = inPrescription.MedicineName;

            return outPrescription;
        }

        //public EntityModel::Organ MapToEntity(DataContracts::Organ inOrgan)
        //{
        //    EntityModel::Organ outOrgan = _dbContext.LoadByKey<EntityModel::Organ>("ID", inOrgan.ID);

        //    outOrgan.ID = inOrgan.ID;
        //    outOrgan.Name = inOrgan.Name;
        //    outOrgan.OrderKey = inOrgan.OrderKey;
        //    outOrgan.Gender = inOrgan.Gender;
        //    outOrgan.Description = inOrgan.Description;
        //    outOrgan.Abbreviation = inOrgan.Abbreviation;

        //    return outOrgan;
        //}
    }
}
