﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Data;
using System.Data.Common;
using System.Data.Objects;
using System.Linq;
using Adviser.Server.DataModel;
using Adviser.Server.Core.Services;

namespace Adviser.Server.Services
{
    /// <summary>
    /// Provides validation methods for Adviser services authentication.
    /// </summary>
    public class CredentialsValidator
    {
        public void ValidateLogin(string email, string password)
        {
            using (AdviserDBEntities dbContext = new AdviserDBEntities())
            {
                List<Practitioner> result = dbContext.GetPractitionerByEmail(email).ToList();
                if (result.Count == 0)
                    throw new AdviserSecurityException("Invalid username.");

                Practitioner practitioner = result[0];
                if (practitioner.Password != password)
                    throw new AdviserSecurityException("Invalid password.");
            }
        }

        public void ValidatePractitioner(string email, string password, Guid practitionerId)
        {
            using (AdviserDBEntities dbContext = new AdviserDBEntities())
            {
                List<Practitioner> result = dbContext.GetPractitionerByEmail(email).ToList();
                if (result.Count == 0)
                    throw new AdviserSecurityException("Invalid username.");

                Practitioner practitioner = result[0];
                if (practitioner.Password != password)
                    throw new AdviserSecurityException("Invalid password.");

                if (practitioner.ID != practitionerId)
                    throw new AdviserSecurityException("Invalid ID.");
            }
        }

        public void ValidatePractitionerDevice(Practitioner practitioner, Guid deviceId)
        {
            using (AdviserDBEntities dbContext = new AdviserDBEntities())
            {
                List<Device> devices = dbContext.GetDeviceByID(deviceId).ToList();
                if (devices.Count == 0)
                    throw new AdviserSecurityException("Invalid device.");

                if (devices[0].OwnerID != practitioner.ID)
                    throw new AdviserSecurityException("Device is not authorized.");
            }
        }
    }
}
