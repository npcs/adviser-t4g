﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;
using Advisor.Modules.Readout.PresentationModels;
namespace Advisor.Modules.Readout.Views
{
    public interface IReadoutView
    {
        IDataMatrixViewModel Model { get; set; }
        event EventHandler<RoutedEventArgs> SaveFile;
        event EventHandler<RoutedEventArgs> Reset;
        void ShowMatrix(IDataMatrixView matrixView);
        void ShowPairDetails(IPairDetailsView pairDetailsView);
        void HidePairDetails();
    }
}
