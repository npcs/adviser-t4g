﻿using System;
using System.Collections.Generic;

namespace Adviser.Client.DataModel
{
    public partial class Symptom
    {
        public bool IsEditable
        {
            get;
            set;
        }
    }
}
