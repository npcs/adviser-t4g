﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Data.Objects.DataClasses;

using Adviser.Client.Core.Presentation;
using Adviser.Client.Core.Services;
using Adviser.Client.DataModel;
using Adviser.Common.Domain;

namespace Advisor.Modules.Interpretation
{
    public class AuraViewModel
    {
        private List<Chakra> _chakras;

        public AuraViewModel(IDBService dbService)
        {
            _chakras = dbService.GetAllChakras().ToList();
        }

        public ObservableCollection<int> ChakraLevels
        {
            get;
            set;
        }

        public ObservableCollection<ChakraViewModel> Chakras
        {
            get;
            set;
        }

        public string WorstChakraStatus
        {
            get;
            set;
        }

        public string BodyImage { get; set; }

        public MandalaViewModel Mandala
        {
            get;
            set;
        }

        public void SetData(AdviserResults results, string sex)
        {
            BodyImage = String.Concat("/Advisor.Modules.Interpretation;component/Resources/Body_ChakraView_", sex, ".png");

            //- set chakras conditions
            if (results != null && results.ChakrasCondition != null)
            {
                ObservableCollection<ChakraViewModel> chakras = new ObservableCollection<ChakraViewModel>();

                //- The following values are need to calculate ellipse shift
                int totalPoints = 11;
                //double ChakrasCountShift = (results.ChakrasCondition.Count - 1) / 2;
                double chakrasCountShift = (totalPoints - 1) / 2;
                double AtoBRatio = chakrasCountShift + 1;
                int worstChakraDelta = 0; //- to determine the worst chakra
                int worstChakraIdx = 0; //- index of the worst chakra

                chakras.Add(new ChakraViewModel("ss", 0.1, ""));
                for (int i = 0; i < totalPoints; i++)
                {
                    ChakraViewModel chakraVM = new ChakraViewModel();
                    if (i < results.ChakrasCondition.Count)
                    {
                        int currentDelta = Math.Abs(100 - results.ChakrasCondition[i]);
                        if (currentDelta > worstChakraDelta)
                        {
                            worstChakraDelta = currentDelta;
                            worstChakraIdx = i;
                        }
                        chakraVM.Delta = EllipseDelta(i - chakrasCountShift, AtoBRatio) * results.ChakrasCondition[i];
                        chakraVM.Name = _chakras[i].Name;

                        chakraVM.Description = "";
                    }
                    else
                    {
                        chakraVM.Delta = EllipseDelta(i - chakrasCountShift, AtoBRatio) * 100;
                    }

                    chakras.Add(chakraVM);
                }

                chakras.Add(new ChakraViewModel("ss", 0.1, ""));

                Chakras = chakras;

                string chakraStatus = String.Concat("Most affected chakra ", _chakras[worstChakraIdx].Name, " is");
                if (results.ChakrasCondition[worstChakraIdx] > 100)
                {
                    chakraStatus += " overloaded.\nIf condition was present for a long time the patient may form the following characteristics:\n";
                    WorstChakraStatus = chakraStatus + _chakras[worstChakraIdx].AboveDesc;
                }
                else
                {
                    chakraStatus += " exhausted.\nIf condition was present for a long time the patient may form the following characteristics:\n";
                    WorstChakraStatus = chakraStatus + _chakras[worstChakraIdx].BelowDesc;
                }

                Mandala = new MandalaViewModel(_chakras[worstChakraIdx].Name, _chakras[worstChakraIdx].MandalaImage, 15, 
                    _chakras[worstChakraIdx].ShortDescription, _chakras[worstChakraIdx].MandalaSound,
                    _chakras[worstChakraIdx].Recommendations, _chakras[worstChakraIdx].ExpectedResults, _chakras[worstChakraIdx].Influence);
            }
        }

        private double EllipseDelta(double x, double AtoBRatio)
        {
            return Math.Sqrt(1 - x * x / (AtoBRatio * AtoBRatio));
        }
    }
}
