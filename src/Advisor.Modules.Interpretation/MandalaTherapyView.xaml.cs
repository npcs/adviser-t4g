﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;

namespace Advisor.Modules.Interpretation
{
    /// <summary>
    /// Interaction logic for MandalaTherapyView.xaml
    /// </summary>
    public partial class MandalaTherapyView : UserControl, IMandalaTherapyView
    {
        public MandalaTherapyView()
        {
            InitializeComponent();   
        }

        #region IMandalaTherapyView Members

        public IMandalaViewModel Model
        {
            get
            {
                return this.DataContext as IMandalaViewModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        #endregion

        private void MediaElement_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            //MessageTextBlock.Text = "Media Player error: " + e.ErrorException.Message;
            //ErrorMessagePopup.IsOpen = true;
            //ErrorMessagePopup.StaysOpen = false;
        }

        private void btnIgnore_Click(object sender, RoutedEventArgs e)
        {
            //ErrorMessagePopup.IsOpen = false;
        }

       
    }
}
