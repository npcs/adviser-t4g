﻿using System;

namespace Adviser.Common.Logging
{
    public interface ILogger
    {
        string Application { get; set; }
        bool Enabled { get; set; }
        LogEntryFilter Filter { get; set; }
        LogEntryFormatter Formatter { get; set; }
        LogSeverity SeverityThreshold { get; set; }
        ILogEntry GetLogEntry(LogSeverity severity, string category, Object logItem, string errorMessage);
        void Log(ILogEntry logEntry);
        void Log(LogSeverity severity, string category, object logItem);
        void Log(LogSeverity severity, object logItem);
        void LogDebug(string category, object logItem);
        void LogDebug(object logItem);
        void LogInfo(string category, object logItem);
        void LogInfo(object logItem);
        void LogStatus(object logItem);
        void LogStatus(string category, object logItem);
        void LogWarning(object logItem);
        void LogWarning(string category, object logItem);
        void LogError(object logItem);
        void LogError(object logItem, string errorMessage);
        void LogError(string category, object logItem);
        void LogError(string category, object logItem, string errorMessage);
    }
}
