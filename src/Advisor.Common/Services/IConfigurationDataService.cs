﻿using System;
using System.Collections.Generic;

using Advisor.Common.Configuration;
using Advisor.Common.DataModel;

namespace Advisor.Common.Services
{
    public interface IConfigurationDataService
    {
        XmlDictionary<string, string> LoadXmlSettings(string settingsName);
        MeasurementSettings GetMeasurementSettings();
        DeviceSettings GetDeviceSettings();
    }
}
