﻿using System;
using System.Collections.Generic;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;

using Adviser.Client.Core.Presentation;
using Adviser.Client.Core.Services;
using Adviser.Modules.PatientDatabase;
using Advisor.Modules.Interpretation.Views;
using Advisor.Modules.Interpretation.PresentationModels;

namespace Adviser.Modules.Print
{
    public class PrintModule : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public PrintModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            RegisterViewsAndServices();

            IRegion contentRegion = _regionManager.Regions[RegionNames.ContentRegion];
            PrintReportPresenter presenter = _container.Resolve<PrintReportPresenter>();
            contentRegion.Add(presenter.View, "IPrintReportView");
            contentRegion.Deactivate(presenter.View);

            //IRegion printRegion = _regionManager.Regions[RegionNames.PrintRegion];
            //IHumanBodyViewModel model = _container.Resolve<IHumanBodyViewModel>();
            //printRegion.Add(model.View);
            //IPatientsPresenter patientsPresenter = _container.Resolve<IPatientsPresenter>();
            //region.Add(patientsPresenter.View, "IPatientsView");
            //region.Deactivate(patientsPresenter.View);

            //IVisitsPresenter visitsPresenter = _container.Resolve<IVisitsPresenter>();
            //region.Add(visitsPresenter.View, "IVisitsView");
            //region.Deactivate(visitsPresenter.View);
        }

        protected void RegisterViewsAndServices()
        {
            _container.RegisterType<IPrintReportView, PrintReportView>();
            //_container.RegisterType<IDBService, DBService>();

            //_container.RegisterType<IPrintableModel, VisitsPresentationModel>("visitsPrint");
            //_container.RegisterType<IPrintableModel, PatientsPresentationModel>("visitsPrint");
            //_container.RegisterType<VisitDetailsPrintView, VisitDetailsPrintView>();

            //IEnumerable<IPrintableModel> p = _container.ResolveAll<IPrintableModel>();

            ////IPrintableModel v1 = _container.Resolve<IPrintableModel>();

            //foreach (IPrintableModel printModel in p)
            //{
            //    IPrintableModel vm = printModel;
            //}
        }
    }
}
