﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Advisor.Common.DataModel;

namespace Advisor.Core
{
    public class DeviceEmulator : IDeviceDriver
    {
        #region IDeviceDriver Members

        public bool ConnectElectrodes(byte front, byte back)
        {
            return true;
        }

        public bool SetPolarity(bool frontPositive)
        {
            return true;
        }

        public bool SetupDevice(DeviceSettings settings)
        {
            return true;
        }

        public ushort ReadData()
        {
            Random rand = new Random(DateTime.Now.Millisecond);
            return Convert.ToUInt16(rand.Next(0, 330));     
        }

        public ushort ReadBusVoltage()
        {
            return 3300;
        }

        public bool GetDeviceGUID(ref Guid DeviceGUID)
        {
            DeviceGUID = System.Guid.Empty;
            return true;
        }

        public bool GetBridgeVersion(ref string BridgeVersion)
        {
            return true;
        }

        public bool IsConnected
        {
            get { return true; }
        }

        public void Release()
        {
        }

        #endregion
    }
}
