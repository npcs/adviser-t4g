
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 04/09/2012 19:28:21
-- Generated from EDMX file: C:\Users\genpetrp\Projects\IKOSoftware\Advisor\AdvisorClient\Adviser.Client.DataModel\ClientDataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [AdviserDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_OrganExamination_Organs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [FK_OrganExamination_Organs];
GO
IF OBJECT_ID(N'[dbo].[FK_OrganExamination_Visits1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [FK_OrganExamination_Visits1];
GO
IF OBJECT_ID(N'[dbo].[FK_Patient_Practitioner]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Patients] DROP CONSTRAINT [FK_Patient_Practitioner];
GO
IF OBJECT_ID(N'[dbo].[FK_Prescriptions_Visit]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Prescriptions] DROP CONSTRAINT [FK_Prescriptions_Visit];
GO
IF OBJECT_ID(N'[AdviserClientDataModelStoreContainer].[FK_SymptomVisitXRef_Symptoms ]', 'F') IS NOT NULL
    ALTER TABLE [AdviserClientDataModelStoreContainer].[SymptomVisitXRef] DROP CONSTRAINT [FK_SymptomVisitXRef_Symptoms ];
GO
IF OBJECT_ID(N'[AdviserClientDataModelStoreContainer].[FK_SymptomVisitXRef_Visit]', 'F') IS NOT NULL
    ALTER TABLE [AdviserClientDataModelStoreContainer].[SymptomVisitXRef] DROP CONSTRAINT [FK_SymptomVisitXRef_Visit];
GO
IF OBJECT_ID(N'[dbo].[FK_Visit_Patient]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Visits] DROP CONSTRAINT [FK_Visit_Patient];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Chakras]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Chakras];
GO
IF OBJECT_ID(N'[dbo].[Changes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Changes];
GO
IF OBJECT_ID(N'[dbo].[Conditions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Conditions];
GO
IF OBJECT_ID(N'[AdviserClientDataModelStoreContainer].[Electrodes]', 'U') IS NOT NULL
    DROP TABLE [AdviserClientDataModelStoreContainer].[Electrodes];
GO
IF OBJECT_ID(N'[dbo].[Errors]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Errors];
GO
IF OBJECT_ID(N'[dbo].[Medicine]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Medicine];
GO
IF OBJECT_ID(N'[dbo].[OrganExamination]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrganExamination];
GO
IF OBJECT_ID(N'[dbo].[Organs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organs];
GO
IF OBJECT_ID(N'[dbo].[Patients]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Patients];
GO
IF OBJECT_ID(N'[dbo].[Practitioners]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Practitioners];
GO
IF OBJECT_ID(N'[dbo].[Prescriptions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Prescriptions];
GO
IF OBJECT_ID(N'[dbo].[Symptoms ]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Symptoms ];
GO
IF OBJECT_ID(N'[AdviserClientDataModelStoreContainer].[SymptomVisitXRef]', 'U') IS NOT NULL
    DROP TABLE [AdviserClientDataModelStoreContainer].[SymptomVisitXRef];
GO
IF OBJECT_ID(N'[dbo].[SyncItems]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SyncItems];
GO
IF OBJECT_ID(N'[dbo].[Visits]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Visits];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ConditionSet'
CREATE TABLE [dbo].[ConditionSet] (
    [ID] int  NOT NULL,
    [Name] nvarchar(250)  NOT NULL,
    [MinRange] int  NULL,
    [MaxRange] int  NULL,
    [OrderKey] int  NOT NULL,
    [ColorCode] nvarchar(20)  NOT NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'MedicineSet'
CREATE TABLE [dbo].[MedicineSet] (
    [ID] int  NOT NULL,
    [Name] nvarchar(250)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Direction] nvarchar(max)  NULL
);
GO

-- Creating table 'OrganExaminationSet'
CREATE TABLE [dbo].[OrganExaminationSet] (
    [ID] uniqueidentifier  NOT NULL,
    [Value] int  NOT NULL,
    [Surgery] bit  NOT NULL,
    [ChronicDisease] bit  NOT NULL,
    [Removed] bit  NOT NULL,
    [Implant] bit  NOT NULL,
    [Organ_ID] int  NOT NULL,
    [Visit_ID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'OrganSet'
CREATE TABLE [dbo].[OrganSet] (
    [ID] int  NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [Gender] int  NULL,
    [Description] nvarchar(200)  NULL,
    [OrderKey] int  NOT NULL,
    [Abbreviation] nvarchar(max)  NULL
);
GO

-- Creating table 'PatientSet'
CREATE TABLE [dbo].[PatientSet] (
    [ID] uniqueidentifier  NOT NULL,
    [LastUpdDate] datetime  NOT NULL,
    [FirstName] nvarchar(50)  NOT NULL,
    [LastName] nvarchar(50)  NOT NULL,
    [MidName] nvarchar(50)  NULL,
    [StreetAddress] nvarchar(100)  NULL,
    [City] nvarchar(50)  NULL,
    [PostalCode] nvarchar(10)  NULL,
    [Country] nvarchar(50)  NULL,
    [Occupation] nvarchar(100)  NULL,
    [PrimaryPhone] nvarchar(50)  NULL,
    [SecondaryPhone] nvarchar(50)  NULL,
    [Email] nvarchar(100)  NULL,
    [DOB] datetime  NULL,
    [Sex] nchar(1)  NOT NULL,
    [Height] int  NULL,
    [Weight] int  NULL,
    [Lifestyle] nvarchar(100)  NULL,
    [MajorComplain] nvarchar(max)  NULL,
    [MajorIllnessId] int  NULL,
    [Deleted] bit  NOT NULL,
    [StateProvince] nvarchar(max)  NULL,
    [Practitioners_ID] uniqueidentifier  NULL
);
GO

-- Creating table 'PractitionerSet'
CREATE TABLE [dbo].[PractitionerSet] (
    [ID] uniqueidentifier  NOT NULL,
    [FirstName] nvarchar(50)  NOT NULL,
    [LastName] nvarchar(50)  NOT NULL,
    [MidName] nvarchar(50)  NULL,
    [BusinessName] nvarchar(50)  NULL,
    [TypeOfPractice] nvarchar(100)  NOT NULL,
    [StreetAddress] nvarchar(100)  NOT NULL,
    [City] nvarchar(50)  NOT NULL,
    [PostalCode] nvarchar(10)  NOT NULL,
    [Country] nvarchar(50)  NOT NULL,
    [Occupation] nvarchar(50)  NOT NULL,
    [PrimaryPhone] nvarchar(50)  NOT NULL,
    [SecondaryPhone] nvarchar(50)  NULL,
    [Fax] nvarchar(50)  NULL,
    [Email] nvarchar(100)  NULL,
    [Password] nvarchar(50)  NULL,
    [StatusID] smallint  NOT NULL,
    [LastUpdDate] datetime  NOT NULL,
    [StateProvince] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'SymptomSet'
CREATE TABLE [dbo].[SymptomSet] (
    [ID] int  NOT NULL,
    [Name] nvarchar(250)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [OrderKey] int  NOT NULL
);
GO

-- Creating table 'VisitSet'
CREATE TABLE [dbo].[VisitSet] (
    [ID] uniqueidentifier  NOT NULL,
    [Date] datetime  NOT NULL,
    [LastUpdDate] datetime  NOT NULL,
    [MeasurementData] varbinary(max)  NULL,
    [ExamResult] varbinary(max)  NULL,
    [Notes] nvarchar(max)  NULL,
    [Deleted] bit  NULL,
    [Patient_ID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'PrescriptionSet'
CREATE TABLE [dbo].[PrescriptionSet] (
    [ID] uniqueidentifier  NOT NULL,
    [MedicineID] int  NULL,
    [MedicineName] nvarchar(max)  NULL,
    [Directions] nvarchar(max)  NULL,
    [Visits_ID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'ChakraSet'
CREATE TABLE [dbo].[ChakraSet] (
    [ID] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [MandalaImage] nvarchar(max)  NULL,
    [MandalaSound] nvarchar(max)  NULL,
    [ShortDescription] nvarchar(max)  NULL,
    [AboveDesc] nvarchar(max)  NULL,
    [BelowDesc] nvarchar(max)  NULL,
    [Recommendations] nvarchar(max)  NULL,
    [ExpectedResults] nvarchar(max)  NULL,
    [Influence] nvarchar(max)  NULL
);
GO

-- Creating table 'SyncItems'
CREATE TABLE [dbo].[SyncItems] (
    [KeyValue] uniqueidentifier  NOT NULL,
    [EntityType] nvarchar(max)  NOT NULL,
    [ActionId] tinyint  NOT NULL,
    [StatusId] tinyint  NOT NULL,
    [Timestamp] datetime  NOT NULL
);
GO

-- Creating table 'ErrorSet'
CREATE TABLE [dbo].[ErrorSet] (
    [ID] int  NOT NULL,
    [Time] datetime  NULL,
    [Data] varbinary(max)  NULL,
    [Lock] bit  NOT NULL
);
GO

-- Creating table 'ChangeSet'
CREATE TABLE [dbo].[ChangeSet] (
    [ID] uniqueidentifier  NOT NULL,
    [EntitySet] nvarchar(max)  NOT NULL,
    [State] nvarchar(max)  NOT NULL,
    [Timestamp] datetime  NULL
);
GO

-- Creating table 'ElectrodeSet'
CREATE TABLE [dbo].[ElectrodeSet] (
    [Number] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [IsFront] bit  NOT NULL
);
GO

-- Creating table 'SymptomVisitXRef'
CREATE TABLE [dbo].[SymptomVisitXRef] (
    [Symptoms_ID] int  NOT NULL,
    [Visits_ID] uniqueidentifier  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'ConditionSet'
ALTER TABLE [dbo].[ConditionSet]
ADD CONSTRAINT [PK_ConditionSet]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MedicineSet'
ALTER TABLE [dbo].[MedicineSet]
ADD CONSTRAINT [PK_MedicineSet]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'OrganExaminationSet'
ALTER TABLE [dbo].[OrganExaminationSet]
ADD CONSTRAINT [PK_OrganExaminationSet]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'OrganSet'
ALTER TABLE [dbo].[OrganSet]
ADD CONSTRAINT [PK_OrganSet]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'PatientSet'
ALTER TABLE [dbo].[PatientSet]
ADD CONSTRAINT [PK_PatientSet]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'PractitionerSet'
ALTER TABLE [dbo].[PractitionerSet]
ADD CONSTRAINT [PK_PractitionerSet]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'SymptomSet'
ALTER TABLE [dbo].[SymptomSet]
ADD CONSTRAINT [PK_SymptomSet]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'VisitSet'
ALTER TABLE [dbo].[VisitSet]
ADD CONSTRAINT [PK_VisitSet]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'PrescriptionSet'
ALTER TABLE [dbo].[PrescriptionSet]
ADD CONSTRAINT [PK_PrescriptionSet]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'ChakraSet'
ALTER TABLE [dbo].[ChakraSet]
ADD CONSTRAINT [PK_ChakraSet]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [KeyValue] in table 'SyncItems'
ALTER TABLE [dbo].[SyncItems]
ADD CONSTRAINT [PK_SyncItems]
    PRIMARY KEY CLUSTERED ([KeyValue] ASC);
GO

-- Creating primary key on [ID] in table 'ErrorSet'
ALTER TABLE [dbo].[ErrorSet]
ADD CONSTRAINT [PK_ErrorSet]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'ChangeSet'
ALTER TABLE [dbo].[ChangeSet]
ADD CONSTRAINT [PK_ChangeSet]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [Number], [Name], [Description], [IsFront] in table 'ElectrodeSet'
ALTER TABLE [dbo].[ElectrodeSet]
ADD CONSTRAINT [PK_ElectrodeSet]
    PRIMARY KEY CLUSTERED ([Number], [Name], [Description], [IsFront] ASC);
GO

-- Creating primary key on [Symptoms_ID], [Visits_ID] in table 'SymptomVisitXRef'
ALTER TABLE [dbo].[SymptomVisitXRef]
ADD CONSTRAINT [PK_SymptomVisitXRef]
    PRIMARY KEY NONCLUSTERED ([Symptoms_ID], [Visits_ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Organ_ID] in table 'OrganExaminationSet'
ALTER TABLE [dbo].[OrganExaminationSet]
ADD CONSTRAINT [FK_OrganExamination_Organs]
    FOREIGN KEY ([Organ_ID])
    REFERENCES [dbo].[OrganSet]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OrganExamination_Organs'
CREATE INDEX [IX_FK_OrganExamination_Organs]
ON [dbo].[OrganExaminationSet]
    ([Organ_ID]);
GO

-- Creating foreign key on [Visit_ID] in table 'OrganExaminationSet'
ALTER TABLE [dbo].[OrganExaminationSet]
ADD CONSTRAINT [FK_OrganExamination_Visits1]
    FOREIGN KEY ([Visit_ID])
    REFERENCES [dbo].[VisitSet]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OrganExamination_Visits1'
CREATE INDEX [IX_FK_OrganExamination_Visits1]
ON [dbo].[OrganExaminationSet]
    ([Visit_ID]);
GO

-- Creating foreign key on [Practitioners_ID] in table 'PatientSet'
ALTER TABLE [dbo].[PatientSet]
ADD CONSTRAINT [FK_Patient_Practitioner]
    FOREIGN KEY ([Practitioners_ID])
    REFERENCES [dbo].[PractitionerSet]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Patient_Practitioner'
CREATE INDEX [IX_FK_Patient_Practitioner]
ON [dbo].[PatientSet]
    ([Practitioners_ID]);
GO

-- Creating foreign key on [Patient_ID] in table 'VisitSet'
ALTER TABLE [dbo].[VisitSet]
ADD CONSTRAINT [FK_Visit_Patient]
    FOREIGN KEY ([Patient_ID])
    REFERENCES [dbo].[PatientSet]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Visit_Patient'
CREATE INDEX [IX_FK_Visit_Patient]
ON [dbo].[VisitSet]
    ([Patient_ID]);
GO

-- Creating foreign key on [Symptoms_ID] in table 'SymptomVisitXRef'
ALTER TABLE [dbo].[SymptomVisitXRef]
ADD CONSTRAINT [FK_SymptomVisitXRef_Symptoms_]
    FOREIGN KEY ([Symptoms_ID])
    REFERENCES [dbo].[SymptomSet]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Visits_ID] in table 'SymptomVisitXRef'
ALTER TABLE [dbo].[SymptomVisitXRef]
ADD CONSTRAINT [FK_SymptomVisitXRef_Visits]
    FOREIGN KEY ([Visits_ID])
    REFERENCES [dbo].[VisitSet]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SymptomVisitXRef_Visits'
CREATE INDEX [IX_FK_SymptomVisitXRef_Visits]
ON [dbo].[SymptomVisitXRef]
    ([Visits_ID]);
GO

-- Creating foreign key on [Visits_ID] in table 'PrescriptionSet'
ALTER TABLE [dbo].[PrescriptionSet]
ADD CONSTRAINT [FK_Prescriptions_Visit]
    FOREIGN KEY ([Visits_ID])
    REFERENCES [dbo].[VisitSet]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Prescriptions_Visit'
CREATE INDEX [IX_FK_Prescriptions_Visit]
ON [dbo].[PrescriptionSet]
    ([Visits_ID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------