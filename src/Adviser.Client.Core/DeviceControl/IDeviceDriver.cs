﻿using System;
using Adviser.Common.Domain;

namespace Adviser.Client.Core.DeviceControl
{
    public interface IDeviceDriver
    {
        bool IsConnected{ get; }
        bool SetupDevice(DeviceSettings settings);
        bool ConnectElectrodes(byte front, byte back);
        bool SetPolarity(bool frontPositive);
        bool GetDeviceGUID(ref Guid DeviceGUID);
        bool GetMainBoardVersion(out byte[] boardVersion, ref string errorMessage);
        bool GetBridgeVersion(ref string BridgeVersion);
        ushort ReadData();
        ushort ReadBusVoltage();
        void Release();
        void ReInitialize();
    }
}
