﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace Adviser.Server.Services.Contracts
{
    [DataContract]
    public class AdviserClientLogEntry
    {
        [DataMember]
        public DateTime Time;
        [DataMember]
        public Guid? DeviceId;
        [DataMember]
        public Guid? PractitionerId;
        [DataMember]
        public Guid? VisistId;
        [DataMember]
        public Object Details;
        [DataMember]
        public String Application;
        [DataMember]
        public byte Severity;
        [DataMember]
        public String Category;
        [DataMember]
        public String LogItemType;
        [DataMember]
        public string ErrorMessage;
    }
}
