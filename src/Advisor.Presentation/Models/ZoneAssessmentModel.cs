﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using Advisor.Core;
using Advisor.Common.DataModel;

namespace Advisor.Presentation.Models
{
    public class ZoneAssessmentModel : ViewModelBase
    {
        private ZoneAssessment assessment;
        

        public ZoneAssessmentModel(ZoneAssessment model)
        {
            assessment = model;
        }

        public string Name
        {
            get { return assessment.Name; }
        }

        public string Description
        {
            get { return assessment.Description; }
        }

        public string RiskLevel
        {
            get 
            {
                string level = "";
                switch (assessment.RiskLevel)
                {
                    case HealthRiskLevel.Normal:
                        level = "No risk";
                        break;
                    case HealthRiskLevel.Low:
                        level = "Low";
                        break;
                    case HealthRiskLevel.Medium:
                        level = "Medium";
                        break;
                    case HealthRiskLevel.High:
                        level = "High";
                        break;
                    case HealthRiskLevel.VeryHigh:
                        level = "Very High";
                        break;
                }
                return level; 
            }
        }

        public string Risks
        {
            get { return assessment.Risks; }
        }

        public string Recommendations
        {
            get { return assessment.Recommendations; }
        }


        public string RiskLevelColor
        {
            get
            {
                string color = "#FFFFFF";
                switch (assessment.RiskLevel)
                {
                    case HealthRiskLevel.Normal:
                        color = "#39B51B";
                        break;
                    case HealthRiskLevel.Low:
                        color = "#03CFEE";
                        break;
                    case HealthRiskLevel.Medium:
                        color = "#F3FF1A";
                        break;
                    case HealthRiskLevel.High:
                        color = "#FA9D19";
                        break;
                    case HealthRiskLevel.VeryHigh:
                        color = "#EE3503";
                        break;
                }

                return color;
            }
        }

        
    }
}
