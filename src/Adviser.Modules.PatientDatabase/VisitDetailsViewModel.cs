﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Wpf.Events;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Common.Domain;
    using Adviser.Client.DataModel;
    using Adviser.Client.Core.Presentation;
    using Adviser.Client.Core.Events;
    using Adviser.Client.Core.Services;

    public interface IVisitDetailsViewModel
    {
        void Refresh();
        DelegateCommand<object> ShowNotesHistoryCommand { get; }
    }

    /// <summary>
    /// Presentation model for Visit entity.
    /// </summary>
    public class VisitDetailsViewModel : ViewModelBase, IVisitDetailsViewModel
    {
        private ObservableDictionary<string, SymptomViewModel> _symptoms;
        private IEnumerable<OrganExamination> _organExaminations;
        private string _notes;
        private Visit _visit;
        private IDBService _dbService;
        private IEventAggregator _eventAggregator;
        private AppointmentSession _session;
        private ObservableCollection<NotesHistoryModel> _notesHistory;


        public DelegateCommand<object> ShowNotesHistoryCommand { get; private set; }

        /// <summary>
        /// Constructor pre-initializes all symptoms and OrganExaminations
        /// for the UI to be able to render all controls independently of
        /// whether records in mapping tables exist or not.
        /// </summary>
        /// <param name="organs"></param>
        /// <param name="symptoms"></param>
        /// <param name="session"></param>
        public VisitDetailsViewModel(
            Visit visit, 
            IDBService dbService,
            IEventAggregator eventAggregator,
            AppointmentSession session)
        {
            _dbService = dbService;
            _eventAggregator = eventAggregator;
            _session = session;
           
            _symptoms = new ObservableDictionary<string, SymptomViewModel>();

            //- read data from the current visit and set up the model
            SetVisit(_session.Visit);

            //UpdateVisitCommand = new DelegateCommand<object>(SaveData);

            //- subscribe to the measurement and results event
            //TODO: update this logic to block from overwriting exisitng results with new tests
            _eventAggregator.GetEvent<MeasurementCompletedEvent>().Subscribe(OnMeasurementCompleted);
            _eventAggregator.GetEvent<ResultsReceivedEvent>().Subscribe(OnResultsReceived);
            _eventAggregator.GetEvent<VisitSelectedEvent>().Subscribe(OnVisitSelected);
            _eventAggregator.GetEvent<NavigationEvent>().Subscribe(OnNavigationEvent, ThreadOption.PublisherThread, false, e => e.EventName == NavigationEventNames.Results);
            
            NotesHistory = new ObservableCollection<NotesHistoryModel>();

            ShowNotesHistoryCommand = new DelegateCommand<object>(ShowNotesHistory);
        }

        //public DelegateCommand<object> UpdateVisitCommand { get; private set; }
        
        public void Refresh()
        {
            SetVisit(_session.Visit);
        }


        public ObservableDictionary<string, SymptomViewModel> Symptoms
        {
            get { return _symptoms; }
            private set
            {
                _symptoms = value;
                RaisePropertyChangedEvent("Symptoms");
            }
        }

        public IEnumerable<OrganExamination> OrganExaminations
        {
            get { return _organExaminations; }
            private set 
            { 
                _organExaminations = value;
                RaisePropertyChangedEvent("OrganExaminations");
            }
        }

        public string Notes
        {
            get { return _visit != null ? _visit.Notes : String.Empty; }
            set 
            {
                if (_visit != null && _visit.Notes != value)
                {
                    _visit.Notes = value;
                    //_notes = value;
                    RaisePropertyChangedEvent("Notes");
                }
            }
        }

        public bool AllowChanges
        {
            get;
            set;
        }

        public Visit Visit
        {
            get { return _visit; }
            private set 
            { 
                _visit = value;
                RaisePropertyChangedEvent("Visit");
            }
        }

        private void OnNavigationEvent(NavigationEventArgs e)
        {
            SaveData(null);
        }

        /// <summary>
        /// Event handler for Update command. Updates changed visit data in the database.
        /// </summary>
        /// <param name="parameters"></param>sults
        private void SaveData(object parameters)
        {
            //if (_visit != null && _visit.Notes != Notes)
            //    _visit.Notes = Notes; //- notes are set on the ViewModel so need to be passed on to data model to be saved
            
            _dbService.SaveChanges();
            IsDirty = false;
        }

        private void OnPatientSelected(Patient visit)
        {
            SetVisit(null);
        }

        /// <summary>
        /// Event handler for VisitSelectedEvent.
        /// </summary>
        /// <param name="visit"></param>
        private void OnVisitSelected(Visit visit)
        {
            SetVisit(visit);
        }

        /// <summary>
        /// Event handler for MeasurementCompletedEvent.
        /// </summary>
        /// <param name="e"></param>
        private void OnMeasurementCompleted(MeasurementEventArgs e)
        {
            AdviserMeasurement measurement = new AdviserMeasurement();
            measurement.Context = e.Context;
            measurement.Data = e.MeasurementData;

            BinaryFormatter fmt = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            fmt.Serialize(stream, measurement);
            byte[] binData = stream.ToArray();
            _visit.MeasurementData = binData;
            _dbService.SaveChanges();
        }

        private void OnResultsReceived(ResultsReceivedEventArgs e)
        {
            if (e.IsSuccess && _visit.ID == e.VisitData.ID)
            {
                Visit = e.VisitData;
            }
        }

        protected override void UpdateDataModel()
        {
            if (_visit != null)
                _visit.Notes = Notes; //- notes are set on the ViewModel so need to be passed on to data model to be saved
        }

        protected override void SaveChanges()
        {
            _dbService.SaveChanges();
        }

        public void ShowNotesHistory(object p)
        {
            NotesHistory.Clear();
            string HighlightColor = "#00000000";
            foreach (Visit v in _session.Patient.Visits.OrderBy(b => b.Date))
            {
                if (v.Notes != null && v.Notes != String.Empty)
                {
                    if (v == _session.Visit)
                    {
                        HighlightColor = "#769271";
                    }
                    else
                    {
                        HighlightColor = "#00000000";
                    }
                    NotesHistory.Add(new NotesHistoryModel(v.Date.ToLocalTime(), v.Notes, HighlightColor));
                }
            }
        }

        public ObservableCollection<NotesHistoryModel> NotesHistory
        {
            get
            {
                return _notesHistory;
            }
            private set
            {
                _notesHistory = value;
                base.RaisePropertyChangedEvent("NotesHistory");
            }
        }

        /// <summary>
        /// Sets the ViewModel data according to the given Visit.
        /// </summary>
        /// <param name="visit"></param>
        private void SetVisit(Visit visit)
        {
            if (Visit != null)
            {//- save data if visit was edited
                if (IsDirty)
                    SaveData(this);
            }

            Visit = visit;
            if (visit != null)
            {
                if (visit.MeasurementData != null)
                    AllowChanges = false;
                else
                    AllowChanges = true;

                //- set Symptoms
                if (!_visit.Symptoms.IsLoaded)
                    _visit.Symptoms.Load();

                //- set symptoms
                ObservableDictionary<string, SymptomViewModel> visitSymptoms = new ObservableDictionary<string, SymptomViewModel>();
                foreach (Symptom symptom in _session.AllSymptoms)
                {
                    symptom.IsEditable = AllowChanges;
                    visitSymptoms.Add(symptom.Name, new SymptomViewModel(symptom, _visit));
                }
                Symptoms = visitSymptoms;          

                //- set OrganExaminations
                if (!_visit.OrganExaminations.IsLoaded)
                {
                    _visit.OrganExaminations.Load();
                }

                foreach (OrganExamination exam in _visit.OrganExaminations)
                    exam.IsEditable = AllowChanges;

                OrganExaminations = _visit.OrganExaminations.Where(p => p.Organ.Gender == 0 || (_visit.Patient.Sex == "M" && p.Organ.Gender == 1) || (_visit.Patient.Sex == "F" && p.Organ.Gender == 2)).OrderBy(exam => exam.Organ.Name);

                //- set Notes
                RaisePropertyChangedEvent("Notes");
                //Notes = _visit.Notes;
            }
            else
            {
                Visit = null;
                Symptoms.Clear();
                OrganExaminations = null;
                Notes = String.Empty;
            }
        }
    }
}
