﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Advisor.Modules.Catalog.Views
{
    /// <summary>
    /// Interaction logic for CatalogView.xaml
    /// </summary>
    public partial class CatalogView : UserControl, ICatalogView
    {
        public CatalogView()
        {
            InitializeComponent();
        }

        public void SetDefaultView(IPatientListView patientListView)
        {
            //this.CatalogContent.Content = patientListView;
        }

        public object CurrentContent
        {
            get
            {
                return this.CatalogPanel.Content;
            }
            set
            {
                this.CatalogPanel.Content = value;
            }
        }
    }
}
