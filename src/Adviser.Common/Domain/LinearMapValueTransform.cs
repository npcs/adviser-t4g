﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Common.Domain
{
    /// <summary>
    /// Maps original value to a given scale linear.
    /// </summary>
    public class LinearMapValueTransform : IValueTransform
    {
        private double _lowerBound;
        private double _upperBound;
        private double _min;
        private double _max;
        private double _sourceRange;
        private double _targetRange;

        public LinearMapValueTransform(double srcMin, double srcMax, double lowerBound, double upperBound)
        {
            _min = srcMin;
            _max = srcMax;
            _lowerBound = lowerBound;
            _upperBound = upperBound;
            _sourceRange = _max - _min;
            _targetRange = _upperBound - _lowerBound;
        }

        public double Transform(double source)
        {
            double sourceRatio = (source - _min) / _sourceRange;
            double result = _lowerBound + _targetRange * sourceRatio;
            return result;
        }

        public void SetParameters(object[] args)
        {
            _min = (double)args[0];
            _max = (double)args[1];
            _sourceRange = _max - _min;
        }
    }
}
