﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Advisor.Modules.Readout.PresentationModels;

namespace Advisor.Modules.Readout.Views
{
    /// <summary>
    /// Interaction logic for PairDetailsView.xaml
    /// </summary>
    public partial class PairDetailsView : UserControl, IPairDetailsView
    {
        public PairDetailsView()
        {
            InitializeComponent();
        }


        public IDataCellViewModel Model
        {
            get
            {
                return DataContext as IDataCellViewModel;
            }
            set
            {
                this.DataContext = value;
                //lineChart.ItemsSource = value.Series;
                lineChart.Title = "Front: " + value.Column + "  Back: " + value.Row;
            }
        }

        public void Reset()
        {
            this.DataContext = null;
            lineChart.Title = "Front: " + "  Back: ";
        }

    }
}
