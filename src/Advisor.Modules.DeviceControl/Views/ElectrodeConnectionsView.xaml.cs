﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Advisor.Modules.DeviceControl.Views
{
    /// <summary>
    /// Interaction logic for ElectrodeConnectionsView.xaml
    /// </summary>
    public partial class ElectrodeConnectionsView : UserControl, IElectrodeConnectionsView
    {
        public ElectrodeConnectionsView()
        {
            InitializeComponent();
        }

        #region IElectrodeConnectionsView Members

        public IElectrodeConnectionsViewModel Model
        {
            get
            {
                return this.DataContext as IElectrodeConnectionsViewModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        #endregion
    }
}
