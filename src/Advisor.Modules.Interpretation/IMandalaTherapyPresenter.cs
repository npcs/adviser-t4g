﻿using System;
using Advisor.Modules.Interpretation;

namespace Advisor.Modules.Interpretation
{
    public interface IMandalaTherapyPresenter
    {
        IMandalaTherapyView View { get; set; }
        void Show(IMandalaViewModel model);
    }
}
