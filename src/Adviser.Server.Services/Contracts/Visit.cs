﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

using Adviser.Common.Domain;

namespace Adviser.Server.Services.Contracts
{
    [DataContract]
    public class Visit
    {
        [DataMember]
        public Guid ID;

        [DataMember]
        public DateTime Date;

        [DataMember]
        public bool Deleted;

        [DataMember]
        public DateTime LastUpdDate;

        [DataMember]
        public byte[] MeasurementData;

        [DataMember]
        public byte[] ExamResult;

        [DataMember]
        public string Notes;

        [DataMember]
        public OrganExamination[] OrganExaminations;

        [DataMember]
        public Symptom[] Symptoms;

        [DataMember]
        public Prescription[] Prescriptions;
    }
}