﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

using Adviser.Client.Core.Services;
using Adviser.Common.Domain;

using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Events;

using Adviser.Client.Core.Events;
using Adviser.Client.Core.Presentation;

using Adviser.Client.DataModel;
using Microsoft.Practices.Unity;


namespace Advisor.Modules.DeviceControl
{
    public class ElectrodeConnectionsViewModel : Advisor.Modules.DeviceControl.IElectrodeConnectionsViewModel
    {
        public ObservableCollection<ElectrodeDescription> FrontElectrodes { get; set; }
        public ObservableCollection<ElectrodeDescription> BackElectrodes { get; set; }

        private AppointmentSession _session;
        
        public ElectrodeConnectionsViewModel(IConfigurationDataService configurationDataService, IEventAggregator eventAggregator, IUnityContainer container)
        {

            _session = container.Resolve<AppointmentSession>();

            this.FrontElectrodes = new ObservableCollection<ElectrodeDescription>();
            this.BackElectrodes = new ObservableCollection<ElectrodeDescription>();
            
            MeasurementSettings measurementSettings = configurationDataService.GetMeasurementSettings();

            for (int i = 0; i < measurementSettings.FrontLeads; i++)
            {
                this.FrontElectrodes.Add(new ElectrodeDescription(true, false, _session.FrontElectrodes[i].Name, _session.FrontElectrodes[i].Description));
            }

            for (int i = 0; i < measurementSettings.BackLeads; i++)
            {
                this.BackElectrodes.Add(new ElectrodeDescription(true, false, _session.BackElectrodes[i].Name, _session.BackElectrodes[i].Description));
            }

            eventAggregator.GetEvent<MeasurementProgressEvent>().Subscribe(MeasurementProgress, ThreadOption.UIThread);
        }

        private int _lastFront = 0;
        private int _lastBack = 0;


        private void MeasurementProgress(MeasurementProgressEventArgs e)
        {
            this.FrontElectrodes[_lastFront].IsOn = false;
            this.BackElectrodes[_lastBack].IsOn = false;

            this.FrontElectrodes[e.FrontElectrode].IsOn = true;
            this.BackElectrodes[e.BackElectrode].IsOn = true;

            _lastFront = e.FrontElectrode;
            _lastBack = e.BackElectrode;
        }
    }
}
