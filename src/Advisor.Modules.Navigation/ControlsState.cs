﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Events;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Presentation;
using Adviser.Client.DataModel;

namespace Advisor.Modules.Navigation
{
    public class ControlsState
    {
        private IEventAggregator _eventAggregator;
        private Dictionary<string, bool> _states;

        public ControlsState(IEventAggregator eventAggregator, AppointmentSession session)
        {
            eventAggregator.GetEvent<VisitSelectedEvent>().Subscribe(OnVisitSelected);
            eventAggregator.GetEvent<ResultsReceivedEvent>().Subscribe(OnResultsReceived);
        }

        private void OnVisitSelected(Visit visit)
        {
            if (visit != null)
            {
                if (visit.MeasurementData == null)
                    IsEnabled = true;
            }
            else
            {
                IsEnabled = false;
            }
        }

        private void OnResultsReceived(Visit visit)
        {
            if (visit.ExamResult != null)
                IsEnabled = false;
        }

        public bool IsEnabled
        {
            get;
            set;
        }
    }
}
