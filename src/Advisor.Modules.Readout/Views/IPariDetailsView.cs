﻿using System;
using System.Collections.Generic;
using Adviser.Common.Domain;
using Advisor.Modules.Readout.PresentationModels;

namespace Advisor.Modules.Readout.Views
{
    public interface IPairDetailsView
    {
        IDataCellViewModel Model { get; set; }
        void Reset();
    }
}
