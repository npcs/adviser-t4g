﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Adviser.Server.Services.Contracts
{
    [DataContract]
    public class SyncDataItem
    {
        [DataMember]
        public Guid Key;

        [DataMember]
        public DateTime Timestamp;

        [DataMember]
        public string ChildType;

        [DataMember]
        public SyncDataItem[] Children;
    }
}
