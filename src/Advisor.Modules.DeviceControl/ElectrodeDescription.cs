﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adviser.Common.Domain;

using Adviser.Client.Core.Presentation;

namespace Advisor.Modules.DeviceControl
{
    public class ElectrodeDescription : ViewModelBase
    {
        private bool _IsDescriptionVisible = true;
        /// <summary>
        /// Is Description visible
        /// </summary>
        public bool IsDescriptionVisible
        {
            get { return _IsDescriptionVisible; }
            set
            {
                _IsDescriptionVisible = value;
                base.RaisePropertyChangedEvent("IsDescriptionVisible");
            }
        }
        private bool _IsOn;
        /// <summary>
        /// Is Electrode on during the measurement.
        /// </summary>
        public bool IsOn
        {
            get { return _IsOn; }
            set
            {
                _IsOn = value;
                base.RaisePropertyChangedEvent("IsOn");
            }
        }

        private string _Description;
        /// <summary>
        /// Description of the electrode
        /// </summary>
        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                base.RaisePropertyChangedEvent("Description");
            }
        }


        private string _ChakraName;
        /// <summary>
        /// Chakra Name
        /// </summary>
        public string ChakraName
        {
            get { return _ChakraName; }
            set
            {
                _ChakraName = value;
                base.RaisePropertyChangedEvent("ChakraName");
            }
        }

        public ElectrodeDescription(bool isDescriptioVisible, bool isOn, string ChakraName, string Description)
        {
            this._ChakraName = ChakraName;
            this._IsDescriptionVisible = isDescriptioVisible;
            this._IsOn = isOn;
            this._Description = Description;
        }
    }
}
