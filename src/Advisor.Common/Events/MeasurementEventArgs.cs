﻿using System;
using System.Collections.Generic;
using Advisor.Common.DataModel;


namespace Advisor.Common.Events
{
    [Serializable()]
    public class MeasurementEventArgs : EventArgs
    {
        private int totalProgress;
        private MeasurementStatus status;
        private MeasurementContext measurementContext;
        private DataMatrix<PairMeasurement> measurementData;

        public MeasurementEventArgs()
        {
            status = MeasurementStatus.NotAvailable;
        }

        public int TotalProgress
        {
            get { return totalProgress; }
            set { totalProgress = value; }
        }

        public MeasurementStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public DataMatrix<PairMeasurement> MeasurementData
        {
            get { return measurementData; }
            set { measurementData = value; }
        }

        public MeasurementContext Context
        {
            get { return measurementContext; }
            set { measurementContext = value; }
        }

    }
}
