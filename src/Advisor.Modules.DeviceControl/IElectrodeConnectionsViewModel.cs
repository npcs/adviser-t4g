﻿using System;
using Adviser.Common.Domain;
namespace Advisor.Modules.DeviceControl
{
    public interface IElectrodeConnectionsViewModel
    {
        System.Collections.ObjectModel.ObservableCollection<ElectrodeDescription> BackElectrodes { get; set; }
        System.Collections.ObjectModel.ObservableCollection<ElectrodeDescription> FrontElectrodes { get; set; }
    }
}
