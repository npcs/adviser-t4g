﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Modules.PatientDatabase
{
    public interface IPatientsView
    {
        void SetLeftView(object view);
        void SetRightView(object view);
    }
}
