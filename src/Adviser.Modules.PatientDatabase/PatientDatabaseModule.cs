﻿using System;
using System.Collections.Generic;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;

using Adviser.Client.DataModel;
using Adviser.Client.Core.Presentation;
using Adviser.Client.Core.Services;

namespace Adviser.Modules.PatientDatabase
{
    public class PatientDatabaseModule : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public PatientDatabaseModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            RegisterViewsAndServices();

            IRegion toolbarRegion = _regionManager.Regions[RegionNames.ToolbarRegion];
            SelectionView selectionView = _container.Resolve<SelectionView>();
            AppointmentSession sessionModel = _container.Resolve<AppointmentSession>();
            selectionView.Model = sessionModel;
            toolbarRegion.Add(selectionView);

            IRegion region = _regionManager.Regions[RegionNames.ContentRegion];
            IPatientsPresenter patientsPresenter = _container.Resolve<IPatientsPresenter>();
            region.Add(patientsPresenter.View, "IPatientsView");
            region.Deactivate(patientsPresenter.View);

            IVisitsPresenter visitsPresenter = _container.Resolve<IVisitsPresenter>();
            region.Add(visitsPresenter.View, "IVisitsView");
            region.Deactivate(visitsPresenter.View);
        }

        protected void RegisterViewsAndServices()
        {
            //AppointmentSession session = new AppointmentSession();
            //_container.RegisterInstance(session, new Microsoft.Practices.Unity.ContainerControlledLifetimeManager());
            //_container.RegisterType<AppointmentSession>(new Microsoft.Practices.Unity.ContainerControlledLifetimeManager());

            _container.RegisterType<ISelectionView, SelectionView>();

            _container.RegisterType<IPatientsView, PatientsView>();
            _container.RegisterType<IPatientsPresenter, PatientsPresenter>();
            _container.RegisterType<IPatientListView, PatientListView>();
            //_container.RegisterType<IPatientListPresenter, PatientListPresenter>();
            _container.RegisterType<IPatientDetailsView, PatientDetailsView>();
            //_container.RegisterType<IPatientDetailsPresenter, PatientDetailsPresenter>();
            _container.RegisterType<IPatientsPresentationModel, PatientsPresentationModel>();

            _container.RegisterType<IVisitsView, VisitsView>();
            _container.RegisterType<IVisitsPresenter, VisitsPresenter>();
            _container.RegisterType<IVisitListView, VisitListView>();
            //_container.RegisterType<IVisitListPresenter, VisitListPresenter>();
            _container.RegisterType<IVisitDetailsView, VisitDetailsView>();
            _container.RegisterType<IVisitListViewModel, VisitListViewModel>();
            _container.RegisterType<IVisitDetailsViewModel, VisitDetailsViewModel>();        
           

            //- print support

            _container.RegisterType<IVisitDetailsPrintViewModel, VisitDetailsPrintViewModel>();
            _container.RegisterType<PrintPresenter, PatientDetailsPrintPresenter>("PatientDetailsPrintPresenter");
            _container.RegisterType<PrintPresenter, VisitDetailsPrintPresenter>("VisitDetailsPrintPresenter");    
            //_container.RegisterType<VisitDetailsPrintView, VisitDetailsPrintView>();

            _container.RegisterType<IPractitionerPresenter, PractitionerPresenter>();
            _container.RegisterType<IPractitionerModel, PractitionerModel>();
            _container.RegisterType<IPractitionerDetailsView, PractitionerDetailsView>();
            _container.RegisterType<IPractitionerLoginView, PractitionerLoginView>();
            _container.RegisterType<IPractitionerRetrievePasswordView, PractitionerRetrievePasswordView>();
            
            _container.RegisterType<PatientDetailsPrintView, PatientDetailsPrintView>();

            _container.RegisterType<IConfigurationDataService, ConfigurationDataService>();
            //_container.RegisterType<IUserProfileService, UserProfileService>();
        }
    }
}
