﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Client.DataModel
{
    public partial class Practitioner
    {
        public AccountStatus Status
        {
            get
            {
                return (AccountStatus)StatusID;
            }
            set
            {
                StatusID = (Int16)value;
            }
        }
    }
}
