﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Adviser.Modules.Print
{
    /// <summary>
    /// Interaction logic for PrintReportView.xaml
    /// </summary>
    public partial class PrintReportView : UserControl, IPrintReportView
    {
        public PrintReportView()
        {
            InitializeComponent();
        }

        #region IPrintReportView Members

        public object Model
        {
            get
            {
                return this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public void AddPrintSection(UserControl printView)
        {
            //this.printContent.Content = printView;
        }

        #endregion
    }
}
