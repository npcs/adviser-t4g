﻿using Adviser.Client.Core.DeviceControl;
using Adviser.Common.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Advisor.Core.Tests
{

    /// <summary>
    ///This is a test class for DeviceEmulatorTest and is intended
    ///to contain all DeviceEmulatorTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DeviceEmulatorTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SetupADC
        ///</summary>
        [TestMethod()]
        public void SetupADCTest()
        {
            DeviceEmulator target = new DeviceEmulator(); 
            DeviceSettings settings = null; 
            bool expected = true; 
            bool actual;
            actual = target.SetupDevice(settings);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ReadData
        ///</summary>
        [TestMethod()]
        public void ReadDataTest()
        {
            DeviceEmulator target = new DeviceEmulator(); 
            float actual;
            actual = target.ReadData();
            Assert.IsTrue(((0F < actual) && (actual < 0.08F)));
        }

        /// <summary>
        ///A test for SetPolarity
        ///</summary>
        [TestMethod()]
        public void SetPolarityTest()
        {
            DeviceEmulator target = new DeviceEmulator(); 
            bool frontPositive = true; 
            bool expected = true; 
            bool actual;
            actual = target.SetPolarity(frontPositive);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ConnectElectrodes
        ///</summary>
        [TestMethod()]
        public void ConnectElectrodesTest()
        {
            DeviceEmulator target = new DeviceEmulator(); 
            byte front = 0; 
            byte back = 0; 
            bool expected = true; 
            bool actual;
            actual = target.ConnectElectrodes(front, back);
            Assert.AreEqual(expected, actual);
        }
    }
}
