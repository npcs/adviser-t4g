﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Common.DataModel
{
    /// <summary>
    /// Represents low level hardvare settings, register addresses, and 
    /// </summary>
    [Serializable()]
    public class DeviceSettings //: IDeviceSettings
    {
        private bool _IsHighSpeedBus = false;
        /// <summary>
        /// Sets the speed of the bus. Default is false.
        /// </summary>
        public bool IsHighSpeedBus
        {
            get { return _IsHighSpeedBus; }
            set { _IsHighSpeedBus = value; }
        }

        private byte _ElectrodeConnectorDeviceAddress = 0x38;
        /// <summary>
        /// Address of PCF8574A.
        /// </summary>
        public byte ElectrodeConnectorDeviceAddress
        {
            get { return _ElectrodeConnectorDeviceAddress; }
            set { _ElectrodeConnectorDeviceAddress = value; }
        }

        private byte _ADCAddress = 0x40;
        /// <summary>
        /// Address of INA209
        /// </summary>
        public byte ADCAddress
        {
            get { return _ADCAddress; }
            set { _ADCAddress = value; }
        }

        private int _GuidAddress = 8160;
        /// <summary>
        /// Address of the GUID value. Guid is stored on EEPROM of the USB Bridge. 
        /// </summary>
        public int GuidAddress
        {
            get { return _GuidAddress; }
            set { _GuidAddress = value; }
        }

        private ushort _ADCSettings = 0x006F;
        /// <summary>
        /// Represents INA209 Settings. Default value x006F
        /// </summary>        
        public ushort ADCSettings
        {
            get { return _ADCSettings; }
            set { _ADCSettings = value; }
        }

        private byte _ADCConfigRegisterAddress = 0;
        /// <summary>
        /// The addres of the config Register
        /// </summary>
        public byte ADCConfigRegisterAddress
        {
            get { return _ADCConfigRegisterAddress; }
            set { _ADCConfigRegisterAddress = value; }
        }

        private byte _ADCShuntVoltageRegisterAddress = 0x03;
        /// <summary>
        /// The addres of the Voltage Shunt Register
        /// </summary>
        public byte ADCShuntVoltageRegisterAddress
        {
            get { return _ADCShuntVoltageRegisterAddress; }
            set { _ADCShuntVoltageRegisterAddress = value; }
        }

        private byte _ADCBusVoltageRegisterAddress = 0x04;
        /// <summary>
        /// The addres of the Bus Voltage Register
        /// </summary>
        public byte ADCBusVoltageRegisterAddress
        {
            get { return _ADCBusVoltageRegisterAddress; }
            set { _ADCBusVoltageRegisterAddress = value; }
        }

        private byte _CalibrationLeadA = 7;
        /// <summary>
        /// Index of first calibration lead.
        /// </summary>
        public byte CalibrationLeadA
        {
            get { return _CalibrationLeadA; }
            set { _CalibrationLeadA = value; }
        }

        private byte _CalibrationLeadB = 7;
        /// <summary>
        /// Index of second calibration lead.
        /// </summary>
        public byte CalibrationLeadB
        {
            get { return _CalibrationLeadB; }
            set { _CalibrationLeadB = value; }
        }

        private double _MeasurementScaleFactor = 1;
        /// <summary>
        /// Factor to convert measured voltage from the ADC to get Volts.
        /// </summary>
        public double MeasurementScaleFactor
        {
            get { return _MeasurementScaleFactor; }
            set { _MeasurementScaleFactor = value; }
        }

        /// <summary>
        /// Turns on/off device emulation mode.
        /// </summary>
        public bool EmulateDevice
        {
            get;
            set;
        }
    }
}
