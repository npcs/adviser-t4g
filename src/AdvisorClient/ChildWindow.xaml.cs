﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Adviser.Client.Core.Presentation;

using System.Runtime.InteropServices;
using System.Windows.Interop;
namespace AdvisorClient
{
    /// <summary>
    /// Interaction logic for ChildWindow.xaml
    /// </summary>
    public partial class ChildWindow : Window, IChildWindow
    {
        public ChildWindow()
        {
            InitializeComponent();
        }

        private bool isClosingByCloseWindowMethod = false;
        #region IChildWindow Members

        public void CloseWindow()
        {
            isClosingByCloseWindowMethod = true;
            this.Close();
        }

        public void ShowAsDialog()
        {
            if (!(this.Visibility == Visibility.Visible))
            {
                this.ShowDialog();
            }
        }

        public void SetContent(object view)
        {
            this.ContentHolder.Content = view;
        }

        private bool _IsFullScreen;
        public bool IsFullScreen
        {
            get
            {
                return _IsFullScreen;
            }
            set
            {
                _IsFullScreen = value;
                if (_IsFullScreen)
                {
                    this.WindowState = WindowState.Maximized;
                }
                else
                {
                    this.WindowState = WindowState.Normal;
                }
            }
        }

        public WindowStyle ChildWindowStyle
        {
            set { this.WindowStyle =  value; }
        }

        public double ChildWindowWidth
        {
            set { this.Width = value; }
        }

        public double ChildWindowHeight
        {
            set { this.Height = value; }
        }

        private bool _CanCloseOnEsc = false;

        public bool CanCloseOnEsc
        {
            set { _CanCloseOnEsc = value; }
        }

        public SizeToContent ChildWindowSizeToContent
        {
            set { this.SizeToContent = value; }
        }

        public string WindowTitle
        {
            set { this.Title = value; }
        }

        #endregion

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape && _CanCloseOnEsc)
            {
                CloseWindow();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!isClosingByCloseWindowMethod && !_CanCloseOnEsc)
            {
                e.Cancel = true;
            }
        }

        //#region Hide close button
        ////WPF doesn't have a built-in property to hide the title bar's Close button, but you can do it with a few lines of P/Invoke.

        //private const int GWL_STYLE = -16; private const int WS_SYSMENU = 0x80000;
        //[DllImport("user32.dll", SetLastError = true)]
        //private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        //[DllImport("user32.dll")]
        //private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);


        //private void Window_Loaded(object sender, RoutedEventArgs e)
        //{
        //    var hwnd = new WindowInteropHelper(this).Handle; 
        //    SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
            
        //}


        //#endregion

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

    }
}
