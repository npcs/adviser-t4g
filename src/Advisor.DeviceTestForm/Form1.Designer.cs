﻿namespace Advisor.DeviceTestForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConnectElectrodes = new System.Windows.Forms.Button();
            this.txtFront = new System.Windows.Forms.TextBox();
            this.txtBack = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGetShuntVoltage = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblBusVoltage = new System.Windows.Forms.Label();
            this.btnGetBusVoltage = new System.Windows.Forms.Button();
            this.lblShuntVoltage = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnSaveConfig = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtADCConfig = new System.Windows.Forms.TextBox();
            this.lblADCConfig = new System.Windows.Forms.Label();
            this.btnCurrentADCConfig = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConnectElectrodes
            // 
            this.btnConnectElectrodes.Location = new System.Drawing.Point(197, 16);
            this.btnConnectElectrodes.Name = "btnConnectElectrodes";
            this.btnConnectElectrodes.Size = new System.Drawing.Size(119, 23);
            this.btnConnectElectrodes.TabIndex = 0;
            this.btnConnectElectrodes.Text = "Connect Electrodes";
            this.btnConnectElectrodes.UseVisualStyleBackColor = true;
            this.btnConnectElectrodes.Click += new System.EventHandler(this.btnConnectElectrodes_Click);
            // 
            // txtFront
            // 
            this.txtFront.Location = new System.Drawing.Point(64, 17);
            this.txtFront.Name = "txtFront";
            this.txtFront.Size = new System.Drawing.Size(31, 20);
            this.txtFront.TabIndex = 1;
            this.txtFront.Text = "7";
            // 
            // txtBack
            // 
            this.txtBack.Location = new System.Drawing.Point(153, 17);
            this.txtBack.Name = "txtBack";
            this.txtBack.Size = new System.Drawing.Size(31, 20);
            this.txtBack.TabIndex = 2;
            this.txtBack.Text = "7";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Front";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(108, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Back";
            // 
            // btnGetShuntVoltage
            // 
            this.btnGetShuntVoltage.Location = new System.Drawing.Point(23, 22);
            this.btnGetShuntVoltage.Name = "btnGetShuntVoltage";
            this.btnGetShuntVoltage.Size = new System.Drawing.Size(127, 23);
            this.btnGetShuntVoltage.TabIndex = 5;
            this.btnGetShuntVoltage.Text = "Get Shunt Voltage";
            this.btnGetShuntVoltage.UseVisualStyleBackColor = true;
            this.btnGetShuntVoltage.Click += new System.EventHandler(this.btnGetShuntVoltage_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtFront);
            this.panel1.Controls.Add(this.btnConnectElectrodes);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtBack);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(27, 170);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(403, 61);
            this.panel1.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblBusVoltage);
            this.panel2.Controls.Add(this.btnGetBusVoltage);
            this.panel2.Controls.Add(this.lblShuntVoltage);
            this.panel2.Controls.Add(this.btnGetShuntVoltage);
            this.panel2.Location = new System.Drawing.Point(27, 253);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(403, 100);
            this.panel2.TabIndex = 7;
            // 
            // lblBusVoltage
            // 
            this.lblBusVoltage.AutoSize = true;
            this.lblBusVoltage.Location = new System.Drawing.Point(157, 67);
            this.lblBusVoltage.Name = "lblBusVoltage";
            this.lblBusVoltage.Size = new System.Drawing.Size(35, 13);
            this.lblBusVoltage.TabIndex = 8;
            this.lblBusVoltage.Text = "label3";
            // 
            // btnGetBusVoltage
            // 
            this.btnGetBusVoltage.Location = new System.Drawing.Point(23, 62);
            this.btnGetBusVoltage.Name = "btnGetBusVoltage";
            this.btnGetBusVoltage.Size = new System.Drawing.Size(127, 23);
            this.btnGetBusVoltage.TabIndex = 7;
            this.btnGetBusVoltage.Text = "Get Bus Voltage";
            this.btnGetBusVoltage.UseVisualStyleBackColor = true;
            this.btnGetBusVoltage.Click += new System.EventHandler(this.btnGetBusVoltage_Click);
            // 
            // lblShuntVoltage
            // 
            this.lblShuntVoltage.AutoSize = true;
            this.lblShuntVoltage.Location = new System.Drawing.Point(157, 27);
            this.lblShuntVoltage.Name = "lblShuntVoltage";
            this.lblShuntVoltage.Size = new System.Drawing.Size(35, 13);
            this.lblShuntVoltage.TabIndex = 6;
            this.lblShuntVoltage.Text = "label3";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnSaveConfig);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.txtADCConfig);
            this.panel3.Location = new System.Drawing.Point(27, 48);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(403, 80);
            this.panel3.TabIndex = 8;
            // 
            // btnSaveConfig
            // 
            this.btnSaveConfig.Location = new System.Drawing.Point(213, 28);
            this.btnSaveConfig.Name = "btnSaveConfig";
            this.btnSaveConfig.Size = new System.Drawing.Size(75, 23);
            this.btnSaveConfig.TabIndex = 2;
            this.btnSaveConfig.Text = "Save Config";
            this.btnSaveConfig.UseVisualStyleBackColor = true;
            this.btnSaveConfig.Click += new System.EventHandler(this.btnSaveConfig_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Config ADC";
            // 
            // txtADCConfig
            // 
            this.txtADCConfig.Location = new System.Drawing.Point(99, 29);
            this.txtADCConfig.Name = "txtADCConfig";
            this.txtADCConfig.Size = new System.Drawing.Size(93, 20);
            this.txtADCConfig.TabIndex = 0;
            // 
            // lblADCConfig
            // 
            this.lblADCConfig.AutoSize = true;
            this.lblADCConfig.Location = new System.Drawing.Point(221, 18);
            this.lblADCConfig.Name = "lblADCConfig";
            this.lblADCConfig.Size = new System.Drawing.Size(35, 13);
            this.lblADCConfig.TabIndex = 3;
            this.lblADCConfig.Text = "label4";
            // 
            // btnCurrentADCConfig
            // 
            this.btnCurrentADCConfig.Location = new System.Drawing.Point(27, 13);
            this.btnCurrentADCConfig.Name = "btnCurrentADCConfig";
            this.btnCurrentADCConfig.Size = new System.Drawing.Size(184, 23);
            this.btnCurrentADCConfig.TabIndex = 9;
            this.btnCurrentADCConfig.Text = "Get Current ADC Config";
            this.btnCurrentADCConfig.UseVisualStyleBackColor = true;
            this.btnCurrentADCConfig.Click += new System.EventHandler(this.btnCurrentADCConfig_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 561);
            this.Controls.Add(this.btnCurrentADCConfig);
            this.Controls.Add(this.lblADCConfig);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConnectElectrodes;
        private System.Windows.Forms.TextBox txtFront;
        private System.Windows.Forms.TextBox txtBack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGetShuntVoltage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblBusVoltage;
        private System.Windows.Forms.Button btnGetBusVoltage;
        private System.Windows.Forms.Label lblShuntVoltage;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnSaveConfig;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtADCConfig;
        private System.Windows.Forms.Label lblADCConfig;
        private System.Windows.Forms.Button btnCurrentADCConfig;
    }
}

