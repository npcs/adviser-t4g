﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmdValidate = New System.Windows.Forms.Button
        Me.cmdProgram = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.lstAvailableImages = New System.Windows.Forms.ListBox
        Me.lblVersion = New System.Windows.Forms.Label
        Me.cmdGetVersion = New System.Windows.Forms.Button
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.tabI2C = New System.Windows.Forms.TabPage
        Me.IsFrontPositiveCheckBox = New System.Windows.Forms.CheckBox
        Me.btnWrite2BtoINA209 = New System.Windows.Forms.Button
        Me.btnRead2BFromINA209 = New System.Windows.Forms.Button
        Me.btnWriteToEEPROM = New System.Windows.Forms.Button
        Me.txtBack = New System.Windows.Forms.TextBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.txtFront = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.btnConnElectr = New System.Windows.Forms.Button
        Me.txtByteToWrite = New System.Windows.Forms.TextBox
        Me.btnWriteByte = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.cmdI2CReadData = New System.Windows.Forms.Button
        Me.cmdI2CWriteData = New System.Windows.Forms.Button
        Me.lblI2CReadValue = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.cboI2CDataToSend = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboI2CRegisterAddress = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboI2CHardwareAddress = New System.Windows.Forms.ComboBox
        Me.lblbusSpeed = New System.Windows.Forms.Label
        Me.cboI2CBusSpeed = New System.Windows.Forms.ComboBox
        Me.tabEasyScale = New System.Windows.Forms.TabPage
        Me.chkEasyScaleLineHigh = New System.Windows.Forms.CheckBox
        Me.chkEasyScaleRequestAck = New System.Windows.Forms.CheckBox
        Me.chkEasyScaleVersion2 = New System.Windows.Forms.CheckBox
        Me.cmdEasyScaleWriteData = New System.Windows.Forms.Button
        Me.lblEasyScaleFeedBack = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.cboEasyScaleDataToSend = New System.Windows.Forms.ComboBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.cboEasyScaleRegisterAddress = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.cboEasyScaleHardwareAddress = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.cboEasyScaleBusSpeed = New System.Windows.Forms.ComboBox
        Me.tabDimming = New System.Windows.Forms.TabPage
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.cboDimmingOff = New System.Windows.Forms.ComboBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.cboDimmingDelay = New System.Windows.Forms.ComboBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.cboDimmingPulse = New System.Windows.Forms.ComboBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.cboDimmingStart = New System.Windows.Forms.ComboBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.cboDimmingNumSteps = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.chkDimmingEnable = New System.Windows.Forms.CheckBox
        Me.cmdDimmingWrite = New System.Windows.Forms.Button
        Me.tabTLC5924 = New System.Windows.Forms.TabPage
        Me.chkBankBEnabled = New System.Windows.Forms.CheckBox
        Me.chkBankAEnabled = New System.Windows.Forms.CheckBox
        Me.cmdTLC5924WriteDotCorrection = New System.Windows.Forms.Button
        Me.cmdTLC5924WriteEnable = New System.Windows.Forms.Button
        Me.cmdTLC5924WriteDotCorrectionAndEnable = New System.Windows.Forms.Button
        Me.gridViewDotCorrection = New System.Windows.Forms.DataGridView
        Me.gridviewEnable = New System.Windows.Forms.DataGridView
        Me.tabPullups = New System.Windows.Forms.TabPage
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.rbRes22K_8 = New System.Windows.Forms.RadioButton
        Me.rbResOpen_8 = New System.Windows.Forms.RadioButton
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.rbRes688_9 = New System.Windows.Forms.RadioButton
        Me.rbRes1K_9 = New System.Windows.Forms.RadioButton
        Me.rbRes22K_9 = New System.Windows.Forms.RadioButton
        Me.rbResOpen_9 = New System.Windows.Forms.RadioButton
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.rbRes688_10 = New System.Windows.Forms.RadioButton
        Me.rbRes1K_10 = New System.Windows.Forms.RadioButton
        Me.rbRes22K_10 = New System.Windows.Forms.RadioButton
        Me.rbResOpen_10 = New System.Windows.Forms.RadioButton
        Me.lstOutputConsole = New System.Windows.Forms.ListBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.USBEventTimer = New System.Windows.Forms.Timer(Me.components)
        Me.cmdConsoleClear = New System.Windows.Forms.Button
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tabI2C.SuspendLayout()
        Me.tabEasyScale.SuspendLayout()
        Me.tabDimming.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.tabTLC5924.SuspendLayout()
        CType(Me.gridViewDotCorrection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridviewEnable, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabPullups.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdValidate)
        Me.GroupBox1.Controls.Add(Me.cmdProgram)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.lstAvailableImages)
        Me.GroupBox1.Controls.Add(Me.lblVersion)
        Me.GroupBox1.Controls.Add(Me.cmdGetVersion)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(362, 175)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "xUSB Hardware"
        '
        'cmdValidate
        '
        Me.cmdValidate.Location = New System.Drawing.Point(184, 142)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(169, 23)
        Me.cmdValidate.TabIndex = 5
        Me.cmdValidate.Text = "Validate Image On EEPROM"
        Me.cmdValidate.UseVisualStyleBackColor = True
        '
        'cmdProgram
        '
        Me.cmdProgram.Location = New System.Drawing.Point(6, 143)
        Me.cmdProgram.Name = "cmdProgram"
        Me.cmdProgram.Size = New System.Drawing.Size(169, 23)
        Me.cmdProgram.TabIndex = 4
        Me.cmdProgram.Text = "Put Image On EEPROM"
        Me.cmdProgram.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label1.Location = New System.Drawing.Point(6, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(174, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Available Images Embedded in GUI"
        '
        'lstAvailableImages
        '
        Me.lstAvailableImages.FormattingEnabled = True
        Me.lstAvailableImages.Items.AddRange(New Object() {"v0_0_1.bin 'I2C PMBus SMBus'", "v17_1_0.bin 'I2C Only'", "v18_1_0.bin 'EasyScale & I2C'", "v18_2_0.bin 'EasyScale V2 & I2C'", "v18_3_1.bin 'EasyScale V3 & I2C'", "v19_1_0.bin 'Digital Dimming & I2C'", "v20_1_0.bin 'TLC5924 & I2C'"})
        Me.lstAvailableImages.Location = New System.Drawing.Point(6, 67)
        Me.lstAvailableImages.Name = "lstAvailableImages"
        Me.lstAvailableImages.Size = New System.Drawing.Size(347, 69)
        Me.lstAvailableImages.TabIndex = 2
        '
        'lblVersion
        '
        Me.lblVersion.AutoSize = True
        Me.lblVersion.Location = New System.Drawing.Point(181, 24)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(146, 13)
        Me.lblVersion.TabIndex = 1
        Me.lblVersion.Text = "No xUSB Hardware Attached"
        '
        'cmdGetVersion
        '
        Me.cmdGetVersion.Location = New System.Drawing.Point(6, 19)
        Me.cmdGetVersion.Name = "cmdGetVersion"
        Me.cmdGetVersion.Size = New System.Drawing.Size(169, 22)
        Me.cmdGetVersion.TabIndex = 0
        Me.cmdGetVersion.Text = "Get Version"
        Me.cmdGetVersion.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabI2C)
        Me.TabControl1.Controls.Add(Me.tabEasyScale)
        Me.TabControl1.Controls.Add(Me.tabDimming)
        Me.TabControl1.Controls.Add(Me.tabTLC5924)
        Me.TabControl1.Controls.Add(Me.tabPullups)
        Me.TabControl1.Location = New System.Drawing.Point(12, 193)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(362, 489)
        Me.TabControl1.TabIndex = 1
        '
        'tabI2C
        '
        Me.tabI2C.Controls.Add(Me.IsFrontPositiveCheckBox)
        Me.tabI2C.Controls.Add(Me.btnWrite2BtoINA209)
        Me.tabI2C.Controls.Add(Me.btnRead2BFromINA209)
        Me.tabI2C.Controls.Add(Me.btnWriteToEEPROM)
        Me.tabI2C.Controls.Add(Me.txtBack)
        Me.tabI2C.Controls.Add(Me.Label19)
        Me.tabI2C.Controls.Add(Me.txtFront)
        Me.tabI2C.Controls.Add(Me.Label18)
        Me.tabI2C.Controls.Add(Me.btnConnElectr)
        Me.tabI2C.Controls.Add(Me.txtByteToWrite)
        Me.tabI2C.Controls.Add(Me.btnWriteByte)
        Me.tabI2C.Controls.Add(Me.Button2)
        Me.tabI2C.Controls.Add(Me.Button1)
        Me.tabI2C.Controls.Add(Me.cmdI2CReadData)
        Me.tabI2C.Controls.Add(Me.cmdI2CWriteData)
        Me.tabI2C.Controls.Add(Me.lblI2CReadValue)
        Me.tabI2C.Controls.Add(Me.Label6)
        Me.tabI2C.Controls.Add(Me.Label5)
        Me.tabI2C.Controls.Add(Me.cboI2CDataToSend)
        Me.tabI2C.Controls.Add(Me.Label4)
        Me.tabI2C.Controls.Add(Me.cboI2CRegisterAddress)
        Me.tabI2C.Controls.Add(Me.Label3)
        Me.tabI2C.Controls.Add(Me.cboI2CHardwareAddress)
        Me.tabI2C.Controls.Add(Me.lblbusSpeed)
        Me.tabI2C.Controls.Add(Me.cboI2CBusSpeed)
        Me.tabI2C.Location = New System.Drawing.Point(4, 22)
        Me.tabI2C.Name = "tabI2C"
        Me.tabI2C.Padding = New System.Windows.Forms.Padding(3)
        Me.tabI2C.Size = New System.Drawing.Size(354, 463)
        Me.tabI2C.TabIndex = 0
        Me.tabI2C.Text = "I2C"
        Me.tabI2C.UseVisualStyleBackColor = True
        '
        'IsFrontPositiveCheckBox
        '
        Me.IsFrontPositiveCheckBox.AutoSize = True
        Me.IsFrontPositiveCheckBox.Location = New System.Drawing.Point(19, 282)
        Me.IsFrontPositiveCheckBox.Name = "IsFrontPositiveCheckBox"
        Me.IsFrontPositiveCheckBox.Size = New System.Drawing.Size(104, 17)
        Me.IsFrontPositiveCheckBox.TabIndex = 28
        Me.IsFrontPositiveCheckBox.Text = "Is Front Positive "
        Me.IsFrontPositiveCheckBox.UseVisualStyleBackColor = True
        '
        'btnWrite2BtoINA209
        '
        Me.btnWrite2BtoINA209.Location = New System.Drawing.Point(180, 412)
        Me.btnWrite2BtoINA209.Name = "btnWrite2BtoINA209"
        Me.btnWrite2BtoINA209.Size = New System.Drawing.Size(167, 23)
        Me.btnWrite2BtoINA209.TabIndex = 27
        Me.btnWrite2BtoINA209.Text = "Write2BtoINA209"
        Me.btnWrite2BtoINA209.UseVisualStyleBackColor = True
        '
        'btnRead2BFromINA209
        '
        Me.btnRead2BFromINA209.Location = New System.Drawing.Point(9, 412)
        Me.btnRead2BFromINA209.Name = "btnRead2BFromINA209"
        Me.btnRead2BFromINA209.Size = New System.Drawing.Size(137, 23)
        Me.btnRead2BFromINA209.TabIndex = 26
        Me.btnRead2BFromINA209.Text = "Read2BFromINA209"
        Me.btnRead2BFromINA209.UseVisualStyleBackColor = True
        '
        'btnWriteToEEPROM
        '
        Me.btnWriteToEEPROM.Location = New System.Drawing.Point(131, 364)
        Me.btnWriteToEEPROM.Name = "btnWriteToEEPROM"
        Me.btnWriteToEEPROM.Size = New System.Drawing.Size(75, 23)
        Me.btnWriteToEEPROM.TabIndex = 25
        Me.btnWriteToEEPROM.Text = "WriteToEEPROM"
        Me.btnWriteToEEPROM.UseVisualStyleBackColor = True
        '
        'txtBack
        '
        Me.txtBack.Location = New System.Drawing.Point(167, 305)
        Me.txtBack.Name = "txtBack"
        Me.txtBack.Size = New System.Drawing.Size(37, 20)
        Me.txtBack.TabIndex = 24
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(126, 308)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(31, 13)
        Me.Label19.TabIndex = 23
        Me.Label19.Text = "back"
        '
        'txtFront
        '
        Me.txtFront.Location = New System.Drawing.Point(61, 305)
        Me.txtFront.Name = "txtFront"
        Me.txtFront.Size = New System.Drawing.Size(37, 20)
        Me.txtFront.TabIndex = 22
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(16, 308)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(28, 13)
        Me.Label18.TabIndex = 21
        Me.Label18.Text = "front"
        '
        'btnConnElectr
        '
        Me.btnConnElectr.Location = New System.Drawing.Point(248, 303)
        Me.btnConnElectr.Name = "btnConnElectr"
        Me.btnConnElectr.Size = New System.Drawing.Size(75, 23)
        Me.btnConnElectr.TabIndex = 20
        Me.btnConnElectr.Text = "Con.Electr."
        Me.btnConnElectr.UseVisualStyleBackColor = True
        '
        'txtByteToWrite
        '
        Me.txtByteToWrite.Location = New System.Drawing.Point(9, 240)
        Me.txtByteToWrite.Name = "txtByteToWrite"
        Me.txtByteToWrite.Size = New System.Drawing.Size(100, 20)
        Me.txtByteToWrite.TabIndex = 19
        '
        'btnWriteByte
        '
        Me.btnWriteByte.Location = New System.Drawing.Point(131, 237)
        Me.btnWriteByte.Name = "btnWriteByte"
        Me.btnWriteByte.Size = New System.Drawing.Size(135, 23)
        Me.btnWriteByte.TabIndex = 18
        Me.btnWriteByte.Text = "Write Byte"
        Me.btnWriteByte.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(18, 364)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 17
        Me.Button2.Text = "ReadEEPROMPacket"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(210, 149)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 16
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cmdI2CReadData
        '
        Me.cmdI2CReadData.Location = New System.Drawing.Point(154, 199)
        Me.cmdI2CReadData.Name = "cmdI2CReadData"
        Me.cmdI2CReadData.Size = New System.Drawing.Size(135, 23)
        Me.cmdI2CReadData.TabIndex = 15
        Me.cmdI2CReadData.Text = "Read Data"
        Me.cmdI2CReadData.UseVisualStyleBackColor = True
        '
        'cmdI2CWriteData
        '
        Me.cmdI2CWriteData.Location = New System.Drawing.Point(9, 199)
        Me.cmdI2CWriteData.Name = "cmdI2CWriteData"
        Me.cmdI2CWriteData.Size = New System.Drawing.Size(135, 23)
        Me.cmdI2CWriteData.TabIndex = 14
        Me.cmdI2CWriteData.Text = "Write Data"
        Me.cmdI2CWriteData.UseVisualStyleBackColor = True
        '
        'lblI2CReadValue
        '
        Me.lblI2CReadValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblI2CReadValue.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblI2CReadValue.Location = New System.Drawing.Point(6, 152)
        Me.lblI2CReadValue.Name = "lblI2CReadValue"
        Me.lblI2CReadValue.Size = New System.Drawing.Size(92, 21)
        Me.lblI2CReadValue.TabIndex = 13
        Me.lblI2CReadValue.Text = "..."
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label6.Location = New System.Drawing.Point(6, 139)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Data Read"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label5.Location = New System.Drawing.Point(164, 72)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(102, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Data To Send (Hex)"
        '
        'cboI2CDataToSend
        '
        Me.cboI2CDataToSend.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboI2CDataToSend.FormattingEnabled = True
        Me.cboI2CDataToSend.Location = New System.Drawing.Point(164, 88)
        Me.cboI2CDataToSend.Name = "cboI2CDataToSend"
        Me.cboI2CDataToSend.Size = New System.Drawing.Size(92, 21)
        Me.cboI2CDataToSend.TabIndex = 10
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label4.Location = New System.Drawing.Point(6, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Register Address"
        '
        'cboI2CRegisterAddress
        '
        Me.cboI2CRegisterAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboI2CRegisterAddress.FormattingEnabled = True
        Me.cboI2CRegisterAddress.Location = New System.Drawing.Point(6, 88)
        Me.cboI2CRegisterAddress.Name = "cboI2CRegisterAddress"
        Me.cboI2CRegisterAddress.Size = New System.Drawing.Size(92, 21)
        Me.cboI2CRegisterAddress.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label3.Location = New System.Drawing.Point(164, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(122, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Hardware Address (Hex)"
        '
        'cboI2CHardwareAddress
        '
        Me.cboI2CHardwareAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboI2CHardwareAddress.FormattingEnabled = True
        Me.cboI2CHardwareAddress.Location = New System.Drawing.Point(167, 32)
        Me.cboI2CHardwareAddress.Name = "cboI2CHardwareAddress"
        Me.cboI2CHardwareAddress.Size = New System.Drawing.Size(92, 21)
        Me.cboI2CHardwareAddress.TabIndex = 6
        '
        'lblbusSpeed
        '
        Me.lblbusSpeed.AutoSize = True
        Me.lblbusSpeed.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblbusSpeed.Location = New System.Drawing.Point(6, 16)
        Me.lblbusSpeed.Name = "lblbusSpeed"
        Me.lblbusSpeed.Size = New System.Drawing.Size(59, 13)
        Me.lblbusSpeed.TabIndex = 5
        Me.lblbusSpeed.Text = "Bus Speed"
        '
        'cboI2CBusSpeed
        '
        Me.cboI2CBusSpeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboI2CBusSpeed.FormattingEnabled = True
        Me.cboI2CBusSpeed.Items.AddRange(New Object() {"100KHz", "400KHz"})
        Me.cboI2CBusSpeed.Location = New System.Drawing.Point(6, 32)
        Me.cboI2CBusSpeed.Name = "cboI2CBusSpeed"
        Me.cboI2CBusSpeed.Size = New System.Drawing.Size(92, 21)
        Me.cboI2CBusSpeed.TabIndex = 0
        '
        'tabEasyScale
        '
        Me.tabEasyScale.Controls.Add(Me.chkEasyScaleLineHigh)
        Me.tabEasyScale.Controls.Add(Me.chkEasyScaleRequestAck)
        Me.tabEasyScale.Controls.Add(Me.chkEasyScaleVersion2)
        Me.tabEasyScale.Controls.Add(Me.cmdEasyScaleWriteData)
        Me.tabEasyScale.Controls.Add(Me.lblEasyScaleFeedBack)
        Me.tabEasyScale.Controls.Add(Me.Label8)
        Me.tabEasyScale.Controls.Add(Me.Label9)
        Me.tabEasyScale.Controls.Add(Me.cboEasyScaleDataToSend)
        Me.tabEasyScale.Controls.Add(Me.Label10)
        Me.tabEasyScale.Controls.Add(Me.cboEasyScaleRegisterAddress)
        Me.tabEasyScale.Controls.Add(Me.Label11)
        Me.tabEasyScale.Controls.Add(Me.cboEasyScaleHardwareAddress)
        Me.tabEasyScale.Controls.Add(Me.Label12)
        Me.tabEasyScale.Controls.Add(Me.cboEasyScaleBusSpeed)
        Me.tabEasyScale.Location = New System.Drawing.Point(4, 22)
        Me.tabEasyScale.Name = "tabEasyScale"
        Me.tabEasyScale.Padding = New System.Windows.Forms.Padding(3)
        Me.tabEasyScale.Size = New System.Drawing.Size(354, 463)
        Me.tabEasyScale.TabIndex = 1
        Me.tabEasyScale.Text = "EasyScale"
        Me.tabEasyScale.UseVisualStyleBackColor = True
        '
        'chkEasyScaleLineHigh
        '
        Me.chkEasyScaleLineHigh.AutoSize = True
        Me.chkEasyScaleLineHigh.Checked = True
        Me.chkEasyScaleLineHigh.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEasyScaleLineHigh.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.chkEasyScaleLineHigh.Location = New System.Drawing.Point(167, 148)
        Me.chkEasyScaleLineHigh.Name = "chkEasyScaleLineHigh"
        Me.chkEasyScaleLineHigh.Size = New System.Drawing.Size(99, 17)
        Me.chkEasyScaleLineHigh.TabIndex = 29
        Me.chkEasyScaleLineHigh.Text = "Keep Line High"
        Me.chkEasyScaleLineHigh.UseVisualStyleBackColor = True
        '
        'chkEasyScaleRequestAck
        '
        Me.chkEasyScaleRequestAck.AutoSize = True
        Me.chkEasyScaleRequestAck.Checked = True
        Me.chkEasyScaleRequestAck.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEasyScaleRequestAck.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.chkEasyScaleRequestAck.Location = New System.Drawing.Point(9, 148)
        Me.chkEasyScaleRequestAck.Name = "chkEasyScaleRequestAck"
        Me.chkEasyScaleRequestAck.Size = New System.Drawing.Size(88, 17)
        Me.chkEasyScaleRequestAck.TabIndex = 28
        Me.chkEasyScaleRequestAck.Text = "Request Ack"
        Me.chkEasyScaleRequestAck.UseVisualStyleBackColor = True
        '
        'chkEasyScaleVersion2
        '
        Me.chkEasyScaleVersion2.AutoSize = True
        Me.chkEasyScaleVersion2.Checked = True
        Me.chkEasyScaleVersion2.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEasyScaleVersion2.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.chkEasyScaleVersion2.Location = New System.Drawing.Point(9, 125)
        Me.chkEasyScaleVersion2.Name = "chkEasyScaleVersion2"
        Me.chkEasyScaleVersion2.Size = New System.Drawing.Size(190, 17)
        Me.chkEasyScaleVersion2.TabIndex = 27
        Me.chkEasyScaleVersion2.Text = "Use Version 2 Protocol 'Enhanced'"
        Me.chkEasyScaleVersion2.UseVisualStyleBackColor = True
        '
        'cmdEasyScaleWriteData
        '
        Me.cmdEasyScaleWriteData.Location = New System.Drawing.Point(9, 200)
        Me.cmdEasyScaleWriteData.Name = "cmdEasyScaleWriteData"
        Me.cmdEasyScaleWriteData.Size = New System.Drawing.Size(153, 23)
        Me.cmdEasyScaleWriteData.TabIndex = 26
        Me.cmdEasyScaleWriteData.Text = "Write Data"
        Me.cmdEasyScaleWriteData.UseVisualStyleBackColor = True
        '
        'lblEasyScaleFeedBack
        '
        Me.lblEasyScaleFeedBack.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblEasyScaleFeedBack.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblEasyScaleFeedBack.Location = New System.Drawing.Point(212, 200)
        Me.lblEasyScaleFeedBack.Name = "lblEasyScaleFeedBack"
        Me.lblEasyScaleFeedBack.Size = New System.Drawing.Size(92, 21)
        Me.lblEasyScaleFeedBack.TabIndex = 25
        Me.lblEasyScaleFeedBack.Text = "..."
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label8.Location = New System.Drawing.Point(212, 187)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(79, 13)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "Data Recieved"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label9.Location = New System.Drawing.Point(164, 73)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(102, 13)
        Me.Label9.TabIndex = 23
        Me.Label9.Text = "Data To Send (Hex)"
        '
        'cboEasyScaleDataToSend
        '
        Me.cboEasyScaleDataToSend.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEasyScaleDataToSend.FormattingEnabled = True
        Me.cboEasyScaleDataToSend.Location = New System.Drawing.Point(164, 89)
        Me.cboEasyScaleDataToSend.Name = "cboEasyScaleDataToSend"
        Me.cboEasyScaleDataToSend.Size = New System.Drawing.Size(92, 21)
        Me.cboEasyScaleDataToSend.TabIndex = 22
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label10.Location = New System.Drawing.Point(6, 73)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 13)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "Register Address"
        '
        'cboEasyScaleRegisterAddress
        '
        Me.cboEasyScaleRegisterAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEasyScaleRegisterAddress.FormattingEnabled = True
        Me.cboEasyScaleRegisterAddress.Location = New System.Drawing.Point(6, 89)
        Me.cboEasyScaleRegisterAddress.Name = "cboEasyScaleRegisterAddress"
        Me.cboEasyScaleRegisterAddress.Size = New System.Drawing.Size(92, 21)
        Me.cboEasyScaleRegisterAddress.TabIndex = 20
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label11.Location = New System.Drawing.Point(164, 17)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(122, 13)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Hardware Address (Hex)"
        '
        'cboEasyScaleHardwareAddress
        '
        Me.cboEasyScaleHardwareAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEasyScaleHardwareAddress.FormattingEnabled = True
        Me.cboEasyScaleHardwareAddress.Location = New System.Drawing.Point(167, 33)
        Me.cboEasyScaleHardwareAddress.Name = "cboEasyScaleHardwareAddress"
        Me.cboEasyScaleHardwareAddress.Size = New System.Drawing.Size(92, 21)
        Me.cboEasyScaleHardwareAddress.TabIndex = 18
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label12.Location = New System.Drawing.Point(6, 17)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(66, 13)
        Me.Label12.TabIndex = 17
        Me.Label12.Text = "Write Speed"
        '
        'cboEasyScaleBusSpeed
        '
        Me.cboEasyScaleBusSpeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEasyScaleBusSpeed.FormattingEnabled = True
        Me.cboEasyScaleBusSpeed.Items.AddRange(New Object() {"166Kbps", "100Kbps", "10Kbps", "1.66Kbps"})
        Me.cboEasyScaleBusSpeed.Location = New System.Drawing.Point(6, 33)
        Me.cboEasyScaleBusSpeed.Name = "cboEasyScaleBusSpeed"
        Me.cboEasyScaleBusSpeed.Size = New System.Drawing.Size(92, 21)
        Me.cboEasyScaleBusSpeed.TabIndex = 16
        '
        'tabDimming
        '
        Me.tabDimming.Controls.Add(Me.GroupBox2)
        Me.tabDimming.Controls.Add(Me.Label13)
        Me.tabDimming.Controls.Add(Me.cboDimmingNumSteps)
        Me.tabDimming.Controls.Add(Me.Label7)
        Me.tabDimming.Controls.Add(Me.chkDimmingEnable)
        Me.tabDimming.Controls.Add(Me.cmdDimmingWrite)
        Me.tabDimming.Location = New System.Drawing.Point(4, 22)
        Me.tabDimming.Name = "tabDimming"
        Me.tabDimming.Size = New System.Drawing.Size(354, 463)
        Me.tabDimming.TabIndex = 2
        Me.tabDimming.Text = "Dimming"
        Me.tabDimming.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.cboDimmingOff)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.cboDimmingDelay)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.cboDimmingPulse)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.cboDimmingStart)
        Me.GroupBox2.Location = New System.Drawing.Point(18, 65)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(320, 120)
        Me.GroupBox2.TabIndex = 33
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Delay Settings (uS)"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(159, 66)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(57, 13)
        Me.Label17.TabIndex = 40
        Me.Label17.Text = "Off Length"
        '
        'cboDimmingOff
        '
        Me.cboDimmingOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDimmingOff.FormattingEnabled = True
        Me.cboDimmingOff.Location = New System.Drawing.Point(162, 82)
        Me.cboDimmingOff.Name = "cboDimmingOff"
        Me.cboDimmingOff.Size = New System.Drawing.Size(65, 21)
        Me.cboDimmingOff.TabIndex = 39
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(6, 66)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(117, 13)
        Me.Label16.TabIndex = 38
        Me.Label16.Text = "Delay (between pulses)"
        '
        'cboDimmingDelay
        '
        Me.cboDimmingDelay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDimmingDelay.FormattingEnabled = True
        Me.cboDimmingDelay.Location = New System.Drawing.Point(9, 82)
        Me.cboDimmingDelay.Name = "cboDimmingDelay"
        Me.cboDimmingDelay.Size = New System.Drawing.Size(65, 21)
        Me.cboDimmingDelay.TabIndex = 37
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(159, 16)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(69, 13)
        Me.Label15.TabIndex = 36
        Me.Label15.Text = "Pulse Length"
        '
        'cboDimmingPulse
        '
        Me.cboDimmingPulse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDimmingPulse.FormattingEnabled = True
        Me.cboDimmingPulse.Location = New System.Drawing.Point(162, 32)
        Me.cboDimmingPulse.Name = "cboDimmingPulse"
        Me.cboDimmingPulse.Size = New System.Drawing.Size(65, 21)
        Me.cboDimmingPulse.TabIndex = 35
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 16)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(65, 13)
        Me.Label14.TabIndex = 34
        Me.Label14.Text = "Start Length"
        '
        'cboDimmingStart
        '
        Me.cboDimmingStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDimmingStart.FormattingEnabled = True
        Me.cboDimmingStart.Location = New System.Drawing.Point(9, 32)
        Me.cboDimmingStart.Name = "cboDimmingStart"
        Me.cboDimmingStart.Size = New System.Drawing.Size(65, 21)
        Me.cboDimmingStart.TabIndex = 33
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(177, 19)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(86, 13)
        Me.Label13.TabIndex = 32
        Me.Label13.Text = "Number of Steps"
        '
        'cboDimmingNumSteps
        '
        Me.cboDimmingNumSteps.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDimmingNumSteps.FormattingEnabled = True
        Me.cboDimmingNumSteps.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16"})
        Me.cboDimmingNumSteps.Location = New System.Drawing.Point(180, 35)
        Me.cboDimmingNumSteps.Name = "cboDimmingNumSteps"
        Me.cboDimmingNumSteps.Size = New System.Drawing.Size(65, 21)
        Me.cboDimmingNumSteps.TabIndex = 31
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(15, 19)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(144, 13)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Enable/Disable the ILED line"
        '
        'chkDimmingEnable
        '
        Me.chkDimmingEnable.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkDimmingEnable.Location = New System.Drawing.Point(18, 35)
        Me.chkDimmingEnable.Name = "chkDimmingEnable"
        Me.chkDimmingEnable.Size = New System.Drawing.Size(116, 24)
        Me.chkDimmingEnable.TabIndex = 28
        Me.chkDimmingEnable.Text = "Enabled"
        Me.chkDimmingEnable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkDimmingEnable.UseVisualStyleBackColor = True
        '
        'cmdDimmingWrite
        '
        Me.cmdDimmingWrite.Location = New System.Drawing.Point(18, 191)
        Me.cmdDimmingWrite.Name = "cmdDimmingWrite"
        Me.cmdDimmingWrite.Size = New System.Drawing.Size(228, 23)
        Me.cmdDimmingWrite.TabIndex = 27
        Me.cmdDimmingWrite.Text = "Send Pulse Data"
        Me.cmdDimmingWrite.UseVisualStyleBackColor = True
        '
        'tabTLC5924
        '
        Me.tabTLC5924.Controls.Add(Me.chkBankBEnabled)
        Me.tabTLC5924.Controls.Add(Me.chkBankAEnabled)
        Me.tabTLC5924.Controls.Add(Me.cmdTLC5924WriteDotCorrection)
        Me.tabTLC5924.Controls.Add(Me.cmdTLC5924WriteEnable)
        Me.tabTLC5924.Controls.Add(Me.cmdTLC5924WriteDotCorrectionAndEnable)
        Me.tabTLC5924.Controls.Add(Me.gridViewDotCorrection)
        Me.tabTLC5924.Controls.Add(Me.gridviewEnable)
        Me.tabTLC5924.Location = New System.Drawing.Point(4, 22)
        Me.tabTLC5924.Name = "tabTLC5924"
        Me.tabTLC5924.Size = New System.Drawing.Size(354, 463)
        Me.tabTLC5924.TabIndex = 3
        Me.tabTLC5924.Text = "TLC5924"
        Me.tabTLC5924.UseVisualStyleBackColor = True
        '
        'chkBankBEnabled
        '
        Me.chkBankBEnabled.AutoSize = True
        Me.chkBankBEnabled.Location = New System.Drawing.Point(9, 29)
        Me.chkBankBEnabled.Name = "chkBankBEnabled"
        Me.chkBankBEnabled.Size = New System.Drawing.Size(97, 17)
        Me.chkBankBEnabled.TabIndex = 31
        Me.chkBankBEnabled.Text = "Enable Bank B"
        Me.chkBankBEnabled.UseVisualStyleBackColor = True
        '
        'chkBankAEnabled
        '
        Me.chkBankAEnabled.AutoSize = True
        Me.chkBankAEnabled.Checked = True
        Me.chkBankAEnabled.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkBankAEnabled.Location = New System.Drawing.Point(9, 8)
        Me.chkBankAEnabled.Name = "chkBankAEnabled"
        Me.chkBankAEnabled.Size = New System.Drawing.Size(97, 17)
        Me.chkBankAEnabled.TabIndex = 30
        Me.chkBankAEnabled.Text = "Enable Bank A"
        Me.chkBankAEnabled.UseVisualStyleBackColor = True
        '
        'cmdTLC5924WriteDotCorrection
        '
        Me.cmdTLC5924WriteDotCorrection.Location = New System.Drawing.Point(195, 124)
        Me.cmdTLC5924WriteDotCorrection.Name = "cmdTLC5924WriteDotCorrection"
        Me.cmdTLC5924WriteDotCorrection.Size = New System.Drawing.Size(153, 23)
        Me.cmdTLC5924WriteDotCorrection.TabIndex = 29
        Me.cmdTLC5924WriteDotCorrection.Text = "Write Dot Correction Data"
        Me.cmdTLC5924WriteDotCorrection.UseVisualStyleBackColor = True
        '
        'cmdTLC5924WriteEnable
        '
        Me.cmdTLC5924WriteEnable.Location = New System.Drawing.Point(194, 28)
        Me.cmdTLC5924WriteEnable.Name = "cmdTLC5924WriteEnable"
        Me.cmdTLC5924WriteEnable.Size = New System.Drawing.Size(153, 23)
        Me.cmdTLC5924WriteEnable.TabIndex = 28
        Me.cmdTLC5924WriteEnable.Text = "Write Enable Data"
        Me.cmdTLC5924WriteEnable.UseVisualStyleBackColor = True
        '
        'cmdTLC5924WriteDotCorrectionAndEnable
        '
        Me.cmdTLC5924WriteDotCorrectionAndEnable.Location = New System.Drawing.Point(73, 213)
        Me.cmdTLC5924WriteDotCorrectionAndEnable.Name = "cmdTLC5924WriteDotCorrectionAndEnable"
        Me.cmdTLC5924WriteDotCorrectionAndEnable.Size = New System.Drawing.Size(205, 23)
        Me.cmdTLC5924WriteDotCorrectionAndEnable.TabIndex = 27
        Me.cmdTLC5924WriteDotCorrectionAndEnable.Text = "Write Both Data in one Operation"
        Me.cmdTLC5924WriteDotCorrectionAndEnable.UseVisualStyleBackColor = True
        '
        'gridViewDotCorrection
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridViewDotCorrection.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.gridViewDotCorrection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridViewDotCorrection.DefaultCellStyle = DataGridViewCellStyle2
        Me.gridViewDotCorrection.Location = New System.Drawing.Point(5, 149)
        Me.gridViewDotCorrection.Name = "gridViewDotCorrection"
        Me.gridViewDotCorrection.Size = New System.Drawing.Size(344, 60)
        Me.gridViewDotCorrection.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.gridViewDotCorrection, "values from 0 to 127")
        '
        'gridviewEnable
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridviewEnable.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.gridviewEnable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridviewEnable.DefaultCellStyle = DataGridViewCellStyle4
        Me.gridviewEnable.Location = New System.Drawing.Point(5, 55)
        Me.gridviewEnable.Name = "gridviewEnable"
        Me.gridviewEnable.Size = New System.Drawing.Size(344, 63)
        Me.gridviewEnable.TabIndex = 0
        '
        'tabPullups
        '
        Me.tabPullups.Controls.Add(Me.GroupBox5)
        Me.tabPullups.Controls.Add(Me.GroupBox4)
        Me.tabPullups.Controls.Add(Me.GroupBox3)
        Me.tabPullups.Location = New System.Drawing.Point(4, 22)
        Me.tabPullups.Name = "tabPullups"
        Me.tabPullups.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPullups.Size = New System.Drawing.Size(354, 463)
        Me.tabPullups.TabIndex = 4
        Me.tabPullups.Text = "Pull Ups"
        Me.tabPullups.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.rbRes22K_8)
        Me.GroupBox5.Controls.Add(Me.rbResOpen_8)
        Me.GroupBox5.Location = New System.Drawing.Point(33, 27)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(194, 40)
        Me.GroupBox5.TabIndex = 19
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "pin 8"
        '
        'rbRes22K_8
        '
        Me.rbRes22K_8.AutoSize = True
        Me.rbRes22K_8.Location = New System.Drawing.Point(97, 14)
        Me.rbRes22K_8.Name = "rbRes22K_8"
        Me.rbRes22K_8.Size = New System.Drawing.Size(47, 17)
        Me.rbRes22K_8.TabIndex = 1
        Me.rbRes22K_8.Text = "2.2K"
        Me.rbRes22K_8.UseVisualStyleBackColor = True
        '
        'rbResOpen_8
        '
        Me.rbResOpen_8.AutoSize = True
        Me.rbResOpen_8.Location = New System.Drawing.Point(7, 14)
        Me.rbResOpen_8.Name = "rbResOpen_8"
        Me.rbResOpen_8.Size = New System.Drawing.Size(51, 17)
        Me.rbResOpen_8.TabIndex = 0
        Me.rbResOpen_8.Text = "Open"
        Me.rbResOpen_8.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.rbRes688_9)
        Me.GroupBox4.Controls.Add(Me.rbRes1K_9)
        Me.GroupBox4.Controls.Add(Me.rbRes22K_9)
        Me.GroupBox4.Controls.Add(Me.rbResOpen_9)
        Me.GroupBox4.Location = New System.Drawing.Point(33, 73)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(194, 63)
        Me.GroupBox4.TabIndex = 18
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "pin 9"
        '
        'rbRes688_9
        '
        Me.rbRes688_9.AutoSize = True
        Me.rbRes688_9.Location = New System.Drawing.Point(97, 14)
        Me.rbRes688_9.Name = "rbRes688_9"
        Me.rbRes688_9.Size = New System.Drawing.Size(43, 17)
        Me.rbRes688_9.TabIndex = 3
        Me.rbRes688_9.Text = "688"
        Me.rbRes688_9.UseVisualStyleBackColor = True
        '
        'rbRes1K_9
        '
        Me.rbRes1K_9.AutoSize = True
        Me.rbRes1K_9.Location = New System.Drawing.Point(7, 37)
        Me.rbRes1K_9.Name = "rbRes1K_9"
        Me.rbRes1K_9.Size = New System.Drawing.Size(38, 17)
        Me.rbRes1K_9.TabIndex = 2
        Me.rbRes1K_9.Text = "1K"
        Me.rbRes1K_9.UseVisualStyleBackColor = True
        '
        'rbRes22K_9
        '
        Me.rbRes22K_9.AutoSize = True
        Me.rbRes22K_9.Location = New System.Drawing.Point(97, 37)
        Me.rbRes22K_9.Name = "rbRes22K_9"
        Me.rbRes22K_9.Size = New System.Drawing.Size(47, 17)
        Me.rbRes22K_9.TabIndex = 1
        Me.rbRes22K_9.Text = "2.2K"
        Me.rbRes22K_9.UseVisualStyleBackColor = True
        '
        'rbResOpen_9
        '
        Me.rbResOpen_9.AutoSize = True
        Me.rbResOpen_9.Location = New System.Drawing.Point(7, 14)
        Me.rbResOpen_9.Name = "rbResOpen_9"
        Me.rbResOpen_9.Size = New System.Drawing.Size(51, 17)
        Me.rbResOpen_9.TabIndex = 0
        Me.rbResOpen_9.Text = "Open"
        Me.rbResOpen_9.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rbRes688_10)
        Me.GroupBox3.Controls.Add(Me.rbRes1K_10)
        Me.GroupBox3.Controls.Add(Me.rbRes22K_10)
        Me.GroupBox3.Controls.Add(Me.rbResOpen_10)
        Me.GroupBox3.Location = New System.Drawing.Point(33, 142)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(194, 63)
        Me.GroupBox3.TabIndex = 17
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "pin 10"
        '
        'rbRes688_10
        '
        Me.rbRes688_10.AutoSize = True
        Me.rbRes688_10.Location = New System.Drawing.Point(97, 14)
        Me.rbRes688_10.Name = "rbRes688_10"
        Me.rbRes688_10.Size = New System.Drawing.Size(43, 17)
        Me.rbRes688_10.TabIndex = 3
        Me.rbRes688_10.Text = "688"
        Me.rbRes688_10.UseVisualStyleBackColor = True
        '
        'rbRes1K_10
        '
        Me.rbRes1K_10.AutoSize = True
        Me.rbRes1K_10.Location = New System.Drawing.Point(7, 37)
        Me.rbRes1K_10.Name = "rbRes1K_10"
        Me.rbRes1K_10.Size = New System.Drawing.Size(38, 17)
        Me.rbRes1K_10.TabIndex = 2
        Me.rbRes1K_10.Text = "1K"
        Me.rbRes1K_10.UseVisualStyleBackColor = True
        '
        'rbRes22K_10
        '
        Me.rbRes22K_10.AutoSize = True
        Me.rbRes22K_10.Location = New System.Drawing.Point(97, 37)
        Me.rbRes22K_10.Name = "rbRes22K_10"
        Me.rbRes22K_10.Size = New System.Drawing.Size(47, 17)
        Me.rbRes22K_10.TabIndex = 1
        Me.rbRes22K_10.Text = "2.2K"
        Me.rbRes22K_10.UseVisualStyleBackColor = True
        '
        'rbResOpen_10
        '
        Me.rbResOpen_10.AutoSize = True
        Me.rbResOpen_10.Location = New System.Drawing.Point(7, 14)
        Me.rbResOpen_10.Name = "rbResOpen_10"
        Me.rbResOpen_10.Size = New System.Drawing.Size(51, 17)
        Me.rbResOpen_10.TabIndex = 0
        Me.rbResOpen_10.Text = "Open"
        Me.rbResOpen_10.UseVisualStyleBackColor = True
        '
        'lstOutputConsole
        '
        Me.lstOutputConsole.FormattingEnabled = True
        Me.lstOutputConsole.HorizontalScrollbar = True
        Me.lstOutputConsole.Location = New System.Drawing.Point(380, 38)
        Me.lstOutputConsole.Name = "lstOutputConsole"
        Me.lstOutputConsole.ScrollAlwaysVisible = True
        Me.lstOutputConsole.SelectionMode = System.Windows.Forms.SelectionMode.None
        Me.lstOutputConsole.Size = New System.Drawing.Size(436, 641)
        Me.lstOutputConsole.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label2.Location = New System.Drawing.Point(380, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Output Console"
        '
        'USBEventTimer
        '
        Me.USBEventTimer.Interval = 1000
        '
        'cmdConsoleClear
        '
        Me.cmdConsoleClear.Location = New System.Drawing.Point(585, 12)
        Me.cmdConsoleClear.Name = "cmdConsoleClear"
        Me.cmdConsoleClear.Size = New System.Drawing.Size(231, 23)
        Me.cmdConsoleClear.TabIndex = 5
        Me.cmdConsoleClear.Text = "Clear Console"
        Me.cmdConsoleClear.UseVisualStyleBackColor = True
        '
        'frmGUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(828, 694)
        Me.Controls.Add(Me.cmdConsoleClear)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lstOutputConsole)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "frmGUI"
        Me.Text = "xUSBProtocol Driver"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.tabI2C.ResumeLayout(False)
        Me.tabI2C.PerformLayout()
        Me.tabEasyScale.ResumeLayout(False)
        Me.tabEasyScale.PerformLayout()
        Me.tabDimming.ResumeLayout(False)
        Me.tabDimming.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.tabTLC5924.ResumeLayout(False)
        Me.tabTLC5924.PerformLayout()
        CType(Me.gridViewDotCorrection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridviewEnable, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabPullups.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdGetVersion As System.Windows.Forms.Button
    Friend WithEvents cmdValidate As System.Windows.Forms.Button
    Friend WithEvents cmdProgram As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lstAvailableImages As System.Windows.Forms.ListBox
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tabI2C As System.Windows.Forms.TabPage
    Friend WithEvents tabEasyScale As System.Windows.Forms.TabPage
    Friend WithEvents lstOutputConsole As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblbusSpeed As System.Windows.Forms.Label
    Friend WithEvents cboI2CBusSpeed As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboI2CDataToSend As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboI2CRegisterAddress As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboI2CHardwareAddress As System.Windows.Forms.ComboBox
    Friend WithEvents cmdI2CReadData As System.Windows.Forms.Button
    Friend WithEvents cmdI2CWriteData As System.Windows.Forms.Button
    Friend WithEvents lblI2CReadValue As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmdEasyScaleWriteData As System.Windows.Forms.Button
    Friend WithEvents lblEasyScaleFeedBack As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cboEasyScaleDataToSend As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cboEasyScaleRegisterAddress As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboEasyScaleHardwareAddress As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboEasyScaleBusSpeed As System.Windows.Forms.ComboBox
    Friend WithEvents chkEasyScaleLineHigh As System.Windows.Forms.CheckBox
    Friend WithEvents chkEasyScaleRequestAck As System.Windows.Forms.CheckBox
    Friend WithEvents chkEasyScaleVersion2 As System.Windows.Forms.CheckBox
    Friend WithEvents USBEventTimer As System.Windows.Forms.Timer
    Friend WithEvents cmdConsoleClear As System.Windows.Forms.Button
    Friend WithEvents tabDimming As System.Windows.Forms.TabPage
    Friend WithEvents cmdDimmingWrite As System.Windows.Forms.Button
    Friend WithEvents chkDimmingEnable As System.Windows.Forms.CheckBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cboDimmingNumSteps As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cboDimmingStart As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents cboDimmingOff As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cboDimmingDelay As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cboDimmingPulse As System.Windows.Forms.ComboBox
    Friend WithEvents tabTLC5924 As System.Windows.Forms.TabPage
    Friend WithEvents gridViewDotCorrection As System.Windows.Forms.DataGridView
    Friend WithEvents gridviewEnable As System.Windows.Forms.DataGridView
    Friend WithEvents cmdTLC5924WriteDotCorrection As System.Windows.Forms.Button
    Friend WithEvents cmdTLC5924WriteEnable As System.Windows.Forms.Button
    Friend WithEvents cmdTLC5924WriteDotCorrectionAndEnable As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents chkBankAEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents chkBankBEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents tabPullups As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents rbRes688_10 As System.Windows.Forms.RadioButton
    Friend WithEvents rbRes1K_10 As System.Windows.Forms.RadioButton
    Friend WithEvents rbRes22K_10 As System.Windows.Forms.RadioButton
    Friend WithEvents rbResOpen_10 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents rbRes22K_8 As System.Windows.Forms.RadioButton
    Friend WithEvents rbResOpen_8 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents rbRes688_9 As System.Windows.Forms.RadioButton
    Friend WithEvents rbRes1K_9 As System.Windows.Forms.RadioButton
    Friend WithEvents rbRes22K_9 As System.Windows.Forms.RadioButton
    Friend WithEvents rbResOpen_9 As System.Windows.Forms.RadioButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents txtByteToWrite As System.Windows.Forms.TextBox
    Friend WithEvents btnWriteByte As System.Windows.Forms.Button
    Friend WithEvents txtBack As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtFront As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents btnConnElectr As System.Windows.Forms.Button
    Friend WithEvents btnWriteToEEPROM As System.Windows.Forms.Button
    Friend WithEvents btnRead2BFromINA209 As System.Windows.Forms.Button
    Friend WithEvents btnWrite2BtoINA209 As System.Windows.Forms.Button
    Friend WithEvents IsFrontPositiveCheckBox As System.Windows.Forms.CheckBox
End Class
