﻿using System;

namespace Adviser.DataSync.Core
{
    public class PractitionerRecord
    {
        public Guid ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BusinessName { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PrimaryPhone { get; set; }
        public int StatusID { get; set; }
    }
}
