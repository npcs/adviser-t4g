﻿using System;

namespace Adviser.Common.Logging
{
    public abstract class Logger<TEntry> : ILogger where TEntry : ILogEntry, new()
    {
        /// <summary>
        /// A String representing the application.
        /// </summary>
        private String _application;
        /// <summary>
        /// A bool indicating if this Logger is enabled or not.
        /// </summary>
        private bool _enabled = true;
        /// <summary>
        /// The filter through which all LogEntries must pass before being logged.
        /// </summary>
        private LogEntryFilter _filter;
        /// <summary>
        /// A formatter that formats LogEntries before being logged.
        /// </summary>
        private LogEntryFormatter _formatter;
        /// <summary>
        /// The default class to use for formatting.
        /// </summary>
        private static Type _defaultFormatterClass = typeof(DefaultLogEntryFormatter);
        /// <summary>
        /// The default class to use for filtering.
        /// </summary>
        private static Type _defaultFilterClass = typeof(LogEntryPassFilter);

        /// <summary>
        /// Utility property to simply indicate the version of the Logging assembly being used.
        /// </summary>
        public static Version Version
        {
            get { return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version; }
        }

        /// <summary>
        /// Gets and sets the Application.
        /// </summary>
        public String Application
        {
            get { return _application; }
            set { _application = value; }
        }
        /// <summary>
        /// Gets and sets the Enabled flag.
        /// </summary>
        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }
        /// <summary>
        /// Gets and sets the Filter.
        /// </summary>
        public LogEntryFilter Filter
        {
            get { return _filter; }
            set { _filter = value; }
        }
        /// <summary>
        /// Gets and sets the Formatter.
        /// </summary>
        public LogEntryFormatter Formatter
        {
            get { return _formatter; }
            set { _formatter = value; }
        }
        /// <summary>
        /// Gets and sets the DefaultFormatterClass.
        /// </summary>
        public static Type DefaultFormatterClass
        {
            get { return _defaultFormatterClass; }
            set { _defaultFormatterClass = value; }
        }
        /// <summary>
        /// Gets and sets the DefaultFilterClass.
        /// </summary>
        public static Type DefaultFilterClass
        {
            get { return _defaultFilterClass; }
            set { _defaultFilterClass = value; }
        }
        /// <summary>
        /// Logger constructor.
        /// </summary>
        protected Logger()
            : base()
        {
            Filter = GetDefaultFilter();
            Formatter = GetDefaultFormatter();
        }

        /// <summary>
        /// Really log logEntry. (We are past filtering at this point.)
        /// Subclasses might want to do something more interesting and override this method.
        /// </summary>
        /// <param name="logEntry">The LogEntry to log</param>
        /// <returns>true upon success, false upon failure.</returns>
        protected internal virtual bool DoLog(TEntry logEntry)
        {
            return WriteToLog(Formatter.AsString(logEntry));
        }
        /// <summary>
        /// Gets and sets the severity threshold of the filter--the lowest severity which will be logged.
        /// </summary>
        public LogSeverity SeverityThreshold
        {
            get { return Filter.SeverityThreshold; }
            set { Filter.SeverityThreshold = value; }
        }

        public ILogEntry GetLogEntry(LogSeverity severity, string category, Object logItem, string errorMessage)
        {
            return CreateLogEntry(severity, category, logItem, errorMessage) as ILogEntry;
        }

        /// <summary>
        /// Create a LogEntry.
        /// </summary>
        /// <remarks>
        /// This is virtual and can be overriden in subclasses that,
        /// although unlikely, might want to create a subclass of LogEntry.
        /// </remarks>
        /// <param name="severity">The severity of this log entry.</param>
        /// <param name="category">Category that classifies this log entry.</param>
        /// <param name="logItem">An Object (often a String) to log.</param>
        /// <returns>A LogEntry</returns>
        protected virtual TEntry CreateLogEntry(LogSeverity severity, string category, Object logItem, string errorMessage)
        {
            String logString;
            try
            {
                logString = logItem.ToString();
            }
            catch (Exception ex)
            {
                logString = "Exception while trying to convert logItem to string:\n" + ex.Message;
            }
            string logItemType = logItem.GetType().Name;
            TEntry entry = new TEntry()
            {
                Severity = severity,
                Application = Application,
                Category = category,
                LogItemType = logItemType,
                Details = logString,
                Time = DateTime.Now.ToUniversalTime(),
                ErrorMessage = errorMessage   
            };
            return entry;
        }
        /// <summary>
        /// Log something.
        /// </summary>
        /// <param name="severity">The severity of this log entry.</param>
        /// <param name="logItem">An Object (often a String) to log.</param>
        public void Log(LogSeverity severity, Object logItem)
        {
            Log(severity, null, logItem);
        }

        /// <summary>
        /// Log something
        /// </summary>
        /// <param name="severity"></param>
        /// <param name="category"></param>
        /// <param name="logItem"></param>
        public void Log(LogSeverity severity, string category, Object logItem)
        {
            Log(severity, category, logItem, null);
        }

        /// <summary>
        /// Log something.
        /// </summary>
        /// <param name="severity">The severity of this log entry.</param>
        /// <param name="category">String that classifies this log entry.</param>
        /// <param name="logItem">An Object (often a String) to log.</param>
        public void Log(LogSeverity severity, string category, Object logItem, string errorMessage)
        {
            LogInternal(CreateLogEntry(severity, category, logItem, errorMessage));
        }


        public void Log(ILogEntry logEntry)
        {
            LogInternal((TEntry)logEntry);
        }

        /// <summary>
        /// Log something.
        /// </summary>
        /// <param name="logEntry">The LogEntry.</param>
        /// <returns>true upon success, false upon failure.</returns>
        protected internal bool LogInternal(TEntry logEntry)
        {
            lock (this)
            {
                return ShouldLog(logEntry) ? DoLog(logEntry) : true;
            }
        }


        /// <summary>
        /// Make a "Debug" log entry.
        /// </summary>
        /// <param name="logItem">An Object (often a String) to log.</param>
        public void LogDebug(Object logItem)
        {
            Log(LogSeverity.Debug, logItem);
        }

        /// <summary>
        /// Make a "Debug" log entry.
        /// </summary>
        /// <param name="category">An Object (often a String) that classifies this log entry.</param>
        /// <param name="logItem">An Object (often a String) to log.</param>
        public void LogDebug(String category, Object logItem)
        {
            Log(LogSeverity.Debug, category, logItem);
        }

        /// <summary>
        /// Make an "Error" log entry.
        /// </summary>
        /// <param name="logItem">An Object (often a String) to log.</param>
        public void LogError(Object logItem)
        {
            Log(LogSeverity.Error, logItem);
        }

        public void LogError(Object logItem, string errorMessage)
        {
            Log(LogSeverity.Error, null, logItem, errorMessage);
        }

        /// <summary>
        /// Make an "Error" log entry.
        /// </summary>
        /// <param name="category">An Object (often a String) that classifies this log entry.</param>
        /// <param name="logItem">An Object (often a String) to log.</param>
        public void LogError(String category, Object logItem)
        {
            Log(LogSeverity.Error, category, logItem, null);
        }

        public void LogError(String category, Object logItem, string errorMessage)
        {
            Log(LogSeverity.Error, category, logItem, errorMessage);
        }


        /// <summary>
        /// Make an "Info" log entry.
        /// </summary>
        /// <param name="logItem">An Object (often a String) to log.</param>
        public void LogInfo(Object logItem)
        {
            Log(LogSeverity.Info, logItem);
        }
        /// <summary>
        /// Make an "Info" log entry.
        /// </summary>
        /// <param name="category">An Object (often a String) that classifies this log entry.</param>
        /// <param name="logItem">An Object (often a String) to log.</param>
        public void LogInfo(String category, Object logItem)
        {
            Log(LogSeverity.Info, category, logItem);
        }
        /// <summary>
        /// Make an "Status" log entry.
        /// </summary>
        /// <param name="logItem">An Object (often a String) to log.</param>
        public void LogStatus(Object logItem)
        {
            Log(LogSeverity.Status, logItem);
        }
        /// <summary>
        /// Make an "Status" log entry.
        /// </summary>
        /// <param name="category">An Object (often a String) that classifies this log entry.</param>
        /// <param name="logItem">An Object (often a String) to log.</param>
        public void LogStatus(String category, Object logItem)
        {
            Log(LogSeverity.Status, category, logItem);
        }
        /// <summary>
        /// Make an "Warning" log entry.
        /// </summary>
        /// <param name="logItem">An Object (often a String) to log.</param>
        public void LogWarning(Object logItem)
        {
            Log(LogSeverity.Warning, logItem);
        }
        /// <summary>
        /// Make an "Warning" log entry.
        /// </summary>
        /// <param name="category">An Object (often a String) that classifies this log entry.</param>
        /// <param name="logItem">An Object (often a String) to log.</param>
        public void LogWarning(String category, Object logItem)
        {
            Log(LogSeverity.Warning, category, logItem);
        }
        /// <summary>
        /// Create a new filter instance to use for this Logger.
        /// Use the DefaultFilterClass property.
        /// Subclasses may wish to override.
        /// </summary>
        /// <returns>A new filter.</returns>
        protected virtual LogEntryFilter GetDefaultFilter()
        {
            LogEntryFilter aFilter = null;
            try
            {
                aFilter = (LogEntryFilter)DefaultFilterClass.GetConstructor(new Type[0]).Invoke(new Object[0]);
            }
            catch
            {
                aFilter = new LogEntryPassFilter();
            }
            return aFilter;
        }
        /// <summary>
        /// Create a new formatter instance to use for this Logger.
        /// Use the DefaultFormatterClass property.
        /// Subclasses may wish to override.
        /// </summary>
        /// <returns>A new formatter.</returns>
        protected virtual LogEntryFormatter GetDefaultFormatter()
        {
            LogEntryFormatter aFormatter = null;
            try
            {
                aFormatter = (LogEntryFormatter)DefaultFormatterClass.GetConstructor(new Type[0]).Invoke(new Object[0]);
            }
            catch (Exception)
            {
                aFormatter = new DefaultLogEntryFormatter();
            }
            return aFormatter;
        }
        /// <summary>
        /// Determine if logEntry should be logged
        /// </summary>
        /// <param name="logEntry">The LogEntry to test.</param>
        /// <returns>true if logEntry should be logged.</returns>
        private bool ShouldLog(TEntry logEntry)
        {
            return Enabled && Filter.ShouldLog(logEntry);
        }
        /// <summary>
        /// Write a String to the log.
        /// Subclasses can override this to actually write to the log.
        /// </summary>
        /// <param name="s">The String</param>
        /// <returns>true upon success, false upon failure.</returns>
        protected virtual bool WriteToLog(String s)
        {
            return true;
        }
    }
}

