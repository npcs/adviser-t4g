﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Diagnostics;

using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;

using Adviser.Common.Logging;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Presentation;

namespace Adviser.Client.Core.Services
{
    /// <summary>
    /// Provides logging services to AdviserClient application.
    /// </summary
    public class LogService<TEntry> : Logger<TEntry> where TEntry : class, ILogEntry, new()
    {
        private CompositeLogger<AdviserLogEntry> _logger;
        private AppointmentSession _session;
        private IEventAggregator _eventAggregator;

        public LogService(AppointmentSession session, IEventAggregator eventAggregator)
        {
            _session = session;
            _eventAggregator = eventAggregator;

            CompositeLogger<AdviserLogEntry> compositeLogger = new CompositeLogger<AdviserLogEntry>();
            compositeLogger.Application = "AdviserClient";
            compositeLogger.Formatter = new DefaultLogEntryFormatter();

            //EventLog eventLog = new EventLog();
            //eventLog.Source = "AdviserClient";
            //EventLogLogger<AdviserLogEntry> eventLogLogger = new EventLogLogger<AdviserLogEntry>(eventLog);
            //compositeLogger.AddLogger("EventLog", eventLogLogger);

#if DEBUG
            TraceLogger<AdviserLogEntry> traceLogger = new TraceLogger<AdviserLogEntry>();
            compositeLogger.AddLogger("TraceLogger", traceLogger);
#else
            
#endif
            ClientDbLogger<AdviserLogEntry> dbLogger = new ClientDbLogger<AdviserLogEntry>();
            dbLogger.EventAggregator = eventAggregator;
            compositeLogger.AddLogger("AdviserDbLogger", dbLogger);

            _logger = compositeLogger;
        }

        protected override TEntry CreateLogEntry(LogSeverity severity, string category, object logItem, string errorMessage)
        {
            Guid practitionerId = _session.Practitioner != null ? _session.Practitioner.ID : Guid.Empty;
            Guid visitId = _session.Visit != null ? _session.Visit.ID : Guid.Empty;
            Guid deviceId = _session.Device != null ? _session.Device.ID : Guid.Empty;

            AdviserLogEntry entry = new AdviserLogEntry()
            {
                Application = "AdviserClient",
                DeviceId = deviceId,
                PractitionerId = practitionerId,
                VisitId = visitId,
                Details = logItem.ToString(),
                Severity = severity,
                Category = category,
                LogItemType = logItem.GetType().Name,
                Time = DateTime.Now.ToUniversalTime(),
                ErrorMessage = errorMessage
            };

            return entry as TEntry;
        }

        /// <summary>
        /// Exposes underlying logger.
        /// </summary>
        public Logger<AdviserLogEntry> Logger
        {
            get { return _logger; }
        }

        protected override bool DoLog(TEntry logEntry)
        {
            _logger.Log(logEntry);
            return true;
        }

        //public void LogError(string message)
        //{
        //    LogError(message, null);
        //}

        //public void LogWarning(string message)
        //{
        //    LogWarning(message, null);
        //}

        //public void LogInformation(string message)
        //{
        //    LogInformation(message, null);
        //}

        ///// <summary>
        ///// Logs event using underlying logging subsystem.
        ///// </summary>
        ///// <param name="details"></param>
        ///// <param name="ex"></param>
        //public void LogError(string message, object details)
        //{
        //    _logger.Log(CreateLogEntry(message, details, LogSeverity.Error));
        //}

        //public void LogWarning(string message, object details)
        //{
        //    _logger.Log(CreateLogEntry(message, details, LogSeverity.Warning));
        //}

        //public void LogInformation(string message, object details)
        //{
        //    _logger.Log(CreateLogEntry(message, details, LogSeverity.Info));
        //}

        //private AdviserLogEntry CreateLogEntry(string message, object details, LogSeverity severity)
        //{
        //    Guid practitionerId = _session.Practitioner != null ? _session.Practitioner.ID : Guid.Empty;
        //    Guid visitId = _session.Visit != null ? _session.Visit.ID : Guid.Empty;

        //    AdviserLogEntry entry = new AdviserLogEntry();
        //    {
        //        Application = "AdviserClient",
        //        //TODO: set device id
        //        PractitionerId = practitionerId,
        //        VisitId = visitId,
        //        Message = message,
        //        Details = details.ToString(),
        //        Severity = severity,
        //        Time = DateTime.Now.ToUniversalTime()
        //    };

        //    return entry;
        //}
    }
}
