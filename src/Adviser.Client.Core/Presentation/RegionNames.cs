﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Client.Core.Presentation
{
    /// <summary>
    /// Region names for the client UI layer
    /// </summary>
    public class RegionNames
    {
        public const string ToolbarRegion = "ToolbarRegion";
        public const string NavigationRegion = "NavigationRegion";
        public const string ContentRegion = "ContentRegion";
        public const string StatusBarRegion = "StatusBarRegion";
        public const string PrintRegion = "PrintRegion";
    }
}
