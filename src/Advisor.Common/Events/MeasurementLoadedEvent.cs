﻿using System.Collections.Generic;
using Microsoft.Practices.Composite.Wpf.Events;
using Advisor.Common.DataModel;

namespace Advisor.Common.Events
{
    public class MeasurementLoadedEvent : CompositeWpfEvent<MeasurementEventArgs>
    {
    }
}
