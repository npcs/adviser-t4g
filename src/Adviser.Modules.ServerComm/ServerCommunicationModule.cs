﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;

using Adviser.Common.Domain;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Presentation;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Services;
using Adviser.Client.Core.Interfaces;

namespace Adviser.Modules.ServerComm
{
    public class ServerCommunicationModule : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;
        private IServerCommunicationManager _commManager;

        public ServerCommunicationModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            RegisterViewsAndServices();

            _commManager = _container.Resolve<IServerCommunicationManager>();
        }

        protected void RegisterViewsAndServices()
        {
            _container.RegisterType<IServerCommunicationManager, ServerCommunicationManager>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IAdviserServiceAgent, AdviserServiceAgent>();
            _container.RegisterType<IConfigurationDataService, ConfigurationDataService>();
            _container.RegisterType<IDBService, DBService>(new Microsoft.Practices.Unity.ContainerControlledLifetimeManager());
        }
    }
}
