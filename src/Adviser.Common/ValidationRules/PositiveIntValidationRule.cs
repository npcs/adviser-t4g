﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Globalization;

namespace Adviser.Common.ValidationRules
{
    public class PositiveIntValidationRule : ValidationRule
    {
        public bool IsNullAllowed { get; set; }

        private decimal min = 0;
        private decimal max = Decimal.MaxValue;

        public decimal Min
        {
            get { return min; }
            set { min = value; }
        }
        public decimal Max
        {
            get { return max; }
            set { max = value; }
        }

        public override ValidationResult Validate(object value,
          CultureInfo cultureInfo)
        {
            decimal price = 0;

            try
            {
                if (IsNullAllowed && ((string)value).Length == 0)
                {
                    return ValidationResult.ValidResult;
                }
                else if (((string)value).Length > 0)
                {
                    price = Decimal.Parse((string)value, NumberStyles.Any, cultureInfo);
                }
            }
            catch
            {
                return new ValidationResult(false, "Illegal character(s).");
            }

            if ((price < Min) || (price > Max))
            {
                return new ValidationResult(false,
                  "Not in the range " + Min + " to " + Max + ".");
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }

    }
}
