﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Composite.Events;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Client.DataModel;

    public interface IVisitDetailsView
    {
        IVisitDetailsViewModel Model { get; set; }
    }
}
