﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace Advisor.Modules.Readout
{
    [ValueConversion(typeof(object), typeof(string))]
    public sealed class NumericToStringConverter : IValueConverter
    {
        private static readonly NumericToStringConverter _instance = new NumericToStringConverter();

        static NumericToStringConverter() { }

        public static NumericToStringConverter Instance
        {
            get { return _instance; }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            object result = null;
            if (value != null)
            {
                if (parameter != null)
                {
                    string formatString = (string)parameter;
                    //IFormatProvider fmt = new NumberFormatInfo();

                    result = ((double)value).ToString("F2");

                }
                else
                {
                    result = ((double)value).ToString();
                }

            }
            else
            {
                result = String.Empty;
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return double.Parse((string)value);
        }

    }
}
