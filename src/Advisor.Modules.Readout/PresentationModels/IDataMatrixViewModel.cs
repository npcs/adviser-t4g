﻿using System;
using System.Collections.ObjectModel;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Wpf.Events;
using Advisor.Modules.Readout.PresentationModels;
using Advisor.Modules.Readout.Views;
using Adviser.Common.Domain;

namespace Advisor.Modules.Readout.PresentationModels
{
    public interface IDataMatrixViewModel
    {
        event EventHandler<DataEventArgs<IDataCellViewModel>> CellSelected;
        ObservableCollection<ObservableCollection<DataCellViewModel>> Rows { get; }
        DelegateCommand<string> SaveFileCommand { get; }
        //ObservableCollection<DataCellViewModel> Cells { get; }
        double Min { get; }
        double Max { get; }
        double Mean { get; }
        double StandardDeviation { get; }
        IDataMatrixView View { get; set; }
        void UpdateData(DataMatrix<PairMeasurement> matrix);
        string Message { get; set;}
    }
}
