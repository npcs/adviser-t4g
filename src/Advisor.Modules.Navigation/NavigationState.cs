﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;

using Adviser.Client.Core.Events;
using Adviser.Client.Core.Presentation;

namespace Advisor.Modules.Navigation
{
    /// <summary>
    /// Encapsulates navigational state and corresponding behavor.
    /// Used by <see cref="Navigator"/>. Serves as base class for specific navigation state
    /// that can override default behavior.
    /// </summary>
    public class NavigationState : ViewModelBase
    {
        protected Dictionary<string, string> _subscriptions;
        private Dictionary<string, string> _contentMap;
        private Dictionary<string, NavigationControlViewModel> _controlMap;

        private ObservableCollection<NavigationControlViewModel> _controls;

        public NavigationState()
        {
            _subscriptions = new Dictionary<string, string>();
            _contentMap = new Dictionary<string, string>();
            _controls = new ObservableCollection<NavigationControlViewModel>();
            _controlMap = new Dictionary<string, NavigationControlViewModel>();
        }

        /// <summary>
        /// Name of the state.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Observable collection of presentation models for navigation controls.
        /// This property is a binding target for navigation view.
        /// </summary>
        public ObservableCollection<NavigationControlViewModel> Controls
        {
            get { return _controls; }
            private set { _controls = value; }
        }

        /// <summary>
        /// Mapping between regions and views.
        /// Defines which view loads to which region.
        /// </summary>
        public Dictionary<string, string> ContentMap
        {
            get { return _contentMap; }
        }

        /// <summary>
        /// Lookup dictionary for navigation controls.
        /// </summary>
        public Dictionary<string, NavigationControlViewModel> ControlMap
        {
            get { return _controlMap; }
        }

        /// <summary>
        /// Navigator.
        /// </summary>
        public Navigator Context
        {
            get;
            set;
        }

        public IRegionManager RegionManager
        {
            get;
            set;
        }

        /// <summary>
        /// Adds navigation control to the state's control collection.
        /// </summary>
        /// <param name="control"></param>
        public void AddControl(NavigationControlViewModel control)
        {
            Controls.Add(control);
            _controlMap.Add(control.Id, control);
            control.ControlSelected += new EventHandler<ControlSelectedEventArgs>(OnControlSelected);
        }

        /// <summary>
        /// Adds mapping between event and corresponding action (i.e. state transition).
        /// </summary>
        /// <param name="eventName">Event name this state is subscribing to.</param>
        /// <param name="action">Name of the state the current state must transition to.</param>
        public void AddSubscription(string eventName, string action)
        {
            _subscriptions.Add(eventName, action);
        }

        /// <summary>
        /// Adds mapping between content item (View) and presentation placeholder (Region)
        /// where this view must be presented.
        /// </summary>
        /// <param name="viewId">Identifier of the view.</param>
        /// <param name="regionName">Region name.</param>
        public void AddContentMapping(string viewId, string regionName)
        {
            _contentMap.Add(viewId, regionName);
        }

        /// <summary>
        /// This method is called from the context when the state is transitioning to.
        /// </summary>
        /// <param name="fromState">The previous state name</param>
        public virtual void Initialize(string fromState) 
        {
            LoadViews();
            ResetControls();
            SetControls();
        }

        protected void ResetControls()
        {
            foreach (NavigationControlViewModel control in _controls)
            {
                control.IsSelected = false;
            }
        }

        protected virtual void SetControls() { }

        /// <summary>
        /// Transition from current state to the next according to
        /// state transition map.
        /// </summary>
        /// <param name="eventName">Navigation event name.</param>
        public virtual void TransitionState(string eventName)
        {
            if (_subscriptions.ContainsKey(eventName))
            {
                NavigationState nextState = Context.FindState(_subscriptions[eventName]);
                Context.Navigate(nextState);
            }
        }

        /// <summary>
        /// Loads dependent views for this state.
        /// </summary>
        protected virtual void LoadViews()
        {
            IRegion region = null;
            foreach (string viewId in ContentMap.Keys)
            {
                region = RegionManager.Regions[ContentMap[viewId]];
                DeactivateViews(region);
                object view = region.GetView(viewId);
                if (view != null)
                    region.Activate(view);
            }
        }

        ///// <summary>
        ///// Transition from current state to the next according to
        ///// state transition map.
        ///// </summary>
        ///// <param name="context">Navigation context.</param>
        ///// <param name="e">Navigation event.</param>
        //public void Transition(Navigator context, NavigationEventArgs e)
        //{
        //    if (_subscriptions.ContainsKey(e.EventName))
        //    {
        //        NavigationState nextState = context.FindState(_subscriptions[e.EventName]);
        //        context.Navigate(nextState);
        //    }
        //}

        /// <summary>
        /// Event handler for navigation control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnControlSelected(object sender, ControlSelectedEventArgs e)
        {
            TransitionState(e.ActionName);
        }

        /// <summary>
        /// Removes all views from the region.
        /// </summary>
        /// <param name="region"></param>
        private void ClearRegion(IRegion region)
        {
            List<object> views = new List<object>();
            foreach (object v in region.Views)
            {
                views.Add(v);
            }
            foreach (object v in views)
            {
                region.Remove(v);
            }
        }

        /// <summary>
        /// Deactivates all active views in a given region.
        /// </summary>
        /// <param name="region">Region to deactivate views.</param>
        private void DeactivateViews(IRegion region)
        {
            List<object> views = new List<object>();
            foreach (object view in region.ActiveViews)
            {
                views.Add(view);
            }
            foreach (object v in views)
            {
                region.Deactivate(v);
            }
        }
    }
}
