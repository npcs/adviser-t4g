﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using System.Security.Permissions;

using System.Diagnostics;

namespace Advisor.Common.Configuration
{
    /// <summary>
    /// Generic dictionary which is XmlSerializable as well as binary serializable.
    /// It can be used as custom settings, property bag whenever keyed collection 
    /// is to be persisted, passed across application domains or used in the non-volatile cache.
    /// Serialized to XML in the following format:
    /// <properties>
    ///     <add key="" value=""/>
    /// </properties>
    /// </summary>
    /// <typeparam name="TKey">Type of key.</typeparam>
    /// <typeparam name="TVal">Type of value.</typeparam>
    [Serializable()]
    [XmlRoot("properties")]
    public class XmlDictionary<TKey, TVal> : IDictionary<TKey, TVal>,
        IXmlSerializable,
        ISerializable,
        IDeserializationCallback
    {
        private SerializationInfo deserData;
        private Dictionary<TKey, TVal> dict = new Dictionary<TKey, TVal>();

        public XmlDictionary() { }

        /// <summary>
        /// Deserialization constructor.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected XmlDictionary(SerializationInfo info, StreamingContext context)
        {
            deserData = info;
        }

        #region IDictionary<TKey,TVal> Members

        public ICollection<TKey> Keys
        {
            get { return dict.Keys; }
        }

        public void Add(TKey key, TVal value)
        {
            dict.Add(key, value);
        }

        public bool Remove(TKey key)
        {
            return dict.Remove(key);
        }

        public bool TryGetValue(TKey key, out TVal value)
        {
            return dict.TryGetValue(key, out value);
        }

        public ICollection<TVal> Values
        {
            get { return dict.Values; }
        }

        public TVal this[TKey key]
        {
            get
            {
                return dict[key];
            }
            set
            {
                dict[key] = value;
            }
        }

        public bool ContainsKey(TKey key)
        {
            return dict.ContainsKey(key);
        }

        #endregion

        #region ICollection<KeyValuePair<TKey,TVal>> Members

        public void Add(KeyValuePair<TKey, TVal> item)
        {
            dict.Add(item.Key, item.Value);
        }

        public void Clear()
        {
            dict.Clear();
        }

        public bool Contains(KeyValuePair<TKey, TVal> item)
        {
            return dict.ContainsKey(item.Key);
        }

        public void CopyTo(KeyValuePair<TKey, TVal>[] array, int arrayIndex)
        {
            List<KeyValuePair<TKey, TVal>> entries = new List<KeyValuePair<TKey, TVal>>(dict.Count);
            foreach (KeyValuePair<TKey, TVal> entry in dict)
            {
                entries.Add(entry);
            }
            entries.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return dict.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(KeyValuePair<TKey, TVal> item)
        {
            return dict.Remove(item.Key);
        }

        #endregion

        #region IEnumerable<KeyValuePair<TKey,TVal>> Members

        public IEnumerator<KeyValuePair<TKey, TVal>> GetEnumerator()
        {
            return dict.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return dict.GetEnumerator();
        }

        #endregion

        #region ISerializable Members

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            List<KeyValuePair<TKey, TVal>> items = new List<KeyValuePair<TKey, TVal>>(dict.Count);
            foreach (TKey key in dict.Keys)
            {
                KeyValuePair<TKey, TVal> item = new KeyValuePair<TKey, TVal>(key, dict[key]);
                items.Add(item);
            }

            info.AddValue("KeyValueArray", items.ToArray());
        }

        #endregion

        #region IDeserializationCallback Members

        public void OnDeserialization(object sender)
        {
            KeyValuePair<TKey, TVal>[] items = (KeyValuePair<TKey, TVal>[])deserData.GetValue("KeyValueArray", typeof(KeyValuePair<TKey, TVal>[]));
            foreach (KeyValuePair<TKey, TVal> item in items)
            {
                dict.Add(item.Key, item.Value);
            }
            deserData = null;
        }

        #endregion

        #region IXmlSerializable

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            foreach (TKey key in dict.Keys)
            {
                writer.WriteStartElement("add");
                writer.WriteAttributeString("key", key.ToString());
                writer.WriteAttributeString("value", dict[key].ToString());
                writer.WriteEndElement();
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(XmlKeyValue<TKey, TVal>));

            reader.MoveToContent();
            while (!reader.EOF)
            {
                if (reader.NodeType == XmlNodeType.Element && reader.Name == "add")
                {
                    XmlKeyValue<TKey, TVal> entry = (XmlKeyValue<TKey, TVal>)serializer.Deserialize(reader);
                    dict.Add(entry.Key, entry.Value);
                }

                reader.Read();

                /*
                r.MoveToAttribute(0);
                TKey key = (TKey)keySer.Deserialize(r);
                r.MoveToAttribute(1);
                TVal value = (TVal)valSer.Deserialize(r);
                dict.Add(key, value); 
                 * */
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        #endregion IXmlSerializable
    }

    /// <summary>
    /// Helper struct to control XmlSerialization process.
    /// </summary>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="V"></typeparam>
    [Serializable()]
    [XmlRoot("add")]
    public struct XmlKeyValue<K, V>
    {
        private K key;
        private V val;

        public XmlKeyValue(K name, V data)
        {
            this.key = name;
            this.val = data;
        }

        [XmlAttribute("key")]
        public K Key
        {
            get { return key; }
            set { key = value; }
        }

        [XmlAttribute("value")]
        public V Value
        {
            get { return val; }
            set { val = value; }
        }
    }
}
