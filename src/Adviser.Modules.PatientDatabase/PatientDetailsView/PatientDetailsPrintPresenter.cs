﻿using System;
using System.Collections.Generic;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Wpf;

using Adviser.Client.DataModel;
using Adviser.Client.Core.Presentation;

namespace Adviser.Modules.PatientDatabase
{
    /// <summary>
    /// Implementation of PrintPresenter for Patient Details.
    /// </summary>
    public class PatientDetailsPrintPresenter : PrintPresenter
    {
        public PatientDetailsPrintPresenter(PatientDetailsPrintView printableView, IPatientsPresentationModel model)
        {
            Name = "Patient Information";
            OrderKey = 10;
            SelectedToPrint = true;
            PrintableView = printableView;
            PrintableView.DataContext = model;
        }
    }
}
