﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;



namespace Advisor.Modules.Measurement
{
    public class MeasurementModule : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public MeasurementModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;

        }

        public void Initialize()
        {
            RegisterViewsAndServices();

        }

        protected void RegisterViewsAndServices()
        {

        }
    }
}
