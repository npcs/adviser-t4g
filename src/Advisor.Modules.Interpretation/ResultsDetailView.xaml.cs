﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Advisor.Modules.Interpretation.PresentationModels;
using System.ComponentModel;
namespace Advisor.Modules.Interpretation
{
    /// <summary>
    /// Interaction logic for ResultDetailsView.xaml
    /// </summary>
    public partial class ResultsDetailView : UserControl, IResultsDetailView
    {
        private MediaPlayer _player = new MediaPlayer();
        public ResultsDetailView()
        {
            InitializeComponent();
        }

        public IHumanBodyViewModel Model
        {
            get
            {
                return this.DataContext as IHumanBodyViewModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public void CreateCommandBindings()
        {
            //KeyBinding keyBinding = new KeyBinding(Model.ExportToExcelCommand, Key.S, ModifierKeys.Control);
            //keyBinding.CommandParameter = "Try";
            //InputBindings.Add(keyBinding);
        }

        //private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (e.AddedItems.Count == 1)
        //    {
        //        TabItem item = e.AddedItems[0] as TabItem;
        //        if (item == null)
        //            return;
        //        if (item.Name == "tabMandala")
        //        {
        //            _player.Open(new Uri(@"C:\Projects\IKO\Advisor\AdvisorClient\AdvisorClient\bin\Debug\Resources\Do_middle.mp3", UriKind.Absolute));
        //            _player.Play();
        //        }
        //        else
        //        {
        //            _player.Stop();
        //            _player.Close();
        //        }
        //    }
        //}

        #region Sorting

        GridViewColumnHeader _lastAdviserMedicineListHeaderClicked = null;
        ListSortDirection _lastAdviserMedicineListDirection = ListSortDirection.Ascending;

        GridViewColumnHeader _lastPractitionerPrescriptionListHeaderClicked = null;
        ListSortDirection _lastPractitionerPrescriptionListDirection = ListSortDirection.Ascending;

        void PractitionerPrescriptionListGridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;

            if (headerClicked != null && headerClicked.Column != null)
            {
                if (headerClicked != _lastPractitionerPrescriptionListHeaderClicked)
                {
                    direction = ListSortDirection.Ascending;
                }
                else
                {
                    if (_lastPractitionerPrescriptionListDirection == ListSortDirection.Ascending)
                    {
                        direction = ListSortDirection.Descending;
                    }
                    else
                    {
                        direction = ListSortDirection.Ascending;
                    }
                }
                string header = headerClicked.Column.Header as string;
                PractitionerPrescriptionListSort(header, direction);
                _lastPractitionerPrescriptionListHeaderClicked = headerClicked;
                _lastPractitionerPrescriptionListDirection = direction;
            }
        }

        void AdviserMedicineListGridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;

            if (headerClicked != null && headerClicked.Column != null)
            {
                if (headerClicked != _lastAdviserMedicineListHeaderClicked)
                {
                    direction = ListSortDirection.Ascending;
                }
                else
                {
                    if (_lastAdviserMedicineListDirection == ListSortDirection.Ascending)
                    {
                        direction = ListSortDirection.Descending;
                    }
                    else
                    {
                        direction = ListSortDirection.Ascending;
                    }
                }
                string header = headerClicked.Column.Header as string;
                AdviserMedicineListSort(header, direction);
                _lastAdviserMedicineListHeaderClicked = headerClicked;
                _lastAdviserMedicineListDirection = direction;
            }
        }

        private void PractitionerPrescriptionListSort(string sortBy, ListSortDirection direction)
        {
            ICollectionView dataView = CollectionViewSource.GetDefaultView(PractitionerPrescriptionList.ItemsSource);
            if (dataView != null)
            {
                dataView.SortDescriptions.Clear();
                SortDescription sd = new SortDescription(sortBy.Replace(" ", ""), direction);
                dataView.SortDescriptions.Add(sd);
                dataView.Refresh();
            }
        }

        private void AdviserMedicineListSort(string sortBy, ListSortDirection direction)
        {
            ICollectionView dataView = CollectionViewSource.GetDefaultView(AdviserMedicineList.ItemsSource);
            if (dataView != null)
            {
                dataView.SortDescriptions.Clear();
                SortDescription sd = new SortDescription(sortBy.Replace(" ", ""), direction);
                dataView.SortDescriptions.Add(sd);
                dataView.Refresh();
            }
        }

        #endregion

        private void UpdateDatabase(object sender, RoutedEventArgs e)
        {
            if(Model.ResultsDetail != null)
                Model.ResultsDetail.SaveChangesCommand.Execute(null);
        }

        private void AddPrescriptionButton_Click(object sender, RoutedEventArgs e)
        {
            BindingExpression NewPrescriptionNameTextBoxBinding = NewPrescriptionNameTextBox.GetBindingExpression(TextBox.TextProperty);
            NewPrescriptionNameTextBoxBinding.UpdateSource();

            BindingExpression NewPrescriptionDescriptionTextBoxBinding = NewPrescriptionDescriptionTextBox.GetBindingExpression(TextBox.TextProperty);
            NewPrescriptionDescriptionTextBoxBinding.UpdateSource();

            if (!Validation.GetHasError(NewPrescriptionNameTextBox) && !Validation.GetHasError(NewPrescriptionDescriptionTextBox))
            {
                Model.ResultsDetail.AddPrescriptionCommand.Execute(null);
            }
        }

        private void NotesTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Model.ResultsDetail.ShowNotesHistoryCommand.Execute(null);
            ShowNotesPopup(sender);
        }
        
        private void ShowNotesHistory_Click(object sender, RoutedEventArgs e)
        {
            Model.ResultsDetail.ShowNotesHistoryCommand.Execute(null);
            ShowNotesPopup(sender);
        }

        private void ShowNotesPopup(object sender)
        {
            VisitNotesHistoryPopup.PlacementTarget = (UIElement)sender;
            this.VisitNotesHistoryPopup.IsOpen = true;
            VisitNotesHistoryPopup.StaysOpen = true;
        }

        private void btnHideVisitNotesHistoryButton_Click(object sender, RoutedEventArgs e)
        {
            VisitNotesHistoryPopup.IsOpen = false;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            AdviserMedicineListSort("Name", ListSortDirection.Ascending);
        }

    }
}
