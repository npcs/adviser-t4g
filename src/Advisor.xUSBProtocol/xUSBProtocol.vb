Imports System.Threading

Public Class xUSBProtocol

#Region "Enums"

    Public Enum Status
        success
        generalFailure
        invalidAddress
        invalidLength
        invalidResponse
        driverError
        deviceNotFound
        unsupportedFeature
    End Enum
    Public Enum EEPROM_Status
        success = 0
        generalFailure = 1
        fileNotFound = 2
        fileFormatError = 3
        invalidRecordLength = 4
        invalidRecordAddress = 5
        invalidRecordType = 6
        invalidDataLength = 7
        invalidChecksum = 8
        invalidLength = 9
    End Enum
    Public Enum LogicLevel
        low = 0
        high = 1
    End Enum
    Public Enum ControlLine
        CONTROL_1 = 0
        CONTROL_2 = 1
        CONTROL_3 = 2
        CONTROL_4 = 3
        CONTROL_5 = 4
    End Enum
    Public Enum TransportType
        stub
        usbHid
        Ev2300
    End Enum
    Public Enum ResistorValue
        openDrain = 0
        ohm22k = 1
        ohm1k = 2
        ohm668 = 3
    End Enum
    Public Enum BusSpeed
        speed100KHz
        speed400KHz
    End Enum
    Public Enum UsbCommand 'V17 Universal

        'Host to Bridge USB commands
        version = &H0
        i2cWrite = &H14
        i2cRead = &H15
        eepromBlockWrite = &H18
        eepromBlockRead = &H19
        setPullUps = &H1A
        setBusSpeed = &H1B
        runTestProgram = &H7F

        'USB to Host USB commands
        versionResponse = &H80
        i2cWriteResponse = &H94
        i2cReadResponse = &H95
        eepromBlockWriteResponse = &H98
        eepromBlockReadResponse = &H99
        setPullUpsResponse = &H9A
        setBusSpeedResponse = &H9B
        runTestProgramResponse = &HFF

    End Enum
    Private Enum UsbCommand_EasyScale

        'Host to Bridge USB commands
        easyScaleWrite166Kbps = &H1
        easyScaleWrite100Kbps = &H2
        easyScaleWrite10Kbps = &H3
        easyScaleWrite1_66Kbs = &H4
        easyScaleChangeModePin = &H5
        easyScaleWriteTPS6116xPrelogic = &H6

        'USB to Host USB commands
        easyScaleWriteResponse166Kbps = &H81
        easyScaleWriteResponse100Kbps = &H82
        easyScaleWriteResponse10Kbps = &H83
        easyScaleWriteResponse1_66Kbps = &H84
        easyScaleChangeModePinResponse = &H85
        easyScaleWriteTPS6116xPrelogicResponse = &H86

    End Enum
    Public Enum EasyScaleSpeed
        _166Kbps = 0
        _100Kbps = 1
        _10Kbps = 2
        _1_66Kbps = 3
        '_500Kbps = 4
    End Enum
    Public Enum UsbCommand_Dimming 'V19 digital dimming
        dimmingSendPulse = &H1
        dimmingEnable = &H2

        dimmingSendPulseResponse = &H81
        dimmingEnableResponse = &H82
    End Enum

    Public Enum UsbCommand_TLC5924 'V20 TLC5924
        TLC5924_WriteFrameEnableAndDotCorrection = &H1
        TLC5924_WriteFrameEnable = &H2
        TLC5924_WriteFrameDotCorrection = &H3
        TLC5924_SetLEDBanks = &H7
        TLC5924_WriteDualFrameEnableAndDotCorrection = &H8
        'TLC5924_StopDualFrameOperation = &H9

        TLC5924_WriteFrameEnableAndDotCorrectionResponse = &H81
        TLC5924_WriteFrameEnableResponse = &H82
        TLC5924_WriteFrameDotCorrectionResponse = &H83
        TLC5924_SetLEDBanksResponse = &H87
        TLC5924_WriteDualFrameEnableAndDotCorrectionResponse = &H88
        'TLC5924_StopDualFrameOperationResponse = &H89

    End Enum

#End Region

#Region "Private Data Members"
    Private mUsbHid As UsbHid
    Private mPecEnabled As Boolean
    Private mUsbHidInUse As Boolean
    Private mRetries As Integer
    Private mDebugCapture As Boolean
    Private mDebugFile As IO.StreamWriter
    Private mBusSpeed As BusSpeed

    Private _IsBridgeAttached As Boolean = False
    Private _DoesUserWantToUpdateUSBBridge As Boolean = True
    Private _IsVersionOk As Boolean = False
    Private _ProtocolVersion As String = False
    Private _FirmwareCheckWindow As frmFirmwareCheck
#End Region

#Region "Public Properties."

    Public ReadOnly Property IsBridgeAttached() As Boolean
        Get
            Return _IsBridgeAttached
        End Get
    End Property

    Public ReadOnly Property DoesUserWantToUpdateUSBBridge() As Boolean
        Get
            Return _DoesUserWantToUpdateUSBBridge
        End Get
    End Property

    Public ReadOnly Property IsVersionOk() As Boolean
        Get
            Return _IsVersionOk
        End Get
    End Property

    Public ReadOnly Property ProtocolVersion() As String
        Get
            Return _ProtocolVersion
        End Get
    End Property

#End Region

#Region "Constructors. "

    
    ' .NET guarantees thread safety for Shared initialization 
    'Private Shared ReadOnly instance As xUSBProtocol

    'Public Shared Function GetxUSBProtocol(ByVal Image() As Byte, ByVal VersionRequiredByGUI As String) As xUSBProtocol

    '    Return instance
    'End Function


    Public Sub ReInitialize(ByVal Image() As Byte, ByVal VersionRequiredByGUI As String)

        mUsbHid = New UsbHid

        _IsBridgeAttached = mUsbHid.deviceIsAttached()

        mUsbHidInUse = False

        mRetries = 0

        mDebugCapture = False

        Me.FirmWareCheck(Image, VersionRequiredByGUI)

    End Sub


    Public Sub ReInitialize()

        mUsbHid = New UsbHid

        _IsBridgeAttached = mUsbHid.deviceIsAttached()

        mUsbHidInUse = False

        mRetries = 0

        mDebugCapture = False


    End Sub

    'Constructor should be private in Singelton patern
    Public Sub New()

        'call this function every time a hot plug event has occured and you need to reconnect the object to the hardware
        'Instantiate HID stuff
        mUsbHid = New UsbHid

        _IsBridgeAttached = mUsbHid.deviceIsAttached()

        mUsbHidInUse = False

        mRetries = 0

        mDebugCapture = False

        _FirmwareCheckWindow = New frmFirmwareCheck


    End Sub

#End Region

    'Registration with transport client
    Public Function register() As Status

        'See if we find the device

        If (mUsbHid.deviceIsAttached() = True) Then
            _IsBridgeAttached = True
            Return Status.success
        Else
            _IsBridgeAttached = False
            Return Status.deviceNotFound
        End If

    End Function
    Public Function unregister() As Status

        'No easy way to unregister in software, so don't let them.
        'They can always unplug and plug in the device
        'for automatic detection

        Return Status.unsupportedFeature

    End Function

#Region "I2C."
    '
    Public Function i2cWrite(ByVal devAddr As Byte, ByVal command As Byte, ByVal dataLen As Integer, ByVal data() As Byte) As Status

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Validate address
        If (devAddr < 1 Or devAddr > 127) Then
            Return Status.invalidAddress
        End If

        'Validate length
        If (dataLen < 1 Or dataLen > 61) Then
            Return Status.invalidLength
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = Me.UsbCommand.i2cWrite
        buf(1) = devAddr * 2 'addr << 1 + 0 for write
        buf(2) = command
        buf(3) = dataLen

        Dim i As Integer
        For i = 0 To dataLen - 1
            buf(i + 4) = data(i)
        Next

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        If (rxBuffer(0) <> UsbCommand.i2cWriteResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure

    End Function

    'Public Function i2cWriteByte(ByVal devAddr As Byte, ByVal data As Byte) As Status

    '    'Check driver
    '    If (_IsBridgeAttached = False) Then
    '        Return Status.deviceNotFound
    '    End If

    '    'Validate address
    '    If (devAddr < 1 Or devAddr > 127) Then
    '        Return Status.invalidAddress
    '    End If



    '    'Before we do anything else, make sure no one else is talking to USB HID
    '    While (mUsbHidInUse = True)
    '        System.Threading.Thread.Sleep(0)
    '    End While
    '    mUsbHidInUse = True

    '    Dim buf(64) As Byte

    '    'Load up buffer
    '    buf(0) = Me.UsbCommand.i2cWrite
    '    buf(1) = devAddr * 2 'addr << 1 + 0 for write
    '    buf(2) = data
    '    'buf(3) = dataLen



    '    'Send to driver
    '    If (sendAndReceive(buf) = False) Then
    '        mUsbHidInUse = False
    '        Return Status.driverError
    '    End If

    '    'Validate response
    '    If (rxBuffer(0) <> UsbCommand.i2cWriteResponse) Then
    '        mUsbHidInUse = False
    '        Return Status.driverError
    '    End If

    '    If (rxBuffer(1) = 0) Then
    '        mUsbHidInUse = False
    '        Return Status.success
    '    End If

    '    mUsbHidInUse = False
    '    Return Status.generalFailure

    'End Function

    Public Function i2cRead(ByVal devAddr As Byte, ByVal command As Byte, ByVal dataLen As Integer, ByRef data() As Byte) As Status

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Validate address
        If (devAddr < 1 Or devAddr > 127) Then
            Return Status.invalidAddress
        End If

        'Validate data length
        If (dataLen < 1 Or dataLen > 61) Then
            Return Status.invalidLength
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = UsbCommand.i2cRead
        buf(1) = devAddr * 2 'addr << 1 + 0 for write first
        buf(2) = command
        buf(3) = devAddr * 2 + 1 'addr << 1 + 1 for read next
        buf(4) = dataLen

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        If (rxBuffer(0) <> UsbCommand.i2cReadResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) <> 0) Then
            mUsbHidInUse = False
            Return Status.generalFailure
        End If

        Dim i As Integer
        For i = 0 To dataLen - 1
            data(i) = rxBuffer(i + 2)
        Next

        mUsbHidInUse = False
        Return Status.success

    End Function
    Public Function i2cWriteByteToRegister(ByVal devAddr As Byte, ByVal RegAddr As Byte, ByVal dataToWrite As Byte) As Status

        Dim dataLen As Byte
        dataLen = 1

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Validate address
        If (devAddr < 1 Or devAddr > 127) Then
            Return Status.invalidAddress
        End If

        'Validate length
        If (dataLen < 1 Or dataLen > 61) Then
            Return Status.invalidLength
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = UsbCommand.i2cWrite
        buf(1) = devAddr * 2 'addr << 1 + 0 for write
        buf(2) = RegAddr 'register address
        buf(3) = dataLen  ' num bytes to write
        buf(4) = dataToWrite ' data byte

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        If (rxBuffer(0) <> UsbCommand.i2cWriteResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure

    End Function

    Public Function i2cReadByteFromRegister(ByVal devAddr As Byte, ByVal RegAddr As Byte, ByRef ReturnByte As Byte) As Status
        Dim dataLen As Byte
        dataLen = 1

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Validate address
        If (devAddr < 1 Or devAddr > 127) Then
            Return Status.invalidAddress
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = UsbCommand.i2cRead
        buf(1) = devAddr * 2 'addr << 1 + 0 for write slave addr 'write address first
        buf(2) = RegAddr ' register to read from
        buf(3) = devAddr * 2 + 1 'addr << 1 + 1 for read 'read command
        buf(4) = dataLen 'length of read = 1 byte

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        If (rxBuffer(0) <> UsbCommand.i2cReadResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) <> 0) Then
            mUsbHidInUse = False
            Return Status.generalFailure
        End If

        'just return the first data byte according to the protocol 0 is the return code, 1 is the status, 2 is the first data
        ReturnByte = rxBuffer(2)

        mUsbHidInUse = False
        Return Status.success

    End Function
#End Region

#Region "EasyScale"
    Public Function easyScale_WriteByteToRegister(ByVal devAddr As Byte, _
       ByVal dataToWrite As Byte, ByVal Speed As EasyScaleSpeed, ByRef ReturnAck As Boolean) _
       As Status

        'compatible with firmware 18_1_0 or easyscale 1 only

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Validate address
        If (devAddr < 1 Or devAddr > 127) Then
            Return Status.invalidAddress
        End If


        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        If Speed = EasyScaleSpeed._1_66Kbps Then
            buf(0) = xUSBProtocol.UsbCommand_EasyScale.easyScaleWrite1_66Kbs
        ElseIf Speed = EasyScaleSpeed._10Kbps Then
            buf(0) = xUSBProtocol.UsbCommand_EasyScale.easyScaleWrite10Kbps
        ElseIf Speed = EasyScaleSpeed._100Kbps Then
            buf(0) = xUSBProtocol.UsbCommand_EasyScale.easyScaleWrite100Kbps
        ElseIf Speed = EasyScaleSpeed._166Kbps Then
            buf(0) = xUSBProtocol.UsbCommand_EasyScale.easyScaleWrite166Kbps
        Else
            Return Status.unsupportedFeature
        End If

        'Load up buffer

        buf(1) = 1 'num bytes hardcode to 1 for now 'future versions may need this
        buf(2) = devAddr 'device address
        buf(3) = dataToWrite ' data byte

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        'get the ack byte
        If (rxBuffer(2) = 1) Then
            ReturnAck = True
        Else
            ReturnAck = False
        End If

        'validate that the driver worked
        If Speed = EasyScaleSpeed._166Kbps Then
            If (rxBuffer(0) <> UsbCommand_EasyScale.easyScaleWriteResponse166Kbps) Then
                mUsbHidInUse = False
                Return Status.driverError
            End If
        ElseIf Speed = EasyScaleSpeed._100Kbps Then
            If (rxBuffer(0) <> UsbCommand_EasyScale.easyScaleWriteResponse100Kbps) Then
                mUsbHidInUse = False
                Return Status.driverError
            End If
        ElseIf Speed = EasyScaleSpeed._10Kbps Then
            If (rxBuffer(0) <> UsbCommand_EasyScale.easyScaleWriteResponse10Kbps) Then
                mUsbHidInUse = False
                Return Status.driverError
            End If
        ElseIf Speed = EasyScaleSpeed._1_66Kbps Then
            If (rxBuffer(0) <> UsbCommand_EasyScale.easyScaleWriteResponse1_66Kbps) Then
                mUsbHidInUse = False
                Return Status.driverError
            End If

        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure

    End Function

    Public Function easyScale_WriteByteToRegister_V2(ByVal devAddr As Byte, _
       ByVal dataToWrite As Byte, ByVal Speed As EasyScaleSpeed, ByVal RequestAck As Boolean, _
       ByVal RequestLineHigh As Boolean, ByRef ReturnAck As Boolean) _
       As Status

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Validate address
        If (devAddr < 1 Or devAddr > 255) Then
            Return Status.invalidAddress
        End If


        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        If Speed = EasyScaleSpeed._1_66Kbps Then
            buf(0) = xUSBProtocol.UsbCommand_EasyScale.easyScaleWrite1_66Kbs
        ElseIf Speed = EasyScaleSpeed._10Kbps Then
            buf(0) = xUSBProtocol.UsbCommand_EasyScale.easyScaleWrite10Kbps
        ElseIf Speed = EasyScaleSpeed._100Kbps Then
            buf(0) = xUSBProtocol.UsbCommand_EasyScale.easyScaleWrite100Kbps
        ElseIf Speed = EasyScaleSpeed._166Kbps Then
            buf(0) = xUSBProtocol.UsbCommand_EasyScale.easyScaleWrite166Kbps
        Else
            Return Status.unsupportedFeature
        End If

        'Load up buffer
        buf(1) = 1 'num bytes hardcode to 1 for now 'future versions may need this

        'request Ack
        If RequestAck Then buf(2) = 1 Else buf(2) = 0

        'request line high
        If RequestLineHigh Then buf(3) = 1 Else buf(3) = 0

        buf(4) = devAddr 'device address
        buf(5) = dataToWrite ' data byte

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        'get the ack byte
        If (rxBuffer(2) = 1) Then
            ReturnAck = True
        Else
            ReturnAck = False
        End If

        'validate that the driver worked
        If Speed = EasyScaleSpeed._166Kbps Then
            If (rxBuffer(0) <> UsbCommand_EasyScale.easyScaleWriteResponse166Kbps) Then
                mUsbHidInUse = False
                Return Status.driverError
            End If
        ElseIf Speed = EasyScaleSpeed._100Kbps Then
            If (rxBuffer(0) <> UsbCommand_EasyScale.easyScaleWriteResponse100Kbps) Then
                mUsbHidInUse = False
                Return Status.driverError
            End If
        ElseIf Speed = EasyScaleSpeed._10Kbps Then
            If (rxBuffer(0) <> UsbCommand_EasyScale.easyScaleWriteResponse10Kbps) Then
                mUsbHidInUse = False
                Return Status.driverError
            End If
        ElseIf Speed = EasyScaleSpeed._1_66Kbps Then
            If (rxBuffer(0) <> UsbCommand_EasyScale.easyScaleWriteResponse1_66Kbps) Then
                mUsbHidInUse = False
                Return Status.driverError
            End If

        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure

    End Function


    Public Function easyScale_ChangeModePin(ByVal Enabled As Boolean) As Status
        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If


        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = xUSBProtocol.UsbCommand_EasyScale.easyScaleChangeModePin
        If Enabled Then
            buf(1) = 1 'mode pin enabled 1 = true 0 = false ' this causes the mode pin to go high
        Else
            buf(1) = 0
        End If
        ' or low immediately

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If



        'validate that the driver worked

        If (rxBuffer(0) <> UsbCommand_EasyScale.easyScaleChangeModePinResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        Else
            mUsbHidInUse = False
            Return Status.success
        End If

    End Function

    Public Function easyScale_Write6116xPreLogic() As Status

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If


        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = xUSBProtocol.UsbCommand_EasyScale.easyScaleWriteTPS6116xPrelogic

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If



        'validate that the driver worked

        If (rxBuffer(0) <> UsbCommand_EasyScale.easyScaleWriteTPS6116xPrelogicResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        Else
            mUsbHidInUse = False
            Return Status.success
        End If


    End Function

#End Region

#Region "Dimming"
    Public Function dimming_SendPulses(ByVal NumPulses As Byte, ByVal Num_uSecondsStart As UInt16, _
        ByVal Num_uSecondsPulse As UInt16, ByVal Num_uSecondsDelay As UInt16, _
        ByVal Num_uSecondsOff As UInt16) As Status

        'compatible with firmware 19_1_0

        'validate the Inputs
        If NumPulses <= 0 Then Return Status.unsupportedFeature
        If Num_uSecondsStart < 0 Or Num_uSecondsStart > 1000 Then Return Status.unsupportedFeature
        If Num_uSecondsPulse < 0 Or Num_uSecondsPulse > 1000 Then Return Status.unsupportedFeature
        If Num_uSecondsDelay < 0 Or Num_uSecondsDelay > 1000000 Then Return Status.unsupportedFeature
        If Num_uSecondsOff < 0 Or Num_uSecondsOff > 1000 Then Return Status.unsupportedFeature

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'do the necessary calculations
        Dim TempInt As UInt16
        Dim StartByteHigh As Byte
        Dim StartByteLow As Byte
        Dim PulseByteHigh As Byte
        Dim PulseByteLow As Byte
        Dim DelayByteHigh As Byte
        Dim DelayByteLow As Byte
        Dim OffByteHigh As Byte
        Dim OffByteLow As Byte

        'Convert them to IntLoopCounts
        Num_uSecondsStart = Me.ConvertuSecondsToCountStepsInts(Num_uSecondsStart)
        Num_uSecondsPulse = Me.ConvertuSecondsToCountStepsInts(Num_uSecondsPulse)
        Num_uSecondsDelay = Me.ConvertuSecondsToCountStepsInts(Num_uSecondsDelay)
        Num_uSecondsOff = Me.ConvertuSecondsToCountStepsInts(Num_uSecondsOff)

        'start byte  
        StartByteHigh = Num_uSecondsStart >> 8
        TempInt = Num_uSecondsStart << 8
        StartByteLow = TempInt >> 8

        'Pulse byte  
        PulseByteHigh = Num_uSecondsPulse >> 8
        TempInt = Num_uSecondsPulse << 8
        PulseByteLow = TempInt >> 8

        'Delay byte  
        DelayByteHigh = Num_uSecondsDelay >> 8
        TempInt = Num_uSecondsDelay << 8
        DelayByteLow = TempInt >> 8

        'Off time calculations
        OffByteHigh = Num_uSecondsOff >> 8
        TempInt = Num_uSecondsOff << 8
        OffByteLow = TempInt >> 8


        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = xUSBProtocol.UsbCommand_Dimming.dimmingSendPulse
        buf(1) = NumPulses 'number of pulses to make
        buf(2) = StartByteHigh 'count for start delay
        buf(3) = StartByteLow 'count for start delay
        buf(4) = PulseByteHigh 'count for Pulse delay
        buf(5) = PulseByteLow 'count for Pulse delay
        buf(6) = DelayByteHigh 'count for real delay
        buf(7) = DelayByteLow 'count for real delay
        buf(8) = OffByteHigh 'count for off delay (high part of the number
        buf(9) = OffByteLow 'count for off delay (low part of the number)

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'validate that the driver worked
        If (rxBuffer(0) <> UsbCommand_Dimming.dimmingSendPulseResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure


    End Function

    Private Function ConvertuSecondsToCountStepsInts(ByVal num_uSeconds As UInt16) As UInt16
        Dim returnInt As UInt16

        'this is for Loops that aren't affected by an outside loop such as pulse timing and start timing
        'one byte is bounded by 3 and 5000 for now it's really 131,074
        If num_uSeconds > 60000 Then num_uSeconds = 60000
        If num_uSeconds < 4 Then num_uSeconds = 4

        'do the conversion 
        returnInt = Math.Round(((num_uSeconds - 2) / 2) - 1)
        Return returnInt

    End Function

    Public Function dimming_Enable(ByVal Enable As Boolean) As Status

        'compatible with firmware 19_1_0

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = xUSBProtocol.UsbCommand_Dimming.dimmingEnable

        If Enable = False Then
            buf(1) = 0
        Else
            buf(1) = 1
        End If

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'validate that the driver worked
        If (rxBuffer(0) <> UsbCommand_Dimming.dimmingEnableResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure


    End Function
#End Region

#Region "TLC5924"

    Public Function TLC5924_WriteFrameEnableAndDotCorrection(ByRef EnableData() As Byte, ByRef DotCorrectionData() As Byte) As Status

        'compatible with firmware 20_1_0

        'validate the Inputs

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = xUSBProtocol.UsbCommand_TLC5924.TLC5924_WriteFrameEnableAndDotCorrection
        Dim i As Integer
        For i = 1 To 14
            buf(i) = DotCorrectionData(i - 1) 'fill the dotcorrection bytes
        Next i

        'fill the Enable data
        buf(15) = EnableData(0)
        buf(16) = EnableData(1)

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'validate that the driver worked
        If (rxBuffer(0) <> UsbCommand_TLC5924.TLC5924_WriteFrameEnableAndDotCorrectionResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure


    End Function

    Public Function TLC5924_WriteFrameDotCorrection(ByRef DotCorrectionData() As Byte) As Status

        'compatible with firmware 20_1_0

        'validate the Inputs

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = xUSBProtocol.UsbCommand_TLC5924.TLC5924_WriteFrameDotCorrection
        Dim i As Integer
        For i = 1 To 14
            buf(i) = DotCorrectionData(i - 1) 'fill the dotcorrection bytes
        Next i

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'validate that the driver worked
        If (rxBuffer(0) <> UsbCommand_TLC5924.TLC5924_WriteFrameDotCorrectionResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure


    End Function

    Public Function TLC5924_WriteFrameEnable(ByRef EnableData() As Byte) As Status

        'compatible with firmware 20_1_0

        'validate the Inputs

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = xUSBProtocol.UsbCommand_TLC5924.TLC5924_WriteFrameEnable
        'fill the Enable data
        buf(1) = EnableData(0)
        buf(2) = EnableData(1)

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'validate that the driver worked
        If (rxBuffer(0) <> UsbCommand_TLC5924.TLC5924_WriteFrameEnableResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure


    End Function

    Public Function TLC5924_WriteDualFrameEnableAndDotCorrection(ByRef EnableDataA() As Byte, ByRef DotCorrectionA() As Byte, _
        ByRef EnableDataB() As Byte, ByRef DotCorrectionB() As Byte) As Status

        'compatible with firmware 20_1_0

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer

        buf(0) = xUSBProtocol.UsbCommand_TLC5924.TLC5924_WriteDualFrameEnableAndDotCorrection
        Dim i As Integer
        For i = 1 To 14
            buf(i) = DotCorrectionA(i - 1) 'fill the dotcorrection bytes
        Next i

        'fill the Enable data
        buf(15) = EnableDataA(0)
        buf(16) = EnableDataA(1)

        'bank B
        For i = 1 To 14
            buf(16 + i) = DotCorrectionB(i - 1) 'fill the dotcorrection bytes
        Next i

        'fill the Enable data
        buf(31) = EnableDataB(0)
        buf(32) = EnableDataB(1)




        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'validate that the driver worked
        If (rxBuffer(0) <> UsbCommand_TLC5924.TLC5924_WriteDualFrameEnableAndDotCorrectionResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure


    End Function

    'set the LED banks 
    Public Function TLC5294_SetLED_Banks(ByVal bankAEnabled As Boolean, ByVal bankBEnabled As Boolean) As Status

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = UsbCommand_TLC5924.TLC5924_SetLEDBanks
        If bankAEnabled = False Then
            buf(1) = 0
        Else
            buf(1) = 1
        End If
        If bankBEnabled = False Then
            buf(2) = 0
        Else
            buf(2) = 1
        End If

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        If (rxBuffer(0) <> UsbCommand_TLC5924.TLC5924_SetLEDBanksResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure

    End Function

#End Region


    'Resistor values on board
    Public Function setPullUps(ByVal Line10Value As ResistorValue, ByVal Line9Value As ResistorValue, _
         ByVal Line8Value As ResistorValue) As Status

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = UsbCommand.setPullUps
        buf(1) = CByte(Line10Value)
        buf(2) = CByte(Line9Value)
        buf(3) = CByte(Line8Value)

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        If (rxBuffer(0) <> UsbCommand.setPullUpsResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure

    End Function

    'Utilities
    Public Sub codeToString(ByVal code As Status, ByRef str As String)

        Select Case code
            Case Status.success
                str = "Success"
            Case Status.generalFailure
                str = "General Failure"
            Case Status.invalidAddress
                str = "Invalid Address"
            Case Status.invalidLength
                str = "Invalid Data Length"
            Case Status.invalidResponse
                str = "Invalid Response from target"
            Case Status.driverError
                str = "Driver error"
            Case Status.deviceNotFound
                str = "Device Not Found"
            Case Status.unsupportedFeature
                str = "Unsupported Feature"
            Case Else
                str = "Unknown Code"
        End Select

    End Sub

    Public Function type() As TransportType
        Return TransportType.usbHid
    End Function
    Public Function typeString() As String
        Return "USB HID"
    End Function
    Public Function targetAttached() As Boolean
        Return (mUsbHid.deviceIsAttached())
    End Function
    Public Function runTestProgram() As Status

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = UsbCommand.runTestProgram

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        If (rxBuffer(0) <> UsbCommand.runTestProgramResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure

    End Function

    Public Function version(ByRef ver As String) As Status

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = UsbCommand.version

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        If (rxBuffer(0) <> UsbCommand.versionResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If
        ver = CStr(rxBuffer(1)) & "." & CStr(rxBuffer(2)) & "." & CStr(rxBuffer(3))

        mUsbHidInUse = False
        Return Status.success

    End Function
    Public Function setBusSpeed(ByVal speed As BusSpeed) As Status

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = UsbCommand.setBusSpeed

        If (speed = BusSpeed.speed100KHz) Then
            buf(1) = 0
        Else
            buf(1) = 1
        End If

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        If (rxBuffer(0) <> UsbCommand.setBusSpeedResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure

    End Function

    Public Function ResetHardware() As Status
        'cause the Hardware to load from scratch

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = 1 'UsbCommand.version

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        'If (rxBuffer(0) <> UsbCommand.versionResponse) Then
        '    mUsbHidInUse = False
        '    Return Status.driverError
        'End If
        'ver = CStr(rxBuffer(1)) & "." & CStr(rxBuffer(2)) & "." & CStr(rxBuffer(3))

        mUsbHidInUse = False
        Return Status.success

    End Function

    'Update EEPROM firmware
    Public Function validateImageOnEEPROM(ByVal ImageData As Byte()) As EEPROM_Status

        Dim address As Integer = 0
        Dim byteCount As Integer
        Dim data(60) As Byte
        Dim eepromData(32) As Byte
        Dim remainder As Integer = ImageData.Length

        Dim p As Progress_Bar_Window = New Progress_Bar_Window("Validating EEPROM...", ImageData.Length / 32)
        p.setText("EEPROM programming progress")
        p.Show()

        'p.Close()
        'Return EEPROM_Status.generalFailure

        While (remainder > 0)


            Thread.Sleep(10)

            Dim i As Integer
            If (remainder >= 32) Then
                byteCount = 32
            Else
                byteCount = remainder
            End If

            For i = 0 To byteCount - 1
                data(i) = ImageData(i + address)
            Next
            Dim numRetries As Integer = 0

retryLabel:

            Dim status As xUSBProtocol.Status = Me.readEepromPacket(address, byteCount, eepromData)
            If (status <> xUSBProtocol.Status.success) Then
                MessageBox.Show("Error reading EEPROM at address: " + Hex(address) + "h, byteCount: " + CStr(byteCount), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                p.Close()
                Return EEPROM_Status.generalFailure
            End If

            'Validate what we got back
            For i = 0 To byteCount - 1
                If (data(i) <> eepromData(i)) Then

                    If (numRetries < 3) Then
                        numRetries += 1
                        GoTo retryLabel
                    End If

                    MessageBox.Show("Error comparing EEPROM data at address: " + Hex(address) + "h, offset: " + CStr(i), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
                    p.Close()
                    Return EEPROM_Status.generalFailure
                End If
            Next

            address += byteCount
            remainder -= byteCount

            'Progress bar update
            p.incrementValue()


        End While

        p.Close()
        Return EEPROM_Status.success

    End Function
    Public Function putImageOnEEPROM(ByVal ImageData As Byte()) As EEPROM_Status

        'validate the file
        Dim ValidationStatus As Status
        ValidationStatus = Me.validateImage(ImageData)

        'set it to 100khz for programming
        Me.setBusSpeed(BusSpeed.speed100KHz)

        'Go through file  and send to hardware
        Dim address As Integer = 0

        Dim byteCount As Integer
        Dim data(32) As Byte
        Dim remainder As Integer = ImageData.Length
        '        Dim remainder = 32

        Dim p As Progress_Bar_Window = New Progress_Bar_Window("Updating EEPROM - DO NOT REMOVE USB CABLE!", ImageData.Length / 32)
        'p.setText("EEPROM programming progress")
        p.Show()


        ''Debug Code. Delete
        'p.Close()
        'Return EEPROM_Status.success

        While (remainder > 0)


            Thread.Sleep(10)

            If (remainder >= 32) Then
                byteCount = 32
            Else
                byteCount = remainder
            End If

            'Debug code
            Dim i As Integer
            Dim debugString As String = "Write Data: Address: " + Hex(address) + ", byteCount: " + CStr(byteCount) + Environment.NewLine
            For i = 0 To byteCount - 1
                data(i) = ImageData(i + address)
                debugString += " "
                If (data(i) <= &HF) Then
                    debugString += "0"
                End If
                debugString += Hex(data(i))
            Next

            debugString += "h"
            'MsgBox(debugString)




            Dim status As xUSBProtocol.Status = Me.writeEepromPacket(address, byteCount, data)
            If (status <> xUSBProtocol.Status.success) Then
                MessageBox.Show("Error writing EEPROM at address: " + Hex(address) + "h, byteCount: " + CStr(byteCount), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                p.Close()
                Return EEPROM_Status.generalFailure
            End If

            address += byteCount
            remainder -= byteCount

            'Progress bar update
            p.incrementValue()


        End While

        p.Close()
        Return EEPROM_Status.success

    End Function

    'private
    Private Function validateImage(ByVal ImageData As Byte()) As EEPROM_Status

        'Validate that this file contains a valid Intel style image
        Try

            'Check size of file
            If (ImageData.Length > 8 * 1024) Then
                MessageBox.Show("file too large", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                Return Status.invalidLength
            End If

            Return EEPROM_Status.success

        Catch ex As Exception

            Return EEPROM_Status.generalFailure

        End Try

    End Function
    Public Function writeEepromPacket(ByVal address As Integer, ByVal numBytes As Integer, ByVal data() As Byte) As Status

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Validate address
        If (address < 0 Or address >= 8 * 1024) Then
            Return Status.invalidAddress
        End If

        'Validate length
        If (numBytes < 1 Or numBytes > 32) Then
            Return Status.invalidLength
        End If

        'Validate number of bytes to write runs us off the end of EEPROM, flag it
        If (numBytes > (8 * 1024) - address) Then
            Return Status.invalidLength
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = UsbCommand.eepromBlockWrite
        buf(1) = (address >> 8) And &HFF
        buf(2) = address And &HFF
        buf(3) = numBytes

        Dim i As Integer
        For i = 0 To numBytes - 1
            buf(i + 4) = data(i)
        Next

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        If (rxBuffer(0) <> UsbCommand.eepromBlockWriteResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure

    End Function

    Public Function readEepromPacketPublic(ByVal address As Integer, ByVal numBytes As Integer, ByRef data() As Byte) As Status

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Validate address
        If (address < 0 Or address >= 8 * 1024) Then
            Return Status.invalidAddress
        End If

        'Validate length
        If (numBytes < 1 Or numBytes > 32) Then
            Return Status.invalidLength
        End If

        'Validate number of bytes to write runs us off the end of EEPROM, flag it
        If (numBytes > (8 * 1024) - address) Then
            Return Status.invalidLength
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = UsbCommand.eepromBlockRead
        buf(1) = (address >> 8) And &HFF
        buf(2) = address And &HFF
        buf(3) = numBytes

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        If (rxBuffer(0) <> UsbCommand.eepromBlockReadResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Copy data into buffer
        Dim i As Integer
        For i = 0 To numBytes - 1
            data(i) = rxBuffer(i + 2)
        Next

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure

    End Function
    Public Function readEepromPacket(ByVal address As Integer, ByVal numBytes As Integer, ByRef data() As Byte) As Status

        'Check driver
        If (_IsBridgeAttached = False) Then
            Return Status.deviceNotFound
        End If

        'Validate address
        If (address < 0 Or address >= 8 * 1024) Then
            Return Status.invalidAddress
        End If

        'Validate length
        If (numBytes < 1 Or numBytes > 32) Then
            Return Status.invalidLength
        End If

        'Validate number of bytes to write runs us off the end of EEPROM, flag it
        If (numBytes > (8 * 1024) - address) Then
            Return Status.invalidLength
        End If

        'Before we do anything else, make sure no one else is talking to USB HID
        While (mUsbHidInUse = True)
            System.Threading.Thread.Sleep(0)
        End While
        mUsbHidInUse = True

        Dim buf(64) As Byte

        'Load up buffer
        buf(0) = UsbCommand.eepromBlockRead
        buf(1) = (address >> 8) And &HFF
        buf(2) = address And &HFF
        buf(3) = numBytes

        'Send to driver
        If (sendAndReceive(buf) = False) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Validate response
        If (rxBuffer(0) <> UsbCommand.eepromBlockReadResponse) Then
            mUsbHidInUse = False
            Return Status.driverError
        End If

        'Copy data into buffer
        Dim i As Integer
        For i = 0 To numBytes - 1
            data(i) = rxBuffer(i + 2)
        Next

        If (rxBuffer(1) = 0) Then
            mUsbHidInUse = False
            Return Status.success
        End If

        mUsbHidInUse = False
        Return Status.generalFailure

    End Function

    'Private
    Public Function sendAndReceive(ByVal inBuf() As Byte) As Boolean

        mRetries = 0

        'Debug
        Dim str As String = ""
        str += Environment.NewLine
        str += "inBuf[0-63]:" + Environment.NewLine
        Dim i As Integer
        Dim tempString As String = ""
        For i = 0 To 63
            If (inBuf(i) <= &HF) Then
                tempString += "0"
            End If
            tempString += Hex(inBuf(i))
            tempString += " "
        Next
        str += tempString
        str += Environment.NewLine

        'Clear rxBuffer
        For i = 0 To 63
            rxBuffer(i) = 0
        Next

        dataRxDone = False  'Clear the Done Flag before launching the extra thread

        'Send buffer to HID driver
        mUsbHid.ExchangeInputAndOutputReports(inBuf, 64, rxBuffer)

        'Test for response
        While (dataRxDone = False)
            mRetries += 1
            System.Threading.Thread.Sleep(0)
        End While

        'It's back
        dataRxDone = False

        'Debug
        str += Environment.NewLine
        str += "rxBuffer[0-63]:" + Environment.NewLine
        tempString = ""
        For i = 0 To 63
            If (rxBuffer(i) <= &HF) Then
                tempString += "0"
            End If
            tempString += Hex(rxBuffer(i))
            tempString += " "
        Next
        str += tempString
        str += Environment.NewLine

        'MsgBox(str)

        If (mDebugCapture = True) Then
            mDebugFile.WriteLine(str)
        End If

        Return True

    End Function

    'Private
    ' Returns true if FW version is compatable with theVersion Required By GUI
    Private Function isFWVersionOk(ByVal CurrentVersion As String, ByVal VersionRequiredByGUI As String) As Boolean
        Try
            If VersionRequiredByGUI.Split(".")(0) = CurrentVersion.Split(".")(0) _
                And VersionRequiredByGUI.Split(".")(1) = CurrentVersion.Split(".")(1) _
                And VersionRequiredByGUI.Split(".")(2) <= CurrentVersion.Split(".")(2) _
            Then
                Return True
            Else
                Return False
            End If
        Catch
            Return False
        End Try
    End Function

    'return false if we fail to update the bridge
    'this function reprograms the xusb module (SAA)
    Private Function UpdateBridgeImage(ByVal ImageData As Byte()) As Boolean

        'program the device
        Dim _PutImageResponseOk As xUSBProtocol.EEPROM_Status
        Dim _ValidateImageResponseOk As xUSBProtocol.EEPROM_Status
        Dim msgResponse As Microsoft.VisualBasic.MsgBoxResult

        Do 'Validate image loop

            Do 'Write image loop
                _PutImageResponseOk = putImageOnEEPROM(ImageData)
                If _PutImageResponseOk <> xUSBProtocol.EEPROM_Status.success Then
                    msgResponse = MsgBox("There was an Error writing to the EEPROM on your USB Bridge" & _
                            ControlChars.CrLf & "Would you like to try again?", MsgBoxStyle.Critical + MsgBoxStyle.YesNo _
                            , "ERROR - Do Not Unplug USB Bridge!")
                    If msgResponse = MsgBoxResult.No Then
                        _DoesUserWantToUpdateUSBBridge = False
                        Return False
                    End If
                End If
            Loop Until (_PutImageResponseOk = xUSBProtocol.EEPROM_Status.success)

            'Validate
            _ValidateImageResponseOk = Me.validateImageOnEEPROM(ImageData)
            If _ValidateImageResponseOk <> xUSBProtocol.EEPROM_Status.success Then
                msgResponse = MsgBox("There was an Error validating to the EEPROM on your USB Bridge" & _
                        ControlChars.CrLf & "Would you like to try again?", MsgBoxStyle.Critical + MsgBoxStyle.YesNo _
                        , "ERROR - Do Not Unplug USB Bridge!")
                If msgResponse = MsgBoxResult.No Then
                    _DoesUserWantToUpdateUSBBridge = False
                    Return False
                End If
            End If

        Loop Until (_ValidateImageResponseOk = xUSBProtocol.EEPROM_Status.success)
        Return True

    End Function


    Private Sub FirmWareCheck(ByVal Image As Byte(), ByVal VersionRequiredByGUI As String)

        'If Bridge is connected
        If Me.targetAttached Then
            'set IsBridgeAttached property to true
            Me._IsBridgeAttached = True

            'Get Bridge Version
            If Me.version(Me._ProtocolVersion) = Status.success Then
                'Now _ProtocolVersion contains Bridge Version 
                'Find out if current _ProtocolVersion compatable with Version Required By GUI
                Me._IsVersionOk = isFWVersionOk(_ProtocolVersion, VersionRequiredByGUI)

                'If Versions are not compatable
                If Not _IsVersionOk Then
                    '        'promt user to update FW
                    '        'Dim msgResponse As Microsoft.VisualBasic.MsgBoxResult

                    If _FirmwareCheckWindow.Visible = True Then
                        'don't do anything but bring it to the front
                        _FirmwareCheckWindow.Focus()

                        'now exit
                        Exit Sub

                    Else
                        _FirmwareCheckWindow.lblMessage.Text = "The attached USB Interface Adapter is currently using a firmware version " & _ProtocolVersion & " " & ControlChars.CrLf & _
                                       "that is not compatible with this software (which needs ver " & VersionRequiredByGUI & ").  Would you like to " & ControlChars.CrLf & _
                                       "allow this software to install the correct version of firmware now?"

                        Dim result As System.Windows.Forms.DialogResult

                        result = _FirmwareCheckWindow.ShowDialog()

                        If result = DialogResult.OK Then

                            Dim msgResponse As Microsoft.VisualBasic.MsgBoxResult
                            'additional warning about reprogramming
                            msgResponse = MsgBox("Warning: You are about to attempt reprogram the USB Interface Adapter! " & ControlChars.CrLf & _
                                            "Please make sure that you do not have any other software running that also" & ControlChars.CrLf & _
                                            "uses the USB Interface Adpater.  Attempting to reprogram the adapter while" & ControlChars.CrLf & _
                                            "it is being used by another program could cause the operation to fail!" & ControlChars.CrLf & ControlChars.CrLf & _
                                            "If the operation fails the USB Interface Adapter firmware will be corrupted!" & ControlChars.CrLf & _
                                            "To avoid this, take a moment to shut down any other applicaitons that are " & ControlChars.CrLf & _
                                            "using the USB Interface Adapter." & ControlChars.CrLf & ControlChars.CrLf & _
                                            "If for any reason the operation fails, DO NOT uplug the USB Interface Adapter!" & ControlChars.CrLf & _
                                            "Reattaching the adapter will CAUSE THE CORRUPTED FIRWARE TO BE LOADED!" & ControlChars.CrLf & _
                                            "The best way to recover a failed operation is to RESTART THIS SOFTWARE" & ControlChars.CrLf & _
                                            "and then attempt to program again.", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo, "Proceed with Firmware Update?")

                            If msgResponse = MsgBoxResult.Yes Then


                                'reprogram

                                'write the image
                                'valide the image
                                'if not valid while back to prompt to write again, yes reprog
                                'prompt for reset

                                If Not Me.UpdateBridgeImage(Image) Then
                                    Exit Sub
                                End If

                                'prompt for reset
                                MsgBox("In order for firmware changes to become active you must reset the USB Interface Adapter now!" & ControlChars.CrLf & ControlChars.CrLf & _
                                       "To cause the new firmware to be active:" & ControlChars.CrLf & _
                                       "-First unplug the USB Interface Adapter from the PC." & ControlChars.CrLf & _
                                       "-Finally plug it back in to the PC." _
                                       , MsgBoxStyle.Exclamation, _
                                       "Reset USB Interface Adapter Now!")
                            Else
                                'if user do not want to update FW - exit sub.
                                'Do nothing -  all properties have been set. 
                                _DoesUserWantToUpdateUSBBridge = False
                                Exit Sub
                            End If 'end of msgResponse = MsgBoxResult.Yes Then

                        End If 'end of If result = DialogResult.OK Then

                    End If ' end of _FirmwareCheckWindow.Visible = True Then
                Else
                    'If Versions are compatable, exit sub.  All properties have been set. 
                    Exit Sub
                End If

            Else
                'if version is not accessible set _IsBridgeAttached = False and exit sub
                Me._IsBridgeAttached = False
                Exit Sub
            End If


        Else
            'If Bridge is not connected set IsBridgeAttached property to false and exit sub
            Me._IsBridgeAttached = False
        End If

    End Sub

    Public Class TLC5924_DualFrame_Thread
        Public ENA_Data_A As Byte()
        Public ENA_Data_B As Byte()
        Public DC_Data_A As Byte()
        Public DC_Data_B As Byte()

        Public xUSBhandle As xUSBProtocol

        Public Sub New(ByRef HandleToXusb As xUSBProtocol)
            Me.xUSBhandle = HandleToXusb
        End Sub
        Public Sub TLC5924_Dual_Frame_Write()

            'Check driver
            If (xUSBhandle._IsBridgeAttached = False) Then
                'Return xUSBProtocol.Status.deviceNotFound
            End If

            'Before we do anything else, make sure no one else is talking to USB HID
            While (xUSBhandle.mUsbHidInUse = True)
                System.Threading.Thread.Sleep(0)
            End While
            xUSBhandle.mUsbHidInUse = True

            Dim buf(64) As Byte

            'Load up buffer

            buf(0) = xUSBProtocol.UsbCommand_TLC5924.TLC5924_WriteDualFrameEnableAndDotCorrection
            Dim i As Integer
            For i = 1 To 14
                buf(i) = Me.DC_Data_A(i - 1) 'fill the dotcorrection bytes
            Next i

            'fill the Enable data
            buf(15) = Me.ENA_Data_A(0)
            buf(16) = Me.ENA_Data_A(1)

            'bank B
            For i = 1 To 14
                buf(16 + i) = Me.DC_Data_B(i - 1) 'fill the dotcorrection bytes
            Next i

            'fill the Enable data
            buf(31) = Me.ENA_Data_B(0)
            buf(32) = Me.ENA_Data_B(1)




            'Send to driver
            If (xUSBhandle.sendAndReceive(buf) = False) Then
                xUSBhandle.mUsbHidInUse = False
                'Return Status.driverError
            End If

            'validate that the driver worked
            If (rxBuffer(0) <> UsbCommand_TLC5924.TLC5924_WriteDualFrameEnableAndDotCorrectionResponse) Then
                xUSBhandle.mUsbHidInUse = False
                'Return Status.driverError
            End If

            If (rxBuffer(1) = 0) Then
                xUSBhandle.mUsbHidInUse = False
                'Return Status.success
            End If

            xUSBhandle.mUsbHidInUse = False
            'Return Status.generalFailure


        End Sub

    End Class

End Class


