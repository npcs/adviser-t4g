﻿using System.Collections.Generic;
using Microsoft.Practices.Composite.Wpf.Events;
using Adviser.Client.DataModel;

namespace Adviser.Client.Core.Events
{
    public class MeasurementProgressEvent : CompositeWpfEvent<MeasurementProgressEventArgs>
    {
    }
}
