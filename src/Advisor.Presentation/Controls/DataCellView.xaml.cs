﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Advisor.Presentation.Controls
{
    /// <summary>
    /// Interaction logic for DataCellView.xaml
    /// </summary>
    public partial class DataCellView : UserControl
    {
        public static DependencyProperty ValueProperty;

        static DataCellView()
        {
            ValueProperty = DependencyProperty.Register("Value", typeof(string), typeof(DataCellView),
                new UIPropertyMetadata(String.Empty, new PropertyChangedCallback(valueChangedCallback)));
        }

        private static void valueChangedCallback(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            DataCellView cellView = (DataCellView)sender;
            cellView.txtValue.Text = (string)e.NewValue;
        }

        public DataCellView()
        {
            InitializeComponent();
        }

        public string Value
        {
            get { return (string)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

    }
}
