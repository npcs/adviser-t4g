﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Common.DataModel
{
    public enum HealthRiskLevel
    {
        Normal,
        Low,
        Medium,
        High,
        VeryHigh
    }
}
