﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Events;

using Adviser.Common;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Presentation;

namespace Advisor.Modules.Navigation
{
    public class MeasurementNavigationState : NavigationState
    {
        private AppointmentSession _session;
        private IEventAggregator _eventAggregator;

        public MeasurementNavigationState(AppointmentSession session, IEventAggregator eventAggregator) 
        {
            _eventAggregator = eventAggregator;
            _session = session;
            _eventAggregator.GetEvent<MeasurementCompletedEvent>().Subscribe(OnMeasurementCompletedEvent);
            _eventAggregator.GetEvent<ResultsReceivedEvent>().Subscribe(OnResultsReceived);
        }

        public override void TransitionState(string actionName)
        {
            string toState = _subscriptions[actionName];
            if (toState == NavigationEventNames.Results)
            {
                if (!_session.HasResults)
                {
                    OpenDialogGetResults();
                    return;
                }
            }

            base.TransitionState(actionName);
        }

        protected override void SetControls()
        {
            ControlMap[NavigationControls.Measurement].IsSelected = true;

            ControlMap[NavigationControls.Patients].IsEnabled = true;
            ControlMap[NavigationControls.Visits].IsEnabled = true;
            ControlMap[NavigationControls.Measurement].IsEnabled = false;

            if (_session.HasDiagnostics)
                ControlMap[NavigationControls.Results].IsEnabled = true;
            else
                ControlMap[NavigationControls.Results].IsEnabled = false;

            if (_session.HasResults)
                ControlMap[NavigationControls.Print].IsEnabled = true;
            else
                ControlMap[NavigationControls.Print].IsEnabled = false;
        }

        private void OnMeasurementCompletedEvent(MeasurementEventArgs e)
        {
            if (e.MeasurementData != null)
            {
                ControlMap[NavigationControls.Measurement].IsEnabled = false;
                ControlMap[NavigationControls.Results].IsEnabled = true;
            }
        }

        private void OnResultsReceived(ResultsReceivedEventArgs e)
        {
            if (_session.Visit != null && e.IsSuccess)
            {
                if (e.VisitData != null &&_session.Visit.ID == e.VisitData.ID)
                    ControlMap[NavigationControls.Results].IsEnabled = true;
            }
        }

        private void OpenDialogGetResults()
        {
            NavigationEventArgs args = new NavigationEventArgs();
            args.EventName = NavigationEventNames.GetServerResults;
            _eventAggregator.GetEvent<NavigationEvent>().Publish(args);
        }
    }
}
