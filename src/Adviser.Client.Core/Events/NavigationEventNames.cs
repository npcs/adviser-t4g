﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Client.Core.Events
{
    public static class NavigationEventNames
    {
        public static string Patients = "Patients";
        public static string Visits = "Visits";
        public static string Measurement = "Diagnostics";
        public static string GetServerResults = "GetServerResults";
        public static string Results = "Results";
        public static string Print = "Print";
        public static string Readout = "Readout";
    }
}
