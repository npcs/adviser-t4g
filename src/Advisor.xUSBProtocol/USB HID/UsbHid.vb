Option Strict On
Option Explicit On 

Imports System.Runtime.InteropServices
Imports System.Threading

Public Class UsbHid

    'Project: usbhidio_vbdotnet
    'Version: 2.2
    'Date: 7/13/05
    '
    'Purpose: 
    'Demonstrates USB communications with a generic HID-class device

    'Description:
    'Finds an attached device that matches the vendor and product IDs in the form's 
    'text boxes.
    'Retrieves the device's capabilities.
    'Sends and requests HID reports.

    'Uses RegisterDeviceNotification() and WM_DEVICE_CHANGE messages
    'to detect when a device is attached or removed.
    'RegisterDeviceNotification doesn't work under Windows 98 (not sure why).

    'A list box displays the data sent and received,
    'along with error and status messages.

    'Combo boxes select data to send, and 1-time or timed, periodic transfers.

    'You can change the size of the host's Input report buffer and request to use control
    'transfers only to exchange Input and Output reports.

    'To view additional debugging messages, in the Visual Studio development environment,
    'select the Debug build (Build > Configuration Manager > Active Solution Configuration)
    'and view the Output window (View > Other Windows > Output)

    'The application uses a Delegate and the BeginInvoke and EndInvoke methods to read
    'from the device asynchronously. 

    'If you want to only receive data or only send data, comment out the unwanted code 
    '(In ExchangeInputAndOutputReports or ExchangeFeatureReports, comment out
    'the "Success = " line and the "If Success" block that follows it).

    'This project includes the following modules:
    'frmMain.vb - routines specific to the form.
    'Hid.vb - routines specific to HID communications.
    'DeviceManagement.vb - routines for obtaining a handle to a device from its GUID
    'and receiving device notifications. This routines are not specific to HIDs.
    'Debugging.vb - contains a routine for displaying API error messages.

    'HidDeclarations.vb - Declarations for API functions used by Hid.vb.
    'FileIODeclarations.vb - Declarations for file-related API functions.
    'DeviceManagementDeclarations.vb - Declarations for API functions used by DeviceManagement.vb.
    'DebuggingDeclarations.vb - Declarations for API functions used by Debugging.vb.

    'Companion device firmware for several device CPUs is available from www.Lvr.com/hidpage.htm.
    'You can use any generic HID (not a system mouse or keyboard) that sends and receives reports.

    'New in version 2.2:
    'The application obtains separate handles for device information/Feature reports,
    'Input reports, and Output reports. This enables getting information about
    'mice and keyboards.
    'The application detects if the device is a mouse or keyboard
    'and warns that Windows 2000/XP will not allow exchanging Input or Output reports.
    'The list box's contents are trimmed periodically. 

    'For more information about HIDs and USB, and additional example device firmware to use
    'with this application, visit Lakeview Research at http://www.Lvr.com .

    'Send comments, bug reports, etc. to jan@Lvr.com .

    'This application has been tested under Windows 98SE, Windows 2000, and Windows XP.

    Dim DeviceNotificationHandle As IntPtr
    Dim ExclusiveAccess As Boolean
    Dim HIDHandle As Integer
    Dim HIDUsage As String
    Dim MyDeviceDetected As Boolean
    Dim MyDevicePathName As String
    Dim MyHID As New Hid
    Dim MyUsbEventHandler As New UsbEventHandler
    Dim ReadHandle As Integer
    Dim WriteHandle As Integer
    dim TransportDeviceDetected as boolean

    'Used only in viewing results of API calls in debug.write statements:
    Dim MyDebugging As New Debugging
    Dim MyDeviceManagement As New DeviceManagement

    Public Sub New()

        TransportDeviceDetected = False

        'Instantiate my Event Handler Window
        MyUsbEventHandler = New UsbEventHandler(Me)

        'Instantiate the HID
        MyHID = New Hid

        'Search for a specific device.
        Try
            Call FindTheHid()

        Catch ex As Exception
            Call HandleException("UsbHid ctor", ex)
        End Try

    End Sub
    Protected Overrides Sub Finalize()

        Try
            'Close open handles to the device.

            If (HIDHandle <> 0) Then
                CloseHandle(HIDHandle)
                Debug.WriteLine(MyDebugging.ResultOfAPICall("CloseHandle (HIDHandle)"))
            End If

            If (ReadHandle <> 0) Then
                CloseHandle(ReadHandle)
                Debug.WriteLine(MyDebugging.ResultOfAPICall("CloseHandle (ReadHandle)"))
            End If

            If (WriteHandle <> 0) Then
                CloseHandle(WriteHandle)
                Debug.WriteLine(MyDebugging.ResultOfAPICall("CloseHandle (WriteHandle)"))
            End If

            'Stop receiving notifications.

            Call MyDeviceManagement.StopReceivingDeviceNotifications(DeviceNotificationHandle)

            ' Place final cleanup tasks here.
            MyBase.Finalize()

        Catch ex As Exception
            Call HandleException("UsbHid dtor", ex)
        End Try

    End Sub

    'Define a class of delegates that point to the Hid.DeviceReport.Read function.
    'The delegate has the same parameters as Hid.DeviceReport.Read.
    'Used for asynchronous reads from the device.
    Private Delegate Sub ReadInputReportDelegate _
        (ByVal readHandle As Integer, _
        ByVal hidHandle As Integer, _
        ByVal writeHandle As Integer, _
        ByRef myDeviceDetected As Boolean, _
        ByRef readBuffer() As Byte, _
        ByRef success As Boolean)

    Friend Sub OnDeviceChange(ByVal m As Message)

        'Purpose    : Called when a WM_DEVICECHANGE message has arrived,
        '           : indicating that a device has been attached or removed.

        'Accepts    : m - a message with information about the device

        Debug.WriteLine("WM_DEVICECHANGE time: " & Now.ToLongTimeString)

        Try
            If (m.WParam.ToInt32 = DBT_DEVICEARRIVAL) Then

                'If WParam contains DBT_DEVICEARRIVAL, a device has been attached.
                Debug.WriteLine("A device has been attached. time: " & Now.ToLongTimeString)

                'Find out if it's the device we're communicating with.
                If MyDeviceManagement.DeviceNameMatch(m, MyDevicePathName) Then
                    Debug.WriteLine("My device attached. time: " & Now.ToLongTimeString)
                End If

                TransportDeviceDetected = True

            ElseIf (m.WParam.ToInt32 = DBT_DEVICEREMOVECOMPLETE) Then

                'If WParam contains DBT_DEVICEREMOVAL, a device has been removed.
                Debug.WriteLine("A device has been removed. time: " & Now.ToLongTimeString)

                'Find out if it's the device we're communicating with.
                If MyDeviceManagement.DeviceNameMatch(m, MyDevicePathName) Then

                    Debug.WriteLine("My device removed. time: " & Now.ToLongTimeString)

                    'Set MyDeviceDetected False so on the next data-transfer attempt,
                    'FindTheHid() will be called to look for the device 
                    'and get a new handle.
                    MyDeviceDetected = False

                    TransportDeviceDetected = False

                End If
            End If

        Catch ex As Exception
            Call HandleException("OnDeviceChange()", ex)
        End Try
    End Sub

    Public Function FindTheHid() As Boolean

        'Purpose    : Uses a series of API calls to locate a HID-class device
        '           ; by its Vendor ID and Product ID.

        'Returns    : True if the device is detected, False if not detected.

        'Dim Count As Short
        Dim DeviceFound As Boolean
        Dim DevicePathName(127) As String
        Dim GUIDString As String
        Dim HidGuid As System.Guid
        Dim LastDevice As Boolean
        Dim MemberIndex As Integer
        Dim MyProductID As Short
        Dim MyVendorID As Short
        Dim Result As Boolean
        Dim Security As SECURITY_ATTRIBUTES
        Dim Success As Boolean

        Try

            HidGuid = Guid.Empty
            LastDevice = False
            MyDeviceDetected = False

            'Values for the SECURITY_ATTRIBUTES structure:
            Security.lpSecurityDescriptor = 0
            Security.bInheritHandle = CInt(True)
            Security.nLength = Len(Security)

            'Assign the device's Vendor ID and Product ID
            MyVendorID = &H451
            MyProductID = &H5F00

            '***
            'API function: 'HidD_GetHidGuid

            'Purpose: Retrieves the interface class GUID for the HID class.

            'Accepts: 'A System.Guid object for storing the GUID.
            '***

            HidD_GetHidGuid(HidGuid)
            Debug.WriteLine(MyDebugging.ResultOfAPICall("GetHidGuid"))

            'Display the GUID.
            GUIDString = HidGuid.ToString
            Debug.WriteLine("  GUID for system HIDs: " & GUIDString)

            'Fill an array with the device path names of all attached HIDs.
            DeviceFound = MyDeviceManagement.FindDeviceFromGuid _
                (HidGuid, _
                DevicePathName)

            'If there is at least one HID, attempt to read the Vendor ID and Product ID
            'of each device until there is a match or all devices have been examined.

            If DeviceFound = True Then
                MemberIndex = 0
                Do
                    '***
                    'API function:
                    'CreateFile

                    'Purpose:
                    'Retrieves a handle to a device.

                    'Accepts:
                    'A device path name returned by SetupDiGetDeviceInterfaceDetail
                    'The type of access requested (read/write).
                    'FILE_SHARE attributes to allow other processes to access the device while this handle is open.
                    'A Security structure. Using Null for this may cause problems under Windows XP.
                    'A creation disposition value. Use OPEN_EXISTING for devices.
                    'Flags and attributes for files. Not used for devices.
                    'Handle to a template file. Not used.

                    'Returns: a handle without read or write access.
                    'This enables obtaining information about all HIDs, even system
                    'keyboards and mice. 
                    'Separate handles are used for reading and writing.
                    '***

                    HIDHandle = CreateFile _
                        (DevicePathName(MemberIndex), _
                        0, _
                        FILE_SHARE_READ Or FILE_SHARE_WRITE, _
                        Security, _
                        OPEN_EXISTING, _
                        0, _
                        0)

                    Debug.WriteLine(MyDebugging.ResultOfAPICall("CreateFile"))
                    Debug.WriteLine("  Returned handle: " & Hex(HIDHandle) & "h")

                    If (HIDHandle <> INVALID_HANDLE_VALUE) Then

                        'The returned handle is valid, 
                        'so find out if this is the device we're looking for.

                        'Set the Size property of DeviceAttributes to the number of bytes in the structure.
                        MyHID.DeviceAttributes.Size = Marshal.SizeOf(MyHID.DeviceAttributes)

                        '***
                        'API function:
                        'HidD_GetAttributes

                        'Purpose:
                        'Retrieves a HIDD_ATTRIBUTES structure containing the Vendor ID, 
                        'Product ID, and Product Version Number for a device.

                        'Accepts:
                        'A handle returned by CreateFile.
                        'A pointer to receive a HIDD_ATTRIBUTES structure.

                        'Returns:
                        'True on success, False on failure.
                        '***

                        Result = HidD_GetAttributes(HIDHandle, MyHID.DeviceAttributes)

                        Debug.WriteLine(MyDebugging.ResultOfAPICall("HidD_GetAttributes"))

                        If Result Then

                            Debug.WriteLine("  HIDD_ATTRIBUTES structure filled without error.")

                            Debug.WriteLine("  Structure size: " & MyHID.DeviceAttributes.Size)

                            Debug.WriteLine("  Vendor ID: " & Hex(MyHID.DeviceAttributes.VendorID))
                            Debug.WriteLine("  Product ID: " & Hex(MyHID.DeviceAttributes.ProductID))
                            Debug.WriteLine("  Version Number: " & Hex(MyHID.DeviceAttributes.VersionNumber))

                            'Find out if the device matches the one we're looking for.
                            If (MyHID.DeviceAttributes.VendorID = MyVendorID) And _
                                (MyHID.DeviceAttributes.ProductID = MyProductID) Then

                                'It's the desired device.
                                Debug.WriteLine("  My device detected")

                                MyDeviceDetected = True

                                'Save the DevicePathName so OnDeviceChange() knows which name is my device.
                                MyDevicePathName = DevicePathName(MemberIndex)
                            Else

                                'It's not a match, so close the handle.
                                MyDeviceDetected = False

                                Result = CloseHandle(HIDHandle)

                                Debug.WriteLine(MyDebugging.ResultOfAPICall("CloseHandle"))
                            End If
                        Else
                            'There was a problem in retrieving the information. 
                            Debug.WriteLine("  Error in filling HIDD_ATTRIBUTES structure.")
                            MyDeviceDetected = False
                            Result = CloseHandle(HIDHandle)
                        End If

                    End If


                    'Keep looking until we find the device or there are no more left to examine.
                    MemberIndex = MemberIndex + 1

                Loop Until ((MyDeviceDetected = True) Or _
                    (MemberIndex = UBound(DevicePathName) + 1))
            End If

            If MyDeviceDetected Then

                'The device was detected.
                'Register to receive notifications if the device is removed or attached.
                Success = MyDeviceManagement.RegisterForDeviceNotifications _
                    (MyDevicePathName, _
                    MyUsbEventHandler.Handle, _
                    HidGuid, _
                    DeviceNotificationHandle)

                Debug.WriteLine("RegisterForDeviceNotifications = " & Success)

                'Learn the capabilities of the device.
                MyHID.Capabilities = MyHID.GetDeviceCapabilities _
                    (HIDHandle)

                If Success Then

                    'Find out if the device is a system mouse or keyboard.
                    HIDUsage = MyHID.GetHIDUsage(MyHID.Capabilities)

                    'Get and display the Input report buffer size.
                    GetInputReportBufferSize()

                    'Get handles to use in requesting Input and Output reports.

                    ReadHandle = CreateFile _
                        (MyDevicePathName, _
                        GENERIC_READ, _
                        FILE_SHARE_READ Or FILE_SHARE_WRITE, _
                        Security, _
                        OPEN_EXISTING, _
                        FILE_FLAG_OVERLAPPED, _
                        0)

                    Debug.WriteLine(MyDebugging.ResultOfAPICall("CreateFile, ReadHandle"))
                    Debug.WriteLine("  Returned handle: " & Hex(ReadHandle) & "h")

                    If (ReadHandle = INVALID_HANDLE_VALUE) Then

                        ExclusiveAccess = True

                    Else

                        WriteHandle = CreateFile _
                             (MyDevicePathName, _
                             GENERIC_WRITE, _
                             FILE_SHARE_READ Or FILE_SHARE_WRITE, _
                             Security, _
                             OPEN_EXISTING, _
                             0, _
                             0)

                        Debug.WriteLine(MyDebugging.ResultOfAPICall("CreateFile, WriteHandle"))
                        Debug.WriteLine("  Returned handle: " & Hex(WriteHandle) & "h")

                        '(optional)
                        'Flush any waiting reports in the input buffer.
                        MyHID.FlushQueue(ReadHandle)

                        TransportDeviceDetected = True

                    End If

                End If

            Else

                'The device wasn't detected.
                Debug.WriteLine(" Device not found.")
            End If
            Return MyDeviceDetected

        Catch ex As Exception
            Call HandleException("FindTheHid()", ex)
        End Try
    End Function

    Public Function deviceIsAttached() As Boolean
        If (TransportDeviceDetected = True) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub ExchangeFeatureReports(ByVal buf() As Byte, ByVal length As Integer)

        'Purpose    : Sends a Feature report, then retrieves one.
        '           : Assumes report ID = 0 for both reports.

        Dim ByteValue As String
        Dim Count As Integer
        Dim InFeatureReportBuffer() As Byte
        Dim OutFeatureReportBuffer() As Byte
        Dim Success As Boolean

        Try
            Dim myInFeatureReport As New Hid.InFeatureReport
            Dim myOutFeatureReport As New Hid.OutFeatureReport

            If (MyHID.Capabilities.FeatureReportByteLength > 0) Then

                'The HID has a Feature report.

                'Set the size of the Feature report buffer. 
                'Subtract 1 from the value in the Capabilities structure because 
                'the array begins at index 0.

                ReDim OutFeatureReportBuffer(MyHID.Capabilities.FeatureReportByteLength - 1)

                'Store the data buffer:
                Dim i As Integer
                For i = 0 To length
                    OutFeatureReportBuffer(i) = buf(i)
                Next

                'Write a report to the device
                Success = myOutFeatureReport.Write(OutFeatureReportBuffer, HIDHandle)

                If Success Then

                    Debug.WriteLine(" Feature Report ID: " & OutFeatureReportBuffer(0))
                    Debug.WriteLine(" Feature Report Data:")

                    'txtBytesReceived.Text = ""
                    For Count = 1 To UBound(OutFeatureReportBuffer)

                        'Add a leading zero to values from 0 to F.
                        If Len(Hex(OutFeatureReportBuffer(Count))) < 2 Then
                            ByteValue = "0" & Hex(OutFeatureReportBuffer(Count))
                        Else
                            ByteValue = Hex(OutFeatureReportBuffer(Count))
                        End If

                        'Debug.WriteLine(" " & ByteValue)

                    Next Count
                End If

                'Read a report from the device.

                'Set the size of the Feature report buffer. 
                'Subtract 1 from the value in the Capabilities structure because 
                'the array begins at index 0.

                If (MyHID.Capabilities.FeatureReportByteLength > 0) Then
                    ReDim InFeatureReportBuffer(MyHID.Capabilities.FeatureReportByteLength - 1)
                End If

                'Read a report.
                myInFeatureReport.Read _
                    (ReadHandle, _
                    HIDHandle, _
                    WriteHandle, _
                    MyDeviceDetected, _
                    InFeatureReportBuffer, _
                    Success)

                If Success Then
                    Debug.WriteLine(" Feature Report ID: " & InFeatureReportBuffer(0))
                    Debug.WriteLine(" Feature Report Data:")

                    For Count = 1 To UBound(InFeatureReportBuffer)

                        'Add a leading zero to values from 0 to F.
                        If Len(Hex(InFeatureReportBuffer(Count))) < 2 Then

                            ByteValue = "0" & Hex(InFeatureReportBuffer(Count))
                        Else
                            ByteValue = Hex(InFeatureReportBuffer(Count))
                        End If

                        Debug.WriteLine(" " & ByteValue)

                    Next Count

                End If

            End If


        Catch ex As Exception
            Call HandleException("ExchangeFeatureReports()", ex)
        End Try

    End Sub

    Public Sub ExchangeInputAndOutputReports(ByVal buf() As Byte, ByVal length As Integer, _
        ByRef InputReportBuffer() As Byte, Optional ByVal inLength As Integer = 0)

        'Purpose    : Sends an Outputreport, then retrieves an Input report.
        '           : Assumes report ID = 0 for both reports.

        'Dim ByteValue As String
        'Dim Count As Integer
        Dim OutputReportBuffer() As Byte
        Dim Success As Boolean

        Try

            Success = False

            'Don't attempt to exchange reports if valid handles aren't available
            '(as for a mouse or keyboard under Windows 2000/XP.)

            If ((ReadHandle <> INVALID_HANDLE_VALUE) And (WriteHandle <> INVALID_HANDLE_VALUE)) Then

                'Don't attempt to send an Output report if the HID has no Output report.

                If (MyHID.Capabilities.OutputReportByteLength > 0) Then

                    'Set the size of the Output report buffer. 
                    'Subtract 1 from the value in the Capabilities structure because 
                    'the array begins at index 0.

                    'ReDim OutputReportBuffer(MyHID.Capabilities.OutputReportByteLength - 1)
                    ReDim OutputReportBuffer(64) 'MDS

                    'Report ID
                    OutputReportBuffer(0) = 0

                    Dim i As Integer
                    For i = 1 To length
                        OutputReportBuffer(i) = buf(i - 1)
                    Next

                    'If the HID has an interrupt OUT endpoint, use an interrupt transfer
                    'to send the report. Otherwise use a control transfer.

                    Dim myOutputReport As New Hid.OutputReport
                    Success = myOutputReport.Write(OutputReportBuffer, WriteHandle)


                End If

                'Read an Input report.

                Success = False

                'Don't attempt to send an Input report if the HID has no Input report.
                '(The HID spec requires all HIDs to have an interrupt IN endpoint,
                'which suggests that all HIDs must support Input reports.)

                If (MyHID.Capabilities.InputReportByteLength > 0) Then

                    'Set the size of the Input report buffer. 
                    'Subtract 1 from the value in the Capabilities structure because 
                    'the array begins at index 0.

                    ReDim InputReportBuffer(MyHID.Capabilities.InputReportByteLength - 1)
                    'ReDim InputReportBuffer(64)

                    'Read a report using interrupt transfers.                
                    'To enable reading a report without blocking the main thread, this
                    'application uses an asynchronous delegate.

                    Dim ar As IAsyncResult
                    Dim myInputReport As New Hid.InputReport

                    'Define a delegate for the Read method of myInputReport.
                    Dim MyReadInputReportDelegate As _
                        New ReadInputReportDelegate(AddressOf myInputReport.Read)

                    'The BeginInvoke method calls myInputReport.Read to attempt to read a report.
                    'The method has the same parameters as the Read function,
                    'plus two additional parameters:
                    'GetInputReportData is the callback procedure that executes when the Read function returns.
                    'MyReadInputReportDelegate is the asynchronous delegate object.
                    'The last parameter can optionally be an object passed to the callback.
                    ar = MyReadInputReportDelegate.BeginInvoke _
                        (ReadHandle, _
                         HIDHandle, _
                         WriteHandle, _
                         MyDeviceDetected, _
                         InputReportBuffer, _
                         Success, _
                         New AsyncCallback(AddressOf GetInputReportData), _
                         MyReadInputReportDelegate)
                End If

            End If

        Catch ex As Exception
            Call HandleException("ExchangeInputAndOutputReports", ex)
        End Try

    End Sub

    Private Sub GetInputReportBufferSize()

        'Purpose    : Finds and displays the number of Input buffers
        '           : (the number of Input reports the host will store). 

        Dim NumberOfInputBuffers As Integer

        Try

            'Set the number of buffers. WW
            NumberOfInputBuffers = 64
            MyHID.SetNumberOfInputBuffers _
                (HIDHandle, _
                64)

            'MDS
            'Get the number of input buffers.
            'MyHID.GetNumberOfInputBuffers _
            '   (HIDHandle, _
            '  NumberOfInputBuffers)

        Catch ex As Exception
            Call HandleException("GetInputReportBufferSize()", ex)
        End Try

    End Sub

    Private Sub GetInputReportData(ByVal ar As IAsyncResult)

        'Purpose    : Retrieves Input report data and status information.
        '           : This routine is called automatically when myInputReport.Read
        '           : returns.

        'Accepts    : ar - an object containing status information about 
        '           : the asynchronous operation.    

        'Dim ByteValue As String
        Dim Count As Integer
        Dim InputReportBuffer As Byte()
        'Dim ReadResult As String
        Dim Success As Boolean

        Try

            'Define a delegate using the IAsyncResult object.
            Dim deleg As ReadInputReportDelegate = _
                DirectCast(ar.AsyncState, ReadInputReportDelegate)

            'Get the IAsyncResult object and the values of other paramaters that the
            'BeginInvoke method passed ByRef.
            deleg.EndInvoke(MyDeviceDetected, InputReportBuffer, Success, ar)

            dataRxDone = True

            'Display the received report data in the form's list box.
            If (ar.IsCompleted And Success) Then

                'Debug.WriteLine(" Input Report ID: " & InputReportBuffer(0))
                'Debug.WriteLine(" Input Report Data:")

                For Count = 1 To UBound(InputReportBuffer)

                    rxBuffer(Count - 1) = InputReportBuffer(Count)


                    'Add a leading zero to values from 0 to F.
                    'If Len(Hex(InputReportBuffer(Count))) < 2 Then

                    'ByteValue = "0" & Hex(InputReportBuffer(Count))
                    'Else
                    '    ByteValue = Hex(InputReportBuffer(Count))
                    'End If

                    ' Debug.WriteLine(" " & ByteValue)

                    'Dim j As Integer
                    'For j = 0 To 20
                    'If (InputReportBuffer(j) <> 0) Then
                    'GoTo continue
                    'End If
                    'Next
                ' Dim i As Integer
                    'Dim str As String = "InputReportBuffer: "
                    'Dim substr As String
                    'For i = 0 To 63
                    '    substr = Format("{0:X}", Hex(InputReportBuffer(i)))
                    '    If (InputReportBuffer(i) < &HF) Then
                    '    substr = "0" + substr
                    '    End If
                    '   Str += substr + " "
                    'Next
                    'Str += "h"
                    'MsgBox(Str)
                    'continue:



                Next Count

            Else
                Debug.Write("The attempt to read an Input report has failed")
            End If

            dataRxDone = True

        Catch ex As Exception
            dataRxDone = True
            Call HandleException("GetInputReportData()", ex)
        End Try

    End Sub

    Private Sub ReadAndWriteToDevice(ByVal buf() As Byte, ByVal length As Integer, ByRef inBuf() As Byte)

        'Purpose    : Initiates the exchanging of reports. 
        '           : The application sends a report and requests to read a report.

        'Report header for the debug display:
        Debug.WriteLine("")
        Debug.WriteLine("***** HID Test Report *****")
        Debug.WriteLine(Today & ": " & TimeOfDay)

        Try
            'If the device hasn't been detected, was removed, or timed out on a previous attempt
            'to access it, look for the device.
            If (MyDeviceDetected = False) Then

                MyDeviceDetected = FindTheHid()
            End If

            If (MyDeviceDetected = True) Then

                'An option button selects whether to exchange Input and Output reports
                'or Feature reports.

                Call ExchangeInputAndOutputReports(buf, length, inBuf)

            End If

        Catch ex As Exception
            Call HandleException("ReadAndWriteToDevice()", ex)
        End Try

    End Sub


    Shared Sub HandleException(ByVal moduleName As String, ByVal e As Exception)

        'Purpose    : Provides a central mechanism for exception handling.
        '           : Displays a message box that describes the exception.

        'Accepts    : moduleName - the module where the exception occurred.
        '           : e - the exception

        Dim Message As String
        Dim Caption As String

        Try
            'Create an error message.
            Message = "Exception: " & e.Message & ControlChars.CrLf & _
            "Module: " & moduleName & ControlChars.CrLf & _
            "Method: " & e.TargetSite.Name

            'Specify a caption.
            Caption = "Unexpected Exception"

            'Display the message in a message box.
            MessageBox.Show(Message, Caption, MessageBoxButtons.OK)
            Debug.Write(Message)

        Finally

        End Try

    End Sub

    Public Function deviceAttached() As Boolean
        If ((ReadHandle <> INVALID_HANDLE_VALUE) And (WriteHandle <> INVALID_HANDLE_VALUE)) Then
            Return True
        Else
            Return False
        End If
    End Function
End Class
