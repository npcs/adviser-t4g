﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Composite.Events;
using Advisor.Modules.Catalog.Services;
using Advisor.Common.DataModel;
using Advisor.Modules.Catalog.PresentationModels;

namespace Advisor.Modules.Catalog.Views
{
    /// <summary>
    /// Interaction logic for PatientDetailsView.xaml
    /// </summary>
    public partial class PatientDetailsView : UserControl, IPatientDetailsView
    {
        public event EventHandler<DataEventArgs<Patient>> BackClicked = delegate { };

        public PatientDetailsView()
        {
            InitializeComponent();
        }

        #region IPatientDetailsView Members

        public PatientDetailsPresentationModel Model
        {
            get { return this.DataContext as PatientDetailsPresentationModel; }
            set { this.DataContext = value; }
        }

        #endregion

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            BackClicked(sender, new DataEventArgs<Patient>(null));
        }
    }
}
