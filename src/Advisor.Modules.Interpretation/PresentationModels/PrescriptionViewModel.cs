﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;

using Adviser.Client.Core.Presentation;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Services;
using System.Data.Objects.DataClasses;

namespace Advisor.Modules.Interpretation
{
    /// <summary>
    /// Presentation model for the Prescription item.
    /// </summary>
    public class PrescriptionViewModel : ViewModelBase
    {
        //private EntityCollection<Prescription> _prescriptions;
        ///// <summary>
        ///// Exposes bindable collection of patient objects.
        ///// </summary>
        //public EntityCollection<Prescription> Prescriptions
        //{
        //    get { return _prescriptions; }
        //    private set
        //    {
        //        _prescriptions = value;
        //        RaisePropertyChangedEvent("Prescriptions");
        //    }
        //}

        public PrescriptionViewModel(Visit visit) 
        {
            if (!visit.Prescriptions.IsLoaded)
                visit.Prescriptions.Load();
        }

    }
}
