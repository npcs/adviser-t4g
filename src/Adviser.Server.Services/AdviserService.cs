﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Diagnostics;

using Adviser.Server.DataModel;
using Adviser.Server.Core.Domain;
using Adviser.Server.Core.Services;
using Adviser.Server.Core.Logging;
using Adviser.Common.Logging;
using DataContracts = Adviser.Server.Services.Contracts;
using System.Net.Mail;

namespace Adviser.Server.Services
{
    public class AdviserService : IAdviserService
    {
        public DataContracts::AccountRegistration RegisterAccount(DataContracts::Practitioner practitioner, Guid deviceId)
        {
            DataContracts::AccountRegistration registration = new Adviser.Server.Services.Contracts.AccountRegistration();

            try
            {
                using (AdviserDBEntities dbContext = new AdviserDBEntities())
                {
                    //- check if the practitioner was already registered
                    ObjectResult<Practitioner> practitionerProbe = dbContext.GetPractitionerByID(practitioner.ID);
                    if (practitionerProbe.ToList().Count > 0)
                        throw new FaultException<DataContracts::AdviserServiceFault>(
                            new DataContracts::AdviserServiceFault(1, "Unable to register account for " + practitioner.Email));

                    //- check if email is unique
                    practitionerProbe = dbContext.GetPractitionerByEmail(practitioner.Email);
                    if (practitionerProbe.ToList().Count > 0)
                    {
                        registration.IsSuccess = false;
                        registration.MessageText = "Account with this email already exists on the server.\nPlease use unique email address to register.";
                        return registration;
                    }

                    // device registration
                    Device device = null;
#if LOCAL_SERVER
                    //- autoregister device on the local installation
                    device = new Device()
                    {
                        ID = deviceId,
                        ManufDate = new DateTime(2009, 6, 20),
                        OwnerID = practitioner.ID,
                        Status = DeviceStatus.Online.ToString(),
                        Version = "1.0.0"        
                    };

                    dbContext.DeviceSet.AddObject(device);
                    dbContext.SaveChanges();

#else
                    //- check if device exists and is available for registration
                    List<Device> deviceList = dbContext.GetDeviceByID(deviceId).ToList();
                    if (deviceList.Count == 0)
                    {
                        registration.IsSuccess = false;
                        registration.MessageText = "Account can not be registered.\nPlease contact support for assistance.";
                        return registration;
                    }
                    else
                    {
                        device = deviceList[0];
                        if (device.Status != DeviceStatus.Available.ToString())
                        {//- device must be available
                            registration.IsSuccess = false;
                            registration.MessageText = "Account can not be registered.\nPlease contact support for assistance.";
                            return registration;
                        }
                    }
#endif
                    //- now we can register practitioner
                    DataContractMapper mapper = new DataContractMapper();
                    Practitioner registeredPractitioner = new Practitioner();
                    registeredPractitioner = mapper.MapToEntity(practitioner);
                    registeredPractitioner.LastUpdDate = DateTime.Now.ToUniversalTime();
                    registeredPractitioner.Status = AccountStatus.Active;
                    device.Status = DeviceStatus.Online.ToString();
                    device.OwnerID = registeredPractitioner.ID;

                    //try
                    //{
                        dbContext.AddToPractitionerSet(registeredPractitioner);
                        //dbContext.SaveChanges();

                        Account account = new Account();
                        account.PractitionerID = registeredPractitioner.ID;
                        dbContext.AddToAccountSet(account);

                        DeviceHistory history = new DeviceHistory();
                        history.AccountID = registeredPractitioner.ID;
                        history.DeviceID = deviceId;
                        history.Event = "Registration";
                        history.Date = DateTime.Now.ToUniversalTime();
                        dbContext.AddToDeviceHistory(history);

                        dbContext.SaveChanges();
                    //}
                    //catch (Exception ex)
                    //{
                    //    Debug.WriteLine(ex.ToString());

                    //    throw new FaultException<DataContracts::AdviserServiceFault>(new DataContracts::AdviserServiceFault(101, String.Concat("Unable to register account for ", practitioner.Email, " on the server.\nPlease contact system administrator for further assistance.")));
                    //}

                    try
                    {
#if !LOCAL_SERVER
                        MailMessage message = new MailMessage();
                        message.Subject = "Adviser registration";
                        message.Body = "This is a confirmation email that you have succesfully registered your Adviser account.";
                        message.BodyEncoding = Encoding.UTF8;
                        message.From = new MailAddress("support@td-medical.com");
                        message.To.Add(new MailAddress(registeredPractitioner.Email));

                        System.Net.Mail.SmtpClient Smtp = new SmtpClient();
                        Smtp.Host = "mail.td-medical.com";
                        //Smtp.EnableSsl = true;
                        Smtp.Credentials = new System.Net.NetworkCredential("support@td-medical.com", "Cufn14UG5GHA");

                        Smtp.Send(message);
#endif
                        registration.MessageText = "Congratulations! Your Adviser account has been created.\n" +
                            "Confirmation email is sent to " + practitioner.Email + ".";

                    }
                    catch (Exception emailEx)
                    {
                        registration.MessageText = "Congratulations! Your Adviser account has been created.\n" +
                        "For some reasons we were unable to send confirmation email to " + practitioner.Email + ".";
                        LogProvider.Log(LogProvider.CreateLogEntry(deviceId, practitioner.ID, null, null, LogSeverity.Error, LogCategory.Services.ToString(), emailEx, registration.MessageText));
                    }

                    registration.AccountId = registeredPractitioner.ID.ToString();
                    registration.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                registration.IsSuccess = false;
                registration.MessageText = "Registration failed.\nPlease contact system administrator for further assistance.";

                LogProvider.Log(LogProvider.CreateLogEntry(deviceId, practitioner.ID, null, null, LogSeverity.Error, LogCategory.Services.ToString(), ex, registration.MessageText));
            }

            return registration;
        }


        /// <summary>
        /// Returns results of the measurement data processing.
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="deviceId"></param>
        /// <param name="inPatient"></param>
        /// <returns></returns>
        public DataContracts::Patient GetExamResults(Guid practitionerId, Guid deviceId, DataContracts::Patient inPatient)
        {
            DataContracts::Patient outPatient = new DataContracts::Patient();
            List<DataContracts::Visit> outVisits = new List<DataContracts::Visit>(inPatient.Visits.Length);
            List<Visit> visitsToProcess = new List<Visit>();

            try
            {
                using (AdviserDBEntities dbContext = new AdviserDBEntities())
                {
                    //- authorize operation
                    Practitioner practitioner = dbContext.LoadByKey<Practitioner>("ID", practitionerId);
                    if (practitioner == null)
                        throw new AdviserSecurityException("Practitioner " + practitionerId.ToString() + " doesn't exist.");
                    List<Device> devices = dbContext.GetDeviceByID(deviceId).ToList();
                    if (devices.Count == 0)
                        throw new AdviserSecurityException("Device with id " + deviceId.ToString() + " doesn't exist.");
                    if (devices[0].OwnerID != practitionerId)
                        throw new AdviserSecurityException("Device id " + deviceId.ToString() + " is not available for practitioner " + practitionerId);
                    if (devices[0].Status != DeviceStatus.Online.ToString())
                        throw new AdviserSecurityException("Device is locked out.");

                    IDBService dbService = new DBService();
                    AdviserLab lab = new AdviserLab(dbService);

                    DataContractMapper mapper = new DataContractMapper(dbContext);
                    //- compare received client data to server data and synchronize
                    Patient serverPatient = dbContext.LoadByKey<Patient>("ID", inPatient.ID);// practitioner.Patients.FirstOrDefault<Patient>(p => p.ID == inPatient.ID);
                    if (serverPatient == null)
                    {//- new patient

                        //- create new patient and all associated visits on the server
                        serverPatient = mapper.MapToEntity(inPatient);
                        visitsToProcess = serverPatient.Visits.ToList();

                        practitioner.Patients.Add(serverPatient);
                        //dbContext.AddToPatientSet(serverPatient);
                        dbContext.SaveChanges();
                    }
                    else
                    {//- patient already exists on the server

                        //- check data version and update oldest if required
                        if (inPatient.LastUpdDate > serverPatient.LastUpdDate)
                        {//- client has newer version
                            serverPatient = mapper.FlatCopyToEntity(inPatient, serverPatient);
                        }
                        else
                        {//- server has newer Patient record version
                            //- server patient will be copied to output later
                        }

                        foreach (DataContracts::Visit clientVisit in inPatient.Visits)
                        {
                            Visit serverVisit = dbContext.LoadByKey<Visit>("ID", clientVisit.ID);// serverPatient.Visits.FirstOrDefault(v => v.ID == clientVisit.ID);

                            if (serverVisit == null)
                            {//- create visit
                                serverVisit = mapper.MapToEntity(clientVisit);
                                serverPatient.Visits.Add(serverVisit);
                                dbContext.SaveChanges();
                            }
                            else
                            {//- update visit
                                //- assumption that client visit is always newer if server visit exists
                                serverVisit.Deleted = clientVisit.Deleted;
                                serverVisit.LastUpdDate = clientVisit.LastUpdDate;
                                serverVisit.Notes = clientVisit.Notes;
                                serverVisit.Prescriptions.Clear();
                                foreach (DataContracts::Prescription clientPresc in clientVisit.Prescriptions)
                                {
                                    serverVisit.Prescriptions.Add(mapper.MapToEntity(clientPresc));
                                }
                            }

                            //- add to visits process list
                            visitsToProcess.Add(serverVisit);
                        }
                        //- save synchronized data
                        dbContext.SaveChanges();
                    }

                    foreach (Visit v in visitsToProcess)
                    {//- process each visit
                        lab.ProcessData(v);
                        outVisits.Add(mapper.MapToDataContract(v));
                    }
                    //- save processed data
                    dbContext.SaveChanges();

                    outPatient = mapper.FlatCopyToDataContract(serverPatient, outPatient);
                    outPatient.Visits = outVisits.ToArray();

                    object[] parameters = new object[3];
                    parameters[0] = new SqlParameter("@AccountID", practitioner.ID);
                    parameters[1] = new SqlParameter("@VisitID", Guid.Empty);
                    parameters[2] = new SqlParameter("@DeviceID", deviceId);
                    using (DbCommand cmd = dbContext.CreateCommand("InsertUsageEntry", CommandType.StoredProcedure, parameters))
                    {
                        cmd.Connection.Open();
                        foreach (Visit v in visitsToProcess)
                        {
                            cmd.Parameters[1].Value = v.ID;
                            cmd.ExecuteNonQuery();
                        }
                        cmd.Connection.Close();
                    }
                }//- end of using context scope
            }
            catch (AdviserSecurityException secEx)
            {
                //Debug.WriteLine(secEx.ToString());
                LogProvider.Log(LogProvider.CreateLogEntry(deviceId, practitionerId, inPatient.ID, null, LogSeverity.Error, LogCategory.Security.ToString(), secEx, "Failed to process GetResults()"));

                string message = "You are not allowed to perform requested operation.\nPlease contact support for assistance.";
                throw new FaultException<DataContracts::AdviserServiceFault>(new DataContracts::AdviserServiceFault(403, message));
            }
            catch (Exception ex)
            {
                //Debug.WriteLine(ex.ToString());
                LogProvider.Log(LogProvider.CreateLogEntry(deviceId, practitionerId, inPatient.ID, null, LogSeverity.Error, LogCategory.Services.ToString(), ex, "Exception in process GetResults()"));

#if DEBUG
                string message = ex.ToString();
#else
                string message = "Failed to process results.";
#endif
                throw new FaultException<DataContracts::AdviserServiceFault>(new DataContracts::AdviserServiceFault(500, message));
            }

            return outPatient;
        }

        public DataContracts::SyncDataItem[] GetSyncData(Guid accountId)
        {
            List<DataContracts::SyncDataItem> syncList = new List<DataContracts::SyncDataItem>();
            try
            {
                using (AdviserDBEntities dbContext = new AdviserDBEntities())
                {
                    Practitioner practitioner = dbContext.GetPractitionerByID(accountId).ToList()[0];
                    practitioner.Patients.Load();
                    foreach (Patient patient in practitioner.Patients)
                    {
                        patient.Visits.Load();
                        DataContracts::SyncDataItem parentSyncItem = new Adviser.Server.Services.Contracts.SyncDataItem() { Key = patient.ID, Timestamp = patient.LastUpdDate, ChildType = "Visit", Children = new DataContracts::SyncDataItem[patient.Visits.Count] };
                        int idx = 0;
                        foreach (Visit visit in patient.Visits)
                        {
                            parentSyncItem.Children[idx] = new DataContracts::SyncDataItem() { Key = visit.ID, Timestamp = visit.LastUpdDate };
                            ++idx;
                        }
                        syncList.Add(parentSyncItem);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());

                throw new FaultException<DataContracts::AdviserServiceFault>(new DataContracts::AdviserServiceFault(500, "Unable to complete request."));
            }

            return syncList.ToArray();
        }

        public DataContracts::Practitioner GetPractitioner(string email, string password, Guid deviceId)
        {
            DataContracts::Practitioner outPractitioner = null;
            try
            {
                using (AdviserDBEntities dbContext = new AdviserDBEntities())
                {
                    Practitioner practitioner = dbContext.PractitionerSet.FirstOrDefault(p => p.Email == email);
                    if (practitioner == null)
                        throw new AdviserSecurityException(String.Format("Email {0} was not found.", email));

                    if(practitioner.Password != password)
                        throw new AdviserSecurityException("Invalid password.");

                    DataContractMapper mapper = new DataContractMapper(dbContext);
                    outPractitioner = mapper.MapToDataContract(practitioner);
                }

                return outPractitioner;
            }
            catch (AdviserSecurityException secEx)
            {
                string message = "Invalid email or/and password.\nPlease check and try again.";
                LogProvider.Log(LogProvider.CreateLogEntry(deviceId, null, null, null, LogSeverity.Error, LogCategory.Security.ToString(), secEx, message));
                
                throw new FaultException<DataContracts::AdviserServiceFault>(new DataContracts::AdviserServiceFault(403, message));
            }
            catch (Exception ex)
            {
                LogProvider.Log(LogProvider.CreateLogEntry(deviceId, null, null, null, LogSeverity.Error, LogCategory.Services.ToString(), ex, ex.Message));
                throw new FaultException<DataContracts::AdviserServiceFault>(new DataContracts::AdviserServiceFault(500, "Unable to complete request."));
            }
        }

        public DataContracts::AccountRegistration UpdateAccount(DataContracts::Practitioner practitioner)
        {
            DataContracts::AccountRegistration response = new Adviser.Server.Services.Contracts.AccountRegistration();
            try
            {
                using (AdviserDBEntities dbContext = new AdviserDBEntities())
                {
                    Practitioner serverPractitioner = dbContext.GetPractitionerByID(practitioner.ID).ToList()[0];
                    //- check if server version is newer
                    if (serverPractitioner.LastUpdDate > practitioner.LastUpdDate)
                    {
                        response.IsSuccess = false;
                        response.MessageText = "Server has newer version of your account data. Please contact server administrator to resolve issue.";
                        return response;
                    }
                    //- verify email is unique
                    if (serverPractitioner.Email != practitioner.Email)
                    {
                        ObjectResult<Practitioner> test = dbContext.GetPractitionerByEmail(practitioner.Email);
                        if (test.ToArray().Length > 0)
                        {
                            response.IsSuccess = false;
                            response.MessageText = "Email already exists. Please use different email.";
                            return response;
                        }
                    }

                    DataContractMapper mapper = new DataContractMapper(dbContext);
                    serverPractitioner = mapper.FlatCopyToEntity(practitioner, serverPractitioner);
                    dbContext.SaveChanges();

                    response.IsSuccess = true;
                    response.MessageText = "Account was succesfully updated.";
                }
            }
            catch (Exception ex)
            {
                //Debug.WriteLine(ex.ToString());
                LogProvider.Log(LogProvider.CreateLogEntry(null, practitioner.ID, null, null, LogSeverity.Error, LogCategory.Services.ToString(), ex, "Exception in process GetResults()"));

                throw new FaultException<DataContracts::AdviserServiceFault>(new DataContracts::AdviserServiceFault(500, "Unable to update account."));
            }

            return response;
        }

        public DataContracts::RetrievePasswordResult RetrievePassword(Guid practitionerId, string email)
        {
            DataContracts::RetrievePasswordResult result = new Adviser.Server.Services.Contracts.RetrievePasswordResult();

            using (AdviserDBEntities dbContext = new AdviserDBEntities())
            {
                Practitioner serverPractitioner = dbContext.GetPractitionerByID(practitionerId).ToList()[0];
                if (serverPractitioner.Email != email)
                {
                    result.IsSuccess = false;
                    result.Message = "No account registered under this e-mail.  Please contact system administrator.";
                }
                else
                {
                    try
                    {
                        MailMessage message = new MailMessage();

                        message.Subject = "Your Adviser Password.";
                        message.Body = "Your password is: " + serverPractitioner.Password;
                        message.BodyEncoding = Encoding.UTF8;
                        message.From = new MailAddress("support@td-medical.com");
                        message.To.Add(new MailAddress(email));

                        System.Net.Mail.SmtpClient Smtp = new SmtpClient();
                        Smtp.Host = "mail.td-medical.com";
                        //Smtp.EnableSsl = true;
                        Smtp.Credentials = new System.Net.NetworkCredential("support@td-medical.com", "Cufn14UG5GHA");

                        Smtp.Send(message);

                    }
                    catch (Exception e)
                    {
                        result.IsSuccess = false;
                        result.Message = "Error during sending the email. Please contact system administrator.";
                    }


                    result.IsSuccess = true;
                    result.Message = "Your password has been sent to your e-mail account.";

                }
            }


            return result;
        }

        public bool UpdatePatients(Guid practitionerId, DataContracts::Patient[] patients)
        {
            bool result = false;
            try
            {
                DataContractMapper mapper = new DataContractMapper();
                using (AdviserDBEntities dbContext = new AdviserDBEntities())
                {
                    Practitioner practitioner = dbContext.LoadByKey<Practitioner>("ID", practitionerId);
                    if (practitioner == null)
                        throw new AdviserSecurityException("Practitioner " + practitionerId.ToString() + " doesn't exist.");

                    foreach (DataContracts::Patient inPatient in patients)
                    {
                        Patient patient = dbContext.LoadByKey<Patient>("ID", inPatient.ID);
                        if (patient == null)
                        {
                            if (!inPatient.Deleted)
                            {
                                patient = mapper.MapToEntity(inPatient);
                                practitioner.Patients.Add(patient);
                            }
                        }
                        else
                        {
                            if (inPatient.Deleted)
                            {
                                patient.Deleted = true;
                            }
                            else
                            {
                                if (inPatient.LastUpdDate > patient.LastUpdDate)
                                    patient = mapper.FlatCopyToEntity(inPatient, patient);
                            }
                        }
                    }
                    dbContext.SaveChanges();
                }

                result = true;
            }
            catch (AdviserSecurityException secEx)
            {
                LogProvider.Log(LogProvider.CreateLogEntry(null, practitionerId, null, null, LogSeverity.Error, LogCategory.Security.ToString(), secEx, "Failed to execute UpdatePatients()"));

                //string message = "You are not allowed to perform requested operation.\nPlease contact support for assistance.";
                //throw new FaultException<DataContracts::AdviserServiceFault>(new DataContracts::AdviserServiceFault(403, message));
            }
            catch (Exception ex)
            {
                LogProvider.Log(LogProvider.CreateLogEntry(null, practitionerId, null, null, LogSeverity.Error, LogCategory.Services.ToString(), ex, "Exception in UpdatePatients()"));

                //throw new FaultException<DataContracts::AdviserServiceFault>(new DataContracts::AdviserServiceFault(500, "Unable to update patients."));
            }

            return result;
        }

        public bool UpdateVisits(Guid practitionerId, DataContracts::Visit[] inVisits)
        {
            bool result = false;
            try
            {
                DataContractMapper mapper = new DataContractMapper();
                using (AdviserDBEntities dbContext = new AdviserDBEntities())
                {
                    Practitioner practitioner = dbContext.LoadByKey<Practitioner>("ID", practitionerId);
                    if (practitioner == null)
                        throw new AdviserSecurityException("Practitioner " + practitionerId.ToString() + " doesn't exist.");

                    foreach (DataContracts::Visit inVisit in inVisits)
                    {
                        Visit visit = dbContext.LoadByKey<Visit>("ID", inVisit.ID);
                        if (visit == null)
                        {
                            if (inVisit.Deleted)
                            {
                                LogProvider.Log(LogProvider.CreateLogEntry(null, practitionerId, null, inVisit.ID,
                                    LogSeverity.Warning, LogCategory.Security.ToString(),
                                    "Attempt to delete Visit entity with id=" + inVisit.ID.ToString(),
                                    "Visit doesn't exist."));
                            }
                            else
                            {
                                visit = mapper.MapToEntity(inVisit);
                            }
                        }
                        else
                        {
                            if (inVisit.Deleted)
                            {
                                visit.Deleted = true;
                            }
                            else
                            {
                                visit = mapper.FlatCopyToEntity(inVisit, visit);
                                visit.Prescriptions.Load();
                                foreach (DataContracts::Prescription inPrescription in inVisit.Prescriptions)
                                {
                                    Prescription prescription = visit.Prescriptions.FirstOrDefault(p => p.ID == inPrescription.ID);
                                    if (prescription == null)
                                        prescription = mapper.MapToEntity(inPrescription);
                                    else
                                        prescription = mapper.FlatCopyToEntity(inPrescription, prescription);

                                    visit.Prescriptions.Add(prescription);
                                }
                            }
                        }
                    }

                    dbContext.SaveChanges();
                }
                result = true;
            }
            catch (AdviserSecurityException secEx)
            {
                LogProvider.Log(LogProvider.CreateLogEntry(null, practitionerId, null, null, LogSeverity.Error, LogCategory.Security.ToString(), secEx, "Failed to execute UpdateVisits()"));

                //string message = "You are not allowed to perform requested operation.\nPlease contact support for assistance.";
                //throw new FaultException<DataContracts::AdviserServiceFault>(new DataContracts::AdviserServiceFault(403, message));
            }
            catch (Exception ex)
            {
                LogProvider.Log(LogProvider.CreateLogEntry(null, practitionerId, null, null, LogSeverity.Error, LogCategory.Services.ToString(), ex, "Exception in UpdateVisits()"));

                //throw new FaultException<DataContracts::AdviserServiceFault>(new DataContracts::AdviserServiceFault(500, "Unable to update visits."));
            }

            return result;
        }

        public bool UpdatePrescriptions(Guid practitionerId, DataContracts::Prescription[] prescriptions)
        {
            bool result = false;
            try
            {
                DataContractMapper mapper = new DataContractMapper();
                using (AdviserDBEntities dbContext = new AdviserDBEntities())
                {
                    Practitioner practitioner = dbContext.LoadByKey<Practitioner>("ID", practitionerId);
                    if (practitioner == null)
                        throw new AdviserSecurityException("Practitioner " + practitionerId.ToString() + " doesn't exist.");

                    foreach (DataContracts::Prescription inRx in prescriptions)
                    {
                        Prescription rx = dbContext.LoadByKey<Prescription>("ID", inRx.ID);
                        if (rx == null)
                        {
                            if (inRx.Deleted)
                            {
                                LogProvider.Log(LogProvider.CreateLogEntry(null, practitionerId, null, null,
                                    LogSeverity.Warning, LogCategory.Security.ToString(),
                                    "Attempt to delete Prescription entity with id=" + inRx.ID.ToString(),
                                    "Prescription doesn't exist."));
                            }
                            else
                            {
                                rx = mapper.MapToEntity(inRx);
                            }
                        }
                        else
                        {
                            if (inRx.Deleted)
                                dbContext.DeleteByKey<Prescription>("ID", inRx.ID);
                            else
                                rx = mapper.FlatCopyToEntity(inRx, rx);
                        }
                    }

                    dbContext.SaveChanges();
                }
                result = true;
            }
            catch (AdviserSecurityException secEx)
            {
                LogProvider.Log(LogProvider.CreateLogEntry(null, practitionerId, null, null, LogSeverity.Error, LogCategory.Security.ToString(), secEx, "Failed to execute UpdateVisits()"));
            }
            catch (Exception ex)
            {
                LogProvider.Log(LogProvider.CreateLogEntry(null, practitionerId, null, null, LogSeverity.Error, LogCategory.Services.ToString(), ex, "Exception in UpdateVisits()"));
            }
            return result;
        }

        public void DeleteVisits(Guid practitionerId, Guid[] visits)
        {
            try
            {
                DataContractMapper mapper = new DataContractMapper();
                using (AdviserDBEntities dbContext = new AdviserDBEntities())
                {
                    Practitioner practitioner = dbContext.LoadByKey<Practitioner>("ID", practitionerId);
                    if (practitioner == null)
                        throw new AdviserSecurityException("Practitioner " + practitionerId.ToString() + " doesn't exist.");

                    foreach (Guid visitId in visits)
                    {
                        Visit visit = dbContext.LoadByKey<Visit>("ID", visitId);
                        if (visit != null)
                        {
                            visit.Deleted = true;
                        }
                        else
                        {
                            LogProvider.Log(LogProvider.CreateLogEntry(null, practitionerId, null, visitId, LogSeverity.Warning, LogCategory.Security.ToString(), "Attempt to delete Visit entity with id=" + visitId.ToString(), "Visit doesn't exist."));
                        }
                    }
                    dbContext.SaveChanges();
                }
            }
            catch (AdviserSecurityException secEx)
            {
                LogProvider.Log(LogProvider.CreateLogEntry(null, practitionerId, null, null, LogSeverity.Error, LogCategory.Security.ToString(), secEx, "Failed to execute DeleteVisits()"));

                string message = "You are not allowed to perform requested operation.\nPlease contact support for assistance.";
                throw new FaultException<DataContracts::AdviserServiceFault>(new DataContracts::AdviserServiceFault(403, message));
            }
            catch (Exception ex)
            {
                LogProvider.Log(LogProvider.CreateLogEntry(null, practitionerId, null, null, LogSeverity.Error, LogCategory.Services.ToString(), ex, "Exception in DeleteVisits()"));

                throw new FaultException<DataContracts::AdviserServiceFault>(new DataContracts::AdviserServiceFault(500, "Unable to delete visits."));
            }
        }

        public bool ReportError(DataContracts::AdviserClientLogEntry inLogEntry)
        {
            bool result = false;

            try
            {
                using (AdviserDBEntities dbContext = new AdviserDBEntities())
                {
                    ClientError error = new ClientError();
                    error.AccountID = inLogEntry.PractitionerId;
                    error.DeviceID = inLogEntry.DeviceId;
                    error.VisitID = inLogEntry.VisistId;
                    error.Time = inLogEntry.Time;
                    error.Category = inLogEntry.Category;
                    error.Type = inLogEntry.LogItemType;
                    error.ErrorMessage = inLogEntry.ErrorMessage;
                    if (inLogEntry.Details != null)
                        error.Details = inLogEntry.Details.ToString();

                    error.Status = "Received";

                    dbContext.AddToClientErrorSet(error);
                    dbContext.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                result = false;
            }

            return result;
        }

        public DataContracts::Patient GetPatientWithAllVisits(Guid practitionerId, Guid patientId)
        {
            DataContracts::Patient outPatient = null;
            using (AdviserDBEntities dbContext = new AdviserDBEntities())
            {
                DataContractMapper mapper = new DataContractMapper();
                Patient patient = dbContext.PatientSet
                    .Include("Visit")
                    .Include("OrganExamination")
                    .Include("Prescription")
                    .Include("Symptom")
                    .FirstOrDefault(p => p.ID == patientId);

                outPatient = mapper.MapToDataContract(patient);
            }

            return outPatient;
        }

        public DataContracts::Patient GetPatient(Guid practitionerId, Guid patientId, Guid[] visitIds)
        {
            throw new NotImplementedException();
        }

        public DataContracts::Visit[] GetVisits(Guid practitionerId, Guid patientId, Guid[] visitIds)
        {
            throw new NotImplementedException();
        }

        public void CreatePatient(Guid practitionerId, DataContracts::Patient patient)
        {
            throw new NotImplementedException();
        }

        public void CreateVisits(Guid practitionerId, Guid patientId, DataContracts::Visit[] visits)
        {
            throw new NotImplementedException();
        }
    }
}
