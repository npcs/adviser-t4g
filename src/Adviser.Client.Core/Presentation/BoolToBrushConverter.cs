﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;
using System.Drawing;

namespace Adviser.Client.Core.Presentation
{
    [ValueConversion(typeof(bool), typeof(Brush))]
    public class BoolToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            bool inputBool = bool.Parse(value.ToString());
            if (inputBool)
            {
                return new SolidBrush(HexStringToColor(parameter.ToString())); 
            }
            else
            {
                return Brushes.Transparent;
            }

        }
        private Color HexStringToColor(string hexColor)
        {
            if (hexColor.StartsWith("#"))
                hexColor = hexColor.Remove(0, 1);
            byte a = Byte.Parse(hexColor.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte r = Byte.Parse(hexColor.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = Byte.Parse(hexColor.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = Byte.Parse(hexColor.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);

            Color color = Color.FromArgb(a, r, g, b);
            return color;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }

    }
}
