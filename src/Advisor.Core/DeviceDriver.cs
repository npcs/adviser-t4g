﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Advisor.Common.DataModel;

namespace Advisor.Core
{
    /// <summary>
    /// DeviceDriver class manages access to the hardware
    /// </summary>
    public class DeviceDriver : IDeviceDriver
    {
        #region private 

        private xUSBProtocol.xUSBProtocol _Connector;
        private DeviceSettings _DeviceSettings;
        private bool _IsConnected;
        private bool _FrontPositive = false; //TODO: true or false here. We need to test the hardware. 

        private byte[] GetByteArray(ushort UnsignedShort)
        {
            byte[] _returnValue = new byte[2];
            _returnValue = BitConverter.GetBytes(UnsignedShort);
            byte tmp = _returnValue[0];
            _returnValue[0] = _returnValue[1];
            _returnValue[1] = tmp;
            return _returnValue;
        }

        private ushort GetUShort(byte[] data)
        {
            byte tmp = data[0];
            data[0] = data[1];
            data[1] = tmp;
            return BitConverter.ToUInt16(data, 0);
        }

        #endregion

        public DeviceDriver()
        {
            _Connector = new Advisor.xUSBProtocol.xUSBProtocol();
            _IsConnected = _Connector.IsBridgeAttached;
        }

        /// <summary>
        /// Set I2C Bus Speed. 400 KHz or 100KHz (default)
        /// </summary>
        private void SetBusSpeed()
        {
            _Connector.setBusSpeed(
                _DeviceSettings.IsHighSpeedBus ? Advisor.xUSBProtocol.xUSBProtocol.BusSpeed.speed400KHz : Advisor.xUSBProtocol.xUSBProtocol.BusSpeed.speed100KHz);
        }

        #region IDeviceDriver Members

        /// <summary>
        /// Connects pair of electrodes. If front = 7 and back = 7, then internal 10K resistor is connected. 
        /// </summary>
        /// <param name="front">0-based front electrode number (0..7)</param>
        /// <param name="back">0-based back electrode number(0..7)</param>
        /// <returns>true if successfully connected</returns>
        public bool ConnectElectrodes(byte front, byte back)
        {
            if (front > 7 || back > 7)
            {
                return false;
            }
            byte dataByte;
            dataByte = (byte)(front + (back << 3));
            if (!_FrontPositive)
            {
                dataByte += 128;
            }
            if (_Connector.i2cWriteByteToRegister(_DeviceSettings.ElectrodeConnectorDeviceAddress, dataByte, dataByte) == Advisor.xUSBProtocol.xUSBProtocol.Status.success)
            {
                return true;
            }
            return false;
        }

        public bool SetPolarity(bool frontPositive)
        {
            this._FrontPositive = frontPositive;

            return true;
        }

        public bool SetupDevice(DeviceSettings settings)
        {
            _DeviceSettings = settings;

            SetBusSpeed();
            
            //Setup ADC
            if (_Connector.i2cWrite(_DeviceSettings.ADCAddress, 
                _DeviceSettings.ADCConfigRegisterAddress, 2, 
                GetByteArray(_DeviceSettings.ADCSettings)) == Advisor.xUSBProtocol.xUSBProtocol.Status.success)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Reads Voltage drop on the shunt resistor.
        /// </summary>
        /// <returns></returns>
        public ushort ReadData()
        {
            byte[] data = new byte[2];
            _Connector.i2cRead(_DeviceSettings.ADCAddress, _DeviceSettings.ADCShuntVoltageRegisterAddress, 2, ref data);            
            return GetUShort(data);
        }


        public ushort ReadBusVoltage()
        {
            byte[] data = new byte[2];
            _Connector.i2cRead(_DeviceSettings.ADCAddress, _DeviceSettings.ADCBusVoltageRegisterAddress, 2, ref data);
            return GetUShort(data);
        }

        /// <summary>
        /// Get Device GUID. 
        /// </summary>
        /// <param name="DeviceGUID">Device GUID</param>
        /// <returns>True if Guid is found on the device.</returns>
        public bool GetDeviceGUID(ref Guid DeviceGUID)
        {//TODO: this method needs debugging - throws exception
            bool _returnValue = false;
            Byte[] data = new byte[15];
            if (_Connector.readEepromPacket(_DeviceSettings.GuidAddress, 16, ref data) == xUSBProtocol.xUSBProtocol.Status.success)
            {
                try
                {
                    DeviceGUID = new Guid(data);
                    _returnValue = true;
                }
                catch
                {
                    DeviceGUID = System.Guid.Empty;
                }
            }
            else
            {
                DeviceGUID = System.Guid.Empty;
            }
            return _returnValue;

        }

        /// <summary>
        /// Get USB bridge firmvare version.
        /// </summary>
        /// <param name="BridgeVersion">returns USB bridge version in the followiing format: 17.1.0</param>
        /// <returns>true if bridge is attached and functional</returns>
        public bool GetBridgeVersion(ref string BridgeVersion)
        {
            bool _returnValue = false;

            if (_Connector.version(ref BridgeVersion) == Advisor.xUSBProtocol.xUSBProtocol.Status.success)
            {
                _returnValue = true;
            }
            return _returnValue;
        }

        public bool IsConnected
        {
            get { return _IsConnected;}
        }

        public void Release()
        {
            if (_Connector != null)
            {
                _Connector = null;
            }
        }

        #endregion

        #region Temporary Methods. Remove later

        public ushort ReadADCRegister(byte RegisterAddress)
        {
            byte[] data = new byte[2];
            _Connector.i2cRead(_DeviceSettings.ADCAddress, RegisterAddress, 2, ref data);           
            return GetUShort(data);
        }


        #endregion

    }
}
