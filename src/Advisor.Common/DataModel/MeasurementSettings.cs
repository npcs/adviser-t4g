﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Common.DataModel
{
    /// <summary>
    /// Aggregates attributes used by device during measurement session.
    /// </summary>
    [Serializable()]
    public class MeasurementSettings
    {
        private int _pairMeasurementDuration;
        private int _deviceSamplingRate;
        private List<PairConnection> _electrodesConnectionMap;
        private bool _isFrontPositive;

        /// <summary>
        /// Duration of measurement between one pair of electrodes.
        /// </summary>
        public int PairMeasurementDuration
        {
            get { return _pairMeasurementDuration; }
            set { _pairMeasurementDuration = value; }
        }

        /// <summary>
        /// Electrodes connection configuration as list of
        /// front-back indicies paris.
        /// </summary>
        public List<PairConnection> ElectrodesConnectionMap
        {
            get { return _electrodesConnectionMap; }
            set { _electrodesConnectionMap = value; }
        }

        /// <summary>
        /// True if positive charge is applied to front set of electrodes,
        /// otherwise false.
        /// </summary>
        public bool IsFrontPositive
        {
            get { return _isFrontPositive; }
            set { _isFrontPositive = value; }
        }

        /// <summary>
        /// Sampling rate in ms to read data from the device.
        /// </summary>
        public int DeviceSamplingRate
        {
            get { return _deviceSamplingRate; }
            set { _deviceSamplingRate = value; }
        }

        /// <summary>
        /// Input Voltage (V).
        /// </summary>
        public double Voltage
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates if this is a calibration measurement.
        /// </summary>
        public bool Calibration
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates number of measurement leads (front).
        /// </summary>
        public int FrontLeads
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates number of measurement leads (back).
        /// </summary>
        public int BackLeads
        {
            get;
            set;
        }

        /// <summary>
        /// Value of the shunt resistor (Ohm).
        /// </summary>
        public double ShuntResistor
        {
            get;
            set;
        }

        public override string ToString()
        {
            string output = "Device MeasurementContext:\n" +
                " Pair measurement duration = " + _pairMeasurementDuration + "\n" +
                " Front positive = " + _isFrontPositive.ToString() + "\n" +
                " Sampling rate = " + _deviceSamplingRate;
            return output;
        }
    }
}
