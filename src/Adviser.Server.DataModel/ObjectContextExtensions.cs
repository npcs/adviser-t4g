﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Objects;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.Objects.DataClasses;

namespace Adviser.Server.DataModel
{
    public static class ObjectContextExtensions
    {
        /// <summary>
        /// Loads entity of type T by value of its EntityKey.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context"></param>
        /// <param name="propertyName"></param>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public static T LoadByKey<T>(this ObjectContext context, String propertyName, Object keyValue)
        {
            IEnumerable<KeyValuePair<string, object>> entityKeyValues =
               new KeyValuePair<string, object>[] { new KeyValuePair<string, object>(propertyName, keyValue) };
            string entitySetName = context.GetType().Name + "." + typeof(T).Name + "Set";
            EntityKey key = new EntityKey(entitySetName, entityKeyValues);

            object result = null;
            try
            {
                result = context.GetObjectByKey(key);
            }
            catch (ObjectNotFoundException)
            {
                result = null;
            }

            if (result != null)
                return (T)result;
            else
                return default(T);
        }

        public static void DeleteByKey<T>(this ObjectContext context, String propertyName, Object keyValue)
        {
            T entity = LoadByKey<T>(context, propertyName, keyValue);
            if (entity != null)
                context.DeleteObject(entity);
        }

        /// <summary>
        /// Creates database command (stored proc, query) in the current context.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static DbCommand CreateCommand(this ObjectContext context,
            string commandText,
            CommandType commandType,
            params object[] parameters)
        {
            EntityConnection entityConnection = (EntityConnection)context.Connection;
            DbConnection storeConnection = entityConnection.StoreConnection;
            DbCommand storeCommand = storeConnection.CreateCommand();

            storeCommand.CommandText = commandText;
            storeCommand.CommandType = commandType;

            if (null != parameters)
                storeCommand.Parameters.AddRange(parameters);

            if (context.CommandTimeout.HasValue)
                storeCommand.CommandTimeout = context.CommandTimeout.Value;

            return storeCommand;
        }

            //if (null != parameters && parameters.Length > 0)
            //{
            //    EntityParameter[] dbParams = new EntityParameter[parameters.Length];
            //    if (parameters is EntityParameter[])
            //    {
            //        ((EntityParameter[])parameters).CopyTo(dbParams, 0);
            //    }
            //    else
            //    {
            //        for (int i = 0; i < parameters.Length; i++)
            //        {
            //            EntityParameter p = null;
            //            object param = parameters[i];
            //            if (param is DbParameter)
            //            {
            //                DbParameter parm = (DbParameter)param; p = (EntityParameter)parm;
            //            }
            //            else if (param is ObjectParameter)
            //            {
            //                p = storeCommand.CreateParameter();
            //                ObjectParameter o = (ObjectParameter)param;
            //                p.ParameterName = (o.Name.StartsWith("@")) ? o.Name.TrimStart(new char[] { '@' }) : o.Name; p.Value = o.Value;
            //            }
            //            else
            //            {
            //                p = storeCommand.CreateParameter();
            //                p.Value = parameters[i];
            //            }

            //            dbParams[i] = p;

            //        }
            //        storeCommand.Parameters.AddRange(dbParams);
            //    }
            //}
    }
}
