﻿using System;

namespace Adviser.Client.Core.Events
{
    [Serializable()]
    public class NavigationEventArgs
    {
        public string EventName { get; set; }
        public object State { get; set; }
    }
}
