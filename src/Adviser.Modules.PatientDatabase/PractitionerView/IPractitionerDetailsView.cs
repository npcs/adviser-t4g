﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Modules.PatientDatabase
{
    public interface IPractitionerDetailsView
    {
        void ShowMessage(string message);
        void ShowMessage(string message, bool isOKButtonVisible);
        bool IsUpdateMode { get; set; }
        IPractitionerModel Model { get; set; }
        event EventHandler UpdateClicked;
        event EventHandler CreateClicked;
        event EventHandler CancelClicked;
        event EventHandler OkClicked;
    }
}
