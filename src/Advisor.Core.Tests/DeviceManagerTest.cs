﻿using Adviser.Client.Core.DeviceControl;
using Adviser.Common.Domain;
using Adviser.Client.Core.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Advisor.Core.Tests
{
    /// <summary>
    ///This is a test class for DeviceManagerTest and is intended
    ///to contain all DeviceManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DeviceManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for StartMeasurement
        ///</summary>
        [TestMethod()]
        public void StartMeasurementTest()
        {
            IDeviceDriver driver = new DeviceEmulator(); 
            MeasurementSettings settings = new MeasurementSettings();
            settings.DeviceSamplingRate = 200;
            settings.IsFrontPositive = true;
            settings.PairMeasurementDuration = 1000;
            settings.ElectrodesConnectionMap = new System.Collections.Generic.List<PairConnection>(49);
            for (byte x = 0; x < 7; ++x)
            {
                for (byte y = 0; y < 7; ++y)
                {
                    PairConnection pair = new PairConnection();
                    pair.Front = x;
                    pair.Back = y;
                    settings.ElectrodesConnectionMap.Add(pair);
                }
            }
            
            IConfigurationDataService service = new ConfigurationDataService();
            DeviceSettings deviceSettings = service.GetDeviceSettings();
            deviceSettings.EmulateDevice = true;
            DeviceManager target = new DeviceManager();
            target.Initialize(deviceSettings, settings);
            target.Settings = settings;
            target.StartMeasurement();
        }
    }
}
