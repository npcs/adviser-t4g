﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Server.DataModel
{
    public partial class Visit
    {
        /// <summary>
        /// Copies only visit domain specific data (no children entities, no framework fields).
        /// </summary>
        /// <param name="visit"></param>
        public void CopyData(Visit visit)
        {
            this.Date = visit.Date;
            this.ExamResult = visit.ExamResult;
            this.LastUpdDate = visit.LastUpdDate;
            this.MeasurementData = visit.MeasurementData;
            this.Notes = visit.Notes;
        }

        /// <summary>
        /// Makes shallow copy of the object graph.
        /// </summary>
        /// <param name="visit"></param>
        /// <returns></returns>
        public Visit ShallowCopy(Visit visit)
        {
            return (Visit)this.MemberwiseClone();
        }
    }
}
