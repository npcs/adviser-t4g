﻿using System;

namespace Adviser.Server.DataModel
{
    /// <summary>
    /// Status of the practitioner's account.
    /// </summary>
    public enum AccountStatus
    {
        PendingConfirmation = 1,
        Active = 2,
        Inactive = 3,
        Suspended = 4
    }
}
