﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Adviser.Client.DataModel;
namespace Adviser.Modules.PatientDatabase
{
    /// <summary>
    /// Interaction logic for NotesHistoryView.xaml
    /// </summary>
    public partial class NotesHistoryView : UserControl, INotesHistoryView
    {
        public NotesHistoryView()
        {
            InitializeComponent();
        }

        public IVisitListViewModel Model
        {
            get
            {
                return this.DataContext as IVisitListViewModel;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
