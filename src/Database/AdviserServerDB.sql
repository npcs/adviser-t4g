/****** Object:  ForeignKey [FK_MedicineOrganConditionXRef_Conditions]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicineOrganConditionXRef_Conditions]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicineOrganConditionXRef]'))
ALTER TABLE [dbo].[MedicineOrganConditionXRef] DROP CONSTRAINT [FK_MedicineOrganConditionXRef_Conditions]
GO
/****** Object:  ForeignKey [FK_MedicineOrganConditionXRef_Medicine]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicineOrganConditionXRef_Medicine]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicineOrganConditionXRef]'))
ALTER TABLE [dbo].[MedicineOrganConditionXRef] DROP CONSTRAINT [FK_MedicineOrganConditionXRef_Medicine]
GO
/****** Object:  ForeignKey [FK_MedicineOrganConditionXRef_Organs]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicineOrganConditionXRef_Organs]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicineOrganConditionXRef]'))
ALTER TABLE [dbo].[MedicineOrganConditionXRef] DROP CONSTRAINT [FK_MedicineOrganConditionXRef_Organs]
GO
/****** Object:  ForeignKey [FK_MedicineSymptomXRef_Medicine]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicineSymptomXRef_Medicine]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicineSymptomXRef]'))
ALTER TABLE [dbo].[MedicineSymptomXRef] DROP CONSTRAINT [FK_MedicineSymptomXRef_Medicine]
GO
/****** Object:  ForeignKey [FK_MedicineSymptomXRef_Symptoms ]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicineSymptomXRef_Symptoms ]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicineSymptomXRef]'))
ALTER TABLE [dbo].[MedicineSymptomXRef] DROP CONSTRAINT [FK_MedicineSymptomXRef_Symptoms ]
GO
/****** Object:  ForeignKey [FK_OrganExamination_Organs]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganExamination_Organs]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [FK_OrganExamination_Organs]
GO
/****** Object:  ForeignKey [FK_OrganExamination_Visits1]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganExamination_Visits1]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [FK_OrganExamination_Visits1]
GO
/****** Object:  ForeignKey [FK_Patient_Practitioner]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Patient_Practitioner]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
ALTER TABLE [dbo].[Patients] DROP CONSTRAINT [FK_Patient_Practitioner]
GO
/****** Object:  ForeignKey [FK_Prescriptions_Visit]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Prescriptions_Visit]') AND parent_object_id = OBJECT_ID(N'[dbo].[Prescriptions]'))
ALTER TABLE [dbo].[Prescriptions] DROP CONSTRAINT [FK_Prescriptions_Visit]
GO
/****** Object:  ForeignKey [FK_SymptomVisitXRef_Symptoms ]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SymptomVisitXRef_Symptoms ]') AND parent_object_id = OBJECT_ID(N'[dbo].[SymptomVisitXRef]'))
ALTER TABLE [dbo].[SymptomVisitXRef] DROP CONSTRAINT [FK_SymptomVisitXRef_Symptoms ]
GO
/****** Object:  ForeignKey [FK_SymptomVisitXRef_Visit]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SymptomVisitXRef_Visit]') AND parent_object_id = OBJECT_ID(N'[dbo].[SymptomVisitXRef]'))
ALTER TABLE [dbo].[SymptomVisitXRef] DROP CONSTRAINT [FK_SymptomVisitXRef_Visit]
GO
/****** Object:  ForeignKey [FK_UsageData_Devices]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UsageData_Devices]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsageData]'))
ALTER TABLE [dbo].[UsageData] DROP CONSTRAINT [FK_UsageData_Devices]
GO
/****** Object:  ForeignKey [FK_Visit_Patient]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Visit_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[Visits]'))
ALTER TABLE [dbo].[Visits] DROP CONSTRAINT [FK_Visit_Patient]
GO
/****** Object:  Default [DF_Conditions_OrderKey]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Conditions_OrderKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[Conditions]'))
Begin
ALTER TABLE [dbo].[Conditions] DROP CONSTRAINT [DF_Conditions_OrderKey]

End
GO
/****** Object:  Default [DF_OrganExamination_ID]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [DF_OrganExamination_ID]

End
GO
/****** Object:  Default [DF_OrganExamination_Value]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_Value]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [DF_OrganExamination_Value]

End
GO
/****** Object:  Default [DF_Table_1_Operation]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Table_1_Operation]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [DF_Table_1_Operation]

End
GO
/****** Object:  Default [DF_Table_1_HronicDesease]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Table_1_HronicDesease]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [DF_Table_1_HronicDesease]

End
GO
/****** Object:  Default [DF_OrganExamination_Removed]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_Removed]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [DF_OrganExamination_Removed]

End
GO
/****** Object:  Default [DF_OrganExamination_Implant]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_Implant]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [DF_OrganExamination_Implant]

End
GO
/****** Object:  Default [DF_Organs_OrderKey]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Organs_OrderKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[Organs]'))
Begin
ALTER TABLE [dbo].[Organs] DROP CONSTRAINT [DF_Organs_OrderKey]

End
GO
/****** Object:  Default [DF_Patient_Id]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Patient_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
Begin
ALTER TABLE [dbo].[Patients] DROP CONSTRAINT [DF_Patient_Id]

End
GO
/****** Object:  Default [DF_Patient_LastUpdDate]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Patient_LastUpdDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
Begin
ALTER TABLE [dbo].[Patients] DROP CONSTRAINT [DF_Patient_LastUpdDate]

End
GO
/****** Object:  Default [DF_Patients_Deleted]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Patients_Deleted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
Begin
ALTER TABLE [dbo].[Patients] DROP CONSTRAINT [DF_Patients_Deleted]

End
GO
/****** Object:  Default [DF_Practitioner_Id]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Practitioner_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Practitioners]'))
Begin
ALTER TABLE [dbo].[Practitioners] DROP CONSTRAINT [DF_Practitioner_Id]

End
GO
/****** Object:  Default [DF_Practitioners_StatusId]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Practitioners_StatusId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Practitioners]'))
Begin
ALTER TABLE [dbo].[Practitioners] DROP CONSTRAINT [DF_Practitioners_StatusId]

End
GO
/****** Object:  Default [DF_Prescriptions_ID]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Prescriptions_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Prescriptions]'))
Begin
ALTER TABLE [dbo].[Prescriptions] DROP CONSTRAINT [DF_Prescriptions_ID]

End
GO
/****** Object:  Default [DF_Symptoms _OrderKey]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Symptoms _OrderKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[Symptoms ]'))
Begin
ALTER TABLE [dbo].[Symptoms ] DROP CONSTRAINT [DF_Symptoms _OrderKey]

End
GO
/****** Object:  Default [DF_Visits_Id]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Visits_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Visits]'))
Begin
ALTER TABLE [dbo].[Visits] DROP CONSTRAINT [DF_Visits_Id]

End
GO
/****** Object:  Default [DF_Visits_Date]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Visits_Date]') AND parent_object_id = OBJECT_ID(N'[dbo].[Visits]'))
Begin
ALTER TABLE [dbo].[Visits] DROP CONSTRAINT [DF_Visits_Date]

End
GO
/****** Object:  StoredProcedure [dbo].[InsertSymptomVisitXRef]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertSymptomVisitXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertSymptomVisitXRef]
GO
/****** Object:  StoredProcedure [dbo].[DeleteSymptomVisitXRef]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteSymptomVisitXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteSymptomVisitXRef]
GO
/****** Object:  StoredProcedure [dbo].[UpdateVisit]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateVisit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateVisit]
GO
/****** Object:  StoredProcedure [dbo].[InsertVisit]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertVisit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertVisit]
GO
/****** Object:  StoredProcedure [dbo].[InsertMedicineOrganConditionXRef]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertMedicineOrganConditionXRef]
GO
/****** Object:  StoredProcedure [dbo].[DeleteMedicineOrganConditionXRef]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteMedicineOrganConditionXRef]
GO
/****** Object:  StoredProcedure [dbo].[InsertMedicineSymptomXRef]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertMedicineSymptomXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertMedicineSymptomXRef]
GO
/****** Object:  StoredProcedure [dbo].[DeleteMedicineSymptomXRef]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteMedicineSymptomXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteMedicineSymptomXRef]
GO
/****** Object:  StoredProcedure [dbo].[CreateNewVisit]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateNewVisit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CreateNewVisit]
GO
/****** Object:  StoredProcedure [dbo].[GetPassword]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPassword]
GO
/****** Object:  StoredProcedure [dbo].[GetPractitionerByEmail]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPractitionerByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPractitionerByEmail]
GO
/****** Object:  StoredProcedure [dbo].[GetPractitionerByID]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPractitionerByID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPractitionerByID]
GO
/****** Object:  StoredProcedure [dbo].[GetMedicineByOrganCondition]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMedicineByOrganCondition]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetMedicineByOrganCondition]
GO
/****** Object:  StoredProcedure [dbo].[GetMedicineByChronicOrgan]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMedicineByChronicOrgan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetMedicineByChronicOrgan]
GO
/****** Object:  StoredProcedure [dbo].[GetDeviceByID]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDeviceByID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetDeviceByID]
GO
/****** Object:  StoredProcedure [dbo].[ExportCustomers]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ExportCustomers]
GO
/****** Object:  StoredProcedure [dbo].[ExportInvoices]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportInvoices]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ExportInvoices]
GO
/****** Object:  Table [dbo].[Prescriptions]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Prescriptions]') AND type in (N'U'))
DROP TABLE [dbo].[Prescriptions]
GO
/****** Object:  Table [dbo].[SymptomVisitXRef]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SymptomVisitXRef]') AND type in (N'U'))
DROP TABLE [dbo].[SymptomVisitXRef]
GO
/****** Object:  Table [dbo].[OrganExamination]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganExamination]') AND type in (N'U'))
DROP TABLE [dbo].[OrganExamination]
GO
/****** Object:  Table [dbo].[Visits]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Visits]') AND type in (N'U'))
DROP TABLE [dbo].[Visits]
GO
/****** Object:  Table [dbo].[Patients]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Patients]') AND type in (N'U'))
DROP TABLE [dbo].[Patients]
GO
/****** Object:  Table [dbo].[MedicineOrganConditionXRef]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicineOrganConditionXRef]') AND type in (N'U'))
DROP TABLE [dbo].[MedicineOrganConditionXRef]
GO
/****** Object:  Table [dbo].[MedicineSymptomXRef]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicineSymptomXRef]') AND type in (N'U'))
DROP TABLE [dbo].[MedicineSymptomXRef]
GO
/****** Object:  Table [dbo].[UsageData]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsageData]') AND type in (N'U'))
DROP TABLE [dbo].[UsageData]
GO
/****** Object:  Table [dbo].[Conditions]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Conditions]') AND type in (N'U'))
DROP TABLE [dbo].[Conditions]
GO
/****** Object:  StoredProcedure [dbo].[GetConditionByValue]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetConditionByValue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetConditionByValue]
GO
/****** Object:  StoredProcedure [dbo].[InsertUsageEntry]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertUsageEntry]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertUsageEntry]
GO
/****** Object:  Table [dbo].[OrganZoneDependency]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganZoneDependency]') AND type in (N'U'))
DROP TABLE [dbo].[OrganZoneDependency]
GO
/****** Object:  Table [dbo].[Symptoms ]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Symptoms ]') AND type in (N'U'))
DROP TABLE [dbo].[Symptoms ]
GO
/****** Object:  Table [dbo].[Organs]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Organs]') AND type in (N'U'))
DROP TABLE [dbo].[Organs]
GO
/****** Object:  Table [dbo].[DeviceHistory]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeviceHistory]') AND type in (N'U'))
DROP TABLE [dbo].[DeviceHistory]
GO
/****** Object:  Table [dbo].[Chakras]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Chakras]') AND type in (N'U'))
DROP TABLE [dbo].[Chakras]
GO
/****** Object:  StoredProcedure [dbo].[DeleteVisit]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteVisit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteVisit]
GO
/****** Object:  Table [dbo].[ClientErrors]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClientErrors]') AND type in (N'U'))
DROP TABLE [dbo].[ClientErrors]
GO
/****** Object:  Table [dbo].[ServerErrors]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ServerErrors]') AND type in (N'U'))
DROP TABLE [dbo].[ServerErrors]
GO
/****** Object:  Table [dbo].[Practitioners]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Practitioners]') AND type in (N'U'))
DROP TABLE [dbo].[Practitioners]
GO
/****** Object:  Table [dbo].[Medicine]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Medicine]') AND type in (N'U'))
DROP TABLE [dbo].[Medicine]
GO
/****** Object:  Table [dbo].[MedicineChronicOrgan]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicineChronicOrgan]') AND type in (N'U'))
DROP TABLE [dbo].[MedicineChronicOrgan]
GO
/****** Object:  Table [dbo].[Devices]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Devices]') AND type in (N'U'))
DROP TABLE [dbo].[Devices]
GO
/****** Object:  Table [dbo].[Accounts]    Script Date: 09/08/2009 23:19:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Accounts]') AND type in (N'U'))
DROP TABLE [dbo].[Accounts]
GO
/****** Object:  StoredProcedure [dbo].[UpdateMedicineOrganConditionXRef]    Script Date: 09/08/2009 23:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateMedicineOrganConditionXRef]
GO
/****** Object:  Role [AdviserServerDBUser]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'AdviserServerDBUser')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'AdviserServerDBUser' AND type = 'R')
CREATE ROLE [AdviserServerDBUser]

END
GO
/****** Object:  StoredProcedure [dbo].[UpdateMedicineOrganConditionXRef]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[UpdateMedicineOrganConditionXRef] 
@MedicineID int, 
@OrganID int,
@ConditionID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

-- This procedure is created just to work around Entity Framework limitation with mapping tables
END
' 
END
GO
/****** Object:  Table [dbo].[Accounts]    Script Date: 09/08/2009 23:19:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Accounts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Accounts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PractitionerID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Accounts] ON
INSERT [dbo].[Accounts] ([ID], [PractitionerID]) VALUES (0, N'1ae03abe-aa7f-4e7f-9eaa-fe094b0df8b6')
INSERT [dbo].[Accounts] ([ID], [PractitionerID]) VALUES (2, N'13bc310b-4bf9-47e3-a82d-f8d1fdfff5d1')
SET IDENTITY_INSERT [dbo].[Accounts] OFF
/****** Object:  Table [dbo].[Devices]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Devices]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Devices](
	[ID] [uniqueidentifier] NOT NULL,
	[Version] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ManufDate] [datetime] NULL,
	[Status] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OwnerID] [uniqueidentifier] NULL,
	[Notes] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Devices] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[Devices] ([ID], [Version], [ManufDate], [Status], [OwnerID], [Notes]) VALUES (N'00000000-0000-0000-0000-000000000000', N'1.0.0', NULL, N'Online', N'13bc310b-4bf9-47e3-a82d-f8d1fdfff5d1', N'Default device for testing')
/****** Object:  Table [dbo].[MedicineChronicOrgan]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicineChronicOrgan]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MedicineChronicOrgan](
	[MedicineID] [int] NOT NULL,
	[OrganID] [int] NOT NULL,
 CONSTRAINT [PK_MedicineChronicOrgan] PRIMARY KEY CLUSTERED 
(
	[MedicineID] ASC,
	[OrganID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (1, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (1, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (2, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (2, 13)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (2, 16)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (2, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (3, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (3, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (4, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (4, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (4, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (4, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (5, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (5, 7)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (5, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (5, 11)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (5, 15)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (5, 16)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (5, 19)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (5, 23)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (5, 24)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (5, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (6, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (6, 19)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (6, 26)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (7, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (7, 3)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (7, 5)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (7, 14)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (7, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (7, 19)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (8, 7)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (8, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (8, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (8, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (9, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (9, 20)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (9, 26)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (10, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (13, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (13, 13)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (14, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (14, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (17, 5)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (17, 15)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (18, 19)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (20, 17)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (21, 17)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (22, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (23, 22)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (23, 28)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (24, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (24, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (26, 7)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (28, 11)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (29, 20)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (30, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (31, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (31, 12)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (31, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (31, 20)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (36, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (36, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (38, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (39, 16)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (39, 17)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (41, 2)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (41, 3)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (41, 16)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (42, 16)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (42, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (43, 7)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (43, 20)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (44, 2)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (45, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (45, 26)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (46, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (46, 26)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (47, 19)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (49, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (49, 15)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (49, 19)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (50, 15)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (50, 16)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (51, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (53, 2)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (54, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (56, 15)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (57, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (57, 26)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (58, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (58, 26)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (59, 2)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (59, 3)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (60, 15)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (60, 16)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (61, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (63, 12)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (63, 13)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (65, 7)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (67, 9)
GO
print 'Processed 100 total records'
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (68, 9)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (69, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (71, 9)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (72, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (74, 16)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (74, 17)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (77, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (81, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (84, 15)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (84, 16)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (85, 17)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (86, 19)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (89, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (91, 19)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (92, 19)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (93, 20)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (94, 11)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (95, 13)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (95, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (95, 20)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (96, 20)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (96, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (101, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (102, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (103, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (103, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (103, 14)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (104, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (104, 6)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (104, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (104, 16)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (104, 17)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (104, 23)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (104, 24)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 2)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 5)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 11)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 12)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 13)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 19)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 20)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 23)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 24)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (105, 26)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (106, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (107, 2)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (107, 3)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (107, 16)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (109, 19)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (110, 2)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (110, 3)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (110, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (110, 15)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (111, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (112, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (113, 13)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (115, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (115, 17)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (118, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (118, 9)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (121, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (121, 9)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (122, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (122, 11)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (123, 22)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (123, 28)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (125, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (126, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (126, 2)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (128, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (128, 3)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (128, 4)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (134, 15)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (138, 13)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (142, 7)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (142, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (143, 24)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (144, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (147, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (147, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (148, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (148, 26)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (150, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (154, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (154, 20)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (155, 24)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (161, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (161, 13)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (162, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (162, 15)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (164, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (165, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (165, 20)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (166, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (167, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (171, 24)
GO
print 'Processed 200 total records'
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (174, 7)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (175, 7)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (175, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (175, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (175, 13)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (178, 20)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (178, 21)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (179, 2)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (179, 3)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (179, 16)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (180, 23)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (180, 26)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (182, 13)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (184, 24)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (186, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (187, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (190, 7)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (190, 10)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (191, 23)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (192, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (193, 13)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (194, 24)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (195, 16)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (196, 23)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (197, 17)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (198, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (199, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (204, 13)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (205, 20)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (205, 25)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (209, 5)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (211, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (212, 7)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (215, 8)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (216, 19)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (217, 2)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (217, 3)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (220, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (220, 2)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (220, 3)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (220, 5)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (221, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (223, 1)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (223, 6)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (229, 18)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (229, 21)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (229, 23)
INSERT [dbo].[MedicineChronicOrgan] ([MedicineID], [OrganID]) VALUES (232, 1)
/****** Object:  Table [dbo].[Medicine]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Medicine]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Medicine](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL,
	[Description] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Direction] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Medicine_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Medicine] ON
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (1, N'Abopernol N', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (2, N'Abrotanum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (3, N'Acetylsalicylsaure-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (4, N'Acidum a-Ketoglutaricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (5, N'Acidum citricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (6, N'Acidum formicicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (7, N'Acidum fumaricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (8, N'Acidum lacticum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (9, N'Acidum nitricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (10, N'Acidum oxalicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (11, N'Acidum phosphoricum-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (12, N'Acidum succinicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (13, N'Acidum sulfuricum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (14, N'Acidum uricum-injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (15, N'Aconitum-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (16, N'Acontium-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (17, N'Adonis vernalis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (18, N'Adrenalin-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (19, N'Aesculus compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (20, N'Aesculus-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (21, N'Aesculus-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (22, N'Agaricus-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (23, N'Agnus castus-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (24, N'Albumoheel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (25, N'Alertis-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (26, N'Aloe-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (27, N'Amygdeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (28, N'Anacardium-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (29, N'Angeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (30, N'Antimonium crudum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (31, N'Apis-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (32, N'Areel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (33, N'Argentum nitricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (34, N'Arnica-Heel comp. ', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (35, N'Arnica-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (36, N'Arsenicum album-Injeel forte S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (37, N'Artis-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (38, N'Astricumeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (39, N'Ateria-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (40, N'Aurumheel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (41, N'Barijodeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (42, N'Baryum carbonicum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (43, N'Belladonna-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (44, N'Belladonna-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (45, N'Berberis vulgaris-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (46, N'Berberis-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (47, N'Bronkeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (48, N'Bryaconeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (49, N'Bryonia-Injeel forte S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (50, N'Cactus-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (51, N'Calcium carbonicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (52, N'Calcium fluoratum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (53, N'Calcium phosphoricum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (54, N'Calcium sulfuricum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (55, N'Calcoheel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (56, N'cAMP', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (57, N'Cantharis composium S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (58, N'Cantharis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (59, N'Carbo vegetabilis-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (60, N'Cardiacum-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (61, N'Carduus marianus-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (62, N'Cartilago suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (63, N'Causticum compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (64, N'Causticum-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (65, N'Ceanothus-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (66, N'Chamomilla-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (67, N'Chelidonium-Homaccord P', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (68, N'Chelidonium-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (69, N'China-Homaccord S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (70, N'Cholesterinum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (71, N'Chol-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (72, N'Cimicifuga-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (73, N'Cinnamomum-Homaccord N', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (74, N'Circulo-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (75, N'Cistus canadensis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (76, N'Cocculus-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (77, N'Cocculus-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (78, N'Coffea-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (79, N'Colchium compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (80, N'Colchium-Injeel forte S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (81, N'Colocynthis-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (82, N'Conicum-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (83, N'Cortison-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (84, N'Cralonin', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (85, N'Cruroheel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (86, N'Cuprum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (87, N'Curare-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (88, N'Cutisium', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (89, N'Cynara scolymus-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (90, N'Discompeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (91, N'Droperteel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (92, N'Drosera-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (93, N'Duclamara-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (94, N'Duodenoheel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (95, N'Echinacea angustifolia-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (96, N'Echinacea compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (97, N'Engystol', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (98, N'Eupatorium perfoliatum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (99, N'Euphorbium compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (100, N'Euphrasia-Injeel', NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (101, N'Ferrum metallicum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (102, N'Ferrum phosphoricum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (103, N'Ferrum-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (104, N'Funiculus umbilicalis suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (105, N'Galium-Heel comp.', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (106, N'Gelsemium-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (107, N'Ginkgo-comp.-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (108, N'Ginseng-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (109, N'Glandula suprarenalis suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (110, N'Glyoxal compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (111, N'Gnaphalium polycephalum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (112, N'Graphites-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (113, N'Graphites-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (114, N'Gripp-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (115, N'Hamamelis-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (116, N'Hamamelis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (117, N'Hekla lava-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (118, N'Hepar compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (119, N'Hepar suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (120, N'Hepar sulfuris-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (121, N'Hapeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (122, N'Histamin-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (123, N'Hormeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (124, N'Husteel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (125, N'Hydrastis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (126, N'Hyoscyamus-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (127, N'Hypericum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (128, N'Hypothalamus suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (129, N'Ignatia-Homoccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (130, N'Ipecacuanha-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (131, N'Iris-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (132, N'Jaborandi-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (133, N'Kalium bichromicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (134, N'Kalium carbonicum-Injeel forte N', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (135, N'Kalmia compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (136, N'Kalmia-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (137, N'Klimakt-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (138, N'Kreosotum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (139, N'Lachesis-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (140, N'Ledum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (141, N'Leptandra compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (142, N'Leptandra- Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (143, N'Lilium tigrinum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (144, N'Lithiumeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (145, N'Luffeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (146, N'Luffa operculata-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (147, N'Lycopodium-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (148, N'Lymphomyosot', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (149, N'Magnesium manganum phosphoricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (150, N'Magnesium phosphoricum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (151, N'Medorrhinum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (152, N'Medulla ossis suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (153, N'Melilotus-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (154, N'Mercurius-Heel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (155, N'Metro-Adnex-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (156, N'Mezereum-Homacord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (157, N'Momordica balsamina-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (158, N'Momordica compositum N', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (159, N'Myristica sebifera-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (160, N'Nareel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (161, N'Natrium muriaticum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (162, N'Natrium oxalaceticum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (163, N'Natrium pyruvicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (164, N'Natrium sulfuricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (165, N'Natrium-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (166, N'Nervoheel N', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (167, N'Neralgo-Rhem-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (168, N'Neuro-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (169, N'Nux vomica-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (170, N'Nux vomica-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (171, N'Nymeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (172, N'Osteel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (173, N'Paeonia-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (174, N'Pankreas suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (175, N'Petroleum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (176, N'Phosphor-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (177, N'Phosphorus-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (178, N'Phytolacca-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (179, N'Placenta suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (180, N'Plantago-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (181, N'Platinum metallicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (182, N'Podophyllum compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (183, N'Pulsatilla compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (184, N'Pulsatilla-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (185, N'Ranunculus-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (186, N'Reneel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (187, N'Rhododendroneel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (188, N'Rhododendron-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (189, N'Rhus tox-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (190, N'Ruta-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (191, N'Sabal-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (192, N'Sanguinaria-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (193, N'Schwef-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (194, N'Secale cornutum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (195, N'Selenium-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (196, N'Selenium-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (197, N'Sepia-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (198, N'Silicea-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (199, N'Sinusitis nosode-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (200, N'Sorinoheel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (201, N'Spascupreel', NULL, NULL)
GO
print 'Processed 200 total records'
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (202, N'Spigelon', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (203, N'Spongia-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (204, N'Staphisagria-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (205, N'Staphylococcus-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (206, N'Sticta-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (207, N'Stramonium-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (208, N'Streptococcus haemolyticus-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (209, N'Strumeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (210, N'Sulfur-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (211, N'Sulfur-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (212, N'Syzygium compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (213, N'Tabacum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (214, N'Tanacet-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (215, N'Taraxacum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (216, N'Tartephedreel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (217, N'Thuja-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (218, N'Tonico-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (219, N'Traumeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (220, N'Ubicoenzyme', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (221, N'Vaccininum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (222, N'Valerianaheel comp.', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (223, N'Valeriana-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (224, N'Ventigoheel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (225, N'Veratrum-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (226, N'Vincetoxicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (227, N'Visceel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (228, N'Viscum album-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (229, N'Viscum compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (230, N'Vomitusheel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (231, N'Ypsiloheel N', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (232, N'Zeel comp.', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (233, N'Zincum metallicum-Injeel', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Medicine] OFF
/****** Object:  Table [dbo].[Practitioners]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Practitioners]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Practitioners](
	[ID] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MidName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BusinessName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TypeOfPractice] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StreetAddress] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[City] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[StateProvince] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PostalCode] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Country] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Occupation] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PrimaryPhone] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SecondaryPhone] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Fax] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Password] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[StatusID] [smallint] NULL,
	[LastUpdDate] [datetime] NULL,
 CONSTRAINT [PK_Practitioner] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[ServerErrors]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ServerErrors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ServerErrors](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Time] [datetime] NOT NULL,
	[DeviceID] [uniqueidentifier] NULL,
	[AccountID] [uniqueidentifier] NULL,
	[VisitID] [uniqueidentifier] NULL,
	[ErrorMessage] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Details] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Category] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Type] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_ServerErrors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[ClientErrors]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClientErrors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ClientErrors](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Time] [datetime] NOT NULL,
	[DeviceID] [uniqueidentifier] NULL,
	[AccountID] [uniqueidentifier] NULL,
	[VisitID] [uniqueidentifier] NULL,
	[ErrorMessage] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Details] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Category] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Type] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_ClientErrors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteVisit]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteVisit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteVisit] 
	@ID [uniqueidentifier],
	@PatientID [uniqueidentifier]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DELETE FROM Visits
 WHERE ID = @ID

END
' 
END
GO
/****** Object:  Table [dbo].[Chakras]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Chakras]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Chakras](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShortDescription] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AboveDesc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BelowDesc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MandalaImage] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MandalaSound] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Recommendations] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ExpectedResults] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Influence] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Chakras] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (1, N'Muladhara', N'The Root Chakra', N'Muladhara or root chakra is related to instinct, security, survival and also to basic human potentiality. This centre is located in the region between the genitals and the anus. Although no endocrine organ is placed here, it is said to relate to the gonads and the adrenal medulla, responsible for the fight and flight response when survival is under threat. There is a muscle located in this region that controls ejaculation in the sexual act of the human male. A parallel is charted between the sperm cell and the ovum where the genetic code lies coiled and the kundalini. Muladhara is symbolised by a lotus with four petals and the colour red. Key issues involve sexuality, lust and obsession. Physically, Muladhara governs sexuality, mentally it governs stability, emotionally it governs sensuality, and spiritually it governs a sense of security.', N'Stress state. Overexcitement is possible. Sometimes there takes place sexual frustration. Fear and agitation without specific cause. Distress about loved ones, especially for children. High probability of a conflict situation. Fear of one’s own health.', N'Depressed state. Decrease of interest to life. Disappointment. Possibility of a conflict situation in a family that cannot be resolved for a long period of time. Easy fatigability. Inability to carry one’s point.', N'/Advisor.Modules.Interpretation;component/Resources/m1_Muladhara.jpg', N'Resources/m1_Muladhara.mp3', N'Meditate with Muladhara video in the mornings, 2-3 times a week for 30-40 minutes per session.', N'Energetic balance of Muladhara parameters lead to improved sleep and alertness while decreasing or removing stress, especially if stress is the result of a conflict situation.  Improvements toward quality of life include higher energy levels, discovering new interests and enhanced sexual activity.', N'Mandala Muladhara positively influences reproductive organs, particularly prostate in men and ovaries in women.  It also helps to improves functions of the large intestine.')
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (2, N'Swadhisthana', N'The Sacral Chakra', N'Swadhisthana, Svadisthana or adhishthana is located in the sacrum. It is considered to correspond to the testes or the ovaries that produce the various sex hormones involved in the reproductive cycle. Svadisthana is also considered to be related to, more generally, the genitourinary system and the adrenals. The Sacral Chakra is symbolized by a lotus with six petals, and corresponds to the colour orange. The key issues involving Svadisthana are relationships, violence, addictions, basic emotional needs, and pleasure. Physically, Svadisthana governs reproduction, mentally it governs creativity, emotionally it governs joy, and spiritually it governs enthusiasm.', N'Disordered way of living. Desire to achieve impossible. Excessive use of one’s possibilities including official misconduct. Excessive sought after different kinds of pleasures. Unrealistic evaluation of possibilities of people who stick around (exclusive standards to others). Desire to accomplish one’s purpose at any cost. Sometimes aggressiveness.', N'Decrease of material needs. Low self-esteem. Desire to stay away from events going on around. Inability to resist pressure from people with active way of life. State up to complete apathy. Grunge attitude, restraint. Addiction to alcohol and drugs.', N'/Advisor.Modules.Interpretation;component/Resources/m2_Svadisthana.jpg', N'Resources/m2_Svadisthana.mp3', N'Meditate with Swadhisthana video in the mornings, 2-3 times a week for 20-30 minutes per session.', N'Energetic balance of Swadhisthana parameters helps to restore an active way of life.  Overtime you will notice life becoming more organized, a chance to rid yourself of bad habits, with time your attitude towards yourself and others will change and you will become more aware of how others perceive you.', N'Mandala Swadhisthana positively influence the lymphatic system and helps to improve the function of the intestines and kidneys.')
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (3, N'Manipura', N'The Solar Plexus Chakra', N'Manipura or manipuraka is related to the metabolic and digestive systems. Manipura is believed to correspond to Islets of Langerhans,which are groups of cells in the pancreas, as well as the outer adrenal glands and the adrenal cortex. These play a valuable role in digestion, the conversion of food matter into energy for the body. Symbolised by a lotus with ten petals. The colour that corresponds to Manipura is yellow. Key issues governed by Manipura are issues of personal power, fear, anxiety, opinion-formation, introversion, and transition from simple or base emotions to complex. Physically, Manipura governs digestion, mentally it governs personal power, emotionally it governs expansiveness, and spiritually, all matters of growth.', N'Excessive scrupulousness and neatness that may irritate the others. Desire to obtrude one’s point of view up to a conflict situation creation. Pettiness in relations with financial partners.', N'Inability to concentrate and to bring an undertaking to an end. Communication failure from the side of others. Low self-esteem. Untidiness, in some cases sloppy manner. Inability to manage financial resources that may be expressed in form of avarice.', N'/Advisor.Modules.Interpretation;component/Resources/m3_Manipura.jpg', N'Resources/m3_Manipura.mp3', N'Meditate with Manipura video in the mornings as the sun rises, 2-3 times a week for 30-40 minutes per session.', N'Energetic balance of Manipura paramerters helps to inspire positive feelings.  Secondary problems or compulsions, which conflict your lifestyle will dissipate as your thoughts become lighter.  Overtime you will begin to focus on your contributions in life.  As your stressors reduce your outlook will improve and these feelings will have an overall positive reaction to your personal, financial and professional life.', N'Mandala Manipura positively influences the digestive system and improves function of liver.')
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (4, N'Anahata', N'The Heart Chakra', N'Anahata, or Anahata-puri, or padma-sundara is related to the thymus, located in the chest. The thymus is an element of the immune system as well as being part of the endocrine system. It produces the T cells responsible for fending off disease and may be adversely affected by stress. Anahata is symbolised by a lotus flower with twelve petals. Anahata is related to the colours green or pink. Key issues involving Anahata involve complex emotions, compassion, tenderness, unconditional love, equilibrium, rejection and well being. Physically Anahata governs circulation, emotionally it governs unconditional love for the self and others, mentally it governs passion, and spiritually it governs devotion.', N'Excessive credulity. Superfluous expression of love to others that may irritate them. Carelessness and even some stupidity in relations with people, specifically with those who may use you in their own lucrative aims. Sometimes unreasonable self-assurance on the brink of risk.', N'Undivided love to people, that arise incomprehension from their side. Because of this decline in strength, unwillingness to struggle for existence and to defend one’s own point of view. Disposition to depression. Lack of attention from the side of the others.', N'/Advisor.Modules.Interpretation;component/Resources/m4_Anahata.jpg', N'Resources/m4_Anahata.mp3', N'Meditate with Anahata video during day time hours, every other day for 40-60 minutes per session.', N'Energetic balance of Anahata parameters help to eliminate depression, if present.  Helps to improve social well being and relationships between people, emotional frustrations withdraw after sessions of meditation.  Feeling of satisfaction and quality of life are improved, as well.', N'Mandala Anahata positively influences the lungs and heart to improve their function, also improving blood circulation.')
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (5, N'Vishuddha', N'The Throat Chakra', N'Vishuddha (also Vishuddhi) is related to communication and growth through expression. This chakra is paralleled to the thyroid, a gland that is also in the throat and which produces thyroid hormone, responsible for growth and maturation. Symbolised by a lotus with sixteen petals. Vishudda is characterized by the color light or pale blue, or turquoise. It governs such issues as self-expression and communication, as discussed above. Physically, Vishuddha governs communication, emotionally it governs independence, mentally it governs fluent thought, and spiritually, it governs a sense of security.', N'Inclination to make decisions in haste without thinking out one’s actions. Hastiness. Desire to suppress the others including their initiative. Too high self-esteem. Genius with big ego. Proneness to conflict. Prepossession to persons of opposite sex.', N'Dissatisfaction with oneself. Inclination to gossip and to discuss disadvantages of the others. Unconscious desire to offend the other. Excessive pathologic diligence in carrying out some task getting to absurd. Passive attitude to persons of opposite sex.', N'/Advisor.Modules.Interpretation;component/Resources/m5_Vishuddha.jpg', N'Resources/m5_Vishuddha.mp3', N'Meditate with Vishuddha video during the afternoon, 3-4 times a week for 30-40 minutes per session.', N'Energetic balance of Vishuddha parameters help to remove tension and frustrations with yourself and others.  Helps you become calm and less argumentative.  Meditation will enhance your ability to focus and concentrate.', N'Mandala Vishuddha positively influences the lymph circulation, improves the immune system and assists with properly balancing the endocrine system.')
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (6, N'Ajna', N'The Brow Chakra', N'Ajna is linked to the pineal gland which may inform a model of its envisioning. The pineal gland is a light sensitive gland that produces the hormone melatonin which regulates sleep and awakening. Ajna is symbolised by a lotus with two petals, and corresponds to the colour white, indigo or deep blue. Ajna''s key issues involve balancing the higher & lower selves and trusting inner guidance. Ajna''s inner aspect relates to the access of intuition. Emotionally, Ajna deals with clarity on an intuitive level.', N'Very high self-esteem. Egoism. Desire to suppress other people by means of knowledge received previously. Considers himself above all and everyone. At the same time cannot concentrate on the most important issue, but puts a lot of attention and energy into non-significant details. His attitude to the people who are around him is prejudicial and sometimes cruel, the fact of which creates conflict situations.', N'Obsession, retreat into himself. As a rule people like that are concentrated on their own problems and cannot concentrate their ideas and attention. They have propensity for a feeling that soon they will have troubles. They are always up to some hanky-panky from those who are around them. Suspiciousness. Disposition to psychiatric disturbances. As a rule they live a world of illusion.', N'/Advisor.Modules.Interpretation;component/Resources/m6_Ajna.jpg', N'Resources/m6_Ajna.mp3', N'Meditate with Ajna video during the evening time, every other day for 30-40 minutes per session.', N'Energetic balance of Ajna parameters help to improve concentration and inspire positive feelings.  Secondary problems or compulsions, which conflict your lifestyle will dissipate as your thoughts become lighter.  Overtime you will feel more relaxed as your soul becomes more balanced.  As your stressors reduce, your love and trust towards people will improve.', N'Mandala Ajna positively influences the pituitary gland, increases resistance toward stress and balancing functions of the nervous system.')
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (7, N'Sahasrara', N'The Crown Chakra', N'Sahasrara is the chakra of pure consciousness. Its role may be envisioned somewhat similarly to that of the pituitary gland, which secretes hormones to communicate to the rest of the endocrine system and also connects to the central nervous system via the hypothalamus. The thalamus is thought to have a key role in the physical basis of consciousness. Symbolized by a lotus with one thousand petals, it is located at the crown of the head. Sahasrara is represented by the colour violet and it involves such issues as inner wisdom and the death of the body.Sahasrara''s inner aspect deals with the release of karma, physical action with meditation, mental action with universal consciousness and unity, and emotional action with "beingness".', N'Excessively expressed necessity to control and manage people. Necessity to be at the head of all events, sometimes intruding into private space of the people around him imposing his point of view that may cause reluctance and inappropriate response from their side. Dictator.', N'Superstitiousness. He is permanently in search of truth. Excessive credulity, naivety and narrow-mindedness. A very sensuous and vulnerable person demanding tender attitude. In some cases his approach to life is like an attitude of a teenager with distinctive for such and age maximalism.', N'/Advisor.Modules.Interpretation;component/Resources/m7_Sahasrara.jpg', N'Resources/m7_Sahasrara.mp3', N'Meditate with Sahasrara video before bedtime, 1-2 times a week, for 20-60 minutes per session.  Increasing frequency and duration is determined by your mood and amount of restlessness.', N'Energetic balance of Sahasrara parameters helps to relax and reenergize the body.  Helps to eliminate stress and depression, feelings of worry and fear dissipate.  Improved sleep will allow you to feel more rested in the morning.  After a period of time you will see improvements in your personal, social and professional life.', N'Mandala Sahasrara positively influences the brain, relaxes the nervous system and improves blood circulation in the entire body.  Over time may strengthen your intuition.')
/****** Object:  Table [dbo].[DeviceHistory]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeviceHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DeviceHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DeviceID] [uniqueidentifier] NOT NULL,
	[AccountID] [uniqueidentifier] NOT NULL,
	[Event] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Date] [datetime] NOT NULL,
	[Notes] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_DeviceHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[DeviceHistory] ON
INSERT [dbo].[DeviceHistory] ([ID], [DeviceID], [AccountID], [Event], [Date], [Notes]) VALUES (1, N'00000000-0000-0000-0000-000000000000', N'13bc310b-4bf9-47e3-a82d-f8d1fdfff5d1', N'Registration', CAST(0x00009C79001079E8 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[DeviceHistory] OFF
/****** Object:  Table [dbo].[Organs]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Organs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Organs](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL,
	[Gender] [int] NULL,
	[Description] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OrderKey] [int] NOT NULL,
	[Abbreviation] [nvarchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Organs] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (1, N'Nervous System', 0, NULL, 1, N'Ns')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (2, N'Brain', 0, NULL, 2, N'Bn')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (3, N'Cerebellum', 0, NULL, 3, N'Cb')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (4, N'Pituitary Gland', 0, NULL, 4, N'Py')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (5, N'Thyroid Gland', 0, NULL, 5, N'Td')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (6, N'Adrenal Gland', 0, NULL, 6, N'Al')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (7, N'Pancreas', 0, NULL, 7, N'Ps')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (8, N'Liver', 0, NULL, 8, N'Lr')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (9, N'Gallbladder', 0, NULL, 9, N'Gb')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (10, N'Stomach', 0, NULL, 10, N'Sc')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (11, N'Duodenum', 0, NULL, 11, N'Dm')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (12, N'Small Intestine', 0, NULL, 12, N'Si')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (13, N'Large Intestine', 0, NULL, 13, N'Li')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (14, N'Spleen', 0, NULL, 14, N'Sn')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (15, N'Heart', 0, NULL, 15, N'Ht')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (16, N'Arteries', 0, NULL, 16, N'As')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (17, N'Veins', 0, NULL, 17, N'Vs')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (18, N'Lymphatic System', 0, NULL, 18, N'Ls')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (19, N'Lungs', 0, NULL, 19, N'Lu')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (20, N'Tonsils', 0, NULL, 20, N'To')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (21, N'Mammary', 2, NULL, 21, N'Mm')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (22, N'Testicles', 1, NULL, 22, N'Ts')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (23, N'Prostate Gland', 1, NULL, 23, N'Pg')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (24, N'Uterus', 2, NULL, 24, N'Us')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (25, N'Kidney', 0, NULL, 25, N'Ks')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (26, N'Urinary Bladder', 0, NULL, 26, N'Ub')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (27, N'Thymus', 0, NULL, 27, N'Ty')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (28, N'Ovaries', 2, NULL, 22, N'Ov')
/****** Object:  Table [dbo].[Symptoms ]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Symptoms ]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Symptoms ](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL,
	[Description] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CS_AS NULL,
	[OrderKey] [int] NOT NULL,
 CONSTRAINT [PK_Symptoms _1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (1, N'Headache', NULL, 1)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (2, N'Vertigo', NULL, 2)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (3, N'Sore throat', NULL, 3)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (4, N'Chest pain', NULL, 4)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (5, N'Muscle pain', NULL, 5)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (6, N'Joint pain', NULL, 6)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (7, N'Abdominal pain', NULL, 7)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (8, N'Back pain', NULL, 8)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (9, N'Cough', NULL, 9)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (10, N'Rhinitis', NULL, 10)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (11, N'Urination problem', NULL, 11)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (12, N'Bleeding', NULL, 12)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (13, N'Diarrhea', NULL, 13)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (14, N'Sweating', NULL, 15)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (15, N'Skin disorders', NULL, 16)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (16, N'Fever', NULL, 17)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (17, N'Bulimia', NULL, 18)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (18, N'Anorexia', NULL, 19)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (19, N'Depression', NULL, 20)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (20, N'Anxiety', NULL, 21)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (21, N'Weakness', NULL, 22)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (22, N'Sleep disorders', NULL, 23)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (23, N'Nausea, vomiting', NULL, 24)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (24, N'Constipation', NULL, 14)
/****** Object:  Table [dbo].[OrganZoneDependency]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganZoneDependency]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OrganZoneDependency](
	[OrganID] [int] NOT NULL,
	[Zone1] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Zone2] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Zone3] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Zone4] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Zone5] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Zone6] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Zone7] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
END
GO
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (1, N'M', N'+', N'+', N'M', N'+', N'+', N'+')
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (2, N'+', NULL, NULL, NULL, NULL, NULL, N'+')
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (3, NULL, N'+', NULL, N'+', NULL, NULL, NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (4, N'+', N'+', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (5, NULL, N'+', N'+', NULL, NULL, NULL, NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (6, NULL, N'+', NULL, NULL, N'+', NULL, NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (7, NULL, N'+', NULL, N'M', N'+', NULL, NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (8, NULL, NULL, NULL, N'+', N'M', N'+', N'+')
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (9, NULL, NULL, NULL, N'+', N'+', NULL, NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (10, NULL, NULL, NULL, N'+', N'M', N'+', NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (11, NULL, NULL, NULL, NULL, N'+', NULL, NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (12, NULL, NULL, NULL, N'+', N'+', N'+', NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (13, NULL, NULL, NULL, NULL, N'+', N'M', N'+')
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (14, NULL, NULL, NULL, NULL, N'+', NULL, NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (15, NULL, NULL, NULL, N'+', NULL, NULL, NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (16, N'+', N'+', N'M', N'M', N'+', N'+', N'+')
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (17, N'+', N'+', N'+', N'+', N'M', N'M', N'+')
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (18, NULL, NULL, N'+', N'+', N'+', N'+', NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (19, NULL, NULL, N'+', N'M', N'+', NULL, NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (20, NULL, NULL, N'+', NULL, NULL, NULL, NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (21, NULL, N'+', NULL, N'M', NULL, NULL, N'+')
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (22, NULL, N'+', NULL, NULL, NULL, NULL, N'+')
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (23, NULL, N'+', NULL, NULL, NULL, N'+', N'M')
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (24, NULL, NULL, NULL, NULL, NULL, N'+', N'+')
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (25, NULL, NULL, NULL, NULL, N'+', N'+', NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (26, NULL, NULL, NULL, NULL, NULL, NULL, N'+')
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (27, NULL, NULL, N'+', N'+', NULL, NULL, NULL)
INSERT [dbo].[OrganZoneDependency] ([OrganID], [Zone1], [Zone2], [Zone3], [Zone4], [Zone5], [Zone6], [Zone7]) VALUES (28, NULL, N'+', NULL, NULL, NULL, NULL, N'+')
/****** Object:  StoredProcedure [dbo].[InsertUsageEntry]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertUsageEntry]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[InsertUsageEntry] 
  @AccountID uniqueidentifier,
  @VisitID uniqueidentifier, 
  @DeviceID uniqueidentifier
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

INSERT INTO [UsageData]
           ([AccountID],[VisitID],[DeviceID],[TransactionDate])
     VALUES
           (@AccountID, @VisitID, @DeviceID, GETUTCDATE())
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetConditionByValue]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetConditionByValue]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetConditionByValue]
	@Value int
AS
BEGIN
	
	SET NOCOUNT ON;
SELECT top 1 [ID]
      ,[Name]
      ,[MinRange]
      ,[MaxRange]
      ,[OrderKey]
	  ,[ColorCode]
	  ,[Description]
  FROM [Conditions]
	WHERE [MinRange] <= @Value AND [MaxRange] > @Value
	
END
' 
END
GO
/****** Object:  Table [dbo].[Conditions]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Conditions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Conditions](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL,
	[MinRange] [int] NULL,
	[MaxRange] [int] NULL,
	[OrderKey] [int] NOT NULL,
	[ColorCode] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL,
	[Description] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Conditions_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (0, N'Exhaustion', 0, 45, 1, N'#5e005d', N'Protective inhibition of organ function. High probability of irreversible metabolic disorder. Gas exchange and microcirculation disorder.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (2, N'Strain', 45, 60, 2, N'#7522e5', N'Organ function slowdown. Depletion of organ’s reserves.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (3, N'Functional Decrease', 60, 75, 3, N'#046bde', N'Organ trophism and lymphotrophy disorder. Metabolic disorders.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (4, N'Low Energy', 75, 90, 4, N'#35e5f5', N'Decrease of organ energy parameters. Metabolic processes slowing down.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (6, N'Normal', 90, 110, 5, N'#9fd998', N'Normal functioning')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (7, N'Reaction', 110, 125, 6, N'#f4eb56', N'Biochemical processes and methabolism activation. Microcirculation increase.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (9, N'Irritation', 125, 140, 7, N'#ff987c', N'Intoxication. Enzyme system operation increase. Biochemical processes activation on cell level.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (10, N'Agitation', 140, 155, 8, N'#c33e0a', N'Acute intoxication of organs and substances. Excretion of toxins. Sensibility ')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (11, N'Overload', 155, 200, 9, N'#8f0000', N'Critical toxic load on organs. Cell structures damage probability.')
/****** Object:  Table [dbo].[UsageData]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsageData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UsageData](
	[VisitID] [uniqueidentifier] NOT NULL,
	[AccountID] [uniqueidentifier] NOT NULL,
	[DeviceID] [uniqueidentifier] NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
 CONSTRAINT [PK_UsageData_1] PRIMARY KEY CLUSTERED 
(
	[VisitID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[MedicineSymptomXRef]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicineSymptomXRef]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MedicineSymptomXRef](
	[SymptomID] [int] NOT NULL,
	[MedicineID] [int] NOT NULL
)
END
GO
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 3)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 4)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 5)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 7)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 12)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 17)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 33)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 35)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 44)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 49)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 50)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 55)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 72)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 75)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 86)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 95)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 101)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 103)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 106)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 107)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 114)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 123)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 126)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 131)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 137)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 139)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 146)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 151)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 153)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 167)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 168)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 176)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 177)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 179)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 190)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 192)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 202)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 204)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 213)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 221)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 228)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (1, 233)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 4)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 5)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 7)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 10)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 12)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 18)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 19)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 21)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 22)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 25)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 33)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 39)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 40)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 41)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 42)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 44)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 53)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 59)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 69)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 72)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 74)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 82)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 84)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 86)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 90)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 107)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 110)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 126)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 133)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 137)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 139)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 168)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 172)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 175)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 179)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 195)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 204)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 213)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 224)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 228)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (2, 231)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 1)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 15)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 16)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 27)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 29)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 31)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 33)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 42)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 43)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 44)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 52)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 55)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 64)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 93)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 96)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 97)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 98)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 99)
GO
print 'Processed 100 total records'
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 100)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 105)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 114)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 120)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 126)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 134)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 139)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 148)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 154)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 177)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 183)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 198)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 203)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 205)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 208)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 210)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 219)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (3, 226)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 3)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 6)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 12)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 15)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 17)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 19)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 34)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 35)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 47)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 48)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 49)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 50)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 60)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 84)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 92)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 114)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 124)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 127)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 136)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 153)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 178)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 179)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 185)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 188)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 195)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 203)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (4, 221)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 3)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 4)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 5)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 6)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 7)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 8)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 12)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 22)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 24)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 34)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 35)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 37)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 39)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 43)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 44)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 48)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 49)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 51)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 55)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 63)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 69)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 74)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 75)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 76)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 77)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 80)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 83)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 85)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 93)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 96)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 98)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 101)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 103)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 104)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 105)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 106)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 109)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 110)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 111)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 114)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 115)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 122)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 127)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 144)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 150)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 156)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 162)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 163)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 167)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 169)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 177)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 178)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 183)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 187)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 190)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 204)
GO
print 'Processed 200 total records'
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 217)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 219)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 220)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 226)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (5, 233)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 2)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 3)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 4)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 5)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 6)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 7)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 14)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 16)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 17)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 26)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 30)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 35)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 37)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 45)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 46)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 48)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 49)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 52)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 55)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 62)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 64)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 69)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 72)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 75)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 77)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 80)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 83)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 90)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 98)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 103)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 104)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 105)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 106)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 109)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 110)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 111)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 114)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 117)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 127)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 135)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 136)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 139)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 140)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 144)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 152)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 156)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 162)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 163)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 164)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 167)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 169)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 172)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 187)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 188)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 189)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 192)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 198)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 204)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 205)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 206)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 208)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 219)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 220)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 221)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 232)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (6, 233)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 2)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 3)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 4)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 5)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 7)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 8)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 11)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 12)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 13)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 20)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 24)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 28)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 30)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 33)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 35)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 38)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 44)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 46)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 59)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 61)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 65)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 66)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 67)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 68)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 71)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 77)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 80)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 94)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 96)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 105)
GO
print 'Processed 300 total records'
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 118)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 121)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 125)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 126)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 141)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 142)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 157)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 158)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 171)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 174)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 182)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 194)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 201)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 212)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 215)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 225)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 229)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (7, 233)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 4)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 5)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 7)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 10)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 21)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 24)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 35)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 49)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 57)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 58)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 62)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 66)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 69)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 72)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 81)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 89)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 90)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 101)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 102)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 103)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 104)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 106)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 109)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 111)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 144)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 150)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 167)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 172)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 174)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 178)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 185)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 186)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 187)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 189)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 219)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (8, 232)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 6)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 12)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 15)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 18)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 27)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 30)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 34)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 35)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 47)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 49)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 66)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 82)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 86)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 91)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 92)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 97)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 98)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 100)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 105)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 109)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 114)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 122)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 124)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 126)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 130)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 134)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 151)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 164)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 169)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 177)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 178)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 198)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 199)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 203)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 206)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 213)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 216)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (9, 226)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 1)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 9)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 15)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 27)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 31)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 36)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 42)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 43)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 51)
GO
print 'Processed 400 total records'
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 52)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 55)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 75)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 93)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 97)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 98)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 99)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 100)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 105)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 110)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 125)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 133)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 134)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 145)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 146)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 148)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 151)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 160)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 161)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 198)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 199)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 200)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 205)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 206)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 208)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 221)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (10, 226)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 3)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 4)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 5)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 7)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 8)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 9)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 10)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 11)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 12)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 14)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 24)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 44)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 45)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 46)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 57)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 58)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 61)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 82)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 101)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 105)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 110)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 120)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 126)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 134)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 138)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 147)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 151)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 174)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 180)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 186)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 191)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 196)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 197)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 211)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 212)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 227)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (11, 233)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 3)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 9)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 11)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 13)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 20)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 21)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 26)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 35)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 55)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 57)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 61)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 73)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 101)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 116)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 125)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 127)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 130)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 143)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 147)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 155)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 171)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 181)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 182)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 194)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 197)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 219)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (12, 221)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 2)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 11)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 13)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 22)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 26)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 32)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 33)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 36)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 46)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 59)
GO
print 'Processed 500 total records'
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 63)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 94)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 95)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 105)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 110)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 133)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 142)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 143)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 164)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 174)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 176)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 213)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 225)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (13, 233)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 4)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 5)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 6)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 7)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 8)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 12)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 13)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 18)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 22)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 46)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 47)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 48)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 51)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 53)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 55)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 59)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 80)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 92)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 96)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 97)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 98)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 104)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 105)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 110)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 119)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 123)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 132)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 134)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 139)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 152)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 175)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 178)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 214)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 225)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (14, 229)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 1)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 4)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 5)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 7)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 9)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 13)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 20)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 27)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 30)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 36)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 43)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 51)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 54)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 55)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 63)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 64)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 75)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 83)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 88)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 94)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 95)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 96)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 104)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 105)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 109)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 110)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 112)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 113)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 115)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 116)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 118)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 119)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 120)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 122)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 123)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 125)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 127)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 133)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 138)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 139)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 148)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 149)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 154)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 156)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 159)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 162)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 163)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 165)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 173)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 174)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 175)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 177)
GO
print 'Processed 600 total records'
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 179)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 183)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 185)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 189)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 193)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 197)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 200)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 208)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 210)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 211)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 214)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 215)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (15, 228)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 14)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 16)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 22)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 24)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 29)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 31)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 34)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 43)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 47)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 48)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 57)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 85)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 96)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 97)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 98)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 102)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 105)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 139)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 145)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 159)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (16, 226)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (17, 44)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (17, 70)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (17, 112)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (17, 113)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (17, 118)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (17, 128)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (18, 2)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (18, 22)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (18, 123)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (18, 174)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (18, 221)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 11)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 18)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 23)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 25)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 30)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 33)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 36)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 56)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 109)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 110)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 123)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 128)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 129)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 137)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 151)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 166)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 168)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 169)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 176)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 178)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 181)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 184)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 195)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 209)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 218)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 220)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (19, 221)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 16)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 22)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 36)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 44)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 66)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 78)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 122)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 123)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 126)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 128)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 129)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 132)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 137)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 139)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 151)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 166)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 168)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 169)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 170)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 176)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 177)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 178)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 181)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 189)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 192)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 207)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 222)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 223)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 231)
GO
print 'Processed 700 total records'
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (20, 233)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 2)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 3)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 4)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 5)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 6)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 7)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 8)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 13)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 18)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 19)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 22)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 23)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 25)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 30)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 36)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 41)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 46)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 47)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 50)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 55)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 56)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 59)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 63)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 69)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 74)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 76)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 79)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 80)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 82)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 83)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 84)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 85)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 86)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 87)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 89)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 93)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 96)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 98)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 100)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 102)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 105)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 107)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 108)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 109)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 110)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 111)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 117)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 123)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 132)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 137)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 144)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 149)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 151)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 152)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 162)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 165)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 169)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 170)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 174)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 177)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 178)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 179)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 195)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 196)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 208)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 209)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 212)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 214)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 217)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 218)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 220)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 221)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 225)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 227)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 229)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (21, 232)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 11)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 16)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 41)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 66)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 78)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 110)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 128)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 129)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 137)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 151)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 166)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 168)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 169)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 176)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 184)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 207)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 222)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 223)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 231)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (22, 233)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 10)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 13)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 28)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 32)
GO
print 'Processed 800 total records'
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 38)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 59)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 68)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 77)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 105)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 122)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 130)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 131)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 141)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 151)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 169)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 170)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 175)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 214)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 225)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 229)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (23, 230)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 2)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 9)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 20)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 21)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 110)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 113)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 119)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 151)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 161)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 173)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 183)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 193)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 198)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 201)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 210)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 211)
INSERT [dbo].[MedicineSymptomXRef] ([SymptomID], [MedicineID]) VALUES (24, 217)
/****** Object:  Table [dbo].[MedicineOrganConditionXRef]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicineOrganConditionXRef]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MedicineOrganConditionXRef](
	[MedicineID] [int] NOT NULL,
	[OrganID] [int] NOT NULL,
	[ConditionID] [int] NOT NULL
)
END
GO
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 22, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 22, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 28, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 28, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 26, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (169, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (170, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 22, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 22, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 24, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 24, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 24, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 24, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 28, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (171, 28, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 4, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 4, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 4, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 4, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 5, 2)
GO
print 'Processed 100 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (172, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (173, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (173, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (173, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (173, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (173, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (173, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (173, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (173, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (173, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (173, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (173, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (173, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 7, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 7, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 7, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 7, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 9, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 9, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 9, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 9, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 11, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 11, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 11, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (174, 11, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (175, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (175, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (175, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (175, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (175, 26, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (175, 26, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (175, 26, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (175, 26, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (176, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (177, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (177, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (177, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (177, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (177, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (177, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (177, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (177, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (177, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (177, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (177, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (177, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 21, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 21, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 21, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 21, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 21, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 21, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 21, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 21, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 22, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 22, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 22, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 22, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 28, 0)
GO
print 'Processed 200 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 28, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 28, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (178, 28, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 3, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 3, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (179, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 23, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 23, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 23, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 23, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (180, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 22, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 22, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 24, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 24, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 24, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 24, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 28, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (181, 28, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 17, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (182, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 13, 2)
GO
print 'Processed 300 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (183, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 4, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 4, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 4, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 4, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 17, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 24, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 24, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 24, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (184, 24, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (185, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (185, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (185, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (185, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (185, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (185, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (185, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (185, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 25, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (186, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (187, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (187, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (187, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (187, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (187, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (187, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (187, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (187, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (188, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (188, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (188, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (188, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (188, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (188, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (188, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (188, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (189, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (189, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (189, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (189, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (189, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (189, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (189, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (189, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (189, 21, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (189, 21, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (189, 21, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (189, 21, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 14, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 14, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 14, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 14, 11)
GO
print 'Processed 400 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (190, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (191, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (191, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (191, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (191, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (191, 23, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (191, 23, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (191, 23, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (191, 23, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (191, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (191, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (191, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (191, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (192, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (192, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (192, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (192, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (192, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (192, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (192, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (192, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (192, 24, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (192, 24, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (192, 24, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (192, 24, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (193, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (193, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (193, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (193, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (193, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (193, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (193, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (193, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (193, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (193, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (193, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (193, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 22, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 22, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 24, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 24, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 24, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 24, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 28, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (194, 28, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (195, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (195, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (195, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (195, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (195, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (195, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (195, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (195, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (195, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (195, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (195, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (195, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 22, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 22, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 28, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 28, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (196, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 5, 4)
GO
print 'Processed 500 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 17, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 22, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 22, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 24, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 24, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 28, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 28, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (197, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (198, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (198, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (198, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (198, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (198, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (198, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (198, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (198, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (199, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (200, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 9, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 9, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 9, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 9, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 12, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 12, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 12, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 12, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (201, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 1, 9)
GO
print 'Processed 600 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 9, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 9, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 9, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 9, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (202, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 4, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 4, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 4, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 4, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (203, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 3, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 3, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 3, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (204, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 14, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 14, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 20, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 20, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 20, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 20, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (205, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (206, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (206, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (206, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (206, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (206, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (206, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (207, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (207, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (207, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (207, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (207, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (207, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (207, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (207, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (207, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 15, 4)
GO
print 'Processed 700 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 20, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 20, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 20, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 20, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (208, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (209, 4, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (209, 4, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (209, 4, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (209, 4, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (209, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (209, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (209, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (209, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 20, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 20, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 20, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 26, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 26, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (210, 26, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (211, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (212, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (213, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (214, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (214, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (214, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (214, 1, 11)
GO
print 'Processed 800 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (214, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (214, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (214, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (214, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (214, 12, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (214, 12, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (214, 12, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (214, 12, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 9, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 9, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 9, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 9, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 17, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 23, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 23, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 23, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (215, 23, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (216, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (216, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (216, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (216, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (216, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (216, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (216, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (216, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (216, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (216, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (216, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (216, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 3, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 3, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 4, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 4, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 4, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 4, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 21, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 21, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 21, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 21, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (217, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (218, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 8, 0)
GO
print 'Processed 900 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (219, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (220, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (220, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (220, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (220, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (220, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (220, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (220, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (220, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (220, 21, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (220, 21, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (220, 21, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (220, 21, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (221, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 5, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 5, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 5, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 5, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (222, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (223, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 3, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 3, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 16, 2)
GO
print 'Processed 1000 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (224, 17, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 12, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 12, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 12, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 12, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (225, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (226, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 21, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 21, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 21, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (227, 21, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 3, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 3, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 3, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 3, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 8, 3)
GO
print 'Processed 1100 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 21, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 21, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 21, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 21, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (228, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 21, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 21, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 21, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 21, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (229, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (230, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (230, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (230, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (230, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (230, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (230, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (230, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (230, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 5, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 5, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 5, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 5, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 22, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 22, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 28, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (231, 28, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 1, 11)
GO
print 'Processed 1200 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (232, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (233, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (1, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (1, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (1, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (1, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (1, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (1, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (1, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (1, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (1, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (1, 22, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (1, 22, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (1, 28, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (1, 28, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (2, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (2, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (2, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (2, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (2, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (3, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (3, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (3, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (3, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (3, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (3, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (3, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (3, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (3, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (3, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (3, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (3, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (3, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 14, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 14, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (4, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 11, 10)
GO
print 'Processed 1300 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (5, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (6, 25, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 3, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 14, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 14, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 20, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 20, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 21, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 21, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 21, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (7, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 7, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 7, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (8, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 18, 3)
GO
print 'Processed 1400 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 20, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 20, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 20, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 20, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 26, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 26, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 26, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 26, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (9, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (10, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 22, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 22, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 28, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (11, 28, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 3, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 3, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 7, 2)
GO
print 'Processed 1500 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (12, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (13, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (13, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (13, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (13, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (13, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (13, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (13, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (13, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 9, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 9, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 9, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 25, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (14, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (15, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (16, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 5, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 5, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (17, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 5, 3)
GO
print 'Processed 1600 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (18, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 14, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 14, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 14, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (19, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 17, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (20, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (21, 17, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 3, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 3, 4)
GO
print 'Processed 1700 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 12, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 12, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 12, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 12, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (22, 17, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 22, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 22, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 22, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 22, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 22, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 22, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 28, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 28, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 28, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 28, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 28, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (23, 28, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (24, 25, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 4, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 4, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 4, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 4, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (25, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 7, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 7, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 7, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 7, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 12, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 12, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 12, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 12, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 13, 3)
GO
print 'Processed 1800 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 22, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 22, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 24, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 24, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 24, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 24, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 28, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (26, 28, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 20, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 20, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 20, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (27, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (28, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (28, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (28, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (28, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (29, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (29, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (29, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (29, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (29, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (29, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (29, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (30, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (30, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (30, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (30, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (30, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (30, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 12, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 12, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 12, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 12, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 21, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 21, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 21, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 21, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 22, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 22, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 28, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (31, 28, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 6, 7)
GO
print 'Processed 1900 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (33, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (34, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (34, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (34, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (34, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (34, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (34, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (34, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (34, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (34, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (34, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (34, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (34, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 14, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 14, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 14, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (35, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (36, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (36, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (36, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (36, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (36, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (36, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (36, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (36, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (36, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (36, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (36, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (36, 25, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (37, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (37, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (37, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (37, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (37, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (37, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (37, 20, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (37, 20, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (38, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (38, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (38, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (38, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (38, 11, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (38, 11, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (38, 11, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (38, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (38, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (38, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (38, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 17, 10)
GO
print 'Processed 2000 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 17, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (39, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 3, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 3, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 3, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 3, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (40, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 3, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 3, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (41, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (42, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (42, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (42, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (42, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (42, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (42, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (42, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (42, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (42, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (42, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (42, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (42, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (42, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 9, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 9, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 9, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 9, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (43, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 18, 10)
GO
print 'Processed 2100 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (44, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 9, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 9, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 9, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 9, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (45, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 9, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 9, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 9, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 9, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (46, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (47, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (47, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (47, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (47, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (47, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (47, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (47, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (47, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (48, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (49, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (50, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (50, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (50, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (50, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (50, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (50, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (50, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (50, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (50, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (50, 16, 9)
GO
print 'Processed 2200 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (50, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (50, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (51, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (51, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (51, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (51, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (51, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (51, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (51, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (51, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (52, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (52, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (52, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (52, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (52, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (52, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (52, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (52, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (53, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (53, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (53, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (53, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (54, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (54, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (54, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (54, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (54, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (54, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (54, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (54, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (55, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 3, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 3, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 4, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 4, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 4, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 4, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 21, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 21, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 21, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 21, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 25, 3)
GO
print 'Processed 2300 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (56, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 25, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 26, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 26, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 26, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 26, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (57, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (58, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (58, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (58, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (58, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (58, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (58, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (58, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (58, 25, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (58, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (58, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (58, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (58, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 3, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 3, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 12, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 12, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 12, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 12, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (59, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (60, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (60, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (60, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (60, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (60, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (60, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (60, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (60, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (60, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (60, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (60, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (60, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 14, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (61, 14, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (62, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (62, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (62, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (62, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (63, 12, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (63, 12, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (63, 12, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (63, 12, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (63, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (63, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (63, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (63, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 14, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 14, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 14, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 14, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 26, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 26, 2)
GO
print 'Processed 2400 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (64, 26, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (65, 7, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (65, 7, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (65, 7, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (65, 7, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (65, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (65, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (65, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (65, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (66, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (66, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (66, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (66, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (66, 12, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (66, 12, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (66, 12, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (66, 12, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (66, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (66, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (66, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (66, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (67, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (67, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (67, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (67, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (67, 9, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (67, 9, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (67, 9, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (67, 9, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (67, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (67, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (67, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (67, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (68, 9, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (68, 9, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (68, 9, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (68, 9, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (68, 9, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (68, 9, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (68, 9, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (68, 9, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (69, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (69, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (69, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (69, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (69, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (69, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (69, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (69, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (69, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (69, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (69, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (69, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (70, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (70, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (70, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (70, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 9, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 9, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 9, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 9, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 9, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 9, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 9, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (71, 9, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (72, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (72, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (72, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (72, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (72, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (72, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (72, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (72, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (73, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (73, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (73, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (73, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 14, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 14, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 14, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 17, 2)
GO
print 'Processed 2500 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (74, 17, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (75, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (75, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (75, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (75, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (75, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (75, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (75, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (75, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (75, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (75, 20, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (75, 20, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (75, 20, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (76, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (76, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (76, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 24, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 24, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 24, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 24, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (77, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (78, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (78, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (78, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (78, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (78, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (78, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (78, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (78, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (78, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (78, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (78, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (78, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (79, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (79, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (79, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (79, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (79, 14, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (79, 14, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (79, 14, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (79, 14, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (79, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (79, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (79, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (79, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (80, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (81, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (81, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (81, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (81, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 2, 4)
GO
print 'Processed 2600 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 22, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 22, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 22, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 22, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 24, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 24, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 24, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 28, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 28, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 28, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (82, 28, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (83, 4, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (83, 4, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (83, 4, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (83, 4, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (83, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (83, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (83, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (83, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (83, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (83, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (83, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (83, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (84, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 17, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (85, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (86, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (86, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (86, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (86, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (86, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (86, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (86, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (86, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (86, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (86, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (86, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (87, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (87, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (87, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (87, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (88, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (88, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (88, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (88, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (89, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (89, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (89, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (89, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (89, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (89, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (89, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (89, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 6, 3)
GO
print 'Processed 2700 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (90, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (91, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (91, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (91, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (91, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (91, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (91, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (91, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (92, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 20, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 20, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 20, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 20, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (93, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 7, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 7, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 7, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 7, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 9, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 9, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 9, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 9, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 11, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 11, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 11, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 11, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (94, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 12, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 12, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 12, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 12, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (95, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 12, 7)
GO
print 'Processed 2800 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 12, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 12, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 12, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 22, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 22, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 25, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 28, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (96, 28, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 4, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 4, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 4, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 4, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (97, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (98, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (98, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (98, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (98, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (98, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (98, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (98, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (98, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (98, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (98, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (98, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (98, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 20, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 20, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 20, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 20, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (99, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (100, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (100, 16, 9)
GO
print 'Processed 2900 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (100, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (100, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (100, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (100, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (100, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (100, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (100, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (100, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (100, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (100, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 4, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 4, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 4, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 4, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 14, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 14, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 14, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 14, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 25, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 26, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 26, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 26, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 26, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (101, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (102, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (102, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (102, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (102, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (102, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (102, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (102, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (102, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 14, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 14, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 14, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 14, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 14, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 14, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 14, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 14, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 25, 3)
GO
print 'Processed 3000 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (103, 25, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 17, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 23, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 23, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 23, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 23, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 24, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 24, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 24, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (104, 24, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 25, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (105, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 4, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 4, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 4, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 4, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 4, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 4, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 4, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 4, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 16, 11)
GO
print 'Processed 3100 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (106, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 3, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 3, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 3, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 3, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 3, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 3, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (107, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (108, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (109, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 3, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 3, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 18, 0)
GO
print 'Processed 3200 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 20, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 20, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 20, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 20, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 21, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 21, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 21, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 21, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (110, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (111, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (111, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (111, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (111, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (111, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (111, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (111, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (111, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 6, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 6, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (112, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (113, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (114, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 8, 2)
GO
print 'Processed 3300 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 17, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 17, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (115, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (116, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (116, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (116, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (116, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (116, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (116, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (116, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (116, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (117, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (117, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (117, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (117, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (117, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (117, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (117, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (117, 16, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 9, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 9, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 9, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 9, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 9, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 9, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 9, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 9, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 23, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (118, 23, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (119, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (119, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (119, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (119, 9, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (119, 9, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (119, 9, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (119, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (119, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (119, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (119, 14, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (119, 14, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (119, 17, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (119, 17, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (120, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (120, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (120, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (120, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (120, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (120, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (120, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (120, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 9, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 9, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 9, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 9, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 9, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 9, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 9, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (121, 9, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 1, 10)
GO
print 'Processed 3400 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (122, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 4, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 4, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 4, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 5, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 5, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 21, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 21, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 21, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 21, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 22, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 22, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 28, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (123, 28, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (124, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (124, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (124, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (124, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (124, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (124, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 11, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 11, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 11, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 12, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 12, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 12, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 21, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 21, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 21, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (125, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 3, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 3, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 3, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 12, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 12, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 12, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 25, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (126, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (127, 16, 11)
GO
print 'Processed 3500 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 3, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 3, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 3, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 3, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 3, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 3, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 4, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 4, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 4, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 4, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 4, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 4, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 4, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 4, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (128, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (129, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (129, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (129, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (129, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (129, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (129, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (129, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (129, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (129, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (129, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (129, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (130, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (130, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (130, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (130, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (130, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (130, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (130, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (130, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (130, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (130, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (130, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (130, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (131, 7, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (131, 7, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (131, 7, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (131, 7, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (131, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (131, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (131, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (131, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (131, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (131, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (131, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (131, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (132, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (132, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (132, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (132, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (132, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (132, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (132, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (132, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (132, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (132, 16, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (133, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (133, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (133, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (133, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (133, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (133, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (133, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (133, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (133, 20, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (133, 20, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (133, 20, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (133, 20, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 24, 0)
GO
print 'Processed 3600 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 26, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 26, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 26, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (134, 26, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (135, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (135, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (135, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (135, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (136, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (136, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (136, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (136, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (136, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (136, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (136, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (136, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (136, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (136, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 22, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 22, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 28, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (137, 28, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 26, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 26, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 26, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (138, 26, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 22, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 22, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 22, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 22, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 28, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 28, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 28, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (139, 28, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (140, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (140, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (140, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (140, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (140, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (140, 18, 4)
GO
print 'Processed 3700 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (141, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (141, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (141, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (141, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (141, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (141, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (141, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (141, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (142, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (142, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (142, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (142, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (142, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (142, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (142, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (142, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (142, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (142, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (142, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (142, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (143, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (143, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (143, 13, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (143, 13, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (143, 24, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (143, 24, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (143, 24, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (143, 24, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (144, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (144, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (144, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (144, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (144, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (144, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (144, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (145, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (145, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (145, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (145, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (145, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (145, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (145, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (145, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (145, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (145, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (145, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (145, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (146, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (146, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (146, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (146, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (147, 25, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 8, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 8, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 20, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 20, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 20, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 20, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (148, 26, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 7, 3)
GO
print 'Processed 3800 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 10, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 10, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 17, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 17, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 17, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 17, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 19, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 21, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 21, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 21, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 21, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (149, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (150, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (150, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (150, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (150, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 4, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 4, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 4, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 4, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 25, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 25, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 25, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 26, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 26, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (151, 26, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 14, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 14, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 14, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 14, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 16, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 16, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (152, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (153, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (153, 2, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (153, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (153, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (153, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (153, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (153, 16, 7)
GO
print 'Processed 3900 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (153, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (153, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (153, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 18, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 20, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 20, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 20, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 20, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (154, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 4, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 4, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 6, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 6, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 13, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 21, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 21, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 21, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 22, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 22, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 22, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 22, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 24, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 24, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 24, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 28, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 28, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 28, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (155, 28, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (156, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (156, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (156, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (156, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (156, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (156, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (156, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (156, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (157, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (157, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (157, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (157, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (157, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (157, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (157, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (157, 24, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (158, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (158, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (158, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (158, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (158, 7, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (158, 7, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (158, 7, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (158, 7, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (158, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (158, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (158, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (158, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (159, 8, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (159, 8, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (159, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (159, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (159, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (159, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (159, 24, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (159, 24, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (160, 18, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (160, 18, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (160, 18, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (160, 18, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (160, 20, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (160, 20, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (160, 20, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (160, 20, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 10, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 10, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 13, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 13, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 13, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 13, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 13, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 19, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 19, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 19, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 22, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 22, 2)
GO
print 'Processed 4000 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 22, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 22, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 25, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 25, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 25, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 25, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 28, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 28, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 28, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (161, 28, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 21, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 21, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 21, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 21, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (162, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 2, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 2, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 2, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 3, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 3, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 3, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 3, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 5, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 5, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 5, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 5, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 7, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 7, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 7, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 7, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 15, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 15, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 15, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 15, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 21, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 21, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 21, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 21, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 23, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 23, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 23, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 23, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 24, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 24, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 24, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (163, 24, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (164, 10, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (164, 10, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (164, 10, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (164, 10, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (164, 12, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (164, 12, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (164, 12, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (164, 19, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (165, 8, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (165, 8, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (165, 8, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (165, 8, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (165, 18, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (165, 18, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (165, 18, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (165, 19, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (165, 19, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (165, 19, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 2, 4)
GO
print 'Processed 4100 total records'
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 6, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 6, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 6, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 6, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 11, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 11, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 11, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 11, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 15, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 15, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 15, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 15, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 16, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 16, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 16, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (166, 16, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (167, 1, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (167, 1, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (167, 1, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (167, 1, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (167, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (167, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (167, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (167, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 1, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 1, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 1, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 1, 11)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 2, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 2, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 2, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 2, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 4, 0)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 4, 2)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 4, 3)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 4, 4)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 5, 7)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 5, 9)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 5, 10)
INSERT [dbo].[MedicineOrganConditionXRef] ([MedicineID], [OrganID], [ConditionID]) VALUES (168, 5, 11)
/****** Object:  Table [dbo].[Patients]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Patients]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Patients](
	[ID] [uniqueidentifier] NOT NULL,
	[PractitionerId] [uniqueidentifier] NULL,
	[LastUpdDate] [datetime] NOT NULL,
	[FirstName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MidName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StreetAddress] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StateProvince] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PostalCode] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Occupation] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PrimaryPhone] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SecondaryPhone] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DOB] [datetime] NULL,
	[Sex] [nchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Height] [int] NULL,
	[Weight] [int] NULL,
	[Lifestyle] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MajorComplain] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MajorIllnessId] [int] NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[Visits]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Visits]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Visits](
	[ID] [uniqueidentifier] NOT NULL,
	[Date] [smalldatetime] NOT NULL,
	[LastUpdDate] [smalldatetime] NOT NULL,
	[PatientId] [uniqueidentifier] NOT NULL,
	[MeasurementData] [varbinary](max) NULL,
	[ExamResult] [varbinary](max) NULL,
	[Notes] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Visit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[OrganExamination]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganExamination]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OrganExamination](
	[ID] [uniqueidentifier] NOT NULL,
	[VisitID] [uniqueidentifier] NOT NULL,
	[OrganID] [int] NOT NULL,
	[Value] [int] NOT NULL,
	[Surgery] [bit] NOT NULL,
	[ChronicDisease] [bit] NOT NULL,
	[Removed] [bit] NOT NULL,
	[Implant] [bit] NOT NULL,
 CONSTRAINT [PK_OrganExamination] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[SymptomVisitXRef]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SymptomVisitXRef]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SymptomVisitXRef](
	[VisitID] [uniqueidentifier] NOT NULL,
	[SymptomID] [int] NOT NULL
)
END
GO
/****** Object:  Table [dbo].[Prescriptions]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Prescriptions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Prescriptions](
	[ID] [uniqueidentifier] NOT NULL,
	[VisitID] [uniqueidentifier] NOT NULL,
	[MedicineID] [int] NULL,
	[MedicineName] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CS_AS NULL,
	[Directions] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CS_AS NULL,
 CONSTRAINT [PK_Prescriptions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  StoredProcedure [dbo].[ExportInvoices]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportInvoices]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:  Alex Kovalov
-- Create date: 
-- Description: Returns transactions between dates
-- =============================================
-- Modifications:
-- 2010-03-02: removed DATEADD(minute, -2, @EndDate) AS [Invoice Date], left @EndDate only
CREATE PROCEDURE [dbo].[ExportInvoices] 
 -- Add the parameters for the stored procedure here
 @BeginDate smalldatetime, 
 @EndDate smalldatetime
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;


SELECT  
  ''INV'' + CAST(Accounts.ID as varchar) + ''-'' + CAST(MONTH(@BeginDate) as varchar) + ''-''+ CAST(YEAR(@BeginDate) as varchar) AS [Invoice Number],
  @EndDate AS [Invoice Date],
  Practitioners.LastName  + '' '' +  ISNULL(Practitioners.MidName,'''') + '' '' + Practitioners.FirstName + '' [Acc'' + CAST(Accounts.ID as varchar) +'']'' AS [Customer], 
  ''Patient Exam'' AS [Item],
  '''' AS [Financial Account],
  CAST (Visits.Date as varchar) + '': '' + Patients.LastName + '','' + Patients.FirstName  AS [Description],
         1 AS [Quantity],
      20 AS [Price],
   '''' AS [Item Tax Group],
         '''' AS [Salesperson]
       --dbo.Visits.Date
FROM         dbo.Practitioners INNER JOIN
                      dbo.Patients ON dbo.Practitioners.ID = dbo.Patients.PractitionerId INNER JOIN
                      dbo.Visits ON dbo.Patients.ID = dbo.Visits.PatientId INNER JOIN
                      dbo.Accounts ON dbo.Practitioners.Id = dbo.Accounts.PractitionerID
WHERE Visits.Date >= @BeginDate AND Visits.Date < @EndDate
ORDER BY [Invoice Number], Visits.Date

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[ExportCustomers]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportCustomers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:  Name
-- Create date: 
-- Description: 
-- =============================================
CREATE  PROCEDURE [dbo].[ExportCustomers] 
 -- Add the parameters for the stored procedure here
 @BeginDate smalldatetime, 
 @EndDate smalldatetime
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

 SELECT
  Accounts.ID AS [Customer Number],
  LastName  + '' '' +  ISNULL(MidName,'''') + '' '' + FirstName + '' [Acc'' + CAST(Accounts.ID as varchar) +'']'' AS [Customer Name], 
  StreetAddress As [Business Address 1],
  '''' AS [Business Address 2],
  City AS [Business City],
  StateProvince AS [Business State],
  PostalCode AS [Business Zip],
  Country AS [Business Country],
  PrimaryPhone AS [Business Phone],
  Fax AS [Business Fax],
  SecondaryPhone AS [Mobile Phone],
  LastName + '' '' + ISNULL(MidName, '''') + '' '' + FirstName  AS [Contact Name],
  '''' AS [Contact Title],
  PrimaryPhone AS [Contact Phone],
  Email AS [Contact Email],
  '''' AS [Customer Since],
  Email AS [Customer Email],
  '''' AS [Customer URL],
  '''' AS [Opening Balance],
  '''' AS [Balance As Of],
  '''' AS [Credit Limit],
  '''' AS [Comments],
  StreetAddress AS [Bill To Address 1],
  '''' AS [Bill To Address 2],
  City AS [Bill to City],
  StateProvince AS [Bill To State],
  PostalCode AS [Bill to Zip],
  Country AS [Bill to Country],
  StreetAddress AS [Ship To Address 1],
  '''' AS [Ship To Address 2],
  City AS [Ship To City],
  StateProvince AS [Ship To State],
  PostalCode AS [Ship To Zip],
  Country AS [Ship To Country],
  '''' AS [Tax Group]

 FROM Practitioners INNER JOIN
                      dbo.Accounts ON dbo.Practitioners.Id = dbo.Accounts.PractitionerID
 WHERE LastUpdDate >= @BeginDate AND LastUpdDate < @EndDate
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetDeviceByID]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDeviceByID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetDeviceByID]
	@id uniqueidentifier
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT [ID]
      ,[Version]
      ,[ManufDate]
      ,[Status]
      ,[OwnerID]
      ,[Notes]
  FROM [Devices]
	WHERE [Devices].[ID] = @id
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetMedicineByChronicOrgan]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMedicineByChronicOrgan]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetMedicineByChronicOrgan]
	@OrganID int
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT [MedicineID]
      ,[OrganID]
  FROM [MedicineChronicOrgan]
	WHERE [OrganID] = @OrganID
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetMedicineByOrganCondition]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMedicineByOrganCondition]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetMedicineByOrganCondition]
	@OrganID int,
	@ConditionValue int
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT [Medicine].[ID], [Medicine].[Name], [Medicine].[Description], [Medicine].[Direction]
  FROM [Conditions] INNER JOIN [MedicineOrganConditionXRef] ON [Conditions].[ID] = [MedicineOrganConditionXRef].[ConditionID]
  INNER JOIN  [Medicine] ON [Medicine].[ID] = [MedicineOrganConditionXRef].[MedicineID]
	WHERE [Conditions].[MinRange] <= @ConditionValue 
		AND [Conditions].[MaxRange] > @ConditionValue
		AND [MedicineOrganConditionXRef].[OrganID] = @OrganID
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetPractitionerByID]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPractitionerByID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetPractitionerByID]
	@ID uniqueidentifier
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT [ID]
      ,[FirstName]
      ,[LastName]
      ,[MidName]
      ,[BusinessName]
      ,[TypeOfPractice]
      ,[StreetAddress]
      ,[City]
      ,[PostalCode]
      ,[Country]
      ,[Occupation]
      ,[PrimaryPhone]
      ,[SecondaryPhone]
      ,[Fax]
      ,[Email]
      ,[Password]
      ,[StatusID]
      ,[LastUpdDate]
	  ,[StateProvince]
  FROM [Practitioners]
	WHERE [ID] = @ID
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetPractitionerByEmail]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPractitionerByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetPractitionerByEmail]
	@Email varchar(100)
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT [ID]
      ,[FirstName]
      ,[LastName]
      ,[MidName]
      ,[BusinessName]
      ,[TypeOfPractice]
      ,[StreetAddress]
      ,[City]
      ,[PostalCode]
      ,[Country]
      ,[Occupation]
      ,[PrimaryPhone]
      ,[SecondaryPhone]
      ,[Fax]
      ,[Email]
      ,[Password]
      ,[StatusID]
      ,[LastUpdDate]
	  ,[StateProvince]
  FROM [Practitioners]
	WHERE [Practitioners].[Email] = @Email
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetPassword]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetPassword]
	@Email nvarchar(100)
AS
BEGIN	
	SET NOCOUNT ON;

SELECT [Password]
  FROM [Practitioners]
	WHERE [Email] = @Email
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CreateNewVisit]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateNewVisit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[CreateNewVisit] 
@PatientID UNIQUEIDENTIFIER
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

declare @lastVisitID UNIQUEIDENTIFIER;
declare @newVisitID UNIQUEIDENTIFIER;
declare @visitDate datetime;

set @visitDate = getdate();
set @newVisitID = newid();

set @lastVisitID = (select top(1) ID from Visits
	where PatientID = @PatientID
	order by Date DESC);

	INSERT INTO [Visits]
			(ID
			,Date
			,LastUpdDate
			,PatientID)
		values
			(@newVisitID
			,@visitDate
			,@visitDate
			,@PatientID);

IF @lastVisitID IS NULL

	BEGIN
	INSERT INTO [OrganExamination]
				([ID]
			   ,[VisitID]
			   ,[OrganID]
			   ,[Value]
			   ,[Surgery]
			   ,[ChronicDisease]
			   ,[Removed]
			   ,[Implant])
		 select newid()
			   ,@newVisitID
			   ,[ID]
			   ,0
			   ,0
			   ,0
			   ,0
			   ,0
			from [Organs]  		
	END

ELSE

	BEGIN
	INSERT INTO [SymptomVisitXRef]
           ([VisitID]
           ,[SymptomID])
		select @newVisitID, [SymptomID] from [SymptomVisitXRef]
				where [VisitID] = @lastVisitID

	INSERT INTO [OrganExamination]
				([ID]
			   ,[VisitID]
			   ,[OrganID]
			   ,[Value]
			   ,[Surgery]
			   ,[ChronicDisease]
			   ,[Removed]
			   ,[Implant])
		 select newid()
			   ,@newVisitID
			   ,[OrganID]
			   ,0
			   ,[Surgery]
			   ,[ChronicDisease]
			   ,[Removed]
			   ,[Implant]
			from [OrganExamination]  
		where [VisitID] = @lastVisitID
	END

select @newVisitID as ID

END

/****** Object:  Table [dbo].[Chakras]    Script Date: 04/19/2009 21:33:42 ******/
SET ANSI_NULLS ON
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteMedicineSymptomXRef]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteMedicineSymptomXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DeleteMedicineSymptomXRef] 
@MedicineID int, 
@SymptomID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DELETE FROM MedicineSymptomXRef
      WHERE ([MedicineID] = @MedicineID
           AND [SymptomID] = @SymptomID)
   
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[InsertMedicineSymptomXRef]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertMedicineSymptomXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[InsertMedicineSymptomXRef] 
@MedicineID int, 
@SymptomID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


INSERT INTO [MedicineSymptomXRef]
           ([MedicineID]
           ,[SymptomID])
     VALUES
           (@MedicineID
           ,@SymptomID)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteMedicineOrganConditionXRef]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[DeleteMedicineOrganConditionXRef] 
@MedicineID int, 
@OrganID int,
@ConditionID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DELETE FROM [MedicineOrganConditionXRef]
	WHERE([MedicineID] = @MedicineID
		AND [OrganID] = @OrganID
		AND [ConditionID] = @ConditionID)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[InsertMedicineOrganConditionXRef]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[InsertMedicineOrganConditionXRef] 
@MedicineID int, 
@OrganID int,
@ConditionID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


INSERT INTO [MedicineOrganConditionXRef]
           ([MedicineID]
           ,[OrganID]
		   ,[ConditionID])
     VALUES
           (@MedicineID
           ,@OrganID
		   ,@ConditionID)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[InsertVisit]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertVisit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertVisit] 
	@ID [uniqueidentifier] ,
	@VisitDate [smalldatetime],
	@LastUpdDate [smalldatetime],
	@PatientId [uniqueidentifier],
	@MeasurementData [varbinary] (max),
	@ExamResult [varbinary] (max),
	@Notes [nvarchar] (max),
	@Deleted [bit]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [Visits]
			(ID
			,Date
			,LastUpdDate
			,PatientID
			,MeasurementData
			,ExamResult
			,Notes
			,Deleted)
		values
			(@ID
			,@VisitDate
			,@LastUpdDate
			,@PatientId
			,@MeasurementData
			,@ExamResult
			,@Notes
			,@Deleted);
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateVisit]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateVisit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateVisit] 
	@ID [uniqueidentifier] ,
	@Date [smalldatetime],
	@LastUpdDate [smalldatetime],
	@PatientId [uniqueidentifier],
	@MeasurementData [varbinary] (max),
	@ExamResult [varbinary] (max),
	@Notes [nvarchar] (max),
	@Deleted [bit]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE [Visits]
   SET [Date] = @Date
      ,[LastUpdDate] = getdate()
      ,[PatientId] = @PatientId
      ,[MeasurementData] = @MeasurementData
      ,[ExamResult] = @ExamResult
      ,[Notes] = @Notes
	  ,[Deleted] = @Deleted
 WHERE ID = @ID

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteSymptomVisitXRef]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteSymptomVisitXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DeleteSymptomVisitXRef] 
@VisitID uniqueidentifier, 
@SymptomID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DELETE FROM SymptomVisitXRef
      WHERE ([VisitID] = @VisitID
           AND [SymptomID] = @SymptomID)
   
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[InsertSymptomVisitXRef]    Script Date: 09/08/2009 23:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertSymptomVisitXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[InsertSymptomVisitXRef] 
@VisitID uniqueidentifier, 
@SymptomID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


INSERT INTO [SymptomVisitXRef]
           ([VisitID]
           ,[SymptomID])
     VALUES
           (@VisitID
           ,@SymptomID)
END
' 
END
GO
/****** Object:  Default [DF_Conditions_OrderKey]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Conditions_OrderKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[Conditions]'))
Begin
ALTER TABLE [dbo].[Conditions] ADD  CONSTRAINT [DF_Conditions_OrderKey]  DEFAULT ((100)) FOR [OrderKey]

End
GO
/****** Object:  Default [DF_OrganExamination_ID]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] ADD  CONSTRAINT [DF_OrganExamination_ID]  DEFAULT (newid()) FOR [ID]

End
GO
/****** Object:  Default [DF_OrganExamination_Value]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_Value]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] ADD  CONSTRAINT [DF_OrganExamination_Value]  DEFAULT ((0)) FOR [Value]

End
GO
/****** Object:  Default [DF_Table_1_Operation]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Table_1_Operation]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] ADD  CONSTRAINT [DF_Table_1_Operation]  DEFAULT ((0)) FOR [Surgery]

End
GO
/****** Object:  Default [DF_Table_1_HronicDesease]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Table_1_HronicDesease]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] ADD  CONSTRAINT [DF_Table_1_HronicDesease]  DEFAULT ((0)) FOR [ChronicDisease]

End
GO
/****** Object:  Default [DF_OrganExamination_Removed]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_Removed]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] ADD  CONSTRAINT [DF_OrganExamination_Removed]  DEFAULT ((0)) FOR [Removed]

End
GO
/****** Object:  Default [DF_OrganExamination_Implant]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_Implant]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] ADD  CONSTRAINT [DF_OrganExamination_Implant]  DEFAULT ((0)) FOR [Implant]

End
GO
/****** Object:  Default [DF_Organs_OrderKey]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Organs_OrderKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[Organs]'))
Begin
ALTER TABLE [dbo].[Organs] ADD  CONSTRAINT [DF_Organs_OrderKey]  DEFAULT ((100)) FOR [OrderKey]

End
GO
/****** Object:  Default [DF_Patient_Id]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Patient_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
Begin
ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patient_Id]  DEFAULT (newid()) FOR [ID]

End
GO
/****** Object:  Default [DF_Patient_LastUpdDate]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Patient_LastUpdDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
Begin
ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patient_LastUpdDate]  DEFAULT (getutcdate()) FOR [LastUpdDate]

End
GO
/****** Object:  Default [DF_Patients_Deleted]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Patients_Deleted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
Begin
ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patients_Deleted]  DEFAULT ((0)) FOR [Deleted]

End
GO
/****** Object:  Default [DF_Practitioner_Id]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Practitioner_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Practitioners]'))
Begin
ALTER TABLE [dbo].[Practitioners] ADD  CONSTRAINT [DF_Practitioner_Id]  DEFAULT (newid()) FOR [ID]

End
GO
/****** Object:  Default [DF_Practitioners_StatusId]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Practitioners_StatusId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Practitioners]'))
Begin
ALTER TABLE [dbo].[Practitioners] ADD  CONSTRAINT [DF_Practitioners_StatusId]  DEFAULT ((1)) FOR [StatusID]

End
GO
/****** Object:  Default [DF_Prescriptions_ID]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Prescriptions_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Prescriptions]'))
Begin
ALTER TABLE [dbo].[Prescriptions] ADD  CONSTRAINT [DF_Prescriptions_ID]  DEFAULT (newid()) FOR [ID]

End
GO
/****** Object:  Default [DF_Symptoms _OrderKey]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Symptoms _OrderKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[Symptoms ]'))
Begin
ALTER TABLE [dbo].[Symptoms ] ADD  CONSTRAINT [DF_Symptoms _OrderKey]  DEFAULT ((100)) FOR [OrderKey]

End
GO
/****** Object:  Default [DF_Visits_Id]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Visits_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Visits]'))
Begin
ALTER TABLE [dbo].[Visits] ADD  CONSTRAINT [DF_Visits_Id]  DEFAULT (newid()) FOR [ID]

End
GO
/****** Object:  Default [DF_Visits_Date]    Script Date: 09/08/2009 23:19:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Visits_Date]') AND parent_object_id = OBJECT_ID(N'[dbo].[Visits]'))
Begin
ALTER TABLE [dbo].[Visits] ADD  CONSTRAINT [DF_Visits_Date]  DEFAULT (getutcdate()) FOR [Date]

End
GO
/****** Object:  ForeignKey [FK_MedicineOrganConditionXRef_Conditions]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicineOrganConditionXRef_Conditions]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicineOrganConditionXRef]'))
ALTER TABLE [dbo].[MedicineOrganConditionXRef]  WITH CHECK ADD  CONSTRAINT [FK_MedicineOrganConditionXRef_Conditions] FOREIGN KEY([ConditionID])
REFERENCES [dbo].[Conditions] ([ID])
GO
ALTER TABLE [dbo].[MedicineOrganConditionXRef] CHECK CONSTRAINT [FK_MedicineOrganConditionXRef_Conditions]
GO
/****** Object:  ForeignKey [FK_MedicineOrganConditionXRef_Medicine]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicineOrganConditionXRef_Medicine]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicineOrganConditionXRef]'))
ALTER TABLE [dbo].[MedicineOrganConditionXRef]  WITH CHECK ADD  CONSTRAINT [FK_MedicineOrganConditionXRef_Medicine] FOREIGN KEY([MedicineID])
REFERENCES [dbo].[Medicine] ([ID])
GO
ALTER TABLE [dbo].[MedicineOrganConditionXRef] CHECK CONSTRAINT [FK_MedicineOrganConditionXRef_Medicine]
GO
/****** Object:  ForeignKey [FK_MedicineOrganConditionXRef_Organs]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicineOrganConditionXRef_Organs]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicineOrganConditionXRef]'))
ALTER TABLE [dbo].[MedicineOrganConditionXRef]  WITH CHECK ADD  CONSTRAINT [FK_MedicineOrganConditionXRef_Organs] FOREIGN KEY([OrganID])
REFERENCES [dbo].[Organs] ([ID])
GO
ALTER TABLE [dbo].[MedicineOrganConditionXRef] CHECK CONSTRAINT [FK_MedicineOrganConditionXRef_Organs]
GO
/****** Object:  ForeignKey [FK_MedicineSymptomXRef_Medicine]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicineSymptomXRef_Medicine]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicineSymptomXRef]'))
ALTER TABLE [dbo].[MedicineSymptomXRef]  WITH CHECK ADD  CONSTRAINT [FK_MedicineSymptomXRef_Medicine] FOREIGN KEY([MedicineID])
REFERENCES [dbo].[Medicine] ([ID])
GO
ALTER TABLE [dbo].[MedicineSymptomXRef] CHECK CONSTRAINT [FK_MedicineSymptomXRef_Medicine]
GO
/****** Object:  ForeignKey [FK_MedicineSymptomXRef_Symptoms ]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MedicineSymptomXRef_Symptoms ]') AND parent_object_id = OBJECT_ID(N'[dbo].[MedicineSymptomXRef]'))
ALTER TABLE [dbo].[MedicineSymptomXRef]  WITH CHECK ADD  CONSTRAINT [FK_MedicineSymptomXRef_Symptoms ] FOREIGN KEY([SymptomID])
REFERENCES [dbo].[Symptoms ] ([ID])
GO
ALTER TABLE [dbo].[MedicineSymptomXRef] CHECK CONSTRAINT [FK_MedicineSymptomXRef_Symptoms ]
GO
/****** Object:  ForeignKey [FK_OrganExamination_Organs]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganExamination_Organs]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
ALTER TABLE [dbo].[OrganExamination]  WITH CHECK ADD  CONSTRAINT [FK_OrganExamination_Organs] FOREIGN KEY([OrganID])
REFERENCES [dbo].[Organs] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrganExamination] CHECK CONSTRAINT [FK_OrganExamination_Organs]
GO
/****** Object:  ForeignKey [FK_OrganExamination_Visits1]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganExamination_Visits1]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
ALTER TABLE [dbo].[OrganExamination]  WITH CHECK ADD  CONSTRAINT [FK_OrganExamination_Visits1] FOREIGN KEY([VisitID])
REFERENCES [dbo].[Visits] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrganExamination] CHECK CONSTRAINT [FK_OrganExamination_Visits1]
GO
/****** Object:  ForeignKey [FK_Patient_Practitioner]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Patient_Practitioner]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
ALTER TABLE [dbo].[Patients]  WITH CHECK ADD  CONSTRAINT [FK_Patient_Practitioner] FOREIGN KEY([PractitionerId])
REFERENCES [dbo].[Practitioners] ([ID])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Patients] CHECK CONSTRAINT [FK_Patient_Practitioner]
GO
/****** Object:  ForeignKey [FK_Prescriptions_Visit]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Prescriptions_Visit]') AND parent_object_id = OBJECT_ID(N'[dbo].[Prescriptions]'))
ALTER TABLE [dbo].[Prescriptions]  WITH CHECK ADD  CONSTRAINT [FK_Prescriptions_Visit] FOREIGN KEY([VisitID])
REFERENCES [dbo].[Visits] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Prescriptions] CHECK CONSTRAINT [FK_Prescriptions_Visit]
GO
/****** Object:  ForeignKey [FK_SymptomVisitXRef_Symptoms ]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SymptomVisitXRef_Symptoms ]') AND parent_object_id = OBJECT_ID(N'[dbo].[SymptomVisitXRef]'))
ALTER TABLE [dbo].[SymptomVisitXRef]  WITH CHECK ADD  CONSTRAINT [FK_SymptomVisitXRef_Symptoms ] FOREIGN KEY([SymptomID])
REFERENCES [dbo].[Symptoms ] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SymptomVisitXRef] CHECK CONSTRAINT [FK_SymptomVisitXRef_Symptoms ]
GO
/****** Object:  ForeignKey [FK_SymptomVisitXRef_Visit]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SymptomVisitXRef_Visit]') AND parent_object_id = OBJECT_ID(N'[dbo].[SymptomVisitXRef]'))
ALTER TABLE [dbo].[SymptomVisitXRef]  WITH CHECK ADD  CONSTRAINT [FK_SymptomVisitXRef_Visit] FOREIGN KEY([VisitID])
REFERENCES [dbo].[Visits] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SymptomVisitXRef] CHECK CONSTRAINT [FK_SymptomVisitXRef_Visit]
GO
/****** Object:  ForeignKey [FK_UsageData_Devices]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UsageData_Devices]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsageData]'))
ALTER TABLE [dbo].[UsageData]  WITH CHECK ADD  CONSTRAINT [FK_UsageData_Devices] FOREIGN KEY([DeviceID])
REFERENCES [dbo].[Devices] ([ID])
GO
ALTER TABLE [dbo].[UsageData] CHECK CONSTRAINT [FK_UsageData_Devices]
GO
/****** Object:  ForeignKey [FK_Visit_Patient]    Script Date: 09/08/2009 23:19:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Visit_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[Visits]'))
ALTER TABLE [dbo].[Visits]  WITH CHECK ADD  CONSTRAINT [FK_Visit_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patients] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Visits] CHECK CONSTRAINT [FK_Visit_Patient]
GO
