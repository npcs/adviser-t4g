﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Modules.Interpretation
{
    using Advisor.Modules.Interpretation.Views;
    using Advisor.Modules.Interpretation.PresentationModels;

    public interface IHumanBodyPresenter
    {
        IHumanBodyView View { get; set; }
    }

    public class HumanBodyPresenter : IHumanBodyPresenter
    {
        public HumanBodyPresenter(IHumanBodyView view, IHumanBodyViewModel model)
        {
            View = view;
            view.Model = model;
        }

        public IHumanBodyView View { get; set; }
    }
}
