﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Advisor.Common.DataModel;
using Advisor.Modules.Catalog.Views;

namespace Advisor.Modules.Catalog.Controllers
{
    public interface ICatalogController
    {
        void OnPatientSelected(Patient patient);
        void SetPatientListView(Patient patient);
        void SetPatientDetailsView(Patient patient);
    }
}
