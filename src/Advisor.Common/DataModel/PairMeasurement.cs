﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Common.DataModel
{
    public interface IDataSeries
    {
        double Mean { get; }
        double Maximum { get; }
        double Minimum { get; }
        ushort SampleRate { get; }
        void Add(double sample);
        void Clear();
    }

    [Serializable]
    public class PairMeasurement : IDataSeries
    {
        private double mean;
        private double maximum;
        private double minimum;
        private ushort sampleRate;
        private List<double> samples;

        public PairMeasurement()
        {
            maximum = 0;
            minimum = 0;
            samples = new List<double>();
        }

        public double Mean
        {
            get { return samples.Average(); }
            set { mean = value; }
        }

        public double Maximum
        {
            get { return maximum; }
            set { maximum = value; }
        }

        public double Minimum
        {
            get { return minimum; }
            set { minimum = value; }
        }

        public ushort SampleRate
        {
            get { return sampleRate; }
            set { sampleRate = value; }
        }

        public List<double> Samples
        {
            get { return samples; }
            set { samples = value; }
        }

        /// <summary>
        /// Adds new sample to underlying data series.
        /// </summary>
        /// <param name="sample"></param>
        public void Add(double sample)
        {
            samples.Add(sample);
            if (samples.Count == 0)
            {
                Minimum = sample;
                Maximum = sample;
            }
            else
            {
                if (sample < Minimum)
                    Minimum = sample;
                if (sample > Maximum)
                    Maximum = sample;
            }
        }

        public void Clear()
        {
            samples.Clear();
            Minimum = 0;
            Maximum = 0;
        }

        public override string ToString()
        {
            return "Pair: mean=" + Mean.ToString() + ", max=" + Maximum.ToString();
        }
    }
}
