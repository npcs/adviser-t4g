﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;

using Adviser.Client.Core.Presentation;
using Microsoft.Practices.Unity;
using System.Media;
using System.Windows.Media;

namespace Advisor.Modules.Interpretation
{
    public class MandalaTherapyPresenter : IMandalaTherapyPresenter
    {
        private IChildWindow _window;
        private DispatcherTimer _timer;
        private MediaPlayer _player;
        private TimeSpan _playStartPosition = new TimeSpan(0, 0, 0);

        public MandalaTherapyPresenter(IChildWindow window, IMandalaTherapyView view)
        {
            _window = window;
            _window.WindowTitle = "Mandala Therapy";
            _window.IsFullScreen = true;
            ((System.Windows.Window)_window).Closing += new System.ComponentModel.CancelEventHandler(MandalaTherapyPresenter_Closing);
            View = view;

            _timer = new DispatcherTimer();
            _player = new MediaPlayer();
            _player.MediaEnded += new EventHandler(_player_MediaEnded);
        }

        /// <summary>
        /// Starts playing loaded file from the beginning.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _player_MediaEnded(object sender, EventArgs e)
        {
            _player.Position = _playStartPosition;
            _player.Play();
        }

        /// <summary>
        /// Stops timer and media if window was closed from the UI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MandalaTherapyPresenter_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Stop();
        }

        public IMandalaTherapyView View { get; set; }

        public void Show(IMandalaViewModel mandala)
        {
            View.Model = mandala;
            _window.SetContent(View);
            _window.CanCloseOnEsc = true;

            _player.Open(new Uri(mandala.MelodySource, UriKind.Relative));

            _timer.Interval = new TimeSpan(0, mandala.TimeLength, 0);
            _timer.Tick += new EventHandler(OnTimerCallback);
            _timer.IsEnabled = true;
            _timer.Start();

            _player.Play();

            _window.ShowAsDialog();
        }

        /// <summary>
        /// Closes window on timer expiration.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnTimerCallback(object source, EventArgs e)
        {
            _window.CloseWindow();
        }

        private void Stop()
        {
            _timer.Stop();
            _player.Stop();
            _timer.IsEnabled = false;
        }
    }
}
