﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Unity;

using Adviser.Common.Domain;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Presentation;
using Adviser.Client.Core.Events;
using Advisor.Modules.Readout.PresentationModels;

namespace Advisor.Modules.Readout.Views
{
    
    public class ReadoutPresenter : IReadoutPresenter
    {
        private readonly IEventAggregator _eventAggregator;
        private IReadoutView _view;
        private IPairDetailsView _pairDetailsView;
        private IUnityContainer _container;

        public ReadoutPresenter(IEventAggregator eventAggregator, 
            IReadoutView readoutView, 
            IDataMatrixViewModel matrixViewModel,
            IPairDetailsView pairDetailsView,
            IUnityContainer container)
        {
            _container = container;
            _eventAggregator = eventAggregator;

            _view = readoutView;
            _pairDetailsView = pairDetailsView;

            _view.Model = matrixViewModel;

            //_view.SaveFile += new EventHandler<RoutedEventArgs>(OnSaveFile);
            //_view.Reset += new EventHandler<RoutedEventArgs>(OnReset);

            //_eventAggregator.GetEvent<MeasurementLoadedEvent>().Subscribe(OnMeasurementLoaded, ThreadOption.UIThread);

            matrixViewModel.CellSelected += new EventHandler<DataEventArgs<IDataCellViewModel>>(matrixViewModel_CellSelected);

            _eventAggregator.GetEvent<NavigationEvent>().Subscribe(OnNavigationEvent, 
                ThreadOption.PublisherThread, false, 
                e => e.EventName == "Readout");

            ShowMatrix(matrixViewModel);
            ShowPairDetails();
        }

        private void OnNavigationEvent(NavigationEventArgs e)
        {

            AppointmentSession session = _container.Resolve<AppointmentSession>();
            AdviserMeasurement measurement = null;

            byte[] rawData = session.Visit.MeasurementData;
            if (rawData != null && rawData.Length > 0)
            {
                BinaryFormatter fmt = new BinaryFormatter();
                using (MemoryStream stream = new MemoryStream(rawData))
                {
                    stream.Seek(0, SeekOrigin.Begin);
                    measurement = (AdviserMeasurement)fmt.Deserialize(stream);
                }

                _view.Model.UpdateData(measurement.Data);
            }
            else
            {
                _view.Model.Message = "No measurement data present in the visit.";
            }
        }

        void matrixViewModel_CellSelected(object sender, DataEventArgs<IDataCellViewModel> e)
        {
            _pairDetailsView.Model = e.Value;
        }

        private void ShowMatrix(IDataMatrixViewModel viewModel)
        {
            _view.ShowMatrix(viewModel.View);
        }

        private void ShowPairDetails()
        {
            _view.ShowPairDetails(_pairDetailsView);
        }

        private void OnSaveFile(object sender, RoutedEventArgs e)
        {
            
        }

        private void OnMeasurementLoaded(MeasurementEventArgs e)
        {
            _pairDetailsView.Reset();
        }

        private void OnReset(object sender, RoutedEventArgs e)
        {
            _pairDetailsView.Reset();
            _eventAggregator.GetEvent<MeasurementResetEvent>().Publish(true);
        }

        public void HidePairDetails()
        {
        }

        public IReadoutView View
        {
            get { return _view; }
        }
    }
}
