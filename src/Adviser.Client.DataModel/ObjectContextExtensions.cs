﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Objects;

namespace Adviser.Client.DataModel
{
    public static class ObjectContextExtensions
    {
        /// <summary>
        /// This method will load entity by key value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context"></param>
        /// <param name="propertyName"></param>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public static T LoadByKey<T>(this ObjectContext context, String propertyName, Object keyValue)
        {
            IEnumerable<KeyValuePair<string, object>> entityKeyValues =
               new KeyValuePair<string, object>[] { new KeyValuePair<string, object>(propertyName, keyValue) };
            string entitySetName = context.GetType().Name + "." + typeof(T).Name + "Set";
            EntityKey key = new EntityKey(entitySetName, entityKeyValues);

            object result = null;
            try
            {
                result = context.GetObjectByKey(key);
            }
            catch (ObjectNotFoundException)
            {
                result = null;
            }

            if (result != null)
                return (T)result;
            else
                return default(T);
        }

        public static void DeleteByKey<T>(this ObjectContext context, String propertyName, Object keyValue)
        {
            T entity = LoadByKey<T>(context, propertyName, keyValue);
            if (entity != null)
                context.DeleteObject(entity);
        }
    }
}
