﻿using System;
using System.Collections.Generic;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Wpf;

using Adviser.Client.DataModel;
using Adviser.Client.Core.Presentation;
using Advisor.Modules.Interpretation.PresentationModels;

namespace Advisor.Modules.Interpretation
{
    public class ResultsDetailPrintPresenter : PrintPresenter
    {
        public ResultsDetailPrintPresenter(ResultsDetailPrintView printableView, IHumanBodyViewModel model)
        {
            Name = "Results Details";
            OrderKey = 30;
            SelectedToPrint = true;
            PrintableView = printableView;
            PrintableView.DataContext = model;
        }
    }
}
