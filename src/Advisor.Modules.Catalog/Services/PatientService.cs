﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections.ObjectModel;
using Advisor.Common.DataModel;
using System.Data.Objects;
using System.Data.Objects.DataClasses;

using System.Data;


namespace Advisor.Modules.Catalog.Services
{

    class PatientService : IPatientService
    {
        #region Constructor
        private ClientDataModelEntities _dataContext;

        public PatientService()
        {
            _dataContext = new ClientDataModelEntities();
        }
        #endregion

        #region IPatientService implementation

        public EntityCollection<Visit> RetriveVisitList(Patient CurrentParient)
        {
            if (!CurrentParient.Visit.IsLoaded)
            {
                CurrentParient.Visit.Load();
            }
            return CurrentParient.Visit;
        }

        public EntityCollection<Anamnesis> RetriveAnamnesisList(Patient CurrentParient)
        {
            if (!CurrentParient.Anamnesis.IsLoaded)
            {
                CurrentParient.Anamnesis.Load();
            }
            return CurrentParient.Anamnesis;
        }

        public Practitioner RetrivePractitioner()
        {
            return _dataContext.Practitioner.First();
        }

        public EntityCollection<Patient> RetrivePatientList(Practitioner CurrentPractitioner)
        {
            if (!CurrentPractitioner.Patient.IsLoaded)
            {
                CurrentPractitioner.Patient.Load();
            }
            return CurrentPractitioner.Patient;
        }

        public void DeleteObject(Object EntityObject)
        {
            _dataContext.DeleteObject(EntityObject);
            _dataContext.SaveChanges();
        }

        public void AddPatient(Practitioner CurrentPractitioner)
        {
            Patient newPatient = new Patient();
            newPatient.Id = Guid.NewGuid();
            newPatient.FirstName = "FirstName";
            newPatient.LastName = "LastName";
            newPatient.LastUpdDate = DateTime.Now.ToUniversalTime();
            
            CurrentPractitioner.Patient.Add(newPatient);
            _dataContext.SaveChanges();
        }

        public void AddVisit(Patient CurrentPatient)
        {
            Visit newVisit = new Visit();
            newVisit.Id = Guid.NewGuid();
            newVisit.LastUpdDate = DateTime.Now.ToUniversalTime();
            newVisit.Date = DateTime.Now.ToUniversalTime();

            CurrentPatient.Visit.Add(newVisit);
            _dataContext.SaveChanges();
        }

        public void AddAnamnez(Patient CurrentPatient)
        {
            Anamnesis newAnamnesis = new Anamnesis();
            newAnamnesis.Id = Guid.NewGuid();
            newAnamnesis.LastUpdDate = DateTime.Now.ToUniversalTime();

            CurrentPatient.Anamnesis.Add(newAnamnesis);
            _dataContext.SaveChanges();
        }

        #endregion

        public ObjectResult<Patient> RetrievePatients()
        {
            ObjectQuery<Patient> PatientQuery = _dataContext.Patient;
            return PatientQuery.Execute(MergeOption.AppendOnly);
        }

        public ObjectResult<Patient> GetPatient(string id)
        {
            ObjectQuery<Patient> PatientQuery = _dataContext.Patient.Where("id=@PatientID", new ObjectParameter("PatientID", "f4dd85cf-ce1e-e2d1-3171-650938abd2b7"));
            return PatientQuery.Execute(MergeOption.AppendOnly);
        }

        public void AddPatient(Patient patient)
        {
            _dataContext.AddToPatient(patient);
        }

        public void UpdateDatabase()
        {
            _dataContext.SaveChanges();
        }
    }
}
