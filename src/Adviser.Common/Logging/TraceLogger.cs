﻿using System;
using System.Diagnostics;

namespace Adviser.Common.Logging
{
    public class TraceLogger<TEntry> : Logger<TEntry> where TEntry : ILogEntry, new()
    {
        /// <summary>
        /// Send logEntry information to System.Diagnostics.Trace.
        /// </summary>
        /// <param name="logEntry">A LogEntry.</param>
        /// <returns>true upon success, false upon failure.</returns>
        protected internal override bool DoLog(TEntry logEntry)
        {
            try
            {
                Trace.WriteLine(
                    Formatter.AsString(logEntry),
                    (logEntry.Category != null ? logEntry.Category.ToString() : null));
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Create a new instance of TraceLogger.
        /// </summary>
        public TraceLogger()
            : base()
        {
        }
    }
}
