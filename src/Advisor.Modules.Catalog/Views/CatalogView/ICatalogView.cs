﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Advisor.Modules.Catalog.Views
{
    public interface ICatalogView
    {
        void SetDefaultView(IPatientListView patientListView);
        object CurrentContent { get; set; }
    }
}
