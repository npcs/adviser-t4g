﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using System.Data;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Diagnostics;
using System.Data.OleDb;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Adviser.Common.Domain;
using Adviser.Server.Services;
using Adviser.Server.DataModel;

namespace Adviser.Server.Tests
{
    [TestClass]
    public class ImportServerData
    {
        [TestMethod]
        public void ImportMedicineToSymptomData()
        {
            string filename = @"C:\Projects\IKO\Advisor Docs\Requirements\Medicine-Symptom.xls";
            DataSet ds = new DataSet();

            try
            {
                using (OleDbConnection con = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + ";Extended Properties=Excel 8.0"))
                {
                    con.Open();
                    OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", con);
                    adapter.Fill(ds);
                }

                //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                //{
                //    DataRow row = ds.Tables[0].Rows[i];
                //    for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                //    {
                //        Debug.Write(row[j].ToString() + ",");
                //    }
                //    Debug.WriteLine(Environment.NewLine);
                //}

                DBService adviserDB = new DBService();
                IEnumerable<Medicine> allMeds = adviserDB.GetAllMedicine().AsEnumerable();
                IEnumerable<Symptom> allSymptoms = adviserDB.GetAllSymptoms().AsEnumerable();

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {//- for each medicine

                    DataRow row = ds.Tables[0].Rows[i];
                    string medicineName = row[0].ToString();
                    Debug.Write(medicineName + ": ");
                    for (int j = 1; j < ds.Tables[0].Columns.Count && j < 25; j++)
                    {//- iterate over symptoms
                        string data = row[j].ToString().Replace(" ", "");
                        Debug.Write(data + ",");
                        if (data != String.Empty)
                        {
                            string columnName = ds.Tables[0].Columns[j].ColumnName;
                            try
                            {
                                Symptom symptom = allSymptoms.Single(s => s.Name.ToLower() == columnName.ToLower());
                                Medicine med = allMeds.Single(m => m.Name.ToUpper() == medicineName.ToUpper());
                                symptom.Medicine.Add(med);
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine(ex.ToString());
                                throw;
                            }
                        }
                    }
                    Debug.WriteLine(Environment.NewLine);
                }

                adviserDB.SaveChanges();

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Loads medicine to organ condition mappings from excel spreadsheet to the database.
        /// </summary>
        //[TestMethod]
        public void ImportMedicineToOrganConditionData()
        {
            string filename = @"C:\Projects\IKO\Advisor Docs\Requirements\Medicine-Organ-v2.xls";
            DataSet ds = new DataSet();

            Dictionary<int, int> condIdMapping = new Dictionary<int, int>();
            condIdMapping.Add(1, 0);
            condIdMapping.Add(2, 2);
            condIdMapping.Add(3, 3);
            condIdMapping.Add(4, 4);
            //- normal has no entries
            condIdMapping.Add(5, 7);
            condIdMapping.Add(6, 9);
            condIdMapping.Add(7, 10);
            condIdMapping.Add(8, 11);

            try
            {
                using (OleDbConnection con = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + ";Extended Properties=Excel 8.0"))
                {
                    con.Open();
                    OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", con);
                    adapter.Fill(ds);
                }

                DBService adviserDB = new DBService();
                IQueryable<Medicine> allMeds = adviserDB.GetAllMedicine();
                IQueryable<Organ> allOrgans = adviserDB.GetAllOrgans();
                IQueryable<Condition> allConditions = adviserDB.GetAllConditions();

                string[] split = { "-" };
                for (int i = 0; i < ds.Tables[0].Rows.Count && i < 233; i++)
                {//- for each medicine

                    DataRow row = ds.Tables[0].Rows[i];
                    string medicineName = row[0].ToString();
                    
                    Debug.Write(medicineName + ": ");

                    Medicine med = allMeds.FirstOrDefault(m => m.Name.ToUpper() == medicineName.ToUpper());

                    if (med == null)
                        Debug.WriteLine("Medicine not found!");

                    for (int j = 1; j < ds.Tables[0].Columns.Count && j < 235; j++)
                    {//- iterate over organ conditions (columns)

                        string data = row[j].ToString().Replace(" ", "");
                        Debug.Write(data + ",");

                        if (data != String.Empty)
                        {
                            string columnName = ds.Tables[0].Columns[j].ColumnName;
                            string[] parts = columnName.Split(split, StringSplitOptions.None);

                            string organName = parts[1].ToLower();
                            string conditionNumber = parts[0];
                            int conditionID = -1;
                            try
                            {
                                if (Int32.TryParse(conditionNumber, out conditionID))
                                {//- write record into MedicineOrganConditionXref table
                                    conditionID = condIdMapping[conditionID];
                                    Condition cond = allConditions.FirstOrDefault(c => c.ID == conditionID);
                                    if (cond == null)
                                        Debug.WriteLine("Condition " + cond + " not found!");

                                    if (organName == "testis# ovary#")
                                    {//- need two records for this one
                                        Organ organ1 = allOrgans.FirstOrDefault(o => o.Name.ToLower() == "testicles");
                                        Organ organ2 = allOrgans.FirstOrDefault(o => o.Name.ToLower() == "ovaries");
                                        if (organ1 == null || organ2 == null)
                                            Debug.WriteLine("Organ " + organName + " not found!");

                                        MedicineOrganConditionXRef xref1 = MedicineOrganConditionXRef.CreateMedicineOrganConditionXRef(med.ID, organ1.ID, cond.ID);
                                        MedicineOrganConditionXRef xref2 = MedicineOrganConditionXRef.CreateMedicineOrganConditionXRef(med.ID, organ2.ID, cond.ID);
                                        med.MedicineOrganConditionXRef.Add(xref1);
                                        med.MedicineOrganConditionXRef.Add(xref2);
                                        organ1.MedicineOrganConditionXRef.Add(xref1);
                                        organ2.MedicineOrganConditionXRef.Add(xref2);
                                        cond.MedicineOrganConditionXRef.Add(xref1);
                                        cond.MedicineOrganConditionXRef.Add(xref2);
                                        adviserDB.AddMedicineOrganCondition(xref1);
                                        adviserDB.AddMedicineOrganCondition(xref2);
                                    }
                                    else
                                    {
                                        Organ organ = allOrgans.FirstOrDefault(o => o.Name.ToLower() == organName);
                                        if (organ == null)
                                            Debug.WriteLine("Organ " + organName + " not found!");

                                        MedicineOrganConditionXRef xref = MedicineOrganConditionXRef.CreateMedicineOrganConditionXRef(med.ID, organ.ID, cond.ID);
                                        med.MedicineOrganConditionXRef.Add(xref);
                                        organ.MedicineOrganConditionXRef.Add(xref);
                                        cond.MedicineOrganConditionXRef.Add(xref);
                                        adviserDB.AddMedicineOrganCondition(xref);
                                    }
                                }
                                else
                                {//- chronic organ processing
                                    if (conditionNumber == "hd")
                                    {//- write record into MedicineChronicOrgan table
                                        if (organName == "testis# ovary#")
                                        {
                                            Organ organ1 = allOrgans.FirstOrDefault(o => o.Name.ToLower() == "testicles");
                                            Organ organ2 = allOrgans.FirstOrDefault(o => o.Name.ToLower() == "ovaries");
                                            if (organ1 == null || organ2 == null)
                                                Debug.WriteLine("Organ " + organName + " not found!");

                                            MedicineChronicOrgan medChronic1 = MedicineChronicOrgan.CreateMedicineChronicOrgan(med.ID, organ1.ID);
                                            MedicineChronicOrgan medChronic2 = MedicineChronicOrgan.CreateMedicineChronicOrgan(med.ID, organ2.ID);
                                            adviserDB.AddMedicineChronicOrgan(medChronic1);
                                            adviserDB.AddMedicineChronicOrgan(medChronic2);
                                        }
                                        else
                                        {
                                            Organ organ = allOrgans.FirstOrDefault(o => o.Name.ToLower() == organName);
                                            if (organ == null)
                                                Debug.WriteLine("Organ " + organName + " not found!");

                                            MedicineChronicOrgan medChronic = MedicineChronicOrgan.CreateMedicineChronicOrgan(med.ID, organ.ID);
                                            adviserDB.AddMedicineChronicOrgan(medChronic);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine(ex.ToString());
                                throw;
                            }
                        }
                    }
                    Debug.WriteLine(Environment.NewLine);
                }
                adviserDB.SaveChanges();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                throw;
            }
        }
    }
}
