﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;

using Adviser.Common.Domain;
using Adviser.Client.Core.Presentation;
using Adviser.Common.Collections;
using Adviser.Client.Core.Services;
using Adviser.Client.Core.DeviceControl;
using Advisor.Modules.DeviceControl.Views;
using Advisor.Modules.DeviceControl.Services;

namespace Advisor.Modules.DeviceControl
{
    public class DeviceControlModule : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public DeviceControlModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            RegisterViewsAndServices();

            IRegion region = _regionManager.Regions[RegionNames.ContentRegion];

            IDiagnosticsPresenter diagPresenter = _container.Resolve<DiagnosticsPresenter>();
            region.Add(diagPresenter.View, "IDiagnosticsView");
            region.Deactivate(diagPresenter.View);
        }

        protected void RegisterViewsAndServices()
        {
            ConfigurationDataService configurationService = new ConfigurationDataService();
            DeviceSettings settings =  configurationService.GetDeviceSettings();

            _container.RegisterType<IDiagnosticsView, DiagnosticsView>();
            _container.RegisterType<IDiagnosticsPresenter, DiagnosticsPresenter>();
            _container.RegisterType<IElectrodeConnectionsView, ElectrodeConnectionsView>();
            _container.RegisterType<IElectrodeConnectionsPresenter, ElectrodeConnectionsPresenter>();
            _container.RegisterType<IDeviceControlView, DeviceControlView>();
            _container.RegisterType<IDeviceControlPresenter, DeviceControlPresenter>();
            _container.RegisterType<IDeviceControlPresentationModel, DeviceControlPresentationModel>();
            _container.RegisterType<IDeviceControlService, DeviceControlService>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IElectrodeConnectionsViewModel, ElectrodeConnectionsViewModel>();

            
            if(settings.EmulateDevice)
                _container.RegisterType<IDeviceDriver, DeviceEmulator>();
            else
                _container.RegisterType<IDeviceDriver, DeviceDriver>();
            
            _container.RegisterType<IConfigurationDataService, ConfigurationDataService>();
            _container.RegisterType<IDeviceManager, DeviceManager>();

        }
    }
}
