﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using Adviser.Server.DataModel;
using Adviser.Common.Logging;

namespace Adviser.Server.Core.Logging
{
    public class ServerDbLogger<TEntry> : Logger<TEntry> where TEntry : ILogEntry, new()
    {
        protected override bool DoLog(TEntry logEntry)
        {
            bool result = false;
            AdviserDBEntities dbContext = new AdviserDBEntities();
            try
            {
                ServerError error = ServerError.CreateServerError(0, logEntry.Time);
                AdviserLogEntry adviserLogEntry = logEntry as AdviserLogEntry;
                error.AccountID = adviserLogEntry.PractitionerId;
                error.DeviceID = adviserLogEntry.DeviceId;
                error.VisitID = adviserLogEntry.VisitId;

                error.Category = logEntry.Category;
                error.Details = logEntry.Details.ToString();
                error.ErrorMessage = logEntry.ErrorMessage;
                error.Type = logEntry.LogItemType;
                error.Status = "Created";
                error.Time = logEntry.Time;

                dbContext.AddToServerErrorSet(error);
                dbContext.SaveChanges();
            }
            catch
            {
                result = false;
            }
            finally
            {
                dbContext.Dispose();
            }

            return result;
        }
    }
}
