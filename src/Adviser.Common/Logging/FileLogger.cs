﻿using System;
using System.IO;
using System.Text;

namespace Adviser.Common.Logging
{
    public class FileLogger<TEntry> : Logger<TEntry> where TEntry : ILogEntry, new()
    {
        /// <summary>
        /// The name of the file to which this Logger is writing.
        /// </summary>
        private string _fileName;

        /// <summary>
        /// Gets and sets the file name.
        /// </summary>
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        /// <summary>
        /// Create a new instance of FileLogger.
        /// </summary>
        protected FileLogger()
            : base()
        {
        }
        /// <summary>
        /// Create a new instance of FileLogger.
        /// </summary>
        /// <param name="fileName">The name of the file to which this Logger should write.</param>
        public FileLogger(string fileName)
            : this()
        {
            FileName = fileName;
        }

        /// <summary>
        /// Create a new FileStream.
        /// </summary>
        /// <returns>The newly created FileStream.</returns>
        private FileStream CreateFileStream()
        {
            return new FileStream(FileName, FileMode.Append);
        }

        /// <summary>
        /// Get the FileStream.
        /// Create the directory structure if necessary.
        /// </summary>
        /// <returns>The FileStream.</returns>
        private FileStream GetFileStream()
        {
            try
            {
                return CreateFileStream();
            }
            catch (DirectoryNotFoundException)
            {
                Directory.CreateDirectory((new FileInfo(FileName)).DirectoryName);
                return CreateFileStream();
            }
        }

        /// <summary>
        /// Create a new StreamWriter.
        /// </summary>
        /// <returns>A new StreamWriter.</returns>
        private StreamWriter GetStreamWriter()
        {
            return new StreamWriter(GetFileStream());
        }

        /// <summary>
        /// Write the String to the file.
        /// </summary>
        /// <param name="s">The String representing the LogEntry being logged.</param>
        /// <returns>true upon success, false upon failure.</returns>
        protected override bool WriteToLog(String s)
        {
            StreamWriter writer = null;
            try
            {
                writer = GetStreamWriter();
                writer.WriteLine(s);
            }
            catch
            {
                return false;
            }
            finally
            {
                try
                {
                    writer.Close();
                }
                catch
                {
                }
            }
            return true;
        }
    }
}
