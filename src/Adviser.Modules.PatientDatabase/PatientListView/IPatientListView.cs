﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Data.Objects.DataClasses;

namespace Adviser.Modules.PatientDatabase
{
    using Microsoft.Practices.Composite.Events;
    using Adviser.Client.DataModel;
    using Adviser.Client.Core.Services;

    public interface IPatientListView
    {
        IPatientsPresentationModel Model { get; set; }
        event EventHandler<DataEventArgs<Patient>> PatientSelected;
        event EventHandler PatientDoubleClicked;
        void SetSelectedPatient(Patient patient);
    }
}
