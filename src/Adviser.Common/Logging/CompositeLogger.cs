﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Adviser.Common.Logging
{
    public class CompositeLogger<TEntry> : Logger<TEntry> where TEntry : ILogEntry, new()
    {
        /// <summary>
        /// The Collection of Loggers.
        /// </summary>
        private IDictionary<string, ILogger> _loggers = new Dictionary<string, ILogger>();

        /// <summary>
        /// Gets and sets the Collection of Loggers.
        /// </summary>
        protected IDictionary<string, ILogger> Loggers
        {
            get { return _loggers; }
            set { _loggers = value; }
        }

        /// <summary>
        /// Add a Logger to this CompositeLogger.
        /// </summary>
        /// <param name="name">A meaningful name for possible access later.</param>
        /// <param name="logger">The Logger to add.</param>
        public void AddLogger(String name, ILogger logger)
        {
            Loggers.Add(name, logger);
        }

        /// <summary>
        /// Remove a Logger
        /// </summary>
        /// <param name="name">The name of the Logger to remove</param>
        public void RemoveLogger(String name)
        {
            Loggers.Remove(name);
        }

        /// <summary>
        /// Send the log message to contained Loggers.
        /// </summary>
        /// <param name="fileName">The LogEntry to log.</param>
        /// <returns>Always return true--assume success</returns>
        protected internal override bool DoLog(TEntry logEntry)
        {
            foreach (ILogger logger in Loggers.Values)
                logger.Log(logEntry);
            return true;
        }

        /// <summary>
        /// Gets the Logger with the name corresponding to the given String.
        /// </summary>
        /// <param name="name">The name of the desired Logger</param>
        /// <returns>The Logger corresponding with name</returns>
        public ILogger this[String name]
        {
            get { return Loggers[name]; }
        }

        /// <summary>
        /// Create a new instance of CompositeLogger
        /// </summary>
        public CompositeLogger()
            : base()
        {
        }
    }
}
