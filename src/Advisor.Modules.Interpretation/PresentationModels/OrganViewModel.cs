﻿using System;
using System.Windows.Media;

namespace Advisor.Modules.Interpretation
{
    /// <summary>
    /// Presentation model for the organ view.
    /// </summary>
    public class OrganViewModel
    {
        public OrganViewModel() { }

        public OrganViewModel(
            string name, 
            Brush organBrush, 
            bool isVisible, 
            bool surgery,
            bool chronicDisease,
            int organValue, 
            string abbrevName, 
            int orderKey)
        {
            Name = name;
            OrganBrush = organBrush;
            IsVisible = isVisible;
            Surgery = surgery;
            ChronicDisease = chronicDisease;
            Value = organValue;
            AbbreviatedName = abbrevName;
            OrderKey = orderKey;
        }

        public string Name { set; get; }
        public Brush OrganBrush { set; get; }
        public bool IsVisible { set; get; }
        public bool Surgery { set; get; }
        public bool ChronicDisease { set; get; }
        public string AbbreviatedName { get; set; }
        public int OrderKey { get; set; }

        /// <summary>
        /// Exposes calculated condition value.
        /// </summary>
        public int Value { get; set; }
    }
}
