﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Modules.Interpretation
{
    public interface IMandalaTherapyView
    {
        IMandalaViewModel Model { get; set; }
    }
}
