﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Adviser.Common.Domain
{
    /// <summary>
    /// Data container for relevance value per adviser testing results.
    /// </summary>
    [Serializable()]
    public class MedicineRelevance
    {
        public MedicineRelevance() { }

        /// <summary>
        /// Unique medicine ID from the Adviser medicine database.
        /// </summary>
        public int MedicineID
        {
            get;
            set;
        }

        /// <summary>
        /// Relative relevance of the medicine in percents (0-100)
        /// </summary>
        public ushort Relevance
        {
            get;
            set;
        }
    }
}
