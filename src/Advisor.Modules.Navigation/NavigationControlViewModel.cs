﻿using System;
using System.Collections.Generic;
using System.Windows;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Wpf.Events;

using Adviser.Client.Core.Presentation;
using Adviser.Client.Core.Events;

namespace Advisor.Modules.Navigation
{
    public class ControlSelectedEventArgs : EventArgs
    {
        public string ControlId;
        public string ActionName;
    }

    public class NavigationControlViewModel : ViewModelBase
    {
        private IEventAggregator _eventAggregator;
        private bool _isEnabled;

        public NavigationControlViewModel(IEventAggregator eventAggregator, string id, string label, bool isEnabled, string actionName)
        {
            _eventAggregator = eventAggregator;
            Id = id;
            Label = label;
            IsEnabled = isEnabled;
            ActionName = actionName;

            SelectCommand = new DelegateCommand<string>(OnExecute);
        }

        public bool CanExecute(string action)
        {
            return IsEnabled;
        }

        public void OnExecute(string action)
        {
            //NavigationEventArgs args = new NavigationEventArgs();
            //args.EventName = ActionName;
            //_eventAggregator.GetEvent<NavigationEvent>().Publish(args);

            if (ControlSelected != null)
                ControlSelected(this, new ControlSelectedEventArgs() { ActionName = this.ActionName, ControlId = this.Id });
        }

        public void OnExecuteCommand(object sender, RoutedEventArgs e)
        {
            NavigationEventArgs args = new NavigationEventArgs();
            args.EventName = ActionName;
            _eventAggregator.GetEvent<NavigationEvent>().Publish(args);
        }

        public event EventHandler<ControlSelectedEventArgs> ControlSelected;

        public DelegateCommand<string> SelectCommand { get; private set; }

        public string Id { get; set; }
        public string Label { get; set; }
        public string ActionName { get; set; }
        public bool IsSelected { get; set; }
        //public bool IsEnabled { get; set; }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                _isEnabled = value;
                RaisePropertyChangedEvent("IsEnabled");
            }
        }
    }
}
