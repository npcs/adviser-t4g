﻿using System;
using System.Diagnostics;

namespace Adviser.Common.Logging
{
    /// <summary>
    /// EventLogLogger writes log entries to the Windows Event Log
    /// </summary>
    public class EventLogLogger<TEntry> : Logger<TEntry> where TEntry : ILogEntry, new()
    {
        private EventLog _eventLog;
        /// <summary>
        /// Gets and Sets the Windows event log.
        /// </summary>
        protected EventLog EventLog
        {
            get { return _eventLog; }
            set { _eventLog = value; }
        }

        /// <summary>
        /// Write logEntry information to the Windows event log
        /// </summary>
        /// <param name="logEntry">The LogEntry being logged</param>
        /// <returns>true upon success, false upon failure.</returns>
        protected internal override bool DoLog(TEntry logEntry)
        {
            // convert the log entry's severity to an appropriate type for the event log
            EventLogEntryType t =
                (logEntry.Severity < LogSeverity.Warning) ?
                    EventLogEntryType.Information :
                    (logEntry.Severity == LogSeverity.Warning ?
                        EventLogEntryType.Warning :
                        EventLogEntryType.Error);

            try
            {
                EventLog.WriteEntry(Formatter.AsString(logEntry), t);
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Create a new instance of EventLogLogger
        /// </summary>
        public EventLogLogger()
            : base()
        {
            EventLog = new EventLog();
            EventLog.Source = AppDomain.CurrentDomain.FriendlyName;
        }
        /// <summary>
        /// Create a new instance of EventLogLogger with a given Eventog
        /// </summary>
        /// <param name="eventLog">An EventLog to which logs will be written by this logger</param>
        public EventLogLogger(EventLog eventLog)
            : base()
        {
            EventLog = eventLog;
        }

        /// <summary>
        /// EventLogLogger uses LogEntryMessageOnlyFormatter by default.
        /// </summary>
        /// <returns></returns>
        protected override LogEntryFormatter GetDefaultFormatter()
        {
            return new MessageOnlyLogEntryFormatter();
        }
    }
}
