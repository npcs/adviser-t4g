﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Advisor.Modules.Catalog.Views
{
    /// <summary>
    /// Interaction logic for VisitDetailsFormView.xaml
    /// </summary>
    public partial class VisitDetailsFormView : UserControl
    {
        public VisitDetailsFormView()
        {
            InitializeComponent();
        }
    }
}
