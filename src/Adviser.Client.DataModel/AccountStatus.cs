﻿using System;

namespace Adviser.Client.DataModel
{
    /// <summary>
    /// Status of the practitioner's account.
    /// </summary>
    public enum AccountStatus
    {
        Confirmed = 1,
        NotConfirmend = 2
    }
}
