﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Client.Core.Presentation;

    public interface IPractitionerModel
    {
        //Guid ID { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string MidName { get; set; }
        string BusinessName { get; set; }
        string TypeOfPractice { get; set; }
        string StreetAddress { get; set; }
        string City { get; set; }
        string StateProvince { get; set; }
        string PostalCode { get; set; }
        string Country { get; set; }
        string Occupation { get; set; }
        string PrimaryPhone { get; set; }
        string SecondaryPhone { get; set; }
        string Fax { get; set; }
        string Email { get; set; }
        string Password { get; set; }
        string Message { get; set; }
        bool IsStatusOk { get; set; }
        bool DataSyncEnabled { get; set; }
        bool DataSyncInProcess { get; set; }
    }

    public class PractitionerModel : ViewModelBase, IPractitionerModel
    {

        //private Guid _ID;
        //public Guid ID
        //{
        //    get
        //    {
        //        return _ID;
        //    }
        //    set
        //    {
        //        _ID = value;
        //        RaisePropertyChangedEvent("ID");
        //    }
        //}

        private string _FirstName;
        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
                RaisePropertyChangedEvent("FirstName");
            }
        }

        private string _LastName;
        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
                RaisePropertyChangedEvent("LastName");
            }
        }

        private string _MidName;
        public string MidName
        {
            get
            {
                return _MidName;
            }
            set
            {
                _MidName = value;
                RaisePropertyChangedEvent("MidName");
            }
        }

        private string _BusinessName;
        public string BusinessName
        {
            get
            {
                return _BusinessName;
            }
            set
            {
                _BusinessName = value;
                RaisePropertyChangedEvent("BusinessName");
            }
        }

        private string _TypeOfPractice;
        public string TypeOfPractice
        {
            get
            {
                return _TypeOfPractice;
            }
            set
            {
                _TypeOfPractice = value;
                RaisePropertyChangedEvent("TypeOfPractice");
            }
        }

        private string _StreetAddress;
        public string StreetAddress
        {
            get
            {
                return _StreetAddress;
            }
            set
            {
                _StreetAddress = value;
                RaisePropertyChangedEvent("StreetAddress");
            }
        }

        private string _City;
        public string City
        {
            get
            {
                return _City;
            }
            set
            {
                _City = value;
                RaisePropertyChangedEvent("City");
            }
        }

        private string _stateProvince;
        public string StateProvince
        {
            get { return _stateProvince; }
            set
            {
                _stateProvince = value;
                RaisePropertyChangedEvent("StateProvince");
            }
        }

        private string _PostalCode;
        public string PostalCode
        {
            get
            {
                return _PostalCode;
            }
            set
            {
                _PostalCode = value;
                RaisePropertyChangedEvent("PostalCode");
            }
        }

        private string _Country;
        public string Country
        {
            get
            {
                return _Country;
            }
            set
            {
                _Country = value;
                RaisePropertyChangedEvent("Country");
            }
        }

        private string _Occupation;
        public string Occupation
        {
            get
            {
                return _Occupation;
            }
            set
            {
                _Occupation = value;
                RaisePropertyChangedEvent("Occupation");
            }
        }

        private string _PrimaryPhone;
        public string PrimaryPhone
        {
            get
            {
                return _PrimaryPhone;
            }
            set
            {
                _PrimaryPhone = value;
                RaisePropertyChangedEvent("PrimaryPhone");
            }
        }

        private string _SecondaryPhone;
        public string SecondaryPhone
        {
            get
            {
                return _SecondaryPhone;
            }
            set
            {
                _SecondaryPhone = value;
                RaisePropertyChangedEvent("SecondaryPhone");
            }
        }

        private string _Fax;
        public string Fax
        {
            get
            {
                return _Fax;
            }
            set
            {
                _Fax = value;
                RaisePropertyChangedEvent("Fax");
            }
        }

        private string _Email;
        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                _Email = value;
                RaisePropertyChangedEvent("Email");
            }
        }

        private string _Password;
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;
                RaisePropertyChangedEvent("Password");
            }
        }

        private string _Message;
        public string Message
        {
            get
            {
                return _Message;
            }
            set
            {
                _Message = value;
                RaisePropertyChangedEvent("Message");
            }
        }

        private bool _IsStatusOk;
        public bool IsStatusOk
        {
            get
            {
                return _IsStatusOk;
            }
            set
            {
                _IsStatusOk = value;
                RaisePropertyChangedEvent("IsStatusOk");
            }
        }

        private bool _DataSyncEnabled;

        public bool DataSyncEnabled
        {
            get
            {
                return _DataSyncEnabled;
            }
            set
            {
                _DataSyncEnabled = value;
                RaisePropertyChangedEvent("DataSyncEnabled");
            }
        }

		private bool _DataSyncInProcess;

		public bool DataSyncInProcess
		{
			get
			{
				return _DataSyncInProcess;
			}
			set
			{
				_DataSyncInProcess = value;
				RaisePropertyChangedEvent("DataSyncInProcess");
			}
		}
	}
}
