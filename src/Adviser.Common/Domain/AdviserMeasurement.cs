﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Common.Domain
{
    /// <summary>
    /// This class is used to store measurement results in the Visits table and
    /// to transfer them between client and server.
    /// </summary>
    [Serializable]
    public class AdviserMeasurement
    {
        public Guid DeviceID;

        public MeasurementContext Context
        {
            get;
            set;
        }

        public DataMatrix<PairMeasurement> Data
        {
            get;
            set;
        }
    }
}
