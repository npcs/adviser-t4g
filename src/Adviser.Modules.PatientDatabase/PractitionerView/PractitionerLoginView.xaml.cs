﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Adviser.Modules.PatientDatabase
{
    /// <summary>
    /// Interaction logic for PractitionerLoginView.xaml
    /// </summary>
    public partial class PractitionerLoginView : UserControl, IPractitionerLoginView
    {
        public PractitionerLoginView()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            BindingExpression binding = EmailTextBox.GetBindingExpression(TextBox.TextProperty);
            binding.UpdateSource();

            if (!Validation.GetHasError(EmailTextBox))
            {
                LoginClicked(this, null);
            }
        }

        private void ForgotPasswordButton_Click(object sender, RoutedEventArgs e)
        {
            ForgotPasswordClicked(this, null);
        }

        private void CreateAccountButton_Click(object sender, RoutedEventArgs e)
        {
            CreateAccountClicked(this, null);
        }

        private void DataSyncButton_Click(object sender, RoutedEventArgs e)
        {
            BindingExpression binding = EmailTextBox.GetBindingExpression(TextBox.TextProperty);
            binding.UpdateSource();

            if (!Validation.GetHasError(EmailTextBox))
            {
                DataSyncClicked(this, null);
            }
        }


        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            CancelClicked(this, null);
        }

		private void btnOk_Click(object sender, RoutedEventArgs e)
		{
			MessageTextBlock.Text = "";
			this.MessagePopup.IsOpen = false;
		}
		#region IPractitionerLoginView Members

		public IPractitionerModel Model
        {
            get
            {
                return this.DataContext as IPractitionerModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public event EventHandler LoginClicked;

        public event EventHandler CreateAccountClicked;

        public event EventHandler ForgotPasswordClicked;

        public event EventHandler DataSyncClicked;

        public event EventHandler CancelClicked;

		public void ShowMessage(string message, bool isOKButtonVisible)
		{
			btnOk.Visibility = isOKButtonVisible ? Visibility.Visible : Visibility.Hidden;
			MessageTextBlock.Text = message;
			MessagePopup.StaysOpen = true;
			MessagePopup.IsOpen = true;
		}

		#endregion


	}
}
