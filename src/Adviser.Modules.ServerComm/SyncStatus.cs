﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Modules.ServerComm
{
    /// <summary>
    /// List of possible synchronization status values.
    /// </summary>
    public enum SyncStatus
    {
        Pending = 1,
        InProgress = 2,
        Failed = 3
    }
}
