﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Adviser.Client.Core.Presentation;

namespace Adviser.Client.Core.Presentation
{
    public class NotesHistoryModel: ViewModelBase
    {
        public NotesHistoryModel()
        {
        }

        public NotesHistoryModel(DateTime visitDate, string visitNotes, string highlightColor)
        {
            this.VisitDate = visitDate;
            this.VisitNotes = visitNotes;
            this.HighlightColor = highlightColor;
        }

        public DateTime VisitDate
        {
            get;
            set;
        }     

        public string VisitNotes
        {
            get;
            set;
        }

        public string HighlightColor
        {
            get;
            set;
        }
   
    }
}
