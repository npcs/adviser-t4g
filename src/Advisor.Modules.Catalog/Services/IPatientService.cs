﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects;
using System.Data.Objects.DataClasses;

namespace Advisor.Modules.Catalog.Services
{
    using System.Collections.ObjectModel;
    using Advisor.Common.DataModel;

    public interface IPatientService
    {
        Practitioner RetrivePractitioner();

        EntityCollection<Patient> RetrivePatientList(Practitioner CurrentPractitioner);
        void AddPatient(Practitioner CurrentPractitioner);
        
        EntityCollection<Anamnesis> RetriveAnamnesisList(Patient CurrentPatient);
        void AddAnamnez(Patient CurrentPatient);

        EntityCollection<Visit> RetriveVisitList(Patient CurrentParient);
        void AddVisit(Patient CurrentPatient);
        
        void DeleteObject(Object EntityObject);
        
        void UpdateDatabase();
    }
}
