﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Common.Domain
{
    public class ScaleValueTransform : IValueTransform
    {
        private double _scaleFactor;

        public ScaleValueTransform(double scaleFactor)
        {
            _scaleFactor = scaleFactor;
        }

        public double Transform(double source)
        {
            return source * _scaleFactor;
        }

        public void SetParameters(object[] args)
        {
        }

    }
}
