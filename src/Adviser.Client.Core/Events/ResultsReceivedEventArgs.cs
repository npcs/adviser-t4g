﻿using System;
using System.Collections.Generic;

using Adviser.Client.DataModel;

namespace Adviser.Client.Core.Events
{
    [Serializable]
    public class ResultsReceivedEventArgs
    {
        public bool IsSuccess;
        public string Message;
        public Visit VisitData;
    }
}
