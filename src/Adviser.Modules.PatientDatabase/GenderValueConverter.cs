﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Adviser.Modules.PatientDatabase
{
    [ValueConversion(typeof(string), typeof(Boolean))]
    public class MaleConverter : IValueConverter
    {
        private static readonly MaleConverter _instance = new MaleConverter();

        static MaleConverter() { }

        public static MaleConverter Instance
        {
            get { return _instance; }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                if (String.Equals(value.ToString(), "M", StringComparison.InvariantCultureIgnoreCase))
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (System.Convert.ToBoolean(value))
            {
                return "M";
            }
            else
            {
                return "F";
            }
        }
    }

    [ValueConversion(typeof(string), typeof(Boolean))]
    public class FemaleConverter : IValueConverter
    {
        private static readonly FemaleConverter _instance = new FemaleConverter();

        static FemaleConverter() { }

        public static FemaleConverter Instance
        {
            get { return _instance; }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                if (String.Equals(value.ToString(), "F", StringComparison.InvariantCultureIgnoreCase))
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (System.Convert.ToBoolean(value))
            {
                return "F";
            }
            else
            {
                return "M";
            }
        }
    }
}
