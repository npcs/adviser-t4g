﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.Unity;


namespace Advisor.Modules.DeviceControl.Views
{
    public interface IDiagnosticsPresenter
    {
        IDiagnosticsView View { get; set; }
    }

    public class DiagnosticsPresenter : IDiagnosticsPresenter
    {
        public DiagnosticsPresenter(
            IDiagnosticsView diagnosticsView,
            IElectrodeConnectionsPresenter electrodesPresenter,
            IDeviceControlPresenter devicePresenter)
        {
            View = diagnosticsView;
            View.SetLeftPanel(electrodesPresenter.View);
            View.SetRightPanel(devicePresenter.View);
        }

        public IDiagnosticsView View
        {
            get;
            set;
        }

    }
}
