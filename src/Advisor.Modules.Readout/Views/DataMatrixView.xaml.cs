﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Composite.Events;
using Advisor.Modules.Readout.PresentationModels;

namespace Advisor.Modules.Readout.Views
{
    /// <summary>
    /// Interaction logic for DataMatrixView.xaml
    /// </summary>
    public partial class DataMatrixView : UserControl, IDataMatrixView
    {
        public event EventHandler<DataEventArgs<IDataCellViewModel>> CellSelected = delegate { };

        public DataMatrixView()
        {
            InitializeComponent();
        }

        public IDataMatrixViewModel Model
        {
            get { return this.DataContext as IDataMatrixViewModel; }
            set { this.DataContext = value; }
        }

        private void Cell_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            CellSelected(sender, new DataEventArgs<IDataCellViewModel>((DataCellViewModel)((TextBlock)e.Source).DataContext));
        }
    }
}
