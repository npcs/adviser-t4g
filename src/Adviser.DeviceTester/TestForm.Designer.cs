﻿namespace Adviser.DeviceTester
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.HisrotyTextBlock = new System.Windows.Forms.TextBox();
            this.WriteGuidButton = new System.Windows.Forms.Button();
            this.ReadAndCheckButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ReadButton = new System.Windows.Forms.Button();
            this.WriteVersionButton = new System.Windows.Forms.Button();
            this.ReadVersionButton = new System.Windows.Forms.Button();
            this.USBEventTimer = new System.Windows.Forms.Timer(this.components);
            this.GetFirmwareVersionButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HisrotyTextBlock
            // 
            this.HisrotyTextBlock.Location = new System.Drawing.Point(12, 99);
            this.HisrotyTextBlock.Multiline = true;
            this.HisrotyTextBlock.Name = "HisrotyTextBlock";
            this.HisrotyTextBlock.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.HisrotyTextBlock.Size = new System.Drawing.Size(992, 617);
            this.HisrotyTextBlock.TabIndex = 0;
            // 
            // WriteGuidButton
            // 
            this.WriteGuidButton.Location = new System.Drawing.Point(12, 12);
            this.WriteGuidButton.Name = "WriteGuidButton";
            this.WriteGuidButton.Size = new System.Drawing.Size(327, 39);
            this.WriteGuidButton.TabIndex = 1;
            this.WriteGuidButton.Text = "Write GUID and Version to the board and update the Device";
            this.WriteGuidButton.UseVisualStyleBackColor = true;
            this.WriteGuidButton.Click += new System.EventHandler(this.WriteGuidButton_Click);
            // 
            // ReadAndCheckButton
            // 
            this.ReadAndCheckButton.Location = new System.Drawing.Point(405, 12);
            this.ReadAndCheckButton.Name = "ReadAndCheckButton";
            this.ReadAndCheckButton.Size = new System.Drawing.Size(189, 39);
            this.ReadAndCheckButton.TabIndex = 2;
            this.ReadAndCheckButton.Text = "Read Guid and check the DB";
            this.ReadAndCheckButton.UseVisualStyleBackColor = true;
            this.ReadAndCheckButton.Click += new System.EventHandler(this.ReadAndCheckButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(877, 12);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(127, 39);
            this.CancelButton.TabIndex = 3;
            this.CancelButton.Text = "Clear History";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ReadButton
            // 
            this.ReadButton.Location = new System.Drawing.Point(515, 57);
            this.ReadButton.Name = "ReadButton";
            this.ReadButton.Size = new System.Drawing.Size(159, 36);
            this.ReadButton.TabIndex = 4;
            this.ReadButton.Text = "Read Guid from the board";
            this.ReadButton.UseVisualStyleBackColor = true;
            this.ReadButton.Click += new System.EventHandler(this.ReadButton_Click);
            // 
            // WriteVersionButton
            // 
            this.WriteVersionButton.Location = new System.Drawing.Point(12, 57);
            this.WriteVersionButton.Name = "WriteVersionButton";
            this.WriteVersionButton.Size = new System.Drawing.Size(230, 36);
            this.WriteVersionButton.TabIndex = 5;
            this.WriteVersionButton.Text = "Write version to the board";
            this.WriteVersionButton.UseVisualStyleBackColor = true;
            this.WriteVersionButton.Click += new System.EventHandler(this.WriteVersionButton_Click);
            // 
            // ReadVersionButton
            // 
            this.ReadVersionButton.Location = new System.Drawing.Point(302, 57);
            this.ReadVersionButton.Name = "ReadVersionButton";
            this.ReadVersionButton.Size = new System.Drawing.Size(189, 36);
            this.ReadVersionButton.TabIndex = 6;
            this.ReadVersionButton.Text = "Read Version from the board";
            this.ReadVersionButton.UseVisualStyleBackColor = true;
            this.ReadVersionButton.Click += new System.EventHandler(this.ReadVersionButton_Click);
            // 
            // USBEventTimer
            // 
            this.USBEventTimer.Interval = 1000;
            this.USBEventTimer.Tick += new System.EventHandler(this.USBEventTimer_Tick);
            // 
            // GetFirmwareVersionButton
            // 
            this.GetFirmwareVersionButton.Location = new System.Drawing.Point(726, 57);
            this.GetFirmwareVersionButton.Name = "GetFirmwareVersionButton";
            this.GetFirmwareVersionButton.Size = new System.Drawing.Size(127, 36);
            this.GetFirmwareVersionButton.TabIndex = 7;
            this.GetFirmwareVersionButton.Text = "Get Firmware Version";
            this.GetFirmwareVersionButton.UseVisualStyleBackColor = true;
            this.GetFirmwareVersionButton.Click += new System.EventHandler(this.GetFirmwareVersionButton_Click);
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 728);
            this.Controls.Add(this.GetFirmwareVersionButton);
            this.Controls.Add(this.ReadVersionButton);
            this.Controls.Add(this.WriteVersionButton);
            this.Controls.Add(this.ReadButton);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.ReadAndCheckButton);
            this.Controls.Add(this.WriteGuidButton);
            this.Controls.Add(this.HisrotyTextBlock);
            this.Name = "TestForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.TestForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox HisrotyTextBlock;
        private System.Windows.Forms.Button WriteGuidButton;
        private System.Windows.Forms.Button ReadAndCheckButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button ReadButton;
        private System.Windows.Forms.Button WriteVersionButton;
        private System.Windows.Forms.Button ReadVersionButton;
        internal System.Windows.Forms.Timer USBEventTimer;
        private System.Windows.Forms.Button GetFirmwareVersionButton;
    }
}

