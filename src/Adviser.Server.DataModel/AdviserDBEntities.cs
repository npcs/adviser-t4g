﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adviser.Server.DataModel
{
    public partial class AdviserDBEntities
    {
        public AdviserDBEntities(string connectionString, string defaultContainerName) :
            base(connectionString, defaultContainerName)
        {
            this.OnContextCreated();
        }
    }
}
