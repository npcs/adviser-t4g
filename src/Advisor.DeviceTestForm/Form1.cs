﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Adviser.Client.Core.DeviceControl;
using Adviser.Common.Domain;

namespace Advisor.DeviceTestForm
{
    public partial class Form1 : Form
    {
        private DeviceDriver _device;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            DeviceSettings _DeviceSettings = new DeviceSettings();


            _DeviceSettings.ADCSettings = ushort.Parse(txtADCConfig.Text, System.Globalization.NumberStyles.HexNumber);

            _device.SetupDevice(_DeviceSettings);

            lblADCConfig.Text = (_device.ReadADCRegister(0)).ToString("X");

        }

        private void btnConnectElectrodes_Click(object sender, EventArgs e)
        {
            _device.ConnectElectrodes(Convert.ToByte(txtFront.Text), Convert.ToByte(txtBack.Text));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _device = new DeviceDriver();
            _device.SetupDevice(new DeviceSettings());
            lblADCConfig.Text = ConvertToString(_device.ReadADCRegister(0));

        }

        private void btnCurrentADCConfig_Click(object sender, EventArgs e)
        {
            lblADCConfig.Text = String.Concat(ConvertToString(_device.ReadADCRegister(0)), " ", System.DateTime.Now);
        }

        private void btnGetShuntVoltage_Click(object sender, EventArgs e)
        {
            lblShuntVoltage.Text = ConvertToString(_device.ReadData());
        }

        private void btnGetBusVoltage_Click(object sender, EventArgs e)
        {
            lblBusVoltage.Text = ConvertToString(_device.ReadADCRegister(4));
        }

        private string ConvertToString(ushort UShort)
        {
            return String.Concat("Hex: ", UShort.ToString("X"), " --- Dec: ", UShort, " === Volts:", ((decimal)UShort)/100000);
        }

    }
}
