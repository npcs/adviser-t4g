﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Adviser.Server.Services.Contracts
{
    [DataContract]
    public class Prescription
    {
        [DataMember]
        public Guid ID;

        [DataMember]
        public bool Deleted;

        [DataMember]
        public int? MedicineID;

        [DataMember]
        public string MedicineName;

        [DataMember]
        public string Directions;
    }
}
