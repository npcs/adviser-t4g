﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Xml;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DataContracts = Adviser.Server.Services.Contracts;
using Adviser.Server.DataModel;
using Adviser.Server.Services;

namespace Adviser.Server.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class AdviserServicesTests
    {
        public AdviserServicesTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        private DataContracts::Practitioner InitializeDataContractPractitioner()
        {
            DataContracts::Practitioner practitioner = new DataContracts::Practitioner();
            practitioner.ID = Guid.Empty;
            practitioner.FirstName = "UNIT";
            practitioner.LastName = "TEST";
            practitioner.Occupation = "QA";
            practitioner.StreetAddress = "123 Windows Drive";
            practitioner.City = "Dallas";
            practitioner.StateProvince = "TX";
            practitioner.Country = "USA";
            practitioner.PostalCode = "12345-7890";
            practitioner.BusinessName = "IKO Software";
            practitioner.PrimaryPhone = "222-333-4567";
            practitioner.TypeOfPractice = "TESTING";
            practitioner.Email = "test@unittest.com";
            practitioner.Password = "password123";

            return practitioner;
        }

        private Practitioner InitializePractitioner()
        {
            Practitioner practitioner = new Practitioner();
            practitioner.ID = Guid.Empty;
            practitioner.FirstName = "UNIT";
            practitioner.LastName = "TEST";
            practitioner.Occupation = "QA";
            practitioner.StreetAddress = "123 Windows Drive";
            practitioner.City = "Dallas";
            practitioner.StateProvince = "TX";
            practitioner.Country = "USA";
            practitioner.PostalCode = "12345-7890";
            practitioner.BusinessName = "IKO Software";
            practitioner.PrimaryPhone = "222-333-4567";
            practitioner.TypeOfPractice = "TESTING";
            practitioner.Email = "test@unittest.com";
            practitioner.Password = "password123";
            practitioner.LastUpdDate = DateTime.Now.ToUniversalTime();
            return practitioner;
        }

        [TestMethod]
        public void RegisterAccount()
        {
            DataContracts::Practitioner practitioner = InitializeDataContractPractitioner();
            string strDeviceId = "2b5d770e-c014-4e38-8c1a-846f84f2c5cf";
            Device device = Device.CreateDevice(new Guid(strDeviceId));
            device.OwnerID = practitioner.ID;
            device.Status = DeviceStatus.Available.ToString();
            device.Notes = "UNIT TEST: PLEASE DELETE IF FOUND";

            AdviserDBEntities dbContext = new AdviserDBEntities();
            try
            {
                dbContext.AddToDeviceSet(device);
                dbContext.SaveChanges();
                //- test positive outcome
                AdviserService service = new AdviserService();
                DataContracts::AccountRegistration registration = service.RegisterAccount(practitioner, device.ID);

                Assert.IsTrue(registration.IsSuccess);
                Assert.AreEqual(practitioner.ID.ToString(), registration.AccountId);
                Assert.IsTrue(registration.MessageText != String.Empty);

                //- test rejection scenario
                try
                {
                    registration = service.RegisterAccount(practitioner, device.ID);
                    Assert.IsFalse(registration.IsSuccess);
                }
                catch (FaultException<DataContracts::AdviserServiceFault> fault)
                {
                    Assert.IsTrue(fault.Detail.FaultCode == 403);
                    Assert.IsTrue(fault.Detail.Message != String.Empty);
                }
            }
            finally
            {
                //- clean up
                dbContext.DeleteObject(device);
                dbContext.SaveChanges();
                dbContext.Dispose();
                DBService dbService = new DBService();
                dbService.DeletePractitioner(practitioner.ID);
                dbService.SaveChanges();
            }
        }

        [TestMethod]
        public void GetExamResults()
        {
            //- create practitioner and device
            Practitioner practitioner = InitializePractitioner();
            string strDeviceId = "2b5d770e-c014-4e38-8c1a-846f84f2c5cf";
            Device device = Device.CreateDevice(new Guid(strDeviceId));
            device.OwnerID = practitioner.ID;
            device.Status = DeviceStatus.Online.ToString();
            device.Notes = "UNIT TEST: PLEASE DELETE IF FOUND";

            //- data that will vary
            string oldComplain = "Can't bind two words in a sentence";
            string oldCity = "Washington";
            string newerComplain = "Nobody likes me";
            string newerCity = "Dallas";
            DateTime newerDate = DateTime.Now.ToUniversalTime();
            DateTime oldDate = newerDate.Subtract(new TimeSpan(1, 0, 0, 0));

            //- create patient
            DataContracts::Patient patient = new DataContracts::Patient();
            patient.ID = Guid.NewGuid();
            patient.FirstName = "George";
            patient.MidName = "W";
            patient.LastName = "Bush";
            patient.Sex = "M";
            patient.Occupation = "Drone";
            patient.Lifestyle = "Sedentary";
            patient.Height = 160;
            patient.Weight = 300;
            patient.StreetAddress = "Losers Ave.";
            patient.City = newerCity;
            patient.Country = "USA";
            patient.Email = "top@losers.com";
            patient.MajorComplain = newerComplain;
            patient.LastUpdDate = newerDate;

            //- load visit
            DataContractSerializer contractSerializer = new DataContractSerializer(typeof(DataContracts::Visit));
            Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Adviser.Server.Tests.Resources.Visit.xml");
            XmlTextReader xmlReader = new XmlTextReader(stream);
            DataContracts::Visit visit = (DataContracts::Visit)contractSerializer.ReadObject(xmlReader);
            visit.LastUpdDate = DateTime.Now.ToUniversalTime();

            patient.Visits = new DataContracts::Visit[] { visit };

            Guid olderVisitID = Guid.Empty;
            Guid newerVisitID = visit.ID;
            AdviserDBEntities dbContext = new AdviserDBEntities();
            try
            {
                dbContext.AddToDeviceSet(device);
                dbContext.AddToPractitionerSet(practitioner);
                dbContext.SaveChanges();

                //- test new patient
                AdviserService service = new AdviserService();
                //DataContracts::AccountRegistration registration = service.RegisterAccount(practitioner, device.ID);
                //Assert.IsTrue(registration.IsSuccess);

                DataContracts::Patient outPatient = service.GetExamResults(practitioner.ID, device.ID, patient);
                Assert.AreNotEqual(outPatient.Visits[0].ExamResult, null);

                //- test followup: new visit and "older" version of Patient
                patient.City = oldCity;
                patient.MajorComplain = oldComplain;
                patient.LastUpdDate = oldDate;//- simulate like older version was entered on other client

                stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Adviser.Server.Tests.Resources.Visit2.xml");
                xmlReader = new XmlTextReader(stream);
                visit = (DataContracts::Visit)contractSerializer.ReadObject(xmlReader);
                visit.LastUpdDate = DateTime.Now.ToUniversalTime();
                patient.Visits[0] = visit;
                olderVisitID = visit.ID;

                outPatient = service.GetExamResults(practitioner.ID, device.ID, patient);

                Assert.AreNotEqual(outPatient.Visits[0].ExamResult, null);
                Assert.AreEqual(outPatient.MajorComplain, newerComplain);
                Assert.AreEqual(outPatient.City, newerCity);

                //- check that patient updated date must match newer version
                Assert.IsTrue(outPatient.LastUpdDate.Day == newerDate.Day);
                Assert.IsTrue(outPatient.LastUpdDate.Hour == newerDate.Hour);

                //- check usage entries
                var usageItems = from usagedata in dbContext.UsageDataItemSet 
                                 where (usagedata.VisitID == newerVisitID || 
                                 usagedata.VisitID == olderVisitID)
                                 select usagedata;
                int count = 0;
                foreach (UsageDataItem item in usageItems)
                {
                    Assert.AreEqual(item.AccountID, practitioner.ID);
                    Assert.AreEqual(item.Devices.ID, device.ID);
                    dbContext.DeleteObject(item);
                    count++;
                }
                Assert.IsTrue(count == 2);
            }
            finally
            {
                stream.Close();

                //- clean up
               // Practitioner prac = dbContext.PractitionerSet.First(p => p.ID == practitioner.ID);
               // dbContext.DeleteObject(prac);
                Patient pat = dbContext.PatientSet.First(p => p.ID == patient.ID);
                dbContext.DeleteObject(pat);
                dbContext.DeleteObject(practitioner);
                dbContext.DeleteObject(device);
                dbContext.SaveChanges();

                dbContext.Dispose();
            }
        }

        [TestMethod]
        public void AdviserUserNamePasswordValidatorTest()
        {
            using (AdviserDBEntities dbContext = new AdviserDBEntities())
            {
                string deviceId = "2b5d770e-c014-4e38-8c1a-846f84f2c5cf";
                Device device = Device.CreateDevice(new Guid(deviceId));
                device.Notes = "UNIT TEST: PLEASE DELETE IF FOUND";
                device.Status = DeviceStatus.NotAvailable.ToString();

                AdviserUserNamePasswordValidator validator = new AdviserUserNamePasswordValidator();
                try
                {
                    validator.Validate(String.Empty, String.Empty);
                    Assert.Fail("Failed empty userName validation.");
                }
                catch(Exception ex)
                {
                    Assert.IsTrue(ex is SecurityTokenException);
                }

                try
                {
                    validator.Validate("WrongUser", String.Empty);
                    Assert.Fail("Failed invalid fromat userName validation.");
                }
                catch(Exception ex)
                {
                    Assert.IsTrue(ex is SecurityTokenException);
                }

                try
                {
                    validator.Validate(device.ID.ToString(), String.Empty);
                    Assert.Fail("Failed not existing device validation.");
                }
                catch(Exception ex)
                {
                    Assert.IsTrue(ex is SecurityTokenException);
                }

                try
                {
                    try
                    {
                        dbContext.AddToDeviceSet(device);
                        dbContext.SaveChanges();
                        validator.Validate(device.ID.ToString(), String.Empty);
                        Assert.Fail("Failed invalid device status validation.");
                    }
                    catch (Exception ex)
                    {
                        Assert.IsTrue(ex is SecurityTokenException);
                    }

                    try
                    {//- pass validation
                        device.Status = DeviceStatus.Available.ToString();
                        dbContext.SaveChanges();
                        validator.Validate(device.ID.ToString(), String.Empty);
                    }
                    catch
                    {
                        Assert.Fail("Failed correct device validation.");
                    }
                }
                finally
                {//- delete test device
                    dbContext.DeleteObject(device);
                    dbContext.SaveChanges();
                }
            }
        }

        //public void OLD_CustomValidatorTest()
        //{
        //    DataContracts::Practitioner practitioner = InitializeDataContractPractitioner();
        //    Guid deviceId = Guid.Empty;
        //    DBService dbService = new DBService();

        //    Device device = dbService.GetDeviceByID(deviceId);
        //    device.Status = DeviceStatus.Available.ToString();
        //    device.OwnerID = null;
        //    dbService.SaveChanges();

        //    AdviserService service = new AdviserService();
        //    DataContracts::AccountRegistration registration = service.RegisterAccount(practitioner, deviceId);
        //    Assert.IsTrue(registration.IsSuccess);

        //    AdviserUserNamePasswordValidator validator = new AdviserUserNamePasswordValidator();
        //    bool passSuccess = false;
        //    bool passFail = false;
        //    try
        //    {
        //        validator.Validate(practitioner.Email, practitioner.Password);
        //        passSuccess = true;
        //    }
        //    catch { }

        //    try
        //    {
        //        validator.Validate(practitioner.Email, "Wrong Password");
        //    }
        //    catch (SecurityTokenException)
        //    {
        //        passFail = true;
        //    }
        //    catch (Exception)
        //    {
        //        passFail = false;
        //    }

        //    if (registration.IsSuccess)
        //    {
        //        dbService.DeletePractitioner(practitioner.ID);
        //        device = dbService.GetDeviceByID(deviceId);
        //        device.Status = DeviceStatus.Available.ToString();
        //        device.OwnerID = null;
        //        dbService.SaveChanges();
        //    }

        //    Assert.IsTrue(passSuccess);
        //    Assert.IsTrue(passFail);
        //}

        [TestMethod]
        public void ValidatePractitionerTest()
        {
            Practitioner practitioner = InitializePractitioner();
            string deviceId = "2b5d770e-c014-4e38-8c1a-846f84f2c5cf";
            Device device = Device.CreateDevice(new Guid(deviceId));
            device.OwnerID = practitioner.ID;
            device.Status = DeviceStatus.NotAvailable.ToString();
            device.Notes = "UNIT TEST: PLEASE DELETE IF FOUND";
            Guid invalidId = Guid.NewGuid();

            using (AdviserDBEntities dbContext = new AdviserDBEntities())
            {
                try
                {
                    dbContext.AddToDeviceSet(device);
                    dbContext.AddToPractitionerSet(practitioner);
                    dbContext.SaveChanges();

                    CredentialsValidator validator = new CredentialsValidator();

                    //- positive test
                    validator.ValidatePractitioner(practitioner.Email, practitioner.Password, practitioner.ID);

                    try
                    {//- invalid email
                        validator.ValidatePractitioner("false@email.com", practitioner.Password, practitioner.ID);
                        Assert.Fail("Failed invalid email validation");
                    }
                    catch (Exception ex)
                    {
                        Assert.IsTrue(ex is AdviserSecurityException);
                    }
                    try
                    {//- invalid password
                        validator.ValidatePractitioner(practitioner.Email, "Levyi Parol", practitioner.ID);
                        Assert.Fail("Failed invalid password validation");
                    }
                    catch (Exception ex)
                    {
                        Assert.IsTrue(ex is AdviserSecurityException);
                    }
                    try
                    {//- invalid practitioner id
                        validator.ValidatePractitioner(practitioner.Email, practitioner.Password, invalidId);
                        Assert.Fail("Failed invalid practitioner id");
                    }
                    catch (Exception ex)
                    {
                        Assert.IsTrue(ex is AdviserSecurityException);
                    }
                }
                finally
                {
                    dbContext.DeleteObject(practitioner);
                    dbContext.DeleteObject(device);
                    dbContext.SaveChanges();
                }
            }
        }
    }
}
