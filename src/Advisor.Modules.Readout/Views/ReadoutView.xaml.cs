﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Advisor.Modules.Readout.PresentationModels;

namespace Advisor.Modules.Readout.Views
{
    /// <summary>
    /// Interaction logic for ReadoutView.xaml
    /// </summary>
    public partial class ReadoutView : UserControl, IReadoutView
    {
        public event EventHandler<RoutedEventArgs> SaveFile = delegate { };
        public event EventHandler<RoutedEventArgs> Reset = delegate { };

        public ReadoutView()
        {
            InitializeComponent();
        }

        public IDataMatrixViewModel Model
        {
            get { return DataContext as IDataMatrixViewModel; }
            set { DataContext = value; }
        }

        public void ShowMatrix(IDataMatrixView matrixView)
        {
            this.MatrixPanel.Content = matrixView;
        }

        public void ShowPairDetails(IPairDetailsView pairDetailsView)
        {
            this.DetailsPanel.Content = pairDetailsView;
        }

        public void HidePairDetails()
        {
            this.DetailsPanel.Content = null;
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            Reset(sender, e);
        }

        //private void SaveFileButton_Click(object sender, RoutedEventArgs e)
        //{
        //    SaveFile(sender, e);
        //}

    }
}
