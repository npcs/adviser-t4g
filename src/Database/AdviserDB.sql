/****** Object:  ForeignKey [FK_OrganExamination_Organs]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganExamination_Organs]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [FK_OrganExamination_Organs]
GO
/****** Object:  ForeignKey [FK_OrganExamination_Visits1]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganExamination_Visits1]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [FK_OrganExamination_Visits1]
GO
/****** Object:  ForeignKey [FK_Patient_Practitioner]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Patient_Practitioner]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
ALTER TABLE [dbo].[Patients] DROP CONSTRAINT [FK_Patient_Practitioner]
GO
/****** Object:  ForeignKey [FK_Prescriptions_Visit]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Prescriptions_Visit]') AND parent_object_id = OBJECT_ID(N'[dbo].[Prescriptions]'))
ALTER TABLE [dbo].[Prescriptions] DROP CONSTRAINT [FK_Prescriptions_Visit]
GO
/****** Object:  ForeignKey [FK_SymptomVisitXRef_Symptoms ]    Script Date: 08/09/2009 21:40:28 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SymptomVisitXRef_Symptoms ]') AND parent_object_id = OBJECT_ID(N'[dbo].[SymptomVisitXRef]'))
ALTER TABLE [dbo].[SymptomVisitXRef] DROP CONSTRAINT [FK_SymptomVisitXRef_Symptoms ]
GO
/****** Object:  ForeignKey [FK_SymptomVisitXRef_Visit]    Script Date: 08/09/2009 21:40:28 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SymptomVisitXRef_Visit]') AND parent_object_id = OBJECT_ID(N'[dbo].[SymptomVisitXRef]'))
ALTER TABLE [dbo].[SymptomVisitXRef] DROP CONSTRAINT [FK_SymptomVisitXRef_Visit]
GO
/****** Object:  ForeignKey [FK_Visit_Patient]    Script Date: 08/09/2009 21:40:28 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Visit_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[Visits]'))
ALTER TABLE [dbo].[Visits] DROP CONSTRAINT [FK_Visit_Patient]
GO
/****** Object:  Default [DF_Conditions_OrderKey]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Conditions_OrderKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[Conditions]'))
Begin
ALTER TABLE [dbo].[Conditions] DROP CONSTRAINT [DF_Conditions_OrderKey]

End
GO
/****** Object:  Default [DF_OrganExamination_ID]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [DF_OrganExamination_ID]

End
GO
/****** Object:  Default [DF_OrganExamination_Value]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_Value]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [DF_OrganExamination_Value]

End
GO
/****** Object:  Default [DF_Table_1_Operation]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Table_1_Operation]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [DF_Table_1_Operation]

End
GO
/****** Object:  Default [DF_Table_1_HronicDesease]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Table_1_HronicDesease]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [DF_Table_1_HronicDesease]

End
GO
/****** Object:  Default [DF_OrganExamination_Removed]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_Removed]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [DF_OrganExamination_Removed]

End
GO
/****** Object:  Default [DF_OrganExamination_Implant]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_Implant]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] DROP CONSTRAINT [DF_OrganExamination_Implant]

End
GO
/****** Object:  Default [DF_Organs_OrderKey]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Organs_OrderKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[Organs]'))
Begin
ALTER TABLE [dbo].[Organs] DROP CONSTRAINT [DF_Organs_OrderKey]

End
GO
/****** Object:  Default [DF_Patient_Id]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Patient_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
Begin
ALTER TABLE [dbo].[Patients] DROP CONSTRAINT [DF_Patient_Id]

End
GO
/****** Object:  Default [DF_Patient_LastUpdDate]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Patient_LastUpdDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
Begin
ALTER TABLE [dbo].[Patients] DROP CONSTRAINT [DF_Patient_LastUpdDate]

End
GO
/****** Object:  Default [DF_Patients_Deleted]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Patients_Deleted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
Begin
ALTER TABLE [dbo].[Patients] DROP CONSTRAINT [DF_Patients_Deleted]

End
GO
/****** Object:  Default [DF_Practitioner_Id]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Practitioner_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Practitioners]'))
Begin
ALTER TABLE [dbo].[Practitioners] DROP CONSTRAINT [DF_Practitioner_Id]

End
GO
/****** Object:  Default [DF_Prescriptions_ID]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Prescriptions_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Prescriptions]'))
Begin
ALTER TABLE [dbo].[Prescriptions] DROP CONSTRAINT [DF_Prescriptions_ID]

End
GO
/****** Object:  Default [DF_Symptoms _OrderKey]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Symptoms _OrderKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[Symptoms ]'))
Begin
ALTER TABLE [dbo].[Symptoms ] DROP CONSTRAINT [DF_Symptoms _OrderKey]

End
GO
/****** Object:  Default [DF_Visits_Id]    Script Date: 08/09/2009 21:40:28 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Visits_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Visits]'))
Begin
ALTER TABLE [dbo].[Visits] DROP CONSTRAINT [DF_Visits_Id]

End
GO
/****** Object:  Default [DF_Visits_Date]    Script Date: 08/09/2009 21:40:28 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Visits_Date]') AND parent_object_id = OBJECT_ID(N'[dbo].[Visits]'))
Begin
ALTER TABLE [dbo].[Visits] DROP CONSTRAINT [DF_Visits_Date]

End
GO
/****** Object:  StoredProcedure [dbo].[GetMedicineIdByOrganCondition]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMedicineIdByOrganCondition]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetMedicineIdByOrganCondition]
GO
/****** Object:  StoredProcedure [dbo].[GetConditionByValue]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetConditionByValue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetConditionByValue]
GO
/****** Object:  StoredProcedure [dbo].[InsertVisit]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertVisit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertVisit]
GO
/****** Object:  StoredProcedure [dbo].[InsertSymptomVisitXRef]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertSymptomVisitXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertSymptomVisitXRef]
GO
/****** Object:  StoredProcedure [dbo].[DeleteSymptomVisitXRef]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteSymptomVisitXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteSymptomVisitXRef]
GO
/****** Object:  StoredProcedure [dbo].[CreateNewVisit]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateNewVisit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CreateNewVisit]
GO
/****** Object:  Table [dbo].[Prescriptions]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Prescriptions]') AND type in (N'U'))
DROP TABLE [dbo].[Prescriptions]
GO
/****** Object:  Table [dbo].[SymptomVisitXRef]    Script Date: 08/09/2009 21:40:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SymptomVisitXRef]') AND type in (N'U'))
DROP TABLE [dbo].[SymptomVisitXRef]
GO
/****** Object:  Table [dbo].[OrganExamination]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganExamination]') AND type in (N'U'))
DROP TABLE [dbo].[OrganExamination]
GO
/****** Object:  Table [dbo].[Visits]    Script Date: 08/09/2009 21:40:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Visits]') AND type in (N'U'))
DROP TABLE [dbo].[Visits]
GO
/****** Object:  Table [dbo].[Patients]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Patients]') AND type in (N'U'))
DROP TABLE [dbo].[Patients]
GO
/****** Object:  Table [dbo].[Conditions]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Conditions]') AND type in (N'U'))
DROP TABLE [dbo].[Conditions]
GO
/****** Object:  Table [dbo].[Medicine]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Medicine]') AND type in (N'U'))
DROP TABLE [dbo].[Medicine]
GO
/****** Object:  StoredProcedure [dbo].[GetMedicineByChronicOrgan]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMedicineByChronicOrgan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetMedicineByChronicOrgan]
GO
/****** Object:  StoredProcedure [dbo].[DeleteVisit]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteVisit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteVisit]
GO
/****** Object:  StoredProcedure [dbo].[UpdateVisit]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateVisit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateVisit]
GO
/****** Object:  StoredProcedure [dbo].[UpdateMedicineOrganConditionXRef]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateMedicineOrganConditionXRef]
GO
/****** Object:  StoredProcedure [dbo].[DeleteMedicineOrganConditionXRef]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteMedicineOrganConditionXRef]
GO
/****** Object:  StoredProcedure [dbo].[InsertMedicineOrganConditionXRef]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertMedicineOrganConditionXRef]
GO
/****** Object:  StoredProcedure [dbo].[DeleteMedicineSymptomXRef]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteMedicineSymptomXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteMedicineSymptomXRef]
GO
/****** Object:  StoredProcedure [dbo].[InsertMedicineSymptomXRef]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertMedicineSymptomXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertMedicineSymptomXRef]
GO
/****** Object:  Table [dbo].[Practitioners]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Practitioners]') AND type in (N'U'))
DROP TABLE [dbo].[Practitioners]
GO
/****** Object:  Table [dbo].[Chakras]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Chakras]') AND type in (N'U'))
DROP TABLE [dbo].[Chakras]
GO
/****** Object:  Table [dbo].[Electrodes]    Script Date: 08/31/2009 14:04:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Electrodes]') AND type in (N'U'))
DROP TABLE [dbo].[Electrodes]
GO
/****** Object:  Table [dbo].[Changes]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Changes]') AND type in (N'U'))
DROP TABLE [dbo].[Changes]
GO
/****** Object:  Table [dbo].[Errors]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Errors]') AND type in (N'U'))
DROP TABLE [dbo].[Errors]
GO
/****** Object:  StoredProcedure [dbo].[InsertError]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertError]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertError]
GO
/****** Object:  Table [dbo].[Symptoms ]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Symptoms ]') AND type in (N'U'))
DROP TABLE [dbo].[Symptoms ]
GO
/****** Object:  Table [dbo].[Organs]    Script Date: 08/09/2009 21:40:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Organs]') AND type in (N'U'))
DROP TABLE [dbo].[Organs]
GO
/****** Object:  Table [dbo].[SyncItems]    Script Date: 08/09/2009 21:40:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SyncItems]') AND type in (N'U'))
DROP TABLE [dbo].[SyncItems]
GO
/****** Object:  Role [AdviserClientDBUser]    Script Date: 08/09/2009 21:40:28 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'AdviserClientDBUser')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'AdviserClientDBUser' AND type = 'R')
CREATE ROLE [AdviserClientDBUser]

END
GO
/****** Object:  Table [dbo].[SyncItems]    Script Date: 08/09/2009 21:40:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SyncItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SyncItems](
	[KeyValue] [uniqueidentifier] NOT NULL,
	[EntityType] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ActionId] [tinyint] NOT NULL,
	[StatusId] [tinyint] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_SyncItems] PRIMARY KEY CLUSTERED 
(
	[KeyValue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[Organs]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Organs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Organs](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL,
	[Gender] [int] NULL,
	[Description] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OrderKey] [int] NOT NULL,
	[Abbreviation] [nvarchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Organs] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (1, N'Nervous System', 0, NULL, 1, N'Ns')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (2, N'Brain', 0, NULL, 2, N'Bn')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (3, N'Cerebellum', 0, NULL, 3, N'Cb')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (4, N'Pituitary Gland', 0, NULL, 4, N'Py')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (5, N'Thyroid Gland', 0, NULL, 5, N'Td')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (6, N'Adrenal Gland', 0, NULL, 6, N'Al')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (7, N'Pancreas', 0, NULL, 7, N'Ps')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (8, N'Liver', 0, NULL, 8, N'Lr')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (9, N'Gallbladder', 0, NULL, 9, N'Gb')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (10, N'Stomach', 0, NULL, 10, N'Sc')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (11, N'Duodenum', 0, NULL, 11, N'Dm')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (12, N'Small Intestine', 0, NULL, 12, N'Si')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (13, N'Large Intestine', 0, NULL, 13, N'Li')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (14, N'Spleen', 0, NULL, 14, N'Sn')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (15, N'Heart', 0, NULL, 15, N'Ht')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (16, N'Arteries', 0, NULL, 16, N'As')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (17, N'Veins', 0, NULL, 17, N'Vs')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (18, N'Lymphatic System', 0, NULL, 18, N'Ls')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (19, N'Lungs', 0, NULL, 19, N'Lu')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (20, N'Tonsils', 0, NULL, 20, N'To')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (21, N'Mammary', 2, NULL, 21, N'Mm')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (22, N'Testicles', 1, NULL, 22, N'Ts')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (23, N'Prostate Gland', 1, NULL, 23, N'Pg')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (24, N'Uterus', 2, NULL, 24, N'Us')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (25, N'Kidney', 0, NULL, 25, N'Ks')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (26, N'Urinary Bladder', 0, NULL, 26, N'Ub')
INSERT [dbo].[Organs] ([ID], [Name], [Gender], [Description], [OrderKey], [Abbreviation]) VALUES (28, N'Ovaries', 2, NULL, 22, N'Ov')
/****** Object:  Table [dbo].[Symptoms ]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Electrodes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Electrodes](
	[Number] [int] NOT NULL,
	[Name] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsFront] [bit] NOT NULL
)
END
GO
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (1, N'Sahasrara', N'Place of connection of  Frontal bone and right and left Parietal bone', 1)
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (1, N'Sahasrara', N'Place of connection of right and left Parietal bone and Occipital bone', 0)
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (2, N'Ajna', N'Middle forehead', 1)
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (2, N'Ajna', N'1-2 cm lower External occipital protuberance', 0)
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (3, N'Vishuddha', N'1-2 cm above Manubrium', 1)
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (3, N'Vishuddha', N'1-2 cm above Vertebra prominens', 0)
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (4, N'Anahata', N'1-2 cm lower Xiphoid process', 1)
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (4, N'Anahata', N'Between V and VI Thoracic  vertebrae', 0)
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (5, N'Manipura', N'4-6  cm above Umbilicus', 1)
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (5, N'Manipura', N'Between - XII Thoracic vertebrae and I Lumbar vertebrae', 0)
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (6, N'Swadhisthana', N'4-6 cm lower Umbilicus', 1)
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (6, N'Swadhisthana', N'Between IV and V Lumbar vertebrae', 0)
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (7, N'Muladhara', N'Pubic symphysis middle line', 1)
INSERT [dbo].[Electrodes] ([Number], [Name], [Description], [IsFront]) VALUES (7, N'Muladhara', N'Sacrum transition into Coccyx', 0)
/****** Object:  Table [dbo].[SyncItems]    Script Date: 08/31/2009 14:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Symptoms ]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Symptoms ](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL,
	[Description] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CS_AS NULL,
	[OrderKey] [int] NOT NULL,
 CONSTRAINT [PK_Symptoms _1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (1, N'Headache', NULL, 1)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (2, N'Vertigo', NULL, 2)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (3, N'Sore throat', NULL, 3)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (4, N'Chest pain', NULL, 4)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (5, N'Muscle pain', NULL, 5)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (6, N'Joint pain', NULL, 6)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (7, N'Abdominal pain', NULL, 7)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (8, N'Back pain', NULL, 8)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (9, N'Cough', NULL, 9)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (10, N'Rhinitis', NULL, 10)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (11, N'Urination problem', NULL, 11)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (12, N'Bleeding', NULL, 12)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (13, N'Diarrhea', NULL, 13)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (14, N'Sweating', NULL, 15)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (15, N'Skin disorders', NULL, 16)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (16, N'Fever', NULL, 17)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (17, N'Bulimia', NULL, 18)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (18, N'Anorexia', NULL, 19)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (19, N'Depression', NULL, 20)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (20, N'Anxiety', NULL, 21)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (21, N'Weakness', NULL, 22)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (22, N'Sleep disorders', NULL, 23)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (23, N'Nausea, vomiting', NULL, 24)
INSERT [dbo].[Symptoms ] ([ID], [Name], [Description], [OrderKey]) VALUES (24, N'Constipation', NULL, 14)
/****** Object:  StoredProcedure [dbo].[InsertError]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertError]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[InsertError] 
@Data varbinary(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


INSERT INTO Errors
           ([Time],[Details], [Status])
     VALUES
           (GetUtcDate(), @Data, ''Enqueue'')
END
' 
END
GO
/****** Object:  Table [dbo].[Errors]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Errors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Errors](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Time] [datetime] NULL,
	[Data] [varbinary](max) NULL,
	[Lock] [bit] NOT NULL,
 CONSTRAINT [PK_Errors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[Changes]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Changes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Changes](
	[ID] [uniqueidentifier] NOT NULL,
	[EntitySet] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[State] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Timestamp] [datetime] NULL,
 CONSTRAINT [PK_Changes_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[Chakras]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Chakras]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Chakras](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShortDescription] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AboveDesc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BelowDesc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MandalaImage] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MandalaSound] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Recommendations] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ExpectedResults] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Influence] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Chakras] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (1, N'Muladhara', N'The Root Chakra', N'Muladhara or root chakra is related to instinct, security, survival and also to basic human potentiality. This centre is located in the region between the genitals and the anus. Although no endocrine organ is placed here, it is said to relate to the gonads and the adrenal medulla, responsible for the fight and flight response when survival is under threat. There is a muscle located in this region that controls ejaculation in the sexual act of the human male. A parallel is charted between the sperm cell and the ovum where the genetic code lies coiled and the kundalini. Muladhara is symbolised by a lotus with four petals and the colour red. Key issues involve sexuality, lust and obsession. Physically, Muladhara governs sexuality, mentally it governs stability, emotionally it governs sensuality, and spiritually it governs a sense of security.', N'Stressful state, possibilities of being overwhelmed and sexual dissatisfied.  Feelings of fear and agitation without specific cause.  Distress about loved ones, especially for children.  High probability of conflict situations.  Potential hypochondriac.', N'Depressed state, decrease in life interests.  Feelings of sadness, fatigue and disappointment.  Possibility of conflict situations within the family that has not been resolved for a long period of time.  Inability to express one’s point of view.', N'/Advisor.Modules.Interpretation;component/Resources/m1_Muladhara.jpg', N'Resources/m1_Muladhara.mp3', N'Meditate with Muladhara video in the mornings, 2-3 times a week for 30-40 minutes per session.', N'Energetic balance of Muladhara parameters lead to improved sleep and alertness while decreasing or removing stress, especially if stress is the result of a conflict situation.  Improvements toward quality of life include higher energy levels, discovering new interests and enhanced sexual activity.' ,N'Mandala Muladhara positively influences reproductive organs, particularly prostate in men and ovaries in women.  It also helps to improves functions of the large intestine.')
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (2, N'Swadhisthana', N'The Sacral Chakra', N'Swadhisthana, Svadisthana or adhishthana is located in the sacrum. It is considered to correspond to the testes or the ovaries that produce the various sex hormones involved in the reproductive cycle. Svadisthana is also considered to be related to, more generally, the genitourinary system and the adrenals. The Sacral Chakra is symbolized by a lotus with six petals, and corresponds to the colour orange. The key issues involving Svadisthana are relationships, violence, addictions, basic emotional needs, and pleasure. Physically, Svadisthana governs reproduction, mentally it governs creativity, emotionally it governs joy, and spiritually it governs enthusiasm.', N'Disordered way of life, desire to reach impossible feats.  Abuse of power and unrealistic expectations from others in personal, social and professional life.  Possibility of over indulging and accomplishing one’s purpose at any cost.  Probable aggressive personality.', N'Low self-esteem, decrease in physical needs.  Desire to keep a sheltered life.  Inability to resist peer pressure from others with an active way of life.  Cranky and pessimistic mood, could be susceptible to addictions.', N'/Advisor.Modules.Interpretation;component/Resources/m2_Svadisthana.jpg', N'Resources/m2_Svadisthana.mp3', N'Meditate with Swadhisthana video in the mornings, 2-3 times a week for 20-30 minutes per session.' ,N'Energetic balance of Swadhisthana parameters helps to restore an active way of life.  Overtime you will notice life becoming more organized, a chance to rid yourself of bad habits, with time your attitude towards yourself and others will change and you will become more aware of how others perceive you.' ,N'Mandala Swadhisthana positively influence the lymphatic system and helps to improve the function of the intestines and kidneys.')
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (3, N'Manipura', N'The Solar Plexus Chakra', N'Manipura or manipuraka is related to the metabolic and digestive systems. Manipura is believed to correspond to Islets of Langerhans,which are groups of cells in the pancreas, as well as the outer adrenal glands and the adrenal cortex. These play a valuable role in digestion, the conversion of food matter into energy for the body. Symbolised by a lotus with ten petals. The colour that corresponds to Manipura is yellow. Key issues governed by Manipura are issues of personal power, fear, anxiety, opinion-formation, introversion, and transition from simple or base emotions to complex. Physically, Manipura governs digestion, mentally it governs personal power, emotionally it governs expansiveness, and spiritually, all matters of growth.', N'Excessive scrupulousness and cleanliness that may irritate others.  Desire to impose their point of view, which creates conflict situation.  Meticulous in relations with financial partners.', N'Incapable to concentrate and complete tasks. Incomprehension from surrounding people. Untidiness, in some situations negligent.  Inability to manage financial resources, which may be expressed as greedy.', N'/Advisor.Modules.Interpretation;component/Resources/m3_Manipura.jpg', N'Resources/m3_Manipura.mp3', N'Meditate with Manipura video in the mornings as the sun rises, 2-3 times a week for 30-40 minutes per session.' ,N'Energetic balance of Manipura paramerters helps to inspire positive feelings.  Secondary problems or compulsions, which conflict your lifestyle will dissipate as your thoughts become lighter.  Overtime you will begin to focus on your contributions in life.  As your stressors reduce your outlook will improve and these feelings will have an overall positive reaction to your personal, financial and professional life.' ,N'Mandala Manipura positively influences the digestive system and improves function of liver.')
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (4, N'Anahata', N'The Heart Chakra', N'Anahata, or Anahata-puri, or padma-sundara is related to the thymus, located in the chest. The thymus is an element of the immune system as well as being part of the endocrine system. It produces the T cells responsible for fending off disease and may be adversely affected by stress. Anahata is symbolised by a lotus flower with twelve petals. Anahata is related to the colours green or pink. Key issues involving Anahata involve complex emotions, compassion, tenderness, unconditional love, equilibrium, rejection and well being. Physically Anahata governs circulation, emotionally it governs unconditional love for the self and others, mentally it governs passion, and spiritually it governs devotion.', N'Excessive gullibility.  Unnecessary display of love toward others, which may irritate them.  Carelessness and lack of common sense in relations with people, particularly with those who may take advantage of you for their own gain.  Sometimes unreasonable self-confidence, which exposes you to more risk.', N'Undivided love to people, which cause misunderstanding from their side. Therefore, possibilities of break-down, depression and unwillingness to struggle for existence and to defend one’s own point of view.', N'/Advisor.Modules.Interpretation;component/Resources/m4_Anahata.jpg', N'Resources/m4_Anahata.mp3', N'Meditate with Anahata video during day time hours, every other day for 40-60 minutes per session.' ,N'Energetic balance of Anahata parameters help to eliminate depression, if present.  Helps to improve social well being and relationships between people, emotional frustrations withdraw after sessions of meditation.  Feeling of satisfaction and quality of life are improved, as well.' ,N'Mandala Anahata positively influences the lungs and heart to improve their function, also improving blood circulation.')
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (5, N'Vishuddha', N'The Throat Chakra', N'Vishuddha (also Vishuddhi) is related to communication and growth through expression. This chakra is paralleled to the thyroid, a gland that is also in the throat and which produces thyroid hormone, responsible for growth and maturation. Symbolised by a lotus with sixteen petals. Vishudda is characterized by the color light or pale blue, or turquoise. It governs such issues as self-expression and communication, as discussed above. Physically, Vishuddha governs communication, emotionally it governs independence, mentally it governs fluent thought, and spiritually, it governs a sense of security.', N'Tendency to make a decision quickly without much thought regarding one’s action.  Desire to suppress others, including their initiatives.  Too high self-esteem.  Genius with a large ego.  Prejudice towards others of the opposite sex.', N'Dissatisfaction with oneself.  Tendency to gossip and discuss weaknesses of others, unaware of how others are offended.  Excessive pathological diligence in carrying out tasks to the point of becoming absurd.  Passive attitude toward persons of the opposite sex.', N'/Advisor.Modules.Interpretation;component/Resources/m5_Vishuddha.jpg', N'Resources/m5_Vishuddha.mp3', N'Meditate with Vishuddha video during the afternoon, 3-4 times a week for 30-40 minutes per session.' ,N'Energetic balance of Vishuddha parameters help to remove tension and frustrations with yourself and others.  Helps you become calm and less argumentative.  Meditation will enhance your ability to focus and concentrate.' ,N'Mandala Vishuddha positively influences the lymph circulation, improves the immune system and assists with properly balancing the endocrine system.')
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (6, N'Ajna', N'The Brow Chakra', N'Ajna is linked to the pineal gland which may inform a model of its envisioning. The pineal gland is a light sensitive gland that produces the hormone melatonin which regulates sleep and awakening. Ajna is symbolised by a lotus with two petals, and corresponds to the colour white, indigo or deep blue. Ajna''s key issues involve balancing the higher & lower selves and trusting inner guidance. Ajna''s inner aspect relates to the access of intuition. Emotionally, Ajna deals with clarity on an intuitive level.', N'Very high self-esteem and egoism.  Aspiration to suppress other people by means if knowledge received previously.  Considers himself above everything and everyone.  At the same time may lack focus on primary problems and exerting all energies on secondary problems.  Attitude towards others are prejudicial and sometimes cruel, which may create conflict situations.', N'As a rule these people are preoccupied with their own problems, which become a distraction to their thoughts and abilities to concentrate.  They have a tendency to feel that some trouble may happen soon.  Suspicious, usually awaiting mischief from other people.  As a rule they live in a world of illusions.', N'/Advisor.Modules.Interpretation;component/Resources/m6_Ajna.jpg', N'Resources/m6_Ajna.mp3', N'Meditate with Ajna video during the evening time, every other day for 30-40 minutes per session.' ,N'Energetic balance of Ajna parameters help to improve concentration and inspire positive feelings.  Secondary problems or compulsions, which conflict your lifestyle will dissipate as your thoughts become lighter.  Overtime you will feel more relaxed as your soul becomes more balanced.  As your stressors reduce, your love and trust towards people will improve.' ,N'Mandala Ajna positively influences the pituitary gland, increases resistance toward stress and balancing functions of the nervous system.')
INSERT [dbo].[Chakras] ([ID], [Name], [ShortDescription], [Description], [AboveDesc], [BelowDesc], [MandalaImage], [MandalaSound], [Recommendations], [ExpectedResults], [Influence]) VALUES (7, N'Sahasrara', N'The Crown Chakra', N'Sahasrara is the chakra of pure consciousness. Its role may be envisioned somewhat similarly to that of the pituitary gland, which secretes hormones to communicate to the rest of the endocrine system and also connects to the central nervous system via the hypothalamus. The thalamus is thought to have a key role in the physical basis of consciousness. Symbolized by a lotus with one thousand petals, it is located at the crown of the head. Sahasrara is represented by the colour violet and it involves such issues as inner wisdom and the death of the body.Sahasrara''s inner aspect deals with the release of karma, physical action with meditation, mental action with universal consciousness and unity, and emotional action with "beingness".', N'Dictator, excessive need to control and manage people.  Necessity to be at the head of all events.  Intruding into the private space of people around them, imposing their point of view, which may cause resistance and inadequate reactions from others.', N'Superstitious.  Permanently searching for the truth.  Exhibits excessive trustworthiness, naivety and easily upset.  Very sensitive and vulnerable personality requiring a tender attitude from others.  In some situations feelings towards life is that of a teenager, which is expressed as either black or white, without compromise.', N'/Advisor.Modules.Interpretation;component/Resources/m7_Sahasrara.jpg', N'Resources/m7_Sahasrara.mp3', N'Meditate with Sahasrara video before bedtime, 1-2 times a week, for 20-60 minutes per session.  Increasing frequency and duration is determined by your mood and amount of restlessness.' ,N'Energetic balance of Sahasrara parameters helps to relax and reenergize the body.  Helps to eliminate stress and depression, feelings of worry and fear dissipate.  Improved sleep will allow you to feel more rested in the morning.  After a period of time you will see improvements in your personal, social and professional life.' ,N'Mandala Sahasrara positively influences the brain, relaxes the nervous system and improves blood circulation in the entire body.  Over time may strengthen your intuition.')
/****** Object:  Table [dbo].[Practitioners]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Practitioners]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Practitioners](
	[ID] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MidName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BusinessName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TypeOfPractice] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[StreetAddress] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[City] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[StateProvince] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PostalCode] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Country] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Ocupation] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PrimaryPhone] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SecondaryPhone] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Fax] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Password] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StatusID] [smallint] NOT NULL,
	[LastUpdDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Practitioner] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  StoredProcedure [dbo].[InsertMedicineSymptomXRef]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertMedicineSymptomXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[InsertMedicineSymptomXRef] 
@MedicineID int, 
@SymptomID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


INSERT INTO [MedicineSymptomXRef]
           ([MedicineID]
           ,[SymptomID])
     VALUES
           (@MedicineID
           ,@SymptomID)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteMedicineSymptomXRef]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteMedicineSymptomXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DeleteMedicineSymptomXRef] 
@MedicineID int, 
@SymptomID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DELETE FROM MedicineSymptomXRef
      WHERE ([MedicineID] = @MedicineID
           AND [SymptomID] = @SymptomID)
   
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[InsertMedicineOrganConditionXRef]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[InsertMedicineOrganConditionXRef] 
@MedicineID int, 
@OrganID int,
@ConditionID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


INSERT INTO [MedicineOrganConditionXRef]
           ([MedicineID]
           ,[OrganID]
		   ,[ConditionID])
     VALUES
           (@MedicineID
           ,@OrganID
		   ,@ConditionID)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteMedicineOrganConditionXRef]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[DeleteMedicineOrganConditionXRef] 
@MedicineID int, 
@OrganID int,
@ConditionID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DELETE FROM [MedicineOrganConditionXRef]
	WHERE([MedicineID] = @MedicineID
		AND [OrganID] = @OrganID
		AND [ConditionID] = @ConditionID)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateMedicineOrganConditionXRef]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[UpdateMedicineOrganConditionXRef] 
@MedicineID int, 
@OrganID int,
@ConditionID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

-- This procedure is created just to work around Entity Framework limitation with mapping tables
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateVisit]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateVisit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateVisit] 
	@ID [uniqueidentifier] ,
	@Date [smalldatetime],
	@LastUpdDate [smalldatetime],
	@PatientId [uniqueidentifier],
	@MeasurementData [varbinary] (max),
	@ExamResult [varbinary] (max),
	@Notes [nvarchar] (max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE [Visits]
   SET [Date] = @Date
      ,[LastUpdDate] = getdate()
      ,[PatientId] = @PatientId
      ,[MeasurementData] = @MeasurementData
      ,[ExamResult] = @ExamResult
      ,[Notes] = @Notes
 WHERE ID = @ID

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteVisit]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteVisit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteVisit] 
	@ID [uniqueidentifier],
	@PatientID [uniqueidentifier]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DELETE FROM Visits
 WHERE ID = @ID

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetMedicineByChronicOrgan]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMedicineByChronicOrgan]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetMedicineByChronicOrgan]
	@OrganID int
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT [MedicineID]
      ,[OrganID]
  FROM [MedicineChronicOrgan]
	WHERE [OrganID] = @OrganID
	
END
' 
END
GO
/****** Object:  Table [dbo].[Medicine]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Medicine]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Medicine](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL,
	[Description] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Direction] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Medicine_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Medicine] ON
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (1, N'Abopernol N', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (2, N'Abrotanum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (3, N'Acetylsalicylsaure-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (4, N'Acidum a-Ketoglutaricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (5, N'Acidum citricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (6, N'Acidum formicicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (7, N'Acidum fumaricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (8, N'Acidum lacticum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (9, N'Acidum nitricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (10, N'Acidum oxalicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (11, N'Acidum phosphoricum-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (12, N'Acidum succinicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (13, N'Acidum sulfuricum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (14, N'Acidum uricum-injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (15, N'Aconitum-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (16, N'Acontium-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (17, N'Adonis vernalis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (18, N'Adrenalin-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (19, N'Aesculus compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (20, N'Aesculus-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (21, N'Aesculus-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (22, N'Agaricus-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (23, N'Agnus castus-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (24, N'Albumoheel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (25, N'Alertis-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (26, N'Aloe-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (27, N'Amygdeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (28, N'Anacardium-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (29, N'Angeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (30, N'Antimonium crudum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (31, N'Apis-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (32, N'Areel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (33, N'Argentum nitricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (34, N'Arnica-Heel comp. ', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (35, N'Arnica-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (36, N'Arsenicum album-Injeel forte S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (37, N'Artis-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (38, N'Astricumeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (39, N'Ateria-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (40, N'Aurumheel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (41, N'Barijodeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (42, N'Baryum carbonicum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (43, N'Belladonna-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (44, N'Belladonna-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (45, N'Berberis vulgaris-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (46, N'Berberis-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (47, N'Bronkeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (48, N'Bryaconeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (49, N'Bryonia-Injeel forte S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (50, N'Cactus-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (51, N'Calcium carbonicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (52, N'Calcium fluoratum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (53, N'Calcium phosphoricum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (54, N'Calcium sulfuricum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (55, N'Calcoheel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (56, N'cAMP', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (57, N'Cantharis composium S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (58, N'Cantharis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (59, N'Carbo vegetabilis-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (60, N'Cardiacum-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (61, N'Carduus marianus-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (62, N'Cartilago suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (63, N'Causticum compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (64, N'Causticum-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (65, N'Ceanothus-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (66, N'Chamomilla-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (67, N'Chelidonium-Homaccord P', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (68, N'Chelidonium-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (69, N'China-Homaccord S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (70, N'Cholesterinum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (71, N'Chol-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (72, N'Cimicifuga-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (73, N'Cinnamomum-Homaccord N', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (74, N'Circulo-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (75, N'Cistus canadensis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (76, N'Cocculus-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (77, N'Cocculus-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (78, N'Coffea-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (79, N'Colchium compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (80, N'Colchium-Injeel forte S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (81, N'Colocynthis-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (82, N'Conicum-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (83, N'Cortison-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (84, N'Cralonin', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (85, N'Cruroheel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (86, N'Cuprum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (87, N'Curare-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (88, N'Cutisium', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (89, N'Cynara scolymus-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (90, N'Discompeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (91, N'Droperteel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (92, N'Drosera-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (93, N'Duclamara-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (94, N'Duodenoheel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (95, N'Echinacea angustifolia-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (96, N'Echinacea compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (97, N'Engystol', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (98, N'Eupatorium perfoliatum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (99, N'Euphorbium compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (100, N'Euphrasia-Injeel', NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (101, N'Ferrum metallicum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (102, N'Ferrum phosphoricum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (103, N'Ferrum-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (104, N'Funiculus umbilicalis suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (105, N'Galium-Heel comp.', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (106, N'Gelsemium-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (107, N'Ginkgo-comp.-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (108, N'Ginseng-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (109, N'Glandula suprarenalis suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (110, N'Glyoxal compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (111, N'Gnaphalium polycephalum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (112, N'Graphites-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (113, N'Graphites-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (114, N'Gripp-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (115, N'Hamamelis-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (116, N'Hamamelis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (117, N'Hekla lava-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (118, N'Hepar compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (119, N'Hepar suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (120, N'Hepar sulfuris-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (121, N'Hapeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (122, N'Histamin-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (123, N'Hormeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (124, N'Husteel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (125, N'Hydrastis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (126, N'Hyoscyamus-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (127, N'Hypericum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (128, N'Hypothalamus suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (129, N'Ignatia-Homoccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (130, N'Ipecacuanha-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (131, N'Iris-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (132, N'Jaborandi-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (133, N'Kalium bichromicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (134, N'Kalium carbonicum-Injeel forte N', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (135, N'Kalmia compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (136, N'Kalmia-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (137, N'Klimakt-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (138, N'Kreosotum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (139, N'Lachesis-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (140, N'Ledum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (141, N'Leptandra compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (142, N'Leptandra- Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (143, N'Lilium tigrinum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (144, N'Lithiumeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (145, N'Luffeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (146, N'Luffa operculata-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (147, N'Lycopodium-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (148, N'Lymphomyosot', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (149, N'Magnesium manganum phosphoricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (150, N'Magnesium phosphoricum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (151, N'Medorrhinum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (152, N'Medulla ossis suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (153, N'Melilotus-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (154, N'Mercurius-Heel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (155, N'Metro-Adnex-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (156, N'Mezereum-Homacord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (157, N'Momordica balsamina-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (158, N'Momordica compositum N', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (159, N'Myristica sebifera-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (160, N'Nareel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (161, N'Natrium muriaticum-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (162, N'Natrium oxalaceticum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (163, N'Natrium pyruvicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (164, N'Natrium sulfuricum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (165, N'Natrium-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (166, N'Nervoheel N', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (167, N'Neralgo-Rhem-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (168, N'Neuro-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (169, N'Nux vomica-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (170, N'Nux vomica-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (171, N'Nymeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (172, N'Osteel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (173, N'Paeonia-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (174, N'Pankreas suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (175, N'Petroleum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (176, N'Phosphor-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (177, N'Phosphorus-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (178, N'Phytolacca-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (179, N'Placenta suis-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (180, N'Plantago-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (181, N'Platinum metallicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (182, N'Podophyllum compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (183, N'Pulsatilla compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (184, N'Pulsatilla-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (185, N'Ranunculus-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (186, N'Reneel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (187, N'Rhododendroneel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (188, N'Rhododendron-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (189, N'Rhus tox-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (190, N'Ruta-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (191, N'Sabal-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (192, N'Sanguinaria-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (193, N'Schwef-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (194, N'Secale cornutum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (195, N'Selenium-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (196, N'Selenium-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (197, N'Sepia-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (198, N'Silicea-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (199, N'Sinusitis nosode-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (200, N'Sorinoheel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (201, N'Spascupreel', NULL, NULL)
GO
print 'Processed 200 total records'
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (202, N'Spigelon', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (203, N'Spongia-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (204, N'Staphisagria-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (205, N'Staphylococcus-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (206, N'Sticta-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (207, N'Stramonium-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (208, N'Streptococcus haemolyticus-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (209, N'Strumeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (210, N'Sulfur-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (211, N'Sulfur-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (212, N'Syzygium compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (213, N'Tabacum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (214, N'Tanacet-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (215, N'Taraxacum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (216, N'Tartephedreel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (217, N'Thuja-Injeel S', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (218, N'Tonico-Heel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (219, N'Traumeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (220, N'Ubicoenzyme', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (221, N'Vaccininum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (222, N'Valerianaheel comp.', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (223, N'Valeriana-Injeel forte', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (224, N'Ventigoheel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (225, N'Veratrum-Homaccord', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (226, N'Vincetoxicum-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (227, N'Visceel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (228, N'Viscum album-Injeel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (229, N'Viscum compositum', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (230, N'Vomitusheel', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (231, N'Ypsiloheel N', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (232, N'Zeel comp.', NULL, NULL)
INSERT [dbo].[Medicine] ([ID], [Name], [Description], [Direction]) VALUES (233, N'Zincum metallicum-Injeel', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Medicine] OFF
/****** Object:  Table [dbo].[Conditions]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Conditions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Conditions](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL,
	[MinRange] [int] NULL,
	[MaxRange] [int] NULL,
	[OrderKey] [int] NOT NULL,
	[ColorCode] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL,
	[Description] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Conditions_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Conditions] ON
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (0, N'Exhaustion', 0, 45, 1, N'#5e005d', N'Depleted energy of an organ. High possibilities of a decline in the metabolic process in organ tissues.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (2, N'Fatigued', 45, 60, 2, N'#7522e5', N'Excessive decrease in energetic levels of an organ. Functional decline of an organ is possible.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (3, N'Weakened', 60, 75, 3, N'#046bde', N'Decrease of energetic parameters and biological activity of an organ.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (4, N'Relaxed', 75, 90, 4, N'#35e5f5', N'Moderate decrease of organ activity.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (6, N'Normal', 90, 110, 5, N'#9fd998', N'Optimum energetic balance of an organ.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (7, N'Agitated', 110, 125, 6, N'#f4eb56', N'Moderate increase of organ activity.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (9, N'Strained', 125, 140, 7, N'#ff987c', N'Increasing energetic parameters and biological activity of an organ.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (10, N'Tense', 140, 155, 8, N'#c33e0a', N'Energetic level of organ is very high. Functional activity and metabolic process is increasing.')
INSERT [dbo].[Conditions] ([ID], [Name], [MinRange], [MaxRange], [OrderKey], [ColorCode], [Description]) VALUES (11, N'Overload', 155, 200, 9, N'#8f0000', N'Excessive and increasing energetic activity of an organ.  Physiological possibilities of the working organ are reaching functional limits. ')
SET IDENTITY_INSERT [dbo].[Conditions] OFF
/****** Object:  Table [dbo].[Patients]    Script Date: 06/16/2009 21:01:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Patients]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Patients](
	[ID] [uniqueidentifier] NOT NULL,
	[PractitionerId] [uniqueidentifier] NULL,
	[LastUpdDate] [datetime] NOT NULL,
	[FirstName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MidName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StreetAddress] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StateProvince] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PostalCode] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Occupation] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PrimaryPhone] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SecondaryPhone] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DOB] [datetime] NULL,
	[Sex] [nchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Height] [int] NULL,
	[Weight] [int] NULL,
	[Lifestyle] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MajorComplain] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MajorIllnessId] [int] NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[Visits]    Script Date: 08/09/2009 21:40:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Visits]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Visits](
	[ID] [uniqueidentifier] NOT NULL,
	[Date] [smalldatetime] NOT NULL,
	[LastUpdDate] [smalldatetime] NOT NULL,
	[PatientId] [uniqueidentifier] NOT NULL,
	[MeasurementData] [varbinary](max) NULL,
	[ExamResult] [varbinary](max) NULL,
	[Notes] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Deleted] [bit] NULL,
 CONSTRAINT [PK_Visit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[OrganExamination]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganExamination]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OrganExamination](
	[ID] [uniqueidentifier] NOT NULL,
	[VisitID] [uniqueidentifier] NOT NULL,
	[OrganID] [int] NOT NULL,
	[Value] [int] NOT NULL,
	[Surgery] [bit] NOT NULL,
	[ChronicDisease] [bit] NOT NULL,
	[Removed] [bit] NOT NULL,
	[Implant] [bit] NOT NULL,
 CONSTRAINT [PK_OrganExamination] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[SymptomVisitXRef]    Script Date: 08/09/2009 21:40:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SymptomVisitXRef]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SymptomVisitXRef](
	[VisitID] [uniqueidentifier] NOT NULL,
	[SymptomID] [int] NOT NULL
)
END
GO
/****** Object:  Table [dbo].[Prescriptions]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Prescriptions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Prescriptions](
	[ID] [uniqueidentifier] NOT NULL,
	[VisitID] [uniqueidentifier] NOT NULL,
	[MedicineID] [int] NULL,
	[MedicineName] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CS_AS NULL,
	[Directions] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CS_AS NULL,
 CONSTRAINT [PK_Prescriptions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  StoredProcedure [dbo].[CreateNewVisit]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateNewVisit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[CreateNewVisit] 
@PatientID UNIQUEIDENTIFIER
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

declare @lastVisitID UNIQUEIDENTIFIER;
declare @newVisitID UNIQUEIDENTIFIER;
declare @visitDate datetime;

set @visitDate = getdate();
set @newVisitID = newid();

set @lastVisitID = (select top(1) ID from Visits
	where PatientID = @PatientID
	order by Date DESC);

	INSERT INTO [Visits]
			(ID
			,Date
			,LastUpdDate
			,PatientID)
		values
			(@newVisitID
			,@visitDate
			,@visitDate
			,@PatientID);

IF @lastVisitID IS NULL

	BEGIN
	INSERT INTO [OrganExamination]
				([ID]
			   ,[VisitID]
			   ,[OrganID]
			   ,[Value]
			   ,[Surgery]
			   ,[ChronicDisease]
			   ,[Removed]
			   ,[Implant])
		 select newid()
			   ,@newVisitID
			   ,[ID]
			   ,0
			   ,0
			   ,0
			   ,0
			   ,0
			from [Organs]  		
	END

ELSE

	BEGIN
	INSERT INTO [SymptomVisitXRef]
           ([VisitID]
           ,[SymptomID])
		select @newVisitID, [SymptomID] from [SymptomVisitXRef]
				where [VisitID] = @lastVisitID

	INSERT INTO [OrganExamination]
				([ID]
			   ,[VisitID]
			   ,[OrganID]
			   ,[Value]
			   ,[Surgery]
			   ,[ChronicDisease]
			   ,[Removed]
			   ,[Implant])
		 select newid()
			   ,@newVisitID
			   ,[OrganID]
			   ,0
			   ,[Surgery]
			   ,[ChronicDisease]
			   ,[Removed]
			   ,[Implant]
			from [OrganExamination]  
		where [VisitID] = @lastVisitID
	END

select @newVisitID as ID

END

/****** Object:  Table [dbo].[Chakras]    Script Date: 04/19/2009 21:33:42 ******/
SET ANSI_NULLS ON
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteSymptomVisitXRef]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteSymptomVisitXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DeleteSymptomVisitXRef] 
@VisitID uniqueidentifier, 
@SymptomID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DELETE FROM SymptomVisitXRef
      WHERE ([VisitID] = @VisitID
           AND [SymptomID] = @SymptomID)
   
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[InsertSymptomVisitXRef]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertSymptomVisitXRef]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[InsertSymptomVisitXRef] 
@VisitID uniqueidentifier, 
@SymptomID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


INSERT INTO [SymptomVisitXRef]
           ([VisitID]
           ,[SymptomID])
     VALUES
           (@VisitID
           ,@SymptomID)
END
' 
END
GO
/****** Object:  Trigger [OnPatientChange]    Script Date: 08/09/2009 21:40:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[OnPatientChange]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[OnPatientChange]
ON [dbo].[Patients]
FOR UPDATE
AS
BEGIN
IF NOT UPDATE(LastUpdDate)
	UPDATE Patients
	SET Patients.LastUpdDate = GETUTCDATE()
		WHERE ID IN(SELECT ID FROM INSERTED)
END
'
GO
/****** Object:  Trigger [OnPractitionerChange]    Script Date: 08/09/2009 21:40:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[OnPractitionerChange]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[OnPractitionerChange]
ON [dbo].[Practitioners]
FOR UPDATE
AS
BEGIN
IF NOT UPDATE(LastUpdDate)
	UPDATE Practitioners
	SET Practitioners.LastUpdDate = GETUTCDATE()
		WHERE ID IN(SELECT ID FROM INSERTED)
END
'
GO
/****** Object:  StoredProcedure [dbo].[InsertVisit]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertVisit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertVisit] 
	@ID [uniqueidentifier] ,
	@VisitDate [smalldatetime],
	@LastUpdDate [smalldatetime],
	@PatientId [uniqueidentifier],
	@MeasurementData [varbinary] (max),
	@ExamResult [varbinary] (max),
	@Notes [nvarchar] (max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [Visits]
			(ID
			,Date
			,LastUpdDate
			,PatientID
			,MeasurementData
			,ExamResult
			,Notes)
		values
			(@ID
			,@VisitDate
			,@LastUpdDate
			,@PatientId
			,@MeasurementData
			,@ExamResult
			,@Notes);
END

' 
END
GO
/****** Object:  Trigger [OnVisitChange]    Script Date: 08/09/2009 21:40:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[OnVisitChange]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[OnVisitChange]
ON [dbo].[Visits]
FOR UPDATE
AS
BEGIN
IF NOT UPDATE(LastUpdDate)
	UPDATE Visits
	SET Visits.LastUpdDate = GETUTCDATE()
		WHERE ID IN(SELECT ID FROM INSERTED)
END
'
GO
/****** Object:  StoredProcedure [dbo].[GetConditionByValue]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetConditionByValue]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetConditionByValue]
	@Value int
AS
BEGIN
	
	SET NOCOUNT ON;
SELECT top 1 [ID]
      ,[Name]
      ,[MinRange]
      ,[MaxRange]
      ,[OrderKey]
	  ,[ColorCode]
	  ,[Description]
  FROM [Conditions]
	WHERE [MinRange] <= @Value AND [MaxRange] > @Value
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetMedicineIdByOrganCondition]    Script Date: 08/09/2009 21:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMedicineIdByOrganCondition]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetMedicineIdByOrganCondition]
	@OrganID int,
	@ConditionValue int
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT [MedicineOrganConditionXRef].[MedicineID]
  FROM [Conditions] INNER JOIN [MedicineOrganConditionXRef] ON [Conditions].[ID] = [MedicineOrganConditionXRef].[ConditionID]
	WHERE [Conditions].[MinRange] <= @ConditionValue 
		AND [Conditions].[MaxRange] > @ConditionValue
		AND [MedicineOrganConditionXRef].[OrganID] = @OrganID
	
END
' 
END
GO
/****** Object:  Default [DF_Conditions_OrderKey]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Conditions_OrderKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[Conditions]'))
Begin
ALTER TABLE [dbo].[Conditions] ADD  CONSTRAINT [DF_Conditions_OrderKey]  DEFAULT ((100)) FOR [OrderKey]

End
GO
/****** Object:  Default [DF_OrganExamination_ID]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] ADD  CONSTRAINT [DF_OrganExamination_ID]  DEFAULT (newid()) FOR [ID]

End
GO
/****** Object:  Default [DF_OrganExamination_Value]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_Value]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] ADD  CONSTRAINT [DF_OrganExamination_Value]  DEFAULT ((0)) FOR [Value]

End
GO
/****** Object:  Default [DF_Table_1_Operation]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Table_1_Operation]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] ADD  CONSTRAINT [DF_Table_1_Operation]  DEFAULT ((0)) FOR [Surgery]

End
GO
/****** Object:  Default [DF_Table_1_HronicDesease]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Table_1_HronicDesease]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] ADD  CONSTRAINT [DF_Table_1_HronicDesease]  DEFAULT ((0)) FOR [ChronicDisease]

End
GO
/****** Object:  Default [DF_OrganExamination_Removed]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_Removed]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] ADD  CONSTRAINT [DF_OrganExamination_Removed]  DEFAULT ((0)) FOR [Removed]

End
GO
/****** Object:  Default [DF_OrganExamination_Implant]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OrganExamination_Implant]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
Begin
ALTER TABLE [dbo].[OrganExamination] ADD  CONSTRAINT [DF_OrganExamination_Implant]  DEFAULT ((0)) FOR [Implant]

End
GO
/****** Object:  Default [DF_Organs_OrderKey]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Organs_OrderKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[Organs]'))
Begin
ALTER TABLE [dbo].[Organs] ADD  CONSTRAINT [DF_Organs_OrderKey]  DEFAULT ((100)) FOR [OrderKey]

End
GO
/****** Object:  Default [DF_Patient_Id]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Patient_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
Begin
ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patient_Id]  DEFAULT (newid()) FOR [ID]

End
GO
/****** Object:  Default [DF_Patient_LastUpdDate]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Patient_LastUpdDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
Begin
ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patient_LastUpdDate]  DEFAULT (getutcdate()) FOR [LastUpdDate]

End
GO
/****** Object:  Default [DF_Patients_Deleted]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Patients_Deleted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
Begin
ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patients_Deleted]  DEFAULT ((0)) FOR [Deleted]

End
GO
/****** Object:  Default [DF_Practitioner_Id]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Practitioner_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Practitioners]'))
Begin
ALTER TABLE [dbo].[Practitioners] ADD  CONSTRAINT [DF_Practitioner_Id]  DEFAULT (newid()) FOR [ID]

End
GO
/****** Object:  Default [DF_Prescriptions_ID]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Prescriptions_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Prescriptions]'))
Begin
ALTER TABLE [dbo].[Prescriptions] ADD  CONSTRAINT [DF_Prescriptions_ID]  DEFAULT (newid()) FOR [ID]

End
GO
/****** Object:  Default [DF_Symptoms _OrderKey]    Script Date: 08/09/2009 21:40:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Symptoms _OrderKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[Symptoms ]'))
Begin
ALTER TABLE [dbo].[Symptoms ] ADD  CONSTRAINT [DF_Symptoms _OrderKey]  DEFAULT ((100)) FOR [OrderKey]

End
GO
/****** Object:  Default [DF_Visits_Id]    Script Date: 08/09/2009 21:40:28 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Visits_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Visits]'))
Begin
ALTER TABLE [dbo].[Visits] ADD  CONSTRAINT [DF_Visits_Id]  DEFAULT (newid()) FOR [ID]

End
GO
/****** Object:  Default [DF_Visits_Date]    Script Date: 08/09/2009 21:40:28 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Visits_Date]') AND parent_object_id = OBJECT_ID(N'[dbo].[Visits]'))
Begin
ALTER TABLE [dbo].[Visits] ADD  CONSTRAINT [DF_Visits_Date]  DEFAULT (getutcdate()) FOR [Date]

End
GO
/****** Object:  ForeignKey [FK_OrganExamination_Organs]    Script Date: 08/09/2009 21:40:27 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganExamination_Organs]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
ALTER TABLE [dbo].[OrganExamination]  WITH CHECK ADD  CONSTRAINT [FK_OrganExamination_Organs] FOREIGN KEY([OrganID])
REFERENCES [dbo].[Organs] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrganExamination] CHECK CONSTRAINT [FK_OrganExamination_Organs]
GO
/****** Object:  ForeignKey [FK_OrganExamination_Visits1]    Script Date: 08/09/2009 21:40:27 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganExamination_Visits1]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganExamination]'))
ALTER TABLE [dbo].[OrganExamination]  WITH CHECK ADD  CONSTRAINT [FK_OrganExamination_Visits1] FOREIGN KEY([VisitID])
REFERENCES [dbo].[Visits] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrganExamination] CHECK CONSTRAINT [FK_OrganExamination_Visits1]
GO
/****** Object:  ForeignKey [FK_Patient_Practitioner]    Script Date: 08/09/2009 21:40:27 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Patient_Practitioner]') AND parent_object_id = OBJECT_ID(N'[dbo].[Patients]'))
ALTER TABLE [dbo].[Patients]  WITH CHECK ADD  CONSTRAINT [FK_Patient_Practitioner] FOREIGN KEY([PractitionerId])
REFERENCES [dbo].[Practitioners] ([ID])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Patients] CHECK CONSTRAINT [FK_Patient_Practitioner]
GO
/****** Object:  ForeignKey [FK_Prescriptions_Visit]    Script Date: 08/09/2009 21:40:27 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Prescriptions_Visit]') AND parent_object_id = OBJECT_ID(N'[dbo].[Prescriptions]'))
ALTER TABLE [dbo].[Prescriptions]  WITH CHECK ADD  CONSTRAINT [FK_Prescriptions_Visit] FOREIGN KEY([VisitID])
REFERENCES [dbo].[Visits] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Prescriptions] CHECK CONSTRAINT [FK_Prescriptions_Visit]
GO
/****** Object:  ForeignKey [FK_SymptomVisitXRef_Symptoms ]    Script Date: 08/09/2009 21:40:28 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SymptomVisitXRef_Symptoms ]') AND parent_object_id = OBJECT_ID(N'[dbo].[SymptomVisitXRef]'))
ALTER TABLE [dbo].[SymptomVisitXRef]  WITH CHECK ADD  CONSTRAINT [FK_SymptomVisitXRef_Symptoms ] FOREIGN KEY([SymptomID])
REFERENCES [dbo].[Symptoms ] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SymptomVisitXRef] CHECK CONSTRAINT [FK_SymptomVisitXRef_Symptoms ]
GO
/****** Object:  ForeignKey [FK_SymptomVisitXRef_Visit]    Script Date: 08/09/2009 21:40:28 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SymptomVisitXRef_Visit]') AND parent_object_id = OBJECT_ID(N'[dbo].[SymptomVisitXRef]'))
ALTER TABLE [dbo].[SymptomVisitXRef]  WITH CHECK ADD  CONSTRAINT [FK_SymptomVisitXRef_Visit] FOREIGN KEY([VisitID])
REFERENCES [dbo].[Visits] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SymptomVisitXRef] CHECK CONSTRAINT [FK_SymptomVisitXRef_Visit]
GO
/****** Object:  ForeignKey [FK_Visit_Patient]    Script Date: 08/09/2009 21:40:28 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Visit_Patient]') AND parent_object_id = OBJECT_ID(N'[dbo].[Visits]'))
ALTER TABLE [dbo].[Visits]  WITH CHECK ADD  CONSTRAINT [FK_Visit_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patients] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Visits] CHECK CONSTRAINT [FK_Visit_Patient]
GO
