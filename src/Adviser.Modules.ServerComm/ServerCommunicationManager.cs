﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;
using System.Data.Entity;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;

using Adviser.Client.Core.Interfaces;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Services;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Presentation;
using Adviser.Common.Utils;
using Adviser.Common.Logging;

namespace Adviser.Modules.ServerComm
{
    public interface IServerCommunicationManager
    {
    }


    public class ServerCommunicationManager : IServerCommunicationManager
    {
        private IEventAggregator _eventAggregator;
        private IUnityContainer _container;
        private AppointmentSession _session;
        private IDBService _dbService;

        public ServerCommunicationManager(IUnityContainer container, IEventAggregator eventAggregator, IDBService dbService, AppointmentSession session)
        {
            _container = container;
            _eventAggregator = eventAggregator;
            _dbService = dbService;
            _session = session;
            _eventAggregator.GetEvent<ResultsRequestEvent>().Subscribe(OnResultsRequest);
            _eventAggregator.GetEvent<SendErrorReportEvent>().Subscribe(OnAdviserErrorEvent);
        }

        private void OnResultsRequest(Visit visit)
        {
            IAdviserServiceAgent agent = _container.Resolve<IAdviserServiceAgent>();
            ResultsReceivedEventArgs e = agent.GetResults(visit);
            if (e.IsSuccess)
                _dbService.SaveServerChanges();

            SendChangesIfAny();
            SendAllErrors();
            _eventAggregator.GetEvent<ResultsReceivedEvent>().Publish(e);

        }

        private void OnAdviserErrorEvent(AdviserLogEntry logEntry)
        {
            if (SendErrorReport(logEntry))
                SendAllErrors();
        }

        private bool SendErrorReport(AdviserLogEntry entry)
        {
            bool isSuccess = false;
            IAdviserServiceAgent agent = _container.Resolve<IAdviserServiceAgent>();
            isSuccess = agent.ReportError(entry);

            if (isSuccess)
            {
                using (AdviserDBEntities dbContext = new AdviserDBEntities())
                {
                    dbContext.DeleteByKey<Error>("ID", entry.Id);
                    dbContext.SaveChanges();
                }
            }

            return isSuccess;
        }

        private void SendAllErrors()
        {
            AdviserDBEntities dbContext = _dbService.DataContext;

            IAdviserServiceAgent agent = _container.Resolve<IAdviserServiceAgent>();
            BinaryFormatter fmt = new BinaryFormatter();
            int failedAttemptCount = 0;
            var errorQuery = from error in dbContext.ErrorSet
                             where error.Lock == false
                             select error;

            foreach (Error error in errorQuery)
            {
                error.Lock = true;

                //- send to server
                bool isSuccess = false;
                try
                {
                    AdviserLogEntry entry = null;
                    using (MemoryStream stream = new MemoryStream(error.Data))
                    {
                        stream.Seek(0, SeekOrigin.Begin);
                        entry = (AdviserLogEntry)fmt.Deserialize(stream);
                    }

                    isSuccess = agent.ReportError(entry);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                    isSuccess = false;
                }

                if (isSuccess)
                {
                    dbContext.DeleteObject(error);
                }
                else
                {
                    failedAttemptCount++;
                    error.Lock = false;

                    if (failedAttemptCount > 3)
                        return;
                }
            }
            _dbService.SaveServerChanges();
        }

        private void SendChangesIfAny()
        {
            AdviserDBEntities dbContext = _dbService.DataContext;

            int count = dbContext.ChangeSet.Count();
            if (count == 0)
                return;

            IAdviserServiceAgent agent = _container.Resolve<IAdviserServiceAgent>();
            _session = _container.Resolve<AppointmentSession>();
            
            Change practitionerChange = dbContext.ChangeSet.FirstOrDefault(c => c.ID == _session.Practitioner.ID);
            if (practitionerChange != null)
            {
                string message;
                bool success = agent.UpdatePractitioner(_session.Practitioner, out message);
                if (success)
                    dbContext.DeleteObject(practitionerChange);
            }

            List<Change> patientChanges = dbContext.ChangeSet.Where(c => c.EntitySet == "PatientSet").ToList();
            if (patientChanges.Count > 0)
            {
                List<Patient> patientUpdateList = new List<Patient>();
                foreach (Change change in patientChanges)
                {
                    Patient patient = null;
                    if (change.State == System.Data.EntityState.Deleted.ToString())
                    {
                        patient = new Patient();
                        patient.ID = change.ID;
                        patient.Deleted = true;
                        patient.LastUpdDate = DateTime.Now.ToUniversalTime();
                    }
                    else
                    {
                        patient = dbContext.PatientSet.FirstOrDefault(p => p.ID == change.ID);
                    }
                    patientUpdateList.Add(patient);
                }

                // send patients updates (including deletes)
                if (agent.UpdatePatients(_session.Practitioner, patientUpdateList.ToArray()))
                {
                    foreach (Change change in patientChanges)
                        dbContext.DeleteObject(change);

                    _dbService.SaveServerChanges();
                }
                
            }

            //- send visit updates (including deletes)
            List<Change> visitChanges = dbContext.ChangeSet.Where(c => c.EntitySet == "VisitSet").ToList();
            if (visitChanges.Count > 0)
            {
                //Dictionary<Patient, List<KeyValuePair<Change, Visit>>> visitsUpdatesByPatient = new Dictionary<Patient, List<KeyValuePair<Change, Visit>>>();
                List<Visit> visitUpdates = new List<Visit>();
                foreach (Change change in visitChanges)
                {
                    Visit visit = null;
                    if (change.State == System.Data.EntityState.Deleted.ToString())
                    {
                        visit = new Visit();
                        visit.ID = change.ID;
                        visit.Deleted = true;
                        visit.Date = DateTime.MinValue;
                        visit.LastUpdDate = DateTime.Now.ToUniversalTime();
                    }
                    else
                    {
                        visit = dbContext.VisitSet.FirstOrDefault(v => v.ID == change.ID);
                        visit.Prescriptions.Load();
                        visit.OrganExaminations.Load();
                        visit.PatientReference.Load();

                        //visit = dbContext.VisitSet.FirstOrDefault(v => v.ID == change.ID);
                        //visit.Prescriptions.Load();
                        //visit.OrganExaminations.Load();
                        //visit.PatientReference.Load();
                        //Patient patient = visit.Patient;
                        //if (visitsUpdatesByPatient.ContainsKey(patient))
                        //{
                        //    visitsUpdatesByPatient[patient].Add(new KeyValuePair<Change, Visit>(change, visit));
                        //}
                        //else
                        //{
                        //    List<KeyValuePair<Change, Visit>> list = new List<KeyValuePair<Change, Visit>>();
                        //    list.Add(new KeyValuePair<Change, Visit>(change, visit));
                        //    visitsUpdatesByPatient.Add(patient, list);
                        //}
                    }
                    visitUpdates.Add(visit);
                }

                if (agent.UpdateVisits(_session.Practitioner, visitUpdates.ToArray()))
                {
                    foreach (Change change in visitChanges)
                        dbContext.DeleteObject(change);
                }
                _dbService.SaveServerChanges();
            }

            List<Change> rxChanges = dbContext.ChangeSet.Where(c => c.EntitySet == "PrescriptionSet").ToList();
            if (rxChanges.Count > 0)
            {
                List<Prescription> rxList = new List<Prescription>();
                foreach (Change change in rxChanges)
                {
                    Prescription rx = null;
                    if (change.State == System.Data.EntityState.Deleted.ToString())
                    {
                        rx = new Prescription();
                        rx.ID = change.ID;
                        rx.Deleted = true;
                    }
                    else
                    {
                        rx = dbContext.LoadByKey<Prescription>("ID", change.ID);//dbContext.PrescriptionSet.FirstOrDefault(p => p.ID == change.ID);
                    }
                    rxList.Add(rx);
                }

                if (agent.UpdatePrescriptions(_session.Practitioner, rxList.ToArray()))
                {
                    foreach (Change change in rxChanges)
                        dbContext.DeleteObject(change);
                }
                _dbService.SaveServerChanges();
            }
 
            ////- send changes to the server and remove change log
            //foreach (Patient patient in visitsUpdatesByPatient.Keys)
            //{
            //    Visit[] visits = new Visit[visitsUpdatesByPatient[patient].Count];
            //    Change[] changes = new Change[visitsUpdatesByPatient[patient].Count];
            //    for (int i = 0; i < visits.Length; ++i)
            //    {
            //        visits[i] = visitsUpdatesByPatient[patient][i].Value;
            //        changes[i] = visitsUpdatesByPatient[patient][i].Key;
            //    }

            //    agent.UpdateVisits(_session.Practitioner, patient, visits);
            //    foreach (Change chg in changes)
            //        dbContext.DeleteObject(chg);
            //}
            
        }

        //private void OnAdviserErrorEventOld(int id)
        //{
        //    using (AdviserDBEntities dbContext = new AdviserDBEntities())
        //    {
        //        BinaryFormatter fmt = new BinaryFormatter();
        //        int failedAttemptCount = 0;
        //        var errorQuery = from error in dbContext.ErrorSet
        //                         where error.Lock == false
        //                         select error;

        //        foreach (Error error in errorQuery)
        //        {
        //            error.Lock = true;

        //            //- send to server
        //            bool isSuccess = false;
        //            try
        //            {
        //                AdviserClientLogEntry entry = null;
        //                using (MemoryStream stream = new MemoryStream(error.Data))
        //                {
        //                    stream.Seek(0, SeekOrigin.Begin);
        //                    entry = (AdviserClientLogEntry)fmt.Deserialize(stream);
        //                }
        //                IAdviserServiceAgent agent = _container.Resolve<IAdviserServiceAgent>();
        //                isSuccess = agent.ReportError(entry);    
        //            }
        //            catch (Exception ex)
        //            {
        //                System.Diagnostics.Debug.WriteLine(ex.ToString());
        //                isSuccess = false;
        //            }

        //            if (isSuccess)
        //            {
        //                dbContext.DeleteObject(error);
        //            }
        //            else
        //            {
        //                failedAttemptCount++;
        //                error.Time = DateTime.Now.ToUniversalTime();
        //                //dbContext.SaveChanges();

        //                if (failedAttemptCount > 3)
        //                {
        //                    return;
        //                }
        //            }   
        //        }
        //        dbContext.SaveChanges();
        //    }
        //}
    }
}
