﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;

using Adviser.Client.Core.Presentation;
using Adviser.Client.Core.Interfaces;
using Adviser.Client.Core.Events;

namespace Advisor.Modules.Interpretation
{
    public interface IRequestServerResultsPresenter
    {
        IRequestServerResultsView View { get; set; }
        void Show();
        void Close();
    }

    public class RequestServerResultsPresenter : IRequestServerResultsPresenter
    {
        private IUnityContainer _container;
        private IEventAggregator _eventAggregator;
        private AppointmentSession _session;
        private IChildWindow _window;

        public RequestServerResultsPresenter(IUnityContainer container, 
            IEventAggregator eventAggregator, 
            IChildWindow window, 
            IRequestServerResultsView view,
            AppointmentSession session)
        {
            _container = container;
            _window = window;
            _window.WindowTitle = "Request Results";
            _eventAggregator = eventAggregator;
            View = view;
            _session = session;

            view.ResutsRequested += new EventHandler(view_ResutsRequested);
            view.CancelClick += new EventHandler(view_CancelClick);
            _eventAggregator.GetEvent<ResultsReceivedEvent>().Subscribe(OnResultsReceived, ThreadOption.UIThread);
        }

        public IRequestServerResultsView View
        {
            get;
            set;
        }

        public void Show()
        {
            _window.SetContent(View);
            _window.ChildWindowSizeToContent = System.Windows.SizeToContent.WidthAndHeight;
            _window.ShowAsDialog();
        }

        public void Close()
        {
            _eventAggregator.GetEvent<ResultsReceivedEvent>().Unsubscribe(OnResultsReceived);
            _window.CloseWindow();
        }

        private void view_CancelClick(object sender, EventArgs e)
        {
            NavigationEventArgs navEvent = new NavigationEventArgs();
            navEvent.EventName = "VisitsClick";
            _eventAggregator.GetEvent<NavigationEvent>().Publish(navEvent);

            Close();
        }

        private void view_ResutsRequested(object sender, EventArgs e)
        {
            View.UserMessage = "Please wait while results are being received from the server...";
            
            View.SubmitEnabled = false;
            View.CancelEnabled = false;

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(RequestAdviserResultsAsynch);
            worker.RunWorkerAsync();
        }

        /// <summary>
        /// This method will publish event on background thread from the thread pool.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestAdviserResultsAsynch(object sender, DoWorkEventArgs e)
        {
            _eventAggregator.GetEvent<ResultsRequestEvent>().Publish(_session.Visit);
        }

        private void OnResultsReceived(ResultsReceivedEventArgs e)
        {
            if (e.IsSuccess)
            {
                NavigationEventArgs navEvent = new NavigationEventArgs();
                navEvent.EventName = NavigationEventNames.Results;
                _eventAggregator.GetEvent<NavigationRequestEvent>().Publish(navEvent);

                Close();
            }
            else
            {
                View.UserMessage = "Unable to receive results at this time. Reason:\n\n"
                    + e.Message
                    + "\n\n You can try again using Submit data or click Cancel to return to program.\n"
                    + "If this error persists, please contact system administrator.";

                View.SubmitEnabled = true;
                View.CancelEnabled = true;
            }
        }
    }
}
