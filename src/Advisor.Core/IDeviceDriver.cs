﻿using System;
using Advisor.Common.DataModel;

namespace Advisor.Core
{
    public interface IDeviceDriver
    {
        bool IsConnected{ get; }
        bool SetupDevice(DeviceSettings settings);
        bool ConnectElectrodes(byte front, byte back);
        bool SetPolarity(bool frontPositive);
        bool GetDeviceGUID(ref Guid DeviceGUID);
        bool GetBridgeVersion(ref string BridgeVersion);
        ushort ReadData();
        ushort ReadBusVoltage();
        void Release();
    }
}
