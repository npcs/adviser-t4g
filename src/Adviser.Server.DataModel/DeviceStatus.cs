﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Server.DataModel
{
    public enum DeviceStatus
    {
        Available = 1,
        NotAvailable = 2,
        Online = 3,
        Locked = 4
    }
}
