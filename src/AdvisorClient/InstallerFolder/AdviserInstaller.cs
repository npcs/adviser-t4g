﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Text.RegularExpressions;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace AdvisorClient
{
	[RunInstaller(true)]

    public partial class AdviserInstaller : Installer
    {
        //const string SQLServerDBName = "AdviserDB";
        //const string SQLDatabaseServer = @".\SQLEXPRESS";
        //const string SQLConnectionString = @"data source=" + SQLDatabaseServer + ";Initial Catalog=" + SQLServerDBName + ";Integrated Security=SSPI;Trusted_Connection=True";
        //const string SQLSetupConnectionString = @"data source=" + SQLDatabaseServer + ";Initial Catalog=master;Integrated Security=SSPI;Trusted_Connection=True";

        const string SQLServerDBName = "AdviserDB";
		const string SQLDatabaseServer = @".\SQLEXPRESS";
        const string SQLSetupConnectionString = @"data source=" + SQLDatabaseServer + ";Initial Catalog=master;Integrated Security=True;MultipleActiveResultSets=True";

		private static AdvisorClient.ScriptProgressBar _ScriptProgressBar = new AdvisorClient.ScriptProgressBar();

        public AdviserInstaller()
        {
            InitializeComponent();
        }

        public override void Install(IDictionary savedState)
        {
            base.Install(savedState);

			if (!SQLDBExists())
            {
                EventLogWriter.WriteToELog("DesignMigrator: !SQLDBExists", "");
                UpdateDB();
            }
        }

        #region Private methods

        //private static void WriteToELog(string title, string body)
        //{
        //    if (!EventLog.SourceExists(sSource))
        //        EventLog.CreateEventSource(sSource,sLog);

        //    EventLog.WriteEntry(sSource,sEvent);
        //    EventLog.WriteEntry(sSource, sEvent,
        //        EventLogEntryType.Warning, 234);

        //    EventLog.WriteEntry(
        //}

		private static void UpdateDB()
        {

            EventLogWriter.WriteToELog("DesignMigrator: Begin UpdateDB", SQLSetupConnectionString);

            int ScriptVersion = GetSQLDBVersion();

            SqlConnection _SQLServerConnnection = new SqlConnection(SQLSetupConnectionString);
            _SQLServerConnnection.Open();

            EventLogWriter.WriteToELog("DesignMigrator: UpdateDB _SQLServerConnnection.Open", SQLSetupConnectionString);

            _ScriptProgressBar.totalProgressBar.Maximum = TotalFilesNeedToRun(ScriptVersion);
            _ScriptProgressBar.totalProgressBar.Minimum = 0;
            _ScriptProgressBar.totalProgressBar.Value = 0;
            _ScriptProgressBar.Show();
            _ScriptProgressBar.Refresh();
            _ScriptProgressBar.BringToFront();

            System.Windows.Forms.Application.DoEvents();

            EventLogWriter.WriteToELog("DesignMigrator: UpdateDB ScriptVersion=" + ScriptVersion.ToString(), "");

            int sqlScriptsRan = 0;
            while (GetFileInfo(GetDBScriptFileName(ScriptVersion)).Exists)
            {
                //_SQLServerConnnection.Open();
                //Run the update script on VistaDB                
                EventLogWriter.WriteToELog("DesignMigrator: UpdateDB ScriptVersion=" + ScriptVersion.ToString() + " Exists", "");
                ExecuteSql(_SQLServerConnnection, GetDBScriptFileName(ScriptVersion));
                ScriptVersion++;


                EventLogWriter.WriteToELog("DesignMigrator: UpdateDB ScriptVersion=" + ScriptVersion.ToString() + " Exists", "");
                sqlScriptsRan++;
                _ScriptProgressBar.totalProgressBar.Value = sqlScriptsRan;
                _ScriptProgressBar.Invalidate();
                _ScriptProgressBar.BringToFront();
                System.Windows.Forms.Application.DoEvents();

            }

            if (_SQLServerConnnection.State != ConnectionState.Closed) _SQLServerConnnection.Close();

            _ScriptProgressBar.Close();
            EventLogWriter.WriteToELog("DesignMigrator: End UpdateDB ScriptVersion=" + ScriptVersion.ToString(), "");
            
        }


        private static int TotalFilesNeedToRun(int currentVersion)
        {
            EventLogWriter.WriteToELog("DesignMigrator: Begin Counting files need to run", "");

            int _currentVersion = currentVersion;
            int i = 1;

            while (GetFileInfo(GetDBScriptFileName(_currentVersion)).Exists)
            {
                _currentVersion++;
                i++;
            }

            EventLogWriter.WriteToELog("DesignMigrator: End Counting files : Need to run " + i.ToString() + " files", "");
            return i;
        }

        private static string GetDBScriptFileName(int ScriptVersion)
        {
            return String.Concat(@"DatabaseMigrationScripts\Version", ScriptVersion.ToString(), "to", (ScriptVersion + 1).ToString(), ".sql");
        }

        private static FileInfo GetFileInfo(String FileName)
        {
            System.Reflection.Assembly Asm = System.Reflection.Assembly.GetExecutingAssembly();
            String strConfigLoc;
            strConfigLoc = Asm.Location;

            string strTemp;
            strTemp = strConfigLoc;
            strTemp = strTemp.Remove(strTemp.LastIndexOf("\\"), strTemp.Length - strTemp.LastIndexOf("\\"));
            //strTemp = strTemp.Remove(strTemp.LastIndexOf("\\"), strTemp.Length - strTemp.LastIndexOf("\\"));         

            System.IO.FileInfo FileInfo = new System.IO.FileInfo(String.Concat(strTemp, "\\", FileName));
            return FileInfo;
        }

        private string GetParameter(string parameterKey)
        {
            if (Context.Parameters[parameterKey] == null)
            { return String.Empty; }
            return Context.Parameters[parameterKey].Trim();
        }

        private static string GetScript(string ScriptFileName)
        {

            FileInfo fi = GetFileInfo(ScriptFileName);

			EventLogWriter.WriteToELog("DesignMigrator: Begin GetScript fi.Exists=" + fi.Exists.ToString(), ScriptFileName);

            if (!fi.Exists)
            {
                throw new InstallException(String.Concat("GetScript: Missing file: ", ScriptFileName));
            }

            string SQLString;

            using (StreamReader reader = fi.OpenText())
            {
                SQLString = reader.ReadToEnd();
            }

            EventLogWriter.WriteToELog("DesignMigrator: End GetScript fi.Exists=" + fi.Exists.ToString(), "");
            return SQLString;
        }

        private static void ExecuteSql(SqlConnection sqlCon, string SQLScriptFileName)
        {

            EventLogWriter.WriteToELog("Begin ExecuteSql ", "");
            string[] SqlLine;
            Regex regex = new Regex("^GO", RegexOptions.IgnoreCase | RegexOptions.Multiline);

            string txtSQL = GetScript(SQLScriptFileName);

            //if (SQLScriptFileName.Contains("Version"))
            //{
            //    txtSQL = String.Concat("use [", SQLServerDBName, "] ", txtSQL);
            //}

            SqlLine = regex.Split(txtSQL);

            SqlCommand cmd = sqlCon.CreateCommand();
            cmd.Connection = sqlCon;

            EventLogWriter.WriteToELog("ExecuteSql SqlLine", SqlLine.Length.ToString());
            foreach (string line in SqlLine)
            {
                if (line.Length > 0)
                {
                    cmd.CommandText = line;
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        EventLogWriter.WriteToELog("ExecuteSql SqlLine exception", "SQL: " + line + "\n " + e.ToString());

                        //EventLog elog = new EventLog();
                        //elog.Log = "Application";
                        //elog.Source = "SwitcherPro DT ExecuteSql";
                        //elog.WriteEntry(String.Concat(SQLScriptFileName, " ", e.Message, "DBName: ", SQLServerDBName, " SQLScriptFileName: ", SQLScriptFileName, " Conn str: ", sqlCon.ConnectionString, " SQL: ", line));
                        //elog.Close();

                        if (sqlCon.State != ConnectionState.Closed) sqlCon.Close();

                        throw new InstallException(String.Concat(SQLScriptFileName, e.Message, " SQL: ", line.Remove(80)));
                        //rollback
                        //ExecuteDrop(sqlCon); 
                        //ExecuteDrop(sqlCon, DBName);
                    }
                }
            }

            EventLogWriter.WriteToELog("End ExecuteSql ", "");
        }

        //Returns true if DB exists, false if not
        private static bool SQLDBExists()
        {
            EventLogWriter.WriteToELog("DesignMigrator: Begin SQLDBExists", SQLSetupConnectionString);

            SqlConnection sqlCon = new SqlConnection(SQLSetupConnectionString);
            try
            {
                sqlCon.Open();
            }
            catch (SqlException e)
            {
                EventLogWriter.WriteToELog("Exception!!!: SQLDBExists Message=" + e.Message, e.Message);
                return false;
            }
            int count = 0;
            SqlCommand cmd = sqlCon.CreateCommand();
            cmd.Connection = sqlCon;
            cmd.CommandText = String.Concat("SELECT Count(name) FROM master.dbo.sysdatabases WHERE name = N'", SQLServerDBName, "'");
            cmd.CommandType = CommandType.Text;
            try
            {
                count = (int)cmd.ExecuteScalar();
            }
            catch (SqlException e)
            {

                if (sqlCon.State != ConnectionState.Closed) sqlCon.Close();
                EventLogWriter.WriteToELog("Exception!!!: SQLDBExists ExecuteScalar Message=" + e.Message, e.Message);
                throw new InstallException(String.Concat("DBExists: Sql Exception: ", e.Message));

                //rollback
                //ExecuteDrop(sqlCon); 
                //ExecuteDrop(sqlCon, DBName);
            }
            sqlCon.Close();

            EventLogWriter.WriteToELog("DesignMigrator: End SQLDBExists", "count=" + count.ToString());
            if (count == 1)
            {
                return true;
            }

            return false;

        }

        //Returns DB version
        private static int GetSQLDBVersion()
        {
            return 0;

            //SqlConnection sqlCon = new SqlConnection(SQLConnectionString);
            //sqlCon.Open();
            //int DBVersion = 999999;
            //SqlCommand cmd = sqlCon.CreateCommand();
            //cmd.Connection = sqlCon;
            //cmd.CommandText = String.Concat("SELECT max (ID) FROM [", SQLServerDBName, "].[dbo].[DatabaseVersion]");
            //cmd.CommandType = CommandType.Text;
            //try
            //{
            //    DBVersion = (int)cmd.ExecuteScalar();
            //}
            //catch (SqlException e)
            //{

            //    if (sqlCon.State != ConnectionState.Closed) sqlCon.Close();
            //    throw new InstallException(String.Concat("GetDBVersion: Sql Exception: ", e.Message));
            //}
            //sqlCon.Close();
            //return DBVersion;
        }

        #endregion

        public override void Uninstall(IDictionary savedState)
        {
            base.Uninstall(savedState);
        }

		}

	public static class EventLogWriter
    {
        private const string AppName = "Adviser. ";

        public static void WriteToELog(string header, string body)
        {
            EventLog elog = new EventLog();
            elog.Log = "Application";
            if (header.Length > 210)
            {
                header = header.Remove(160);
            }
            elog.Source = String.Concat(AppName, header);
            elog.WriteEntry(body);
            elog.Close();
        }
        public static void WriteToELog(string header)
        {

            EventLog elog = new EventLog();
            elog.Log = "Application";
            if (header.Length > 210)
            {
                header = header.Remove(160);
            }
            elog.Source = String.Concat(AppName, header);
            elog.WriteEntry(header);
            elog.Close();
        }
    }
}
