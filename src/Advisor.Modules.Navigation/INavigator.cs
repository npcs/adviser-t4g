﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Composite.Wpf.Commands;

namespace Advisor.Modules.Navigation
{
    public interface INavigator
    {
        INavigationView View { get; }
        void OnNavigationCommand(object source, System.Windows.RoutedEventArgs e);
        void Navigate(NavigationState toState);
        DelegateCommand<System.Windows.Input.Key> KeyboardCommand { get; }
    }
}
