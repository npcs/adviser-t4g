﻿using System;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using Adviser.Client.DataModel;

namespace Adviser.Client.Core.Services
{
    public interface IDBService
    {
        AdviserDBEntities DataContext { get; }
        void CreateNewPractitioner(ref Practitioner practitioner);
        void AddPatient(Practitioner CurrentPractitioner);
        Patient CreateNewPatient(Practitioner CurrentPractitioner);
        Visit CreateNewVisit(Patient CurrentPatient);
        IQueryable<Organ> GetAllOrgans();
        IQueryable<Condition> GetAllConditions();
        IQueryable<Medicine> GetAllMedicine();
        IQueryable<Chakra> GetAllChakras();
        IQueryable<Electrode> GetElectrodes(bool isFront);
        IQueryable<Organ> GetOrgansBySex(bool isMale);
        IQueryable<Symptom> GetAllSymptoms();
        Medicine GetMedicineByID(int id);
        Condition GetConditionByValue(int examValue);

        EntityCollection<OrganExamination> LoadOrganExaminationEntityCollection(Visit CurrentVisit);
        EntityCollection<Prescription> LoadPrescriptionEntityCollection(Visit CurrentVisit);
        EntityCollection<Symptom> LoadSymptomEntityCollection(Visit CurrentVisit);
        EntityCollection<Visit> LoadVisitEntityCollection(Patient CurrentPatient);

        Practitioner GetPractitionerByEmail(string email, string password);
        Practitioner GetPractitionerByEmailOnly(string email);
        Practitioner GetPractitionerByID(Guid id);

        bool IsPractitionerEmailUnique(string email);
        bool DoesAnyPractitionerExist();

        void DeleteObject(object EntityObject);
        object GetObjectByKey(System.Data.EntityKey key);
        void SaveChanges();
        void SaveServerChanges();
    }
}
