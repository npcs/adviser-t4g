﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Objects.DataClasses;
using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Events;
using System.ComponentModel;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Client.DataModel;
    using Adviser.Client.Core.Services;

    /// <summary>
    /// Interaction logic for PatientListView.xaml
    /// </summary>
    public partial class PatientListView : UserControl, IPatientListView
    {
        public PatientListView()
        {
            InitializeComponent();
            this.PatientList.AddHandler(Control.MouseDoubleClickEvent, new RoutedEventHandler(HandleDoubleClick));
        }

        public event EventHandler<DataEventArgs<Patient>> PatientSelected = delegate { };
        public event EventHandler PatientDoubleClicked = delegate { };


        public IPatientsPresentationModel Model 
        {
            get
            {
                return this.DataContext as IPatientsPresentationModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public void SetSelectedPatient(Patient patient)
        {
            PatientList.SelectedItem = patient;
        }

        private void HandleDoubleClick(object sender, RoutedEventArgs e)
        {
            PatientDoubleClicked(this, null);
        }

        private void PatientList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Patient patient = e.AddedItems[0] as Patient;
                if (patient != null)
                {
                    PatientSelected(this, new DataEventArgs<Patient>(patient));
                }
            }
        }


        #region Sorting

        GridViewColumnHeader _lastHeaderClicked = null;
        ListSortDirection _lastDirection = ListSortDirection.Ascending;

        void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;

            if (headerClicked != null && headerClicked.Column != null)
            {
                if (headerClicked != _lastHeaderClicked)
                {
                    direction = ListSortDirection.Ascending;
                }
                else
                {
                    if (_lastDirection == ListSortDirection.Ascending)
                    {
                        direction = ListSortDirection.Descending;
                    }
                    else
                    {
                        direction = ListSortDirection.Ascending;
                    }
                }
                string header = headerClicked.Column.Header as string;
                Sort(header, direction);
                _lastHeaderClicked = headerClicked;
                _lastDirection = direction;
            }
        }

        private void Sort(string sortBy, ListSortDirection direction)
        {
            ICollectionView dataView = CollectionViewSource.GetDefaultView(PatientList.ItemsSource);            
            dataView.SortDescriptions.Clear();
            SortDescription sd = new SortDescription(sortBy.Replace(" ",""), direction);
            dataView.SortDescriptions.Add(sd);
            dataView.Refresh();
        }

        #endregion


        private void btnDeletePatient_Click(object sender, RoutedEventArgs e)
        {
            this.DeleteConfirmationPopup.IsOpen = true;
            this.DeleteConfirmationPopup.StaysOpen = false;
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            this.DeleteConfirmationPopup.IsOpen = false;
        } 
    }
}
