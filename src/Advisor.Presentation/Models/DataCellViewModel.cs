﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;

using Advisor.Core;
using Advisor.Common.DataModel;

namespace Advisor.Presentation.Models
{
    public enum ValuePresentationType
    {
        Raw,
        Scale,
        Relative
    }

    public enum SourceValueType
    {
        Maximum,
        Minimum,
        Mean
    }

    /// <summary>
    /// Implements presentation model for PairMeasurement
    /// </summary>
    public class DataCellViewModel : ViewModelBase
    {
        private PairMeasurement _data;
        private SourceValueType _sourceValue;
        private IValueTransform _valueTransform;
        private int x;
        private int y;

        public DataCellViewModel(int xpos, int ypos, PairMeasurement data, SourceValueType sourceValue, IValueTransform valueTransform)
        {
            x = xpos + 1;
            y = ypos + 1;
            _data = data;
            _sourceValue = sourceValue;
            _valueTransform = valueTransform;

        }

        public string Value
        {
            get
            {
                return GetValue().ToString();
            }
        }

        public int X
        {
            get{ return x; }
        }

        public int Y
        {
            get { return y; }
        }

        public void UpdateData(PairMeasurement data)
        {
            _data = data;
            RaisePropertyChangedEvent("Value");
        }

        private double GetValue()
        {
            double result;
            switch (_sourceValue)
            {
                case SourceValueType.Minimum:
                    result = ApplyTransform(_data.Minimum);
                    break;
                case SourceValueType.Mean:
                    result = ApplyTransform(_data.Mean);
                    break;
                default:
                    result = ApplyTransform(_data.Maximum);
                    break;
            }
            return result;
        }

        private double ApplyTransform(double source)
        {
            if (_valueTransform == null)
                return source;
            else
                return _valueTransform.Transform(source);
        }
    }
}
