﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

using Adviser.Client.DataModel;
using Adviser.Client.Core.Presentation;

namespace Advisor.Modules.Navigation
{
    public class NavigationModule : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public NavigationModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            RegisterViewsAndServices();

            IRegion navRegion = _regionManager.Regions[RegionNames.NavigationRegion];
            INavigator viewModel = _container.Resolve<INavigator>();
            navRegion.Add(viewModel.View);

        }

        protected void RegisterViewsAndServices()
        {
            _container.RegisterType<INavigator, Navigator>();
            _container.RegisterType<INavigationView, NavigationView>();

            //- register navigation states
            _container.RegisterType<NavigationState, PatientsNavigationState>("PatientsNavigationState");
            _container.RegisterType<NavigationState, VisitsNavigationState>("VisitsNavigationState");
            _container.RegisterType<NavigationState, MeasurementNavigationState>("MeasurementNavigationState");
            _container.RegisterType<NavigationState, ResultsNavigationState>("ResultsNavigationState");
            _container.RegisterType<NavigationState, PrintNavigationState>("PrintNavigationState");
        }
    }
}
