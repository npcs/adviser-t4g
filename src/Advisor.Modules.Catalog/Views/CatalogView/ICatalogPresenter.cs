﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Modules.Catalog.Views
{
    public interface ICatalogPresenter
    {
        ICatalogView View { get; }
    }
}
