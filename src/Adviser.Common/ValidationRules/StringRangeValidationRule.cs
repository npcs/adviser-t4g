﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Globalization;

namespace Adviser.Common.ValidationRules
{
    public class StringRangeValidationRule : ValidationRule
    {
        private int _minimumLength = -1;
        private int _maximumLength = -1;
        private string _errorMessage;

        public int MinimumLength
        {
            get { return _minimumLength; }
            set { _minimumLength = value; }
        }

        public int MaximumLength
        {
            get { return _maximumLength; }
            set { _maximumLength = value; }
        }

        public string LessThenMinErrorMessage { get; set; }
        public string MoreThenMaxErrorMessage { get; set; }

        public override ValidationResult Validate(object value,
            CultureInfo cultureInfo)
        {
            string inputString = (value ?? string.Empty).ToString();
            if (inputString.Length < this.MinimumLength)
            {
                return new ValidationResult(false, this.LessThenMinErrorMessage);
            }
            if (this.MaximumLength > 0 &&
                inputString.Length > this.MaximumLength)
            {
                return new ValidationResult(false, this.MoreThenMaxErrorMessage);
            }
            return ValidationResult.ValidResult;
        }
    }

}
