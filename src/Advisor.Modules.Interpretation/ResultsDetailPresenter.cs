﻿using System;
using System.Collections.Generic;

namespace Advisor.Modules.Interpretation
{
    using Microsoft.Practices.Composite.Events;
    using Microsoft.Practices.Composite.Wpf.Events;
    using Adviser.Client.Core.Events;
    using Adviser.Client.DataModel;
    using Advisor.Modules.Interpretation.PresentationModels;

    public interface IResulsDetailPresenter
    {
        IResultsDetailView View { get; set; }
    }

    public class ResultsDetailPresenter : IResulsDetailPresenter
    {
        public ResultsDetailPresenter(IResultsDetailView view, IHumanBodyViewModel bodyViewModel)
        {
            View = view;

            View.Model = bodyViewModel;
            
        }

        public IResultsDetailView View { get; set; }
    }
}
