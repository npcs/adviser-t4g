﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;

using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;
using Adviser.Common.Logging;

namespace Adviser.Client.Core.Services
{
    public class ClientDbLogger<TEntry> : Logger<TEntry> where TEntry : ILogEntry, new()
    {
        public string ConnectionString
        {
            get;
            set;
        }

        public IEventAggregator EventAggregator
        {
            get;
            set;
        }

        protected override bool DoLog(TEntry logEntry)
        {
            bool result = false;
            AdviserLogEntry clientEntry = logEntry as AdviserLogEntry;
            AdviserDBEntities dbContext = new AdviserDBEntities();
            try
            {
                Error error = new Error();
                error.Time = DateTime.Now.ToUniversalTime();
                error.Lock = false;
                BinaryFormatter fmt = new BinaryFormatter();
                using (MemoryStream stream = new MemoryStream())
                {
                    fmt.Serialize(stream, clientEntry);
                    error.Data = stream.ToArray();
                }
                dbContext.AddToErrorSet(error);
                dbContext.SaveChanges();
                result = true;

                clientEntry.Id = error.ID;

                EventAggregator.GetEvent<SendErrorReportEvent>().Publish(clientEntry);
            }
            catch
            {
                result = false;
            }
            finally
            {
                dbContext.Dispose();
            }

            return result;
        }
    }
}
