﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Client.DataModel;
    using Adviser.Client.Core.Presentation;
    using Adviser.Client.Core.Events;
    using Adviser.Client.Core.Services;

    /// <summary>
    /// Presentation model for symptom.
    /// </summary>
    public class SymptomViewModel : ViewModelBase
    {
        private bool _isChecked = false;
        private Symptom _symptom;
        private Visit _visit;
        private object _synchlock = new object();

        public SymptomViewModel(Symptom symptom, Visit visit)
        {
            _symptom = symptom;
            _visit = visit;
            if (visit.Symptoms.Contains(symptom))
                _isChecked = true;
        }

        public string Name
        {
            get { return _symptom.Name; }
        }

        public string Description
        {
            get { return _symptom.Description; }
        }

        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                lock (_synchlock)
                {
                    if (_isChecked)
                    {
                        _isChecked = false;
                        _visit.Symptoms.Remove(Symptom);
                    }
                    else
                    {
                        _isChecked = true;
                        _visit.Symptoms.Add(Symptom);
                    }
                }
                base.RaisePropertyChangedEvent("IsChecked");
            }
        }

        public Symptom Symptom
        {
            get { return _symptom; }
            private set { _symptom = value; }
        }
    }
}
