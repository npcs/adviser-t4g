﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Wpf.Commands;
using Adviser.Client.Core.Presentation;

namespace Adviser.Modules.Print
{
    public interface IPrintReportView
    {
        object Model { get; set; }
        void AddPrintSection(UserControl printView);

    }
}
