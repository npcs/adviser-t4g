﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Microsoft.Practices.Composite.Events;
using Advisor.Common.DataModel;

using System.Data.Objects;
using System.Data.Objects.DataClasses;

namespace Advisor.Modules.Catalog.Views
{
    /// <summary>
    /// Interaction logic for PatientListView.xaml
    /// </summary>
    public partial class PatientListView : UserControl, IPatientListView
    {
        public PatientListView()
        {
            InitializeComponent();
        }

        public ObjectResult<Patient> Model
        {
            get { return this.DataContext as ObjectResult<Patient>; }
            set { this.DataContext = value; }
        }

        public event EventHandler<DataEventArgs<Patient>> PatientSelected = delegate { };

        private void PatientList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Patient selected = e.AddedItems[0] as Patient;
                if (selected != null)
                {
                    PatientSelected(this, new DataEventArgs<Patient>(selected));
                }
            }
        }
    }
}
