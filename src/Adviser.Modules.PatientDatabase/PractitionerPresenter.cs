﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;
using System.ComponentModel;
using System.Security.Cryptography;

using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Unity;

using Adviser.Common.Domain;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Presentation;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Interfaces;
using Adviser.Client.Core.Services;

namespace Adviser.Modules.PatientDatabase
{
    public class PractitionerPresenter : IPractitionerPresenter
    {
        private readonly IEventAggregator _eventAggregator;
        private IUnityContainer _container;
        private IChildWindow _window;
        private AppointmentSession _session;
        private IPractitionerModel _model;
        private IDBService _dbService;
        private IAdviserServiceAgent _server;
        private IConfigurationDataService _configuration;

        #region IPractitionerPresenter

        private IPractitionerDetailsView _detailsView;

        public IPractitionerDetailsView DetailsView
        {
            get { return _detailsView; }
            set { _detailsView = value; }
        }

        private IPractitionerLoginView _loginView;

        public IPractitionerLoginView LoginView
        {
            get { return _loginView; }
            set { _loginView = value; }
        }

        private IPractitionerRetrievePasswordView _retrievePasswordView;

        public IPractitionerRetrievePasswordView RetrievePasswordView
        {
            get { return _retrievePasswordView; }
            set { _retrievePasswordView = value; }
        }

        public void ShowUpdateAccount(bool isUpdateMode)
        {

            //-update the model with the current patient
            if (isUpdateMode)
            {
                _model.BusinessName = _session.Practitioner.BusinessName;
                _model.City = _session.Practitioner.City;
                _model.Country = _session.Practitioner.Country;
                _model.Email = _session.Practitioner.Email;
                _model.Fax = _session.Practitioner.Fax;
                _model.FirstName = _session.Practitioner.FirstName;
                _model.LastName = _session.Practitioner.LastName;
                _model.MidName = _session.Practitioner.MidName;
                _model.Occupation = _session.Practitioner.Occupation;
                _model.Password = _session.Practitioner.Password;
                _model.StateProvince = _session.Practitioner.StateProvince;
                _model.PostalCode = _session.Practitioner.PostalCode;
                _model.PrimaryPhone = _session.Practitioner.PrimaryPhone;
                _model.SecondaryPhone = _session.Practitioner.SecondaryPhone;
                _model.StreetAddress = _session.Practitioner.StreetAddress;
                _model.TypeOfPractice = _session.Practitioner.TypeOfPractice;
            }

            _detailsView.IsUpdateMode = isUpdateMode;
            _window.WindowTitle = "Practitioner Details";
            _window.SetContent(_detailsView);
            _window.ChildWindowStyle = WindowStyle.None;
            _window.ChildWindowWidth = 450;
            _window.ChildWindowHeight = 450;
            _window.ShowAsDialog();
        }

        public void Start()
        {
            //-if there are no practitioners in the database show create account window, otherwise show login window
            if (_dbService.DoesAnyPractitionerExist())
            {
                ShowLogin();
            }
            else
            {
                ShowUpdateAccount(false);
                _detailsView.IsUpdateMode = false;
            }
        }

        public void ShowLogin()
        {
            _window.WindowTitle = "Login";
            _window.SetContent(_loginView);
            _window.ChildWindowStyle = WindowStyle.None;
            _window.ChildWindowWidth = 410;
            _window.ChildWindowHeight = 180;
            _window.ShowAsDialog();
        }

        #endregion

        private void ShowRetrievePassword()
        {
            _window.WindowTitle = "Retrieve Password";
            _window.SetContent(_retrievePasswordView);
            _window.ShowAsDialog();
        }

        public PractitionerPresenter(IChildWindow window, IEventAggregator eventAggregator,
            IPractitionerModel model,
            IPractitionerDetailsView detailsView,
            IPractitionerLoginView loginView,
            IPractitionerRetrievePasswordView retrievePasswordView,
            IUnityContainer container,
            IDBService dbService,
            IConfigurationDataService configuration,
            AppointmentSession session,
            IAdviserServiceAgent server)
        {
            _configuration = configuration;

            _window = window;
            _window.ChildWindowSizeToContent = SizeToContent.WidthAndHeight;
            _window.ChildWindowStyle = WindowStyle.None;

            _server = server;

            _model = model;
            _container = container;
            _eventAggregator = eventAggregator;
            _session = session;
            _detailsView = detailsView;
            _detailsView.Model = model;
            _detailsView.CancelClicked += new EventHandler(_detailsView_CancelClicked);
            _detailsView.UpdateClicked += new EventHandler(_detailsView_UpdateClicked);
            _detailsView.CreateClicked += new EventHandler(_detailsView_CreateClicked);
            _detailsView.OkClicked += new EventHandler(_detailsView_OkClicked);

            _dbService = dbService;

            // pre-populate Email from user profile:
            string email = "";

            try
            {
                email = (_configuration.LoadXmlSettings("UserProfile.xml"))["LastLoginName"];
            }
            catch { }

            model.Email = email;
#if DEBUG
            model.Password = "aaa";
#endif
            model.DataSyncEnabled = !String.IsNullOrEmpty(_configuration.GetAppSetting("datasync")) 
                ? Boolean.Parse(_configuration.GetAppSetting("datasync")) 
                : false;

            _loginView = loginView;
            _loginView.Model = model;

            _loginView.LoginClicked += new EventHandler(_loginView_LoginClicked);
            _loginView.CancelClicked += new EventHandler(_loginView_CancelClicked);
            _loginView.ForgotPasswordClicked += new EventHandler(_loginView_ForgotPasswordClicked);
            _loginView.CreateAccountClicked += new EventHandler(_loginView_CreateAccountClicked);
            _loginView.DataSyncClicked += new EventHandler(_loginView_DataSyncClicked);

            _retrievePasswordView = retrievePasswordView;
            _retrievePasswordView.Model = model;
            _retrievePasswordView.CancelClicked += new EventHandler(_retrievePasswordView_CancelClicked);
            _retrievePasswordView.RetrievePasswordClicked += new EventHandler(_retrievePasswordView_RetrievePasswordClicked);

            _eventAggregator.GetEvent<PractitionerSyncCompletedEvent>().Subscribe(OnPractitionerSyncCompleted);
        }

        /// <summary>
        /// Event handler that sends account registration request to the server.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SendAccountRegistrationRequest(object sender, DoWorkEventArgs e)
        {
            Practitioner newPractitioner = new Practitioner();
            UpdatePractitionerWithModel(newPractitioner);
            newPractitioner.ID = Guid.NewGuid();
            newPractitioner.LastUpdDate = DateTime.Now.ToUniversalTime();

            string message = String.Empty;

            //- If practitioner is succesfully registered, then update local database
            _model.IsStatusOk = _server.RegisterPractitioner(newPractitioner, out message);
            if (_model.IsStatusOk)
            {
                newPractitioner.Status = AccountStatus.Confirmed;
                _dbService.CreateNewPractitioner(ref newPractitioner);
                _session.Practitioner = newPractitioner;
            }
            else
            {
                newPractitioner.Status = AccountStatus.NotConfirmend;
            }
            e.Result = message;
        }

        private void AccountRegistrationRequestReceived(object sender, RunWorkerCompletedEventArgs e)
        {
            string message = (string)e.Result;
            if (_model.IsStatusOk)
            {
                _eventAggregator.GetEvent<PractitionerChangedEvent>().Publish(_session.Practitioner);
            }
            _detailsView.ShowMessage(message);
        }

        #region DetailsView EventHandlers

        private void UpdatePractitionerWithModel(Practitioner practitioner)
        {
            practitioner.FirstName = _model.FirstName;
            practitioner.LastName = _model.LastName;
            practitioner.MidName = _model.MidName;
            practitioner.BusinessName = _model.BusinessName;
            practitioner.TypeOfPractice = _model.TypeOfPractice;
            practitioner.StreetAddress = _model.StreetAddress;
            practitioner.City = _model.City;
            practitioner.StateProvince = _model.StateProvince;
            practitioner.PostalCode = _model.PostalCode;
            practitioner.Country = _model.Country;
            practitioner.Occupation = _model.Occupation;
            practitioner.PrimaryPhone = _model.PrimaryPhone;
            practitioner.SecondaryPhone = _model.SecondaryPhone;
            practitioner.Fax = _model.Fax;
            practitioner.Email = _model.Email;

            //TODO: store password hash instead:
            string passwordHash = UTF8Encoding.UTF8.GetString(new SHA1Managed().ComputeHash(new UTF8Encoding().GetBytes(_model.Password)));
            practitioner.Password = _model.Password;

            practitioner.BusinessName = _model.BusinessName;
        }

        void _detailsView_CreateClicked(object sender, EventArgs e)
        {
            //TODO: Add the logic: If Device is not connected - display varning message

            if (!_dbService.IsPractitionerEmailUnique(_model.Email))
            {
                _detailsView.ShowMessage("Account with this email already exists in the local database.\nPlease use unique email address to register.");
                return;
            }

            _detailsView.ShowMessage("Please wait while we contacting server to register new account...", false);

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(SendAccountRegistrationRequest);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(AccountRegistrationRequestReceived);
            worker.RunWorkerAsync();
        }

        void _detailsView_UpdateClicked(object sender, EventArgs e)
        {
            UpdatePractitionerWithModel(_session.Practitioner);
            string message = "";
            if (_session.Practitioner.Status == AccountStatus.NotConfirmend)
            {
                _model.IsStatusOk = _server.UpdatePractitioner(_session.Practitioner, out message);
            }
            else
            {
                _session.Practitioner.Status = AccountStatus.NotConfirmend;
                _session.Practitioner.LastUpdDate = DateTime.Now.ToUniversalTime();
                _model.IsStatusOk = _server.UpdatePractitioner(_session.Practitioner, out message);
            }

            _detailsView.ShowMessage(message);
            if (_model.IsStatusOk)
            {
                _session.Practitioner.Status = AccountStatus.Confirmed;
            }
            else
            {
                _session.Practitioner.Status = AccountStatus.NotConfirmend;
            }
            
            _dbService.SaveChanges();
        }

        void _detailsView_OkClicked(object sender, EventArgs e)
        {
            if (_model.IsStatusOk)
            {
                _window.CloseWindow();
            }
        }

        void _detailsView_CancelClicked(object sender, EventArgs e)
        {
            if (_detailsView.IsUpdateMode)
            {
                _window.CloseWindow();
            }
            else
            {
                ShowLogin();
            }
        }

        #endregion

        #region RetrievePasswordView EventHandlers

        void _retrievePasswordView_RetrievePasswordClicked(object sender, EventArgs e)
        {
            string message = "Cannot connect to the server.\nMake sure your computer is connected to the internet";
            _model.IsStatusOk = _server.RetrievePassword(_model.Email, out message);
            _retrievePasswordView.ShowMessage(message);
        }

        void _retrievePasswordView_CancelClicked(object sender, EventArgs e)
        {
            ShowLogin();
        }

        #endregion

        #region LoginView EventHandlers

        void _loginView_CreateAccountClicked(object sender, EventArgs e)
        {
            ShowUpdateAccount(false);
            _detailsView.IsUpdateMode = false;
        }

        void _loginView_ForgotPasswordClicked(object sender, EventArgs e)
        {
            ShowRetrievePassword();
        }

        void _loginView_CancelClicked(object sender, EventArgs e)
        {
            CloseWindow();
        }

        private void CloseWindow()
        {
            _window.CloseWindow();
            _eventAggregator.GetEvent<CloseApplicationEvent>().Publish(null);
        }

        void _loginView_DataSyncClicked(object sender, EventArgs e)
        {
			if (_model.DataSyncInProcess)
			{
				return;
			};
			_model.DataSyncInProcess = true;
            var se = new PractitionerSyncEventArgs()
            {
                Email = _loginView.Model.Email
			};
            
            _eventAggregator.GetEvent<PractitionerSyncEvent>().Publish(se);
            _loginView.ShowMessage("Please wait while data is being retrieved...", false);
        }

        void _loginView_LoginClicked(object sender, EventArgs e)
        {
            string datasync = _configuration.GetAppSetting("datasync");

            string email = _loginView.Model.Email;
            string password = _loginView.Model.Password;

            //- Validate login
            //TODO: validate hash instead of password
            //string passwordHash = UTF8Encoding.UTF8.GetString(new SHA1Managed().ComputeHash(new UTF8Encoding().GetBytes(password)));
            Practitioner p = null;
            if(_loginView.Model.DataSyncEnabled)
                p = _dbService.GetPractitionerByEmailOnly(email);
            else
                p = _dbService.GetPractitionerByEmail(email, password);

            if (p != null)
            {
                _session.Practitioner = p;
                _eventAggregator.GetEvent<PractitionerChangedEvent>().Publish(p);

                //-save email to the profile xml file
                Adviser.Common.Collections.XmlDictionary<string, string> xml = new Adviser.Common.Collections.XmlDictionary<string, string>();
                xml.Add("LastLoginName", email);
                _configuration.SaveXmlSettings(xml, "UserProfile.xml");

                if (p.Status == AccountStatus.Confirmed)
                {
                    _window.CloseWindow();
                }
                else
                {
                    ShowUpdateAccount(true);
                    _loginView.ShowMessage("Your account is not confirmed.\nPlease verify your contact information and click 'Update' button.\n(Computer should be connected to the internet)", true);
                }
            }
            else
            {
				//-the practitioner is not found in the local database. Try to authenticate on the server.
				_loginView.ShowMessage("Invalid email or password. Please try again.", true);
            }
        }

        #endregion

        public void OnPractitionerSyncCompleted(PractitionerSyncCompletedEventArgs e)
        {
			_model.DataSyncInProcess = false;
            if(e.Practitioner == null)
            {
                _loginView.ShowMessage("Unable to receive practitioner data. The reason given:\n" + e.Message, true);
                return;
            }

            _window.CloseWindow();

            var p = _dbService.GetPractitionerByEmailOnly(e.Practitioner.Email);
            _session.Practitioner = p;
            _eventAggregator.GetEvent<PractitionerChangedEvent>().Publish(p);

            //-save email to the profile xml file
            Adviser.Common.Collections.XmlDictionary<string, string> xml = new Adviser.Common.Collections.XmlDictionary<string, string>();
            xml.Add("LastLoginName", e.Practitioner.Email);
            _configuration.SaveXmlSettings(xml, "UserProfile.xml");
        }
    }
}
