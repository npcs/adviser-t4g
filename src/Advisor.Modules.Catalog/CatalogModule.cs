﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;

using Advisor.Common;
using Advisor.Common.Presentation;
using Advisor.Modules.Catalog.Views;
using Advisor.Modules.Catalog.Services;
using Advisor.Modules.Catalog.Controllers;
using System.Diagnostics;

namespace Advisor.Modules.Catalog
{
    public class CatalogModule: IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public CatalogModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            RegisterViewsAndServices();

            try
            {
            PatientPresenter presenter = this._container.Resolve<PatientPresenter>();

            IRegion region = _regionManager.Regions[RegionNames.ContentRegion];
            region.Add(presenter.View, "PatientView");
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("AdvisorClient", ex.ToString(), EventLogEntryType.Error);
            }  
            
        }

        protected void RegisterViewsAndServices()
        {
            //_container.RegisterType<ICatalogView, CatalogView>();
            //_container.RegisterType<ICatalogController, CatalogController>();
            //_container.RegisterType<ICatalogPresenter, CatalogPresenter>();

            _container.RegisterType<IPatientView, PatientView>();
            _container.RegisterType<IPatientPresenter, PatientPresenter>();
            
            //_container.RegisterType<IPatientListView, PatientListView>();
            //_container.RegisterType<IPatientListPresenter, PatientListPresenter>();

            //_container.RegisterType<IPatientDetailsView, PatientDetailsView>();
            //_container.RegisterType<IPatientDetailsPresenter, PatientDetailsPresenter>();

            _container.RegisterType<IPatientService, PatientService>();

            
            
        }
    }
}
