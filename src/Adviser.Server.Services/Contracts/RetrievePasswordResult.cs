﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Adviser.Server.Services.Contracts
{
    [DataContract]
    public class RetrievePasswordResult
    {
        [DataMember]
        public bool IsSuccess;

        [DataMember]
        public string Message;
    }
}
