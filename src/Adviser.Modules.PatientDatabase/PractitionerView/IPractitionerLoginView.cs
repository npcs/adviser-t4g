﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Modules.PatientDatabase
{
    public interface IPractitionerLoginView
    {
        IPractitionerModel Model { get; set; }
        event EventHandler LoginClicked;
        event EventHandler CreateAccountClicked;
        event EventHandler ForgotPasswordClicked;
        event EventHandler DataSyncClicked;
        event EventHandler CancelClicked;
		void ShowMessage(string message, bool isOKButtonVisible);
	}
}
