----- Get Practitioners, Patients Visits -------------
select a.FirstName, a.LastName, b.FirstName, b.LastName, c.Date, c.Notes from practitioners a
inner join patients b on a.id=b.practitionerId 
inner join visits c on c.patientid = b.id order by a.LastName

------ Remove registration ---------------------------
declare @ownerId uniqueidentifier
set @ownerId='353c24cb-b4f8-43f6-93b6-7d16466bcca9'

update Devices set Status='Available', OwnerID=NULL where OwnerID=@ownerId
delete from Accounts where PractitionerID=@ownerId
delete from Practitioners where ID=@ownerId

------- Delete Client SEED Data --------------

delete from chakras
delete from conditions
delete from medicine
delete from organexamination
delete from organs
delete from symptoms

---------------------------------------------

---- Delete local practitioners data ------
SELECT TOP 1000 pr.[ID]
		,pr.Email
      ,p.[FirstName]
      ,p.[LastName]
      ,v.[Date]
  FROM [adviserserverdb].[dbo].[Practitioners] pr
  inner join patients p on p.PractitionerID = pr.ID
  inner join visits v on v.PatientID = p.ID
  where pr.ID = 'E14A1C65-04F5-4AC7-A2F2-065DB5EB2D13'

declare @id uniqueidentifier
set @id = '36FE606B-D6E3-4ABD-9DCE-1E3844F84DD8'

delete from SymptomVisitXRef where VisitID in (select ID from Visits where PatientID in (select ID from Patients where PractitionerId=@id))
delete from OrganExamination where VisitID in (select ID from Visits where PatientID in (select ID from Patients where PractitionerId=@id))
delete from Prescriptions where VisitID in (select ID from Visits where PatientID in (select ID from Patients where PractitionerId=@id))
delete from Visits where PatientId in (select ID from Patients where practitionerId=@id)
delete from patients where PractitionerID=@id
delete from Practitioners where ID=@id

delete from dbo.organexamination where visitid not in (select id from dbo.visits)
delete from dbo.SymptomVisitXRef where visitid not in (select id from dbo.visits)

