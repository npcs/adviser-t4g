﻿using System;
using Microsoft.Practices.Composite.Wpf.Events;
using Advisor.Common.DataModel;

namespace Advisor.Common.Events
{
    public class VisitSelectedEvent : CompositeWpfEvent<Visit>
    {
    }
}
