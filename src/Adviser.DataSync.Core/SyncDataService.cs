﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Configuration;
using System.ComponentModel;
using ClientModel = Adviser.Client.DataModel;
//using ServerModel = Adviser.Server.DataModel;

namespace Adviser.DataSync.Core
{
    public class SyncDataService
    {
        private string _srcConnString = "";
        private string _destConnString = @"Data Source=.\SQLEXPRESS;Database=AdviserDB;Integrated Security=True;MultipleActiveResultSets=True";
        private Guid _masterPractitionerID;
        private string _masterEmail;

        private string _practitionerQueryFormat = "{0} from Practitioners where ID='{1}'";
        private string _patientsQueryFormat = "{0} from Patients where PractitionerID='{1}' and deleted=0";
        private string _visitsQueryFormat = "{0} from Visits where PatientId in (select ID from Patients where practitionerId='{1}' and deleted=0)";
        private string _prescriptionsQueryFormat = "{0} from Prescriptions where VisitID in (select ID from Visits where PatientID in (select ID from Patients where PractitionerId='{1}' and deleted=0) and deleted=0)";
        private string _organExamsQueryFormat = "{0} from OrganExamination where VisitID in (select ID from Visits where PatientID in (select ID from Patients where PractitionerId='{1}' and deleted=0) and deleted=0)";
        private string _symptomVisitsQueryFormat = "{0} from SymptomVisitXRef where VisitID in (select ID from Visits where PatientID in (select ID from Patients where PractitionerId='{1} and deleted=0') and deleted=0)";
        private string _orphanVisitClause = " or VisitID not in (select VisitID from Visits)";
        private DBNull dbNull;

        //private ClientModel.AdviserDBEntities _clientDb;
        //private ServerModel.AdviserDBEntities _serverDb;

        public SyncDataService(string srcConnString, string destConnString, string masterEmail)
        {
            _srcConnString = srcConnString;
            _destConnString = destConnString;
            _masterEmail = masterEmail;

            //_clientDb = new ClientModel.AdviserDBEntities(destConnString);
            //_serverDb = new ServerModel.AdviserDBEntities(srcConnString);
        }

        public SyncDataService()
        {
            _srcConnString = ConfigurationManager.AppSettings["srcConnection"];
            _destConnString = ConfigurationManager.AppSettings["destConnection"];
            _masterEmail = ConfigurationManager.AppSettings["masterEmail"];

            //_clientDb = new ClientModel.AdviserDBEntities("name=AdviserDBEntities", "AdviserDBEntities");
            //_serverDb = new ServerModel.AdviserDBEntities("name=AdviserServerDBEntities", "AdviserDBEntities");
        }

        public DataSyncResult SyncData(PractitionerRecord practitioner, BackgroundWorker worker, DoWorkEventArgs args)
        {
            DataSyncResult result = new DataSyncResult();
            var practitionerId = practitioner.ID;
            result.Data = practitioner.ID;

            string destTableName = "";
            string srcQuery = String.Format(_patientsQueryFormat, "select * ", practitionerId);
            string destQuery = "SELECT COUNT(*) FROM " + destTableName;
            try
            {
                using (SqlConnection srcConn = new SqlConnection(_srcConnString))
                {
                    using (SqlConnection destConn = new SqlConnection(_destConnString))
                    {
                        destConn.Open();
                        srcConn.Open();
                        SqlTransaction transaction = null;// destConn.BeginTransaction();

                        //SqlCommand checkCmd = new SqlCommand("", destConn);
                        //checkCmd.CommandText = String.Format(_prescriptionsQueryFormat, "select count(*) ", practitionerId);
                        //var practitionerExists = checkCmd.ExecuteScalar();
                        //if((int)practitionerExists == 0)
                        //{
                            
                        //}

                        SqlCommand cleanUpCmd = new SqlCommand("", destConn);
                        cleanUpCmd.CommandText = String.Format(_organExamsQueryFormat, "delete ", practitionerId);
                        int deletedRows = cleanUpCmd.ExecuteNonQuery();
                        cleanUpCmd.CommandText = String.Format(_organExamsQueryFormat, "delete ", practitionerId);
                        deletedRows = cleanUpCmd.ExecuteNonQuery();
                        //cleanUpCmd.CommandText = String.Format(_prescriptionsQueryFormat + _orphanVisitClause, "delete ", practitionerId);
                        //deletedRows = cleanUpCmd.ExecuteNonQuery();
                        cleanUpCmd.CommandText = String.Format(_symptomVisitsQueryFormat, "delete ", practitionerId);
                        deletedRows = cleanUpCmd.ExecuteNonQuery();
                        cleanUpCmd.CommandText = String.Format(_visitsQueryFormat, "delete ", practitionerId);
                        deletedRows = cleanUpCmd.ExecuteNonQuery();
                        cleanUpCmd.CommandText = String.Format(_patientsQueryFormat, "delete ", practitionerId);
                        deletedRows = cleanUpCmd.ExecuteNonQuery();
                        cleanUpCmd.CommandText = String.Format(_practitionerQueryFormat, "delete ", practitionerId);
                        deletedRows = cleanUpCmd.ExecuteNonQuery();
                        
                        worker.ReportProgress(0, "Deleted old practitioner from local device.");

                        ProcessCancellation(transaction, worker, args);

                        // load practitioner
                        destTableName = "Practitioners";
                        srcQuery = String.Format(_practitionerQueryFormat, "select * ", practitionerId);
                        destQuery = "SELECT COUNT(*) FROM " + destTableName;
                        CopyTable(destTableName, srcQuery, destQuery, srcConn, destConn, transaction);

                        if (ProcessCancellation(transaction, worker, args))
                            return result;

                        // load patients
                        destTableName = "Patients";
                        srcQuery = String.Format(_patientsQueryFormat, "select * ", practitionerId);
                        destQuery = "SELECT COUNT(*) FROM " + destTableName;
                        CopyTable(destTableName, srcQuery, destQuery, srcConn, destConn, transaction);

                        if (ProcessCancellation(transaction, worker, args))
                            return result;

                        // load visits
                        destTableName = "Visits";
                        srcQuery = String.Format(_visitsQueryFormat, "select * ", practitionerId);
                        destQuery = "SELECT COUNT(*) FROM " + destTableName;
                        CopyTable(destTableName, srcQuery, destQuery, srcConn, destConn, transaction);

                        if (ProcessCancellation(transaction, worker, args))
                            return result;

                        // load SymptomVisitXRef
                        destTableName = "SymptomVisitXRef";
                        srcQuery = String.Format(_symptomVisitsQueryFormat, "select * ", practitionerId);
                        destQuery = "SELECT COUNT(*) FROM " + destTableName;
                        CopyTable(destTableName, srcQuery, destQuery, srcConn, destConn, transaction);

                        if (ProcessCancellation(transaction, worker, args))
                            return result;

                        // load OrganExamination
                        destTableName = "OrganExamination";
                        srcQuery = String.Format(_organExamsQueryFormat, "select * ", practitionerId);
                        destQuery = "SELECT COUNT(*) FROM " + destTableName;
                        CopyTable(destTableName, srcQuery, destQuery, srcConn, destConn, transaction);

                        if (ProcessCancellation(transaction, worker, args))
                            return result;

                        //// load Prescriptions
                        //destTableName = "Prescriptions";
                        //srcQuery = String.Format(_prescriptionsQueryFormat, "select * ", practitionerId);
                        //destQuery = "SELECT COUNT(*) FROM " + destTableName;
                        //CopyTable(destTableName, srcQuery, destQuery, srcConn, destConn, transaction);

                        //if (ProcessCancellation(transaction, worker, args))
                        //    return result;

                    }
                }

                result.Message = "Succesfully transferred data";
                result.IsSuccess = true;

                //using (ClientModel.AdviserDBEntities localDb = new ClientModel.AdviserDBEntities())
                //{
                //    var p = localDb.PractitionerSet.FirstOrDefault(x => x.ID == practitioner.ID);
                //    if (p == null)
                //        throw new ApplicationException(String.Format("Practitioner ID: {0} doesn't exist in the database.", _masterPractitionerID.ToString()));

                //    if(p.Status != ClientModel.AccountStatus.Confirmed)
                //    {
                //        p.Status = ClientModel.AccountStatus.Confirmed;
                //        localDb.SaveChanges();
                //    }
                //    p.Patients.Load();
                    
                //}
            }
            catch (Exception ex)
            {
                result.Message = "Error while transferring " + destTableName + "\n" + ex.ToString();
                result.IsSuccess = false;
            }

            return result;
        }

        public IList<PractitionerRecord> FindPractitioner(string email)
        {
            var result = new List<PractitionerRecord>();
            var sqlQuery = "select * from Practitioners where Email=@email";
            using (SqlConnection conn = new SqlConnection(_srcConnString))
            {
                using (var cmd = new SqlCommand(sqlQuery, conn))
                {
                    cmd.Parameters.Add("@email", SqlDbType.NVarChar, 100);
                    cmd.Parameters["@email"].Value = email;

                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(
                                new PractitionerRecord()
                                {
                                    ID = reader.GetGuid(0),
                                    FirstName = (string)reader["FirstName"],
                                    LastName = (string)reader["LastName"],
                                    BusinessName = (string)reader["BusinessName"],
                                    City = (string)reader["City"],
                                    StateProvince = (string)reader["StateProvince"],
                                    PrimaryPhone = (string)reader["PrimaryPhone"],
                                    Email = (string)reader["Email"],
                                    StatusID = (short)reader["StatusID"]
                                }
                            );
                        }
                    }
                }
            }

            return result;
        }

        //public DataSyncResult ImportPractitioner(string email, BackgroundWorker worker, DoWorkEventArgs args)
        //{
        //    DataSyncResult result = new DataSyncResult();
        //    try
        //    {
        //        var pc = _clientDb.PractitionerSet.FirstOrDefault(x => x.Email == email);
        //        if (pc == null)
        //        {
        //            var ps = _serverDb.PractitionerSet.FirstOrDefault(x => x.Email == email);
        //            if (ps == null)
        //            {
        //                result.IsSuccess = false;
        //                result.Message = "Unable to find practitioner for " + email;
        //                return result;
        //            }

        //            pc = new ClientModel.Practitioner()
        //            {
        //                ID = ps.ID,
        //                BusinessName = ps.BusinessName,
        //                City = ps.City,
        //                Country = ps.Country,
        //                Email = ps.Email,
        //                Fax = ps.Fax,
        //                FirstName = ps.FirstName,
        //                LastName = ps.LastName,
        //                LastUpdDate = ps.LastUpdDate,
        //                MidName = ps.MidName,
        //                Occupation = ps.Occupation,
        //                Password = ps.Password,
        //                PostalCode = ps.PostalCode,
        //                PrimaryPhone = ps.PrimaryPhone,
        //                SecondaryPhone = ps.SecondaryPhone,
        //                StateProvince = ps.StateProvince,
        //                Status = ClientModel.AccountStatus.Confirmed,
        //                StreetAddress = ps.StreetAddress,
        //                TypeOfPractice = ps.TypeOfPractice
        //            };

        //            _clientDb.AddToPractitionerSet(pc);
        //        }
        //        ImportPatients(pc);

        //        _clientDb.SaveChanges();
        //        result.IsSuccess = true;
        //        result.Message = "Successfully imported data for " + email;
        //    }
        //    catch(Exception ex)
        //    {
        //        var msg = String.Format("Error while importing data for practitioner: {0}.\n{1}", email, ex.ToString());
        //        result.IsSuccess = false;
        //        result.Message = msg;

        //        args.Result = result;
        //    }

        //    return result;
        //}

        //private void ImportPatients(ClientModel.Practitioner practitioner)
        //{
        //    var existingIDs = _clientDb.PatientSet.Where(x => x.Practitioners.ID == practitioner.ID).Select(x => x.ID).ToList();

        //    var patients = _serverDb.PatientSet.Where(x => 
        //        x.Practitioners.ID == practitioner.ID 
        //        && !x.Deleted).ToList();

        //    foreach(var src in patients)
        //    {
        //        var patient = _clientDb.PatientSet.FirstOrDefault(x => x.ID == src.ID);
        //        if (patient == null)
        //        {
        //            patient = new ClientModel.Patient()
        //            {
        //                ID = src.ID,
        //                City = src.City,
        //                Country = src.Country,
        //                Deleted = src.Deleted,
        //                DOB = src.DOB,
        //                Email = src.Email,
        //                FirstName = src.FirstName,
        //                Height = src.Height,
        //                LastName = src.LastName,
        //                LastUpdDate = src.LastUpdDate,
        //                Lifestyle = src.Lifestyle,
        //                MajorComplain = src.MajorComplain,
        //                MajorIllnessId = src.MajorIllnessId,
        //                MidName = src.MidName,
        //                Occupation = src.Occupation,
        //                PostalCode = src.PostalCode,
        //                PrimaryPhone = src.PrimaryPhone,
        //                SecondaryPhone = src.SecondaryPhone,
        //                Sex = src.Sex,
        //                StateProvince = src.StateProvince,
        //                StreetAddress = src.StreetAddress,
        //                Weight = src.Weight
        //            };

        //            practitioner.Patients.Add(patient);
        //        }
   
        //        ImportVisits(patient);
        //    }
        //}

        //private void ImportVisits(ClientModel.Patient patient)
        //{
        //    var existingIDs = _clientDb.VisitSet.Where(x => x.Patient.ID == patient.ID).Select(x => x.ID).ToList();

        //    var visits = _serverDb.VisitSet.Include("Symptoms")
        //        .Where(x => x.Patient.ID == patient.ID
        //            && !x.Deleted
        //            && !existingIDs.Any(y => y == x.ID))
        //            .ToList();

        //    foreach (var src in visits)
        //    {
        //        var visit = _clientDb.VisitSet.FirstOrDefault(x => x.ID == src.ID);
        //        visit = new ClientModel.Visit()
        //        {
        //            ID = src.ID,
        //            Date = src.Date,
        //            Deleted = src.Deleted,
        //            ExamResult = src.ExamResult,
        //            MeasurementData = src.MeasurementData,
        //            LastUpdDate = src.LastUpdDate,
        //            Notes = src.Notes,
        //            Patient = patient
        //        };

        //        foreach (var symptom in src.Symptoms)
        //        {
        //            var destSymptom = _clientDb.SymptomSet.SingleOrDefault(x => x.ID == symptom.ID);
        //            visit.Symptoms.Add(destSymptom);
        //        }

        //        _clientDb.AddToVisitSet(visit);

        //        ImportOrganExaminations(visit);
        //    }
        //}

        //private void ImportOrganExaminations(ClientModel.Visit visit)
        //{
        //    var exams = _serverDb.OrganExaminationSet.Include("Organ").Where(x => x.Visit.ID == visit.ID).ToList();
        //    foreach(var src in exams)
        //    {
        //        var organ = _clientDb.OrganSet.FirstOrDefault(x => x.ID == src.Organ.ID);
        //        var organExam = new ClientModel.OrganExamination()
        //        {
        //            ChronicDisease = src.ChronicDisease,
        //            ID = src.ID,
        //            Implant = src.Implant,
        //            IsEditable = false,
        //            Organ = organ,
        //            Removed = src.Removed,
        //            Surgery = src.Surgery,
        //            Value = src.Value,
        //            Visit = visit
        //        };

        //        _clientDb.AddToOrganExaminationSet(organExam);
        //    }
        //}

        //private void ImportSymptoms(ClientModel.Visit visit)
        //{

        //}

        private void CopyTable(
             string tableName,
             string srcQuery,
             string destQuery,
             SqlConnection srcConn,
             SqlConnection destConn,
             SqlTransaction transaction)
        {
            SqlCommand srcCmd = new SqlCommand(srcQuery, srcConn);

            if (destConn.State != ConnectionState.Open)
                destConn.Open();
            if (srcConn.State != ConnectionState.Open)
                srcConn.Open();

            IDataReader reader = srcCmd.ExecuteReader();

            SqlCommand controlCmd = new SqlCommand(destQuery, destConn);
            long rowsBefore = Convert.ToInt64(controlCmd.ExecuteScalar());

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConn))
            {
                bulkCopy.DestinationTableName = tableName;
                try
                {
                    bulkCopy.WriteToServer(reader);
                }
                catch (Exception ex)
                {
                    //TODO: log
                    throw ex;
                }
                finally
                {
                    reader.Close();
                }

                //long rowsAfter = Convert.ToInt64(controlCmd.ExecuteScalar());
                long rowsInserted = Convert.ToInt64(controlCmd.ExecuteScalar());//rowsAfter - rowsBefore;
                string statusMsg = rowsInserted + " rows transferred to '" + tableName + "' table.\n";
                //backgroundWorker1.ReportProgress(0, statusMsg);
            }
        }

        private bool ProcessCancellation(SqlTransaction transaction, BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (worker.CancellationPending)
            {
                e.Cancel = true;
                try
                {
                    if (transaction != null)
                        transaction.Rollback();
                    e.Result = "Operation was cancelled by user.";
                }
                catch (Exception ex)
                {
                    e.Result = "Error while cancelling transaction:\n" + ex.ToString();
                }
                return true;
            }
            else
            {
                return false;
            }
        }




        #region Bulk sync

        public void SyncData()
        {
            try
            {
                var filter = ConfigurationManager.AppSettings["filter"];
                var practitioners = GetPractitioners(filter);

                using(var srcConn = new SqlConnection(_srcConnString))
                {
                    List<Guid> patientIDs = new List<Guid>();
                    using (ClientModel.AdviserDBEntities destCtx = new ClientModel.AdviserDBEntities())
                    {
                        var masterPractitioner = destCtx.PractitionerSet.FirstOrDefault(x => x.Email == _masterEmail);
                        if (masterPractitioner == null)
                            throw new ApplicationException(String.Format("Practitioner ID: {0} doesn't exist in the database.", _masterPractitionerID.ToString()));

                        _masterPractitionerID = masterPractitioner.ID;
                        patientIDs = SyncPatients(masterPractitioner, practitioners, srcConn, destCtx);
                    }

                    using(var destConn = new SqlConnection(_destConnString))
                    {
                        SyncVisits(patientIDs, srcConn, destConn);
                    }
                }

            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private List<Guid> SyncPatients(
            ClientModel.Practitioner masterPractitioner,
            List<PractitionerRecord> practitioners,
            SqlConnection srcConn,
            ClientModel.AdviserDBEntities destContext)
        {
            var patientIDs = new List<Guid>();

            if (srcConn.State != ConnectionState.Open)
                srcConn.Open();

            foreach (var practitioner in practitioners)
            {
                var patientsQuery = String.Format(_patientsQueryFormat, practitioner.ID);

                SqlCommand cmd = new SqlCommand(patientsQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Connection = srcConn;
                using(SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var patientID = (Guid)reader["ID"];
                        patientIDs.Add(patientID);

                        var patient = destContext.PatientSet.FirstOrDefault(x => x.ID == patientID);
                        if (patient != null)
                        {// delete this patient to provide with new data
                            destContext.DeleteObject(patient);
                            destContext.SaveChanges();
                        }

                        destContext.AddToPatientSet(new ClientModel.Patient()
                        {
                            ID = patientID,
                            City = reader.GetValue<string>("City"),
                            Country = reader.GetValue<string>("Country"),
                            Deleted = false,
                            DOB = reader.GetValue<DateTime?>("DOB"),
                            Email = reader.GetValue<string>("Email"),
                            FirstName = reader.GetValue<string>("FirstName"),
                            LastName = reader.GetValue<string>("LastName"),
                            Height = reader.GetValue<int?>("Height"),
                            LastUpdDate = reader.GetValue<DateTime>("LastUpdDate"),
                            Lifestyle = reader.GetValue<string>("Lifestyle"),
                            MajorComplain = reader.GetValue<string>("MajorComplain"),
                            MajorIllnessId = reader.GetValue<int?>("MajorIllnessId"),
                            MidName = reader.GetValue<string>("MidName"),
                            Occupation = reader.GetValue<string>("Occupation"),
                            PostalCode = reader.GetValue<string>("PostalCode"),
                            PrimaryPhone = reader.GetValue<string>("PrimaryPhone"),
                            SecondaryPhone = reader.GetValue<string>("SecondaryPhone"),
                            Sex = reader.GetValue<string>("Sex"),
                            StateProvince = reader.GetValue<string>("StateProvince"),
                            StreetAddress = reader.GetValue<string>("StreetAddress"),
                            Weight = reader.GetValue<int?>("Weight"),
                            Practitioners = masterPractitioner
                        });

                    }
                }
                destContext.SaveChanges();
            }

            return patientIDs;
        }

        private void SyncVisits(List<Guid> patientIDs, 
            SqlConnection srcConn, 
            SqlConnection destConn)
        {
            //using (var destCmd = new SqlCommand(srcQuery, srcConn))
            //{
            //    var reader = destCmd.ExecuteReader();
            //    while (reader.Read())
            //    {
            //        var visitId = (Guid)reader["ID"];
            //        if (destContext.VisitSet.Any(x => x.ID == visitId))
            //            continue;

            //        destContext.AddToVisitSet(new ClientModel.Visit()
            //        {

            //        });
            //    }
            //}

            var includeFilter = patientIDs.ToSqlInFilter();

            var srcQuery = "SELECT * FROM Visits WHERE PatientId IN " + includeFilter;

            var existingIDs = new List<Guid>();

            if (destConn.State != ConnectionState.Open)
                destConn.Open();

            using (SqlCommand destCmd = new SqlCommand())
            {
                destCmd.Connection = destConn;
                destCmd.CommandText = "SELECT ID FROM Visits WHERE PatientId IN " + includeFilter;

                var reader = destCmd.ExecuteReader();
                while (reader.Read())
                {
                    existingIDs.Add((Guid)reader["ID"]);
                }
            }

            if (existingIDs.Count > 0)
            {
                var excludeFilter = existingIDs.ToSqlInFilter();
                srcQuery += " AND ID NOT IN " + excludeFilter;
            }

            if (srcConn.State != ConnectionState.Open)
                srcConn.Open();

            using(var srcCmd = new SqlCommand(srcQuery, srcConn))
            {
                var reader = srcCmd.ExecuteReader();
                using(var bulkCopy = new SqlBulkCopy(destConn))
                {
                    bulkCopy.DestinationTableName = "Visits";
                    try
                    {
                        bulkCopy.WriteToServer(reader);
                    }
                    catch(Exception ex)
                    {

                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
        }

        public List<PractitionerRecord> GetPractitioners(string filter)
        {
            List<PractitionerRecord> practitioners = new List<PractitionerRecord>();

            string query = "SELECT ID, FirstName, LastName, BusinessName, City, StateProvince, PrimaryPhone, Email, TypeOfPractice, Occupation, StatusID FROM Practitioners";
            if (!String.IsNullOrEmpty(filter))
                query += " WHERE " + filter;

            using (SqlConnection conn = new SqlConnection(_srcConnString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = query;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;

                conn.Open();
                using(SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        practitioners.Add(
                            new PractitionerRecord()
                            {
                                ID = reader.GetGuid(0),
                                FirstName = (string)reader["FirstName"],
                                LastName = (string)reader["LastName"],
                                BusinessName = (string)reader["BusinessName"],
                                City = (string)reader["City"],
                                StateProvince = (string)reader["StateProvince"],
                                PrimaryPhone = (string)reader["PrimaryPhone"],
                                Email = (string)reader["Email"],
                                StatusID = (short)reader["StatusID"]
                            }
                        );
                    }
                }
            }


            return practitioners;
        }

        private long BulkCopyTable(string destTableName, string readQuery)
        {
            string controlQuery = "SELECT COUNT(*) FROM " + destTableName;
            long rowsInserted = 0;

            using (SqlConnection srcConn = new SqlConnection(_srcConnString))
            {
                SqlCommand srcCmd = new SqlCommand(readQuery, srcConn);
                srcConn.Open();
                using(var reader = srcCmd.ExecuteReader())
                {
                    using (SqlConnection destConn = new SqlConnection(_destConnString))
                    {
                        SqlCommand controlCmd = new SqlCommand(controlQuery, destConn);
                        destConn.Open();
                        long rowsBefore = Convert.ToInt64(controlCmd.ExecuteScalar());
                        destConn.Close();

                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConn))
                        {
                            destConn.Open();
                            bulkCopy.DestinationTableName = destTableName;
                            try
                            {
                                bulkCopy.WriteToServer(reader);
                            }
                            catch (Exception ex)
                            {
                                //txtOutput2.Text = ex.ToString();
                            }
                            finally
                            {
                                reader.Close();
                            }

                            long rowsAfter = Convert.ToInt64(controlCmd.ExecuteScalar());
                            rowsInserted = rowsAfter - rowsBefore;
                            //txtOutput2.Text = rowsInserted + " rows inserted into '" + destTableName + "' table";
                            destConn.Close();
                        }
                    }
                }
            }

            return rowsInserted;
        }

        #endregion
    }
}
