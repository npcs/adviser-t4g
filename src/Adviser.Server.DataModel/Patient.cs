﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Server.DataModel
{
    public partial class Patient
    {
        /// <summary>
        /// Copies only patient domain specific data (no children entities, no framework fields).
        /// </summary>
        /// <param name="patient"></param>
        public void CopyData(Patient patient)
        {
            this.FirstName = patient.FirstName;
            this.LastName = patient.LastName;
            this.MidName = patient.MidName;
            this.City = patient.City;
            this.Country = patient.Country;
            this.Deleted = patient.Deleted;
            this.DOB = patient.DOB;
            this.Email = patient.Email;
            this.Height = patient.Height;
            this.LastUpdDate = patient.LastUpdDate;
            this.Lifestyle = patient.Lifestyle;
            this.MajorComplain = patient.MajorComplain;
            this.MajorIllnessId = patient.MajorIllnessId;
            this.Occupation = patient.Occupation;
            this.PostalCode = patient.PostalCode;
            this.PrimaryPhone = patient.PrimaryPhone;
            this.SecondaryPhone = patient.SecondaryPhone;
            this.Sex = patient.Sex;
            this.StreetAddress = patient.StreetAddress;
            this.Weight = patient.Weight;
        }

        /// <summary>
        /// Makes shallow copy of the object graph.
        /// </summary>
        /// <param name="patient"></param>
        /// <returns></returns>
        public Patient ShallowCopy(Patient patient)
        {
            return (Patient)this.MemberwiseClone();
        }
    }
}
