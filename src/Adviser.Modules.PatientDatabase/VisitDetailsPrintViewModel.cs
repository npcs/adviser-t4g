﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Commands;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Client.DataModel;
    using Adviser.Client.Core.Presentation;
    using Adviser.Client.Core.Events;
    using Adviser.Client.Core.Services;

    public class VisitDetailsPrintViewModel : ViewModelBase, IVisitDetailsPrintViewModel
    {
        public VisitDetailsPrintViewModel(AppointmentSession session)
        {
            if (session.Visit != null)
            {
                if (!session.Visit.Symptoms.IsLoaded)
                {
                    session.Visit.Symptoms.Load();
                }


                this.Symptoms = new ObservableCollection<string>();

                foreach (Symptom s in session.Visit.Symptoms)
                {
                    this.Symptoms.Add(s.Name);
                }

                if (!session.Visit.OrganExaminations.IsLoaded)
                {
                    session.Visit.OrganExaminations.Load();
                }

                _notes = session.Visit.Notes;

                this.OrganExaminations = new ObservableCollection<string>();

                string organExaminationString = "";
                foreach (OrganExamination o in session.Visit.OrganExaminations)
                {
                    organExaminationString += o.ChronicDisease ? " Chronic Disease." : "";
                    organExaminationString += o.Surgery ? " Surgery." : "";
                    organExaminationString += o.Removed ? " Removed." : "";
                    organExaminationString += o.Implant ? " Implant." : "";
                    if (organExaminationString.Length > 0)
                    {
                        this.OrganExaminations.Add(String.Concat(o.Organ.Name, ":", organExaminationString));
                    }
                    organExaminationString = "";

                }
            }
            else
            {
                throw new ApplicationException("VisitDetailsPrintViewModel ApplicationException. Session Visit is null.");
            }
        }

        #region IVisitDetailsPrintViewModel Members
        private string _notes;
        private ObservableCollection<string> _symptoms;
        public ObservableCollection<string> Symptoms
        {
            get
            {
                return _symptoms;
            }
            private set
            {
                _symptoms = value;
            }

        }
        private ObservableCollection<string> _OrganExaminations;
        public ObservableCollection<string> OrganExaminations
        {
            get { return _OrganExaminations; }
            private set { _OrganExaminations = value; }
        }

        public string Notes
        {
            get { return _notes; }
            private set
            {
                _notes = value;
                RaisePropertyChangedEvent("Notes");
            }
        }

        #endregion
    }
}
