﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Common.DataModel
{
    [Serializable()]
    public class BodyAssessment
    {
        private string assessment;
        private string recommendations;
        private List<ZoneAssessment> zones;

        public BodyAssessment()
        {
            zones = new List<ZoneAssessment>();
        }

        public string Assessment
        {
            get { return assessment; }
            set { assessment = value; }
        }

        public string Recommendations
        {
            get { return recommendations; }
            set { recommendations = value; }
        }

        public List<ZoneAssessment> Zones
        {
            get { return zones; }
        }
    }
}
