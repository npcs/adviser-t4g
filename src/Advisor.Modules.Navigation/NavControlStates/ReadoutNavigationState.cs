﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Events;

using Adviser.Common;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Presentation;

namespace Advisor.Modules.Navigation
{
    public class ReadoutNavigationState : NavigationState
    {
        private IEventAggregator _eventAggregator;
        private AppointmentSession _session;

        public ReadoutNavigationState(IEventAggregator eventAggregator, AppointmentSession session)
        {
            _eventAggregator = eventAggregator;
            _session = session;
        }
    }
}
