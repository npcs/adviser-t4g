﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adviser.Common.Domain;

namespace Adviser.Client.Core.DeviceControl
{
    public class DeviceEmulator : IDeviceDriver
    {
        private Random _rand = new Random(DateTime.Now.Millisecond);
        private DeviceSettings _settings;

        #region IDeviceDriver Members

        public bool ConnectElectrodes(byte front, byte back)
        {
            return true;
        }

        public bool SetPolarity(bool frontPositive)
        {
            return true;
        }

        public bool SetupDevice(DeviceSettings settings)
        {
            _settings = settings;
            return true;
        }

        public ushort ReadData()
        {
            //Random rand = new Random(DateTime.Now.Millisecond);
            return Convert.ToUInt16(_rand.Next((int)_settings.MeasurementMeanNoConnectionValue, (int)_settings.MeasurementMeanShortValue));     
            //return Convert.ToUInt16(_rand.Next(0, (int)_settings.MeasurementMeanShortValue));     
            //return 90;
        }

        public ushort ReadBusVoltage()
        {
            return 3300;
        }

        public bool GetDeviceGUID(ref Guid DeviceGUID)
        {
            DeviceGUID = System.Guid.Empty; //new Guid("ca775b25-f989-4944-a09d-24dde7a65007");
            return true;
        }

        public bool GetMainBoardVersion(out byte[] boardVersion, ref string errorMessage)
        {
            bool returnValue = false;
            boardVersion = new byte[16];
            return returnValue;
        }

        public bool GetBridgeVersion(ref string BridgeVersion)
        {
            return true;
        }

        public bool IsConnected
        {
            get { return true; }
        }

        public void Release()
        {
        }


        public void ReInitialize()
        {
            
        }

        #endregion
    }
}
