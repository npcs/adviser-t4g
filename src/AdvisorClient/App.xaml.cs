﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Collections;
using System.Windows;
using System.Windows.Threading;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using Adviser.Client.Core.Services;
using Adviser.Common.Logging;
//using Adviser.Client.DataModel;

namespace AdvisorClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            Bootstrapper bootStrapper = new AdvisorClient.Bootstrapper();
            bootStrapper.Run();
            this.DispatcherUnhandledException += new System.Windows.Threading.DispatcherUnhandledExceptionEventHandler(App_DispatcherUnhandledException);

        }

        private void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            //TODO: log this exception!
            System.Diagnostics.Trace.WriteLine(e.Exception.ToString());
            string userMessage = "";
#if DEBUG
            userMessage = e.Exception.ToString();
#else
            userMessage = e.Exception.Message;
#endif
            MessageBox.Show("Error occured in the Adviser application:\n" + userMessage 
                + "\nPlease contact Adviser support.");

            //try
            //{
            //    AdviserDBEntities dbContext = new AdviserDBEntities();

            //        Error error = new Error();
            //        AdviserLogEntry entry = new AdviserLogEntry();
            //        entry.Application = "AdviserClient";
            //        entry.Category = "Exception";
            //        entry.Details = e.Exception;
            //        entry.ErrorMessage = "Unhandled exception in AdviserClient.";
            //        entry.LogItemType = "";
            //        entry.Severity = LogSeverity.Error;
            //        entry.Time = DateTime.Now.ToUniversalTime();

            //        BinaryFormatter fmt = new BinaryFormatter();
            //        using (MemoryStream stream = new MemoryStream())
            //        {
            //            fmt.Serialize(stream, entry);
            //            error.Data = stream.ToArray();
            //        }
            //        error.Time = DateTime.Now.ToUniversalTime();

            //        dbContext.ErrorSet.Add(error);
            //}
            //catch
            //{
            //    MessageBox.Show("Unable to send error information to the Adviser Service support.\n Please contact support for further assistance.");
            //}
        }
        /// <summary>
        /// http://social.msdn.microsoft.com/Forums/en-US/wpf/thread/b97a5f83-5394-430e-9a78-9d3a957e3537/
        /// Entries in a resource dictionary are lazily hydrated. 
        /// To do this, the ResourceDictionary hands out a 
        /// DeferredReference to refer to an item in the dictionary. 
        /// When that item is hydrated that deferred reference raises an event - Inflated. 
        /// The problem is that the ResourceReferenceExpression is hooked into that event and never unhooks 
        /// (or maybe its should use a weakeventlistener to listen for the events). 
        /// In any case, you can get around this by forcing the items in the dictionary 
        /// to be hydrated before anyone tries to reference this. One way to do this is to just iterate the resources in 
        /// the OnStartup of the Application. e.g.
        /// </summary>
        /// <param name="e"></param>
        //protected override void OnStartup(StartupEventArgs e)
        //{
        //    Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Loaded,
        // (DispatcherOperationCallback)delegate { CloseSplashScreenWithoutFade(); return null; },
        // this);

        //    WalkDictionary(this.Resources);

        //    base.OnStartup(e);


        //}

        //private void CloseSplashScreen()
        //{
        //    // signal the native process (that launched us) to close the splash screen
        //    using (var closeSplashEvent = new EventWaitHandle(false,
        //        EventResetMode.ManualReset, "CloseSplashScreenEventSplashScreenStarter"))
        //    {
        //        closeSplashEvent.Set();
        //    }
        //}

        //static protected void CloseSplashScreenWithoutFade()
        //{
        //    // signal the native process (that launched us) to close the splash screen
        //    using (var closeSplashEvent = new EventWaitHandle(false,
        //        EventResetMode.ManualReset, "CloseSplashScreenWithoutFadeEventSplashScreenStarter"))
        //    {
        //        closeSplashEvent.Set();
        //    }
        //}
        //private static void WalkDictionary(ResourceDictionary resources)
        //{
        //    foreach (DictionaryEntry entry in resources)
        //    {
        //    }

        //    foreach (ResourceDictionary rd in resources.MergedDictionaries)
        //        WalkDictionary(rd);
        //}
    }
}
