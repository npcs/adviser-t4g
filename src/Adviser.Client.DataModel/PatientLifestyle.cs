﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Client.DataModel
{
    enum PatientLifestyle
    {
        Active,
        Moderate,
        Sedentary
    }
}
