﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;

using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;

namespace Adviser.Modules.PatientDatabase
{
    public interface IVisitsPresenter
    {
        IVisitsView View { get; set; }
    }

    /// <summary>
    /// Presenter for Visits view (Composite Master-Details view)
    /// </summary>
    public class VisitsPresenter : IVisitsPresenter
    {
        private IVisitDetailsViewModel _detailsModel;
        private EventAggregator _eventAggregator;


        public VisitsPresenter(
            IVisitsView view, 
            IVisitListView listView, 
            IVisitDetailsView detailsView,
            IVisitListViewModel listModel,
            IVisitDetailsViewModel detailsModel,
            EventAggregator eventAggregator)
        {
            View = view;
            _detailsModel = detailsModel;
            _eventAggregator = eventAggregator;

            //- set master view on the left panel
            listView.Model = listModel;
            View.SetLeftView(listView);
            listView.VisitSelected += new EventHandler<DataEventArgs<Visit>>(listModel.OnVisitSelected);

            //- set details view on the right panel
            detailsView.Model = detailsModel;
            View.SetRightView(detailsView);

            _eventAggregator.GetEvent<NavigationEvent>().Subscribe(OnNavigationEvent, true);
        }

        public IVisitsView View
        {
            get;
            set;
        }

        /// <summary>
        /// Updates view models when Visits view is shown.
        /// </summary>
        /// <param name="e"></param>
        private void OnNavigationEvent(NavigationEventArgs e)
        {
            if (e.EventName == NavigationEventNames.Visits)
            {
                _detailsModel.Refresh();
            }
        }

    }
}
