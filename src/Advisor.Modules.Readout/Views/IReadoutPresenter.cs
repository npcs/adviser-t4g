﻿using System;
using System.Collections.Generic;
using Advisor.Modules.Readout.PresentationModels;

namespace Advisor.Modules.Readout.Views
{
    public interface IReadoutPresenter
    {
        IReadoutView View { get; }
    }
}
