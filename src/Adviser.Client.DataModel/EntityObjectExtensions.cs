﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Diagnostics;

namespace Adviser.Client.DataModel
{
    public static class EntityObjectExtensions
    {
        public static void TrackChange(this EntityObject entity)
        {
            Debug.WriteLine(entity.EntityKey.EntityKeyValues.ToArray()[0].Value);
            Debug.WriteLine(entity.EntityState);
            Debug.WriteLine(entity.EntityKey.EntitySetName);
        }
    }
}
