﻿using System;
using System.Collections.Generic;

namespace Adviser.Common.Logging
{
    [Serializable]
    public class AdviserLogEntry : LogEntry
    {
        public int Id;
        public Guid? DeviceId;
        public Guid? PractitionerId;
        public Guid? VisitId;
    }
}
