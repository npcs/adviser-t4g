﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Modules.DeviceControl.Views
{
    public interface IDeviceControlPresenter
    {
        IDeviceControlView View { get; set; }
    }

    public class DeviceControlPresenter : IDeviceControlPresenter
    {
        public DeviceControlPresenter(IDeviceControlView view, IDeviceControlPresentationModel model)
        {
            View = view;
            View.Model = model;
        }

        public IDeviceControlView View
        {
            get;
            set;
        }
    }
}
