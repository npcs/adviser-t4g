﻿using System;
using System.Collections.Generic;

namespace Adviser.Modules.PatientDatabase
{
    public interface IPatientsPresenter
    {
        IPatientsView View { get; set; }
    }

    public class PatientsPresenter : IPatientsPresenter
    {
        //public PatientsPresenter(IPatientsView view, IPatientListPresenter listPresenter, IPatientDetailsPresenter detailsPresenter)
        //{
        //    View = view;
        //    View.SetLeftView(listPresenter.View);
        //    View.SetRightView(detailsPresenter.View);
        //}

        //public PatientsPresenter(IPatientListView patientListView,
        //    IPatientDetailsView patientDetailsView,
        //    IDBService service,
        //    IEventAggregator eventAggregator,
        //    IUnityContainer container,
        //    PatientDetailsPrintView printableView)
        //{
        //}

        public PatientsPresenter(IPatientsView view, IPatientsPresentationModel model)
        {
            View = view;
            View.SetLeftView(model.ListView);
            View.SetRightView(model.DetailsView);
        }

        public IPatientsView View { get; set; }
    }
}
