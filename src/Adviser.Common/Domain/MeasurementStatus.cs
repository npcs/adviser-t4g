﻿
namespace Adviser.Common.Domain
{
    /// <summary>
    /// Status of the measurement job.
    /// </summary>
    public enum MeasurementStatus
    {
        NotAvailable,
        InProgress,
        Completed,
        Failed
    }
}
