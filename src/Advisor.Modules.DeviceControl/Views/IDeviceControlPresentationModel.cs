﻿using System;
using Microsoft.Practices.Composite.Wpf.Commands;

namespace Advisor.Modules.DeviceControl.Views
{
    public interface IDeviceControlPresentationModel
    {
        IDeviceControlView View { get; }
        decimal TotalProgress { get; }
        decimal PairProgress { get; }
        string DeviceStatusMessage { get; }
        string BatteryMessage { get; }
        DelegateCommand<string> ConnectToTheDeviceCommand { get; }
        DelegateCommand<string> StartCommand { get; }
        DelegateCommand<string> StopCommand { get; }
    }
}
