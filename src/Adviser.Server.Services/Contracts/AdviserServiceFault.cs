﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace Adviser.Server.Services.Contracts
{
    [DataContract]
    public class AdviserServiceFault
    {
        public AdviserServiceFault(int code, string message)
        {
            FaultCode = code;
            Message = message;
        }

        [DataMember]
        public int FaultCode;

        [DataMember]
        public string Message;
    }
}
