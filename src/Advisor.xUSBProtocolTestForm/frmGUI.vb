﻿
Public Class frmGUI
    Private myConnector As xUSBProtocol.xUSBProtocol
    Private _isUSBEventTimerActivated As Boolean



    Public UserSelectedController As String
    Public DCTable As DataTable 'channel 0-15
    Public EnableTable As DataTable 'channel 0-15
    Private DataViewDC As DataView
    Private DataViewEN As DataView
    Private TableStyleDC As DataGridTableStyle
    Private TableStyleEN As DataGridTableStyle

    Private Sub frmGUI_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        'set up the form

        'avaialble images
        Me.lstAvailableImages.SelectedIndex = 0


        'load I2C combos ***********************************
        'bus speed
        Me.cboI2CBusSpeed.SelectedIndex = 0


        'hardware address
        Dim i As Integer
        For i = 0 To 127
            Me.cboI2CHardwareAddress.Items.Add(Format(i, "X"))
        Next
        Me.cboI2CHardwareAddress.SelectedIndex = &H38

        'datatosend
        For i = 0 To 255
            Me.cboI2CDataToSend.Items.Add(Format(i, "X"))
        Next
        Me.cboI2CDataToSend.SelectedIndex = 0

        'register address
        For i = 0 To 127
            Me.cboI2CRegisterAddress.Items.Add(i)
        Next
        Me.cboI2CRegisterAddress.SelectedIndex = 0
        '********************************************************

        'load EasyScale combos ***********************************

        'easyscale speed
        Me.cboEasyScaleBusSpeed.SelectedIndex = 0

        'hardware address
        For i = 0 To 127
            Me.cboEasyScaleHardwareAddress.Items.Add(Format(i, "X"))
        Next
        Me.cboEasyScaleHardwareAddress.SelectedIndex = &H4E

        'datatosend
        For i = 0 To 31
            Me.cboEasyScaleDataToSend.Items.Add(Format(i, "X"))
        Next
        Me.cboEasyScaleDataToSend.SelectedIndex = 0

        'register address
        For i = 0 To 3
            Me.cboEasyScaleRegisterAddress.Items.Add(i)
        Next
        Me.cboEasyScaleRegisterAddress.SelectedIndex = 0
        '********************************************************


        'load dimming combos ************************************
        'num Steps default to 1
        Me.cboDimmingNumSteps.SelectedIndex = 0

        'start length
        For i = 90 To 200
            Me.cboDimmingStart.Items.Add(i)
        Next
        Me.cboDimmingStart.SelectedIndex = 50 'default to 150

        'pulse length
        For i = 0 To 1000
            Me.cboDimmingPulse.Items.Add(i)
        Next
        Me.cboDimmingPulse.SelectedIndex = 48 'default to 50 'for turning the device up a notch

        'Delay length
        For i = 0 To 1000
            Me.cboDimmingDelay.Items.Add(i)
        Next
        Me.cboDimmingDelay.SelectedIndex = 48 'default to 50

        'Off length
        For i = 500 To 1000
            Me.cboDimmingOff.Items.Add(i)
        Next
        Me.cboDimmingOff.SelectedIndex = 250 'default to 50

        '********************************************************

        'TLC5924 data

        'Setup and Format Grid Controls
        Me.SetupAndFormatGrids()

        'set tooltips on the grids
        ToolTip1.SetToolTip(Me.gridViewDotCorrection, "Dot Correction Values: 0 to 127")
        ToolTip1.SetToolTip(Me.gridviewEnable, "Enable Values: true or false")

        'go ahead and write the data
        'set the banks to A on B off default
        'Me.chkBankBEnabled.Checked = False
        'Me.chkBankAEnabled.Checked = True

        ' Me.cmdTLC5924WriteDotCorrectionAndEnable_Click(Me.cmdTLC5924WriteDotCorrectionAndEnable, New System.EventArgs())

        '******************************************************




        'connect to the usb device
        Me.myConnector = New xUSBProtocol.xUSBProtocol
        'upate the version label
        Me.lblVersion.Text = GetVersion()

        'set the I2C tab first
        Me.TabControl1.SelectedIndex = 0

        'setup pull ups

        Me.rbRes22K_10.Checked = True
        Me.rbRes22K_9.Checked = True
        Me.rbRes22K_8.Checked = True

    End Sub

    Private Sub frmGUI_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



    End Sub

#Region "I2C Functions Features"
    Private Sub cmdI2CWriteData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdI2CWriteData.Click
        Dim Status As xUSBProtocol.xUSBProtocol.Status
        'set bus speed
        Me.I2CsetBusSpeed()

        'get the device address
        Dim devAddr As Byte
        devAddr = Me.cboI2CHardwareAddress.SelectedIndex

        'get the reg address
        Dim RegAddr As Byte
        RegAddr = Me.cboI2CRegisterAddress.SelectedIndex

        'get the data to write
        Dim DataByte As Byte
        DataByte = Me.cboI2CDataToSend.SelectedIndex


        'write the I2C data
        Status = Me.myConnector.i2cWriteByteToRegister(devAddr, RegAddr, DataByte)

        If Status = xUSBProtocol.xUSBProtocol.Status.success Then
            Me.ConsoleWriteLine("I2C Write successful, wrote data " & Format(Me.cboI2CDataToSend.SelectedIndex, "X") & "h" & _
            " To HardAddr " & Format(Me.cboI2CHardwareAddress.SelectedIndex, "X") & _
                "h, RegAddr " & Me.cboI2CRegisterAddress.SelectedIndex & ", Speed " & Me.cboI2CBusSpeed.SelectedItem)
        Else
            Me.ConsoleWriteLine("I2C Write Failed")

        End If


    End Sub
    Private Sub cmdI2CReadData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdI2CReadData.Click
        Dim Status As xUSBProtocol.xUSBProtocol.Status

        'set the bus speed
        Me.I2CsetBusSpeed()

        'get the device address
        Dim devAddr As Byte
        devAddr = Me.cboI2CHardwareAddress.SelectedIndex

        'get the reg address
        Dim RegAddr As Byte
        RegAddr = Me.cboI2CRegisterAddress.SelectedIndex

        'data variable
        Dim DataByte As Byte

        'read I2C data from the end device
        Status = Me.myConnector.i2cReadByteFromRegister(devAddr, RegAddr, DataByte)

        If Status = xUSBProtocol.xUSBProtocol.Status.success Then
            Me.ConsoleWriteLine("I2C Read successful, read data " & Format(DataByte, "X") & "h" & _
            " From HardAddr " & Format(Me.cboI2CHardwareAddress.SelectedIndex, "X") & _
                "h, RegAddr " & Me.cboI2CRegisterAddress.SelectedIndex & ", Speed " & Me.cboI2CBusSpeed.SelectedItem)
            'update the read label
            Me.lblI2CReadValue.Text = Format(DataByte, "X") & "h"
        Else
            Me.ConsoleWriteLine("I2C Read Failed")
            'update the read label
            Me.lblI2CReadValue.Text = "error"
        End If

    End Sub
    Private Sub I2CsetBusSpeed()
        'set the bus speed
        Me.myConnector.setBusSpeed(Me.cboI2CBusSpeed.SelectedIndex)
    End Sub
#End Region

#Region "EasyScale Functions Features"
    Private Sub chkEasyScaleVersion2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEasyScaleVersion2.CheckedChanged
        Dim Version2ck As CheckBox
        Version2ck = sender
        If Version2ck.Checked = True Then
            Me.chkEasyScaleLineHigh.Enabled = True
            Me.chkEasyScaleRequestAck.Enabled = True
        Else
            Me.chkEasyScaleLineHigh.Enabled = False
            Me.chkEasyScaleRequestAck.Enabled = False
        End If
    End Sub
    Private Sub cmdEasyScaleWriteData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEasyScaleWriteData.Click
        Dim Status As xUSBProtocol.xUSBProtocol.Status

        Dim DevAddr As Byte
        DevAddr = Me.cboEasyScaleHardwareAddress.SelectedIndex

        Dim DataForReg As Byte
        DataForReg = Me.cboEasyScaleDataToSend.SelectedIndex

        Dim RegAddr As Byte
        RegAddr = Me.cboEasyScaleRegisterAddress.SelectedIndex
        'shift left by 5
        RegAddr = RegAddr << 5

        Dim RequestAck As Byte
        If Me.chkEasyScaleRequestAck.Checked = True Then
            RequestAck = 128
        Else
            RequestAck = 0
        End If

        Dim ReturnedAck As Boolean

        Dim DataByte As Byte
        'and them together
        DataByte = RequestAck Or RegAddr Or DataForReg

        'check the version
        If Me.chkEasyScaleVersion2.Checked = False Then
            Status = Me.myConnector.easyScale_WriteByteToRegister(DevAddr, DataByte, Me.cboEasyScaleBusSpeed.SelectedIndex, ReturnedAck)
        Else
            Status = Me.myConnector.easyScale_WriteByteToRegister_V2(DevAddr, DataByte, Me.cboEasyScaleBusSpeed.SelectedIndex, _
                Me.chkEasyScaleRequestAck.Checked, Me.chkEasyScaleLineHigh.Checked, ReturnedAck)
        End If

        If Status = xUSBProtocol.xUSBProtocol.Status.success Then
            'write to the console
            If ReturnedAck = False And Me.chkEasyScaleRequestAck.Checked = True Then
                'error from the part
                'requested ack but didn't get one
                Me.ConsoleWriteLine("EasyScale Write Failed, Requested Ack = true but no Ack Received")
            Else
                'worked
                Me.ConsoleWriteLine("EasyScale Write Successful, RequestAck " & Me.chkEasyScaleRequestAck.Checked & _
                    ", RegAddr " & Me.cboEasyScaleRegisterAddress.SelectedIndex & ", Data " & DataForReg & _
                    ", to DevAddr " & Format(DevAddr, "X") & ", at " & Me.cboEasyScaleBusSpeed.SelectedItem)
            End If

            If ReturnedAck = True Then
                Me.lblEasyScaleFeedBack.Text = "Ack Received"
            Else
                Me.lblEasyScaleFeedBack.Text = "No Ack Received"
            End If
        Else
            Me.ConsoleWriteLine("EasyScale Write Failed")
            Me.lblEasyScaleFeedBack.Text = "Error"

        End If
    End Sub
#End Region

#Region "Digital Dimming Function Features"
    Private Sub cmdDimmingWrite_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDimmingWrite.Click
        Dim Status As xUSBProtocol.xUSBProtocol.Status

        'get the data
        Dim StartDelay As Int16
        Dim PulseDelay As Int16
        Dim DelayBetween As Int16
        Dim DelayOff As Int16
        Dim NumPulses As Byte

        'num pulses
        NumPulses = CInt(Me.cboDimmingNumSteps.SelectedItem)

        'start delay
        StartDelay = CInt(Me.cboDimmingStart.SelectedItem)

        'pulse delay
        PulseDelay = CInt(Me.cboDimmingPulse.SelectedItem)

        'delay between
        DelayBetween = CInt(Me.cboDimmingDelay.SelectedItem)

        'delay off
        DelayOff = CInt(Me.cboDimmingOff.SelectedItem)


        Status = Me.myConnector.dimming_SendPulses(NumPulses, StartDelay, PulseDelay, DelayBetween, DelayOff)


        If Status = xUSBProtocol.xUSBProtocol.Status.success Then
            Me.ConsoleWriteLine("Dimming Write Successful, Wrote " & NumPulses & " Pulses")
        Else
            'failed
            Dim myStr() As String
            myStr = [Enum].GetNames(GetType(xUSBProtocol.xUSBProtocol.Status))

            Me.ConsoleWriteLine("Dimming Write Failed: " & myStr(Status))
        End If

    End Sub
    Private Sub chkDimmingEnable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDimmingEnable.CheckedChanged
        Dim Status As xUSBProtocol.xUSBProtocol.Status
        Dim chk As CheckBox
        Dim Enable As Boolean
        chk = sender

        If chk.Checked Then
            'disabled
            Me.chkDimmingEnable.Text = "Disabled"
            Enable = False
        Else
            'enabled
            Me.chkDimmingEnable.Text = "Enabled"
            Enable = True
        End If

        Status = Me.myConnector.dimming_Enable(Enable)

        If Status = xUSBProtocol.xUSBProtocol.Status.success Then
            If Enable Then
                Me.ConsoleWriteLine("Dimming Enabled Successful")
            Else
                Me.ConsoleWriteLine("Dimming Disabled Successful")
            End If
        Else
            If Enable Then
                Me.ConsoleWriteLine("Dimming Enabled Failed")
            Else
                Me.ConsoleWriteLine("Dimming Disabled Failed")
            End If
        End If
    End Sub

#End Region


#Region "TLC5924"

    Private Sub SetupAndFormatGrids()
        'create  tables
        Me.DCTable = New DataTable("DC") 'the names given here must be used later to 
        Me.EnableTable = New DataTable("EN") 'the names given here must be used later to 

        'add the columns to the table
        Me.AddColumnsToDCTable(Me.DCTable)
        Me.AddColumnsToENTable(Me.EnableTable)

        'create the Dataviews - this code lets us change the view of the grid
        Me.DataViewDC = New DataView(Me.DCTable)
        Me.DataViewEN = New DataView(Me.EnableTable)

        'configure the dataviews
        Me.DataViewDC.AllowNew = False
        Me.DataViewDC.AllowDelete = False
        Me.DataViewEN.AllowNew = False
        Me.DataViewEN.AllowDelete = False

        'set the datasources
        Me.gridViewDotCorrection.DataSource = Me.DataViewDC
        Me.gridviewEnable.DataSource = Me.DataViewEN

        'create the table styles
        Me.TableStyleDC = New DataGridTableStyle
        Me.TableStyleEN = New DataGridTableStyle

        'load the data
        Load_Default_Data_in_Tables()

    End Sub

    Private Sub AddColumnsToDCTable(ByRef Table As DataTable)
        'add the columns
        Table.Columns.Add(New DataColumn("0", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("1", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("2", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("3", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("4", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("5", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("6", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("7", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("8", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("9", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("10", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("11", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("12", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("13", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("14", Type.GetType("System.Byte")))
        Table.Columns.Add(New DataColumn("15", Type.GetType("System.Byte")))



    End Sub

    Private Sub AddColumnsToENTable(ByRef Table As DataTable)
        'add the columns
        Table.Columns.Add(New DataColumn("0", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("1", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("2", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("3", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("4", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("5", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("6", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("7", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("8", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("9", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("10", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("11", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("12", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("13", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("14", Type.GetType("System.Boolean")))
        Table.Columns.Add(New DataColumn("15", Type.GetType("System.Boolean")))



    End Sub

    Public Sub Load_Default_Data_in_Tables()

        'add the bytes to the table
        Me.DCTable.Rows.Add(New Object() {127, 127, 127, 127, 127, 127, 127, 127, _
                                          127, 127, 127, 127, 127, 127, 127, 127})

        'add the bytes to the table
        Me.EnableTable.Rows.Add(New Object() {True, True, True, True, True, True, True, True, _
                                          True, True, True, True, True, True, True, True})

    End Sub

    Private Sub cmdTLC5924WriteDotCorrectionAndEnable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTLC5924WriteDotCorrectionAndEnable.Click


        Dim DotCorrection(14) As Byte
        Dim Enable(2) As Byte

        'For i As Integer = 0 To 13
        '    DotCorrection(i) = Byte.Parse(Me.DCTable.Rows(0).Item(i).ToString)
        'Next
        DotCorrection = Convert16BytesTo14DC()

        Enable = Convert16BytesTo2Enable()

        'print out the conversion
        'Me.ConsoleWriteLine("Frame DotCorrection translated to: ")

        'Dim s As String
        'For i As Int16 = 0 To 13
        '    s = s & DotCorrection(i).ToString & ","
        'Next
        'Me.ConsoleWriteLine(s)

        'Me.ConsoleWriteLine("Enable Data translated to: " & Enable(0) & "," & Enable(1))


        Dim Status As xUSBProtocol.xUSBProtocol.Status

        Status = Me.myConnector.TLC5924_WriteFrameEnableAndDotCorrection(Enable, DotCorrection)


        If Status = xUSBProtocol.xUSBProtocol.Status.success Then
            Me.ConsoleWriteLine("Frame DotCorrection and Enable Data Written Successfully")
        Else
            'failed
            Dim myStr() As String
            myStr = [Enum].GetNames(GetType(xUSBProtocol.xUSBProtocol.Status))

            Me.ConsoleWriteLine("Frame DotCorrection and Enable Write Failed: " & myStr(Status))
        End If

    End Sub


    Private Sub cmdTLC5924WriteDotCorrection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTLC5924WriteDotCorrection.Click


        Dim DotCorrection(14) As Byte
        DotCorrection = Convert16BytesTo14DC()

        Dim Status As xUSBProtocol.xUSBProtocol.Status

        Status = Me.myConnector.TLC5924_WriteFrameDotCorrection(DotCorrection)


        If Status = xUSBProtocol.xUSBProtocol.Status.success Then
            Me.ConsoleWriteLine("Frame DotCorrection Data Written Successfully")
        Else
            'failed
            Dim myStr() As String
            myStr = [Enum].GetNames(GetType(xUSBProtocol.xUSBProtocol.Status))

            Me.ConsoleWriteLine("Frame DotCorrection Write Failed: " & myStr(Status))
        End If

    End Sub


    Private Sub cmdTLC5924WriteEnable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTLC5924WriteEnable.Click

        Dim Enable(2) As Byte
        Enable = Convert16BytesTo2Enable()

        Dim Status As xUSBProtocol.xUSBProtocol.Status

        Status = Me.myConnector.TLC5924_WriteFrameEnable(Enable)


        If Status = xUSBProtocol.xUSBProtocol.Status.success Then
            Me.ConsoleWriteLine("Frame Enable Data Written Successfully")
        Else
            'failed
            Dim myStr() As String
            myStr = [Enum].GetNames(GetType(xUSBProtocol.xUSBProtocol.Status))

            Me.ConsoleWriteLine("Frame Enable Write Failed: " & myStr(Status))
        End If



    End Sub

    Private Function Convert16BytesTo14DC() As Byte()
        Dim myArray14(14) As Byte
        Dim myArray16(16) As Byte

        'get data from datagrid an flip it so msb is first
        For i As Integer = 0 To 15
            myArray16(15 - i) = Byte.Parse(Me.DCTable.Rows(0).Item(i).ToString)
        Next

        'go from Channel 15 bit 6 is the Most Significant Byte and should be first

        'Every 7 bytes we are back at the beginning
        Dim Index16 As Int16 = 0
        Dim RelFirstByte As Byte
        Dim RelSecondByte As Byte

        'first7 bytes of the 14 byte array
        For index14 As Integer = 0 To 6

            'get 7 - index14 bits from relative first byte in Array16
            RelFirstByte = myArray16(Index16)
            'shift over
            RelFirstByte = RelFirstByte << (index14 + 1)

            'get index14  + 1 bits from relative second byte in Array 16
            RelSecondByte = myArray16(Index16 + 1)
            'shift over
            RelSecondByte = RelSecondByte >> (6 - index14)

            'or them together to for the byte14 number 
            myArray14(index14) = RelFirstByte Or RelSecondByte

            'increment index16
            Index16 += 1

        Next

        Index16 = 8
        'second set of 7 bytes of the 14 byte array
        For index14 As Integer = 0 To 6

            'get 7 - index14 bits from relative first byte in Array16
            RelFirstByte = myArray16(Index16)
            'shift over
            RelFirstByte = RelFirstByte << (index14 + 1)

            'get index14  + 1 bits from relative second byte in Array 16
            RelSecondByte = myArray16(Index16 + 1)
            'shift over
            RelSecondByte = RelSecondByte >> (6 - index14)

            'or them together to for the byte14 number 
            myArray14(index14 + 7) = RelFirstByte Or RelSecondByte

            'increment index16
            Index16 += 1

        Next

        Return myArray14

    End Function

    Private Function Convert16BytesTo2Enable() As Byte()

        Dim myArray2(2) As Byte
        Dim myArray16(16) As Boolean

        'get data from datagrid an flip it so msb is first
        For i As Integer = 0 To 15
            myArray16(15 - i) = Boolean.Parse(Me.EnableTable.Rows(0).Item(i).ToString)
        Next

        'go from Channel 15 bit 6 is the Most Significant Byte and should be first

        'Every byte we are back at the beginning
        Dim Index16 As Int16 = 0

        'first byte of the 2 byte array

        'get first 8 booleans from boolean Array16
        Dim tempByte As Byte
        Dim FinalByte As Byte

        FinalByte = 0
        For Index16 = 0 To 7
            If (myArray16(Index16) = True) Then
                tempByte = 1
            Else
                tempByte = 0
            End If

            'shift over 
            tempByte = tempByte << (7 - Index16)
            FinalByte = FinalByte Or tempByte
        Next
        myArray2(0) = FinalByte


        FinalByte = 0
        For Index16 = 0 To 7
            If (myArray16(Index16 + 8) = True) Then
                tempByte = 1
            Else
                tempByte = 0
            End If

            'shift over 
            tempByte = tempByte << (7 - Index16)
            FinalByte = FinalByte Or tempByte
        Next
        myArray2(1) = FinalByte


        Return myArray2
    End Function


    Private Sub gridViewDotCorrection_CellValidating(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles gridViewDotCorrection.CellValidating

        Dim s As String

        s = e.FormattedValue

        Dim myByteValue As Byte = 127

        Try
            myByteValue = Byte.Parse(s)

        Catch myEx As Exception

            Me.DCTable.Rows(e.RowIndex).Item(e.ColumnIndex) = 0
            Exit Sub

        End Try

        'else able to parse
        If myByteValue > 127 Then
            myByteValue = 127
            Me.DCTable.Rows(e.RowIndex).Item(e.ColumnIndex) = myByteValue
        End If


    End Sub

    Private Sub chkBankAEnabled_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBankAEnabled.CheckedChanged, chkBankBEnabled.CheckedChanged

        If Me.Visible = True Then
            Dim myStatus As xUSBProtocol.xUSBProtocol.Status
            myStatus = Me.myConnector.TLC5294_SetLED_Banks(chkBankAEnabled.Checked, chkBankBEnabled.Checked)

            If myStatus = xUSBProtocol.xUSBProtocol.Status.success Then
                Me.ConsoleWriteLine("Set LED Banks Operation was successfull")
            Else
                Me.ConsoleWriteLine("Set LED Banks Operation failed.")
            End If
        End If
    End Sub
#End Region

#Region "Generic hardware functions"
    Private Sub cmdGetVersion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGetVersion.Click
        Me.lblVersion.Text = GetVersion()
    End Sub
    Public Function GetVersion() As String
        Dim myVersion As New String("")
        Dim status As xUSBProtocol.xUSBProtocol.Status
        status = Me.myConnector.version(myVersion)
        If status = xUSBProtocol.xUSBProtocol.Status.success Then
            Me.ConsoleWriteLine("called 'Version' successfully, recieved " & myVersion)
            Return myVersion
        Else
            Me.ConsoleWriteLine("call to 'Version' failed")
            Return "Error Reading From Hardware"
        End If
    End Function
    Private Sub cmdProgram_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProgram.Click
        'put the selected image on eeprom

        'get the image
        Dim myImage() As Byte
        If Me.lstAvailableImages.SelectedIndex = 0 Then
            myImage = My.Resources.v0_0_1
        ElseIf Me.lstAvailableImages.SelectedIndex = 1 Then
            myImage = My.Resources.v17_1_0
        ElseIf Me.lstAvailableImages.SelectedIndex = 2 Then
            myImage = My.Resources.v18_1_0
        ElseIf Me.lstAvailableImages.SelectedIndex = 3 Then
            myImage = My.Resources.v18_2_0
        ElseIf Me.lstAvailableImages.SelectedIndex = 4 Then
            myImage = My.Resources.v18_3_1
        ElseIf Me.lstAvailableImages.SelectedIndex = 5 Then
            myImage = My.Resources.v19_1_0
        ElseIf Me.lstAvailableImages.SelectedIndex = 6 Then
            myImage = My.Resources.v20_1_0
        Else
            'no image selected
            Me.ConsoleWriteLine("Write To EEPROM Failed because the target Image was not Found.")
            Exit Sub
        End If

        'put it on the device

        If Me.myConnector.putImageOnEEPROM(myImage) = xUSBProtocol.xUSBProtocol.EEPROM_Status.success Then
            Me.ConsoleWriteLine("Write To EEPROM successful, Image = " & Me.lstAvailableImages.SelectedItem.ToString)
        Else
            Me.ConsoleWriteLine("Write To EEPROM Failed.")
        End If

    End Sub
    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        'Validate the selected image on eeprom

        'get the image
        Dim myImage() As Byte
        If Me.lstAvailableImages.SelectedIndex = 0 Then
            myImage = My.Resources.v0_0_1
        ElseIf Me.lstAvailableImages.SelectedIndex = 1 Then
            myImage = My.Resources.v17_1_0
        ElseIf Me.lstAvailableImages.SelectedIndex = 2 Then
            myImage = My.Resources.v18_1_0
        ElseIf Me.lstAvailableImages.SelectedIndex = 3 Then
            myImage = My.Resources.v18_2_0
        ElseIf Me.lstAvailableImages.SelectedIndex = 4 Then
            myImage = My.Resources.v18_3_1
        ElseIf Me.lstAvailableImages.SelectedIndex = 5 Then
            myImage = My.Resources.v19_1_0
        ElseIf Me.lstAvailableImages.SelectedIndex = 6 Then
            myImage = My.Resources.v20_1_0
        Else
            'no image selected
            Me.ConsoleWriteLine("Validate EEPROM Image Failed because the target Image was not Found.")
            Exit Sub
        End If

        'Validate it on the device
        If Me.myConnector.validateImageOnEEPROM(myImage) = xUSBProtocol.xUSBProtocol.EEPROM_Status.success Then
            Me.ConsoleWriteLine("Validate EEPROM Image successful, Image = " & Me.lstAvailableImages.SelectedItem.ToString)
        Else
            Me.ConsoleWriteLine("Validate EEPROM Image Failed.")
        End If
    End Sub




#End Region

#Region "events related to hot plug usb"

    Private Sub USBEventTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles USBEventTimer.Tick
        Me._isUSBEventTimerActivated = False

        Me.USBEventTimer.Stop()

        OnDeviceChange()
    End Sub

    Protected Overrides Sub WndProc(ByRef m As Message)

        'Purpose    : Overrides WndProc to enable checking for and handling
        '           : WM_DEVICECHANGE(messages)

        'Accepts    : m - a Windows Message  
        Dim WM_DEVICECHANGE As Integer = &H219

        Try
            'The OnDeviceChange routine processes WM_DEVICECHANGE messages.
            If m.Msg = WM_DEVICECHANGE Then
                'Activate the timer
                If Not Me._isUSBEventTimerActivated Then
                    Me._isUSBEventTimerActivated = True
                    Me.USBEventTimer.Interval = 1500
                    Me.USBEventTimer.Start()
                End If
            End If

            'Let the base form process the message.
            MyBase.WndProc(m)

        Catch ex As Exception
            Debug.WriteLine(Me.ToString, ex.ToString)
            MsgBox("Exception was thrown during the WndProc Sub. " & ex.ToString, MsgBoxStyle.Critical)
            'Call HandleException(Me.Name, ex)
        End Try

    End Sub

    Friend Sub OnDeviceChange()
        'Purpose    : Called when a WM_DEVICECHANGE message has arrived,
        '           : indicating that a device has been attached or removed.

        'Accepts    : m - a message with information about the device

        'just see if it's our device
        'find out which image is selected
        Me.myConnector.ReInitialize(My.Resources.v17_1_0, "17.1.0")
        'update the version label
        Me.lblVersion.Text = GetVersion()

    End Sub

#End Region

#Region "Console"

    Public Sub ConsoleWriteLine(ByVal TextStr)
        Me.lstOutputConsole.Items.Add(TextStr)
    End Sub

    Public Sub ConsoleClear()
        Me.lstOutputConsole.Items.Clear()
    End Sub

    Private Sub cmdConsoleClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConsoleClear.Click
        Call ConsoleClear()
    End Sub
#End Region



    Private Sub Res_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbResOpen_10.CheckedChanged, rbRes688_10.CheckedChanged, _
        rbRes22K_10.CheckedChanged, rbRes1K_10.CheckedChanged, rbRes1K_9.CheckedChanged, rbRes22K_9.CheckedChanged, rbRes688_9.CheckedChanged, rbResOpen_9.CheckedChanged, _
        rbRes22K_8.CheckedChanged, rbResOpen_8.CheckedChanged


        Dim theButton As RadioButton
        theButton = sender
        If theButton.Checked = False Then
            Return 'don't worry about changing except on a postive check changed
        End If


        Dim line9 As xUSBProtocol.xUSBProtocol.ResistorValue
        Dim line10 As xUSBProtocol.xUSBProtocol.ResistorValue
        Dim line8 As xUSBProtocol.xUSBProtocol.ResistorValue

        'pin 10
        If rbResOpen_10.Checked = True Then
            line10 = xUSBProtocol.xUSBProtocol.ResistorValue.openDrain
        ElseIf rbRes688_10.Checked = True Then
            line10 = xUSBProtocol.xUSBProtocol.ResistorValue.ohm668
        ElseIf rbRes1K_10.Checked = True Then
            line10 = xUSBProtocol.xUSBProtocol.ResistorValue.ohm1k
        ElseIf rbRes22K_10.Checked = True Then
            line10 = xUSBProtocol.xUSBProtocol.ResistorValue.ohm22k
        End If

        'pin 9 
        If rbResOpen_9.Checked = True Then
            line9 = xUSBProtocol.xUSBProtocol.ResistorValue.openDrain
        ElseIf rbRes688_9.Checked = True Then
            line9 = xUSBProtocol.xUSBProtocol.ResistorValue.ohm668
        ElseIf rbRes1K_9.Checked = True Then
            line9 = xUSBProtocol.xUSBProtocol.ResistorValue.ohm1k
        ElseIf rbRes22K_9.Checked = True Then
            line9 = xUSBProtocol.xUSBProtocol.ResistorValue.ohm22k
        End If

        'pin 8 
        If rbResOpen_8.Checked = True Then
            line8 = xUSBProtocol.xUSBProtocol.ResistorValue.openDrain
        ElseIf rbRes22K_8.Checked = True Then
            line8 = xUSBProtocol.xUSBProtocol.ResistorValue.ohm22k
        End If

        Dim myStatus As xUSBProtocol.xUSBProtocol.Status

        myStatus = Me.myConnector.setPullUps(line10, line9, line8)

        If myStatus = xUSBProtocol.xUSBProtocol.Status.success Then
            Me.ConsoleWriteLine("Pull Up Change successful, Pullups set to:")
            Me.ConsoleWriteLine( _
            "Pin8 = " & line8.ToString() & _
            ", Pin9 = " & line9.ToString() & _
            ", Pin10 = " & line10.ToString())
        Else
            Me.ConsoleWriteLine("Pull Up Change failed.")
        End If


    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim Status As xUSBProtocol.xUSBProtocol.Status

        'set the bus speed
        Me.I2CsetBusSpeed()

        'get the device address
        Dim devAddr As Byte
        devAddr = Me.cboI2CHardwareAddress.SelectedIndex

        'get the reg address
        Dim RegAddr As Byte
        RegAddr = Me.cboI2CRegisterAddress.SelectedIndex

        'data variable
        Dim DataByte As Byte

        'read I2C data from the end device
        Status = Me.myConnector.i2cReadByteFromRegister(devAddr, RegAddr, DataByte)
        Dim DataBytes(4) As Byte
        DataBytes(0) = 0
        DataBytes(1) = 0
        DataBytes(2) = 0
        DataBytes(3) = 0
        Status = Me.myConnector.i2cRead(devAddr, 0, 2, DataBytes)

        If Status = xUSBProtocol.xUSBProtocol.Status.success Then
            Me.ConsoleWriteLine("I2C Read successful, read data " & Format(DataByte, "X") & "h" & _
            " From HardAddr " & Format(Me.cboI2CHardwareAddress.SelectedIndex, "X") & _
                "h, RegAddr " & Me.cboI2CRegisterAddress.SelectedIndex & ", Speed " & Me.cboI2CBusSpeed.SelectedItem)
            'update the read label
            Me.lblI2CReadValue.Text = Format(DataByte, "X") & "h"
            Dim s As String
            s = ""
            For Each i As Byte In DataBytes
                s = s & " " & i.ToString

            Next
            Me.ConsoleWriteLine("s=" & s)
        Else
            Me.ConsoleWriteLine("I2C Read Failed")
            'update the read label
            Me.lblI2CReadValue.Text = "error"
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Validate it on the device
        Dim data(15) As Byte
        Dim startAddress As Integer

        startAddress = 8160

        If Me.myConnector.readEepromPacketPublic(startAddress, 16, data) = xUSBProtocol.xUSBProtocol.EEPROM_Status.success Then

            Dim GUIDData As System.Guid

            GUIDData = New Guid(data)

            Me.ConsoleWriteLine("readEepromPacket, data = " & GUIDData.ToString())

            For i As Integer = startAddress To startAddress + 15
                Me.ConsoleWriteLine(i & " " & data(i - startAddress))
            Next

        Else
            Me.ConsoleWriteLine("Validate EEPROM Image Failed.")
        End If


        startAddress = 8176

        If Me.myConnector.readEepromPacketPublic(startAddress, 16, data) = xUSBProtocol.xUSBProtocol.EEPROM_Status.success Then

            Dim GUIDData As System.Guid

            GUIDData = New Guid(data)

            Me.ConsoleWriteLine("readEepromPacket, data = " & GUIDData.ToString())

            For i As Integer = startAddress To startAddress + 15
                Me.ConsoleWriteLine(i & " " & data(i - startAddress))
            Next

        Else
            Me.ConsoleWriteLine("Validate EEPROM Image Failed.")
        End If



    End Sub

    Private Sub btnWriteByte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWriteByte.Click
        Dim Status As xUSBProtocol.xUSBProtocol.Status
        'set bus speed
        Me.I2CsetBusSpeed()

        'get the device address
        Dim devAddr As Byte
        devAddr = Me.cboI2CHardwareAddress.SelectedIndex

        'get the reg address
        Dim ByteToWrite(0) As Byte
        ByteToWrite(0) = Convert.ToByte(Me.txtByteToWrite.Text)



        'write the I2C data
        Status = Me.myConnector.i2cWrite(devAddr, Convert.ToByte(Me.myConnector.UsbCommand.i2cWrite), 1, ByteToWrite)
        'Status = Me.myConnector.i2cWriteByte(devAddr, Convert.ToByte(Me.txtByteToWrite.Text))


        If Status = xUSBProtocol.xUSBProtocol.Status.success Then
            Me.ConsoleWriteLine("I2C Write successful, wrote data " & Format(Me.cboI2CDataToSend.SelectedIndex, "X") & "h" & _
            " To HardAddr " & Format(Me.cboI2CHardwareAddress.SelectedIndex, "X") & _
                "h, RegAddr " & Me.cboI2CRegisterAddress.SelectedIndex & ", Speed " & Me.cboI2CBusSpeed.SelectedItem)
        Else
            Me.ConsoleWriteLine("I2C Write Failed")

        End If

    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub btnConnElectr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnElectr.Click
        Dim Status As xUSBProtocol.xUSBProtocol.Status
        'set bus speed
        Me.I2CsetBusSpeed()

        'get the device address
        Dim devAddr As Byte
        devAddr = Me.cboI2CHardwareAddress.SelectedIndex

        'get the reg address
        Dim data As Byte

        data = Convert.ToByte(Convert.ToByte(txtFront.Text) + ((Convert.ToByte(txtBack.Text)) << 3))

        If Not IsFrontPositiveCheckBox.Checked Then
            data = data + 128

        End If



        'write the I2C data
        Status = Me.myConnector.i2cWriteByteToRegister(devAddr, data, data)
        'Status = Me.myConnector.i2cWriteByte(devAddr, Convert.ToByte(Me.txtByteToWrite.Text))


        If Status = xUSBProtocol.xUSBProtocol.Status.success Then
            Me.ConsoleWriteLine("I2C Write successful, wrote data " & Format(Me.cboI2CDataToSend.SelectedIndex, "X") & "h" & _
            " To HardAddr " & Format(Me.cboI2CHardwareAddress.SelectedIndex, "X") & _
                "h, RegAddr " & data & ", Speed " & Me.cboI2CBusSpeed.SelectedItem)
        Else
            Me.ConsoleWriteLine("I2C Write Failed")

        End If

    End Sub

    Private Sub btnWriteToEEPROM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWriteToEEPROM.Click
        'Validate it on the device
        Dim data(15) As Byte

        Dim myGuid = System.Guid.NewGuid()

        Dim GUIDByte() As Byte
        GUIDByte = myGuid.ToByteArray()
        ConsoleWriteLine("GUID = " & GUIDByte.Length)
        For i As Byte = 0 To 15
            data(i) = GUIDByte(i)
        Next

        'For i As Byte = 16 To 31
        '    data(i) = i
        'Next

        If Me.myConnector.writeEepromPacket(8160, 16, data) = xUSBProtocol.xUSBProtocol.EEPROM_Status.success Then
            Me.ConsoleWriteLine("writeEepromPacket, myGuid = " & myGuid.ToString())

            For i As Integer = 0 To 15
                Me.ConsoleWriteLine(i & " " & data(i))
            Next

        Else
            Me.ConsoleWriteLine("Validate EEPROM Image Failed.")
        End If
    End Sub

    Private Sub btnRead2BFromINA209_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRead2BFromINA209.Click
        Dim data(1) As Byte

        If Me.myConnector.i2cRead(Me.cboI2CHardwareAddress.SelectedIndex, Me.cboI2CRegisterAddress.SelectedIndex, 2, data) = xUSBProtocol.xUSBProtocol.EEPROM_Status.success Then
            Me.ConsoleWriteLine("writeEepromPacket, data = " & data.ToString())

            'If Me.myConnector.i2cRead(Me.cboI2CHardwareAddress.SelectedIndex, Me.cboI2CRegisterAddress.SelectedIndex, 2, data) = xUSBProtocol.xUSBProtocol.EEPROM_Status.success Then
            '    Me.ConsoleWriteLine("writeEepromPacket, data = " & data.ToString())

            For i As Integer = 0 To 1
                Me.ConsoleWriteLine(i & " " & Format(data(i), "X"))
            Next

        Else
            Me.ConsoleWriteLine("Validate EEPROM Image Failed.")
        End If
    End Sub

    Private Sub btnWrite2BtoINA209_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWrite2BtoINA209.Click
        Dim data(1) As Byte
        data(0) = Byte.Parse(txtFront.Text, System.Globalization.NumberStyles.HexNumber)
        data(1) = Byte.Parse(txtBack.Text, System.Globalization.NumberStyles.HexNumber)



        If Me.myConnector.i2cWrite(Me.cboI2CHardwareAddress.SelectedIndex, Me.cboI2CRegisterAddress.SelectedIndex, 2, data) = xUSBProtocol.xUSBProtocol.EEPROM_Status.success Then
            Me.ConsoleWriteLine("writeEepromPacket, data = " & data.ToString())

            For i As Integer = 0 To 1
                Me.ConsoleWriteLine(i & " " & Format(data(i), "X"))
            Next

        Else
            Me.ConsoleWriteLine("Validate EEPROM Image Failed.")
        End If
    End Sub
End Class
