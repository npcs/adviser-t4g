﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using Advisor.Common.Presentation;
using Advisor.Common.DataModel;
using Advisor.Modules.Catalog.Views;

namespace Advisor.Modules.Catalog.Controllers
{
    public class CatalogController : ICatalogController
    {
        private IUnityContainer container;
        private IRegionManager regionManager;
        private Dictionary<string, object> _viewsCache;
        private string _currentViewName;

        public CatalogController(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
            _viewsCache = new Dictionary<string, object>();

            //SetPatientListView(null);

            //IRegion catalogRegion = regionManager.Regions[RegionNames.MainRegion];
            //IPatientListPresenter patientListPresenter = container.Resolve<IPatientListPresenter>();
            //catalogRegion.Add(patientListPresenter.View, "PatientListPresenter");
        }

        public void SetPatientListView(Patient patient)
        {
            IPatientListPresenter patientListPresenter = null;
            if (_viewsCache.ContainsKey("PatientList"))
            {
                patientListPresenter = (IPatientListPresenter)_viewsCache["PatientList"];
            }
            else
            {
                patientListPresenter = container.Resolve<IPatientListPresenter>();
                _viewsCache.Add("PatientList", patientListPresenter);
            }
            if (patient != null)
                patientListPresenter.SetSelectedPatient(patient);

            IRegion catalogRegion = regionManager.Regions["CatalogRegion"];
            catalogRegion.Add(patientListPresenter.View, "PatientListView");
            _currentViewName = "PatientListView";
        }

        public void SetPatientDetailsView(Patient patient)
        {
            IRegion catalogRegion = regionManager.Regions["CatalogRegion"];
            object existingView = catalogRegion.GetView(_currentViewName);
            catalogRegion.Remove(existingView);
            

            IPatientDetailsPresenter presenter = container.Resolve<IPatientDetailsPresenter>();
            presenter.SetSelectedPatient(patient);

            catalogRegion.Add(presenter.View, "PatientDetailsView");
            _currentViewName = "PatientDetailsView";
        }

        public virtual void OnPatientSelected(Patient patient)
        {
            IRegion catalogRegion = regionManager.Regions[RegionNames.MainRegion];

            //object existingView = catalogRegion.GetView("PatientListPresenter");
            //catalogRegion.Remove(existingView);      

            IPatientDetailsPresenter presenter = container.Resolve<IPatientDetailsPresenter>();
            presenter.SetSelectedPatient(patient);

            UserControl parentView = (UserControl)catalogRegion.GetView("CatalogView");
            ContentControl contentControl = (ContentControl)parentView.FindName("CatalogContent");
            contentControl.Content = presenter.View;

            //if (existingView == null)
            //{
            //    IProjectsListPresenter projectsListPresenter = this.container.Resolve<IProjectsListPresenter>();
            //    projectsListPresenter.SetProjects(employee.EmployeeId);

            //    IEmployeesDetailsPresenter detailsPresenter = this.container.Resolve<IEmployeesDetailsPresenter>();
            //    detailsPresenter.SetSelectedEmployee(employee);

            //    IRegionManager detailsRegionManager = detailsRegion.Add(detailsPresenter.View, employee.EmployeeId.ToString(CultureInfo.InvariantCulture), true);
            //    IRegion region = detailsRegionManager.Regions[RegionNames.TabRegion];
            //    region.Add(projectsListPresenter.View, "CurrentProjectsView");
            //    detailsRegion.Activate(detailsPresenter.View);
            //}
            //else
            //{
            //    detailsRegion.Activate(existingView);
            //}
        }
    }
}
