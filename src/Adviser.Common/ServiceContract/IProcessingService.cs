﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Adviser.Common.ServiceContract
{
    /// <summary>
    /// Service contract for processing device measurement data and keeping client-server records in synch
    /// </summary>
    [ServiceContract]
    public interface IProcessingService
    {
        [OperationContract]
        void SynchronizeData();

        [OperationContract]
        void GetResults();
    }
}
