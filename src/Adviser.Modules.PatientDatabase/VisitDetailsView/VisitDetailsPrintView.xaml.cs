﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Composite.Events;

namespace Adviser.Modules.PatientDatabase
{
    /// <summary>
    /// Interaction logic for VisitDetailsPrintView.xaml
    /// </summary>
    public partial class VisitDetailsPrintView : UserControl, IVisitDetailsView
    {
        public VisitDetailsPrintView()
        {
            InitializeComponent();
        }

        #region IVisitDetailsView Members

        IVisitDetailsViewModel IVisitDetailsView.Model
        {
            get
            {
                return this.DataContext as IVisitDetailsViewModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public void ShowConfirmRequestExamPopup()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
