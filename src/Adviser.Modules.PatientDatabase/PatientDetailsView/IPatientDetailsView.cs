﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Modules.PatientDatabase
{
    using Adviser.Client.DataModel;

    public interface IPatientDetailsView
    {
        IPatientsPresentationModel Model { get; set; }
    }
}
