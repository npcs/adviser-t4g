﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Common.Logging
{
    /// <summary>
    /// This formatter is concerned only with a LogEntry's message, ignoring all other information
    /// </summary>
    public class MessageOnlyLogEntryFormatter : LogEntryFormatter
    {
        /// <summary>
        /// A String formatted version of a LogEntry.
        /// </summary>
        /// <param name="logEntry">The LogEntry to format.</param>
        /// <returns>The LogEntry message.</returns>
        protected internal override String AsString(ILogEntry logEntry)
        {
            return logEntry.LogItemType;
        }

        /// <summary>
        /// Create a new instance of LogEntryMessageOnlyFormatter.
        /// </summary>
        public MessageOnlyLogEntryFormatter()
            : base()
        {
        }
    }
}
