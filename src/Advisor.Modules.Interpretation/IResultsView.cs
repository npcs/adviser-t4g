﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advisor.Modules.Interpretation
{
    public interface IResultsView
    {
        void SetLeftView(object view);
        void SetRightView(object view);
    }
}
