﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.IO;

using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Commands;
using Microsoft.Practices.Composite.Wpf.Events;
using Adviser.Common.Domain;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Presentation;
using Advisor.Modules.Readout.Views;

namespace Advisor.Modules.Readout.PresentationModels
{
    /// <summary>
    /// Implements presentation model for data matrix.
    /// </summary>
    public class DataMatrixViewModel : ViewModelBase, IDataMatrixViewModel
    {
        public event EventHandler<DataEventArgs<IDataCellViewModel>> CellSelected = delegate { };

        private IEventAggregator _eventAggregator;
        private DataMatrix<PairMeasurement> _matrix;
        private ObservableCollection<ObservableCollection<DataCellViewModel>> _rows;
        private IValueTransform _valueTransform;
        private double _min;
        private double _max;
        private double _mean;
        private double _deviation;
        private bool _isActive;
        private MeasurementContext _context;
        private string _voltage;
        private string _positiveLeads;
        private string _duration;
        private string _samplingRate;
        private string _graphTitle;

        public DataMatrixViewModel(IEventAggregator eventAggregator, IDataMatrixView matrixView)
        {
            _isActive = false;
            _eventAggregator = eventAggregator;
            View = matrixView;
            matrixView.Model = this;
            matrixView.CellSelected += new EventHandler<DataEventArgs<IDataCellViewModel>>(matrixView_CellSelected);

            SaveFileCommand = new DelegateCommand<string>(OnSaveFileCommand);

            _rows = new ObservableCollection<ObservableCollection<DataCellViewModel>>();
            //- subscribe to global events
            //_eventAggregator.GetEvent<MeasurementCompletedEvent>().Subscribe(OnMeasurementCompleted, ThreadOption.UIThread, true);
            //_eventAggregator.GetEvent<MeasurementResetEvent>().Subscribe(OnMeasurementReset, true);
            //_eventAggregator.GetEvent<MeasurementLoadedEvent>().Subscribe(OnMeasurementLoaded, true);
            _valueTransform = new LinearMapValueTransform(0, 100, 0, 100);

            InitializeDataMatrixView(InitializeDataMatrix());
        }

        #region Private methods

        /// <summary>
        /// Initializes empty data matrix.
        /// </summary>
        /// <returns></returns>
        protected DataMatrix<PairMeasurement> InitializeDataMatrix()
        {
            PairMeasurement defaultVal = new PairMeasurement();
            defaultVal.Add(0F);
            DataMatrix<PairMeasurement> matrix = new DataMatrix<PairMeasurement>(7, 7, defaultVal);
            return matrix;
        }

        /// <summary>
        /// Initializes new DataMatrixView.
        /// </summary>
        /// <param name="matrix"></param>
        private void InitializeDataMatrixView(DataMatrix<PairMeasurement> matrix)
        {
            _rows.Clear();
            Min = 0.0;
            Max = 0.0;
            Mean = 0.0;
            StandardDeviation = 0.0;

            for (int y = 0; y < matrix.ColumnCount; ++y)
            {
                ObservableCollection<DataCellViewModel> row = new ObservableCollection<DataCellViewModel>();
                for (int x = 0; x < matrix.RowCount; ++x)
                {
                    DataCellViewModel cellViewModel = new DataCellViewModel(x, y, matrix[x, y], SourceValueType.Maximum, _valueTransform);
                    row.Add(cellViewModel);
                }
                _rows.Add(row);
            }
        }

        protected void matrixView_CellSelected(object sender, DataEventArgs<IDataCellViewModel> e)
        {
            //if (_isActive)
            //    CellSelected(sender, e);
            CellSelected(sender, e);
        }

        protected void OnMeasurementCompleted(MeasurementEventArgs e)
        {
            MeasurementContext = e.Context;
            Voltage = e.Context.MeasurementSettings.Voltage + " V";
            if (e.Context.MeasurementSettings.IsFrontPositive)
                PositiveLeads = "Front";
            else
                PositiveLeads = "Back";
            Duration = e.Context.MeasurementSettings.PairMeasurementDuration + "ms";
            SamplingRate = e.Context.MeasurementSettings.DeviceSamplingRate + "ms";

            //- update matrix data
            UpdateData(e.MeasurementData);
            _isActive = true;
            
        }

        protected void OnMeasurementLoaded(MeasurementEventArgs e)
        {
            if (e == null)
                OnMeasurementReset(true);
            else
                OnMeasurementCompleted(e);
        }

        protected void OnMeasurementReset(bool e)
        {
            if (_matrix == null)
                return;
            _matrix.Clear();
            _isActive = false;
            UpdateData(_matrix);
            //InitializeDataMatrixView(_matrix);
        }

        protected double CalculateStandardDeviation(double mean)
        {
            double deviationSquare;
            double deviationSquareSum = 0;

            foreach (ObservableCollection<DataCellViewModel> row in Rows)
            {
                foreach (DataCellViewModel cellView in row)
                {
                    double val = cellView.Value;
                    deviationSquare = Math.Pow((val - mean), 2);
                    deviationSquareSum += deviationSquare;
                }
            }

            double variance = deviationSquareSum / (_matrix.ColumnCount * _matrix.RowCount);
            return Math.Sqrt(variance);
        }

        /// <summary>
        /// SaveMeasurements command action.
        /// </summary>
        /// <param name="param"></param>
        protected void SaveMeasurements(string param)
        {
            MeasurementEventArgs payload = new MeasurementEventArgs();
            payload.Context = _context;
            payload.MeasurementData = _matrix;
            _eventAggregator.GetEvent<MeasurementSaveEvent>().Publish(payload);
        }

        /// <summary>
        /// Defines if SaveMeasurements command can be executed.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        protected bool CanSaveMeasurements(string param)
        {
            return true;
        }

        #endregion Private methods

        #region Properties

        public IDataMatrixView View { get; set; }

        public string Message { get; set; }

        public ObservableCollection<ObservableCollection<DataCellViewModel>> Rows
        {
            get { return _rows; }
        }

        //public ObservableCollection<DataCellViewModel> Cells
        //{
        //    get { return _dataCells; }
        //}

        public double Mean
        {
            get
            {
                return _mean;
            }
            private set
            {
                _mean = value;
                base.RaisePropertyChangedEvent("Mean");
            }
        }

        public double StandardDeviation
        {
            get { return _deviation; }
            private set
            {
                _deviation = value;
                base.RaisePropertyChangedEvent("StandardDeviation");
            }
        }

        public double Min
        {
            get { return _min; }
            private set
            {
                _min = value;
                base.RaisePropertyChangedEvent("Min");
            }
        }

        public double Max
        {
            get { return _max; }
            private set
            {
                _max = value;
                base.RaisePropertyChangedEvent("Max");
            }
        }

        public MeasurementContext MeasurementContext
        {
            get { return _context; }
            private set
            {
                _context = value;
                RaisePropertyChangedEvent("MeasurementContext");
            }
        }

        public string Voltage
        {
            get { return _voltage; }
            private set
            {
                _voltage = value;
                RaisePropertyChangedEvent("Voltage");
            }
        }

        public string PositiveLeads
        {
            get { return _positiveLeads; }
            private set
            {
                _positiveLeads = value;
                RaisePropertyChangedEvent("PositiveLeads");
            }
        }

        public string Duration
        {
            get { return _duration; }
            private set
            {
                _duration = value;
                RaisePropertyChangedEvent("Duration");
            }
        }

        public string SamplingRate
        {
            get { return _samplingRate; }
            private set
            {
                _samplingRate = value;
                RaisePropertyChangedEvent("SamplingRate");
            }
        }

        public string GraphTitle
        {
            get { return _graphTitle; }
            private set
            {
                _graphTitle = value;
                RaisePropertyChangedEvent("GraphTitle");
            }
        }

        public DelegateCommand<string> SaveFileCommand
        {
            get;
            private set;
        }

        #endregion Properties

        #region    Public methods

        public void UpdateData(DataMatrix<PairMeasurement> matrix)
        {
            
            //TODO: implement more elegant dynamic transform resolution
            if (_valueTransform is LinearMapValueTransform)
            {
                _valueTransform.SetParameters(new object[] { (object)matrix.Min, (object)matrix.Max });
            }

            _matrix = matrix;

            double sum = 0;
            double? min = null;
            double? max = null;        

            for (int y = 0; y < _matrix.ColumnCount; ++y)
            {
                ObservableCollection<DataCellViewModel> row = Rows[y];
                for (int x = 0; x < _matrix.RowCount; ++x)
                {
                    PairMeasurement pair = _matrix[x, y] as PairMeasurement;
                    if (pair == null)
                        continue;
                    DataCellViewModel cellViewModel = row[x];
                    cellViewModel.SourceValue = SourceValueType.Maximum;
                    cellViewModel.ValueTransformation = _valueTransform;
                    cellViewModel.SetData(pair);

                    double val = cellViewModel.Value;
                    sum += cellViewModel.Value;

                    if (min == null)
                        min = val;
                    else
                        min = val < min ? val : min;

                    if (max == null)
                        max = val;
                    else
                        max = val > max ? val : max;
                }
                
            }
            int n = _matrix.ColumnCount * _matrix.RowCount;
            Mean = sum / n;
            Min = (double)min;
            Max = (double)max;
            StandardDeviation = CalculateStandardDeviation(Mean);
        }

        #endregion

        private void OnSaveFileCommand(string visit)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".csv"; // Default file extension
            //dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;
                SaveToFile(filename, visit);
            }

        }

        public void SaveToFile(string fileName, string title)
        {
            _matrix.CalculateStatistics();

            StreamWriter writer = new StreamWriter(fileName);
            try
            {
                writer.WriteLine("Visit: " + title);
                writer.WriteLine("1. Relative values:");
                
                writer.WriteLine(",F1,F2,F3,F4,F5,F6,F7,XAvg");
                string line = "";
                double diagSum = 0;

                for (int y = 0; y < _matrix.RowCount; ++y)
                {
                    line = "B" + (y + 1).ToString();
                    double rowSum = 0;
                    for (int x = 0; x < _matrix.ColumnCount; ++x)
                    {
                        double val = Rows[y][x].Value;
                        line += "," + val.ToString("F2");

                        //- accum diag sum
                        if (x == y)
                            diagSum += val;

                        //- calculate row sum
                        rowSum += val;
                    }
                    //- row average
                    line += "," + (rowSum / _matrix.ColumnCount).ToString("F2");
                    writer.WriteLine(line);
                }

                //- calculate column averages
                line = "YAvg";
                for (int x = 0; x < _matrix.ColumnCount; ++x)
                {
                    double colSum = 0;
                    for (int y = 0; y < _matrix.RowCount; ++y)
                    {
                        colSum += Rows[y][x].Value;
                    }
                    line += "," + (colSum / _matrix.RowCount).ToString("F2");
                }
                //- diag mean
                double diagMean = diagSum / _matrix.RowCount;
                line += "," + diagMean.ToString("F2");
                writer.WriteLine(line);
                writer.WriteLine("Mean: " + Mean.ToString("F2"));
                writer.WriteLine("Diagonal Mean: " + diagMean.ToString("F2"));
                writer.WriteLine("Standard deviation: " + StandardDeviation.ToString("F2"));

                writer.WriteLine(Environment.NewLine);
                writer.WriteLine("1. Absolute values (conductivity):");
                writer.WriteLine(",F1,F2,F3,F4,F5,F6,F7,XAvg");
                diagSum = 0;
                line = String.Empty;
                double deviationSquare;
                double deviationSquareSum = 0;
                double mean = _matrix.Mean;
                for (int y = 0; y < _matrix.RowCount; ++y)
                {
                    line = "B" + (y + 1).ToString();
                    double rowSum = 0;
                    for (int x = 0; x < _matrix.ColumnCount; ++x)
                    {
                        double val = _matrix[x, y].Maximum;
                        line += "," + val.ToString("F8");

                        //- accum diag sum
                        if (x == y)
                            diagSum += val;

                        //- calculate row sum
                        rowSum += val;

                        deviationSquare = Math.Pow((val - mean), 2);
                        deviationSquareSum += deviationSquare;
                    }
                    //- row average
                    line += "," + (rowSum / _matrix.ColumnCount).ToString("F8");
                    writer.WriteLine(line);
                }
                //- calculate deviation
                double variance = deviationSquareSum / (_matrix.ColumnCount * _matrix.RowCount);
                double stdDev = Math.Sqrt(variance);

                //- calculate column averages
                line = "YAvg";
                for (int x = 0; x < _matrix.ColumnCount; ++x)
                {
                    double colSum = 0;
                    for (int y = 0; y < _matrix.RowCount; ++y)
                    {
                        colSum += _matrix[x,y].Maximum;
                    }
                    line += "," + (colSum / _matrix.RowCount).ToString("F8");
                }
                //- diag mean
                diagMean = diagSum / _matrix.RowCount;
                line += "," + diagMean.ToString("F8");
                writer.WriteLine(line);
                writer.WriteLine("Min: " + _matrix.Min.ToString("F8"));
                writer.WriteLine("Max: " + _matrix.Max.ToString("F8"));
                writer.WriteLine("Mean: " + _matrix.Mean.ToString("F8"));
                writer.WriteLine("Diagonal Mean: " + diagMean.ToString("F8"));
                writer.WriteLine("Standard deviation: " + stdDev.ToString("F8"));

            }
            finally
            {
                writer.Close();
                writer.Dispose();
            }
        }
    }
}
