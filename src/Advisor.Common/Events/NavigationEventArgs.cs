﻿using System;

namespace Advisor.Common.Events
{
    [Serializable()]
    public class NavigationEventArgs
    {
        public string EventName { get; set; }
        public object State { get; set; }
    }
}
