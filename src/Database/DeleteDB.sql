
/****** Object:  Table [dbo].[OrganZoneDependency]    Script Date: 02/22/2009 14:14:06 ******/
DROP TABLE [dbo].[OrganZoneDependency]
GO
/****** Object:  Table [dbo].[PractitionerPrescriptions]    Script Date: 02/22/2009 14:14:38 ******/
DROP TABLE [dbo].[PractitionerPrescriptions]
GO
/****** Object:  Table [dbo].[SymptomMedicineXRef]    Script Date: 02/22/2009 14:14:57 ******/
DROP TABLE [dbo].[SymptomMedicineXRef]
GO
/****** Object:  Table [dbo].[SymptomVisitXRef]    Script Date: 02/22/2009 14:15:12 ******/
DROP TABLE [dbo].[SymptomVisitXRef]
GO
/****** Object:  Table [dbo].[MedicineSymptomXRef]    Script Date: 02/22/2009 14:15:52 ******/
DROP TABLE [dbo].[MedicineSymptomXRef]
GO
/****** Object:  Table [dbo].[ConditionMedicineXRef]    Script Date: 02/22/2009 14:16:18 ******/
DROP TABLE [dbo].[ConditionMedicineXRef]
GO
/****** Object:  Table [dbo].[MedicineOrganConditionXRef]    Script Date: 02/22/2009 14:16:52 ******/
DROP TABLE [dbo].[MedicineOrganConditionXRef]
GO
/****** Object:  Table [dbo].[OrganExamination]    Script Date: 02/22/2009 14:17:25 ******/
DROP TABLE [dbo].[OrganExamination]
GO
/****** Object:  Table [dbo].[Organs]    Script Date: 02/22/2009 14:17:42 ******/
DROP TABLE [dbo].[Organs]
GO
/****** Object:  Table [dbo].[Conditions]    Script Date: 02/22/2009 14:18:10 ******/
DROP TABLE [dbo].[Conditions]
GO
/****** Object:  Table [dbo].[Symptoms ]    Script Date: 02/22/2009 14:18:49 ******/
DROP TABLE [dbo].[Symptoms ]
GO
/****** Object:  Table [dbo].[Prescriptions]    Script Date: 02/22/2009 14:19:28 ******/
DROP TABLE [dbo].[Prescriptions]
GO
/****** Object:  Table [dbo].[Medicine]    Script Date: 02/22/2009 14:19:42 ******/
DROP TABLE [dbo].[Medicine]
GO
/****** Object:  Table [dbo].[Visits]    Script Date: 02/22/2009 14:20:12 ******/
DROP TABLE [dbo].[Visits]
GO
/****** Object:  Table [dbo].[Patients]    Script Date: 02/22/2009 14:20:06 ******/
DROP TABLE [dbo].[Patients]
GO
/****** Object:  Table [dbo].[Practitioners]    Script Date: 02/22/2009 14:20:36 ******/
DROP TABLE [dbo].[Practitioners]
GO
/****** Object:  Table [dbo].[MedicineChronicOrgan]    Script Date: 02/23/2009 11:00:58 ******/
DROP TABLE [dbo].[MedicineChronicOrgan]
GO
/****** Object:  StoredProcedure [dbo].[CreateNewVisit]    Script Date: 03/10/2009 17:30:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateNewVisit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CreateNewVisit]

GO
/****** Object:  StoredProcedure [dbo].[DeleteMedicineOrganConditionXRef]    Script Date: 03/10/2009 17:31:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteMedicineOrganConditionXRef]
GO
/****** Object:  StoredProcedure [dbo].[DeleteMedicineSymptomXRef]    Script Date: 03/10/2009 17:31:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteMedicineSymptomXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteMedicineSymptomXRef]


GO
/****** Object:  StoredProcedure [dbo].[DeleteSymptomVisitXRef]    Script Date: 03/10/2009 17:31:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteSymptomVisitXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteSymptomVisitXRef]

GO
/****** Object:  StoredProcedure [dbo].[DeleteVisit]    Script Date: 03/10/2009 17:31:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteVisit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteVisit]

GO
/****** Object:  StoredProcedure [dbo].[GetConditionByValue]    Script Date: 03/10/2009 17:32:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetConditionByValue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetConditionByValue]

GO
/****** Object:  StoredProcedure [dbo].[InsertMedicineOrganConditionXRef]    Script Date: 03/10/2009 17:32:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertMedicineOrganConditionXRef]

GO
/****** Object:  StoredProcedure [dbo].[InsertMedicineSymptomXRef]    Script Date: 03/10/2009 17:32:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertMedicineSymptomXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertMedicineSymptomXRef]

GO
/****** Object:  StoredProcedure [dbo].[InsertSymptomVisitXRef]    Script Date: 03/10/2009 17:32:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertSymptomVisitXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertSymptomVisitXRef]

GO
/****** Object:  StoredProcedure [dbo].[UpdateMedicineOrganConditionXRef]    Script Date: 03/10/2009 17:33:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateMedicineOrganConditionXRef]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateMedicineOrganConditionXRef]

GO
/****** Object:  StoredProcedure [dbo].[UpdateVisit]    Script Date: 03/10/2009 17:33:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateVisit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateVisit]
GO
/****** Object:  Table [dbo].[Mandalas]    Script Date: 03/18/2009 14:15:30 ******/
DROP TABLE [dbo].[Mandalas]
GO

/****** Object:  StoredProcedure [dbo].[GetMedicineByChronicOrgan]    Script Date: 03/19/2009 21:25:48 ******/
DROP PROCEDURE [dbo].[GetMedicineByChronicOrgan]
GO
/****** Object:  StoredProcedure [dbo].[GetMedicineIdByOrganCondition]    Script Date: 03/19/2009 21:26:35 ******/
DROP PROCEDURE [dbo].[GetMedicineIdByOrganCondition]
GO
/****** Object:  Table [dbo].[Chakras]    Script Date: 04/27/2009 23:27:37 ******/
DROP TABLE [dbo].[Chakras]
GO
/****** Object:  Table [dbo].[DeviceHistory]    Script Date: 05/16/2009 11:26:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeviceHistory]') AND type in (N'U'))
DROP TABLE [dbo].[DeviceHistory]
GO
/****** Object:  Table [dbo].[Devices]    Script Date: 05/16/2009 11:27:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Devices]') AND type in (N'U'))
DROP TABLE [dbo].[Devices]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UsageData_Devices]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsageData]'))
ALTER TABLE [dbo].[UsageData] DROP CONSTRAINT [FK_UsageData_Devices]
GO
/****** Object:  Table [dbo].[UsageData]    Script Date: 05/16/2009 11:27:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsageData]') AND type in (N'U'))
DROP TABLE [dbo].[UsageData]
GO
/****** Object:  Table [dbo].[Devices]    Script Date: 05/16/2009 11:27:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Devices]') AND type in (N'U'))
DROP TABLE [dbo].[Devices]
GO
/****** Object:  StoredProcedure [dbo].[GetDeviceByID]    Script Date: 05/16/2009 11:28:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDeviceByID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetDeviceByID]
GO
/****** Object:  StoredProcedure [dbo].[GetMedicineByOrganCondition]    Script Date: 05/16/2009 11:28:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMedicineByOrganCondition]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetMedicineByOrganCondition]
GO
/****** Object:  StoredProcedure [dbo].[GetPractitionerByEmail]    Script Date: 05/16/2009 11:28:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPractitionerByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPractitionerByEmail]
GO
/****** Object:  StoredProcedure [dbo].[GetPractitionerByID]    Script Date: 05/16/2009 11:29:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPractitionerByID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPractitionerByID]
GO
/****** Object:  StoredProcedure [dbo].[InsertUsageEntry]    Script Date: 05/25/2009 18:43:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertUsageEntry]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertUsageEntry]
GO
/****** Object:  Table [dbo].[ClientErrors]    Script Date: 05/28/2009 23:37:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClientErrors]') AND type in (N'U'))
DROP TABLE [dbo].[ClientErrors]

GO
/****** Object:  StoredProcedure [dbo].[GetPassword]    Script Date: 05/28/2009 23:38:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPassword]

GO
/****** Object:  StoredProcedure [dbo].[InsertVisit]    Script Date: 05/28/2009 23:39:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertVisit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertVisit]

GO
/****** Object:  Table [dbo].[Errors]    Script Date: 06/10/2009 22:47:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Errors]') AND type in (N'U'))
DROP TABLE [dbo].[Errors]

GO
/****** Object:  Table [dbo].[Properties]    Script Date: 06/10/2009 22:47:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Properties]') AND type in (N'U'))
DROP TABLE [dbo].[Properties]

GO
/****** Object:  Table [dbo].[SyncItems]    Script Date: 06/10/2009 22:48:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SyncItems]') AND type in (N'U'))
DROP TABLE [dbo].[SyncItems]

GO
/****** Object:  StoredProcedure [dbo].[InsertError]    Script Date: 06/10/2009 22:48:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertError]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertError]

GO
/****** Object:  Table [dbo].[ServerErrors]    Script Date: 08/26/2009 16:38:21 ******/
DROP TABLE [dbo].[ServerErrors]

GO
/****** Object:  Table [dbo].[Accounts]    Script Date: 08/26/2009 16:38:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Accounts]') AND type in (N'U'))
DROP TABLE [dbo].[Accounts]

GO
/****** Object:  StoredProcedure [dbo].[ExportCustomers]    Script Date: 08/26/2009 16:39:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ExportCustomers]

GO
/****** Object:  StoredProcedure [dbo].[ExportInvoices]    Script Date: 08/26/2009 16:39:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportInvoices]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ExportInvoices]

GO
/****** Object:  Table [dbo].[Changes]    Script Date: 08/27/2009 11:06:23 ******/
DROP TABLE [dbo].[Changes]
GO

/****** Object:  Table [dbo].[Electrodes]    Script Date: 09/02/2009 11:21:44 ******/
DROP TABLE [dbo].[Electrodes]
GO
