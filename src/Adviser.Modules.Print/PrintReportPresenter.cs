﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Wpf;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Wpf.Commands;

using Adviser.Client.Core.Presentation;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Services;

using System.Printing;
using System.Windows.Xps;
using System.Windows.Documents.Serialization;
using System.Windows.Documents;
using System.Windows;
using System.Windows.Media;

namespace Adviser.Modules.Print
{
    public interface IPrintReportPresenter
    {
    }

    public class PrintReportPresenter
    {
        private IUnityContainer _container;
        public DelegateCommand<object> PrintReportCommand { get; private set; }

        public PrintReportPresenter(IPrintReportView view, IUnityContainer container, IEventAggregator eventAgregator)
        {
            _container = container;
            View = view;
            View.Model = this;
            PrintSections = new ObservableCollection<PrintPresenter>();
            eventAgregator.GetEvent<NavigationEvent>().Subscribe(OnNavigationEvent, true);
            PrintReportCommand = new DelegateCommand<object>(PrintReport);
        }

        private void PrintReport(object parameters)
        {
            PrintDocumentImageableArea imageArea = null;
            XpsDocumentWriter xpdw = PrintQueue.CreateXpsDocumentWriter(ref imageArea);
            if (xpdw != null)
            {
                double leftMargin = imageArea.OriginWidth;
                double topMargin = imageArea.OriginHeight;
                double rightMargin = imageArea.MediaSizeWidth - imageArea.ExtentWidth - leftMargin;
                double bottomMargin = imageArea.MediaSizeHeight - imageArea.ExtentHeight - topMargin;

                Size outputSize = new Size(imageArea.MediaSizeWidth, imageArea.MediaSizeHeight);
                SerializerWriterCollator c = xpdw.CreateVisualsCollator();
                c.BeginBatchWrite();


                Thickness margingThickness = new Thickness(leftMargin, topMargin,
                                    rightMargin, bottomMargin);

                foreach (PrintPresenter P in PrintSections)
                {
                    if (P.SelectedToPrint)
                    {                        
                        P.PrintableView.Margin = margingThickness;
                        P.PrintableView.Measure(outputSize);
                        P.PrintableView.Arrange(new Rect(outputSize));
                        //P.PrintableView.UpdateLayout();    -- no need to call    UpdateLayout after  Arrange call.                
                        c.Write(P.PrintableView);                       
                    }
                }
                c.EndBatchWrite();
            }
        }

        public IPrintReportView View
        {
            get;
            set;
        }

        public ObservableCollection<PrintPresenter> PrintSections
        {
            get;
            private set;
        }

        /// <summary>
        /// When user selects Print global navigation button this handler will instantiate
        /// IPrintableModel objects in the given context and render appropriate views.
        /// </summary>
        /// <param name="e"></param>
        private void OnNavigationEvent(NavigationEventArgs e)
        {
            if (e.EventName == NavigationEventNames.Print)
            {
                IEnumerable<PrintPresenter> printableModels = _container.ResolveAll<PrintPresenter>();
                IOrderedEnumerable<PrintPresenter> orderedModels = printableModels.OrderBy(model => model.OrderKey);
                PrintSections.Clear();
                foreach (PrintPresenter printModel in orderedModels)
                {
                    PrintSections.Add(printModel);
                }
            }
        }


    }
}
