﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Advisor.Common.DataModel;
using System.Data;
using System.Windows.Media;
using System.Data.Objects;
using System.Data.Objects.DataClasses;

namespace Advisor.Common.Services
{
    public class DBService : Advisor.Common.Services.IDBService
    {
        #region Constructor
        private AdviserDBEntities _dataContext;
        public DBService()
        {
            _dataContext = new AdviserDBEntities();
        }
        #endregion

        public EntityCollection<Patient> RetrivePatientEntityCollection(Practitioner CurrentPractitioner)
        {
            if (!CurrentPractitioner.Patients.IsLoaded)
            {
                CurrentPractitioner.Patients.Load();
            }
            return CurrentPractitioner.Patients;
        }

        public void AddPatient(Practitioner CurrentPractitioner)
        {
            Patient newPatient = new Patient();
            newPatient.ID = Guid.NewGuid();
            newPatient.FirstName = "FirstName";
            newPatient.LastName = "LastName";
            newPatient.LastUpdDate = DateTime.Now.ToUniversalTime();

            CurrentPractitioner.Patients.Add(newPatient);
            _dataContext.SaveChanges();
        }

        public Patient CreateNewPatient(Practitioner CurrentPractitioner)
        {
            Patient newPatient = Patient.CreatePatient(
                Guid.NewGuid(),
                DateTime.Now.ToUniversalTime(),
                "FirstName",
                "LastName",
                "M",
                false);

            CurrentPractitioner.Patients.Add(newPatient);
            _dataContext.AddToPatientSet(newPatient);
            _dataContext.SaveChanges();

            return newPatient;
        }

        public EntityCollection<Visit> RetriveVisitEntityCollection(Patient CurrentPatient)
        {
            if (!CurrentPatient.Visits.IsLoaded)
            {
                CurrentPatient.Visits.Load();
            }
            return CurrentPatient.Visits;
        }

        public IQueryable<OrganZoneDependency> GetOrganZoneDependencies()
        {
            IQueryable<OrganZoneDependency> query = from dep in _dataContext.OrganZoneDependencySet
                                                    select dep;
            return query;
        }


        //public void AddVisit(Patient CurrentPatient)
        //{
        //    Visit newVisit = new Visit();
        //    newVisit.ID = Guid.NewGuid();
        //    newVisit.LastUpdDate = DateTime.Now.ToUniversalTime();
        //    newVisit.Date = DateTime.Now.ToUniversalTime();

        //    CurrentPatient.Visits.Add(newVisit);
        //    _dataContext.SaveChanges();
        //}

        /// <summary>
        /// Creates new Visit in the database for given patient.
        /// </summary>
        /// <param name="CurrentPatient"></param>
        /// <returns></returns>
        public Visit CreateNewVisit(Patient currentPatient)
        {
            //ObjectResult<Visit> result = _dataContext.CreateNewVisit(currentPatient.ID);
            //Visit visit = result.FirstOrDefault();

            Visit visit = Visit.CreateVisit(Guid.NewGuid(), DateTime.Now.ToUniversalTime());
            //visit.Date = DateTime.Now.ToUniversalTime();

            //currentPatient.Visits.Add(visit);
            //CreateOrganExaminationEntityCollection(visit);

            currentPatient.Visits.Add(visit);
            _dataContext.AddToVisitSet(visit);
            _dataContext.SaveChanges();

            return visit;
        }

        /// <summary>
        /// Creates initial collection of OrganExamination entities.
        /// </summary>
        /// <returns></returns>
        public void CreateOrganExaminationEntityCollection(Visit visit)
        {
            //EntityCollection<OrganExamination> organExams = new EntityCollection<OrganExamination>();
            IQueryable<Organ> organs = GetOrgansBySex(visit.Patient.Sex == "M" ? true : false);
            
            foreach(Organ organ in organs)
            {
                OrganExamination exm = OrganExamination.CreateOrganExamination(Guid.NewGuid(), -1, false, false, false, false);
                exm.Organ = organ;
                visit.OrganExaminations.Add(exm);
            }
        }

        public EntityCollection<Symptom> CreateSymptomExaminationEntityCollection()
        {
            EntityCollection<Symptom> symptomExams = new EntityCollection<Symptom>();
            IQueryable<Symptom> symptoms = GetAllSymptoms();
            foreach (Symptom symptom in symptoms)
            {
                symptomExams.Add(Symptom.CreateSymptom(symptom.ID, symptom.Name, symptom.OrderKey));
            }

            return symptomExams;
        }

        public Condition GetCondition(int organId, int examValue)
        {
            //ObjectQuery<Condition> conditions = _dataContext.ConditionSet;
            //IQueryable<Condition> conditionQuery = from c in conditions
            //                                  where c.MinRange < examValue && 
            //                                  c.MaxRange >= examValue
            //                                  select c;

            Condition condition = null;
            ObjectResult<Condition> result = _dataContext.GetConditionByValue(examValue);
            List<Condition> list = result.ToList();
            if (list.Count != 0)
                condition = list[0];

            return condition;
        }

        public EntityCollection<OrganExamination> RetriveOrganExaminationEntityCollection(Visit CurrentVisit)
        {
            if (!CurrentVisit.OrganExaminations.IsLoaded)
            {
                CurrentVisit.OrganExaminations.Load();
            }
            return CurrentVisit.OrganExaminations;
        }

        public object GetObjectByKey(System.Data.EntityKey key)
        {
            return _dataContext.GetObjectByKey(key);
        }

        //public EntityCollection<Condition> RetriveConditionEntityCollection(Organ CurrentOrgan)
        //{
        //    if (!CurrentOrgan.Conditions.IsLoaded)
        //    {
        //        CurrentOrgan.Conditions.Load();
        //    }
        //    return CurrentOrgan.Conditions;
        //}

        public EntityCollection<Symptom> RetriveSymptomEntityCollection(Visit CurrentVisit)
        {
            if (!CurrentVisit.Symptoms.IsLoaded)
            {
                CurrentVisit.Symptoms.Load();
            }
            return CurrentVisit.Symptoms;
        }

        public EntityCollection<Prescription> RetrivePrescriptionEntityCollection(Visit CurrentVisit)
        {
            if (!CurrentVisit.Prescriptions.IsLoaded)
            {
                CurrentVisit.Prescriptions.Load();
            }
            return CurrentVisit.Prescriptions;
        }

        //public IQueryable<Organ> RetriveOrgansCollection()
        //{
        //    IQueryable<Organ> query =
        //                      from organ in _dataContext.OrganSet
        //                      select organ;
        //    return query;                    
        //}

        /// <summary>
        /// Returns all available Organ entities from the database.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Organ> GetAllOrgans()
        {
            IQueryable<Organ> query =
                              from organ in _dataContext.OrganSet
                              orderby organ.OrderKey
                              select organ;
            return query;
        }

        /// <summary>
        /// Returns all available Organ entities from the database.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Organ> GetOrgansBySex(bool isMale)
        {
            int OrganGender = isMale ? 1 : 2;
            IQueryable<Organ> query =
                              from organ in _dataContext.OrganSet
                              where organ.Gender == 0 || organ.Gender == OrganGender
                              orderby organ.OrderKey
                              select organ;
            return query;
        }

        /// <summary>
        /// Returns collection of all available symptoms.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Symptom> GetAllSymptoms()
        {
            IQueryable<Symptom> query =
                              from symptom in _dataContext.SymptomSet
                              select symptom;
            return query;
        }

        /// <summary>
        /// Returns all conditions from the Conditions table.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Condition> GetAllConditions()
        {
            IQueryable<Condition> query =
                from condition in _dataContext.ConditionSet
                orderby condition.OrderKey
                select condition;
            return query;
        }

        /// <summary>
        /// Returns all medicine from the Medicine table.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Medicine> GetAllMedicine()
        {
            IQueryable<Medicine> query =
                from medicine in _dataContext.MedicineSet
                select medicine;

            return query;
        }

        public Medicine GetMedicineByID(int id)
        {
            return _dataContext.MedicineSet.First(m => m.ID == id);
        }

        public void AddMedicineChronicOrgan(MedicineChronicOrgan entry)
        {
            _dataContext.AddToMedicineChronicOrganSet(entry);
        }

        public void AddMedicineOrganCondition(MedicineOrganConditionXRef entry)
        {
            _dataContext.AddToMedicineOrganConditionXRef(entry);
        }

        //public IQueryable<Symptom> GetMedicineBySymptom(Symptom symptom)
        //{
        //    symptom.m
        //}

        /// <summary>
        /// Deletes data corresponding to the entity from the database.
        /// </summary>
        /// <param name="EntityObject"></param>
        public void DeleteObject(Object EntityObject)
        {
            if (EntityObject != null)
            {
                _dataContext.DeleteObject(EntityObject);
                _dataContext.SaveChanges();
            }
        }

        public void SaveChanges()
        {
            _dataContext.SaveChanges();
        }

        //TODO: This is a temp code.
        public Practitioner RetrivePractitioner(Guid PractitionerID)
        {

            return _dataContext.PractitionerSet.First();
        }

    }
}
