﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adviser.Client.DataModel
{
    public partial class Visit
    {
        public Visit()
            : base()
        {
            //this.PropertyChanging += new System.ComponentModel.PropertyChangingEventHandler(Visit_PropertyChanging);
        }

        void Visit_PropertyChanging(object sender, System.ComponentModel.PropertyChangingEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Visit." + e.PropertyName + " is changing");
        }
    }
}
