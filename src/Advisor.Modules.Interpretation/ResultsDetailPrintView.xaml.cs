﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Advisor.Modules.Interpretation.PresentationModels;

namespace Advisor.Modules.Interpretation
{
    /// <summary>
    /// Interaction logic for ResultsDetailPrintView.xaml
    /// </summary>
    public partial class ResultsDetailPrintView : UserControl, IResultsDetailView
    {
        public ResultsDetailPrintView()
        {
            InitializeComponent();
        }



        #region IResultsDetailView Members

        public IHumanBodyViewModel Model
        {
            get
            {
                return this.DataContext as IHumanBodyViewModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

       

        public event EventHandler ShowNotesHistoryClicked;

        #endregion
    }
}
