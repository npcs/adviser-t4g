﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Windows.Media;
using System.Windows.Controls;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Wpf.Events;
using Microsoft.Practices.Composite.Wpf.Commands;

using Adviser.Client.Core.Presentation;
using Advisor.Modules.Interpretation.Views;
using Adviser.Client.DataModel;
using Adviser.Client.Core.Events;
using Adviser.Client.Core.Services;

using System.Printing;
using System.Windows.Xps;

namespace Advisor.Modules.Interpretation.PresentationModels
{
    /// <summary>
    /// Presentation model for the HumanBodyView control. 
    /// Also used for the Organ Condition Summary view. (consider moving to another model)
    /// </summary>
    public class HumanBodyViewModel : ViewModelBase, IHumanBodyViewModel
    {
        private IDBService _service;
        private IUnityContainer _container;
        private Visit _visit;
        private bool isModelSync;
        private AppointmentSession _session;
        private ObservableCollection<OrganViewModel> _organViewCollection;
        private ObservableCollection<ConditionSummaryViewModel> _conditionSummary;
        private ObservableCollection<OrganViewModel> _organViewList;
        private IResultsDetailViewModel _resultsDetail;

        public HumanBodyViewModel(IDBService service, IEventAggregator eventAggregator, IUnityContainer container)
        {
            _service = service;
            _container = container;
            _session = container.Resolve<AppointmentSession>();

            //eventAggregator.GetEvent<VisitSelectedEvent>().Subscribe(OnVisitChange);
            eventAggregator.GetEvent<NavigationEvent>().Subscribe(OnNavigationEvent, ThreadOption.UIThread);
            eventAggregator.GetEvent<ResultsReceivedEvent>().Subscribe(OnResultsReceived, ThreadOption.UIThread);

            if (_session.Visit != null)
                UpdateViewModel(_session.Visit);

        }

        #region IHumanBodyViewModel members

      

        public ObservableCollection<OrganViewModel> OrganViewList
        {
            get { return _organViewList; }
            set
            {
                _organViewList = value;
                base.RaisePropertyChangedEvent("OrganViewList");
            }
        }

        /// <summary>
        /// Observable dictionary exposed for Organ Condition Summary view binding (Charts).
        /// </summary>
        public ObservableCollection<ConditionSummaryViewModel> ConditionSummary
        {
            get { return _conditionSummary; }
            private set 
            { 
                _conditionSummary = value;
                base.RaisePropertyChangedEvent("ConditionSummary");
            }
        }

        public bool IsMale { set; get; }

        /// <summary>
        /// Exposes presentation model for results detail view.
        /// </summary>
        public IResultsDetailViewModel ResultsDetail
        {
            get { return _resultsDetail; }
            set
            {
                _resultsDetail = value;
                base.RaisePropertyChangedEvent("ResultsDetail");
            }
        }

        #endregion IHumanBodyViewModel members

        #region Private Members

        /// <summary>
        /// Updates model when user selects Results button in the navigation menu
        /// and Results Interpretation view becomes visible. This method responds to ResultsClick event only.
        /// </summary>
        /// <param name="e">Navigation event arguments, must contain event name.</param>
        private void OnNavigationEvent(NavigationEventArgs e)
        {
            //TODO: I don't like it. Factor out navigation event processing into presenter code.
            if (e.EventName == NavigationEventNames.Results)
            {
                 UpdateViewModel(_session.Visit);
            }
        }

        /// <summary>
        /// Updates internal reference to the selected Visit.
        /// Sets flag if visit has changed indicates
        /// that model is out of synch with the visit.
        /// </summary>
        /// <param name="visit"></param>
        private void OnVisitChange(Visit visit)
        {
            isModelSync = false;
            _visit = visit;
            //if (_visit == null)
            //{//- new visit
            //    _visit = visit;
            //    isModelSync = false;
            //}
            //else
            //{//- change visit
            //    if (visit.ID != _visit.ID)
            //    {
            //        _visit = visit;
            //        isModelSync = false;
            //    }
            //}
        }

        /// <summary>
        /// Updates model on results received event
        /// if result data corresponds to the currently selected one.
        /// </summary>
        /// <param name="visit">Visit object for which results have been received</param>
        private void OnResultsReceived(ResultsReceivedEventArgs e)
        {
            if (e.IsSuccess && _session.Visit.ID == e.VisitData.ID)
            {
                UpdateViewModel(e.VisitData);
            }
        }

        /// <summary>
        /// Updates data of the model from the current visit.
        /// </summary>
        /// <param name="visit"></param>
        private void UpdateViewModel(Visit visit)
        {
            this.IsMale = visit.Patient.Sex == "M" ? true : false;

            ResultsDetailViewModel resultsVM = _container.Resolve<ResultsDetailViewModel>();
            resultsVM.SetData(visit);
            ResultsDetail = resultsVM;
            
            ////- clear organs presentation models
            //if (OrganViewDictionary == null)
            //    OrganViewDictionary = new ObservableDictionary<string, OrganViewModel>();
            //else
            //    OrganViewDictionary.Clear();
            ObservableCollection<OrganViewModel> organsViewList = new ObservableCollection<OrganViewModel>();
            
            ObservableDictionary<int, ConditionSummaryViewModel> summaryDict = new ObservableDictionary<int, ConditionSummaryViewModel>();
            foreach (Condition condition in _session.AllConditions)
            {
                ConditionSummaryViewModel conditionVM = new ConditionSummaryViewModel(condition);
                conditionVM.ConditionBrush = new SolidColorBrush(HexStringToColor(condition.ColorCode));
                summaryDict.Add(condition.ID, conditionVM);
            }

            if (!visit.OrganExaminations.IsLoaded)
                visit.OrganExaminations.Load();

            string organName;
            int organValue;
            string abbrevName;
            int orderKey;
            Brush organBrush;
            Brush defaultBrush = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            int summaryMax = 0;
            //System.Data.Objects.DataClasses.EntityCollection<OrganExamination> organExaminations = visit.OrganExaminations.Where(
            foreach (OrganExamination organExamination in visit.OrganExaminations)
            {

                // - include organ only if it is in appropriate gender
                if ((organExamination.Organ.Gender == 0) ||                    
                    (this.IsMale && organExamination.Organ.Gender == 1) ||
                    (!this.IsMale && organExamination.Organ.Gender == 2 ))
                {
                    organName = organExamination.Organ.Name;
                    abbrevName = organExamination.Organ.Abbreviation;
                    organValue = organExamination.Value;
                    orderKey = organExamination.Organ.OrderKey;
                    Condition condition = _service.GetConditionByValue(organValue);//TODO: optimize for speed: we don't need Condition object here, just ConditionID
                    if (condition != null)
                    {
                        //- convert color to brush for the presentation
                      
                        organBrush = new SolidColorBrush(HexStringToColor(condition.ColorCode));
                        
                        //- increment count for this condition summary
                        if (summaryDict.ContainsKey(condition.ID))
                        {
                            summaryDict[condition.ID].Count += 1;
                            if (summaryMax < summaryDict[condition.ID].Count)
                                summaryMax = summaryDict[condition.ID].Count;
                        }
                        else
                        {
                            ConditionSummaryViewModel condVM = new ConditionSummaryViewModel(condition);
                            condVM.ConditionBrush = organBrush;
                            summaryDict.Add(condition.ID, condVM);
                        }
                    }
                    else
                    {
                        organBrush = defaultBrush;
                    }

                    OrganViewModel organViewModel = new OrganViewModel(organName, organBrush, true, 
                        organExamination.Surgery,organExamination.ChronicDisease, organValue, abbrevName, orderKey);

                    organsViewList.Add(organViewModel);
                    //OrganViewDictionary.Add(organName, organViewModel);
                }
            }

            OrganViewList = new ObservableCollection<OrganViewModel>(organsViewList.OrderBy(p=>p.OrderKey));

            foreach (ConditionSummaryViewModel condSummVM in summaryDict.Values)
            {
                double ratio = (double)condSummVM.Count / (double)organsViewList.Count;
                condSummVM.Rate = (int)(ratio * 100);
            }
            ConditionSummary = new ObservableCollection<ConditionSummaryViewModel>(summaryDict.Values.ToList());

            isModelSync = true;
        }


        /// <summary>
        /// Converts hexadecimal string to Color struct.
        /// </summary>
        /// <param name="hexColor">Hexadecimal color code as string</param>
        /// <returns>Color structure</returns>
        private Color HexStringToColor(string hexColor)
        {
            if (hexColor.StartsWith("#"))
                hexColor = hexColor.Remove(0, 1);
            byte r = Byte.Parse(hexColor.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = Byte.Parse(hexColor.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = Byte.Parse(hexColor.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);

            Color color = Color.FromRgb(r, g, b);
            return color;
        }

        #endregion Private Members
    }
}
