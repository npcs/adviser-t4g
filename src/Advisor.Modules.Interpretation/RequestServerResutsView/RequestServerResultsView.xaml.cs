﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Composite.Events;
using Adviser.Client.DataModel;

namespace Advisor.Modules.Interpretation
{
    /// <summary>
    /// Interaction logic for RequestServerResutsView.xaml
    /// </summary>
    public partial class RequestServerResultsView : UserControl, IRequestServerResultsView
    {
        public RequestServerResultsView()
        {
            InitializeComponent();
        }

        public event EventHandler ResutsRequested;
        public event EventHandler CancelClick;

        public string UserMessage
        {
            get
            {
                return this.txtMessage.Text;
            }
            set
            {
                this.txtMessage.Text = value;
            }
        }

        public bool SubmitEnabled
        {
            get
            {
                return this.btnRequestServerResuts.IsEnabled;
            }
            set
            {
                this.btnRequestServerResuts.IsEnabled = value;
            }
        }

        public bool CancelEnabled
        {
            get
            {
                return this.btnCancel.IsEnabled;
            }
            set
            {
                this.btnCancel.IsEnabled = value;
            }
        }


        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (CancelClick != null)
                CancelClick(this, null);
        }

        private void btnRequestServerResuts_Click(object sender, RoutedEventArgs e)
        {
            if (ResutsRequested != null)
            {
                ResutsRequested(this, null);
            }

        }
    }
}
