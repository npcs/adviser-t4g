﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Adviser.Common.Domain;
using Adviser.Client.Core.Presentation;
using Advisor.Modules.Readout.Views;

namespace Advisor.Modules.Readout.PresentationModels
{
    public enum ValuePresentationType
    {
        Raw,
        Scale,
        Relative
    }

    public enum SourceValueType
    {
        Maximum,
        Minimum,
        Mean
    }


    /// <summary>
    /// Implements presentation model for PairMeasurement
    /// </summary>
    public class DataCellViewModel : ViewModelBase, IDataCellViewModel
    {
        private IDataCellView _view;
        private PairMeasurement _data;
        private double _value;
        private SourceValueType _sourceValueType;
        private IValueTransform _valueTransform;
        private int _column;
        private int _row;
        private PairDataSampleCollection _series;

        public DataCellViewModel(int xpos, int ypos, SourceValueType sourceValue, IValueTransform valueTransform) 
            : this(xpos, ypos, new PairMeasurement(), sourceValue, valueTransform)
        { 
        }

        public DataCellViewModel(int xpos, int ypos, PairMeasurement data, SourceValueType sourceValue, IValueTransform valueTransform)
        {
            _column = xpos + 1;
            _row = ypos + 1;
            _sourceValueType = sourceValue;
            _valueTransform = valueTransform;

            SetData(data);
        }

        public IDataCellView View
        {
            get { return _view; }
        }

        public IValueTransform ValueTransformation
        {
            get { return _valueTransform; }
            set 
            { 
                _valueTransform = value;
                SetData(_data);   
                RaisePropertyChangedEvent("Value");
            }
        }

        public SourceValueType SourceValue
        {
            get { return _sourceValueType; }
            set { _sourceValueType = value; }
        }

        public double Value
        {
            get
            {
                return _value;
            }
            private set
            {
                _value = value;
                RaisePropertyChangedEvent("Value");
            }
        }


        public int Column
        {
            get { return _column; }
            private set
            {
                _column = value;
                RaisePropertyChangedEvent("Column");
            }
        }

        public int Row
        {
            get { return _row; }
            private set
            {
                _row = value;
                RaisePropertyChangedEvent("Row");
            }
        }

        public PairDataSampleCollection Series
        {
            get { return _series; }
        }

        public void SetData(PairMeasurement data)
        {
            _data = data;
            double srcVal;
            if (_sourceValueType == SourceValueType.Mean)
                srcVal = _data.Mean;
            else
                srcVal = _data.Maximum;
            Value = ApplyTransform(srcVal);

            _series = new PairDataSampleCollection(data, data.SampleRate, _valueTransform);
        }

        private double GetValue()
        {
            double result;
            switch (_sourceValueType)
            {
                case SourceValueType.Minimum:
                    result = ApplyTransform(_data.Minimum);
                    break;
                case SourceValueType.Mean:
                    result = ApplyTransform(_data.Mean);
                    break;
                default:
                    result = ApplyTransform(_data.Maximum);
                    break;
            }
            return result;
        }

        private double ApplyTransform(double source)
        {
            if (_valueTransform == null)
                return source;
            else
                return _valueTransform.Transform(source);
        }
    }
}
