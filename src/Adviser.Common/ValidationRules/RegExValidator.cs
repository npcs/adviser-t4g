﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Adviser.Common.ValidationRules
{
    public class RegExValidator : ValidationRule
    {
        public string RegExExpression { get; set; }

        private bool IsMatch(string emailAddress)
        {
            Regex reStrict = new Regex(RegExExpression);
            return reStrict.IsMatch(emailAddress);
        }

        public override ValidationResult Validate(object value,
          CultureInfo cultureInfo)
        {

            if (IsMatch((string)value))
                return ValidationResult.ValidResult;
            else
                return new ValidationResult(false, "Not valid e-mail address.");

        }
    }
}
