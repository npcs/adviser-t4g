﻿using System;
using System.Collections.Generic;
using Advisor.Common.DataModel;

namespace Advisor.Common.DataModel
{
    [Serializable()]
    public class MeasurementContext
    {
        public DeviceSettings DeviceSettings { get; set; }
        public MeasurementSettings MeasurementSettings { get; set; }
    }
}
