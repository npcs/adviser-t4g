﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Advisor.Modules.Interpretation.PresentationModels;

namespace Advisor.Modules.Interpretation.Views
{
    public interface IHumanBodyView
    {
        IHumanBodyViewModel Model { get; set; }
        void RenderOrgans();
    }
}
