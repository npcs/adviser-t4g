﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;

using Adviser.Client.Core.Presentation;
using Adviser.Client.Core.Services;
using Adviser.Client.DataModel;
using Advisor.Modules.Interpretation.Views;
using Advisor.Modules.Interpretation.PresentationModels;

namespace Advisor.Modules.Interpretation
{
    public class InterpretationModule : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public InterpretationModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            RegisterViewsAndServices();
            IRegion region = _regionManager.Regions[RegionNames.ContentRegion];
            IResultsPresenter resultsPresenter = _container.Resolve<IResultsPresenter>();

            region.Add(resultsPresenter.View, "IResultsView");
            region.Deactivate(resultsPresenter.View);
        }

        protected void RegisterViewsAndServices()
        {
            _container.RegisterType<AppointmentSession>(new Microsoft.Practices.Unity.ContainerControlledLifetimeManager());
            //_container.RegisterType<IDBService, DBService>(new Microsoft.Practices.Unity.ContainerControlledLifetimeManager());

            _container.RegisterType<IResultsView, ResultsView>();
            _container.RegisterType<IResultsPresenter, ResultsPresenter>();
            _container.RegisterType<IResultsDetailView, ResultsDetailView>();
            _container.RegisterType<IResulsDetailPresenter, ResultsDetailPresenter>();
            _container.RegisterType<IResultsDetailViewModel, ResultsDetailViewModel>();
            
            _container.RegisterType<IHumanBodyView, HumanBodyView>();
            _container.RegisterType<IHumanBodyPresenter, HumanBodyPresenter>();
            _container.RegisterType<IHumanBodyViewModel, HumanBodyViewModel>(new Microsoft.Practices.Unity.ContainerControlledLifetimeManager());
            _container.RegisterType<_3DTools.Viewport3DDecorator, _3DTools.TrackballDecorator>();

            _container.RegisterType<PrintPresenter, ResultsDetailPrintPresenter>("ResultsDetailPrintPresenter");

            _container.RegisterType<IMandalaTherapyPresenter, MandalaTherapyPresenter>();
            _container.RegisterType<IMandalaTherapyView, MandalaTherapyView>();

            _container.RegisterType<IRequestServerResultsView, RequestServerResultsView>();
            _container.RegisterType<IRequestServerResultsPresenter, RequestServerResultsPresenter>();
            
        }
    }
}
